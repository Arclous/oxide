﻿using System.Web.Mvc;
using Oxide.Core.Base.OxideActionFilters;
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.Managers.SettingsManager;
using Oxide.Core.Services.Interface.Registers.FilterRegister;
using Oxide.Core.Services.LogService;

namespace Oxide.Filters
{
    public class FilterRegister : IFilterRegisterer
    {
        public void RegisterFilter(GlobalFilterCollection filters)
        {
            IProviderManager providerManager = new ProviderManager();
            var logService = providerManager.Provide<ILogService>();
            filters.Add(new LoggingExceptionFilter(new HandleErrorAttribute(), logService));

            if (SettingsManager.IsOutputMinifyEnable())
            {
                //For minify html pages on the front side
                filters.Add(new MinifyHtmlAttribute());
            }
        }
    }
}