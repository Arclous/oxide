﻿using System;
using System.Web;
using Oxide.Core;
using Oxide.Core.Helpers;

namespace Oxide
{
    public class Global : HttpApplication
    {
        private static Starter _oxideStarter;
        protected void Application_Start(object sender, EventArgs e)
        {
            _oxideStarter = new Starter();
            _oxideStarter.Application_Start(sender, e);
        }
        protected void Session_Start(object sender, EventArgs e)
        {
            _oxideStarter.Session_Start(sender, e);
            OnlineUsers.OnlineUserStart();
        }
        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            if (!Request.IsLocal)
            {
                if (!Request.Url.Host.StartsWith(Core.Syntax.Constants.Www) && !Request.Url.IsLoopback)
                {
                    var builder = new UriBuilder(Request.Url) { Host = Request.Url.Host.Insert(0, Core.Syntax.Constants.WwwWithDot) };
                    Response.StatusCode = 301;
                    Response.AddHeader(Core.Syntax.Constants.Location, builder.ToString());
                    Response.End();
                }
            }
            _oxideStarter.Application_BeginRequest(sender, e);
        }
        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            _oxideStarter.Application_AuthenticateRequest(sender, e);
        }
        protected void Application_Error(object sender, EventArgs e)
        {
            _oxideStarter.Application_Error(sender, e, Server);
        }
        protected void Session_End(object sender, EventArgs e)
        {
            _oxideStarter.Session_End(sender, e);
            OnlineUsers.OnlineUserFinish();
        }
        protected void Application_End(object sender, EventArgs e)
        {
            _oxideStarter.Application_End(sender, e);
        }
    }
}