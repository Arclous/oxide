using System.Data.Entity.Migrations;
using Oxide.Core.Base.OxideModelManager;
using Oxide.Core.Data.Context;
using Oxide.Core.Managers.AssemblyManager;
using Oxide.Core.Managers.SettingsManager;

namespace Oxide.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<OxideContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = SettingsManager.IsAutoMigrationEnabled();
            AutomaticMigrationDataLossAllowed = SettingsManager.IsDataLossEnabled();
        }
        protected override void Seed(OxideContext context)
        {
            var list = AssemblyManager.GetAllFor<IOxideModelManager>();
            for (var index = 0; index < list.Length; index++)
            {
                var instance = list[index];
                instance.Seed(context);
            }
        }
    }
}