// <auto-generated />
// This file was generated by a T4 template.
// Don't change it directly as your change would get overwritten.  Instead, make changes
// to the .tt file (i.e. the T4 template) and save it to regenerate this file.

// Make sure the compiler doesn't complain about missing Xml comments and CLS compliance
// 0108: suppress "Foo hides inherited member Foo. Use the new keyword if hiding was intended." when a controller and its abstract parent are both processed
// 0114: suppress "Foo.BarController.Baz()' hides inherited member 'Qux.BarController.Baz()'. To make the current member override that implementation, add the override keyword. Otherwise add the new keyword." when an action (with an argument) overrides an action in a parent controller
#pragma warning disable 1591, 3008, 3009, 0108, 0114
#region T4MVC

using System;
using System.Diagnostics;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Mvc.Html;
using System.Web.Routing;
using T4MVC;
namespace Oxide.Areas.Admin.Controllers
{
    public partial class PageController
    {
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected PageController(Dummy d) { }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(ActionResult result)
        {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoute(callInfo.RouteValueDictionary);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(Task<ActionResult> taskResult)
        {
            return RedirectToAction(taskResult.Result);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToActionPermanent(ActionResult result)
        {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoutePermanent(callInfo.RouteValueDictionary);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToActionPermanent(Task<ActionResult> taskResult)
        {
            return RedirectToActionPermanent(taskResult.Result);
        }

        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult EditPage()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.EditPage);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult EditTemplatePage()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.EditTemplatePage);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult ManageWidgets()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.ManageWidgets);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult CreatePage()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.CreatePage);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult UpdatePage()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.UpdatePage);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult UpdateTemplatePage()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.UpdateTemplatePage);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.JsonResult PageList()
        {
            return new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.PageList);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.JsonResult ChangePublishStatus()
        {
            return new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.ChangePublishStatus);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.JsonResult DeletePage()
        {
            return new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.DeletePage);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.JsonResult CheckLanguageAvailability()
        {
            return new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.CheckLanguageAvailability);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.JsonResult CheckNameAvailability()
        {
            return new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.CheckNameAvailability);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.JsonResult ClonePage()
        {
            return new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.ClonePage);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.JsonResult GetLayoutsWithWidgets()
        {
            return new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.GetLayoutsWithWidgets);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public PageController Actions { get { return MVC.Admin.Page; } }
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Area = "Admin";
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Name = "Page";
        [GeneratedCode("T4MVC", "2.0")]
        public const string NameConst = "Page";
        [GeneratedCode("T4MVC", "2.0")]
        static readonly ActionNamesClass s_actions = new ActionNamesClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionNamesClass ActionNames { get { return s_actions; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNamesClass
        {
            public readonly string NewPage = "NewPage";
            public readonly string DisplayPages = "DisplayPages";
            public readonly string EditPage = "EditPage";
            public readonly string EditTemplatePage = "EditTemplatePage";
            public readonly string ManageWidgets = "ManageWidgets";
            public readonly string CreatePage = "CreatePage";
            public readonly string UpdatePage = "UpdatePage";
            public readonly string UpdateTemplatePage = "UpdateTemplatePage";
            public readonly string PageList = "PageList";
            public readonly string ChangePublishStatus = "ChangePublishStatus";
            public readonly string DeletePage = "DeletePage";
            public readonly string LanguageList = "LanguageList";
            public readonly string CheckLanguageAvailability = "CheckLanguageAvailability";
            public readonly string CheckNameAvailability = "CheckNameAvailability";
            public readonly string ClonePage = "ClonePage";
            public readonly string GetLayoutsWithWidgets = "GetLayoutsWithWidgets";
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNameConstants
        {
            public const string NewPage = "NewPage";
            public const string DisplayPages = "DisplayPages";
            public const string EditPage = "EditPage";
            public const string EditTemplatePage = "EditTemplatePage";
            public const string ManageWidgets = "ManageWidgets";
            public const string CreatePage = "CreatePage";
            public const string UpdatePage = "UpdatePage";
            public const string UpdateTemplatePage = "UpdateTemplatePage";
            public const string PageList = "PageList";
            public const string ChangePublishStatus = "ChangePublishStatus";
            public const string DeletePage = "DeletePage";
            public const string LanguageList = "LanguageList";
            public const string CheckLanguageAvailability = "CheckLanguageAvailability";
            public const string CheckNameAvailability = "CheckNameAvailability";
            public const string ClonePage = "ClonePage";
            public const string GetLayoutsWithWidgets = "GetLayoutsWithWidgets";
        }


        static readonly ActionParamsClass_EditPage s_params_EditPage = new ActionParamsClass_EditPage();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_EditPage EditPageParams { get { return s_params_EditPage; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_EditPage
        {
            public readonly string pageId = "pageId";
            public readonly string returnUrl = "returnUrl";
        }
        static readonly ActionParamsClass_EditTemplatePage s_params_EditTemplatePage = new ActionParamsClass_EditTemplatePage();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_EditTemplatePage EditTemplatePageParams { get { return s_params_EditTemplatePage; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_EditTemplatePage
        {
            public readonly string pageId = "pageId";
            public readonly string returnUrl = "returnUrl";
        }
        static readonly ActionParamsClass_ManageWidgets s_params_ManageWidgets = new ActionParamsClass_ManageWidgets();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_ManageWidgets ManageWidgetsParams { get { return s_params_ManageWidgets; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_ManageWidgets
        {
            public readonly string pageId = "pageId";
        }
        static readonly ActionParamsClass_CreatePage s_params_CreatePage = new ActionParamsClass_CreatePage();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_CreatePage CreatePageParams { get { return s_params_CreatePage; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_CreatePage
        {
            public readonly string model = "model";
        }
        static readonly ActionParamsClass_UpdatePage s_params_UpdatePage = new ActionParamsClass_UpdatePage();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_UpdatePage UpdatePageParams { get { return s_params_UpdatePage; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_UpdatePage
        {
            public readonly string model = "model";
        }
        static readonly ActionParamsClass_UpdateTemplatePage s_params_UpdateTemplatePage = new ActionParamsClass_UpdateTemplatePage();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_UpdateTemplatePage UpdateTemplatePageParams { get { return s_params_UpdateTemplatePage; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_UpdateTemplatePage
        {
            public readonly string model = "model";
        }
        static readonly ActionParamsClass_PageList s_params_PageList = new ActionParamsClass_PageList();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_PageList PageListParams { get { return s_params_PageList; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_PageList
        {
            public readonly string pageSize = "pageSize";
            public readonly string page = "page";
            public readonly string filter = "filter";
            public readonly string filterType = "filterType";
        }
        static readonly ActionParamsClass_ChangePublishStatus s_params_ChangePublishStatus = new ActionParamsClass_ChangePublishStatus();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_ChangePublishStatus ChangePublishStatusParams { get { return s_params_ChangePublishStatus; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_ChangePublishStatus
        {
            public readonly string pageId = "pageId";
            public readonly string status = "status";
        }
        static readonly ActionParamsClass_DeletePage s_params_DeletePage = new ActionParamsClass_DeletePage();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_DeletePage DeletePageParams { get { return s_params_DeletePage; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_DeletePage
        {
            public readonly string pageId = "pageId";
        }
        static readonly ActionParamsClass_CheckLanguageAvailability s_params_CheckLanguageAvailability = new ActionParamsClass_CheckLanguageAvailability();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_CheckLanguageAvailability CheckLanguageAvailabilityParams { get { return s_params_CheckLanguageAvailability; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_CheckLanguageAvailability
        {
            public readonly string pageId = "pageId";
            public readonly string laguageId = "laguageId";
        }
        static readonly ActionParamsClass_CheckNameAvailability s_params_CheckNameAvailability = new ActionParamsClass_CheckNameAvailability();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_CheckNameAvailability CheckNameAvailabilityParams { get { return s_params_CheckNameAvailability; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_CheckNameAvailability
        {
            public readonly string pageId = "pageId";
            public readonly string name = "name";
        }
        static readonly ActionParamsClass_ClonePage s_params_ClonePage = new ActionParamsClass_ClonePage();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_ClonePage ClonePageParams { get { return s_params_ClonePage; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_ClonePage
        {
            public readonly string pageId = "pageId";
            public readonly string laguageId = "laguageId";
            public readonly string name = "name";
        }
        static readonly ActionParamsClass_GetLayoutsWithWidgets s_params_GetLayoutsWithWidgets = new ActionParamsClass_GetLayoutsWithWidgets();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_GetLayoutsWithWidgets GetLayoutsWithWidgetsParams { get { return s_params_GetLayoutsWithWidgets; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_GetLayoutsWithWidgets
        {
            public readonly string pageId = "pageId";
        }
        static readonly ViewsClass s_views = new ViewsClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ViewsClass Views { get { return s_views; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ViewsClass
        {
            static readonly _ViewNamesClass s_ViewNames = new _ViewNamesClass();
            public _ViewNamesClass ViewNames { get { return s_ViewNames; } }
            public class _ViewNamesClass
            {
                public readonly string DisplayPages = "DisplayPages";
                public readonly string EditPage = "EditPage";
                public readonly string EditTemplatePage = "EditTemplatePage";
                public readonly string ManageWidgets = "ManageWidgets";
                public readonly string NewPage = "NewPage";
            }
            public readonly string DisplayPages = "~/Areas/Admin/Views/Page/DisplayPages.cshtml";
            public readonly string EditPage = "~/Areas/Admin/Views/Page/EditPage.cshtml";
            public readonly string EditTemplatePage = "~/Areas/Admin/Views/Page/EditTemplatePage.cshtml";
            public readonly string ManageWidgets = "~/Areas/Admin/Views/Page/ManageWidgets.cshtml";
            public readonly string NewPage = "~/Areas/Admin/Views/Page/NewPage.cshtml";
        }
    }

    [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
    public partial class T4MVC_PageController : Oxide.Areas.Admin.Controllers.PageController
    {
        public T4MVC_PageController() : base(Dummy.Instance) { }

        [NonAction]
        partial void NewPageOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult NewPage()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.NewPage);
            NewPageOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void DisplayPagesOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult DisplayPages()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.DisplayPages);
            DisplayPagesOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void EditPageOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, int pageId, string returnUrl);

        [NonAction]
        public override System.Web.Mvc.ActionResult EditPage(int pageId, string returnUrl)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.EditPage);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "pageId", pageId);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "returnUrl", returnUrl);
            EditPageOverride(callInfo, pageId, returnUrl);
            return callInfo;
        }

        [NonAction]
        partial void EditTemplatePageOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, int pageId, string returnUrl);

        [NonAction]
        public override System.Web.Mvc.ActionResult EditTemplatePage(int pageId, string returnUrl)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.EditTemplatePage);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "pageId", pageId);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "returnUrl", returnUrl);
            EditTemplatePageOverride(callInfo, pageId, returnUrl);
            return callInfo;
        }

        [NonAction]
        partial void ManageWidgetsOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, int pageId);

        [NonAction]
        public override System.Web.Mvc.ActionResult ManageWidgets(int pageId)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.ManageWidgets);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "pageId", pageId);
            ManageWidgetsOverride(callInfo, pageId);
            return callInfo;
        }

        [NonAction]
        partial void CreatePageOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, Oxide.Core.ViewModels.PageViewModels.PageViewModel model);

        [NonAction]
        public override System.Web.Mvc.ActionResult CreatePage(Oxide.Core.ViewModels.PageViewModels.PageViewModel model)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.CreatePage);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "model", model);
            CreatePageOverride(callInfo, model);
            return callInfo;
        }

        [NonAction]
        partial void UpdatePageOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, Oxide.Core.ViewModels.PageViewModels.PageViewModel model);

        [NonAction]
        public override System.Web.Mvc.ActionResult UpdatePage(Oxide.Core.ViewModels.PageViewModels.PageViewModel model)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.UpdatePage);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "model", model);
            UpdatePageOverride(callInfo, model);
            return callInfo;
        }

        [NonAction]
        partial void UpdateTemplatePageOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, Oxide.Core.ViewModels.PageViewModels.TemplatePageViewModel model);

        [NonAction]
        public override System.Web.Mvc.ActionResult UpdateTemplatePage(Oxide.Core.ViewModels.PageViewModels.TemplatePageViewModel model)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.UpdateTemplatePage);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "model", model);
            UpdateTemplatePageOverride(callInfo, model);
            return callInfo;
        }

        [NonAction]
        partial void PageListOverride(T4MVC_System_Web_Mvc_JsonResult callInfo, int? pageSize, int? page, string filter, int filterType);

        [NonAction]
        public override System.Web.Mvc.JsonResult PageList(int? pageSize, int? page, string filter, int filterType)
        {
            var callInfo = new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.PageList);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "pageSize", pageSize);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "page", page);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "filter", filter);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "filterType", filterType);
            PageListOverride(callInfo, pageSize, page, filter, filterType);
            return callInfo;
        }

        [NonAction]
        partial void ChangePublishStatusOverride(T4MVC_System_Web_Mvc_JsonResult callInfo, int pageId, bool status);

        [NonAction]
        public override System.Web.Mvc.JsonResult ChangePublishStatus(int pageId, bool status)
        {
            var callInfo = new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.ChangePublishStatus);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "pageId", pageId);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "status", status);
            ChangePublishStatusOverride(callInfo, pageId, status);
            return callInfo;
        }

        [NonAction]
        partial void DeletePageOverride(T4MVC_System_Web_Mvc_JsonResult callInfo, int pageId);

        [NonAction]
        public override System.Web.Mvc.JsonResult DeletePage(int pageId)
        {
            var callInfo = new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.DeletePage);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "pageId", pageId);
            DeletePageOverride(callInfo, pageId);
            return callInfo;
        }

        [NonAction]
        partial void LanguageListOverride(T4MVC_System_Web_Mvc_JsonResult callInfo);

        [NonAction]
        public override System.Web.Mvc.JsonResult LanguageList()
        {
            var callInfo = new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.LanguageList);
            LanguageListOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void CheckLanguageAvailabilityOverride(T4MVC_System_Web_Mvc_JsonResult callInfo, int pageId, int laguageId);

        [NonAction]
        public override System.Web.Mvc.JsonResult CheckLanguageAvailability(int pageId, int laguageId)
        {
            var callInfo = new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.CheckLanguageAvailability);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "pageId", pageId);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "laguageId", laguageId);
            CheckLanguageAvailabilityOverride(callInfo, pageId, laguageId);
            return callInfo;
        }

        [NonAction]
        partial void CheckNameAvailabilityOverride(T4MVC_System_Web_Mvc_JsonResult callInfo, int pageId, string name);

        [NonAction]
        public override System.Web.Mvc.JsonResult CheckNameAvailability(int pageId, string name)
        {
            var callInfo = new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.CheckNameAvailability);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "pageId", pageId);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "name", name);
            CheckNameAvailabilityOverride(callInfo, pageId, name);
            return callInfo;
        }

        [NonAction]
        partial void ClonePageOverride(T4MVC_System_Web_Mvc_JsonResult callInfo, int pageId, int laguageId, string name);

        [NonAction]
        public override System.Web.Mvc.JsonResult ClonePage(int pageId, int laguageId, string name)
        {
            var callInfo = new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.ClonePage);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "pageId", pageId);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "laguageId", laguageId);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "name", name);
            ClonePageOverride(callInfo, pageId, laguageId, name);
            return callInfo;
        }

        [NonAction]
        partial void GetLayoutsWithWidgetsOverride(T4MVC_System_Web_Mvc_JsonResult callInfo, int pageId);

        [NonAction]
        public override System.Web.Mvc.JsonResult GetLayoutsWithWidgets(int pageId)
        {
            var callInfo = new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.GetLayoutsWithWidgets);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "pageId", pageId);
            GetLayoutsWithWidgetsOverride(callInfo, pageId);
            return callInfo;
        }

    }
}

#endregion T4MVC
#pragma warning restore 1591, 3008, 3009, 0108, 0114
