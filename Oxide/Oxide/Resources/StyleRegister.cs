﻿using System.Web.Optimization;
using Oxide.Core.Managers.SettingsManager;
using Oxide.Core.Services.Interface.Registers.ResourceRegister;

namespace Oxide.Resources
{
    public class StyleRegister : IResourceRegisterer
    {
        public void RegisterResource(BundleCollection boundleCollection)
        {
            boundleCollection.Add(new StyleBundle("~/Content/css").Include("~/Content/bootstrap.css",
                "~/Content/site.css"));

            if (SettingsManager.IsOutputMinifyEnable())
            {
                boundleCollection.Add(new StyleBundle("~/css/inline").Include("~/Content/css/Styles/critical/inline.css"));
            }
        }
    }
}