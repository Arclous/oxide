﻿using System.Web.Optimization;
using Oxide.Core.Managers.SettingsManager;
using Oxide.Core.Services.Interface.Registers.ResourceRegister;

namespace Oxide.Resources
{
    public class ScriptRegister : IResourceRegisterer
    {
        public void RegisterResource(BundleCollection boundleCollection)
        {
            boundleCollection.Add(new ScriptBundle("~/bundles/jquery").Include("~/Scripts/jquery-{version}.js"));
            boundleCollection.Add(new ScriptBundle("~/bundles/jqueryval").Include("~/Scripts/jquery.validate*"));
            boundleCollection.Add(new ScriptBundle("~/bundles/modernizr").Include("~/Scripts/modernizr-*"));
            boundleCollection.Add(new ScriptBundle("~/bundles/bootstrap").Include("~/Scripts/bootstrap.js",
                "~/Scripts/respond.js"));

            if (SettingsManager.IsOutputMinifyEnable())
            {
                boundleCollection.Add(new ScriptBundle("~/script/inline").Include("~/Scripts/minify/inline.js"));
            }
        }
    }
}