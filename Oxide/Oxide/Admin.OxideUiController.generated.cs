// <auto-generated />
// This file was generated by a T4 template.
// Don't change it directly as your change would get overwritten.  Instead, make changes
// to the .tt file (i.e. the T4 template) and save it to regenerate this file.

// Make sure the compiler doesn't complain about missing Xml comments and CLS compliance
// 0108: suppress "Foo hides inherited member Foo. Use the new keyword if hiding was intended." when a controller and its abstract parent are both processed
// 0114: suppress "Foo.BarController.Baz()' hides inherited member 'Qux.BarController.Baz()'. To make the current member override that implementation, add the override keyword. Otherwise add the new keyword." when an action (with an argument) overrides an action in a parent controller
#pragma warning disable 1591, 3008, 3009, 0108, 0114
#region T4MVC

using System;
using System.Diagnostics;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Mvc.Html;
using System.Web.Routing;
using T4MVC;
namespace Oxide.Areas.Admin.Controllers
{
    public partial class OxideUiController
    {
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected OxideUiController(Dummy d) { }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(ActionResult result)
        {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoute(callInfo.RouteValueDictionary);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(Task<ActionResult> taskResult)
        {
            return RedirectToAction(taskResult.Result);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToActionPermanent(ActionResult result)
        {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoutePermanent(callInfo.RouteValueDictionary);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToActionPermanent(Task<ActionResult> taskResult)
        {
            return RedirectToActionPermanent(taskResult.Result);
        }

        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult RenderContentSelecter()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.RenderContentSelecter);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.JsonResult GetContents()
        {
            return new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.GetContents);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.JsonResult GetDataListContents()
        {
            return new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.GetDataListContents);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.JsonResult GetMediaContentsWithImage()
        {
            return new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.GetMediaContentsWithImage);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.JsonResult GetFileContents()
        {
            return new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.GetFileContents);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.JsonResult GetMediaContents()
        {
            return new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.GetMediaContents);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public OxideUiController Actions { get { return MVC.Admin.OxideUi; } }
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Area = "Admin";
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Name = "OxideUi";
        [GeneratedCode("T4MVC", "2.0")]
        public const string NameConst = "OxideUi";
        [GeneratedCode("T4MVC", "2.0")]
        static readonly ActionNamesClass s_actions = new ActionNamesClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionNamesClass ActionNames { get { return s_actions; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNamesClass
        {
            public readonly string RenderContentSelecter = "RenderContentSelecter";
            public readonly string GetContents = "GetContents";
            public readonly string GetDataListContents = "GetDataListContents";
            public readonly string GetMediaContentsWithImage = "GetMediaContentsWithImage";
            public readonly string GetFileContents = "GetFileContents";
            public readonly string GetMediaContents = "GetMediaContents";
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNameConstants
        {
            public const string RenderContentSelecter = "RenderContentSelecter";
            public const string GetContents = "GetContents";
            public const string GetDataListContents = "GetDataListContents";
            public const string GetMediaContentsWithImage = "GetMediaContentsWithImage";
            public const string GetFileContents = "GetFileContents";
            public const string GetMediaContents = "GetMediaContents";
        }


        static readonly ActionParamsClass_RenderContentSelecter s_params_RenderContentSelecter = new ActionParamsClass_RenderContentSelecter();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_RenderContentSelecter RenderContentSelecterParams { get { return s_params_RenderContentSelecter; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_RenderContentSelecter
        {
            public readonly string contentName = "contentName";
        }
        static readonly ActionParamsClass_GetContents s_params_GetContents = new ActionParamsClass_GetContents();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_GetContents GetContentsParams { get { return s_params_GetContents; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_GetContents
        {
            public readonly string contentName = "contentName";
            public readonly string filter = "filter";
            public readonly string filterCount = "filterCount";
        }
        static readonly ActionParamsClass_GetDataListContents s_params_GetDataListContents = new ActionParamsClass_GetDataListContents();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_GetDataListContents GetDataListContentsParams { get { return s_params_GetDataListContents; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_GetDataListContents
        {
            public readonly string contentName = "contentName";
            public readonly string page = "page";
            public readonly string filter = "filter";
            public readonly string pageSize = "pageSize";
            public readonly string withPicture = "withPicture";
            public readonly string pictureField = "pictureField";
        }
        static readonly ActionParamsClass_GetMediaContentsWithImage s_params_GetMediaContentsWithImage = new ActionParamsClass_GetMediaContentsWithImage();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_GetMediaContentsWithImage GetMediaContentsWithImageParams { get { return s_params_GetMediaContentsWithImage; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_GetMediaContentsWithImage
        {
            public readonly string contentName = "contentName";
            public readonly string filter = "filter";
            public readonly string imageField = "imageField";
            public readonly string filterCount = "filterCount";
        }
        static readonly ActionParamsClass_GetFileContents s_params_GetFileContents = new ActionParamsClass_GetFileContents();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_GetFileContents GetFileContentsParams { get { return s_params_GetFileContents; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_GetFileContents
        {
            public readonly string contentName = "contentName";
            public readonly string filter = "filter";
            public readonly string filterCount = "filterCount";
        }
        static readonly ActionParamsClass_GetMediaContents s_params_GetMediaContents = new ActionParamsClass_GetMediaContents();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_GetMediaContents GetMediaContentsParams { get { return s_params_GetMediaContents; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_GetMediaContents
        {
            public readonly string page = "page";
            public readonly string directoryId = "directoryId";
            public readonly string pageSize = "pageSize";
        }
        static readonly ViewsClass s_views = new ViewsClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ViewsClass Views { get { return s_views; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ViewsClass
        {
            static readonly _ViewNamesClass s_ViewNames = new _ViewNamesClass();
            public _ViewNamesClass ViewNames { get { return s_ViewNames; } }
            public class _ViewNamesClass
            {
                public readonly string CkEditor = "CkEditor";
                public readonly string ColorPicker = "ColorPicker";
                public readonly string ContentList = "ContentList";
                public readonly string ContentListItem = "ContentListItem";
                public readonly string ContentSelectedItem = "ContentSelectedItem";
                public readonly string ContentSelecter = "ContentSelecter";
                public readonly string DataList = "DataList";
                public readonly string DirectoryItem = "DirectoryItem";
                public readonly string ImageContentList = "ImageContentList";
                public readonly string ImageContentListItem = "ImageContentListItem";
                public readonly string ImageContentSelectedItem = "ImageContentSelectedItem";
                public readonly string ImageContentSelecter = "ImageContentSelecter";
                public readonly string MediaContentList = "MediaContentList";
                public readonly string MediaContentListItem = "MediaContentListItem";
                public readonly string MediaContentSelectedItem = "MediaContentSelectedItem";
                public readonly string MediaContentSelecter = "MediaContentSelecter";
            }
            public readonly string CkEditor = "~/Areas/Admin/Views/OxideUi/CkEditor.cshtml";
            public readonly string ColorPicker = "~/Areas/Admin/Views/OxideUi/ColorPicker.cshtml";
            public readonly string ContentList = "~/Areas/Admin/Views/OxideUi/ContentList.cshtml";
            public readonly string ContentListItem = "~/Areas/Admin/Views/OxideUi/ContentListItem.cshtml";
            public readonly string ContentSelectedItem = "~/Areas/Admin/Views/OxideUi/ContentSelectedItem.cshtml";
            public readonly string ContentSelecter = "~/Areas/Admin/Views/OxideUi/ContentSelecter.cshtml";
            public readonly string DataList = "~/Areas/Admin/Views/OxideUi/DataList.cshtml";
            public readonly string DirectoryItem = "~/Areas/Admin/Views/OxideUi/DirectoryItem.cshtml";
            public readonly string ImageContentList = "~/Areas/Admin/Views/OxideUi/ImageContentList.cshtml";
            public readonly string ImageContentListItem = "~/Areas/Admin/Views/OxideUi/ImageContentListItem.cshtml";
            public readonly string ImageContentSelectedItem = "~/Areas/Admin/Views/OxideUi/ImageContentSelectedItem.cshtml";
            public readonly string ImageContentSelecter = "~/Areas/Admin/Views/OxideUi/ImageContentSelecter.cshtml";
            public readonly string MediaContentList = "~/Areas/Admin/Views/OxideUi/MediaContentList.cshtml";
            public readonly string MediaContentListItem = "~/Areas/Admin/Views/OxideUi/MediaContentListItem.cshtml";
            public readonly string MediaContentSelectedItem = "~/Areas/Admin/Views/OxideUi/MediaContentSelectedItem.cshtml";
            public readonly string MediaContentSelecter = "~/Areas/Admin/Views/OxideUi/MediaContentSelecter.cshtml";
        }
    }

    [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
    public partial class T4MVC_OxideUiController : Oxide.Areas.Admin.Controllers.OxideUiController
    {
        public T4MVC_OxideUiController() : base(Dummy.Instance) { }

        [NonAction]
        partial void RenderContentSelecterOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string contentName);

        [NonAction]
        public override System.Web.Mvc.ActionResult RenderContentSelecter(string contentName)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.RenderContentSelecter);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "contentName", contentName);
            RenderContentSelecterOverride(callInfo, contentName);
            return callInfo;
        }

        [NonAction]
        partial void GetContentsOverride(T4MVC_System_Web_Mvc_JsonResult callInfo, string contentName, string filter, int filterCount);

        [NonAction]
        public override System.Web.Mvc.JsonResult GetContents(string contentName, string filter, int filterCount)
        {
            var callInfo = new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.GetContents);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "contentName", contentName);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "filter", filter);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "filterCount", filterCount);
            GetContentsOverride(callInfo, contentName, filter, filterCount);
            return callInfo;
        }

        [NonAction]
        partial void GetDataListContentsOverride(T4MVC_System_Web_Mvc_JsonResult callInfo, string contentName, int page, string filter, int pageSize, bool withPicture, string pictureField);

        [NonAction]
        public override System.Web.Mvc.JsonResult GetDataListContents(string contentName, int page, string filter, int pageSize, bool withPicture, string pictureField)
        {
            var callInfo = new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.GetDataListContents);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "contentName", contentName);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "page", page);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "filter", filter);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "pageSize", pageSize);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "withPicture", withPicture);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "pictureField", pictureField);
            GetDataListContentsOverride(callInfo, contentName, page, filter, pageSize, withPicture, pictureField);
            return callInfo;
        }

        [NonAction]
        partial void GetMediaContentsWithImageOverride(T4MVC_System_Web_Mvc_JsonResult callInfo, string contentName, string filter, string imageField, int filterCount);

        [NonAction]
        public override System.Web.Mvc.JsonResult GetMediaContentsWithImage(string contentName, string filter, string imageField, int filterCount)
        {
            var callInfo = new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.GetMediaContentsWithImage);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "contentName", contentName);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "filter", filter);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "imageField", imageField);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "filterCount", filterCount);
            GetMediaContentsWithImageOverride(callInfo, contentName, filter, imageField, filterCount);
            return callInfo;
        }

        [NonAction]
        partial void GetFileContentsOverride(T4MVC_System_Web_Mvc_JsonResult callInfo, string contentName, string filter, int filterCount);

        [NonAction]
        public override System.Web.Mvc.JsonResult GetFileContents(string contentName, string filter, int filterCount)
        {
            var callInfo = new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.GetFileContents);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "contentName", contentName);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "filter", filter);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "filterCount", filterCount);
            GetFileContentsOverride(callInfo, contentName, filter, filterCount);
            return callInfo;
        }

        [NonAction]
        partial void GetMediaContentsOverride(T4MVC_System_Web_Mvc_JsonResult callInfo, int page, int directoryId, int pageSize);

        [NonAction]
        public override System.Web.Mvc.JsonResult GetMediaContents(int page, int directoryId, int pageSize)
        {
            var callInfo = new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.GetMediaContents);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "page", page);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "directoryId", directoryId);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "pageSize", pageSize);
            GetMediaContentsOverride(callInfo, page, directoryId, pageSize);
            return callInfo;
        }

    }
}

#endregion T4MVC
#pragma warning restore 1591, 3008, 3009, 0108, 0114
