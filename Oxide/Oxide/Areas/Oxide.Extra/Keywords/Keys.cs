﻿namespace Oxide.Extra.Keywords
{
    public static class Keys
    {
        public enum CommentStatus
        {
            WaitingForApprove = 0,
            Approved = 1
        }

        public enum LoginResult
        {
            Success = 0,
            NotSuccess = 1,
            UserNotFound = 2
        }

        public enum TeamWidgetStyle
        {
            Team = 0,
            TeamGrid = 1,
            TeamBoxed = 2,
            TeamClean = 3
        }

        public enum UserStatus
        {
            Online = 0,
            Offline = 1
        }

        public const string TeamMember = "TeamMember";
        public const string TeamMemberWidget = "TeamMemberWidget";
        public const string TimelineItem = "TimelineItem";

        //User Registiration Constants
        public const string UserRegistirationWidget = "UserRegistirationWidget";
        public const string UserRegistirationFrontWidget = "UserRegistirationFrontWidget";
        public const string UserRegistirationEmailInformerWidget = "UserRegistirationEmailInformerWidget";
        public const string LoginWidget = "LoginWidget";
        public const string LoginValidatorWidget = "LoginValidatorWidget";
        public const string UserRegistirationActivationWidget = "UserRegistirationActivationWidget";
        public const string PasswordRecoveryWdiget = "PasswordRecoveryWdiget";

        //Comments Widget Constants
        public const string Comments = "Comments";
    }
}