﻿using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Extra.Models.Comments
{
    [OxideDatabase("OxideExtraContext")]
    public class CommentsSettings : OxideDatabaseModel, IOxideDataBaseModel
    {
        public int Key { get; set; }
        public bool LoginRequired { get; set; }
        public bool ApproveRequired { get; set; }
        public int MaxCommentsToDisplay { get; set; }
        public string PleaseLoginMessage { get; set; }
        public string FirstCommentMessage { get; set; }
        public string AfterCommentMessage { get; set; }
    }
}