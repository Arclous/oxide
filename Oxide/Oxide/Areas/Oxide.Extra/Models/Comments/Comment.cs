﻿using System;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;
using Oxide.Extra.Keywords;

namespace Oxide.Extra.Models.Comments
{
    [OxideDatabase("OxideExtraContext")]
    public class Comment : OxideDatabaseModel, IOxideDataBaseModel
    {
        public string Message { get; set; }
        public DateTime CommentDateTime { get; set; }
        public User.User User { get; set; }
        public Keys.CommentStatus CommentStatus { get; set; }
        public int Key { get; set; }
    }
}