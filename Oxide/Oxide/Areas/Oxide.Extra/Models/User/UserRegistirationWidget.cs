﻿using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Extra.Models.User
{
    [OxideDatabase("OxideExtraContext")]
    public class UserRegistirationWidget : OxideDatabaseModel, IOxideDataBaseModel
    {
        public int Key { get; set; }
        public int ImageId { get; set; }
        public string License { get; set; }
        public bool ShowLicense { get; set; }
        public bool SendConfirmationEmail { get; set; }
        public int ConfirmationEmailId { get; set; }
        public int ConfirmationPageId { get; set; }
        public int ConfirmationEmailSentPageId { get; set; }
        public int LoginPageId { get; set; }
        public int ValidDays { get; set; }
    }
}