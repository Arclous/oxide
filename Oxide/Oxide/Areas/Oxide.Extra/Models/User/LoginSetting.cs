﻿using Oxide.Areas.Admin.Models;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Extra.Models.User
{
    public class LoginSetting : OxideDatabaseModel, IOxideDataBaseModel
    {
        public int ConfirmationPageId { get; set; }
        public int ConfirmationSentPageId { get; set; }
        public int LoginPageId { get; set; }
        public int PasswordResetPageId { get; set; }
        public int UserSettingPageId { get; set; }
        public int ConfirmationEmailId { get; set; }
        public int RecoveryEmailId { get; set; }
        public virtual Language Culture { get; set; }
    }
}