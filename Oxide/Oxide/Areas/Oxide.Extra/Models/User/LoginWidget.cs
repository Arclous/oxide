﻿using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Extra.Models.User
{
    [OxideDatabase("OxideExtraContext")]
    public class LoginWidget : OxideDatabaseModel, IOxideDataBaseModel
    {
        public int Key { get; set; }
        public int PasswordResetPageId { get; set; }
        public int ImageId { get; set; }
    }
}