﻿using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Extra.Models.User
{
    [OxideDatabase("OxideExtraContext")]
    public class PasswordRecoveryWidget : OxideDatabaseModel, IOxideDataBaseModel
    {
        public int Key { get; set; }
        public int EmailId { get; set; }
        public int ValidMinutes { get; set; }
    }
}