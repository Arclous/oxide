﻿using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Extra.Models.User
{
    [OxideDatabase("OxideExtraContext")]
    public class LoginValidatorWidget : OxideDatabaseModel, IOxideDataBaseModel
    {
        public int Key { get; set; }
        public int LoginPageId { get; set; }
    }
}