﻿using System;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Extra.Models.User
{
    public class UserConfirmation : OxideDatabaseModel, IOxideDataBaseModel
    {
        public int UserId { get; set; }
        public DateTime ConfirmationSent { get; set; }
        public int ValidDays { get; set; }
        public string Key { get; set; }
        public int WidgetHash { get; set; }
    }
}