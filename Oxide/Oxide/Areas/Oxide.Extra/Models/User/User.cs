﻿using System;
using Oxide.Core.Base.OxideDataBaseModel;
using Oxide.Media.Models.File;

namespace Oxide.Extra.Models.User
{
    public class User : OxideDatabaseModel, IOxideDataBaseModel
    {
        public string Surname { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsEmailConfirmed { get; set; }
        public DateTime CreateDateTime { get; set; }
        public DateTime LastAccessDateTime { get; set; }
        public int UsrImageId { get; set; }
        public File UserImage { get; set; }
        public bool Activated { get; set; }
        public string LoginKey { get; set; }
    }
}