﻿using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Extra.Models.User
{
    [OxideDatabase("OxideExtraContext")]
    public class UserRegistirationEmailInformerWidget : OxideDatabaseModel, IOxideDataBaseModel
    {
        public int Key { get; set; }
        public int ImageId { get; set; }
        public string Message { get; set; }
    }
}