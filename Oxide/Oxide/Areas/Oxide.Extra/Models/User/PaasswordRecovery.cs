﻿using System;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Extra.Models.User
{
    [OxideDatabase("OxideExtraContext")]
    public class PasswordRecoveryItem : OxideDatabaseModel, IOxideDataBaseModel
    {
        public int UserId { get; set; }
        public string RecoveryCode { get; set; }
        public int ValidMinutes { get; set; }
        public DateTime CreataionDate { get; set; }
    }
}