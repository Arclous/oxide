﻿using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Extra.Models.Team
{
    [OxideDatabase("OxideExtraContext")]
    public class TeamWidgetItem : OxideDatabaseModel, IOxideDataBaseModel
    {
        public virtual int TeamWidgetId { get; set; }
        public virtual int TeamMemberItemId { get; set; }
    }
}