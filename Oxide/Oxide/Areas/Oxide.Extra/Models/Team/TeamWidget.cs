﻿using System.Collections.Generic;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;
using Oxide.Extra.Keywords;

namespace Oxide.Extra.Models.Team
{
    [OxideDatabase("OxideExtraContext")]
    public class TeamWidget : OxideDatabaseModel, IOxideDataBaseModel
    {
        public int Key { get; set; }
        public virtual Keys.TeamWidgetStyle Type { get; set; }
        public virtual ICollection<TeamWidgetItem> TeamMemberItems { get; set; }
        public string Information { get; set; }
    }
}