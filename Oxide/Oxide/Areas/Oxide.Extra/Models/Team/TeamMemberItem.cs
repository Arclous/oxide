﻿using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Extra.Models.Team
{
    [OxideDatabase("OxideExtraContext")]
    public class TeamMemberItem : OxideDatabaseModel, IOxideDataBaseModel
    {
        public string MemberName { get; set; }
        public string Info { get; set; }
        public string Description { get; set; }
        public string TwiterLink { get; set; }
        public string FacebookLink { get; set; }
        public string LinkedInLink { get; set; }
        public int ImageId { get; set; }
    }
}