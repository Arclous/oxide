﻿using System.Collections.Generic;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Extra.Models.Timeline
{
    [OxideDatabase("OxideExtraContext")]
    public class TimelineWidget : OxideDatabaseModel, IOxideDataBaseModel
    {
        public int Key { get; set; }
        public virtual ICollection<TimelineWidgetItem> TimelineItems { get; set; }
    }
}