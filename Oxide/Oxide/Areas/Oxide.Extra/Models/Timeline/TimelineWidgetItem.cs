﻿using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Extra.Models.Timeline
{
    [OxideDatabase("OxideExtraContext")]
    public class TimelineWidgetItem : OxideDatabaseModel, IOxideDataBaseModel
    {
        public virtual int TimelineWidgetId { get; set; }
        public virtual int TimelineItemId { get; set; }
    }
}