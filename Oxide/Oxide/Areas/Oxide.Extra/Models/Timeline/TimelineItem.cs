﻿using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Extra.Models.Timeline
{
    [OxideDatabase("OxideExtraContext")]
    public class TimelineItem : OxideDatabaseModel, IOxideDataBaseModel
    {
        public string Description { get; set; }
        public int ImageId { get; set; }
    }
}