﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.Models.Menus;
using Oxide.Core.Services.AdminMenuService;
using Oxide.Core.Services.Interface.Registers.AdminMenuRegister;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Syntax;

namespace Oxide.Extra.Admin
{
    public class AdminMenu : IAdminMenuRegisterer
    {
        private const string CreateTeamMemberPermissionKey = "Can_Create_Team_Member";
        private const string DisplayTeamMembersPermissionKey = "Can_Display_Team_Members";
        private const string CanManageLoginSystemWidgets = "Can_Manage_Login_System_Widget";
        private const string AreaName = "Oxide.Extra";
        private readonly IProviderManager _providerManager = new ProviderManager();

        public void BuildMenus(RequestContext requestContext, IAdminMenuService adminMenuService)
        {
            var urlHelper = new UrlHelper(requestContext);
            var oxideService = _providerManager.Provide<IOxideServices>();

            //*******************************************************************************************************************************************
            //Navigation Header
            var navigationCreate = adminMenuService.AddHeaderMenu("Oxide_Extra",
                oxideService.GetText("Oxide Extra", AreaName), Syntax.CssClass.FaCertificate, 3, PermissionKeys.Empty);

            //Team Widget Main Menu
            var menuMainItem = new MenuItem
            {
                Key = "Team Widget",
                Title = oxideService.GetText("Team Widget", AreaName),
                Order = 0,
                SubMenuItems = new List<MenuItem>()
            };
            adminMenuService.AddMenuItem(navigationCreate, menuMainItem);

            //Create Team Member
            var menuItem = new MenuItem
            {
                Key = "Create Team Member",
                Title = oxideService.GetText("Create Team Member", AreaName),
                ControllerName = "TeamMember",
                ActionName = "CreateTeamMember",
                Order = 0,
                PermissionKey = CreateTeamMemberPermissionKey
            };
            menuItem.UrlAddress = urlHelper.Action(menuItem.ActionName, menuItem.ControllerName, new {area = AreaName});
            menuMainItem.SubMenuItems.Add(menuItem);

            //*******************************************************************************************************************************************
            //Display Team Members
            menuItem = new MenuItem
            {
                Key = "Display Team Members",
                Title = oxideService.GetText("Display Team Members", AreaName),
                ControllerName = "TeamMember",
                ActionName = "DisplayTeamMembers",
                Order = 0,
                PermissionKey = DisplayTeamMembersPermissionKey
            };
            menuItem.UrlAddress = urlHelper.Action(menuItem.ActionName, menuItem.ControllerName, new {area = AreaName});
            menuMainItem.SubMenuItems.Add(menuItem);

            //*******************************************************************************************************************************************
            menuMainItem = new MenuItem
            {
                Key = "Timeline Widget",
                Title = oxideService.GetText("Timeline Widget", AreaName),
                Order = 0,
                SubMenuItems = new List<MenuItem>()
            };
            adminMenuService.AddMenuItem(navigationCreate, menuMainItem);

            //Create Team Member
            menuItem = new MenuItem
            {
                Key = "Create Timeline Item",
                Title = oxideService.GetText("Create Timeline Item", AreaName),
                ControllerName = "Timeline",
                ActionName = "CreateTimelineItem",
                Order = 0,
                PermissionKey = CreateTeamMemberPermissionKey
            };
            menuItem.UrlAddress = urlHelper.Action(menuItem.ActionName, menuItem.ControllerName, new {area = AreaName});
            menuMainItem.SubMenuItems.Add(menuItem);

            //*******************************************************************************************************************************************
            //Display Team Members
            menuItem = new MenuItem
            {
                Key = "Display Timeline Items",
                Title = oxideService.GetText("Display Timeline Items", AreaName),
                ControllerName = "Timeline",
                ActionName = "DisplayTimelineItems",
                Order = 0,
                PermissionKey = DisplayTeamMembersPermissionKey
            };
            menuItem.UrlAddress = urlHelper.Action(menuItem.ActionName, menuItem.ControllerName, new {area = AreaName});
            menuMainItem.SubMenuItems.Add(menuItem);

            //*******************************************************************************************************************************************
            menuMainItem = new MenuItem
            {
                Key = "Login Settings",
                Title = oxideService.GetText("Login Settings", AreaName),
                Order = 0,
                SubMenuItems = new List<MenuItem>()
            };
            adminMenuService.AddMenuItem(navigationCreate, menuMainItem);

            //Create Team Member
            menuItem = new MenuItem
            {
                Key = "Display Settings",
                Title = oxideService.GetText("Display Settings", AreaName),
                ControllerName = "User",
                ActionName = "DisplaySettings",
                Order = 0,
                PermissionKey = CanManageLoginSystemWidgets
            };
            menuItem.UrlAddress = urlHelper.Action(menuItem.ActionName, menuItem.ControllerName, new {area = AreaName});
            menuMainItem.SubMenuItems.Add(menuItem);

            //*******************************************************************************************************************************************
            //Display Team Members
            menuItem = new MenuItem
            {
                Key = "New Setting",
                Title = oxideService.GetText("New Setting", AreaName),
                ControllerName = "User",
                ActionName = "NewSetting",
                Order = 0,
                PermissionKey = CanManageLoginSystemWidgets
            };
            menuItem.UrlAddress = urlHelper.Action(menuItem.ActionName, menuItem.ControllerName, new {area = AreaName});
            menuMainItem.SubMenuItems.Add(menuItem);
        }
    }
}