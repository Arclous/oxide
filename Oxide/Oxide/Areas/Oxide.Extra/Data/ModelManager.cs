﻿using System.Data.Entity;
using Oxide.Core.Base.OxideModelManager;
using Oxide.Core.Data.Context;

namespace Oxide.Extra.Data
{
    public class ModelManager : IOxideModelManager
    {
        public void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }

        public void Seed(OxideContext context)
        {
        }
    }
}