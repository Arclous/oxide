﻿using System.Web.Optimization;
using Oxide.Core.Services.Interface.Registers.ResourceRegister;

namespace Oxide.Extra.Resources
{
    public class ModelRegister : IResourceRegisterer
    {
        public void RegisterResource(BundleCollection boundleCollection)
        {
            boundleCollection.Add(
                new ScriptBundle("~/models/CommentsWidgetModel").Include(
                    "~/Areas/Oxide.Extra/Scripts/Models/CommentsWidgetModel.js"));
        }
    }
}