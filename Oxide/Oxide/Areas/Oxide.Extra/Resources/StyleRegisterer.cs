﻿using System.Web.Optimization;
using Oxide.Core.Services.Interface.Registers.ResourceRegister;

namespace Oxide.Extra.Resources
{
    public class StyleRegister : IResourceRegisterer
    {
        public void RegisterResource(BundleCollection boundleCollection)
        {
            boundleCollection.Add(
                new StyleBundle("~/OxideExtra/oxideextra").Include("~/Areas/Oxide.Extra/content/oxideextra.css"));
        }
    }
}