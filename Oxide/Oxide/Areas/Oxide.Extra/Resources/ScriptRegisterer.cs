﻿using System.Web.Optimization;
using Oxide.Core.Services.Interface.Registers.ResourceRegister;

namespace Oxide.Extra.Resources
{
    public class ScriptRegisterer : IResourceRegisterer
    {
        public void RegisterResource(BundleCollection boundleCollection)
        {
            boundleCollection.Add(
                new ScriptBundle("~/OxideExtra/bootstrap-filestyle").Include(
                    "~/Areas/Oxide.Extra/Scripts/bootstrap-filestyle.js"));
        }
    }
}