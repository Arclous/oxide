﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using Oxide.Core.Elements;
using Oxide.Core.Helpers;
using Oxide.Core.Managers.CacheManager;
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.Models.Base;
using Oxide.Core.ResultModels;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Services.OxideUiService;
using Oxide.Core.Syntax;
using Oxide.Extra.Keywords;
using Oxide.Extra.Models.Team;
using Oxide.Extra.ViewModels.Team;
using Oxide.Fundamentals.Services.Interface;

namespace Oxide.Extra.Widgets
{
    public class Team : IOxideWidget
    {
        #region WidgetBasic

        public int Id => 1;

        public string Name => "Team";

        public string Title => "Team";

        public string Description => "Widget which can display your teams with details.";

        public string Category => "Extra";

        public string PreviewImageUrl => "/areas/Oxide.Extra/content/images/widgets/Team.png";

        public bool Active { get; set; }

        public string ModuleName => "Oxide.Extra";

        public string FrontView { get; set; } = "Team";

        public string Type2 => "TeamGrid";

        public string Type3 => "TeamBoxed";

        public string Type4 => "TeamClean";

        public string EditorView { get; set; } = "Team_Editor";

        public string WidgetKey { get; set; }

        public string Dependencies { get; set; }

        #endregion

        #region Renders

        WidgetResult IOxideWidget.RenderFront(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var selectedView = FrontView;
            var teamMemberService = providerManager.Provide<ITeamMemberService>();
            var cacheManager = providerManager.Provide<IOxideCacheManager>();
            var oxideUiService = providerManager.Provide<IOxideUiService>();
            var selectedWidget = cacheManager.GetOrSet(widgetInfoPackage.UniqeHash.ToString(), "Team Widget",
                widgetInfoPackage.Title, () => teamMemberService.GetWidget(widgetInfoPackage.UniqeHash));
            var teamWidgetFrontViewModel = new TeamWidgetFrontViewModel
            {
                TeamMemberItems = new List<TeamMemberFrontViewModel>()
            };
            if (selectedWidget != null)
            {
                teamWidgetFrontViewModel.Information = selectedWidget.Information;
                teamWidgetFrontViewModel.Name = selectedWidget.Name;
                teamWidgetFrontViewModel.Title = selectedWidget.Title;
                if (selectedWidget.TeamMemberItems.Count > 0)
                {
                    foreach (var memberId in
                        selectedWidget.TeamMemberItems.OrderBy(x => x.Id).Select(x => x.TeamMemberItemId))
                    {
                        var member = teamMemberService.GetTeamMember(memberId);
                        var memberImage = oxideUiService.GetContent(Constants.FileContent, member.ImageId.ToString());
                        var memberImagePath = ((dynamic) memberImage).Path;
                        var teamMemberFrontViewModel = new TeamMemberFrontViewModel
                        {
                            MemberName = member.MemberName,
                            Info = member.Info,
                            Description = member.Description,
                            TwiterLink = member.TwiterLink,
                            FacebookLink = member.FacebookLink,
                            LinkedInLink = member.LinkedInLink,
                            Name = member.Name,
                            Title = member.Title,
                            ImagePath = string.Format(@"{0}", memberImagePath),
                            BackgorundImagePath = OxideUtils.GetFullUrl(memberImagePath)
                        };
                        teamWidgetFrontViewModel.TeamMemberItems.Add(teamMemberFrontViewModel);
                    }
                    switch (selectedWidget.Type)
                    {
                        case Keys.TeamWidgetStyle.Team:
                            selectedView = FrontView;
                            break;
                        case Keys.TeamWidgetStyle.TeamGrid:
                            selectedView = Type2;
                            break;
                        case Keys.TeamWidgetStyle.TeamBoxed:
                            selectedView = Type3;
                            break;
                        case Keys.TeamWidgetStyle.TeamClean:
                            selectedView = Type4;
                            break;
                        default:
                            selectedView = FrontView;
                            break;
                    }
                }
            }
            var widgetResult = new WidgetResult {ViewName = selectedView, Model = teamWidgetFrontViewModel};
            return widgetResult;
        }

        public WidgetResult RenderAdmin(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var teamMemberService = providerManager.Provide<ITeamMemberService>();
            var selectedWidget = teamMemberService.GetWidget(widgetInfoPackage.UniqeHash);
            var teamWidgetViewModel = new TeamWidgetViewModel();
            var selectedMembers = selectedWidget?.TeamMemberItems;
            if (selectedMembers != null)
                teamWidgetViewModel.TeamMemberItems = string.Join(",",
                    selectedMembers.OrderBy(x => x.Id).Select(x => x.TeamMemberItemId));
            teamWidgetViewModel.UniqeHash = widgetInfoPackage.UniqeHash;
            teamWidgetViewModel.ActiveUrl = HttpContext.Current.Request.RawUrl;
            if (selectedWidget != null)
            {
                teamWidgetViewModel.Information = selectedWidget.Information;
                teamWidgetViewModel.Name = selectedWidget.Name;
                teamWidgetViewModel.Title = selectedWidget.Title;
                teamWidgetViewModel.Type = (int) selectedWidget.Type;
            }
            var widgetResult = new WidgetResult
            {
                ViewName = EditorView,
                Model =
                    new OxideModel
                    {
                        RequestType = Syntax.Requests.Dynamic,
                        Header = widgetInfoPackage.Title,
                        Title = Title,
                        Parameters = teamWidgetViewModel
                    }
            };
            return widgetResult;
        }

        public WidgetUpdatePackage RunUpdate(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager,
            ExpandoObject model)
        {
            var updateResult = new WidgetUpdatePackage();
            var teamMemberService = providerManager.Provide<ITeamMemberService>();
            var oxideServices = providerManager.Provide<IOxideServices>();
            var teamMembers = ((string) ((dynamic) model).MemberSelecter).Split(Constants.Splitter).ToList();
            var widgetType = Convert.ToInt32(((dynamic) model).WidgetStyle.ToString());
            var teamWidget = new TeamWidget
            {
                TeamMemberItems = new List<TeamWidgetItem>(),
                Type = (Keys.TeamWidgetStyle) widgetType,
                Key = widgetInfoPackage.UniqeHash,
                Name = ((dynamic) model).Name,
                Title = ((dynamic) model).Title,
                Information = ((dynamic) model).Information
            };
            if (((dynamic) model).MemberSelecter != "")
            {
                foreach (var teamWidgetItem in
                    teamMembers.Select(v => teamMemberService.GetTeamMember(Convert.ToInt32(v)))
                               .Select(
                                   member =>
                                       new TeamWidgetItem {TeamWidgetId = teamWidget.Id, TeamMemberItemId = member.Id}))
                {
                    teamWidget.TeamMemberItems.Add(teamWidgetItem);
                }
            }
            var saveWidget = teamMemberService.SaveWidget(teamWidget);
            if (saveWidget)
                updateResult.SuccessMessage = oxideServices.GetText("Team Widget saved successfully");
            else
                updateResult.ErrorMessage =
                    oxideServices.GetText(
                        "Problem while saving team widget, operation is not succesfull, please check the logs");
            return updateResult;
        }

        public void OnDeleted(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var teamMemberService = providerManager.Provide<ITeamMemberService>();
            teamMemberService.DeleteWidget(widgetInfoPackage.UniqeHash);
        }

        #endregion
    }
}