﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Web;
using Oxide.Core.Elements;
using Oxide.Core.Helpers;
using Oxide.Core.Managers.CacheManager;
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.Models.Base;
using Oxide.Core.ResultModels;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Services.OxideUiService;
using Oxide.Core.Syntax;
using Oxide.Extra.Models.User;
using Oxide.Extra.Services.Interface;
using Oxide.Extra.ViewModels.User;

namespace Oxide.Extra.Widgets
{
    public class UserRegistirationEmailInformer : IOxideWidget
    {
        #region WidgetBasic

        public int Id => 1;

        public string Name => "User Registiration Email Informer";

        public string Title => "User Registiration Email Informer";

        public string Description
            =>
                "It is the widget which you can add to the page which users see after registration, this widget gives opportunity to user to send confirmation email again in case of needed."
            ;

        public string Category => "User System";

        public string PreviewImageUrl => "/areas/Oxide.Extra/content/images/widgets/UserRegistirationEmailInformer.png";

        public bool Active { get; set; }

        public string ModuleName => "Oxide.Extra";

        public string FrontView { get; set; } = "UserRegistirationEmailInformer";

        public string EditorView { get; set; } = "UserRegistirationEmailInformer_Editor";

        public string WidgetKey { get; set; }

        public string Dependencies { get; set; }

        #endregion

        #region Renders

        WidgetResult IOxideWidget.RenderFront(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var userRegistirationEmailInformerViewModel = new UserRegistirationEmailInformerViewModel();
            var reg = "";
            if (HttpContext.Current.Request.QueryString["reg"] != "" &&
                HttpContext.Current.Request.QueryString["reg"] != null)
                reg =
                    OxideUtils.GetDecryptedQueryString(
                        HttpUtility.UrlDecode(HttpContext.Current.Request.QueryString["reg"]));
            else
            {
                userRegistirationEmailInformerViewModel.Valid = false;
                return new WidgetResult {ViewName = FrontView, Model = userRegistirationEmailInformerViewModel};
            }
            var userService = providerManager.Provide<IUserService>();
            var cacheManager = providerManager.Provide<IOxideCacheManager>();
            var oxideUiService = providerManager.Provide<IOxideUiService>();
            var user = userService.GetUser(Convert.ToInt32(reg));
            if (user == null)
            {
                userRegistirationEmailInformerViewModel.Valid = false;
                return new WidgetResult {ViewName = FrontView, Model = userRegistirationEmailInformerViewModel};
            }
            var previousUserConfirmation = userService.GetConfirmation(user.Id);
            if (previousUserConfirmation == null)
            {
                userRegistirationEmailInformerViewModel.Valid = false;
                return new WidgetResult {ViewName = FrontView, Model = userRegistirationEmailInformerViewModel};
            }
            var selectedWidget = cacheManager.GetOrSet(widgetInfoPackage.UniqeHash.ToString(), widgetInfoPackage.Title,
                "User Login System",
                () => userService.GetUserRegistirationEmailInformerWidget(widgetInfoPackage.UniqeHash));
            if (selectedWidget != null)
            {
                var widgetImagePath = oxideUiService.GetImageContentUrl(selectedWidget.ImageId.ToString(),
                    Syntax.UrlType.Absolute);
                userRegistirationEmailInformerViewModel.Valid = true;
                userRegistirationEmailInformerViewModel.Email = user.Email;
                userRegistirationEmailInformerViewModel.ActiveUrl = HttpContext.Current.Request.RawUrl;
                userRegistirationEmailInformerViewModel.UniqeHash = previousUserConfirmation.WidgetHash;
                userRegistirationEmailInformerViewModel.Key = reg;
                userRegistirationEmailInformerViewModel.ImageUrl = widgetImagePath;
                userRegistirationEmailInformerViewModel.Name = selectedWidget.Name;
                var keywords = new Dictionary<string, string> {{"[UserEmail]", $@"<strong>{user.Email}</strong>"}};
                userRegistirationEmailInformerViewModel.Message = OxideUtils.FillParameters(selectedWidget.Message,
                    keywords);
            }
            var widgetResult = new WidgetResult {ViewName = FrontView, Model = userRegistirationEmailInformerViewModel};
            return widgetResult;
        }

        public WidgetResult RenderAdmin(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var userService = providerManager.Provide<IUserService>();
            var selectedWidget = userService.GetUserRegistirationEmailInformerWidget(widgetInfoPackage.UniqeHash);
            var widgetViewModel = new UserRegistirationEmailInformerWidgetAdminViewModel
            {
                UniqeHash = widgetInfoPackage.UniqeHash,
                ActiveUrl = HttpContext.Current.Request.RawUrl
            };
            if (selectedWidget != null)
            {
                widgetViewModel.Name = selectedWidget.Name;
                widgetViewModel.ImageId = selectedWidget.ImageId;
                widgetViewModel.Message = selectedWidget.Message;
            }
            var widgetResult = new WidgetResult
            {
                ViewName = EditorView,
                Model =
                    new OxideModel
                    {
                        RequestType = Syntax.Requests.Dynamic,
                        Header = widgetInfoPackage.Title,
                        Title = Title,
                        Parameters = widgetViewModel
                    }
            };
            return widgetResult;
        }

        public WidgetUpdatePackage RunUpdate(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager,
            ExpandoObject model)
        {
            var updateResult = new WidgetUpdatePackage();
            var userService = providerManager.Provide<IUserService>();
            var oxideServices = providerManager.Provide<IOxideServices>();
            var userRegistirationWidget = new UserRegistirationEmailInformerWidget
            {
                Name = ((dynamic) model).Title,
                Title = ((dynamic) model).Title,
                Key = widgetInfoPackage.UniqeHash,
                Message = ((dynamic) model).Message
            };
            if (((dynamic) model).MediaSelecter != string.Empty)
                userRegistirationWidget.ImageId = Convert.ToInt32(((dynamic) model).MediaSelecter);
            var userRegistirationEmailInformerWidget =
                userService.SaveUserRegistirationEmailInformerWidget(userRegistirationWidget);
            if (userRegistirationEmailInformerWidget)
                updateResult.SuccessMessage =
                    oxideServices.GetText("User Registiration Email Informer widget saved successfully");
            else
                updateResult.ErrorMessage =
                    oxideServices.GetText(
                        "Problem while saving User Registiration Email Informer Widget, operation is not succesfull, please check the logs");
            return updateResult;
        }

        public void OnDeleted(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var userService = providerManager.Provide<IUserService>();
            userService.DeleteUserRegistirationEmailInformerWidget(widgetInfoPackage.UniqeHash);
        }

        #endregion
    }
}