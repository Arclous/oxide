﻿using System;
using System.Dynamic;
using System.Web;
using Oxide.Core.Elements;
using Oxide.Core.Managers.CacheManager;
using Oxide.Core.Managers.CultureManager;
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.Models.Base;
using Oxide.Core.ResultModels;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Services.OxideUiService;
using Oxide.Core.Syntax;
using Oxide.Extra.Models.User;
using Oxide.Extra.Services.Interface;
using Oxide.Extra.ViewModels.User;

namespace Oxide.Extra.Widgets
{
    public class LoginValidator : IOxideWidget
    {
        #region Helpers

        protected LoginValidatorWidgetViewModel GetViewModel(WidgetInfoPackage widgetInfoPackage,
            IProviderManager providerManager)
        {
            var userService = providerManager.Provide<IUserService>();
            var cacheManager = providerManager.Provide<IOxideCacheManager>();
            var oxideUiService = providerManager.Provide<IOxideUiService>();
            var cultureManager = providerManager.Provide<ICultureManager>();
            var widget = userService.GetLoginValidatorWidget(widgetInfoPackage.UniqeHash);
            var loginValidatorWidgetViewModel = new LoginValidatorWidgetViewModel();
            if (widget != null)
            {
                //Get specific widget settings
                var loginPageUrl = oxideUiService.GetPageContentUrl(widget.LoginPageId.ToString());

                //Get global settings
                var globalSetting =
                    userService.GetSettingByCulture(cultureManager.GetFrontCurrentCulture().ThreeLetterISOLanguageName);

                //Assing global settings if there is not specific setting for the widget
                if (globalSetting != null)
                {
                    if (loginPageUrl == null && globalSetting.LoginPageId != 0)
                        loginPageUrl = oxideUiService.GetPageContentUrl(globalSetting.LoginPageId.ToString());
                }
                loginValidatorWidgetViewModel.Name = widget.Name;
                loginValidatorWidgetViewModel.LoginPageUrl = loginPageUrl;
                loginValidatorWidgetViewModel.LoginPageId = widget.LoginPageId;
                loginValidatorWidgetViewModel.UniqeHash = widgetInfoPackage.UniqeHash;
            }
            return loginValidatorWidgetViewModel;
        }

        #endregion

        #region WidgetBasic

        public int Id => 1;

        public string Name => "Login Validator";

        public string Title => "Login Validator";

        public string Description => "Widget which can be used to valiate login for any page.";

        public string Category => "User System";

        public string PreviewImageUrl => "/areas/Oxide.Extra/content/images/widgets/UserRegistiration.png";

        public bool Active { get; set; }

        public string ModuleName => "Oxide.Extra";

        public string FrontView { get; set; } = "LoginValidator";

        public string EditorView { get; set; } = "LoginValidator_Editor";

        public string WidgetKey { get; set; }

        public string Dependencies { get; set; }

        #endregion

        #region Renders

        WidgetResult IOxideWidget.RenderFront(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var selectedView = FrontView;
            var loginService = providerManager.Provide<ILoginService>();
            var cacheManager = providerManager.Provide<IOxideCacheManager>();
            var loginValidatorWidgetViewModel = cacheManager.GetOrSet(widgetInfoPackage.UniqeHash.ToString(),
                widgetInfoPackage.Title, "User Login System", () => GetViewModel(widgetInfoPackage, providerManager));

            //No need cache for this value
            loginValidatorWidgetViewModel.ActiveUrl = HttpContext.Current.Request.RawUrl;
            if (!loginService.IsUserLogined())
                HttpContext.Current.Response.Redirect(loginValidatorWidgetViewModel.LoginPageUrl ?? "/", true);
            var widgetResult = new WidgetResult {ViewName = selectedView, Model = loginValidatorWidgetViewModel};
            return widgetResult;
        }

        public WidgetResult RenderAdmin(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var userService = providerManager.Provide<IUserService>();
            var selectedWidget = userService.GetLoginValidatorWidget(widgetInfoPackage.UniqeHash);
            var widgetViewModel = new LoginValidatorWidgetEditorViewModel
            {
                UniqeHash = widgetInfoPackage.UniqeHash,
                ActiveUrl = HttpContext.Current.Request.RawUrl
            };
            if (selectedWidget != null)
            {
                widgetViewModel.Name = selectedWidget.Name;
                widgetViewModel.LoginPageId = selectedWidget.LoginPageId;
            }
            var widgetResult = new WidgetResult
            {
                ViewName = EditorView,
                Model =
                    new OxideModel
                    {
                        RequestType = Syntax.Requests.Dynamic,
                        Header = widgetInfoPackage.Title,
                        Title = Title,
                        Parameters = widgetViewModel
                    }
            };
            return widgetResult;
        }

        public WidgetUpdatePackage RunUpdate(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager,
            ExpandoObject model)
        {
            var updateResult = new WidgetUpdatePackage();
            var userService = providerManager.Provide<IUserService>();
            var oxideServices = providerManager.Provide<IOxideServices>();
            var loginValidatorWidget = new LoginValidatorWidget {Key = widgetInfoPackage.UniqeHash};
            if (((dynamic) model).LoginPage != string.Empty)
                loginValidatorWidget.LoginPageId = Convert.ToInt32(((dynamic) model).LoginPage);
            var validatorWidget = userService.SaveLoginValidatorWidget(loginValidatorWidget);
            if (validatorWidget)
                updateResult.SuccessMessage = oxideServices.GetText("Login Validator widget saved successfully");
            else
                updateResult.ErrorMessage =
                    oxideServices.GetText(
                        "Problem while saving Login Validator Widget, operation is not succesfull, please check the logs");
            return updateResult;
        }

        public void OnDeleted(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var userService = providerManager.Provide<IUserService>();
            userService.DeleteLoginValidatorWidget(widgetInfoPackage.UniqeHash);
        }

        #endregion
    }
}