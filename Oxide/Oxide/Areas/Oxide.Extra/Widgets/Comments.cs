﻿using System.Dynamic;
using Oxide.Core.Elements;
using Oxide.Core.Extesions;
using Oxide.Core.Managers.CacheManager;
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.Models.Base;
using Oxide.Core.ResultModels;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Syntax;
using Oxide.Extra.Services.Interface;
using Oxide.Extra.ViewModels.Comments;

namespace Oxide.Extra.Widgets
{
    public class Comments : IOxideWidget
    {
        #region WidgetBasic

        public int Id => 1;

        public string Name => "Comments";

        public string Title => "Comments";

        public string Description => "Widget which you can use to give chance the user to make comments on any page.";

        public string Category => "Content";

        public string PreviewImageUrl => "/areas/OxideFundamentals/content/images/widgets/Carousel.png";

        public bool Active { get; set; }

        public string ModuleName => "Oxide.Extra";

        public string FrontView { get; set; } = "Comments";

        public string EditorView { get; set; } = "Comments_Editor";

        public string WidgetKey { get; set; }

        public string Dependencies { get; set; }

        #endregion

        #region Renders

        WidgetResult IOxideWidget.RenderFront(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var commentsService = providerManager.Provide<ICommentsService>();
            var cacheManager = providerManager.Provide<IOxideCacheManager>();
            var oxideModel = new OxideModel {RequestType = Syntax.Requests.Dynamic, Header = Name, Title = Title};
            var widgetSettings = cacheManager.GetOrSet(widgetInfoPackage.UniqeHash.ToString(), widgetInfoPackage.Title,
                "Comments Widget", () => commentsService.GetWidget(widgetInfoPackage.UniqeHash));
            oxideModel.Parameters = widgetSettings;
            var widgetResult = new WidgetResult {ViewName = FrontView, Model = oxideModel};
            return widgetResult;
        }

        public WidgetResult RenderAdmin(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var commentsService = providerManager.Provide<ICommentsService>();
            var widgetSettings = commentsService.GetWidget(widgetInfoPackage.UniqeHash);
            widgetSettings.Key = widgetInfoPackage.UniqeHash;
            widgetSettings.IsLoginRequired = widgetSettings.LoginRequired ? "checked" : "";
            widgetSettings.IsApproveRequired = widgetSettings.ApproveRequired ? "checked" : "";
            var widgetResult = new WidgetResult
            {
                ViewName = EditorView,
                Model =
                    new OxideModel
                    {
                        RequestType = Syntax.Requests.Dynamic,
                        Header = widgetInfoPackage.Title,
                        Title = Title,
                        Parameters = widgetSettings
                    }
            };
            return widgetResult;
        }

        public WidgetUpdatePackage RunUpdate(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager,
            ExpandoObject model)
        {
            var updateResult = new WidgetUpdatePackage();
            var commentsService = providerManager.Provide<ICommentsService>();
            var oxideServices = providerManager.Provide<IOxideServices>();
            var commentsEditorViewModel = new CommentsEditorViewModel();
            Mapper<CommentsEditorViewModel>.Map(model, commentsEditorViewModel);
            commentsEditorViewModel.Key = widgetInfoPackage.UniqeHash;
            updateResult.Success = commentsService.SaveWidget(commentsEditorViewModel);
            if (updateResult.Success)
                updateResult.SuccessMessage = oxideServices.GetText("Comments widget saved successfully");
            else
                updateResult.ErrorMessage =
                    oxideServices.GetText(
                        "Problem while saving comments widget, operation is not succesfull, please check the logs");
            return updateResult;
        }

        public void OnDeleted(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var commentsService = providerManager.Provide<ICommentsService>();
            commentsService.DeleteWidget(widgetInfoPackage.UniqeHash);
        }

        #endregion
    }
}