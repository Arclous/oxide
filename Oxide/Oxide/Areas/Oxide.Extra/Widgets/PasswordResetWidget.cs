﻿using System;
using System.Dynamic;
using System.Web;
using Oxide.Core.Elements;
using Oxide.Core.Helpers;
using Oxide.Core.Managers.CacheManager;
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.Models.Base;
using Oxide.Core.ResultModels;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Syntax;
using Oxide.Extra.Models.User;
using Oxide.Extra.Services.Interface;
using Oxide.Extra.ViewModels.User;

namespace Oxide.Extra.Widgets
{
    public class PasswordResetWidget : IOxideWidget
    {
        #region Helpers

        protected PasswordRecoveryWidgetViewModel GetViewModel(WidgetInfoPackage widgetInfoPackage,
            IProviderManager providerManager)
        {
            var userService = providerManager.Provide<IUserService>();
            var widget = userService.GetPasswordRecoveryWidget(widgetInfoPackage.UniqeHash);
            var userRegistirationWidgetViewModel = new PasswordRecoveryWidgetViewModel();
            if (widget != null)
            {
                userRegistirationWidgetViewModel.Name = widget.Name;
                userRegistirationWidgetViewModel.Title = widget.Title;
                userRegistirationWidgetViewModel.EmailId = widget.EmailId;
                userRegistirationWidgetViewModel.ValidMinutes = widget.ValidMinutes;
                userRegistirationWidgetViewModel.UniqeHash = widgetInfoPackage.UniqeHash;
            }
            return userRegistirationWidgetViewModel;
        }

        #endregion

        #region WidgetBasic

        public int Id => 1;

        public string Name => "Password Reset Widget";

        public string Title => "Password Reset Widget";

        public string Description => "Widget which can be used for reseting passwords.";

        public string Category => "User System";

        public string PreviewImageUrl => "/areas/Oxide.Extra/content/images/widgets/PasswordRecovery.png";

        public bool Active { get; set; }

        public string ModuleName => "Oxide.Extra";

        public string FrontView { get; set; } = "PasswordResetWidget";

        public string EditorView { get; set; } = "PasswordResetWidget_Editor";

        public string WidgetKey { get; set; }

        public string Dependencies { get; set; }

        #endregion

        #region Renders

        WidgetResult IOxideWidget.RenderFront(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var status = "0";
            if (HttpContext.Current.Request.QueryString["sts"] != "" &&
                HttpContext.Current.Request.QueryString["sts"] != null)
                status =
                    OxideUtils.GetDecryptedQueryString(
                        HttpUtility.UrlDecode(HttpContext.Current.Request.QueryString["sts"]));
            var selectedView = FrontView;
            var cacheManager = providerManager.Provide<IOxideCacheManager>();
            var loginService = providerManager.Provide<ILoginService>();
            var userRegistirationWidgetViewModel = cacheManager.GetOrSet(widgetInfoPackage.UniqeHash.ToString(),
                widgetInfoPackage.Title, "User Login System", () => GetViewModel(widgetInfoPackage, providerManager));

            //we dont need cache for those values
            userRegistirationWidgetViewModel.ActiveUrl = HttpContext.Current.Request.RawUrl;
            userRegistirationWidgetViewModel.IsLogined = loginService.IsUserLogined();
            userRegistirationWidgetViewModel.Status = Convert.ToInt32(status);
            var widgetResult = new WidgetResult {ViewName = selectedView, Model = userRegistirationWidgetViewModel};
            return widgetResult;
        }

        public WidgetResult RenderAdmin(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var userService = providerManager.Provide<IUserService>();
            var selectedWidget = userService.GetPasswordRecoveryWidget(widgetInfoPackage.UniqeHash);
            var widgetViewModel = new PasswordRecoveryWidgetAdminViewModel
            {
                UniqeHash = widgetInfoPackage.UniqeHash,
                ActiveUrl = HttpContext.Current.Request.RawUrl
            };
            if (selectedWidget != null)
            {
                widgetViewModel.Title = selectedWidget.Title;
                widgetViewModel.Name = selectedWidget.Name;
                widgetViewModel.EmailId = selectedWidget.EmailId;
                widgetViewModel.ValidMinutes = selectedWidget.ValidMinutes;
            }
            var widgetResult = new WidgetResult
            {
                ViewName = EditorView,
                Model =
                    new OxideModel
                    {
                        RequestType = Syntax.Requests.Dynamic,
                        Header = widgetInfoPackage.Title,
                        Title = Title,
                        Parameters = widgetViewModel
                    }
            };
            return widgetResult;
        }

        public WidgetUpdatePackage RunUpdate(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager,
            ExpandoObject model)
        {
            var updateResult = new WidgetUpdatePackage();
            var userService = providerManager.Provide<IUserService>();
            var oxideServices = providerManager.Provide<IOxideServices>();
            var userRegistirationWidget = new PasswordRecoveryWidget
            {
                Key = widgetInfoPackage.UniqeHash,
                ValidMinutes = Convert.ToInt32(((dynamic) model).ValidMinutes),
                Name = ((dynamic) model).Name,
                Title = ((dynamic) model).Title
            };
            if (((dynamic) model).RecoveryEmail != string.Empty)
                userRegistirationWidget.EmailId = Convert.ToInt32(((dynamic) model).RecoveryEmail);
            var registirationWidget = userService.SavePasswordRecoveryWidget(userRegistirationWidget);
            if (registirationWidget)
                updateResult.SuccessMessage = oxideServices.GetText("Password recovery widget saved successfully");
            else
                updateResult.ErrorMessage =
                    oxideServices.GetText(
                        "Problem while saving Password Recovery Widget, operation is not succesfull, please check the logs");
            return updateResult;
        }

        public void OnDeleted(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var userService = providerManager.Provide<IUserService>();
            userService.DeletePasswordRecoveryWidget(widgetInfoPackage.UniqeHash);
        }

        #endregion
    }
}