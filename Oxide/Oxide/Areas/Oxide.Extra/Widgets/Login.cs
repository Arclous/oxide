﻿using System;
using System.Dynamic;
using System.Web;
using Oxide.Core.Elements;
using Oxide.Core.Managers.CacheManager;
using Oxide.Core.Managers.CultureManager;
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.Models.Base;
using Oxide.Core.ResultModels;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Services.OxideUiService;
using Oxide.Core.Syntax;
using Oxide.Extra.Models.User;
using Oxide.Extra.Services.Interface;
using Oxide.Extra.ViewModels.User;

namespace Oxide.Extra.Widgets
{
    public class Login : IOxideWidget
    {
        #region Helpers

        protected LoginWidgetViewModel GetViewModel(WidgetInfoPackage widgetInfoPackage,
            IProviderManager providerManager)
        {
            var userService = providerManager.Provide<IUserService>();
            var oxideUiService = providerManager.Provide<IOxideUiService>();
            var cultureManager = providerManager.Provide<ICultureManager>();
            var widget = userService.GetLoginWidget(widgetInfoPackage.UniqeHash);
            var loginWidgetViewModel = new LoginWidgetViewModel();
            if (widget != null)
            {
                //Get specific widget settings
                var widgetImagePath = oxideUiService.GetImageContentUrl(widget.ImageId.ToString(),
                    Syntax.UrlType.Absolute);
                var resetPasswordPageUrl = oxideUiService.GetPageContentUrl(widget.PasswordResetPageId.ToString());

                //Get global settings
                var globalSetting =
                    userService.GetSettingByCulture(cultureManager.GetFrontCurrentCulture().ThreeLetterISOLanguageName);

                //Assing global settings if there is not specific setting for the widget
                if (globalSetting != null)
                {
                    if (resetPasswordPageUrl == null && globalSetting.PasswordResetPageId != 0)
                        resetPasswordPageUrl = oxideUiService.GetPageContentUrl(globalSetting.LoginPageId.ToString());
                }
                loginWidgetViewModel.Name = widget.Name;
                loginWidgetViewModel.ImageId = widget.ImageId;
                loginWidgetViewModel.ImageUrl = widgetImagePath;
                loginWidgetViewModel.ResetPasswordPageUrl = resetPasswordPageUrl;
                loginWidgetViewModel.ResetPasswordPageId = widget.PasswordResetPageId;
                loginWidgetViewModel.UniqeHash = widgetInfoPackage.UniqeHash;
            }
            return loginWidgetViewModel;
        }

        #endregion

        #region WidgetBasic

        public int Id => 1;

        public string Name => "Login";

        public string Title => "Login";

        public string Description => "Widget which can be used for logining.";

        public string Category => "User System";

        public string PreviewImageUrl => "/areas/Oxide.Extra/content/images/widgets/Login.png";

        public bool Active { get; set; }

        public string ModuleName => "Oxide.Extra";

        public string FrontView { get; set; } = "Login";

        public string EditorView { get; set; } = "Login_Editor";

        public string WidgetKey { get; set; }

        public string Dependencies { get; set; }

        #endregion

        #region Renders

        WidgetResult IOxideWidget.RenderFront(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var selectedView = FrontView;
            var cacheManager = providerManager.Provide<IOxideCacheManager>();
            var loginService = providerManager.Provide<ILoginService>();
            var loginWidgetViewModel = cacheManager.GetOrSet(widgetInfoPackage.UniqeHash.ToString(),
                widgetInfoPackage.Title, "User Login System", () => GetViewModel(widgetInfoPackage, providerManager));

            //We dont need cache for those values
            loginWidgetViewModel.IsLogined = loginService.IsUserLogined();
            loginWidgetViewModel.ActiveUrl = HttpContext.Current.Request.RawUrl;
            if (loginWidgetViewModel.IsLogined)
            {
                var userInfo = loginService.GetUserInfo();
                loginWidgetViewModel.UserName = userInfo.UserName;
                loginWidgetViewModel.UserImage = userInfo.UserImageUrl;
            }
            var widgetResult = new WidgetResult {ViewName = selectedView, Model = loginWidgetViewModel};
            return widgetResult;
        }

        public WidgetResult RenderAdmin(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var userService = providerManager.Provide<IUserService>();
            var selectedWidget = userService.GetLoginWidget(widgetInfoPackage.UniqeHash);
            var widgetViewModel = new LoginWidgetAdminViewModel
            {
                UniqeHash = widgetInfoPackage.UniqeHash,
                ActiveUrl = HttpContext.Current.Request.RawUrl
            };
            if (selectedWidget != null)
            {
                widgetViewModel.Name = selectedWidget.Name;
                widgetViewModel.ImageId = selectedWidget.ImageId;
                widgetViewModel.ResetPasswordPageId = selectedWidget.PasswordResetPageId;
            }
            var widgetResult = new WidgetResult
            {
                ViewName = EditorView,
                Model =
                    new OxideModel
                    {
                        RequestType = Syntax.Requests.Dynamic,
                        Header = widgetInfoPackage.Title,
                        Title = Title,
                        Parameters = widgetViewModel
                    }
            };
            return widgetResult;
        }

        public WidgetUpdatePackage RunUpdate(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager,
            ExpandoObject model)
        {
            var updateResult = new WidgetUpdatePackage();
            var userService = providerManager.Provide<IUserService>();
            var oxideServices = providerManager.Provide<IOxideServices>();
            var loginWidget = new LoginWidget {Key = widgetInfoPackage.UniqeHash};
            if (((dynamic) model).PasswordResetPage != string.Empty)
                loginWidget.PasswordResetPageId = Convert.ToInt32(((dynamic) model).PasswordResetPage);
            if (((dynamic) model).MediaSelecter != string.Empty)
                loginWidget.ImageId = Convert.ToInt32(((dynamic) model).MediaSelecter);
            var validatorWidget = userService.SaveLoginWidget(loginWidget);
            if (validatorWidget)
                updateResult.SuccessMessage = oxideServices.GetText("Login widget saved successfully");
            else
                updateResult.ErrorMessage =
                    oxideServices.GetText(
                        "Problem while saving Login Widget, operation is not succesfull, please check the logs");
            return updateResult;
        }

        public void OnDeleted(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var userService = providerManager.Provide<IUserService>();
            userService.DeleteSetting(widgetInfoPackage.UniqeHash);
        }

        #endregion
    }
}