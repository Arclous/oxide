﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Web;
using Oxide.Core.Elements;
using Oxide.Core.Helpers;
using Oxide.Core.Managers.CacheManager;
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.Models.Base;
using Oxide.Core.ResultModels;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Services.OxideUiService;
using Oxide.Core.Syntax;
using Oxide.Extra.Models.User;
using Oxide.Extra.Services.Interface;
using Oxide.Extra.ViewModels.User;

namespace Oxide.Extra.Widgets
{
    public class UserRegistirationActivator : IOxideWidget
    {
        #region WidgetBasic

        public int Id => 1;

        public string Name => "User Registiration Activator";

        public string Title => "User Registiration Activator";

        public string Description
            =>
                "Widget which can be use to activate user accounts, you need to add this widget to page which is selected as 'Confirmation Page' on the User Registiration widget."
            ;

        public string Category => "User System";

        public string PreviewImageUrl => "/areas/Oxide.Extra/content/images/widgets/UserRegistirationActivator.png";

        public bool Active { get; set; }

        public string ModuleName => "Oxide.Extra";

        public string FrontView { get; set; } = "UserRegistirationActivator";

        public string EditorView { get; set; } = "UserRegistirationActivator_Editor";

        public string WidgetKey { get; set; }

        public string Dependencies { get; set; }

        #endregion

        #region Renders

        WidgetResult IOxideWidget.RenderFront(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var activatorViewModel = new UserRegistirationActivatorViewModel();
            var conf = "";
            if (HttpContext.Current.Request.QueryString["conf"] != "" &&
                HttpContext.Current.Request.QueryString["conf"] != null)
            {
                var urlDecode = HttpUtility.UrlDecode(HttpContext.Current.Request.QueryString["conf"]);
                if (urlDecode != null)
                    conf = urlDecode.Replace(" ", "+");
            }
            else
            {
                //Status 0 not valid data
                activatorViewModel.Status = 0;
                return new WidgetResult {ViewName = FrontView, Model = activatorViewModel};
            }
            var userService = providerManager.Provide<IUserService>();
            var cacheManager = providerManager.Provide<IOxideCacheManager>();
            var oxideUiService = providerManager.Provide<IOxideUiService>();
            var confirmation = userService.GetConfirmation(conf);
            if (confirmation == null)
            {
                //Status 0 not valid data
                activatorViewModel.Status = 0;
                return new WidgetResult {ViewName = FrontView, Model = activatorViewModel};
            }
            var user = userService.GetUser(confirmation.UserId);
            if (DateTime.Now > confirmation.ConfirmationSent.AddDays(confirmation.ValidDays))
            {
                //Status 1 expired link
                activatorViewModel.Status = 1;
                userService.DeleteUser(user.Id);
                userService.DeleteConfirmation(confirmation.Id);
                return new WidgetResult {ViewName = FrontView, Model = activatorViewModel};
            }

            //if the link is valid and not expired activate the user
            userService.ActivateUser(user.Id);
            userService.DeleteConfirmation(confirmation.Id);
            var selectedWidget = cacheManager.GetOrSet(widgetInfoPackage.UniqeHash.ToString(), widgetInfoPackage.Title,
                "User Login System", () => userService.GetUserRegistirationActivatorWidget(widgetInfoPackage.UniqeHash));
            if (selectedWidget != null)
            {
                var widgetImagePath = oxideUiService.GetImageContentUrl(selectedWidget.ImageId.ToString(),
                    Syntax.UrlType.Absolute);

                //Status 2 ok status
                activatorViewModel.Status = 2;
                activatorViewModel.Email = user.Email;
                activatorViewModel.ActiveUrl = HttpContext.Current.Request.RawUrl;
                activatorViewModel.UniqeHash = confirmation.WidgetHash;
                activatorViewModel.Key = HttpContext.Current.Request.QueryString["reg"];
                activatorViewModel.ImageUrl = widgetImagePath;
                activatorViewModel.Name = selectedWidget.Name;
                var keywords = new Dictionary<string, string> {{"[UserEmail]", $@"<strong>{user.Email}</strong>"}};
                activatorViewModel.Message = OxideUtils.FillParameters(selectedWidget.Message, keywords);
            }
            var widgetResult = new WidgetResult {ViewName = FrontView, Model = activatorViewModel};
            return widgetResult;
        }

        public WidgetResult RenderAdmin(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var userService = providerManager.Provide<IUserService>();
            var selectedWidget = userService.GetUserRegistirationActivatorWidget(widgetInfoPackage.UniqeHash);
            var widgetViewModel = new UserRegistirationActivatorWidgetAdminViewModel
            {
                UniqeHash = widgetInfoPackage.UniqeHash,
                ActiveUrl = HttpContext.Current.Request.RawUrl
            };
            if (selectedWidget != null)
            {
                widgetViewModel.Name = selectedWidget.Name;
                widgetViewModel.ImageId = selectedWidget.ImageId;
                widgetViewModel.Message = selectedWidget.Message;
            }
            var widgetResult = new WidgetResult
            {
                ViewName = EditorView,
                Model =
                    new OxideModel
                    {
                        RequestType = Syntax.Requests.Dynamic,
                        Header = widgetInfoPackage.Title,
                        Title = Title,
                        Parameters = widgetViewModel
                    }
            };
            return widgetResult;
        }

        public WidgetUpdatePackage RunUpdate(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager,
            ExpandoObject model)
        {
            var updateResult = new WidgetUpdatePackage();
            var userService = providerManager.Provide<IUserService>();
            var oxideServices = providerManager.Provide<IOxideServices>();
            var registirationActivatorWidget = new UserRegistirationActivatorWidget
            {
                Name = ((dynamic) model).Title,
                Title = ((dynamic) model).Title,
                Key = widgetInfoPackage.UniqeHash,
                Message = ((dynamic) model).Message
            };
            if (((dynamic) model).MediaSelecter != string.Empty)
                registirationActivatorWidget.ImageId = Convert.ToInt32(((dynamic) model).MediaSelecter);
            var userRegistirationActivatorWidget =
                userService.SaveUserRegistirationActivatorWidget(registirationActivatorWidget);
            if (userRegistirationActivatorWidget)
                updateResult.SuccessMessage =
                    oxideServices.GetText("User Registiration Activator widget saved successfully");
            else
                updateResult.ErrorMessage =
                    oxideServices.GetText(
                        "Problem while saving User Registiration Activator Widget, operation is not succesfull, please check the logs");
            return updateResult;
        }

        public void OnDeleted(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var userService = providerManager.Provide<IUserService>();
            userService.DeleteUserRegistirationActivatorWidget(widgetInfoPackage.UniqeHash);
        }

        #endregion
    }
}