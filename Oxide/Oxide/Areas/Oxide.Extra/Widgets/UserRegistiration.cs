﻿using System;
using System.Dynamic;
using System.Web;
using Oxide.Core.Elements;
using Oxide.Core.Extesions;
using Oxide.Core.Managers.CacheManager;
using Oxide.Core.Managers.CultureManager;
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.Models.Base;
using Oxide.Core.ResultModels;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Services.OxideUiService;
using Oxide.Core.Syntax;
using Oxide.Extra.Models.User;
using Oxide.Extra.Services.Interface;
using Oxide.Extra.ViewModels.User;

namespace Oxide.Extra.Widgets
{
    public class UserRegistiration : IOxideWidget
    {
        #region Helpers

        protected UserRegistirationWidgetViewModel GetViewModel(WidgetInfoPackage widgetInfoPackage,
            IProviderManager providerManager)
        {
            var userRegistirationWidgetViewModel = new UserRegistirationWidgetViewModel();
            var userService = providerManager.Provide<IUserService>();
            var oxideUiService = providerManager.Provide<IOxideUiService>();
            var cultureManager = providerManager.Provide<ICultureManager>();
            var selectedWidget = userService.GetUserRegistirationWidget(widgetInfoPackage.UniqeHash);
            if (selectedWidget != null)
            {
                //Get specific widget settings
                var widgetImagePath = oxideUiService.GetImageContentUrl(selectedWidget.ImageId.ToString(),
                    Syntax.UrlType.Absolute);
                var widgetPageUrl = oxideUiService.GetPageContentUrl(selectedWidget.ConfirmationPageId.ToString());
                var loginPageUrl = oxideUiService.GetPageContentUrl(selectedWidget.LoginPageId.ToString());

                //Get global settings
                var globalSetting =
                    userService.GetSettingByCulture(cultureManager.GetFrontCurrentCulture().ThreeLetterISOLanguageName);

                //Assing global settings if there is not specific setting for the widget
                if (globalSetting != null)
                {
                    if (widgetPageUrl == null && globalSetting.ConfirmationPageId != 0)
                        widgetPageUrl = oxideUiService.GetPageContentUrl(globalSetting.ConfirmationPageId.ToString());
                    if (loginPageUrl == null && globalSetting.LoginPageId != 0)
                        loginPageUrl = oxideUiService.GetPageContentUrl(globalSetting.LoginPageId.ToString());
                }
                userRegistirationWidgetViewModel.License = selectedWidget.License;
                userRegistirationWidgetViewModel.ShowLicense = selectedWidget.ShowLicense;
                userRegistirationWidgetViewModel.Name = selectedWidget.Name;
                userRegistirationWidgetViewModel.ImageId = selectedWidget.ImageId;
                userRegistirationWidgetViewModel.ImageUrl = widgetImagePath;
                userRegistirationWidgetViewModel.ConfirmationPageId = selectedWidget.ConfirmationPageId;
                userRegistirationWidgetViewModel.ConfirmationPageUrl = widgetPageUrl;
                userRegistirationWidgetViewModel.ConfirmationEmailId = selectedWidget.ConfirmationEmailId;
                userRegistirationWidgetViewModel.SendConfirmationEmail = selectedWidget.SendConfirmationEmail;
                userRegistirationWidgetViewModel.ConfirmationEmailSentPageId =
                    selectedWidget.ConfirmationEmailSentPageId;
                userRegistirationWidgetViewModel.LoginPageUrl = loginPageUrl;
                userRegistirationWidgetViewModel.LoginPageId = selectedWidget.LoginPageId;
                userRegistirationWidgetViewModel.UniqeHash = widgetInfoPackage.UniqeHash;
                userRegistirationWidgetViewModel.ValidDays = selectedWidget.ValidDays;
            }
            return userRegistirationWidgetViewModel;
        }

        #endregion

        #region WidgetBasic

        public int Id => 1;

        public string Name => "User Registiration";

        public string Title => "User Registiration";

        public string Description => "Widget which can be used for user registiration.";

        public string Category => "User System";

        public string PreviewImageUrl => "/areas/Oxide.Extra/content/images/widgets/UserRegistiration.png";

        public bool Active { get; set; }

        public string ModuleName => "Oxide.Extra";

        public string FrontView { get; set; } = "UserRegistiration";

        public string EditorView { get; set; } = "UserRegistiration_Editor";

        public string WidgetKey { get; set; }

        public string Dependencies { get; set; }

        #endregion

        #region Renders

        WidgetResult IOxideWidget.RenderFront(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var selectedView = FrontView;
            var cacheManager = providerManager.Provide<IOxideCacheManager>();
            var loginService = providerManager.Provide<ILoginService>();
            var userRegistirationWidgetViewModel = cacheManager.GetOrSet(widgetInfoPackage.UniqeHash.ToString(),
                widgetInfoPackage.Title, "User Login System", () => GetViewModel(widgetInfoPackage, providerManager));

            //We dont need cache for those values
            userRegistirationWidgetViewModel.ActiveUrl = HttpContext.Current.Request.RawUrl;
            userRegistirationWidgetViewModel.IsLogined = loginService.IsUserLogined();
            var widgetResult = new WidgetResult {ViewName = selectedView, Model = userRegistirationWidgetViewModel};
            return widgetResult;
        }

        public WidgetResult RenderAdmin(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var userService = providerManager.Provide<IUserService>();
            var selectedWidget = userService.GetUserRegistirationWidget(widgetInfoPackage.UniqeHash);
            var widgetViewModel = new UserRegistirationWidgetAdminViewModel
            {
                UniqeHash = widgetInfoPackage.UniqeHash,
                ActiveUrl = HttpContext.Current.Request.RawUrl
            };
            if (selectedWidget != null)
            {
                widgetViewModel.License = selectedWidget.License;
                widgetViewModel.Name = selectedWidget.Name;
                widgetViewModel.ShowLicense = selectedWidget.ShowLicense;
                widgetViewModel.SendConfirmationEmail = selectedWidget.SendConfirmationEmail;
                widgetViewModel.ImageId = selectedWidget.ImageId;
                widgetViewModel.ConfirmationPageId = selectedWidget.ConfirmationPageId;
                widgetViewModel.ConfirmationEmailId = selectedWidget.ConfirmationEmailId;
                widgetViewModel.ConfirmationEmailSentPageId = selectedWidget.ConfirmationEmailSentPageId;
                widgetViewModel.IsShowLicense = selectedWidget.ShowLicense ? "checked" : "";
                widgetViewModel.IsSendConfirmation = selectedWidget.SendConfirmationEmail ? "checked" : "";
                widgetViewModel.LoginPageId = selectedWidget.LoginPageId;
                widgetViewModel.ValidDays = selectedWidget.ValidDays;
            }
            var widgetResult = new WidgetResult
            {
                ViewName = EditorView,
                Model =
                    new OxideModel
                    {
                        RequestType = Syntax.Requests.Dynamic,
                        Header = widgetInfoPackage.Title,
                        Title = Title,
                        Parameters = widgetViewModel
                    }
            };
            return widgetResult;
        }

        public WidgetUpdatePackage RunUpdate(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager,
            ExpandoObject model)
        {
            var updateResult = new WidgetUpdatePackage();
            var userService = providerManager.Provide<IUserService>();
            var oxideServices = providerManager.Provide<IOxideServices>();
            ;
            var showLicense = model.HasProperty("ShowLicense");
            var sendConfirmationEmail = model.HasProperty("SendConfirmationEmail");
            var userRegistirationWidget = new UserRegistirationWidget
            {
                Name = ((dynamic) model).Title,
                Title = ((dynamic) model).Title,
                License = ((dynamic) model).License,
                ShowLicense = showLicense,
                SendConfirmationEmail = sendConfirmationEmail,
                Key = widgetInfoPackage.UniqeHash,
                ValidDays = Convert.ToInt32(((dynamic) model).ValidDays.ToString())
            };
            if (((dynamic) model).MediaSelecter != string.Empty)
                userRegistirationWidget.ImageId = Convert.ToInt32(((dynamic) model).MediaSelecter);
            if (((dynamic) model).ConfirmationPage != string.Empty)
                userRegistirationWidget.ConfirmationPageId = Convert.ToInt32(((dynamic) model).ConfirmationPage);
            if (((dynamic) model).ConfirmationEmail != string.Empty)
                userRegistirationWidget.ConfirmationEmailId = Convert.ToInt32(((dynamic) model).ConfirmationEmail);
            if (((dynamic) model).LoginPage != string.Empty)
                userRegistirationWidget.LoginPageId = Convert.ToInt32(((dynamic) model).LoginPage);
            if (((dynamic) model).ConfirmationSentPage != string.Empty)
                userRegistirationWidget.ConfirmationEmailSentPageId =
                    Convert.ToInt32(((dynamic) model).ConfirmationSentPage);
            var registirationWidget = userService.SaveUserRegistirationWidget(userRegistirationWidget);
            if (registirationWidget)
                updateResult.SuccessMessage = oxideServices.GetText("User Registiration widget saved successfully");
            else
                updateResult.ErrorMessage =
                    oxideServices.GetText(
                        "Problem while saving User Registiration Widget, operation is not succesfull, please check the logs");
            return updateResult;
        }

        public void OnDeleted(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var userService = providerManager.Provide<IUserService>();
            userService.DeleteUserRegistirationWidget(widgetInfoPackage.UniqeHash);
        }

        #endregion
    }
}