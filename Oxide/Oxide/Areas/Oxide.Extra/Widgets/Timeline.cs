﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using Oxide.Core.Elements;
using Oxide.Core.Managers.CacheManager;
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.Models.Base;
using Oxide.Core.ResultModels;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Services.OxideUiService;
using Oxide.Core.Syntax;
using Oxide.Extra.Models.Timeline;
using Oxide.Extra.Services.Interface;
using Oxide.Extra.ViewModels.Timeline;

namespace Oxide.Extra.Widgets
{
    public class Timeline : IOxideWidget
    {
        #region WidgetBasic

        public int Id => 1;

        public string Name => "Timeline";

        public string Title => "Timeline";

        public string Description => "Widget which can display your teams with details.";

        public string Category => "Extra";

        public string PreviewImageUrl => "/areas/Oxide.Extra/content/images/widgets/Timeline.png";

        public bool Active { get; set; }

        public string ModuleName => "Oxide.Extra";

        public string FrontView { get; set; } = "Timeline";

        public string EditorView { get; set; } = "Timeline_Editor";

        public string WidgetKey { get; set; }

        public string Dependencies { get; set; }

        #endregion

        #region Renders

        WidgetResult IOxideWidget.RenderFront(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var timelineWidgetService = providerManager.Provide<ITimelineWidgetService>();
            var cacheManager = providerManager.Provide<IOxideCacheManager>();
            var oxideUiService = providerManager.Provide<IOxideUiService>();
            var selectedWidget = cacheManager.GetOrSet(widgetInfoPackage.UniqeHash.ToString(), widgetInfoPackage.Title,
                "Timeline Widget", () => timelineWidgetService.GetWidget(widgetInfoPackage.UniqeHash));
            var timelineWidgetFrontViewModel = new TimelineWidgetFrontViewModel
            {
                TeamMemberItems = new List<TimelineItemFrontViewModel>()
            };
            if (selectedWidget != null)
            {
                timelineWidgetFrontViewModel.Name = selectedWidget.Name;
                timelineWidgetFrontViewModel.Title = selectedWidget.Title;
                if (selectedWidget.TimelineItems.Count > 0)
                {
                    foreach (
                        var memberId in selectedWidget.TimelineItems.OrderBy(x => x.Id).Select(x => x.TimelineItemId))
                    {
                        var timelineItem = timelineWidgetService.GetTimelineItem(memberId);
                        var memberImage = oxideUiService.GetContent(Constants.FileContent,
                            timelineItem.ImageId.ToString());
                        var memberImagePath = ((dynamic) memberImage).Path;
                        var timelineItemFrontViewModel = new TimelineItemFrontViewModel
                        {
                            Description = timelineItem.Description,
                            Name = timelineItem.Name,
                            Title = timelineItem.Title,
                            ImagePath = string.Format(@"{0}", memberImagePath)
                        };
                        timelineWidgetFrontViewModel.TeamMemberItems.Add(timelineItemFrontViewModel);
                    }
                }
            }
            var widgetResult = new WidgetResult {ViewName = FrontView, Model = timelineWidgetFrontViewModel};
            return widgetResult;
        }

        public WidgetResult RenderAdmin(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var timelineWidgetService = providerManager.Provide<ITimelineWidgetService>();
            var selectedWidget = timelineWidgetService.GetWidget(widgetInfoPackage.UniqeHash);
            var timelineWidgetViewModel = new TimelineWidgetViewModel();
            var selectedMembers = selectedWidget?.TimelineItems;
            if (selectedMembers != null)
                timelineWidgetViewModel.TimelineItems = string.Join(",",
                    selectedMembers.OrderBy(x => x.Id).Select(x => x.TimelineItemId));
            timelineWidgetViewModel.UniqeHash = widgetInfoPackage.UniqeHash;
            timelineWidgetViewModel.ActiveUrl = HttpContext.Current.Request.RawUrl;
            if (selectedWidget != null)
            {
                timelineWidgetViewModel.Name = selectedWidget.Name;
                timelineWidgetViewModel.Title = selectedWidget.Title;
            }
            var widgetResult = new WidgetResult
            {
                ViewName = EditorView,
                Model =
                    new OxideModel
                    {
                        RequestType = Syntax.Requests.Dynamic,
                        Header = widgetInfoPackage.Title,
                        Title = Title,
                        Parameters = timelineWidgetViewModel
                    }
            };
            return widgetResult;
        }

        public WidgetUpdatePackage RunUpdate(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager,
            ExpandoObject model)
        {
            var updateResult = new WidgetUpdatePackage();
            var timelineWidgetService = providerManager.Provide<ITimelineWidgetService>();
            var oxideServices = providerManager.Provide<IOxideServices>();
            var teamMembers = ((string) ((dynamic) model).MemberSelecter).Split(Constants.Splitter).ToList();
            var teamWidget = new TimelineWidget
            {
                TimelineItems = new List<TimelineWidgetItem>(),
                Key = widgetInfoPackage.UniqeHash,
                Name = ((dynamic) model).Name,
                Title = ((dynamic) model).Title
            };
            if (((dynamic) model).MemberSelecter != "")
            {
                foreach (
                    var member in teamMembers.Select(v => timelineWidgetService.GetTimelineItem(Convert.ToInt32(v))))
                {
                    var teamWidgetItem = new TimelineWidgetItem
                    {
                        TimelineWidgetId = teamWidget.Id,
                        TimelineItemId = member.Id
                    };
                    teamWidget.TimelineItems.Add(teamWidgetItem);
                }
            }
            var saveWidget = timelineWidgetService.SaveWidget(teamWidget);
            if (saveWidget)
                updateResult.SuccessMessage = oxideServices.GetText("Teamline widget saved successfully");
            else
                updateResult.ErrorMessage =
                    oxideServices.GetText(
                        "Problem while saving timeline widget, operation is not succesfull, please check the logs");
            return updateResult;
        }

        public void OnDeleted(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var timelineWidgetService = providerManager.Provide<ITimelineWidgetService>();
            timelineWidgetService.DeleteWidget(widgetInfoPackage.UniqeHash);
        }

        #endregion
    }
}