﻿using System;
using System.Dynamic;
using System.Web;
using Oxide.Core.Elements;
using Oxide.Core.Helpers;
using Oxide.Core.Managers.CultureManager;
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.Models.Base;
using Oxide.Core.ResultModels;
using Oxide.Core.Services.OxideUiService;
using Oxide.Core.Syntax;
using Oxide.Extra.Services.Interface;
using Oxide.Extra.ViewModels.User;

namespace Oxide.Extra.Widgets
{
    public class UserProfile : IOxideWidget
    {
        #region Helpers

        protected UserProfileViewModel GetViewModel(WidgetInfoPackage widgetInfoPackage,
            IProviderManager providerManager, int sts)
        {
            var userProfileViewModel = new UserProfileViewModel();
            var userService = providerManager.Provide<IUserService>();
            var oxideUiService = providerManager.Provide<IOxideUiService>();
            var loginService = providerManager.Provide<ILoginService>();
            var cultureManager = providerManager.Provide<ICultureManager>();
            var loginPageUrl = "";

            //Get global settings
            var globalSetting =
                userService.GetSettingByCulture(cultureManager.GetFrontCurrentCulture().ThreeLetterISOLanguageName);
            if (globalSetting != null)
            {
                if (globalSetting.LoginPageId != 0)
                    loginPageUrl = oxideUiService.GetPageContentUrl(globalSetting.LoginPageId.ToString());
            }
            if (!loginService.IsUserLogined())
            {
                HttpContext.Current.Response.Redirect(loginPageUrl ?? "/", true);
                userProfileViewModel.ActiveUrl = HttpContext.Current.Request.RawUrl;
                return userProfileViewModel;
            }
            var userInfo = loginService.GetUserInfo();
            var user = userService.GetUser(userInfo.Id);
            userProfileViewModel.Status = sts;
            userProfileViewModel.ActiveUrl = HttpContext.Current.Request.RawUrl;
            userProfileViewModel.Name = user.Name;
            userProfileViewModel.Title = user.Title;
            userProfileViewModel.UserName = user.UserName;
            userProfileViewModel.Email = user.Email;
            userProfileViewModel.UserImage = oxideUiService.GetImageContentUrl(user.UsrImageId.ToString(),
                Syntax.UrlType.Absolute);
            return userProfileViewModel;
        }

        #endregion

        #region WidgetBasic

        public int Id => 1;

        public string Name => "User Profile";

        public string Title => "User Profile";

        public string Description => "Widget which can be used as profile for users.";

        public string Category => "User System";

        public string PreviewImageUrl => "/areas/Oxide.Extra/content/images/widgets/Login.png";

        public bool Active { get; set; }

        public string ModuleName => "Oxide.Extra";

        public string FrontView { get; set; } = "UserProfile";

        public string EditorView { get; set; } = "UserProfile_Editor";

        public WidgetUpdatePackage RunUpdate(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager,
            ExpandoObject model)
        {
            return new WidgetUpdatePackage();
        }

        public void OnDeleted(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
        }

        public string WidgetKey { get; set; }

        public string Dependencies { get; set; }

        #endregion

        #region Renders

        WidgetResult IOxideWidget.RenderFront(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var userProfileViewModel = new UserProfileViewModel {Status = 7};
            if (HttpContext.Current.Request.QueryString["sts"] != "" &&
                HttpContext.Current.Request.QueryString["sts"] != null)
            {
                var urlDecode = HttpUtility.UrlDecode(HttpContext.Current.Request.QueryString["sts"]);
                if (urlDecode != null)
                {
                    var sts = urlDecode.Replace(" ", "+");
                    userProfileViewModel.Status = Convert.ToInt32(OxideUtils.GetDecryptedQueryString(sts));
                }
            }
            userProfileViewModel = GetViewModel(widgetInfoPackage, providerManager, userProfileViewModel.Status);
            var widgetResult = new WidgetResult {ViewName = FrontView, Model = userProfileViewModel};
            return widgetResult;
        }

        public WidgetResult RenderAdmin(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var widgetResult = new WidgetResult
            {
                ViewName = EditorView,
                Model =
                    new OxideModel
                    {
                        RequestType = Syntax.Requests.Dynamic,
                        Header = widgetInfoPackage.Title,
                        Title = Title,
                        Parameters = null
                    }
            };
            return widgetResult;
        }

        #endregion
    }
}