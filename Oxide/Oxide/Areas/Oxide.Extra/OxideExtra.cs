﻿using Oxide.Core.Elements;

namespace Oxide.Extra
{
    public class OxideExtra : IOxideModule
    {
        public int Id { get; } = 2;
        public string Name { get; } = "Oxide.Extra";
        public string Title { get; } = "Oxide Extra";
        public string Description { get; } = "Extra widgets and admin pages for the cms";
        public string Author { get; } = "Inevera Studios";
        public string Category { get; } = "Extra";
        public string PreviewImageUrl { get; } = "/Areas/Oxide.Extra/Content/images/oxide.png";
        public bool Active { get; set; }
        public string Dependencies { get; set; }
    }
}