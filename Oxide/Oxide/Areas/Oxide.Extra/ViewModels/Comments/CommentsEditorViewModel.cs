﻿using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Extra.ViewModels.Comments
{
    public class CommentsEditorViewModel : OxideEditorViewModel
    {
        public bool LoginRequired { get; set; }
        public bool ApproveRequired { get; set; }
        public string IsLoginRequired { get; set; }
        public string IsApproveRequired { get; set; }
        public int MaxCommentsToDisplay { get; set; }
        public string PleaseLoginMessage { get; set; }
        public string FirstCommentMessage { get; set; }
        public string AfterCommentMessage { get; set; }
    }
}