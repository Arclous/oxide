﻿using System;
using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Extra.ViewModels.Comments
{
    public class CommentsViewModel : OxideViewModel
    {
        public string Message { get; set; }
        public bool Deletable { get; set; }
        public DateTime CommentDateTime { get; set; }
        public string UserName { get; set; }
        public string UserImage { get; set; }
        public string Key { get; set; }
    }
}