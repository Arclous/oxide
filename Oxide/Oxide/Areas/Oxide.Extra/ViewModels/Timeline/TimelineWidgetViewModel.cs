﻿using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Extra.ViewModels.Timeline
{
    public class TimelineWidgetViewModel : OxideWidgetViewModel
    {
        public string Key { get; set; }
        public string TimelineItems { get; set; }
        public string ActiveUrl { get; set; }
        public string Title { get; set; }
    }
}