﻿using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Extra.ViewModels.Timeline
{
    public class TimelineItemFrontViewModel : OxideViewModel
    {
        public string Description { get; set; }
        public string ImagePath { get; set; }
    }
}