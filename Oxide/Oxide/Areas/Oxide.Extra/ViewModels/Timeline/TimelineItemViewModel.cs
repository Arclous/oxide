﻿using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Extra.ViewModels.Timeline
{
    public class TimelineItemViewModel : OxideWidgetViewModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public int ImageId { get; set; }
        public string ImagePath { get; set; }
        public string ActiveUrl { get; set; }
    }
}