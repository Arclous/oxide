﻿using System.Collections.Generic;
using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Extra.ViewModels.Timeline
{
    public class TimelineWidgetFrontViewModel : OxideViewModel
    {
        public virtual ICollection<TimelineItemFrontViewModel> TeamMemberItems { get; set; }
    }
}