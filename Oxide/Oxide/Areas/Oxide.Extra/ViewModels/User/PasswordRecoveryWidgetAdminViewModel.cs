﻿using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Extra.ViewModels.User
{
    public class PasswordRecoveryWidgetAdminViewModel : OxideWidgetViewModel
    {
        public int Key { get; set; }
        public string ActiveUrl { get; set; }
        public int EmailId { get; set; }
        public int ValidMinutes { get; set; }
        public string Title { get; set; }
    }
}