﻿using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Extra.ViewModels.User
{
    public class LoginValidatorWidgetViewModel : OxideViewModel
    {
        public int UniqeHash { get; set; }
        public int LoginPageId { get; set; }
        public string LoginPageUrl { get; set; }
        public string ActiveUrl { get; set; }
    }
}