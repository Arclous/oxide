﻿using System;
using Oxide.Core.Base.OxideViewModel;
using Oxide.Extra.Keywords;

namespace Oxide.Extra.ViewModels.User
{
    public class UserViewModel : OxideViewModel
    {
        public string Surname { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsEmailConfirmed { get; set; }
        public DateTime CreateDateTime { get; set; }
        public DateTime LastAccessDateTime { get; set; }
        public Keys.UserStatus Status { get; set; }
        public string StatusText { get; set; }
        public string ConfirmPassword { get; set; }
        public int UserImageId { get; set; }
        public string UserImageUrl { get; set; }
        public bool Activated { get; set; }
    }
}