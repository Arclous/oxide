﻿using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Extra.ViewModels.User
{
    public class LoginSettingViewModel : OxideViewModel
    {
        public int ConfirmationPageId { get; set; }
        public int ConfirmationSentPageId { get; set; }
        public int LoginPageId { get; set; }
        public int PasswordResetPageId { get; set; }
        public int UserSettingPageId { get; set; }
        public int ConfirmationEmailId { get; set; }
        public int RecoveryEmailId { get; set; }
        public int CultureId { get; set; }
        public string ConfirmationPageUrl { get; set; }
        public string ConfirmationSentPageUrl { get; set; }
        public string LoginPageUrl { get; set; }
        public string PasswordResetPageUrl { get; set; }
        public int UserSettingPageUrl { get; set; }
        public string ConfirmationEmailTemplate { get; set; }
        public string RecoveryEmailTemplate { get; set; }
    }
}