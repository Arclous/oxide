﻿using Oxide.Core.ViewModels.FilterViewModels;

namespace Oxide.Extra.ViewModels.User
{
    public class UserFilterViewModel : FilterModel<UserViewModel>
    {
        public string ChangeStatusUrl { get; set; }
    }
}