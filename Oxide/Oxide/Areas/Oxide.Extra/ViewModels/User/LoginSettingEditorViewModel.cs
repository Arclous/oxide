﻿using System.Collections;
using Oxide.Areas.Admin.Models;
using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Extra.ViewModels.User
{
    public class LoginSettingEditorViewModel : OxideViewModel
    {
        public int ConfirmationPageId { get; set; }
        public int ConfirmationSentPageId { get; set; }
        public int LoginPageId { get; set; }
        public int PasswordResetPageId { get; set; }
        public int UserSettingPageId { get; set; }
        public int ConfirmationEmailId { get; set; }
        public int RecoveryEmailId { get; set; }
        public int CultureSelected { get; set; }
        public IEnumerable CultureSelectList { get; set; }
        public virtual Language Culture { get; set; }
    }
}