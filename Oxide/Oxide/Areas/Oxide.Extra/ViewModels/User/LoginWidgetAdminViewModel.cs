﻿using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Extra.ViewModels.User
{
    public class LoginWidgetAdminViewModel : OxideWidgetViewModel
    {
        public string Key { get; set; }
        public string ActiveUrl { get; set; }
        public int ResetPasswordPageId { get; set; }
        public int ImageId { get; set; }
        public string ImageUrl { get; set; }
    }
}