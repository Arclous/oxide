﻿using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Extra.ViewModels.User
{
    public class LoginWidgetViewModel : OxideViewModel
    {
        public int UniqeHash { get; set; }
        public int ResetPasswordPageId { get; set; }
        public string ResetPasswordPageUrl { get; set; }
        public string ActiveUrl { get; set; }
        public string ImageUrl { get; set; }
        public int ImageId { get; set; }
        public bool IsLogined { get; set; }
        public string UserName { get; set; }
        public string UserImage { get; set; }
    }
}