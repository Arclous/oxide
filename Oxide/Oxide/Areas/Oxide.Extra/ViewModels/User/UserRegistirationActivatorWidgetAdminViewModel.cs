﻿using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Extra.ViewModels.User
{
    public class UserRegistirationActivatorWidgetAdminViewModel : OxideWidgetViewModel
    {
        public int Key { get; set; }
        public string ActiveUrl { get; set; }
        public int ImageId { get; set; }
        public string Message { get; set; }
    }
}