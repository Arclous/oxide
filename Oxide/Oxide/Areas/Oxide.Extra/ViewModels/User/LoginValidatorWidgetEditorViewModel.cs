﻿using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Extra.ViewModels.User
{
    public class LoginValidatorWidgetEditorViewModel : OxideWidgetViewModel
    {
        public string Key { get; set; }
        public string ActiveUrl { get; set; }
        public int LoginPageId { get; set; }
    }
}