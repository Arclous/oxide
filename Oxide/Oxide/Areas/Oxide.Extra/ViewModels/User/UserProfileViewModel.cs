﻿using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Extra.ViewModels.User
{
    public class UserProfileViewModel : OxideViewModel
    {
        public int UniqeHash { get; set; }
        public string ActiveUrl { get; set; }
        public string UserImage { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public int Status { get; set; }
    }
}