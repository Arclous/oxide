﻿using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Extra.ViewModels.User
{
    public class PasswordRecoveryWidgetViewModel : OxideViewModel
    {
        public string Key { get; set; }
        public int EmailId { get; set; }
        public int ValidMinutes { get; set; }
        public string ActiveUrl { get; set; }
        public int UniqeHash { get; set; }
        public bool IsLogined { get; set; }
        public int Status { get; set; }
    }
}