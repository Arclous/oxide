﻿using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Extra.ViewModels.User
{
    public class UserRegistirationActivatorViewModel : OxideViewModel
    {
        public string Key { get; set; }
        public int UniqeHash { get; set; }
        public string ActiveUrl { get; set; }
        public string Email { get; set; }
        public string Message { get; set; }
        public string ImageUrl { get; set; }
        public int Status { get; set; }
    }
}