﻿using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Extra.ViewModels.User
{
    public class UserRegistirationWidgetViewModel : OxideViewModel
    {
        public string Key { get; set; }
        public int ImageId { get; set; }
        public string ImageUrl { get; set; }
        public string License { get; set; }
        public bool ShowLicense { get; set; }
        public bool SendConfirmationEmail { get; set; }
        public int ConfirmationEmailId { get; set; }
        public int ConfirmationPageId { get; set; }
        public string ConfirmationPageUrl { get; set; }
        public int ConfirmationEmailSentPageId { get; set; }
        public string ConfirmationEmailSentPageUrl { get; set; }
        public int LoginPageId { get; set; }
        public string LoginPageUrl { get; set; }
        public string ActiveUrl { get; set; }
        public int UniqeHash { get; set; }
        public int ValidDays { get; set; }
        public bool IsLogined { get; set; }
    }
}