﻿using System.Collections.Generic;
using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Extra.ViewModels.Team
{
    public class TeamWidgetFrontViewModel : OxideViewModel
    {
        public virtual ICollection<TeamMemberFrontViewModel> TeamMemberItems { get; set; }
        public string Information { get; set; }
    }
}