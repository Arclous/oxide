﻿using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Extra.ViewModels.Team
{
    public class TeamMemberItemViewModel : OxideWidgetViewModel
    {
        public string MemberName { get; set; }
        public string Info { get; set; }
        public string Description { get; set; }
        public string TwiterLink { get; set; }
        public string FacebookLink { get; set; }
        public string LinkedInLink { get; set; }
        public int ImageId { get; set; }
        public string ImagePath { get; set; }
        public string ActiveUrl { get; set; }
    }
}