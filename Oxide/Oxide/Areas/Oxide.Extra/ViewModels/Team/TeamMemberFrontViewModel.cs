﻿using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Extra.ViewModels.Team
{
    public class TeamMemberFrontViewModel : OxideViewModel
    {
        public string MemberName { get; set; }
        public string Info { get; set; }
        public string Description { get; set; }
        public string TwiterLink { get; set; }
        public string FacebookLink { get; set; }
        public string LinkedInLink { get; set; }
        public string ImagePath { get; set; }
        public string BackgorundImagePath { get; set; }
    }
}