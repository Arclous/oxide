﻿using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Extra.ViewModels.Team
{
    public class TeamWidgetViewModel : OxideWidgetViewModel
    {
        public string Key { get; set; }
        public string Information { get; set; }
        public int Type { get; set; }
        public string TeamMemberItems { get; set; }
        public string ActiveUrl { get; set; }
        public string Title { get; set; }
    }
}