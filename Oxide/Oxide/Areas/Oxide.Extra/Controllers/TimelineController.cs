﻿using System;
using System.Web.Mvc;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideController;
using Oxide.Core.Models.Base;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Syntax;
using Oxide.Extra.Keywords;
using Oxide.Extra.Models.Timeline;
using Oxide.Extra.Services.Interface;
using Oxide.Extra.ViewModels.Timeline;

namespace Oxide.Extra.Controllers
{
    public class TimelineController : OxideController
    {
        private readonly ITimelineWidgetService _timelineWidgetService;

        public TimelineController()
        {
        }

        public TimelineController(IOxideServices oxideServices, ITimelineWidgetService timelineWidgetService)
            : base(oxideServices)
        {
            _timelineWidgetService = timelineWidgetService;
        }

        [OxideAdmin]
        [OxideAdminPermission(Group = "Teamline Widget", Permission = "Can Display Timeline Item",
            Key = "Can_Display_Timeline_Items")]
        public ActionResult DisplayTimelineItems()
        {
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Team Members"),
                    Title = Constants.Empty
                });
        }

        [OxideAdmin]
        [OxideAdminPermission(Group = "Teamline Widget", Permission = "Can Create Timeline Items",
            Key = "Can_Create_Timeline_Items")]
        public ActionResult CreateTimelineItem()
        {
            var timelineItemViewModel = new TimelineItemViewModel {ActiveUrl = HttpContext.Request.RawUrl};
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Timeline Items"),
                    Title = OxideServices.GetText("Create Timeline Item"),
                    Parameters = timelineItemViewModel
                });
        }

        [OxideAdmin]
        [OxideAdminPermission(Group = "Teamline Widget", Permission = "Can Create Timeline Items",
            Key = "Can_Create_Timeline_Items")]
        public ActionResult SaveTeamlineItem(string mediaSelecter, string pageUrl, string dateTitle, string title,
            string description)
        {
            var timelineItem = new TimelineItem
            {
                Name = dateTitle,
                Title = title,
                Description = description,
                ImageId = Convert.ToInt32(mediaSelecter)
            };
            var createTeamMember = _timelineWidgetService.CreateTimelineItem(timelineItem);
            var oxideModel = new OxideModel {RequestType = Syntax.Requests.Static};
            if (createTeamMember)
                oxideModel.SuccessMessage = OxideServices.GetText("Timeline item saved successfully");
            else
                oxideModel.ErrorMessage =
                    OxideServices.GetText(
                        "Problem while saving timeline item, operation is not succesfull, please check the logs");
            TempData[Keys.TimelineItem] = oxideModel;
            return Redirect(pageUrl);
        }

        [OxideAdmin]
        [OxideAdminPermission(Group = "Teamline Widget", Permission = "Can Edit Timeline Items",
            Key = "Can_Edit_Timeline_Items")]
        public ActionResult EditTimelineItem(int id)
        {
            var teamMeber = _timelineWidgetService.GetTimelineItem(id);
            if (teamMeber == null) return RedirectToAction("DisplayTimelineItems");
            var teamMemberItemViewModel = new TimelineItemViewModel
            {
                Id = id,
                Name = teamMeber.Name,
                Title = teamMeber.Title,
                Description = teamMeber.Description,
                ActiveUrl = HttpContext.Request.RawUrl,
                ImageId = teamMeber.ImageId
            };
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Timeline Items"),
                    Title = OxideServices.GetText("Edit Timeline Item"),
                    Parameters = teamMemberItemViewModel
                });
        }

        [OxideAdmin]
        [OxideAdminPermission(Group = "Teamline Widget", Permission = "Can Edit Timeline Items",
            Key = "Can_Edit_Timeline_Items")]
        public ActionResult UpdateTeamline(int id, string mediaSelecter, string pageUrl, string dateTitle, string title,
            string description)
        {
            var teamMemberItem = new TimelineItem
            {
                Id = id,
                Name = dateTitle,
                Title = title,
                Description = description,
                ImageId = Convert.ToInt32(mediaSelecter)
            };
            var updateTeamMember = _timelineWidgetService.UpdateTimelineItem(teamMemberItem);
            var oxideModel = new OxideModel {RequestType = Syntax.Requests.Static};
            if (updateTeamMember)
                oxideModel.SuccessMessage = OxideServices.GetText("Timeline item updated successfully");
            else
                oxideModel.ErrorMessage =
                    OxideServices.GetText(
                        "Problem while updating timeline item, operation is not succesfull, please check the logs");
            TempData[Keys.TimelineItem] = oxideModel;
            return Redirect(pageUrl);
        }

        [HttpGet]
        [OxideAdminPermission(Group = "Teamline Widget", Permission = "Can Display Teamline Items",
            Key = "Can_Display_Timeline_Items")]
        public virtual JsonResult DeleteTimelineItem(int id)
        {
            _timelineWidgetService.DeleteTimelineItem(id);
            return Json("", JsonRequestBehavior.AllowGet);
        }
    }
}