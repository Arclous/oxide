﻿using System;
using System.Web.Mvc;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideController;
using Oxide.Core.Models.Base;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Syntax;
using Oxide.Extra.Keywords;
using Oxide.Extra.Models.Team;
using Oxide.Extra.ViewModels.Team;
using Oxide.Fundamentals.Services.Interface;

namespace Oxide.Extra.Controllers
{
    public class TeamMemberController : OxideController
    {
        private readonly ITeamMemberService _teamMemberService;

        public TeamMemberController()
        {
        }

        public TeamMemberController(IOxideServices oxideServices, ITeamMemberService teamMemberService)
            : base(oxideServices)
        {
            _teamMemberService = teamMemberService;
        }

        [OxideAdmin]
        [OxideAdminPermission(Group = "Team Widget", Permission = "Can Display Team Members",
            Key = "Can_Display_Team_Members")]
        public ActionResult DisplayTeamMembers()
        {
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Team Members"),
                    Title = Constants.Empty
                });
        }

        [OxideAdmin]
        [OxideAdminPermission(Group = "Team Widget", Permission = "Can Create Team Member",
            Key = "Can_Create_Team_Member")]
        public ActionResult CreateTeamMember()
        {
            var teamMemberItemViewModel = new TeamMemberItemViewModel {ActiveUrl = HttpContext.Request.RawUrl};
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Team Members"),
                    Title = OxideServices.GetText("Create Team Member"),
                    Parameters = teamMemberItemViewModel
                });
        }

        [OxideAdmin]
        [OxideAdminPermission(Group = "Team Widget", Permission = "Can Create Team Member",
            Key = "Can_Create_Team_Member")]
        public ActionResult SaveTeamMember(string mediaSelecter, string pageUrl, string memberName, string info,
            string description, string faceBook, string twiter, string linkedIn)
        {
            var teamMemberItem = new TeamMemberItem
            {
                MemberName = memberName,
                Info = info,
                Description = description,
                TwiterLink = twiter,
                FacebookLink = faceBook,
                LinkedInLink = linkedIn,
                ImageId = Convert.ToInt32(mediaSelecter)
            };
            var createTeamMember = _teamMemberService.CreateTeamMember(teamMemberItem);
            var oxideModel = new OxideModel {RequestType = Syntax.Requests.Static};
            if (createTeamMember)
                oxideModel.SuccessMessage = OxideServices.GetText("Team member saved successfully");
            else
                oxideModel.ErrorMessage =
                    OxideServices.GetText(
                        "Problem while saving team member, operation is not succesfull, please check the logs");
            TempData[Keys.TeamMember] = oxideModel;
            return Redirect(pageUrl);
        }

        [OxideAdmin]
        [OxideAdminPermission(Group = "Team Widget", Permission = "Can Edit Team Members", Key = "Can_Edit_Team_Members"
            )]
        public ActionResult EditTeamMember(int id)
        {
            var teamMeber = _teamMemberService.GetTeamMember(id);
            if (teamMeber == null) return RedirectToAction("DisplayTeamMembers");
            var teamMemberItemViewModel = new TeamMemberItemViewModel
            {
                Id = id,
                MemberName = teamMeber.MemberName,
                Info = teamMeber.Info,
                Description = teamMeber.Description,
                TwiterLink = teamMeber.TwiterLink,
                FacebookLink = teamMeber.FacebookLink,
                LinkedInLink = teamMeber.LinkedInLink,
                ActiveUrl = HttpContext.Request.RawUrl,
                ImageId = teamMeber.ImageId
            };
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Team Members"),
                    Title = OxideServices.GetText("Edit Team Member"),
                    Parameters = teamMemberItemViewModel
                });
        }

        [OxideAdmin]
        [OxideAdminPermission(Group = "Team Widget", Permission = "Can Edit Team Members", Key = "Can_Edit_Team_Members"
            )]
        public ActionResult UpdateTeamMember(int id, string mediaSelecter, string pageUrl, string memberName,
            string info, string description, string faceBook, string twiter, string linkedIn)
        {
            var teamMemberItem = new TeamMemberItem
            {
                Id = id,
                MemberName = memberName,
                Info = info,
                Description = description,
                TwiterLink = twiter,
                FacebookLink = faceBook,
                LinkedInLink = linkedIn,
                ImageId = Convert.ToInt32(mediaSelecter)
            };
            var updateTeamMember = _teamMemberService.UpdateTeamMember(teamMemberItem);
            var oxideModel = new OxideModel {RequestType = Syntax.Requests.Static};
            if (updateTeamMember)
                oxideModel.SuccessMessage = OxideServices.GetText("Team member updated successfully");
            else
                oxideModel.ErrorMessage =
                    OxideServices.GetText(
                        "Problem while updating team member, operation is not succesfull, please check the logs");
            TempData[Keys.TeamMember] = oxideModel;
            return Redirect(pageUrl);
        }

        [HttpGet]
        [OxideAdminPermission(Group = "Team Widget", Permission = "Can Display Team Members",
            Key = "Can_Display_Team_Members")]
        public virtual JsonResult DeleteTeamMember(int id)
        {
            _teamMemberService.DeleteTeamMember(id);
            return Json("", JsonRequestBehavior.AllowGet);
        }
    }
}