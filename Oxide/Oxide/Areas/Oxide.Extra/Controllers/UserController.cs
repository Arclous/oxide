﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideController;
using Oxide.Core.Extesions;
using Oxide.Core.Helpers;
using Oxide.Core.Managers.CacheManager;
using Oxide.Core.Models.Base;
using Oxide.Core.Models.Email;
using Oxide.Core.Services.EmailService;
using Oxide.Core.Services.EmailTemplateService;
using Oxide.Core.Services.LanguageService;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Services.OxideUiService;
using Oxide.Core.Services.SiteSettings;
using Oxide.Core.Syntax;
using Oxide.Extra.Keywords;
using Oxide.Extra.Models.User;
using Oxide.Extra.Services.Interface;
using Oxide.Extra.ViewModels.User;
using Oxide.Media.Services.Directory;
using Oxide.Media.Services.File;
using Directory = Oxide.Media.Models.Directory.Directory;
using File = Oxide.Media.Models.File.File;

namespace Oxide.Extra.Controllers
{
    public class UserController : OxideController
    {
        private readonly IOxideCacheManager _cacheManager;
        private readonly IDirectoryService _directoryService;
        private readonly IEmailService _emailService;
        private readonly IEmailSettingsService _emailSettingsService;
        private readonly IEmailTemplateService _emailTemplateService;
        private readonly IFileService _fileService;
        private readonly ILanguageService _languageService;
        private readonly ILoginService _loginService;
        private readonly IOxideUiService _oxideUiService;
        private readonly IUserService _userService;

        public UserController()
        {
        }

        public UserController(IOxideServices oxideServices, IUserService userService, IOxideCacheManager cacheManage,
            IEmailService emailService, IEmailTemplateService emailTemplateService,
            IEmailSettingsService emailSettingsService, IOxideUiService oxideUiService, ILoginService loginService,
            ILanguageService languageService, IFileService fileService, IDirectoryService directoryService)
            : base(oxideServices)
        {
            _userService = userService;
            _cacheManager = cacheManage;
            _emailService = emailService;
            _emailTemplateService = emailTemplateService;
            _emailSettingsService = emailSettingsService;
            _oxideUiService = oxideUiService;
            _loginService = loginService;
            _languageService = languageService;
            _directoryService = directoryService;
            _fileService = fileService;
        }

        [OxideAdmin]
        [OxideAdminPermission(Group = "User Login System", Permission = "Can Manage Login System Widget",
            Key = "Can_Manage_Login_System_Widget")]
        public ActionResult DisplaySettings()
        {
            return View();
        }

        [OxideAdmin]
        [OxideAdminPermission(Group = "User Login System", Permission = "Can Manage Login System Widget",
            Key = "Can_Manage_Login_System_Widget")]
        public ActionResult NewSetting()
        {
            var pageViewModel = new LoginSettingEditorViewModel();
            var selectList =
                _languageService.GetAllActiveLanguages()
                                .Select(v => new SelectListItem {Text = v.Native, Value = v.Id.ToString()})
                                .ToList();
            pageViewModel.CultureSelectList = selectList;
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Create New Setting"),
                    Title = Constants.Empty,
                    Parameters = pageViewModel
                });
        }

        [OxideAdmin]
        [OxideAdminPermission(Group = "User Login System", Permission = "Can Manage Login System Widget",
            Key = "Can_Manage_Login_System_Widget")]
        public ActionResult CreateSetting(LoginSettingEditorViewModel model)
        {
            var newLoginSetting = new LoginSetting();
            model.CopyPropertiesTo(newLoginSetting);
            newLoginSetting.Culture = _languageService.GetLanguage(model.CultureSelected);
            _userService.CreateSetting(newLoginSetting);
            return RedirectToAction("DisplaySettings");
        }

        [OxideAdmin]
        [OxideAdminPermission(Group = "User Login System", Permission = "Can Manage Login System Widget",
            Key = "Can_Manage_Login_System_Widget")]
        public ActionResult EditSetting(int id)
        {
            var pageViewModel = new LoginSettingEditorViewModel();
            _userService.GetSetting(id).CopyPropertiesTo(pageViewModel);
            pageViewModel.CultureSelected = pageViewModel.Culture.Id;
            var selectList =
                _languageService.GetAllActiveLanguages()
                                .Select(v => new SelectListItem {Text = v.Native, Value = v.Id.ToString()})
                                .ToList();
            pageViewModel.CultureSelectList = selectList;
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Edit Setting"),
                    Title = Constants.Empty,
                    Parameters = pageViewModel
                });
        }

        [OxideAdmin]
        [OxideAdminPermission(Group = "User Login System", Permission = "Can Manage Login System Widget",
            Key = "Can_Manage_Login_System_Widget")]
        public ActionResult UpdateSetting(LoginSettingEditorViewModel model)
        {
            //model.CultureId=model.CultureSelected;
            model.Culture = _languageService.GetLanguage(model.CultureSelected);
            _userService.UpdateSetting(model);
            return RedirectToAction("DisplaySettings");
        }

        [HttpGet]
        [OxideAdmin]
        [OxideAdminPermission(Group = "User Login System", Permission = "Can Manage Login System Widget",
            Key = "Can_Manage_Login_System_Widget")]
        public virtual JsonResult SettingList(int? pageSize, int? page, string filter)
        {
            if (pageSize == null) pageSize = 10;
            if (page == null) page = 0;
            var settingsWithFilter = _userService.GetSettingsWithFilter((int) pageSize, (int) page, filter);
            return Json(settingsWithFilter, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdmin]
        [OxideAdminPermission(Group = "User Login System", Permission = "Can Manage Login System Widget",
            Key = "Can_Manage_Login_System_Widget")]
        public virtual JsonResult DeleteSetting(int id)
        {
            _userService.DeleteSetting(id);
            return Json(new {success = true}, JsonRequestBehavior.AllowGet);
        }

        #region Settings

        #endregion

        #region Helpers

        public virtual ActionResult UploadFile(HttpPostedFileBase file, string activeUrl)
        {
            //Status
            //0 Error. user not logined
            //1 Profile Image updated
            //2 Error
            var result = new Response();
            var urlToRedirect = "";
            var statusOfAction = 0;
            activeUrl = OxideUtils.GetPureUrl(activeUrl);
            urlToRedirect = $@"{activeUrl}?sts={OxideUtils.GetEncryptedQueryString(statusOfAction.ToString())}";
            if (file != null && file.ContentLength > 0)
            {
                var mediaDirectory = _directoryService.GetMediaDirectory();
                var directory = new Directory {Name = "UserImages", Title = "UserImages", ParentId = mediaDirectory.Id};
                directory.Path = Path.Combine(mediaDirectory.Path, directory.Name);
                if (!_directoryService.IsDirectoryExist(directory.Path))
                {
                    result = _directoryService.CreateDirectory(mediaDirectory.Id, "UserImages");
                    result = _fileService.UploadFile(((Directory) result.Data).Id, file);
                    result.Success = true;
                }
                else
                {
                    directory = (Directory) _directoryService.GetDirectory(directory.Path).Data;
                    result = _fileService.UploadFile(directory.Id, file);
                    result.Success = true;
                }
                if (result.Success)
                {
                    var newUserImageFile = (File) result.Data;
                    var loginedUser = _loginService.GetUserInfo();
                    if (loginedUser != null)
                    {
                        var userInfo = _userService.GetUser(loginedUser.Id);
                        userInfo.UsrImageId = newUserImageFile.Id;
                        _userService.UpdateUseProfileImage(userInfo.Id, newUserImageFile.Id);
                        loginedUser.UserImageId = newUserImageFile.Id;
                        _loginService.UpdateUserInfo(loginedUser);
                        statusOfAction = 1;
                        urlToRedirect =
                            $@"{activeUrl}?sts={OxideUtils.GetEncryptedQueryString(statusOfAction.ToString())}";
                        return Redirect(urlToRedirect);
                    }
                    statusOfAction = 0;
                    urlToRedirect = $@"{activeUrl}?sts={OxideUtils.GetEncryptedQueryString(statusOfAction.ToString())}";
                    return Redirect(urlToRedirect);
                }
            }
            else
                result.Success = false;
            return Redirect(urlToRedirect);
        }

        public virtual ActionResult UpdateUserProfile(string userName, string name, string surname, string email,
            string activePageUrl)
        {
            //Status
            //0 Error. user not logined
            //3 Email used
            //4 UserName used
            //5 Succes, profile updated
            var urlToRedirect = "";
            var statusOfAction = 0;
            activePageUrl = OxideUtils.GetPureUrl(activePageUrl);
            urlToRedirect = $@"{activePageUrl}?sts={OxideUtils.GetEncryptedQueryString(statusOfAction.ToString())}";
            var loginedUser = _loginService.GetUserInfo();
            if (loginedUser == null) return Redirect(urlToRedirect);
            if (loginedUser.Email != email)
            {
                if (_userService.IsEmailUsed(email))
                {
                    statusOfAction = 3;
                    urlToRedirect =
                        $@"{activePageUrl}?sts={OxideUtils.GetEncryptedQueryString(statusOfAction.ToString())}";
                    return Redirect(urlToRedirect);
                }
            }
            if (loginedUser.UserName != userName)
            {
                if (_userService.IsUserExist(userName))
                {
                    statusOfAction = 4;
                    urlToRedirect =
                        $@"{activePageUrl}?sts={OxideUtils.GetEncryptedQueryString(statusOfAction.ToString())}";
                    return Redirect(urlToRedirect);
                }
            }
            var userInfo = _userService.GetUser(loginedUser.Id);
            var userNewInfo = new UserViewModel();
            userInfo.CopyPropertiesTo(userNewInfo);
            userInfo.UserName = userName;
            userInfo.Name = name;
            userInfo.Surname = surname;
            userInfo.Email = email;
            _userService.UpdateUser(userNewInfo);
            statusOfAction = 5;
            urlToRedirect = $@"{activePageUrl}?sts={OxideUtils.GetEncryptedQueryString(statusOfAction.ToString())}";
            return Redirect(urlToRedirect);
        }

        public virtual ActionResult UpdatePassword(string password, string pageUrl)
        {
            //Status
            //0 Error. user not logined
            //6 Succes, password updated
            var urlToRedirect = "";
            var statusOfAction = 0;
            pageUrl = OxideUtils.GetPureUrl(pageUrl);
            urlToRedirect = $@"{pageUrl}?sts={OxideUtils.GetEncryptedQueryString(statusOfAction.ToString())}";
            var loginedUser = _loginService.GetUserInfo();
            if (loginedUser == null) return Redirect(urlToRedirect);
            _userService.UpdateUserPassword(loginedUser.Id, password);
            statusOfAction = 6;
            urlToRedirect = $@"{pageUrl}?sts={OxideUtils.GetEncryptedQueryString(statusOfAction.ToString())}";
            return Redirect(urlToRedirect);
        }

        public ActionResult ReSendConfirmationEmail(string reg, string pageUrl)
        {
            if (Request.QueryString["reg"] != "" && Request.QueryString["reg"] != null)
                reg = OxideUtils.GetDecryptedQueryString(HttpUtility.UrlDecode(Request.QueryString["reg"]));
            var userId = reg;
            var previousUserConfirmation = _userService.GetConfirmation(Convert.ToInt32(userId));
            var user = _userService.GetUser(Convert.ToInt32(userId));

            //Resent confirmation email
            var selectedWidget = _cacheManager.GetOrSet(previousUserConfirmation.WidgetHash.ToString(),
                previousUserConfirmation.WidgetHash.ToString(), "User Login System",
                () => _userService.GetUserRegistirationWidget(previousUserConfirmation.WidgetHash), 60, false);
            var emailSettings = _emailSettingsService.GetEmailSettings();
            var emailTemplate = _emailTemplateService.GetEmailTemplate(selectedWidget.ConfirmationEmailId);
            var confirmationEmailPage = _oxideUiService.GetContent("Page", selectedWidget.ConfirmationPageId.ToString());
            var confirmationEmailPageUrl = confirmationEmailPage != null ? ((dynamic) confirmationEmailPage).Url : "";

            //Create confirmation record for confirming account
            var key = _userService.CreateConfirmation(Convert.ToInt32(userId), selectedWidget.ValidDays,
                Convert.ToInt32(previousUserConfirmation.WidgetHash));
            var linkKey = string.Format(@"{0}?conf={1}", OxideUtils.GetFullUrl(confirmationEmailPageUrl), key);

            //Preapare keywords dictionary
            var keywords = new Dictionary<string, string> {{"[USEREMAIL]", user.Email}, {"[LINK]", linkKey}};
            var emailContent = _emailService.FillParameters(emailTemplate.Template, keywords);

            //Send confirmation email
            SendEmail(emailContent, emailTemplate.Name, emailSettings.Sender, user.Email);

            //Create confirmation record for confirming account
            return Redirect(pageUrl);
        }

        public ActionResult RegisterUser(string email, string password, string pageUrl, string uniqeHash)
        {
            var oxideModel = new OxideModel {RequestType = Syntax.Requests.Static};
            var selectedWidget = _cacheManager.GetOrSet(uniqeHash, uniqeHash, "User Login System",
                () => _userService.GetUserRegistirationWidget(Convert.ToInt32(uniqeHash)), 60, false);
            var isEmailUsed = _userService.IsEmailUsed(email);
            if (isEmailUsed)
            {
                oxideModel.ErrorMessage =
                    OxideServices.GetText("Email address already token,please use different email address");
                Session[Keys.UserRegistirationFrontWidget] = oxideModel;
                return Redirect(pageUrl);
            }
            var newUser = new User
            {
                Activated = !selectedWidget.SendConfirmationEmail,
                CreateDateTime = DateTime.Now,
                Email = email,
                IsEmailConfirmed = !selectedWidget.SendConfirmationEmail,
                LastAccessDateTime = DateTime.Now,
                Password = password,
                UserName = email
            };
            var userId = _userService.CreateUser(newUser);
            if (selectedWidget.SendConfirmationEmail)
            {
                var emailSettings = _emailSettingsService.GetEmailSettings();
                var emailTemplate = _emailTemplateService.GetEmailTemplate(selectedWidget.ConfirmationEmailId);
                var confirmationEmailPage = _oxideUiService.GetContent("Page",
                    selectedWidget.ConfirmationPageId.ToString());
                var confirmationEmailPageUrl = confirmationEmailPage != null
                    ? ((dynamic) confirmationEmailPage).Url
                    : "";

                //Create confirmation record for confirming account
                var key = _userService.CreateConfirmation(userId, selectedWidget.ValidDays, Convert.ToInt32(uniqeHash));
                var linkKey = string.Format(@"{0}?conf={1}", OxideUtils.GetFullUrl(confirmationEmailPageUrl), key);

                //Preapare keywords dictionary
                var keywords = new Dictionary<string, string> {{"[UserEmail]", email}, {"[LINK]", linkKey}};
                var emailContent = _emailService.FillParameters(emailTemplate.Template, keywords);

                //Send confirmation email
                SendEmail(emailContent, emailTemplate.Name, emailSettings.Sender, email);

                //Redirect to "Email Sent Page"
                var confirmationEmailSendPage = _oxideUiService.GetContent("Page",
                    selectedWidget.ConfirmationEmailSentPageId.ToString());
                var confirmationEmailSendPageUrl = confirmationEmailSendPage != null
                    ? ((dynamic) confirmationEmailSendPage).Url
                    : "";
                confirmationEmailSendPageUrl = string.Format(@"/{0}?reg={1}", confirmationEmailSendPageUrl,
                    OxideUtils.GetEncryptedQueryString(HttpUtility.UrlEncode(userId.ToString())));
                return Redirect(OxideUtils.GetFullUrl(confirmationEmailSendPageUrl));
            }
            return Redirect("/");
        }

        [ValidateAntiForgeryToken]
        public ActionResult Login(string email, string password, string pageUrl)
        {
            var loginResult = Keys.LoginResult.NotSuccess;
            var oxideModel = new OxideModel {RequestType = Syntax.Requests.Static};
            if ((email != string.Empty) && (password != string.Empty))
                loginResult = _loginService.LoginUser(email, password);
            switch (loginResult)
            {
                case Keys.LoginResult.Success:
                    if (pageUrl == string.Empty)
                        return Redirect(pageUrl);
                    break;
                case Keys.LoginResult.UserNotFound:
                    oxideModel.ErrorMessage = OxideServices.GetText("User not found");
                    Session[Keys.LoginWidget] = oxideModel;
                    return Redirect(pageUrl);
                default:
                    oxideModel.ErrorMessage =
                        OxideServices.GetText("Incorrect username or password,please check your login information");
                    Session[Keys.LoginWidget] = oxideModel;
                    return Redirect(pageUrl);
            }
            return Redirect(pageUrl);
        }

        public ActionResult LogOut(string url)
        {
            _loginService.SignOut();
            return Redirect(url);
        }

        public ActionResult ResetPassword(string email, string password, string uniqeHash, string pageUrl,
            string recoveryCode)
        {
            var user = _userService.GetUserByUserEmail(email);
            var isValidCode = _userService.IsRecoveryCodeValid(user.Id, recoveryCode);
            if (isValidCode)
            {
                user.Password = password;
                _userService.UpdateUserPassword(user.Id, password);
                _userService.DeletePasswordRecovery(user.Id);
                var newUrl = string.Format(@"{0}?sts={1}", pageUrl,
                    (HttpUtility.UrlEncode(OxideUtils.GetEncryptedQueryString("1"))));
                return Redirect(newUrl);
            }
            else
            {
                var newUrl = string.Format(@"{0}?sts={1}", pageUrl,
                    (HttpUtility.UrlEncode(OxideUtils.GetEncryptedQueryString("2"))));
                return Redirect(newUrl);
            }
        }

        [HttpGet]
        public virtual JsonResult SendRecoveryEmail(string email, string emailId, string uniqeHash)
        {
            var result = new Response();
            var user = _userService.GetUserByUserEmail(email);
            if (user != null)
            {
                var selectedWidget = _cacheManager.GetOrSet(uniqeHash, uniqeHash, "User Login System",
                    () => _userService.GetPasswordRecoveryWidget(Convert.ToInt32(uniqeHash)), 60, false);
                var emailSettings = _emailSettingsService.GetEmailSettings();
                var emailTemplate = _emailTemplateService.GetEmailTemplate(selectedWidget.EmailId);
                var key = _userService.CreatePasswordRecoveryRecord(user.Id, selectedWidget.ValidMinutes);
                var keywords = new Dictionary<string, string> {{"[CODE]", key}};
                var emailContent = _emailService.FillParameters(emailTemplate.Template, keywords);

                //Send confirmation email
                result.Success = SendEmail(emailContent, emailTemplate.Name, emailSettings.Sender, email);
            }
            return Json(result.Success ? new {success = true} : new {success = false}, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual JsonResult ValideRecoveryCode(string email, string recoveryCode)
        {
            var result = new Response();
            var user = _userService.GetUserByUserEmail(email);
            if (user != null)
                result.Success = _userService.IsRecoveryCodeValid(user.Id, recoveryCode);
            return Json(result.Success ? new {success = true} : new {success = false}, JsonRequestBehavior.AllowGet);
        }

        private bool SendEmail(string template, string subject, string from, string to)
        {
            var confirmationEmail = new Email {Body = template, Subject = subject, From = from, To = to};
            return _emailService.SendEmail(confirmationEmail);
        }

        #endregion
    }
}