﻿using System.Web.Mvc;
using Oxide.Core.Base.OxideController;
using Oxide.Core.Services.OxideService;
using Oxide.Extra.Services.Interface;

namespace Oxide.Extra.Controllers
{
    public class CommentsController : OxideController
    {
        private readonly ICommentsService _commentsService;

        public CommentsController()
        {
        }

        public CommentsController(IOxideServices oxideServices, ICommentsService commentsService) : base(oxideServices)
        {
            _commentsService = commentsService;
        }

        public virtual JsonResult CommentsList(int? pageSize, int? page, int key)
        {
            if (pageSize == null) pageSize = 10;
            if (page == null) page = 0;
            var logsList = _commentsService.GetCommentsPages((int) pageSize, (int) page, key);
            return Json(logsList, JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult DeleteComment(int id)
        {
            _commentsService.DeleteComment(id);
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult SaveComment(string message, int key)
        {
            return Json(_commentsService.SaveComment(message, key), JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult UpdateComment(string message, int key, int id)
        {
            _commentsService.UpdateComment(message, key, id);
            return Json("", JsonRequestBehavior.AllowGet);
        }
    }
}