﻿using Oxide.Core.Base.OxideDependency;
using Oxide.Extra.Keywords;
using Oxide.Extra.ViewModels.User;

namespace Oxide.Extra.Services.Interface
{
    /// <summary>
    ///     Service for managing login process
    /// </summary>
    public interface ILoginService : IOxideDependency
    {
        /// <summary>
        ///     Logins user to site
        /// </summary>
        /// <param name="userEmail">User name</param>
        /// <param name="password">Password of the user</param>
        Keys.LoginResult LoginUser(string userEmail, string password);
        /// <summary>
        ///     Sings Out user
        /// </summary>
        void SignOut();
        /// <summary>
        ///     Checks if user logined to site
        /// </summary>
        bool IsUserLogined();
        /// <summary>
        ///     Updates user last access date and time
        /// </summary>
        /// <param name="userName">User name</param>
        void UpdateAccessDateTime(string userName);
        /// <summary>
        ///     Gets logined user info
        /// </summary>
        UserViewModel GetUserInfo();
        /// <summary>
        ///     Updates logined user info
        /// </summary>
        /// <param name="userViewModel">User view model</param>
        void UpdateUserInfo(UserViewModel userViewModel);
        /// <summary>
        ///     Gets key for user login
        /// </summary>
        string GetKeyForUser(int userId);
    }
}