﻿using Oxide.Core.Base.OxideDependency;
using Oxide.Extra.Models.Timeline;

namespace Oxide.Extra.Services.Interface
{
    /// <summary>
    ///     Service for Team Widget
    /// </summary>
    public interface ITimelineWidgetService : IOxideDependency
    {
        /// <summary>
        ///     Creates team member according to model
        /// </summary>
        /// <param name="timelineItem">Model of the team member</param>
        bool CreateTimelineItem(TimelineItem timelineItem);
        /// <summary>
        ///     Updates team member
        /// </summary>
        /// <param name="model">View model for team member</param>
        bool UpdateTimelineItem(TimelineItem model);
        /// <summary>
        ///     Deletes team member according to model
        /// </summary>
        /// <param name="model">Model for team member</param>
        void DeleteTimelineItem(TimelineItem model);
        /// <summary>
        ///     Deletes team member according to id
        /// </summary>
        /// <param name="id">Id of the team member</param>
        void DeleteTimelineItem(int id);
        /// <summary>
        ///     Gets team member according to id
        /// </summary>
        /// <param name="id">Id of the team member</param>
        TimelineItem GetTimelineItem(int id);
        /// <summary>
        ///     Saves team widget according to model
        /// </summary>
        /// <param name="timelineWidget">Model of the team widget</param>
        bool SaveWidget(TimelineWidget timelineWidget);
        /// <summary>
        ///     Deletes team widget according to model
        /// </summary>
        /// <param name="key">Key of the page</param>
        void DeleteWidget(int key);
        /// <summary>
        ///     Gets team widget according to key
        /// </summary>
        /// <param name="key">Key of the page</param>
        TimelineWidget GetWidget(int key);
    }
}