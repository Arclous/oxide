﻿using Oxide.Core.Base.OxideDependency;
using Oxide.Extra.Models.Team;

namespace Oxide.Fundamentals.Services.Interface
{
    /// <summary>
    ///     Service for Team Widget
    /// </summary>
    public interface ITeamMemberService : IOxideDependency
    {
        /// <summary>
        ///     Creates team member according to model
        /// </summary>
        /// <param name="teamMemberItem">Model of the team member</param>
        bool CreateTeamMember(TeamMemberItem teamMemberItem);
        /// <summary>
        ///     Updates team member
        /// </summary>
        /// <param name="model">View model for team member</param>
        bool UpdateTeamMember(TeamMemberItem model);
        /// <summary>
        ///     Deletes team member according to model
        /// </summary>
        /// <param name="model">Model for team member</param>
        void DeleteTeamMember(TeamMemberItem model);
        /// <summary>
        ///     Deletes team member according to id
        /// </summary>
        /// <param name="id">Id of the team member</param>
        void DeleteTeamMember(int id);
        /// <summary>
        ///     Gets team member according to id
        /// </summary>
        /// <param name="id">Id of the team member</param>
        TeamMemberItem GetTeamMember(int id);
        /// <summary>
        ///     Saves team widget according to model
        /// </summary>
        /// <param name="teamWidget">Model of the team widget</param>
        bool SaveWidget(TeamWidget teamWidget);
        /// <summary>
        ///     Deletes team widget according to model
        /// </summary>
        /// <param name="key">Key of the page</param>
        void DeleteWidget(int key);
        /// <summary>
        ///     Gets team widget according to key
        /// </summary>
        /// <param name="key">Key of the page</param>
        TeamWidget GetWidget(int key);
    }
}