﻿using System.Collections.Generic;
using Oxide.Core.Base.OxideDependency;
using Oxide.Core.ViewModels.FilterViewModels;
using Oxide.Extra.Models.User;
using Oxide.Extra.ViewModels.User;

namespace Oxide.Extra.Services.Interface
{
    /// <summary>
    ///     Service for Users
    /// </summary>
    public interface IUserService : IOxideDependency
    {
        #region Users
        /// <summary>
        ///     Creates user according to model
        /// </summary>
        /// <param name="user">Model of the user</param>
        int CreateUser(User user);
        /// <summary>
        ///     Updates user according to model
        /// </summary>
        /// <param name="user">Model of the user</param>
        void UpdateUser(UserViewModel user);
        /// <summary>
        ///     Updates user profile according to model
        /// </summary>
        /// <param name="userId">Id of the user</param>
        /// <param name="userName">User name</param>
        /// <param name="name">Name of the user</param>
        /// <param name="surname">Surname of the user</param>
        /// <param name="email">Email of the user</param>
        void UpdateProfile(int userId, string userName, string name, string surname, string email);
        /// <summary>
        ///     Updates user password according user id
        /// </summary>
        /// <param name="userId">Id of the user</param>
        /// <param name="password">New password of the user</param>
        void UpdateUserPassword(int userId, string password);
        /// <summary>
        ///     Updates user password according user id
        /// </summary>
        /// <param name="userdId">Id of the user</param>
        /// <param name="imageId">Id of the user image</param>
        void UpdateUseProfileImage(int userdId, int imageId);
        /// <summary>
        ///     Activates user according to model
        /// </summary>
        /// <param name="id">Id of the user</param>
        void ActivateUser(int id);
        /// <summary>
        ///     DeActivates user according to model
        /// </summary>
        /// <param name="id">Id of the user</param>
        void DeActivateUser(int id);
        /// <summary>
        ///     Deletes users according to model
        /// </summary>
        /// <param name="model">User model</param>
        void DeleteUser(User model);
        /// <summary>
        ///     Deletes user according to id
        /// </summary>
        /// <param name="id">Id of the user</param>
        void DeleteUser(int id);
        /// <summary>
        ///     Gets user according to id
        /// </summary>
        /// <param name="id">Id of the user</param>
        User GetUser(int id);
        /// <summary>
        ///     Gets user according to username
        /// </summary>
        /// <param name="userName">UserName of the user</param>
        User GetUserByUserName(string userName);
        /// <summary>
        ///     Gets user according to username
        /// </summary>
        /// <param name="email">Email of the user</param>
        User GetUserByUserEmail(string email);
        /// <summary>
        ///     Checks if the user exist
        /// </summary>
        /// <param name="userName">Username of the user</param>
        bool IsUserExist(string userName);
        /// <summary>
        ///     Checks if the email address used
        /// </summary>
        /// <param name="email">Email of the user</param>
        bool IsEmailUsed(string email);
        /// <summary>
        ///     Updates last access date and time of the user according to user name
        /// </summary>
        /// <param name="userName">Username of user</param>
        void UpdateLastAccessDateTime(string userName);
        /// <summary>
        ///     Updates login key of the user according to user id
        /// </summary>
        /// <param name="key">Key of the login</param>
        /// <param name="userId">User id</param>
        void UpdateLoginKey(string key, int userId);
        /// <summary>
        ///     Checks if the key is valid
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="key">Key to check</param>
        bool IsKeyValid(int userId, string key);
        /// <summary>
        ///     Gets users
        /// </summary>
        List<User> GetAllUsers();
        /// <summary>
        ///     Gets pages as filter view model
        /// </summary>
        /// <param name="pageSize">Size of the page for each page</param>
        /// <param name="page">Requested page index</param>
        /// <param name="filter">Filter criterias to filtering pages</param>
        UserFilterViewModel GetUsersWithFilter(int pageSize, int page, string filter);
        #endregion

        #region Settings
        /// <summary>
        ///     Creates setting according to model
        /// </summary>
        /// <param name="setting">Model of the setting</param>
        int CreateSetting(LoginSetting setting);
        /// <summary>
        ///     Updates setting according to model
        /// </summary>
        /// <param name="setting">Model of the setting</param>
        void UpdateSetting(LoginSettingEditorViewModel setting);
        /// <summary>
        ///     Deletes setting according to model
        /// </summary>
        /// <param name="setting">Setting model</param>
        void DeleteSetting(LoginSetting setting);
        /// <summary>
        ///     Deletes setting according to id
        /// </summary>
        /// <param name="id">Id of the setting</param>
        void DeleteSetting(int id);
        /// <summary>
        ///     Gets setting according to id
        /// </summary>
        /// <param name="id">Id of the settings</param>
        LoginSetting GetSetting(int id);
        /// <summary>
        ///     Gets setting according to culture
        /// </summary>
        /// <param name="cultureId">Id of the culture</param>
        LoginSetting GetSettingByCulture(int cultureId);
        /// <summary>
        ///     Gets setting according to culture
        /// </summary>
        /// <param name="cultureCode">Code of the culture</param>
        LoginSetting GetSettingByCulture(string cultureCode);
        /// <summary>
        ///     Gets settings
        /// </summary>
        List<LoginSetting> GetAllSettings();
        /// <summary>
        ///     Gets settings as filter view model
        /// </summary>
        /// <param name="pageSize">Size of the page for each page</param>
        /// <param name="page">Requested page index</param>
        /// <param name="filter">Filter criterias to filtering pages</param>
        FilterModel<LoginSettingEditorViewModel> GetSettingsWithFilter(int pageSize, int page, string filter);
        #endregion

        #region Widgets
        /// <summary>
        ///     Saves user registiration widget according to model
        /// </summary>
        /// <param name="userRegistirationWidget">Model of the user registiration widget</param>
        bool SaveUserRegistirationWidget(UserRegistirationWidget userRegistirationWidget);
        /// <summary>
        ///     Deletes user registiration widget according to key
        /// </summary>
        /// <param name="key">Key of the page</param>
        void DeleteUserRegistirationWidget(int key);
        /// <summary>
        ///     Gets user registiration widget according to key
        /// </summary>
        /// <param name="key">Key of the page</param>
        UserRegistirationWidget GetUserRegistirationWidget(int key);
        /// <summary>
        ///     Saves user registiration email informerwidget according to model
        /// </summary>
        /// <param name="userRegistirationEmailInformerWidget">Model of the user registiration email informer widget</param>
        bool SaveUserRegistirationEmailInformerWidget(
            UserRegistirationEmailInformerWidget userRegistirationEmailInformerWidget);
        /// <summary>
        ///     Deletes user registiration email informerwidget according to key
        /// </summary>
        /// <param name="key">Key of the page</param>
        void DeleteUserRegistirationEmailInformerWidget(int key);
        /// <summary>
        ///     Gets user registiration email informer widget according to key
        /// </summary>
        /// <param name="key">Key of the page</param>
        UserRegistirationEmailInformerWidget GetUserRegistirationEmailInformerWidget(int key);
        /// <summary>
        ///     Saves user registiration activator widget according to model
        /// </summary>
        /// <param name="userRegistirationActivatorWidget">Model of the user registiration activator widget</param>
        bool SaveUserRegistirationActivatorWidget(UserRegistirationActivatorWidget userRegistirationActivatorWidget);
        /// <summary>
        ///     Deletes user registiration activator widget according to model
        /// </summary>
        /// <param name="key">Key of the page</param>
        void DeleteUserRegistirationActivatorWidget(int key);
        /// <summary>
        ///     Gets user registiration activator widget according to key
        /// </summary>
        /// <param name="key">Key of the page</param>
        UserRegistirationActivatorWidget GetUserRegistirationActivatorWidget(int key);
        /// <summary>
        ///     Saves login validator widget according to model
        /// </summary>
        /// <param name="loginValidatorWidget">Model of the login validator widget</param>
        bool SaveLoginValidatorWidget(LoginValidatorWidget loginValidatorWidget);
        /// <summary>
        ///     Deletes login validator widget according to key
        /// </summary>
        /// <param name="key">Key of the page</param>
        void DeleteLoginValidatorWidget(int key);
        /// <summary>
        ///     Gets login validator widget according to key
        /// </summary>
        /// <param name="key">Key of the page</param>
        LoginValidatorWidget GetLoginValidatorWidget(int key);
        /// <summary>
        ///     Saves login widget according to model
        /// </summary>
        /// <param name="loginWidget">Model of the login widget</param>
        bool SaveLoginWidget(LoginWidget loginWidget);
        /// <summary>
        ///     Saves login widget according to key
        /// </summary>
        /// <param name="key">Key of the page</param>
        void DeleteLoginWidget(int key);
        /// <summary>
        ///     Gets login widget according to key
        /// </summary>
        /// <param name="key">Key of the page</param>
        LoginWidget GetLoginWidget(int key);
        /// <summary>
        ///     Saves password recovry widget according to model
        /// </summary>
        /// <param name="recoveryWidget">Model of the password recovery widget</param>
        bool SavePasswordRecoveryWidget(PasswordRecoveryWidget recoveryWidget);
        /// <summary>
        ///     Deletes password recovry widget according to key
        /// </summary>
        /// <param name="key">Key of the page</param>
        void DeletePasswordRecoveryWidget(int key);
        /// <summary>
        ///     Gets password recovery widget according to key
        /// </summary>
        /// <param name="key">Key of the page</param>
        PasswordRecoveryWidget GetPasswordRecoveryWidget(int key);
        #endregion

        #region Confirmation
        /// <summary>
        ///     Creates confirmation record according to model
        /// </summary>
        /// <param name="userId">Id of the user</param>
        /// <param name="validDays">Total days of the waiting for confirmation</param>
        /// <param name="hash">Hash code of the widget</param>
        string CreateConfirmation(int userId, int validDays, int hash);
        /// <summary>
        ///     Gets confirmation record according to user id
        /// </summary>
        /// <param name="userId">Id of the user</param>
        UserConfirmation GetConfirmation(int userId);
        /// <summary>
        ///     Gets confirmation record according to key
        /// </summary>
        /// <param name="key">Key of the confirmation record</param>
        UserConfirmation GetConfirmation(string key);
        /// <summary>
        ///     Deletes confirmation record according to id
        /// </summary>
        /// <param name="id">Id of the confirmation record</param>
        void DeleteConfirmation(int id);
        #endregion

        #region ReconveryPassword
        /// <summary>
        ///     Creates password recovery record according to model
        /// </summary>
        /// <param name="userId">Id of the user</param>
        /// <param name="validMinutes">Total minutes of the waiting for recovery</param>
        string CreatePasswordRecoveryRecord(int userId, int validMinutes);
        /// <summary>
        ///     Gets password recovery record according to user id
        /// </summary>
        /// <param name="userId">Id of the user</param>
        PasswordRecoveryItem GetPasswordRevocery(int userId);
        /// <summary>
        ///     Deletes password recovery record according to user id
        /// </summary>
        /// <param name="userId">Id of the user</param>
        void DeletePasswordRecovery(int userId);
        /// <summary>
        ///     Checks if the recovery code is valid
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="recoveryCode">Recovery code to check</param>
        bool IsRecoveryCodeValid(int userId, string recoveryCode);
        #endregion
    }
}