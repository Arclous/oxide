﻿using Oxide.Core.Base.OxideDependency;
using Oxide.Core.ViewModels.FilterViewModels;
using Oxide.Extra.Models.Comments;
using Oxide.Extra.ViewModels.Comments;

namespace Oxide.Extra.Services.Interface
{
    /// <summary>
    ///     Service for managing comments and comments widget settings
    /// </summary>
    public interface ICommentsService : IOxideDependency
    {
        #region WidgetSettings
        /// <summary>
        ///     Saves comments widget according to model
        /// </summary>
        /// <param name="commentsWidgetViewModel">Model of the comments widget</param>
        bool SaveWidget(CommentsEditorViewModel commentsWidgetViewModel);
        /// <summary>
        ///     Gets comments widget according to key
        /// </summary>
        /// <param name="key">Key of the page</param>
        CommentsEditorViewModel GetWidget(int key);
        /// <summary>
        ///     Deletes comments widget according to key
        /// </summary>
        /// <param name="key">Key of the page</param>
        void DeleteWidget(int key);
        #endregion

        #region Comments
        /// <summary>
        ///     Saves comments according to values
        /// </summary>
        /// <param name="message">Value of the comments</param>
        /// <param name="key">Key of the page widget</param>
        CommentsViewModel SaveComment(string message, int key);
        /// <summary>
        ///     Updates comments according to values
        /// </summary>
        /// <param name="message">Value of the comments</param>
        /// <param name="key">Key of the page widget</param>
        /// <param name="id">Id of the comment</param>
        void UpdateComment(string message, int key, int id);
        /// <summary>
        ///     Gets comments according to id
        /// </summary>
        /// <param name="id">Id of the comment</param>
        Comment GetComment(int id);
        /// <summary>
        ///     Gets comments as filter view model
        /// </summary>
        /// <param name="pageSize">Size of the page for each page</param>
        /// <param name="page">Requested page index</param>
        /// <param name="key">Key of the page</param>
        FilterModel<CommentsViewModel> GetCommentsPages(int pageSize, int page, int key);
        /// <summary>
        ///     Deletes comments according to id
        /// </summary>
        /// <param name="id">Id of the comment</param>
        void DeleteComment(int id);
        #endregion
    }
}