﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using Oxide.Core.Data.Repository;
using Oxide.Core.Extesions;
using Oxide.Core.Helpers;
using Oxide.Core.ViewModels.FilterViewModels;
using Oxide.Extra.Models.User;
using Oxide.Extra.Services.Interface;
using Oxide.Extra.ViewModels.User;
using Oxide.Media.Models.File;

namespace Oxide.Extra.Services.Implementation
{
    public class UserService : IUserService
    {
        private const int KeyLength = 15;
        private readonly IOxideRepository<File> _fileRepository;
        private readonly IOxideRepository<LoginSetting> _loginSettingRepository;
        private readonly IOxideRepository<LoginValidatorWidget> _loginValidatorWidgetRepository;
        private readonly IOxideRepository<LoginWidget> _loginWidgetRepository;
        private readonly IOxideRepository<PasswordRecoveryItem> _passwordRecoveryRepository;
        private readonly IOxideRepository<PasswordRecoveryWidget> _passwordRecoveryWidgetRepository;
        private readonly IOxideRepository<UserConfirmation> _userConfirmationRepository;
        private readonly IOxideRepository<UserRegistirationActivatorWidget> _userRegistirationActivatorWidgetRepository;

        private readonly IOxideRepository<UserRegistirationEmailInformerWidget>
            _userRegistirationEmailInformerWidgetRepository;

        private readonly IOxideRepository<UserRegistirationWidget> _userRegistirationWidgetRepository;
        private readonly IOxideRepository<User> _userRepository;

        public UserService(IOxideRepository<User> userRepository,
            IOxideRepository<UserRegistirationWidget> userRegistirationWidgetRepository,
            IOxideRepository<UserConfirmation> userConfirmationRepository,
            IOxideRepository<UserRegistirationEmailInformerWidget> userRegistirationEmailInformerWidgetRepository,
            IOxideRepository<UserRegistirationActivatorWidget> userRegistirationActivatorWidgetRepository,
            IOxideRepository<LoginValidatorWidget> loginValidatorWidgetRepository,
            IOxideRepository<LoginWidget> loginWidgetRepository,
            IOxideRepository<PasswordRecoveryItem> passwordRecoveryRepository,
            IOxideRepository<PasswordRecoveryWidget> passwordRecoveryWidgetRepositor,
            IOxideRepository<LoginSetting> loginSettingRepository, IOxideRepository<File> fileRepository)
        {
            _userRepository = userRepository;
            _userRegistirationWidgetRepository = userRegistirationWidgetRepository;
            _userConfirmationRepository = userConfirmationRepository;
            _userRegistirationEmailInformerWidgetRepository = userRegistirationEmailInformerWidgetRepository;
            _userRegistirationActivatorWidgetRepository = userRegistirationActivatorWidgetRepository;
            _loginValidatorWidgetRepository = loginValidatorWidgetRepository;
            _loginWidgetRepository = loginWidgetRepository;
            _passwordRecoveryRepository = passwordRecoveryRepository;
            _passwordRecoveryWidgetRepository = passwordRecoveryWidgetRepositor;
            _loginSettingRepository = loginSettingRepository;
            _fileRepository = fileRepository;
        }

        #region Users

        public int CreateUser(User user)
        {
            var newUser = new User();
            newUser = user.CopyPropertiesTo(newUser);
            newUser.CreateDateTime = DateTime.Now;
            newUser.LastAccessDateTime = DateTime.Now;
            newUser.IsEmailConfirmed = false;
            newUser.Password = OxideUtils.ToSha256(user.Password);
            newUser.Email = user.Email;
            newUser.Name = user.Name;
            newUser.Surname = user.Surname;
            newUser.UserName = user.UserName;
            _userRepository.Insert(newUser);
            return newUser.Id;
        }

        public void UpdateUser(UserViewModel user)
        {
            var updateMode = _userRepository.SearchFor(x => x.Id == user.Id).FirstOrDefault();
            if (updateMode != null)
            {
                updateMode.IsEmailConfirmed = false;
                updateMode.Password = OxideUtils.ToSha256(user.Password);
                updateMode.Email = user.Email;
                updateMode.Name = user.Name;
                updateMode.Surname = user.Surname;
                updateMode.UsrImageId = Convert.ToInt32(user.UserImageId);
                _userRepository.Update(updateMode);
            }
        }

        public void UpdateProfile(int userId, string userName, string name, string surname, string email)
        {
            var updateMode = _userRepository.SearchFor(x => x.Id == userId).FirstOrDefault();
            if (updateMode != null)
            {
                updateMode.UserName = userName;
                updateMode.Name = name;
                updateMode.Surname = surname;
                updateMode.Email = email;
                _userRepository.Update(updateMode);
            }
        }

        public void UpdateUserPassword(int userId, string password)
        {
            var updateMode = _userRepository.SearchFor(x => x.Id == userId).FirstOrDefault();
            if (updateMode != null)
            {
                updateMode.Password = OxideUtils.ToSha256(password);
                _userRepository.Update(updateMode);
            }
        }

        public void UpdateUseProfileImage(int userdId, int imageId)
        {
            var updateMode = _userRepository.SearchFor(x => x.Id == userdId).FirstOrDefault();
            if (updateMode == null) return;
            updateMode.UsrImageId = imageId;
            if (updateMode.UsrImageId != 0)
                updateMode.UserImage = _fileRepository.SearchFor(x => x.Id == updateMode.UsrImageId).FirstOrDefault();
            _userRepository.Update(updateMode);
        }

        public void ActivateUser(int id)
        {
            var updateMode = _userRepository.SearchFor(x => x.Id == id).FirstOrDefault();
            if (updateMode != null)
            {
                updateMode.IsEmailConfirmed = true;
                updateMode.Activated = true;
                _userRepository.Update(updateMode);
            }
        }

        public void DeActivateUser(int id)
        {
            var updateMode = _userRepository.SearchFor(x => x.Id == id).FirstOrDefault();
            if (updateMode != null)
            {
                updateMode.IsEmailConfirmed = false;
                _userRepository.Update(updateMode);
            }
        }

        public void DeleteUser(User model)
        {
            _userRepository.Delete(model);
        }

        public void DeleteUser(int id)
        {
            _userRepository.Delete(id);
        }

        public User GetUser(int id)
        {
            return _userRepository.GetById(id, x => x.UserImage);
        }

        public User GetUserByUserName(string userName)
        {
            return _userRepository.SearchFor(x => x.UserName == userName, x => x.UserImage).FirstOrDefault();
        }

        public User GetUserByUserEmail(string email)
        {
            return _userRepository.SearchFor(x => x.Email == email, x => x.UserImage).FirstOrDefault();
        }

        public bool IsUserExist(string userName)
        {
            return _userRepository.Table.Any(x => x.UserName == userName);
        }

        public bool IsEmailUsed(string email)
        {
            return _userRepository.Table.Any(x => x.Email == email);
        }

        public void UpdateLastAccessDateTime(string userName)
        {
            var user = _userRepository.SearchFor(x => x.UserName == userName).FirstOrDefault();
            if (user != null)
            {
                user.LastAccessDateTime = DateTime.Now;
                _userRepository.Update(user);
            }
        }

        public void UpdateLoginKey(string key, int userId)
        {
            var user = _userRepository.SearchFor(x => x.Id == userId).FirstOrDefault();
            if (user != null)
            {
                user.LoginKey = key;
                user.LastAccessDateTime = DateTime.Now;
                _userRepository.Update(user);
            }
        }

        public bool IsKeyValid(int userId, string key)
        {
            var user = _userRepository.SearchFor(x => x.Id == userId).FirstOrDefault();
            if (user != null)
                return user.LoginKey == key;
            return false;
        }

        public List<User> GetAllUsers()
        {
            return _userRepository.GetAll(x => x.UserImage).ToList();
        }

        public UserFilterViewModel GetUsersWithFilter(int pageSize, int page, string filter)
        {
            var pageFilterViewModel = new UserFilterViewModel();
            var includeProperties = new List<Expression<Func<User, object>>> {x => x.UserImage};
            Expression<Func<User, bool>> criteriaFilter =
                x =>
                    x.Name.ToLower().Contains(filter.ToLower()) || x.Title.ToLower().Contains(filter.ToLower()) ||
                    x.UserName.ToLower().Contains(filter.ToLower());
            if (filter != string.Empty)
                pageFilterViewModel.Records =
                    GetAdminUserViewModelList(_userRepository.Get(criteriaFilter, o => o.OrderBy(x => x.Id),
                        includeProperties, page, pageSize));
            else
                pageFilterViewModel.Records =
                    GetAdminUserViewModelList(_userRepository.Get(null, o => o.OrderBy(x => x.Id), includeProperties,
                        page, pageSize));
            pageFilterViewModel.CurrentPage = page;
            pageFilterViewModel.DisplayingRecords = pageFilterViewModel.Records.Count();
            pageFilterViewModel.TotalRecords = _userRepository.TotalRecords();
            if (filter != string.Empty)
                pageFilterViewModel.TotalPages = (_userRepository.Get(criteriaFilter).Count() + pageSize - 1)/pageSize;
            else
                pageFilterViewModel.TotalPages = (pageFilterViewModel.TotalRecords + pageSize - 1)/pageSize;
            pageFilterViewModel.NextEnable = pageFilterViewModel.CurrentPage < pageFilterViewModel.TotalPages - 1;
            pageFilterViewModel.PreviousEnable = pageFilterViewModel.CurrentPage > 0;
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            pageFilterViewModel.ChangeStatusUrl = urlHelper.Action(MVC.Admin.AdminUser.ActionNames.DeleteAdminUser,
                MVC.Admin.AdminUser.Name, new {area = "Oxide.Extra"});
            return pageFilterViewModel;
        }

        private static ICollection<UserViewModel> GetAdminUserViewModelList(IEnumerable<User> userList)
        {
            return
                (from user in userList
                 let userViewModel = new UserViewModel()
                 select user.CopyPropertiesTo(userViewModel)).ToList();
        }

        #endregion

        #region Settings

        public int CreateSetting(LoginSetting setting)
        {
            var loginSetting = new LoginSetting();
            loginSetting = setting.CopyPropertiesTo(loginSetting);
            loginSetting.Culture = setting.Culture;
            _loginSettingRepository.Insert(loginSetting);
            return loginSetting.Id;
        }

        public void UpdateSetting(LoginSettingEditorViewModel setting)
        {
            var updateMode = _loginSettingRepository.SearchFor(x => x.Id == setting.Id).FirstOrDefault();
            if (updateMode != null)
            {
                setting.CopyPropertiesTo(updateMode, new[] {"Id"});
                updateMode.Culture = setting.Culture;
                _loginSettingRepository.Update(updateMode);
            }
        }

        public void DeleteSetting(LoginSetting setting)
        {
            _loginSettingRepository.Delete(setting);
        }

        public void DeleteSetting(int id)
        {
            _loginSettingRepository.Delete(id);
        }

        public LoginSetting GetSetting(int id)
        {
            return _loginSettingRepository.GetById(id);
        }

        public LoginSetting GetSettingByCulture(int cultureId)
        {
            return _loginSettingRepository.SearchFor(x => x.Culture.Id == cultureId).FirstOrDefault();
        }

        public LoginSetting GetSettingByCulture(string cultureCode)
        {
            return _loginSettingRepository.SearchFor(x => x.Culture.Code3 == cultureCode).FirstOrDefault();
        }

        public List<LoginSetting> GetAllSettings()
        {
            return _loginSettingRepository.GetAll().ToList();
        }

        public FilterModel<LoginSettingEditorViewModel> GetSettingsWithFilter(int pageSize, int page, string filter)
        {
            var pageFilterViewModel = new FilterModel<LoginSettingEditorViewModel>();
            if (filter != string.Empty)
                pageFilterViewModel.Records =
                    GetAdminSettingViewModelList(
                        _loginSettingRepository.GetAll()
                                               .OrderBy(x => x.Title)
                                               .Where(
                                                   (x =>
                                                       x.Name.ToLower().Contains(filter.ToLower()) ||
                                                       x.Title.ToLower().Contains(filter.ToLower())))
                                               .Skip(pageSize*page)
                                               .Take(pageSize)
                                               .ToList());
            else
                pageFilterViewModel.Records =
                    GetAdminSettingViewModelList(
                        _loginSettingRepository.GetAll()
                                               .OrderBy(x => x.Title)
                                               .Skip(pageSize*page)
                                               .Take(pageSize)
                                               .ToList());
            pageFilterViewModel.CurrentPage = page;
            pageFilterViewModel.DisplayingRecords = pageFilterViewModel.Records.Count();
            pageFilterViewModel.TotalRecords = _userRepository.GetAll().Count();
            if (filter != string.Empty)
                pageFilterViewModel.TotalPages =
                    (_userRepository.GetAll()
                                    .OrderBy(x => x.UserName)
                                    .Count(x => x.Name.Contains(filter) || x.Title.Contains(filter)) + pageSize - 1)/
                    pageSize;
            else
                pageFilterViewModel.TotalPages = (pageFilterViewModel.TotalRecords + pageSize - 1)/pageSize;
            pageFilterViewModel.NextEnable = pageFilterViewModel.CurrentPage < pageFilterViewModel.TotalPages - 1;
            pageFilterViewModel.PreviousEnable = pageFilterViewModel.CurrentPage > 0;
            return pageFilterViewModel;
        }

        private static ICollection<LoginSettingEditorViewModel> GetAdminSettingViewModelList(
            IEnumerable<LoginSetting> userList)
        {
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            return (from loginSetting in userList
                    let settingAdminViewModel = new LoginSettingEditorViewModel()
                    select loginSetting.CopyPropertiesTo(settingAdminViewModel)).ToList();
        }

        #endregion

        #region Widgets

        public bool SaveUserRegistirationWidget(UserRegistirationWidget userRegistirationWidget)
        {
            try
            {
                var updateMode =
                    _userRegistirationWidgetRepository.SearchFor(x => x.Key == userRegistirationWidget.Key)
                                                      .FirstOrDefault();
                if (updateMode == null)
                    _userRegistirationWidgetRepository.Insert(userRegistirationWidget);
                else
                {
                    updateMode.ImageId = userRegistirationWidget.ImageId;
                    updateMode.Name = userRegistirationWidget.Name;
                    updateMode.Title = userRegistirationWidget.Title;
                    updateMode.License = userRegistirationWidget.License;
                    updateMode.ShowLicense = userRegistirationWidget.ShowLicense;
                    updateMode.SendConfirmationEmail = userRegistirationWidget.SendConfirmationEmail;
                    updateMode.ConfirmationPageId = userRegistirationWidget.ConfirmationPageId;
                    updateMode.ConfirmationEmailId = userRegistirationWidget.ConfirmationEmailId;
                    updateMode.ConfirmationEmailSentPageId = userRegistirationWidget.ConfirmationEmailSentPageId;
                    updateMode.LoginPageId = userRegistirationWidget.LoginPageId;
                    updateMode.ValidDays = userRegistirationWidget.ValidDays;
                    _userRegistirationWidgetRepository.Update(updateMode);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void DeleteUserRegistirationWidget(int key)
        {
            _userRegistirationWidgetRepository.DeleteAll(x => x.Key == key);
        }

        public UserRegistirationWidget GetUserRegistirationWidget(int key)
        {
            var widget = _userRegistirationWidgetRepository.Table.FirstOrDefault(x => x.Key == key);
            return widget;
        }

        public bool SaveUserRegistirationEmailInformerWidget(
            UserRegistirationEmailInformerWidget userRegistirationWidget)
        {
            try
            {
                var updateMode =
                    _userRegistirationEmailInformerWidgetRepository.SearchFor(x => x.Key == userRegistirationWidget.Key)
                                                                   .FirstOrDefault();
                if (updateMode == null)
                    _userRegistirationEmailInformerWidgetRepository.Insert(userRegistirationWidget);
                else
                {
                    updateMode.ImageId = userRegistirationWidget.ImageId;
                    updateMode.Name = userRegistirationWidget.Name;
                    updateMode.Title = userRegistirationWidget.Title;
                    updateMode.Message = userRegistirationWidget.Message;
                    _userRegistirationEmailInformerWidgetRepository.Update(updateMode);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void DeleteUserRegistirationEmailInformerWidget(int key)
        {
            _userRegistirationEmailInformerWidgetRepository.DeleteAll(x => x.Key == key);
        }

        public UserRegistirationEmailInformerWidget GetUserRegistirationEmailInformerWidget(int key)
        {
            var widget = _userRegistirationEmailInformerWidgetRepository.Table.FirstOrDefault(x => x.Key == key);
            return widget;
        }

        public bool SaveUserRegistirationActivatorWidget(
            UserRegistirationActivatorWidget userRegistirationActivatorWidget)
        {
            try
            {
                var updateMode =
                    _userRegistirationActivatorWidgetRepository.SearchFor(
                        x => x.Key == userRegistirationActivatorWidget.Key).FirstOrDefault();
                if (updateMode == null)
                    _userRegistirationActivatorWidgetRepository.Insert(userRegistirationActivatorWidget);
                else
                {
                    updateMode.ImageId = userRegistirationActivatorWidget.ImageId;
                    updateMode.Name = userRegistirationActivatorWidget.Name;
                    updateMode.Title = userRegistirationActivatorWidget.Title;
                    updateMode.Message = userRegistirationActivatorWidget.Message;
                    _userRegistirationActivatorWidgetRepository.Update(updateMode);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void DeleteUserRegistirationActivatorWidget(int key)
        {
            _userRegistirationActivatorWidgetRepository.DeleteAll(x => x.Key == key);
        }

        public UserRegistirationActivatorWidget GetUserRegistirationActivatorWidget(int key)
        {
            var widget = _userRegistirationActivatorWidgetRepository.Table.FirstOrDefault(x => x.Key == key);
            return widget;
        }

        public bool SaveLoginValidatorWidget(LoginValidatorWidget loginValidatorWidget)
        {
            try
            {
                var updateMode =
                    _loginValidatorWidgetRepository.SearchFor(x => x.Key == loginValidatorWidget.Key).FirstOrDefault();
                if (updateMode == null)
                    _loginValidatorWidgetRepository.Insert(loginValidatorWidget);
                else
                {
                    updateMode.Name = loginValidatorWidget.Name;
                    updateMode.Title = loginValidatorWidget.Title;
                    updateMode.LoginPageId = loginValidatorWidget.LoginPageId;
                    _loginValidatorWidgetRepository.Update(updateMode);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void DeleteLoginValidatorWidget(int key)
        {
            _loginValidatorWidgetRepository.DeleteAll(x => x.Key == key);
        }

        public LoginValidatorWidget GetLoginValidatorWidget(int key)
        {
            var widget = _loginValidatorWidgetRepository.Table.FirstOrDefault(x => x.Key == key);
            return widget;
        }

        public bool SaveLoginWidget(LoginWidget loginWidget)
        {
            try
            {
                var updateMode = _loginWidgetRepository.SearchFor(x => x.Key == loginWidget.Key).FirstOrDefault();
                if (updateMode == null)
                    _loginWidgetRepository.Insert(loginWidget);
                else
                {
                    updateMode.Name = loginWidget.Name;
                    updateMode.Title = loginWidget.Title;
                    updateMode.ImageId = loginWidget.ImageId;
                    updateMode.PasswordResetPageId = loginWidget.PasswordResetPageId;
                    _loginWidgetRepository.Update(updateMode);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void DeleteLoginWidget(int key)
        {
            _loginWidgetRepository.DeleteAll(x => x.Key == key);
        }

        public LoginWidget GetLoginWidget(int key)
        {
            var widget = _loginWidgetRepository.Table.FirstOrDefault(x => x.Key == key);
            return widget;
        }

        public bool SavePasswordRecoveryWidget(PasswordRecoveryWidget recoveryWidget)
        {
            try
            {
                var updateMode =
                    _passwordRecoveryWidgetRepository.SearchFor(x => x.Key == recoveryWidget.Key).FirstOrDefault();
                if (updateMode == null)
                    _passwordRecoveryWidgetRepository.Insert(recoveryWidget);
                else
                {
                    updateMode.Name = recoveryWidget.Name;
                    updateMode.Title = recoveryWidget.Title;
                    updateMode.EmailId = recoveryWidget.EmailId;
                    updateMode.ValidMinutes = recoveryWidget.ValidMinutes;
                    _passwordRecoveryWidgetRepository.Update(updateMode);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void DeletePasswordRecoveryWidget(int key)
        {
            _passwordRecoveryWidgetRepository.DeleteAll(x => x.Key == key);
        }

        public PasswordRecoveryWidget GetPasswordRecoveryWidget(int key)
        {
            var widget = _passwordRecoveryWidgetRepository.Table.FirstOrDefault(x => x.Key == key);
            return widget;
        }

        public string CreateConfirmation(int userId, int validDays, int hash)
        {
            //Delete if there is any garbage record with this user id
            _userConfirmationRepository.DeleteAll(x => x.UserId == userId);
            var key = OxideUtils.GetEncryptedQueryString(HttpUtility.UrlEncode(OxideUtils.CreateKey(KeyLength)));
            var userConfirmation = new UserConfirmation
            {
                UserId = userId,
                ConfirmationSent = DateTime.Now,
                ValidDays = validDays,
                WidgetHash = hash,
                Key = key
            };
            _userConfirmationRepository.Insert(userConfirmation);
            return key;
        }

        public UserConfirmation GetConfirmation(int userId)
        {
            return _userConfirmationRepository.SearchFor(x => x.UserId == userId).FirstOrDefault();
        }

        public UserConfirmation GetConfirmation(string key)
        {
            return _userConfirmationRepository.SearchFor(x => x.Key == key).FirstOrDefault();
        }

        public void DeleteConfirmation(int id)
        {
            _userConfirmationRepository.Delete(id);
        }

        public string CreatePasswordRecoveryRecord(int userId, int validMinutes)
        {
            //Delete if there is any garbage record with this user id
            _passwordRecoveryRepository.DeleteAll(x => x.UserId == userId);
            var key = OxideUtils.CreateKey(KeyLength);
            var passwordRecovery = new PasswordRecoveryItem
            {
                UserId = userId,
                RecoveryCode = key,
                ValidMinutes = validMinutes,
                CreataionDate = DateTime.Now
            };
            _passwordRecoveryRepository.Insert(passwordRecovery);
            return key;
        }

        public PasswordRecoveryItem GetPasswordRevocery(int userId)
        {
            return _passwordRecoveryRepository.SearchFor(x => x.UserId == userId).FirstOrDefault();
        }

        public void DeletePasswordRecovery(int userId)
        {
            _passwordRecoveryRepository.DeleteAll(x => x.UserId == userId);
        }

        public bool IsRecoveryCodeValid(int userId, string recoveryCode)
        {
            var baseRecoveryCode = _passwordRecoveryRepository.SearchFor(x => x.UserId == userId).FirstOrDefault();
            if (baseRecoveryCode != null)
            {
                if (DateTime.Now > baseRecoveryCode.CreataionDate.AddMinutes(baseRecoveryCode.ValidMinutes))
                    return false;
                return baseRecoveryCode.RecoveryCode == recoveryCode;
            }
            return false;
        }

        #endregion
    }
}