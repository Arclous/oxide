﻿using System;
using System.Linq;
using Oxide.Core.Data.Repository;
using Oxide.Core.Extesions;
using Oxide.Extra.Models.Timeline;
using Oxide.Extra.Services.Interface;

namespace Oxide.Extra.Services.Implementation
{
    public class TimelineWidgetService : ITimelineWidgetService
    {
        private readonly IOxideRepository<TimelineItem> _timelineItemsRepository;
        private readonly IOxideRepository<TimelineWidgetItem> _timelineWidgetItemRepository;
        private readonly IOxideRepository<TimelineWidget> _timelineWidgetRepository;

        public TimelineWidgetService(IOxideRepository<TimelineItem> timelineItemsRepository,
            IOxideRepository<TimelineWidget> timelineWidgetRepository,
            IOxideRepository<TimelineWidgetItem> timelineWidgetItemRepository)
        {
            _timelineItemsRepository = timelineItemsRepository;
            _timelineWidgetRepository = timelineWidgetRepository;
            _timelineWidgetItemRepository = timelineWidgetItemRepository;
        }

        public bool CreateTimelineItem(TimelineItem timelineItem)
        {
            try
            {
                _timelineItemsRepository.Insert(timelineItem);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateTimelineItem(TimelineItem model)
        {
            try
            {
                var updateMode = _timelineItemsRepository.SearchFor(x => x.Id == model.Id).FirstOrDefault();
                model.CopyPropertiesTo(updateMode, new[] {"Id"});
                _timelineItemsRepository.Update(model);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void DeleteTimelineItem(TimelineItem model)
        {
            _timelineItemsRepository.Delete(model);
        }

        public void DeleteTimelineItem(int id)
        {
            _timelineItemsRepository.Delete(id);
        }

        public TimelineItem GetTimelineItem(int id)
        {
            return _timelineItemsRepository.GetById(id);
        }

        public bool SaveWidget(TimelineWidget timelineWidget)
        {
            try
            {
                var updateMode = _timelineWidgetRepository.SearchFor(x => x.Key == timelineWidget.Key).FirstOrDefault();
                if (updateMode == null)
                {
                    _timelineWidgetRepository.Insert(timelineWidget);
                }
                else
                {
                    //Clean assigned members if exist
                    _timelineWidgetItemRepository.BulkDeleteAll(x => x.TimelineWidgetId == updateMode.Id);
                    updateMode.TimelineItems = timelineWidget.TimelineItems;
                    updateMode.Name = timelineWidget.Name;
                    updateMode.Title = timelineWidget.Title;
                    _timelineWidgetRepository.Update(updateMode);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void DeleteWidget(int key)
        {
            var widget = _timelineWidgetRepository.Get(x => x.Key == key).FirstOrDefault();
            _timelineWidgetItemRepository.DeleteAll(x => x.TimelineWidgetId == widget.Id);
            _timelineWidgetRepository.DeleteAll(x => x.Key == key);
        }

        public TimelineWidget GetWidget(int key)
        {
            var widget = _timelineWidgetRepository.Table.FirstOrDefault(x => x.Key == key);
            if (widget != null)
            {
                widget.TimelineItems =
                    _timelineWidgetItemRepository.Table.Where(x => x.TimelineWidgetId == widget.Id).ToList();
            }
            return widget;
        }
    }
}