﻿using System;
using System.Collections.Generic;
using System.Linq;
using Oxide.Core.Data.Repository;
using Oxide.Core.Extesions;
using Oxide.Core.ViewModels.FilterViewModels;
using Oxide.Extra.Keywords;
using Oxide.Extra.Models.Comments;
using Oxide.Extra.Services.Interface;
using Oxide.Extra.ViewModels.Comments;

namespace Oxide.Extra.Services.Implementation
{
    public class CommentsService : ICommentsService
    {
        private readonly IOxideRepository<Comment> _commentsRepository;
        private readonly IOxideRepository<CommentsSettings> _commentsSettingsRepository;
        private readonly ILoginService _loginService;
        private readonly IUserService _userService;

        public CommentsService(IOxideRepository<CommentsSettings> commentsSettingsRepository,
            IOxideRepository<Comment> commentsRepository, IUserService userService, ILoginService loginService)
        {
            _commentsSettingsRepository = commentsSettingsRepository;
            _commentsRepository = commentsRepository;
            _userService = userService;
            _loginService = loginService;
        }

        #region WidgetSettings

        public bool SaveWidget(CommentsEditorViewModel commentsWidgetViewModel)
        {
            var commentsWidgetSetting =
                _commentsSettingsRepository.SearchFor(x => x.Key == commentsWidgetViewModel.Key).FirstOrDefault();
            if (commentsWidgetSetting != null)
            {
                commentsWidgetViewModel.CopyPropertiesTo(commentsWidgetSetting, new[] {"Id"});
                _commentsSettingsRepository.Update(commentsWidgetSetting);
                return true;
            }
            var newComments = new CommentsSettings();
            commentsWidgetViewModel.CopyPropertiesTo(newComments);
            newComments.Key = commentsWidgetViewModel.Key;
            _commentsSettingsRepository.Insert(newComments);
            return true;
        }

        public CommentsEditorViewModel GetWidget(int key)
        {
            var widget = _commentsSettingsRepository.Table.FirstOrDefault(x => x.Key == key);
            var commentsEditorView = new CommentsEditorViewModel();
            widget?.CopyPropertiesTo(commentsEditorView);
            return commentsEditorView;
        }

        public void DeleteWidget(int key)
        {
            _commentsSettingsRepository.DeleteAll(x => x.Key == key);
        }

        public CommentsViewModel SaveComment(string message, int key)
        {
            var loginedUser = _loginService.GetUserInfo();
            var user = _userService.GetUser(loginedUser.Id);
            var widgetSettings = GetWidget(key);
            var newComment = new Comment
            {
                Message = message,
                Key = key,
                User = user,
                CommentDateTime = DateTime.Now,
                CommentStatus =
                    widgetSettings.ApproveRequired ? Keys.CommentStatus.WaitingForApprove : Keys.CommentStatus.Approved
            };
            _commentsRepository.Insert(newComment);
            var viewModel = new CommentsViewModel();
            newComment.CopyPropertiesTo(viewModel);
            return viewModel;
        }

        public void UpdateComment(string message, int key, int id)
        {
            var widgetSettings = GetWidget(key);
            var comment = GetComment(id);
            if (comment != null && _loginService.GetUserInfo().Id == comment.User.Id)
            {
                comment.Message = message;
                comment.CommentStatus = widgetSettings.ApproveRequired
                    ? Keys.CommentStatus.WaitingForApprove
                    : Keys.CommentStatus.Approved;
                _commentsRepository.Update(comment);
            }
        }

        public Comment GetComment(int id)
        {
            return _commentsRepository.GetById(id);
        }

        public FilterModel<CommentsViewModel> GetCommentsPages(int pageSize, int page, int key)
        {
            var filterViewModel = new FilterModel<CommentsViewModel>();

            //var commentsList = (from comments in _commentsRepository.Table
            //                    from users in _userRepository.Table.Where(user => comments.User == user.Id).DefaultIfEmpty()
            //                    from files in _fileRepository.Table.Where(file => file.Id == users.UserImageId).DefaultIfEmpty()
            //                    select new { comments, users.UserName, files.Path }).OrderByDescending(x => x.comments.CommentDateTime).Skip(pageSize * page).Take(pageSize).ToList();
            var commentsList =
                _commentsRepository.SearchFor(k => k.Key == key, x => x.User, p => p.User.UserImage).ToList();
            if (commentsList.Count == 00) return null;
            var commentViewModelList = GetCommentViewModelList(commentsList).ToList();
            filterViewModel.Records = commentViewModelList;
            filterViewModel.CurrentPage = page;
            filterViewModel.DisplayingRecords = commentViewModelList.Count();
            filterViewModel.TotalRecords = commentViewModelList.Count();
            filterViewModel.TotalPages = (filterViewModel.TotalRecords + pageSize - 1)/pageSize;
            filterViewModel.NextEnable = filterViewModel.CurrentPage < filterViewModel.TotalPages - 1;
            filterViewModel.PreviousEnable = filterViewModel.CurrentPage > 0;
            return filterViewModel;
        }

        public void DeleteComment(int id)
        {
            _commentsRepository.DeleteAll(x => x.Id == id && x.User.Id == _loginService.GetUserInfo().Id);
        }

        public ICollection<CommentsViewModel> GetCommentViewModelList(IEnumerable<Comment> records)
        {
            var currentUser = _loginService.GetUserInfo();
            if (currentUser != null)
            {
                var currentUserId = currentUser.Id;
                return
                    records.Select(
                        item =>
                            new CommentsViewModel
                            {
                                Id = item.Id,
                                Title = item.Title,
                                Name = item.Name,
                                UserName = item.User.UserName,
                                UserImage = item.User.UserImage != null ? item.User.UserImage.Path : "",
                                CommentDateTime = item.CommentDateTime,
                                Deletable = currentUserId == item.User.Id,
                                Message = item.Message
                            }).ToList();
            }
            return null;
        }

        #endregion
    }
}