﻿using System;
using System.Collections.Generic;
using System.Web;
using Oxide.Core.Helpers;
using Oxide.Core.Services.OxideUiService;
using Oxide.Core.Syntax;
using Oxide.Extra.Keywords;
using Oxide.Extra.Models.User;
using Oxide.Extra.Services.Interface;
using Oxide.Extra.ViewModels.User;

namespace Oxide.Extra.Services.Implementation
{
    public class LoginService : ILoginService
    {
        private readonly IOxideUiService _oxideUiService;
        private readonly IUserService _userService;

        public LoginService(IUserService userService, IOxideUiService oxideUiService)
        {
            _userService = userService;
            _oxideUiService = oxideUiService;
        }

        public Keys.LoginResult LoginUser(string userEmail, string password)
        {
            var user = _userService.GetUserByUserEmail(userEmail);
            if (user == null) return Keys.LoginResult.UserNotFound;
            if (IsPasswordMatch(user, password))
            {
                var key = GetKeyForUser(user.Id);
                var userImage = _oxideUiService.GetContent(Constants.FileContent, user.UsrImageId.ToString());
                var userImageUrl = userImage != null ? ((dynamic) userImage).Path : "";
                var dictionary = new Dictionary<string, string>
                {
                    {"k", OxideUtils.GetEncryptedQueryString(key)},
                    {"d", OxideUtils.GetEncryptedQueryString(user.Id.ToString())},
                    {"usrN", OxideUtils.GetEncryptedQueryString(user.UserName)},
                    {"usr", OxideUtils.GetEncryptedQueryString(user.Email)},
                    {"usrI", OxideUtils.GetEncryptedQueryString(user.UsrImageId.ToString())},
                    {"usrIu", OxideUtils.GetEncryptedQueryString(OxideUtils.GetFullUrl(userImageUrl.ToString()))}
                };
                var lgnCookie = new HttpCookie("lgn") {HttpOnly = true, Expires = DateTime.Now.AddHours(1)};
                foreach (var val in dictionary)
                    lgnCookie[val.Key] = val.Value;
                HttpContext.Current.Response.Cookies.Add(lgnCookie);
                return Keys.LoginResult.Success;
            }
            return Keys.LoginResult.NotSuccess;
        }

        public void SignOut()
        {
            if (HttpContext.Current.Request.Cookies["lgn"] == null) return;
            var c = new HttpCookie("lgn") {Expires = DateTime.Now.AddDays(-1)};
            HttpContext.Current.Response.Cookies.Add(c);
        }

        public bool IsUserLogined()
        {
            var values = GetValues();
            if (values.Count == 0) return false;
            return _userService.IsKeyValid(Convert.ToInt32(OxideUtils.GetDecryptedQueryString(values["d"])),
                OxideUtils.GetDecryptedQueryString(values["k"]));
        }

        public void UpdateAccessDateTime(string userName)
        {
            _userService.UpdateLastAccessDateTime(userName);
        }

        public UserViewModel GetUserInfo()
        {
            var values = GetValues();
            if (values.Count == 0) return null;
            var userInfo = new UserViewModel();
            var userId = 0;
            var userName = "";
            var userEmail = "";
            var userImageId = "";
            var userImageUrl = "";
            if (values["d"] != null) userId = Convert.ToInt32(OxideUtils.GetDecryptedQueryString(values["d"]));
            if (values["usrN"] != null) userName = OxideUtils.GetDecryptedQueryString(values["usrN"]);
            if (values["usr"] != null) userEmail = OxideUtils.GetDecryptedQueryString(values["usr"]);
            if (values["usrI"] != null) userImageId = OxideUtils.GetDecryptedQueryString(values["usrI"]);
            if (values["usrIu"] != null) userImageUrl = OxideUtils.GetDecryptedQueryString(values["usrIu"]);
            userInfo.Id = userId;
            userInfo.UserName = userName;
            userInfo.Email = userEmail;
            userInfo.UserImageId = Convert.ToInt32(userImageId);
            userInfo.UserImageUrl = userImageUrl;
            return userInfo;
        }

        public void UpdateUserInfo(UserViewModel userViewModel)
        {
            var key = GetKeyForUser(userViewModel.Id);
            var userImage = _oxideUiService.GetContent(Constants.FileContent, userViewModel.UserImageId.ToString());
            var userImageUrl = userImage != null ? ((dynamic) userImage).Path : "";
            var dictionary = new Dictionary<string, string>
            {
                {"k", OxideUtils.GetEncryptedQueryString(key)},
                {"d", OxideUtils.GetEncryptedQueryString(userViewModel.Id.ToString())},
                {"usrN", OxideUtils.GetEncryptedQueryString(userViewModel.UserName)},
                {"usr", OxideUtils.GetEncryptedQueryString(userViewModel.Email)},
                {"usrI", OxideUtils.GetEncryptedQueryString(userViewModel.UserImageId.ToString())},
                {"usrIu", OxideUtils.GetEncryptedQueryString(OxideUtils.GetFullUrl(userImageUrl.ToString()))}
            };
            var lgnCookie = new HttpCookie("lgn") {HttpOnly = true, Expires = DateTime.Now.AddHours(1)};
            foreach (var val in dictionary)
                lgnCookie[val.Key] = val.Value;
            HttpContext.Current.Response.Cookies.Add(lgnCookie);
        }

        public string GetKeyForUser(int userId)
        {
            var key = OxideUtils.CreateKey(20);
            _userService.UpdateLoginKey(key, userId);
            return key;
        }

        private static bool IsPasswordMatch(User user, string password)
        {
            return user.Password == OxideUtils.ToSha256(password);
        }

        private static Dictionary<string, string> GetValues()
        {
            var dictionary = new Dictionary<string, string>();
            if (HttpContext.Current.Request.Cookies["lgn"] == null) return dictionary;
            var cookie = HttpContext.Current.Request.Cookies["lgn"];
            if (cookie == null) return dictionary;
            var nameValueCollection = cookie.Values;
            foreach (var key in nameValueCollection.AllKeys)
            {
                dictionary.Add(key, cookie[key]);
            }
            return dictionary;
        }
    }
}