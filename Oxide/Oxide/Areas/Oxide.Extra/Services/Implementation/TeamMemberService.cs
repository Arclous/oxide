﻿using System;
using System.Linq;
using Oxide.Core.Data.Repository;
using Oxide.Core.Extesions;
using Oxide.Extra.Models.Team;
using Oxide.Fundamentals.Services.Interface;

namespace Oxide.Extra.Services.Implementation
{
    public class TeamMemberService : ITeamMemberService
    {
        private readonly IOxideRepository<TeamMemberItem> _teamMemberItemsRepository;
        private readonly IOxideRepository<TeamWidgetItem> _teamWidgetItemRepository;
        private readonly IOxideRepository<TeamWidget> _teamWidgetRepository;

        public TeamMemberService(IOxideRepository<TeamMemberItem> teamMemberItemsRepository,
            IOxideRepository<TeamWidget> teamWidgetRepository, IOxideRepository<TeamWidgetItem> teamWidgetItemRepository)
        {
            _teamMemberItemsRepository = teamMemberItemsRepository;
            _teamWidgetRepository = teamWidgetRepository;
            _teamWidgetItemRepository = teamWidgetItemRepository;
        }

        public bool CreateTeamMember(TeamMemberItem teamMemberItem)
        {
            try
            {
                _teamMemberItemsRepository.Insert(teamMemberItem);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateTeamMember(TeamMemberItem model)
        {
            try
            {
                var updateMode = _teamMemberItemsRepository.SearchFor(x => x.Id == model.Id).FirstOrDefault();
                model.CopyPropertiesTo(updateMode, new[] {"Id"});
                _teamMemberItemsRepository.Update(model);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void DeleteTeamMember(TeamMemberItem model)
        {
            _teamMemberItemsRepository.Delete(model);
        }

        public void DeleteTeamMember(int id)
        {
            _teamMemberItemsRepository.Delete(id);
        }

        public TeamMemberItem GetTeamMember(int id)
        {
            return _teamMemberItemsRepository.GetById(id);
        }

        public bool SaveWidget(TeamWidget teamWidget)
        {
            try
            {
                var updateMode = _teamWidgetRepository.SearchFor(x => x.Key == teamWidget.Key).FirstOrDefault();
                if (updateMode == null)
                {
                    _teamWidgetRepository.Insert(teamWidget);
                }
                else
                {
                    //Clean assigned members if exist
                    _teamWidgetItemRepository.BulkDeleteAll(x => x.TeamWidgetId == updateMode.Id);
                    updateMode.Type = teamWidget.Type;
                    updateMode.TeamMemberItems = teamWidget.TeamMemberItems;
                    updateMode.Name = teamWidget.Name;
                    updateMode.Title = teamWidget.Title;
                    updateMode.Information = teamWidget.Information;
                    _teamWidgetRepository.Update(updateMode);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void DeleteWidget(int key)
        {
            var teamWidget = _teamWidgetRepository.Get(x => x.Key == key).FirstOrDefault();
            _teamWidgetItemRepository.DeleteAll(x => x.TeamWidgetId == teamWidget.Id);
            _teamWidgetRepository.DeleteAll(x => x.Key == key);
        }

        public TeamWidget GetWidget(int key)
        {
            var widget = _teamWidgetRepository.Table.FirstOrDefault(x => x.Key == key);
            if (widget != null)
            {
                widget.TeamMemberItems =
                    _teamWidgetItemRepository.Table.Where(x => x.TeamWidgetId == widget.Id).ToList();
            }
            return widget;
        }
    }
}