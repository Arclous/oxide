﻿function CommentsWidgetModel(urlBase, urlAdd, urlDelete, urlUpdate, key) {

    var self = this;
    self.isLoading = ko.observable(false);
    self.approveRequired = ko.observable();
    self.currentPage = ko.observable();
    self.key = ko.observable(key);
    self.comments = ko.observableArray([]);
    self.maxCommentsToDisplay = ko.observable();

    self.newComment = ko.observable();
    self.loadComments = function(pagesize, page) {
        $.get(urlBase, { pageSize: pagesize, page: page, key: key }, function(data) {
            self.comments.removeAll();
            for (var i = 0; i <= data.Records.length - 1; i++) {
                var commentModel = new CommentModel(
                    data.Records[i].Id,
                    data.Records[i].Message,
                    data.Records[i].Deletable,
                    data.Records[i].UserName,
                    data.Records[i].UserImage,
                    data.Records[i].CommentDateTime
                );
                self.addCommentItem(commentModel);
            }
        });
    };
    self.addCommentItem = function(model) {
        self.comments.push(model);
    }.bind(self);

    self.saveComment = function() {
        self.isLoading(true);
        $.get(urlAdd, { message: self.newComment(), key: key }, function(data) {
            var commentModel = new CommentModel(
                data.Id,
                data.Message,
                data.Deletable,
                data.UserName,
                data.UserImage,
                data.CommentDateTime
            );
            self.addCommentItem(commentModel);
            self.isLoading(false);
        });
    }.bind(self);

}

function CommentModel(id, comment, deletable, userName, userPicture, dateTime) {
    var self = this;
    self.id = ko.observable(id);
    self.comment = ko.observable(comment);
    self.deletable = ko.observable(deletable);
    self.userName = ko.observable(userName);
    self.userPicture = ko.observable(userPicture);
    self.dateTime = ko.observable(dateTime);

    self.deleteComment = function(model) {
        if (self.deletable()) {

        }
    }.bind(self);
}