﻿using Oxide.Core.Elements;

namespace Theme.Default
{
    public class Default : IOxideTheme
    {
        public int Id { get; } = 0;

        public string Name { get; } = "Default";

        public string Title { get; } = "Default";

        public string Description { get; } = "Default Theme";

        public string Author { get; } = "Inevera Studios";

        public string Category { get; } = "Themes";

        public string PreviewImageUrl { get; } = "/Areas/Theme/Default/Contents/Oxide.png";

        public bool Active { get; set; }

        public string MasterDocumennt { get; set; } = "~/Areas/Theme/Default/Views/_Layout.cshtml";

        public string Error403 { get; set; } = "~/Areas/Theme/Default/Views/Errors/403.cshtml";

        public string Error404 { get; set; } = "~/Areas/Theme/Default/Views/Errors/404.cshtml";

        public string Error500 { get; set; } = "~/Areas/Theme/Default/Views/Errors/405.cshtml";
    }
}