﻿using System.Web.Optimization;
using Oxide.Core.Services.Interface.Registers.ResourceRegister;

namespace Theme.Default.Registiration
{
    public class ScriptRegistiration : IResourceRegisterer
    {
        public string ThemeName = "Default";
        public void RegisterResource(BundleCollection boundleCollection)
        {
            boundleCollection.Add(
                new ScriptBundle("~/bundles/jqueryFront").Include("~/Areas/Theme/Default/Scripts/jquery.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/bootstrapFront").Include("~/Areas/Theme/Default/Scripts/bootstrap.js"));
        }
    }
}