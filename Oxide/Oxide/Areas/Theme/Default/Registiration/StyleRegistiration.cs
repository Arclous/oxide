﻿using System.Web.Optimization;
using Oxide.Core.Services.Interface.Registers.ResourceRegister;

namespace Theme.Default.Registiration
{
    public class StyleRegistiration : IResourceRegisterer
    {
        public string ThemeName = "Default";
        public void RegisterResource(BundleCollection boundleCollection)
        {
            boundleCollection.Add(
                new StyleBundle("~/DefaultTheme/default").Include("~/Areas/Theme/Default/Styles/bootstrap.css"));
            boundleCollection.Add(
                new StyleBundle("~/DefaultTheme/defaultcss").Include("~/Areas/Theme/Default/Styles/site.css"));
            boundleCollection.Add(
                new StyleBundle("~/DefaultTheme/fontawesome").Include("~/Areas/Theme/Default/Styles/font-awesome.css"));
        }
    }
}