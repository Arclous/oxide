﻿using System.Collections.Generic;
using System.Linq;
using Oxide.Core.Elements;
using Oxide.Core.Extesions;
using Oxide.Core.Helpers;
using Oxide.Core.Syntax;

namespace Theme.Default.Layouts.Main
{
    public class MainLayout2 : IOxideLayout
    {
        public int PageId { get; set; }
        public int Id => 2;
        public string Name => "Main Template 2";
        public string ThemeName { get; set; } = "Default";
        public int LayoutType => Syntax.LayoutTypes.Main;
        public string[] Layouts => new[] {"Header,Footer"};
        public string PreviewImageUrl => "/Areas/Theme/Default/Contents/oxide.png";
        public string GetLayoutNames()
        {
            return Layouts.ConvertStringArrayToString();
        }
        public string[] GetLayouts()
        {
            return Layouts;
        }
        public Dictionary<int, string> GetLayoutsKeys()
        {
            return OxideUtils.GetEnumValues<LayoutKeys>().ToDictionary(v => (int) v, v => v.ToString());
        }
        public string View { get; set; } = "~/Areas/Theme/Default/Views/Main/_Layout2.cshtml";

        #region LayoutDefinations
        protected enum LayoutKeys
        {
            Header = 0,
            Footer = 1
        }
        public ICollection<IOxideWidget> Header = new List<IOxideWidget>();
        public ICollection<IOxideWidget> Container = new List<IOxideWidget>();
        public ICollection<IOxideWidget> Footer = new List<IOxideWidget>();
        #endregion
    }
}