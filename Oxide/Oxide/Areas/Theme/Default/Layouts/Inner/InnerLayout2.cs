﻿using System.Collections.Generic;
using System.Linq;
using Oxide.Core.Elements;
using Oxide.Core.Extesions;
using Oxide.Core.Helpers;
using Oxide.Core.Syntax;

namespace Theme.Default.Layouts.Inner
{
    public class InnerLayout2 : IOxideLayout
    {
        public int PageId { get; set; }
        public int Id => 2;
        public string Name => "Inner Template 2";
        public string ThemeName { get; set; } = "Default";
        public int LayoutType => Syntax.LayoutTypes.Inner;
        public string[] Layouts => new[] {"Left,Top Content,Content"};
        public string PreviewImageUrl => "";
        public string GetLayoutNames()
        {
            return Layouts.ConvertStringArrayToString();
        }
        public string[] GetLayouts()
        {
            return Layouts;
        }
        public Dictionary<int, string> GetLayoutsKeys()
        {
            return OxideUtils.GetEnumValues<LayoutKeys>().ToDictionary(v => (int) v, v => v.ToString());
        }
        public string View { get; set; } = "~/Areas/Theme/Default/Views/Inner/_Layout2.cshtml";

        #region LayoutDefinations
        protected enum LayoutKeys
        {
            Left = 0,
            TopContent = 1,
            Content = 2
        }
        public ICollection<IOxideWidget> Left = new List<IOxideWidget>();
        public ICollection<IOxideWidget> TopContent = new List<IOxideWidget>();
        public ICollection<IOxideWidget> Content = new List<IOxideWidget>();
        #endregion
    }
}