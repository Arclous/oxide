﻿using System.Collections.Generic;
using System.Linq;
using Oxide.Core.Elements;
using Oxide.Core.Extesions;
using Oxide.Core.Helpers;
using Oxide.Core.Syntax;

namespace Theme.Silver.Layouts.Inner
{
    public class InnerSilverLayout1 : IOxideLayout
    {
        public int PageId { get; set; }
        public int Id => 4;
        public string Name => "Silver Inner Template 1";
        public string ThemeName { get; set; } = "Silver";
        public int LayoutType => Syntax.LayoutTypes.Inner;
        public string[] Layouts => new[] {"Left,Content"};
        public string PreviewImageUrl => "";
        public string[] GetLayouts()
        {
            return Layouts;
        }
        public string GetLayoutNames()
        {
            return Layouts.ConvertStringArrayToString();
        }
        public Dictionary<int, string> GetLayoutsKeys()
        {
            return OxideUtils.GetEnumValues<LayoutKeys>().ToDictionary(v => (int) v, v => v.ToString());
        }
        public string View { get; set; } = "~/Areas/Theme/Silver/Views/Inner/_Layout1.cshtml";

        #region LayoutDefinations
        protected enum LayoutKeys
        {
            Left = 0,
            Content = 1
        }
        public ICollection<IOxideWidget> Left = new List<IOxideWidget>();
        public ICollection<IOxideWidget> Content = new List<IOxideWidget>();
        #endregion
    }
}