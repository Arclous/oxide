﻿using System.Collections.Generic;
using System.Linq;
using Oxide.Core.Elements;
using Oxide.Core.Extesions;
using Oxide.Core.Helpers;
using Oxide.Core.Syntax;

namespace Theme.Silver.Layouts.Main
{
    public class MainSilverLayout1 : IOxideLayout
    {
        public int PageId { get; set; }
        public int Id => 3;
        public string Name => "Silver Main Template 1";
        public string ThemeName { get; set; } = "Silver";
        public int LayoutType => Syntax.LayoutTypes.Main;
        public string[] Layouts => new[] {"Header,Container,Footer"};
        public string PreviewImageUrl => "/Areas/Theme/Silver/Contents/oxide.png";
        public string GetLayoutNames()
        {
            return Layouts.ConvertStringArrayToString();
        }
        public string[] GetLayouts()
        {
            return Layouts;
        }
        public Dictionary<int, string> GetLayoutsKeys()
        {
            return OxideUtils.GetEnumValues<LayoutKeys>().ToDictionary(v => (int) v, v => v.ToString());
        }
        public string View { get; set; } = "~/Areas/Theme/Silver/Views/Main/_Layout1.cshtml";

        #region LayoutDefinations
        protected enum LayoutKeys
        {
            Header = 0,
            Container = 1,
            Footer = 2
        }
        public ICollection<IOxideWidget> Header = new List<IOxideWidget>();
        public ICollection<IOxideWidget> Container = new List<IOxideWidget>();
        public ICollection<IOxideWidget> Footer = new List<IOxideWidget>();
        #endregion
    }
}