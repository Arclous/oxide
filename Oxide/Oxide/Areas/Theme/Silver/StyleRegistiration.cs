﻿using System.Web.Optimization;
using Oxide.Core.Services.Interface.Registers.ResourceRegister;

namespace Theme.Silver
{
    public class StyleRegistiration : IResourceRegisterer
    {
        public string ThemeName = "Silver";
        public void RegisterResource(BundleCollection boundleCollection)
        {
            boundleCollection.Add(
                new StyleBundle("~/DefaultTheme/css").Include("~/Areas/Themes/Default/Styles/bootstrap.css"));
        }
    }
}