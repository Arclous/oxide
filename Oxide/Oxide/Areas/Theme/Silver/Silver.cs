﻿using Oxide.Core.Elements;

namespace Theme.Silver
{
    public class Silver : IOxideTheme
    {
        public int Id { get; } = 0;

        public string Name { get; } = "Silver";

        public string Title { get; } = "Silver";

        public string Description { get; } = "Silver Theme";

        public string Author { get; } = "Inevera Studios";

        public string Category { get; } = "Themes";

        public string PreviewImageUrl { get; } = "Silver";

        public bool Active { get; set; }

        public string MasterDocumennt { get; set; } = "~/Areas/Theme/Silver/Views/_Layout.cshtml";

        public string Error403 { get; set; } = "~/Areas/Theme/Silver/Views/Errors/403.cshtml";

        public string Error404 { get; set; } = "~/Areas/Theme/Silver/Views/Errors/404.cshtml";

        public string Error500 { get; set; } = "~/Areas/Theme/Silver/Views/Errors/405.cshtml";
    }
}