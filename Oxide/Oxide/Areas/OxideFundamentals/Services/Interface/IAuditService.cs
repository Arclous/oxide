﻿using System;
using System.Collections.Generic;
using Oxide.Core.Base.OxideDependency;
using Oxide.Core.Syntax;
using Oxide.Core.ViewModels.FilterViewModels;
using Oxide.Fundamentals.Models.Audit;
using Oxide.Fundamentals.ViewModels.Audit;

namespace Oxide.Fundamentals.Services.Interface
{
    /// <summary>
    ///     Service for Audits
    /// </summary>
    public interface IAuditService : IOxideDependency
    {
        /// <summary>
        ///     Creates new audit settings for the content according to content type
        /// </summary>
        /// <param name="contentType">Type of the content</param>
        /// <param name="eventType">Type of the event</param>
        /// <param name="active">Status of audit setting</param>
        void CreateNewAuditSetting(string contentType, int eventType, bool active);
        /// <summary>
        ///     Updates audit settings for the content according id
        /// </summary>
        /// <param name="contentType">Type of the content</param>
        /// <param name="eventType">Type of the event</param>
        /// <param name="active">Status of audit setting</param>
        /// <param name="id">Id of the record</param>
        void UpdateAuditSetting(string contentType, int eventType, bool active, int id);
        /// <summary>
        ///     Deletes audit settings according to id
        /// </summary>
        /// <param name="id">Id of the record</param>
        void DeleteAuditSetting(int id);
        /// <summary>
        ///     Gets audit setting according to id
        /// </summary>
        /// <param name="id">Id of the record</param>
        AuditSettings GetAuditSetting(int id);
        /// <summary>
        ///     Gets audit setting if audit setting is exist
        /// </summary>
        /// <param name="contentType">Type of the content</param>
        /// <param name="eventType">Type of the event</param>
        AuditSettings GetAuditSetting(string contentType, Syntax.EventTypes eventType);
        /// <summary>
        ///     Gets all audit settings
        /// </summary>
        List<AuditSettings> GetAllAuditSettings();
        /// <summary>
        ///     Gets all active audit settings
        /// </summary>
        List<AuditSettings> GetAllActiveAuditSettings();
        /// <summary>
        ///     Gets model for audit setting
        /// </summary>
        List<string> GetModelsForAudit();
        /// <summary>
        ///     Gets model for audit by using name
        /// </summary>
        Type GetModelByName(string modelName);
        /// <summary>
        ///     Checks if audit setting is exist
        /// </summary>
        /// <param name="contentType">Type of the content</param>
        /// <param name="eventType">Type of the event</param>
        bool IsAuditExist(string contentType, Syntax.EventTypes eventType);
        /// <summary>
        ///     Creates audit according to model
        /// </summary>
        /// <param name="audit">Audit model</param>
        void CreateAudit(Audit audit);
        /// <summary>
        ///     Creates audit according to model
        /// </summary>
        /// <param name="audits">Audit model</param>
        void CreateAudit(List<Audit> audits);
        /// <summary>
        ///     Gets all audits
        /// </summary>
        List<Audit> GetAudits();
        /// <summary>
        ///     Deletes all audits
        /// </summary>
        void DeleteAllAudits();
        /// <summary>
        ///     Gets audits as filter view model
        /// </summary>
        /// <param name="pageSize">Size of the page for each page</param>
        /// <param name="page">Requested page index</param>
        /// <param name="filter">Filter criterias to filtering logs</param>
        FilterModel<AuditViewModel> GetAuditWithFilter(int pageSize, int page, string filter);
    }
}