﻿using Oxide.Core.Base.OxideDependency;
using Oxide.Fundamentals.Models.Menu;
using Oxide.Fundamentals.ViewModels.Footer;

namespace Oxide.Fundamentals.Services.Interface
{
    public interface IFooterWidgetService : IOxideDependency
    {
        void UpdateSettings(FooterWidgetEditorViewModel footerWidgetData);
        void DeleteSettings(int key);
        FooterWidgetEditorViewModel LoadSettings(int key);
        Menu GetMenu(int id);
    }
}