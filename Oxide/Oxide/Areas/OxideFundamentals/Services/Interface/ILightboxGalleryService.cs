﻿using System.Collections.Generic;
using Oxide.Core.Base.OxideDependency;

namespace Oxide.Fundamentals.Services.Interface
{
    /// <summary>
    ///     Service for Lightbox Gallery Widget
    /// </summary>
    public interface ILightboxGalleryService : IOxideDependency
    {
        /// <summary>
        ///     Saves medias according to page
        /// </summary>
        /// <param name="key">Key of the page</param>
        /// <param name="media">Id or Ids of the media</param>
        bool SaveMedia(int key, string media);
        /// <summary>
        ///     Gets medias for to page
        /// </summary>
        /// <param name="key">Key of the page</param>
        List<string> GetMedia(int key);
        /// <summary>
        ///     Gets medias for to page
        /// </summary>
        /// <param name="key">Key of the page</param>
        bool ClearMedia(int key);
    }
}