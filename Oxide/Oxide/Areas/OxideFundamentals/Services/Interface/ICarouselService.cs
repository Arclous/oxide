﻿using System.Collections.Generic;
using Oxide.Core.Base.OxideDependency;
using Oxide.Fundamentals.Models.Carousel;

namespace Oxide.Fundamentals.Services.Interface
{
    /// <summary>
    ///     Service for Carouse Widget
    /// </summary>
    public interface ICarouselService : IOxideDependency
    {
        /// <summary>
        ///     Saves medias according to page
        /// </summary>
        /// <param name="key">Key of the page</param>
        /// <param name="media">Id or Ids of the media</param>
        bool SaveMedia(int key, string media);
        /// <summary>
        ///     Gets medias for to page
        /// </summary>
        /// <param name="key">Key of the page</param>
        List<string> GetMedia(int key);
        /// <summary>
        ///     Gets medias for to page
        /// </summary>
        /// <param name="key">Key of the page</param>
        bool ClearMedia(int key);
        /// <summary>
        ///     Save settings for the widget
        /// </summary>
        /// <param name="settingsModel">Model of the settings</param>
        bool SaveSettings(CarouselSetting settingsModel);
        /// <summary>
        ///     Save settings for the widget
        /// </summary>
        /// <param name="key">Key of the page</param>
        CarouselSetting GetSettings(int key);
        /// <summary>
        ///     Save settings for the widget
        /// </summary>
        /// <param name="key">Key of the page</param>
        void DeleteSettings(int key);
    }
}