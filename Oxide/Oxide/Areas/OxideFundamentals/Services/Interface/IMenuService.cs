﻿using System.Collections.Generic;
using Oxide.Core.Base.OxideDependency;
using Oxide.Core.Syntax;
using Oxide.Core.ViewModels.FilterViewModels;
using Oxide.Fundamentals.Models.Menu;
using Oxide.Fundamentals.ViewModels.FilterViewModels;
using Oxide.Fundamentals.ViewModels.Menu;

namespace Oxide.Fundamentals.Services.Interface
{
    public interface IMenuService : IOxideDependency
    {
        int CreateMenu(Menu menu);
        void UpdateMenu(MenuViewModel menu);
        void DeleteMenu(int id);
        void DeleteMenu(Menu menu);
        Menu GetMenu(int id);
        MenuItem GetTopMenu(int id);
        List<Menu> GetAllMenus();
        MenuFilterViewModel GetMenusWithFilter(int pageSize, int page, string filter);
        int CreateMenuItem(MenuItem menuItem);
        void UpdateMenuItem(MenuItemViewModel menuItem);
        void DeleteMenuItem(int id);
        MenuItem GetMenuItem(int id);
        void DeleteMenuItem(MenuItem menuItem);
        List<MenuItem> GetAllMenuItems(LinkTypes linkType);
        List<MenuItem> GetAllMenuItems();
        FilterModel<MenuItem> GetMenuItemsWithFilter(int pageSize, int page, string filter);

        #region Widget
        void UpdateSettings(MenuWidgetViewModel menu);
        void DeleteSettings(int key);
        MenuWidgetViewModel LoadSettings(int key);
        MenuPacket MenuItemList(int menuId);
        List<NavigationMenuViewModel> MenuItemListFront(int menuId, int pageId);
        #endregion

        #region CoolWidget
        void UpdateCoolMenuSettings(CoolMenuWidgetViewModel menu);
        void DeleteCoolMenuSettings(int key);
        CoolMenuWidgetViewModel LoadCoolMenuSettings(int key);
        List<CoolMenuViewModel> CollMenuItemListFront(int menuId, int pageId);
        #endregion
    }
}