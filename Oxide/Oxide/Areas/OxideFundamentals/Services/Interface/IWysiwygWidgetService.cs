﻿using Oxide.Core.Base.OxideDependency;

namespace Oxide.Fundamentals.Services.Interface
{
    public interface IWysiwygWidgetService : IOxideDependency
    {
        void SaveContent(string content, int key);
        string LoadContent(int key);
        void DeleteContent(int key);
    }
}