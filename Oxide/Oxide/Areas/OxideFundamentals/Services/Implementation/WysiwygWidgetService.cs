﻿using System.Linq;
using Oxide.Core.Data.Repository;
using Oxide.Fundamentals.Models.Wysiwyg;
using Oxide.Fundamentals.Services.Interface;

namespace Oxide.Fundamentals.Services.Implementation
{
    public class WysiwygWidgetService : IWysiwygWidgetService
    {
        private readonly IOxideRepository<Wysiwyg> _wysywygRepository;

        public WysiwygWidgetService(IOxideRepository<Wysiwyg> wysywygRepository)
        {
            _wysywygRepository = wysywygRepository;
        }

        public void SaveContent(string content, int key)
        {
            var updateModel = _wysywygRepository.GetAll().FirstOrDefault(x => x.Key == key);
            if (updateModel != null)
            {
                updateModel.Html = content;
                _wysywygRepository.Update(updateModel);
            }
            else
            {
                var newContent = new Wysiwyg {Html = content, Key = key};
                _wysywygRepository.Insert(newContent);
            }
        }

        public string LoadContent(int key)
        {
            var htmlContent = _wysywygRepository.SearchFor(x => x.Key == key).FirstOrDefault();
            return htmlContent != null ? htmlContent.Html : "";
        }

        public void DeleteContent(int key)
        {
            _wysywygRepository.DeleteAll(x => x.Key == key);
            ;
        }
    }
}