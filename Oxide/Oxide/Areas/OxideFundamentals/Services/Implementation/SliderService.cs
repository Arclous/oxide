﻿using System;
using System.Collections.Generic;
using System.Linq;
using Oxide.Core.Data.Repository;
using Oxide.Core.Syntax;
using Oxide.Fundamentals.Models.Slider;
using Oxide.Fundamentals.Services.Interface;

namespace Oxide.Fundamentals.Services.Implementation
{
    public class SliderService : ISliderService
    {
        private readonly IOxideRepository<SliderItem> _sliderItemRepository;

        public SliderService(IOxideRepository<SliderItem> sliderItemRepository)
        {
            _sliderItemRepository = sliderItemRepository;
        }

        public bool SaveMedia(int key, string media)
        {
            if (media == string.Empty) return true;
            try
            {
                var medias = media.Split(Constants.Splitter).ToList();
                foreach (var carouselItem in
                    medias.Select(mdia => new SliderItem {ImageId = Convert.ToInt32(mdia), Key = key}))
                {
                    _sliderItemRepository.Insert(carouselItem);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<string> GetMedia(int key)
        {
            return
                _sliderItemRepository.GetAll()
                                     .Where(x => x.Key == key)
                                     .ToList()
                                     .OrderBy(x => x.Id)
                                     .Select(v => v.ImageId.ToString())
                                     .ToList();
        }

        public bool ClearMedia(int key)
        {
            try
            {
                _sliderItemRepository.DeleteAll(x => x.Key == key);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}