﻿using System;
using System.Collections.Generic;
using System.Linq;
using Oxide.Core.Data.Repository;
using Oxide.Core.Extesions;
using Oxide.Core.Syntax;
using Oxide.Fundamentals.Models.Carousel;
using Oxide.Fundamentals.Services.Interface;

namespace Oxide.Fundamentals.Services.Implementation
{
    public class CarouselService : ICarouselService
    {
        private readonly IOxideRepository<CarouselItem> _carouselItemRepository;
        private readonly IOxideRepository<CarouselSetting> _carouselSettingsRepository;

        public CarouselService(IOxideRepository<CarouselItem> carouselItemRepository,
            IOxideRepository<CarouselSetting> carouselSettingsRepository)
        {
            _carouselItemRepository = carouselItemRepository;
            _carouselSettingsRepository = carouselSettingsRepository;
        }

        public bool SaveMedia(int key, string media)
        {
            if (media == string.Empty) return true;
            try
            {
                var medias = media.Split(Constants.Splitter).ToList();
                foreach (var carouselItem in
                    medias.Select(mdia => new CarouselItem {ImageId = Convert.ToInt32(mdia), Key = key}))
                {
                    _carouselItemRepository.Insert(carouselItem);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<string> GetMedia(int key)
        {
            return
                _carouselItemRepository.GetAll()
                                       .Where(x => x.Key == key)
                                       .ToList()
                                       .OrderBy(x => x.Id)
                                       .Select(v => v.ImageId.ToString())
                                       .ToList();
        }

        public bool ClearMedia(int key)
        {
            try
            {
                _carouselItemRepository.DeleteAll(x => x.Key == key);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool SaveSettings(CarouselSetting settingsModel)
        {
            try
            {
                var existSettings = _carouselSettingsRepository.GetAll()
                                                               .SingleOrDefault(x => x.Key == settingsModel.Key);
                if (existSettings == null)
                    _carouselSettingsRepository.Insert(settingsModel);
                else
                {
                    settingsModel.CopyPropertiesTo(existSettings, new[] {"Id"});
                    _carouselSettingsRepository.Update(existSettings);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public CarouselSetting GetSettings(int key)
        {
            return _carouselSettingsRepository.Get(x => x.Key == key).FirstOrDefault();
        }

        public void DeleteSettings(int key)
        {
            _carouselSettingsRepository.DeleteAll(x => x.Key == key);
        }
    }
}