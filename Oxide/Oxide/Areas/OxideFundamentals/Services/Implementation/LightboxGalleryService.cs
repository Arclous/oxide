﻿using System;
using System.Collections.Generic;
using System.Linq;
using Oxide.Core.Data.Repository;
using Oxide.Core.Syntax;
using Oxide.Fundamentals.Models.LighboxGallery;
using Oxide.Fundamentals.Services.Interface;

namespace Oxide.Fundamentals.Services.Implementation
{
    public class LightboxGalleryService : ILightboxGalleryService
    {
        private readonly IOxideRepository<LightboxGalleryItem> _lightboxGalleryRepository;

        public LightboxGalleryService(IOxideRepository<LightboxGalleryItem> lightboxGalleryRepository)
        {
            _lightboxGalleryRepository = lightboxGalleryRepository;
        }

        public bool SaveMedia(int key, string media)
        {
            if (media == string.Empty) return true;
            try
            {
                var medias = media.Split(Constants.Splitter).ToList();
                foreach (var lightboxGalleryItem in
                    medias.Select(mdia => new LightboxGalleryItem {ImageId = Convert.ToInt32(mdia), Key = key}))
                {
                    _lightboxGalleryRepository.Insert(lightboxGalleryItem);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<string> GetMedia(int key)
        {
            return
                _lightboxGalleryRepository.GetAll().Where(x => x.Key == key).Select(v => v.ImageId.ToString()).ToList();
        }

        public bool ClearMedia(int key)
        {
            try
            {
                _lightboxGalleryRepository.DeleteAll(x => x.Key == key);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}