﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using Oxide.Core.Data.Repository;
using Oxide.Core.Extesions;
using Oxide.Core.Services.PageService;
using Oxide.Core.Syntax;
using Oxide.Core.ViewModels.FilterViewModels;
using Oxide.Fundamentals.Models.Menu;
using Oxide.Fundamentals.Services.Interface;
using Oxide.Fundamentals.ViewModels.FilterViewModels;
using Oxide.Fundamentals.ViewModels.Menu;

namespace Oxide.Fundamentals.Services.Implementation
{
    public class MenuService : IMenuService
    {
        private readonly IOxideRepository<CoolMenuWidget> _coolMenuWidgetRepository;

        private readonly Expression<Func<CoolMenuWidget, object>>[] _includesCoolMenuWidget =
        {
            x =>
                x.Menu.Menus.Select(
                    l1 =>
                        l1.Menus.Select(
                            l2 => l2.Menus.Select(l3 => l3.Menus.Select(l4 => l4.Menus.Select(l5 => l5.Menus)))))
        };

        private readonly Expression<Func<Menu, object>>[] _includesMenu =
        {
            x =>
                x.Menus.Select(
                    l1 =>
                        l1.Menus.Select(
                            l2 => l2.Menus.Select(l3 => l3.Menus.Select(l4 => l4.Menus.Select(l5 => l5.Menus)))))
        };

        private readonly Expression<Func<MenuItem, object>>[] _includesMenuItems =
        {
            x =>
                x.Menus.Select(
                    l1 =>
                        l1.Menus.Select(
                            l2 => l2.Menus.Select(l3 => l3.Menus.Select(l4 => l4.Menus.Select(l5 => l5.Menus)))))
        };

        private readonly Expression<Func<MenuWidget, object>>[] _includesMenuWidget =
        {
            x =>
                x.Menu.Menus.Select(
                    l1 =>
                        l1.Menus.Select(
                            l2 => l2.Menus.Select(l3 => l3.Menus.Select(l4 => l4.Menus.Select(l5 => l5.Menus)))))
        };

        private readonly IOxideRepository<MenuItem> _menuItemRepository;
        private readonly IOxideRepository<Menu> _menusRepository;
        private readonly IOxideRepository<MenuWidget> _menuWidgetRepository;
        private readonly IPageService _pageService;

        public MenuService(IOxideRepository<Menu> menusRepository, IOxideRepository<MenuItem> menuItemRepository,
            IOxideRepository<MenuWidget> menuWidgetRepository, IPageService pageService,
            IOxideRepository<CoolMenuWidget> coolMenuWidgetRepository)
        {
            _menusRepository = menusRepository;
            _menuItemRepository = menuItemRepository;
            _menuWidgetRepository = menuWidgetRepository;
            _pageService = pageService;
            _coolMenuWidgetRepository = coolMenuWidgetRepository;
        }

        public int CreateMenu(Menu menu)
        {
            _menusRepository.Insert(menu);
            return menu.Id;
        }

        public void UpdateMenu(MenuViewModel menu)
        {
            var updateMode = _menusRepository.SearchFor(x => x.Id == menu.Id, _includesMenu).FirstOrDefault();
            menu.CopyPropertiesTo(updateMode, new[] {"Id"});
            _menusRepository.Update(updateMode);
        }

        public void DeleteMenu(int id)
        {
            _menusRepository.Delete(id);
        }

        public void DeleteMenu(Menu menu)
        {
            _menusRepository.Delete(menu);
        }

        public Menu GetMenu(int id)
        {
            return _menusRepository.GetById(id, _includesMenu);
        }

        public MenuItem GetTopMenu(int id)
        {
            return _menusRepository.GetById(id, _includesMenu).Menus.FirstOrDefault(x => x.ParentId == -1);
        }

        public List<Menu> GetAllMenus()
        {
            return _menusRepository.GetAll(_includesMenu).ToList();
        }

        public MenuFilterViewModel GetMenusWithFilter(int pageSize, int page, string filter)
        {
            var pageFilterViewModel = new MenuFilterViewModel();
            if (filter != string.Empty)
                pageFilterViewModel.Records =
                    GetMenuViewModelList(
                        _menusRepository.GetAll()
                                        .OrderBy(x => x.Name)
                                        .Where(
                                            x =>
                                                x.Name.ToLower().Contains(filter.ToLower()) ||
                                                x.Title.ToLower().Contains(filter.ToLower()))
                                        .Skip(pageSize*page)
                                        .Take(pageSize)
                                        .ToList());
            else
                pageFilterViewModel.Records =
                    GetMenuViewModelList(
                        _menusRepository.GetAll().OrderBy(x => x.Name).Skip(pageSize*page).Take(pageSize).ToList());
            pageFilterViewModel.CurrentPage = page;
            pageFilterViewModel.DisplayingRecords = pageFilterViewModel.Records.Count();
            pageFilterViewModel.TotalRecords = _menusRepository.GetAll().Count();
            if (filter != string.Empty)
                pageFilterViewModel.TotalPages =
                    (_menusRepository.GetAll()
                                     .OrderBy(x => x.Name)
                                     .Count(x => x.Name.Contains(filter) || x.Title.Contains(filter)) + pageSize - 1)/
                    pageSize;
            else
                pageFilterViewModel.TotalPages = (pageFilterViewModel.TotalRecords + pageSize - 1)/pageSize;
            pageFilterViewModel.NextEnable = pageFilterViewModel.CurrentPage < pageFilterViewModel.TotalPages - 1;
            pageFilterViewModel.PreviousEnable = pageFilterViewModel.CurrentPage > 0;
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            pageFilterViewModel.DeleteUrl = urlHelper.Action("DeleteMenu", "Menu", new {area = "Oxide.Fundamentals"});
            return pageFilterViewModel;
        }

        public int CreateMenuItem(MenuItem menuItem)
        {
            _menuItemRepository.Insert(menuItem);
            return menuItem.Id;
        }

        public void UpdateMenuItem(MenuItemViewModel menu)
        {
            var updateMode = _menuItemRepository.SearchFor(x => x.Id == menu.Id, _includesMenuItems).FirstOrDefault();
            menu.CopyPropertiesTo(updateMode, new[] {"Id", "Menus"});
            _menuItemRepository.Update(updateMode);
        }

        public void DeleteMenuItem(int id)
        {
            var parent = _menuItemRepository.GetById(id, _includesMenuItems);
            foreach (var v in parent.Menus.ToList())
            {
                DeleteMenuItem(v.Id);
            }
            _menuItemRepository.Delete(parent);
        }

        public MenuItem GetMenuItem(int id)
        {
            return _menuItemRepository.GetById(id, _includesMenuItems);
        }

        public void DeleteMenuItem(MenuItem menuItem)
        {
            foreach (var v in menuItem.Menus.ToList())
            {
                DeleteMenuItem(v.Id);
            }
            _menuItemRepository.Delete(menuItem);
        }

        public List<MenuItem> GetAllMenuItems(LinkTypes linkType)
        {
            return _menuItemRepository.SearchFor(x => x.LinkType == linkType).ToList();
        }

        public List<MenuItem> GetAllMenuItems()
        {
            return _menuItemRepository.GetAll().ToList();
        }

        FilterModel<MenuItem> IMenuService.GetMenuItemsWithFilter(int pageSize, int page, string filter)
        {
            throw new NotImplementedException();
        }

        private static ICollection<MenuViewModel> GetMenuViewModelList(IEnumerable<Menu> recordList)
        {
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            return (from menu in recordList
                    let menuViewModel =
                        new MenuViewModel
                        {
                            EditUrl =
                                urlHelper.Action("EditMenu", "Menu", new {area = "Oxide.Fundamentals", menuId = menu.Id}),
                            MenuItemsUrl =
                                urlHelper.Action("DisplayMenuItems", "Menu",
                                    new {area = "Oxide.Fundamentals", menuId = menu.Id})
                        }
                    select menu.CopyPropertiesTo(menuViewModel)).ToList();
        }

        #region Widget

        public void UpdateSettings(MenuWidgetViewModel menu)
        {
            var updateMode = _menuWidgetRepository.GetAll(_includesMenuWidget).FirstOrDefault(x => x.Key == menu.Key);
            if (updateMode != null)
            {
                menu.CopyPropertiesTo(updateMode, new[] {"Id"});
                _menuWidgetRepository.Update(updateMode);
            }
            else
            {
                var menuWidget = new MenuWidget();
                menu.CopyPropertiesTo(menuWidget);
                _menuWidgetRepository.Insert(menuWidget);
            }
        }

        public void DeleteSettings(int key)
        {
            _menuWidgetRepository.DeleteAll(x => x.Key == key);
        }

        public MenuWidgetViewModel LoadSettings(int key)
        {
            var menu = new MenuWidgetViewModel();
            var updateMode = _menuWidgetRepository.SearchFor(x => x.Key == key, _includesMenuWidget).FirstOrDefault();
            updateMode?.CopyPropertiesTo(menu);
            if (updateMode?.Menu != null) menu.SelectedContents = updateMode.Menu.Id.ToString();
            return menu;
        }

        public MenuPacket MenuItemList(int menuId)
        {
            var menuPacket = new MenuPacket();
            var mainMenu = GetMenu(menuId);
            foreach (var subMenu in mainMenu.Menus.Where(x => x.ParentId == menuId))
            {
                var subMenuItem = new TreeViewItemModel
                {
                    Id = subMenu.Id,
                    Name = subMenu.Name,
                    Childeren = new List<TreeViewItemModel>()
                };
                AddSubMenus(subMenu.Menus, subMenuItem);
                menuPacket.Menus.Add(subMenuItem);
            }
            return menuPacket;
        }

        public List<NavigationMenuViewModel> MenuItemListFront(int menuId, int pageId)
        {
            var menu = new List<NavigationMenuViewModel>();
            var mainMenu = GetMenu(menuId);
            foreach (var subMenu in mainMenu.Menus.Where(x => x.ParentId == menuId && !x.IsChild))
            {
                var subMenuItem = new NavigationMenuViewModel
                {
                    Id = subMenu.Id,
                    Name = subMenu.Name,
                    Active = subMenu.PageId == pageId,
                    SubMenus = new List<NavigationMenuViewModel>()
                };
                SetUrl(subMenu, subMenuItem);
                AddSubMenusFront(subMenu.Menus, subMenuItem, pageId);
                menu.Add(subMenuItem);
            }
            return menu;
        }

        public void UpdateCoolMenuSettings(CoolMenuWidgetViewModel menu)
        {
            var updateMode =
                _coolMenuWidgetRepository.GetAll(_includesCoolMenuWidget).FirstOrDefault(x => x.Key == menu.Key);
            if (updateMode != null)
            {
                menu.CopyPropertiesTo(updateMode, new[] {"Id"});
                _coolMenuWidgetRepository.Update(updateMode);
            }
            else
            {
                var menuWidget = new CoolMenuWidget();
                menu.CopyPropertiesTo(menuWidget);
                _coolMenuWidgetRepository.Insert(menuWidget);
            }
        }

        public void DeleteCoolMenuSettings(int key)
        {
            _coolMenuWidgetRepository.DeleteAll(x => x.Key == key);
        }

        public CoolMenuWidgetViewModel LoadCoolMenuSettings(int key)
        {
            var menu = new CoolMenuWidgetViewModel();
            var updateMode =
                _coolMenuWidgetRepository.SearchFor(x => x.Key == key, _includesCoolMenuWidget).FirstOrDefault();
            updateMode?.CopyPropertiesTo(menu);
            if (updateMode?.Menu != null) menu.SelectedContents = updateMode.Menu.Id.ToString();
            if (updateMode?.Logo != null) menu.SelectedMedias = updateMode.Logo.ToString();
            return menu;
        }

        public List<CoolMenuViewModel> CollMenuItemListFront(int menuId, int pageId)
        {
            var menu = new List<CoolMenuViewModel>();
            var mainMenu = GetMenu(menuId);
            foreach (var subMenu in mainMenu.Menus.Where(x => x.ParentId == menuId && !x.IsChild))
            {
                var subMenuItem = new CoolMenuViewModel
                {
                    Id = subMenu.Id,
                    Name = subMenu.Name,
                    Active = subMenu.PageId == pageId,
                    SubMenus = new List<CoolMenuViewModel>()
                };
                SetUrl(subMenu, subMenuItem);
                AddSubMenusFront(subMenu.Menus, subMenuItem, pageId);
                menu.Add(subMenuItem);
            }
            return menu;
        }

        #region Helpers

        private static void AddSubMenus(IEnumerable<MenuItem> menuItems, TreeViewItemModel target)
        {
            target.Childeren = new List<TreeViewItemModel>();
            foreach (var item in menuItems)
            {
                var menuteItemModel = new TreeViewItemModel
                {
                    Id = item.Id,
                    Name = item.Name,
                    Childeren = new List<TreeViewItemModel>()
                };
                AddSubMenus(item.Menus, menuteItemModel);
                target.Childeren.Add(menuteItemModel);
            }
        }

        private void AddSubMenusFront(IEnumerable<MenuItem> menuItems, NavigationMenuViewModel target, int pageId)
        {
            target.SubMenus = new List<NavigationMenuViewModel>();
            foreach (var item in menuItems)
            {
                var menuteItemModel = new NavigationMenuViewModel
                {
                    Id = item.Id,
                    Name = item.Name,
                    Active = item.PageId == pageId,
                    SubMenus = new List<NavigationMenuViewModel>()
                };
                SetUrl(item, menuteItemModel);
                AddSubMenusFront(item.Menus, menuteItemModel, pageId);
                target.SubMenus.Add(menuteItemModel);
            }
        }

        private void AddSubMenusFront(IEnumerable<MenuItem> menuItems, CoolMenuViewModel target, int pageId)
        {
            target.SubMenus = new List<CoolMenuViewModel>();
            foreach (var item in menuItems)
            {
                var menuteItemModel = new CoolMenuViewModel
                {
                    Id = item.Id,
                    Name = item.Name,
                    Active = item.PageId == pageId,
                    SubMenus = new List<CoolMenuViewModel>()
                };
                SetUrl(item, menuteItemModel);
                AddSubMenusFront(item.Menus, menuteItemModel, pageId);
                target.SubMenus.Add(menuteItemModel);
            }
        }

        private void SetUrl(MenuItem menuItem, NavigationMenuViewModel menuViewModel)
        {
            if (menuItem.PageId == 0)
                menuViewModel.Url = menuItem.Url ?? Constants.Diez;
            else
            {
                var page = _pageService.GetPage(menuItem.PageId);
                menuViewModel.Url = page.Url;
            }
            if (menuItem.OpenType == LinkOpenTypes.New)
                menuViewModel.Target = "_blank";
            if (menuItem.OpenType == LinkOpenTypes.Parent)
                menuViewModel.Target = "_parent";
        }

        private void SetUrl(MenuItem menuItem, CoolMenuViewModel menuViewModel)
        {
            if (menuItem.PageId == 0)
                menuViewModel.Url = menuItem.Url ?? Constants.Diez;
            else
            {
                var page = _pageService.GetPage(menuItem.PageId);
                menuViewModel.Url = page.Url;
            }
            if (menuItem.OpenType == LinkOpenTypes.New)
                menuViewModel.Target = "_blank";
            if (menuItem.OpenType == LinkOpenTypes.Parent)
                menuViewModel.Target = "_parent";
        }

        #endregion

        #endregion
    }
}