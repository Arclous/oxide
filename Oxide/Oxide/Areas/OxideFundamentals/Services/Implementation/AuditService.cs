﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Oxide.Core.Data.Repository;
using Oxide.Core.Services.AdminUserService;
using Oxide.Core.Services.LoginService;
using Oxide.Core.Services.OxideKernelService;
using Oxide.Core.Syntax;
using Oxide.Core.ViewModels.FilterViewModels;
using Oxide.Fundamentals.Models.Audit;
using Oxide.Fundamentals.Services.Interface;
using Oxide.Fundamentals.ViewModels.Audit;

namespace Oxide.Fundamentals.Services.Implementation
{
    public class AuditService : IAuditService
    {
        private readonly IAdminUserService _adminUserService;
        private readonly IOxideRepository<Audit> _auditRepository;
        private readonly IOxideRepository<AuditSettings> _auditSettingRepository;
        private readonly ILoginService _loginService;
        private readonly IOxideKernelService _oxideKernelService;

        public AuditService(IOxideRepository<AuditSettings> auditSettingRepository,
            IOxideKernelService oxideKernelService, IOxideRepository<Audit> auditRepository,
            IAdminUserService adminUserService, ILoginService loginService)
        {
            _auditSettingRepository = auditSettingRepository;
            _oxideKernelService = oxideKernelService;
            _auditRepository = auditRepository;
            _adminUserService = adminUserService;
            _loginService = loginService;
        }

        public void CreateNewAuditSetting(string contentType, int eventType, bool active)
        {
            var auditSetting = new AuditSettings
            {
                Name = contentType,
                EventType = (Syntax.EventTypes) eventType,
                Title = Enum.GetName(typeof (Syntax.EventTypes), eventType),
                Active = active
            };
            _auditSettingRepository.Insert(auditSetting);
        }

        public void UpdateAuditSetting(string contentType, int eventType, bool active, int id)
        {
            var currentSettings = _auditSettingRepository.Table.FirstOrDefault(x => x.Id == id);
            if (currentSettings == null) return;
            currentSettings.Name = contentType;
            currentSettings.Title = Enum.GetName(typeof (Syntax.EventTypes), eventType);
            currentSettings.EventType = (Syntax.EventTypes) eventType;
            currentSettings.Active = active;
            _auditSettingRepository.Update(currentSettings);
        }

        public void DeleteAuditSetting(int id)
        {
            _auditSettingRepository.Delete(id);
        }

        public AuditSettings GetAuditSetting(int id)
        {
            return _auditSettingRepository.Table.FirstOrDefault(x => x.Id == id);
        }

        public AuditSettings GetAuditSetting(string contentType, Syntax.EventTypes eventType)
        {
            return _auditSettingRepository.Table.FirstOrDefault(x => x.Name == contentType && x.EventType == eventType);
        }

        public List<AuditSettings> GetAllAuditSettings()
        {
            return _auditSettingRepository.GetAll().ToList();
        }

        public List<AuditSettings> GetAllActiveAuditSettings()
        {
            return _auditSettingRepository.GetAll().Where(x => x.Active).ToList();
        }

        public List<string> GetModelsForAudit()
        {
            return _oxideKernelService.GetLoadedModels().Select(x => x.Name).ToList();
        }

        public Type GetModelByName(string modelName)
        {
            return _oxideKernelService.GetLoadedModels().FirstOrDefault(x => x.Name == modelName);
        }

        public bool IsAuditExist(string contentType, Syntax.EventTypes eventType)
        {
            return _auditSettingRepository.Table.Any(x => x.Name == contentType && x.EventType == eventType);
        }

        public void CreateAudit(Audit audit)
        {
            audit.User = _adminUserService.GetUser(_loginService.GetUserInfo().Id);
            _auditRepository.Insert(audit);
        }

        public void CreateAudit(List<Audit> audits)
        {
            var userId = _loginService.GetUserInfo().Id;
            foreach (var v in audits)
                v.User = _adminUserService.GetUser(userId);

            //#Todo Bulk insert doesnt add user id, needs to check
            _auditRepository.BulkInsert(audits);

            // _auditRepository.BulkUpdate(audits,x=>x.User.Id);

            // _auditRepository.InsertRange(audits);
            // _auditRepository.BulkInsert(audits);
        }

        public List<Audit> GetAudits()
        {
            return _auditRepository.Table.ToList();
        }

        public void DeleteAllAudits()
        {
            _auditRepository.DeleteAll();
        }

        public FilterModel<AuditViewModel> GetAuditWithFilter(int pageSize, int page, string filter)
        {
            var auditWithFilter = new FilterModel<AuditViewModel>();
            var includeProperties = new List<Expression<Func<Audit, object>>> {x => x.User};
            Expression<Func<Audit, bool>> criteriaFilter =
                x =>
                    x.Name.ToLower().Contains(filter.ToLower()) && x.ContentTitle.ToLower().Contains(filter.ToLower()) &&
                    x.User.UserName.ToLower().Contains(filter.ToLower());
            if (filter != string.Empty)
                auditWithFilter.Records =
                    GetAuditViewModelList(_auditRepository.Get(criteriaFilter, o => o.OrderByDescending(x => x.Id),
                        includeProperties, page, pageSize));
            else
                auditWithFilter.Records =
                    GetAuditViewModelList(_auditRepository.Get(null, o => o.OrderByDescending(x => x.Id),
                        includeProperties, page, pageSize));
            auditWithFilter.CurrentPage = page;
            auditWithFilter.DisplayingRecords = auditWithFilter.Records.Count();
            auditWithFilter.TotalRecords = _auditRepository.TotalRecords();
            if (filter != string.Empty)
                auditWithFilter.TotalPages = (_auditRepository.Get(criteriaFilter).Count() + pageSize - 1)/pageSize;
            else
                auditWithFilter.TotalPages = (auditWithFilter.TotalRecords + pageSize - 1)/pageSize;
            auditWithFilter.NextEnable = auditWithFilter.CurrentPage < auditWithFilter.TotalPages - 1;
            auditWithFilter.PreviousEnable = auditWithFilter.CurrentPage > 0;
            return auditWithFilter;
        }

        public void SaveChangesForAudits()
        {
            _auditRepository.SaveChanges();
        }

        public ICollection<AuditViewModel> GetAuditViewModelList(IEnumerable<Audit> auditList)
        {
            return
                auditList.Select(
                    audit =>
                        new AuditViewModel
                        {
                            Id = audit.Id,
                            Title = audit.Title,
                            Name = audit.Name,
                            ContentTitle = audit.ContentTitle,
                            ContentType = audit.ContentType,
                            ContentId = audit.ContentId,
                            AdminUser = audit.User != null ? audit.User.Name : null,
                            ActionDateTime = audit.ActionDateTime
                        }).ToList();
        }
    }
}