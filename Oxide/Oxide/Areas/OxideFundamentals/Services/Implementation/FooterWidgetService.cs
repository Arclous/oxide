﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Oxide.Core.Data.Repository;
using Oxide.Core.Extesions;
using Oxide.Fundamentals.Models.Footer;
using Oxide.Fundamentals.Models.Menu;
using Oxide.Fundamentals.Services.Interface;
using Oxide.Fundamentals.ViewModels.Footer;

namespace Oxide.Fundamentals.Services.Implementation
{
    public class FooterWidgetService : IFooterWidgetService
    {
        private readonly IOxideRepository<FooterWidget> _footerWidgetRepository;

        private readonly Expression<Func<Menu, object>>[] _includesMenu =
        {
            x =>
                x.Menus.Select(
                    l1 =>
                        l1.Menus.Select(
                            l2 => l2.Menus.Select(l3 => l3.Menus.Select(l4 => l4.Menus.Select(l5 => l5.Menus)))))
        };

        private readonly Expression<Func<FooterWidget, object>>[] _includesMenuWidget =
        {
            x =>
                x.Menu.Menus.Select(
                    l1 =>
                        l1.Menus.Select(
                            l2 => l2.Menus.Select(l3 => l3.Menus.Select(l4 => l4.Menus.Select(l5 => l5.Menus)))))
        };

        private readonly IOxideRepository<Menu> _menusRepository;

        public FooterWidgetService(IOxideRepository<FooterWidget> footerWidgetRepository,
            IOxideRepository<Menu> menusRepository)
        {
            _footerWidgetRepository = footerWidgetRepository;
            _menusRepository = menusRepository;
        }

        public void UpdateSettings(FooterWidgetEditorViewModel footerWidgetData)
        {
            footerWidgetData.Menu = _menusRepository.GetById(footerWidgetData.MenuId);
            var updateMode =
                _footerWidgetRepository.GetAll(_includesMenuWidget).FirstOrDefault(x => x.Key == footerWidgetData.Key);
            if (updateMode != null)
            {
                footerWidgetData.CopyPropertiesTo(updateMode, new[] {"Id"});
                _footerWidgetRepository.Update(updateMode);
            }
            else
            {
                var menuWidget = new FooterWidget();
                footerWidgetData.CopyPropertiesTo(menuWidget);
                _footerWidgetRepository.Insert(menuWidget);
            }
        }

        public void DeleteSettings(int key)
        {
            _footerWidgetRepository.DeleteAll(x => x.Key == key);
        }

        public FooterWidgetEditorViewModel LoadSettings(int key)
        {
            var footer = new FooterWidgetEditorViewModel();
            var updateMode = _footerWidgetRepository.SearchFor(x => x.Key == key, _includesMenuWidget).FirstOrDefault();
            updateMode?.CopyPropertiesTo(footer);
            if (updateMode?.Menu != null) footer.SelectedContents = updateMode.Menu.Id.ToString();
            return footer;
        }

        public Menu GetMenu(int id)
        {
            return _menusRepository.GetById(id, _includesMenu);
        }
    }
}