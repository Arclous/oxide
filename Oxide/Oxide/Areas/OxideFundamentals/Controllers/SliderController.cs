﻿using Oxide.Core.Base.OxideController;
using Oxide.Core.Services.OxideService;

namespace Oxide.Fundamentals.Controllers
{
    public partial class SliderController : OxideController
    {
        public SliderController()
        {
        }

        public SliderController(IOxideServices oxideServices) : base(oxideServices)
        {
        }
    }
}