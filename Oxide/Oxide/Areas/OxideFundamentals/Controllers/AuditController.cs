﻿using System.Linq;
using System.Web.Mvc;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideController;
using Oxide.Core.Extesions;
using Oxide.Core.Models.Base;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Syntax;
using Oxide.Fundamentals.Services.Interface;
using Oxide.Fundamentals.ViewModels.Audit;

namespace Oxide.Fundamentals.Controllers
{
    public partial class AuditController : OxideController
    {
        private readonly IAuditService _auditService;

        public AuditController()
        {
        }

        public AuditController(IOxideServices oxideServices, IAuditService auditService) : base(oxideServices)
        {
            _auditService = auditService;
        }

        [OxideAdmin]
        [OxideAdminPermission(Group = "Audit", Permission = "Can Manage Audit Settings",
            Key = "Can_Manage_Audit_Settings")]
        public virtual ActionResult AuditSettings()
        {
            var resultModel = TempData["AuditSetting"] as OxideModel;
            if (resultModel == null)
                return
                    View(new OxideModel
                    {
                        RequestType = Syntax.Requests.Static,
                        Header = OxideServices.GetText("Audit Settings"),
                        Title = Constants.Empty
                    });
            TempData["AuditSetting"] = null;
            return View(resultModel);
        }

        [OxideAdmin]
        [OxideAdminPermission(Group = "Audit", Permission = "Can Manage Audit Settings",
            Key = "Can_Manage_Audit_Settings")]
        public virtual ActionResult NewAuditSetting()
        {
            var newAuditSetting = new AuditSettingEditorViewModel();
            var resultModel = TempData["AuditSetting"] as OxideModel;
            var selectContentList =
                _auditService.GetModelsForAudit().Select(v => new SelectListItem {Text = v, Value = v}).ToList();
            if (resultModel != null)
            {
                ((dynamic) resultModel.Parameters).Models = selectContentList;
                return View(resultModel);
            }
            newAuditSetting.Models = selectContentList;
            TempData["AuditSetting"] = null;
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("New Audit Setting"),
                    Title = Constants.Empty,
                    Parameters = newAuditSetting
                });
        }

        [OxideAdmin]
        [OxideAdminPermission(Group = "Audit", Permission = "Can Manage Audit Settings",
            Key = "Can_Manage_Audit_Settings")]
        public virtual ActionResult SaveAuditSetting(AuditSettingEditorViewModel model)
        {
            var oxideModel = new OxideModel {RequestType = Syntax.Requests.Static};
            if (!_auditService.IsAuditExist(model.ContentSelected, model.EventType))
            {
                _auditService.CreateNewAuditSetting(model.ContentSelected, (int) model.EventType, model.Active);
                oxideModel.SuccessMessage = OxideServices.GetText("New audit setting created");
                TempData["AuditSetting"] = oxideModel;
                return RedirectToAction("AuditSettings");
            }
            model.EventSelected = (int) model.EventType;
            oxideModel.Header = OxideServices.GetText("New Audit Setting");
            oxideModel.ErrorMessage = OxideServices.GetText("Audit setting alreay exist");
            oxideModel.Parameters = model;
            TempData["AuditSetting"] = oxideModel;
            return RedirectToAction("NewAuditSetting", "Audit", new {area = "Oxide.Fundamentals"});
        }

        [OxideAdmin]
        [OxideAdminPermission(Group = "Audit", Permission = "Can Manage Audit Settings",
            Key = "Can_Manage_Audit_Settings")]
        public virtual ActionResult EditAuditSetting(int id)
        {
            var editAuditSetting = new AuditSettingEditorViewModel();
            var resultModel = TempData["AuditSetting"] as OxideModel;
            var selectContentList =
                _auditService.GetModelsForAudit().Select(v => new SelectListItem {Text = v, Value = v}).ToList();
            if (resultModel != null)
            {
                ((dynamic) resultModel.Parameters).Models = selectContentList;
                return View(resultModel);
            }
            var auditSetting = _auditService.GetAuditSetting(id);
            auditSetting.CopyPropertiesTo(editAuditSetting);
            editAuditSetting.EventSelected = (int) auditSetting.EventType;
            editAuditSetting.ContentSelected = auditSetting.Name;
            editAuditSetting.Models = selectContentList;
            if (editAuditSetting.Active)
                editAuditSetting.IsActive = Constants.Checked;
            TempData["AuditSetting"] = null;
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Edit Audit Setting"),
                    Title = Constants.Empty,
                    Parameters = editAuditSetting
                });
        }

        [OxideAdmin]
        [OxideAdminPermission(Group = "Audit", Permission = "Can Manage Audit Settings",
            Key = "Can_Manage_Audit_Settings")]
        public virtual ActionResult UpdateAuditSetting(AuditSettingEditorViewModel model)
        {
            var oxideModel = new OxideModel {RequestType = Syntax.Requests.Static};
            var existModel = _auditService.GetAuditSetting(model.ContentSelected, model.EventType);
            if (existModel == null)
            {
                _auditService.UpdateAuditSetting(model.ContentSelected, (int) model.EventType, model.Active, model.Id);
                oxideModel.SuccessMessage = OxideServices.GetText("Audit setting updated");
                TempData["AuditSetting"] = oxideModel;
                return RedirectToAction("AuditSettings");
            }
            if (existModel.Id == model.Id)
            {
                _auditService.UpdateAuditSetting(model.ContentSelected, (int) model.EventType, model.Active, model.Id);
                oxideModel.SuccessMessage = OxideServices.GetText("Audit setting updated");
                TempData["AuditSetting"] = oxideModel;
                return RedirectToAction("AuditSettings");
            }
            model.EventSelected = (int) model.EventType;
            oxideModel.Header = OxideServices.GetText("Edit Audit Setting");
            oxideModel.ErrorMessage = OxideServices.GetText("Audit setting alreay exist");
            oxideModel.Parameters = model;
            TempData["AuditSetting"] = oxideModel;
            return RedirectToAction("EditAuditSetting", "Audit", new {area = "Oxide.Fundamentals"});
        }

        [OxideAdmin]
        [OxideAdminPermission(Group = "Audit", Permission = "Can Manage Audit Settings",
            Key = "Can_Manage_Audit_Settings")]
        public virtual JsonResult DeleteAuditSetting(int id)
        {
            _auditService.DeleteAuditSetting(id);
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [OxideAdmin]
        [OxideAdminPermission(Group = "Audit", Permission = "Can Display Audits", Key = "Can_Display_Audits")]
        public virtual ActionResult DisplayAudits()
        {
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Audits"),
                    Title = Constants.Empty
                });
        }

        [OxideAdmin]
        [OxideAdminPermission(Group = "Audit", Permission = "Can Manage Audit Settings",
            Key = "Can_Manage_Audit_Settings")]
        public virtual JsonResult DeleteAudits()
        {
            _auditService.DeleteAllAudits();
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [OxideAdmin]
        [OxideAdminPermission(Group = "Audit", Permission = "Can Display Audits", Key = "Can_Display_Audits")]
        [HttpGet]
        public virtual JsonResult GetAudits(int? pageSize, int? page, string filter)
        {
            if (pageSize == null) pageSize = 10;
            if (page == null) page = 0;
            var cacheItemList = _auditService.GetAuditWithFilter((int) pageSize, (int) page, filter);
            return Json(cacheItemList, JsonRequestBehavior.AllowGet);
        }
    }
}