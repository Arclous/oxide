﻿using Oxide.Core.Base.OxideController;
using Oxide.Core.Services.OxideService;

namespace Oxide.Fundamentals.Controllers
{
    public partial class CarouselController : OxideController
    {
        public CarouselController()
        {
        }

        public CarouselController(IOxideServices oxideServices) : base(oxideServices)
        {
        }
    }
}