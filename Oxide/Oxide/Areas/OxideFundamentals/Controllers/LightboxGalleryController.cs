﻿using Oxide.Core.Base.OxideController;
using Oxide.Core.Services.OxideService;

namespace Oxide.Fundamentals.Controllers
{
    public partial class LightboxGalleryController : OxideController
    {
        public LightboxGalleryController()
        {
        }

        public LightboxGalleryController(IOxideServices oxideServices) : base(oxideServices)
        {
        }
    }
}