﻿using System;
using System.Globalization;
using System.Web.Mvc;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideController;
using Oxide.Core.Managers.CultureManager;
using Oxide.Core.Managers.LayoutManager;
using Oxide.Core.Managers.PageManager;
using Oxide.Core.Models.Pages;
using Oxide.Core.Services.OxideService;

namespace Oxide.Fundamentals.Controllers
{
    public partial class FrontController : OxideController
    {
        private readonly ICultureManager _cultureManager;
        private readonly ILayoutManager _layoutManager;
        private readonly IPageManager _pageManager;

        public FrontController()
        {
        }

        public FrontController(IOxideServices oxideServices, IPageManager pageManager, ILayoutManager layoutManager,
            ICultureManager cultureManager) : base(oxideServices)
        {
            _pageManager = pageManager;
            _layoutManager = layoutManager;
            _cultureManager = cultureManager;
        }

        // GET: Front
        public virtual ActionResult ChangeCulture(string calture, string pageUrl)
        {
            var culture = CultureInfo.GetCultureInfo(calture);
            _cultureManager.SetFrontCulture(culture, Response);
            return Redirect(pageUrl);
        }

        [Front]
        public virtual ActionResult TestTest()
        {
            var page = new Page();
            page.Name = "Test Page";
            var masterLayout = _layoutManager.GetMasterLayout("Main Layout 1");
            var innerLayout = _layoutManager.GetInnerLayout("Inner Template 1");
            page.AggresiveCacheEnabled = false;
            page.BasePageId = -1;
            page.CreateDate = DateTime.Now;
            page.Description = "Dynamic Page";
            page.HomePage = false;
            page.InnerLayoutId = innerLayout.Id;
            page.MainLayoutId = masterLayout.Id;
            return View(_pageManager.GetRenderPageModel(page));
        }
    }
}