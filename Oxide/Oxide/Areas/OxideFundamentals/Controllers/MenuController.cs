﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideController;
using Oxide.Core.Extesions;
using Oxide.Core.Models;
using Oxide.Core.Models.Base;
using Oxide.Core.Services.LanguageService;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Syntax;
using Oxide.Fundamentals.Models.Menu;
using Oxide.Fundamentals.Services.Interface;
using Oxide.Fundamentals.ViewModels.Menu;

namespace Oxide.Fundamentals.Controllers
{
    public partial class MenuController : OxideController
    {
        private readonly ILanguageService _languageService;
        private readonly IMenuService _menuService;

        public MenuController()
        {
        }

        public MenuController(IOxideServices oxideServices, IMenuService menuService, ILanguageService languageService)
            : base(oxideServices)
        {
            _menuService = menuService;
            _languageService = languageService;
        }

        #region Menus

        [OxideAdmin]
        [OxideAdminPermission(Group = "Menu Widget", Permission = "Can Display Menu List", Key = "Can_Display_Menu_List"
            )]
        public virtual ActionResult DisplayMenus()
        {
            return View(new OxideModel {RequestType = Syntax.Requests.Static, Header = "Menus", Title = Constants.Empty});
        }

        [OxideAdmin]
        [OxideAdminPermission(Group = "Menu Widget", Permission = "Can Create Menu", Key = "Can_Create_Menu")]
        public virtual ActionResult NewMenu()
        {
            return
                View(new OxideModel {RequestType = Syntax.Requests.Static, Header = "New Menu", Title = Constants.Empty});
        }

        [OxideAdmin]
        [OxideAdminPermission(Group = "Menu Widget", Permission = "Can Update Menu", Key = "Can_Update_Menu")]
        public virtual ActionResult EditMenu(int menuId)
        {
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = "Edit Menu",
                    Title = Constants.Empty,
                    Parameters = GetEditMenuViewModel(menuId)
                });
        }

        [OxideAdminPermission(Group = "Menu Widget", Permission = "Can Create Menu", Key = "Can_Create_Menu")]
        public virtual ActionResult CreateMenu(Menu model)
        {
            _menuService.CreateMenu(model);
            return RedirectToAction("DisplayMenus");
        }

        [OxideAdminPermission(Group = "Menu Widget", Permission = "Can Update Menu", Key = "Can_Update_Menu")]
        public virtual ActionResult UpdateMenu(MenuViewModel model)
        {
            _menuService.UpdateMenu(model);
            return RedirectToAction("DisplayMenus");
        }

        #endregion Menus

        #region MenuItems

        [OxideAdmin]
        [OxideAdminPermission(Group = "Menu Widget", Permission = "Can Display Menu Items",
            Key = "Can_Display_Menu_Items")]
        public virtual ActionResult DisplayMenuItems(int menuId)
        {
            var urlHelper = new UrlHelper(System.Web.HttpContext.Current.Request.RequestContext);
            var menu = _menuService.GetMenu(menuId);

            //menu.Menus.Remove()
            var menuParameters = new MainMenuPacket
            {
                MenuId = menuId,
                AddUrl =
                    urlHelper.Action(MVCOxideFundamentals.Menu.ActionNames.NewMenuItem, MVCOxideFundamentals.Menu.Name,
                        new {area = "Oxide.Fundamentals"}),
                RemoveUrl =
                    urlHelper.Action(MVCOxideFundamentals.Menu.ActionNames.DeleteMenuItem,
                        MVCOxideFundamentals.Menu.Name, new {area = "Oxide.Fundamentals"}),
                EditUrl =
                    urlHelper.Action(MVCOxideFundamentals.Menu.ActionNames.EditMenuItem, MVCOxideFundamentals.Menu.Name,
                        new {area = "Oxide.Fundamentals"})
            };
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = "Menu Items",
                    Title = menu == null ? "" : menu.Title,
                    Parameters = menuParameters
                });
        }

        [OxideAdmin]
        [OxideAdminPermission(Group = "Menu Widget", Permission = "Can Create Menu Item", Key = "Can_Create_Menu_Item")]
        public virtual ActionResult NewMenuItem(int parentId, int menuId, int top)
        {
            var newMenuItemPacket = new NewMenuItemPacket {ParentId = parentId, MenuId = menuId, Top = top};
            var selectList =
                _languageService.GetAllActiveLanguages()
                                .Select(v => new SelectListItem {Text = v.Native, Value = v.Id.ToString()})
                                .ToList();
            newMenuItemPacket.LanguageSelectList = selectList;
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = "New Menu Item",
                    Title = Constants.Empty,
                    Parameters = newMenuItemPacket
                });
        }

        [OxideAdminPermission(Group = "Menu Widget", Permission = "Can Create Menu Item", Key = "Can_Create_Menu_Item")]
        public virtual ActionResult CreateMenuItem(MenuItemViewModel model)
        {
            var menuItem = new MenuItem();
            model.CopyPropertiesTo(menuItem);
            menuItem.Menus = new List<MenuItem>();
            menuItem.Language = _languageService.GetLanguage(model.LanguageSelected);
            menuItem.LinkType = (LinkTypes) model.LinkTypeSelected;
            menuItem.OpenType = (LinkOpenTypes) model.OpenTypeSelected;
            menuItem.PageId = Convert.ToInt32(model.SelectedContents);
            if (model.Top == 1)
            {
                menuItem.IsChild = false;
                _menuService.CreateMenuItem(menuItem);
            }
            else
            {
                var parentMenu = _menuService.GetMenuItem(model.ParentId);
                menuItem.IsChild = true;
                parentMenu.Menus.Add(menuItem);
                var parentMenuItem = new MenuItemViewModel();
                parentMenu.CopyPropertiesTo(parentMenuItem);
                _menuService.UpdateMenuItem(parentMenuItem);
            }
            return RedirectToAction("DisplayMenuItems", new {menuId = model.MenuId});
        }

        [OxideAdmin]
        [OxideAdminPermission(Group = "Menu Widget", Permission = "Can Update Menu Item", Key = "Can_Update_Menu_Item")]
        public virtual ActionResult EditMenuItem(int id)
        {
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = "Edit Menu",
                    Title = Constants.Empty,
                    Parameters = GetEditMenuItemViewModel(id)
                });
        }

        [HttpGet]
        [OxideAdminPermission(Group = "Menu Widget", Permission = "Can Delete Menu Item", Key = "Can_Delete_Menu_Item")]
        public virtual JsonResult DeleteMenuItem(int id, int menuId)
        {
            _menuService.DeleteMenuItem(id);
            return MenuItemList(menuId);
        }

        [OxideAdminPermission(Group = "Menu Widget", Permission = "Can Update Menu Item", Key = "Can_Update_Menu_Item")]
        public virtual ActionResult UpdateMenuItem(MenuItemViewModel model)
        {
            model.Language = _languageService.GetLanguage(model.LanguageSelected);
            model.LinkType = (LinkTypes) model.LinkTypeSelected;
            model.OpenType = (LinkOpenTypes) model.OpenTypeSelected;
            model.PageId = Convert.ToInt32(model.SelectedContents);
            _menuService.UpdateMenuItem(model);
            return RedirectToAction("DisplayMenuItems", new {menuId = model.MenuId});
        }

        #endregion

        #region Helpers

        [HttpGet]
        [OxideAdminPermission(Group = "Menu Widget", Permission = "Can Display Menu List", Key = "Can_Display_Menu_List"
            )]
        public virtual JsonResult MenuList(int? pageSize, int? page, string filter)
        {
            if (pageSize == null) pageSize = 10;
            if (page == null) page = 0;
            var pageList = _menuService.GetMenusWithFilter((int) pageSize, (int) page, filter);
            return Json(pageList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = "Menu Widget", Permission = "Can Delete Menu", Key = "Can_Delete_Menu")]
        public virtual JsonResult DeleteMenu(int menuId)
        {
            _menuService.DeleteMenu(menuId);
            var result = new JsonPageActionResultModel {Success = true};
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [OxideAdminPermission(Group = "Menu Widget", Permission = "Can Display Menu List", Key = "Can_Display_Menu_List"
            )]
        public MenuViewModel GetEditMenuViewModel(int menuId)
        {
            var menuViewModel = new MenuViewModel();
            _menuService.GetMenu(menuId).CopyPropertiesTo(menuViewModel);
            return menuViewModel;
        }

        private MenuItemViewModel GetEditMenuItemViewModel(int id)
        {
            var menuViewModel = new MenuItemViewModel();
            var menu = _menuService.GetMenuItem(id);
            menu.CopyPropertiesTo(menuViewModel);
            var selectList =
                _languageService.GetAllActiveLanguages()
                                .Select(v => new SelectListItem {Text = v.Native, Value = v.Id.ToString()})
                                .ToList();
            menuViewModel.SelectedContents = menuViewModel.PageId.ToString();
            menuViewModel.LanguageSelected = menu.Language.Id;
            menuViewModel.LinkTypeSelected = (int) menuViewModel.LinkType;
            menuViewModel.OpenTypeSelected = (int) menuViewModel.OpenType;
            menuViewModel.LanguageSelectList = selectList;
            return menuViewModel;
        }

        [HttpGet]
        [OxideAdminPermission(Group = "Menu Widget", Permission = "Can Display Menu List", Key = "Can_Display_Menu_List"
            )]
        public virtual JsonResult MenuItemList(int menuId)
        {
            var menuPacket = new MenuPacket();
            var mainMenu = _menuService.GetMenu(menuId);
            foreach (var subMenu in mainMenu.Menus.Where(x => x.ParentId == menuId && !x.IsChild))
            {
                var subMenuItem = new TreeViewItemModel
                {
                    Id = subMenu.Id,
                    Name = subMenu.Name,
                    Childeren = new List<TreeViewItemModel>()
                };
                AddSubMenus(subMenu.Menus, subMenuItem);
                menuPacket.Menus.Add(subMenuItem);
            }
            return Json(menuPacket, JsonRequestBehavior.AllowGet);
        }

        [OxideAdminPermission(Group = "Menu Widget", Permission = "Can Add Sub Menus", Key = "Can_Add_Sub_Menus")]
        private static void AddSubMenus(IEnumerable<MenuItem> menuItems, TreeViewItemModel target)
        {
            target.Childeren = new List<TreeViewItemModel>();
            foreach (var item in menuItems)
            {
                var menuteItemModel = new TreeViewItemModel
                {
                    Id = item.Id,
                    Name = item.Name,
                    Childeren = new List<TreeViewItemModel>()
                };
                AddSubMenus(item.Menus, menuteItemModel);
                target.Childeren.Add(menuteItemModel);
            }
        }

        #endregion
    }
}