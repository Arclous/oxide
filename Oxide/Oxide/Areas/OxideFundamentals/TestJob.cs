﻿using System;
using Oxide.Core.Services.Implemantation;
using Oxide.Core.Syntax;
using Quartz;

namespace Oxide.Fundamentals
{
    public class TestJob : OxideScheduledBackgroundJob
    {
        public override string Name { get; } = "Test_Job2";
        public override string Title { get; } = "Test Job2";
        public override bool CanForceToRun { get; } = false;
        public override bool CanManage { get; } = true;
        public override Syntax.BackgroundTaskActionType ActionType { get; } = Syntax.BackgroundTaskActionType.Daily;

        public override Action<DailyTimeIntervalScheduleBuilder> DailyAction { get; } =
            s => s.WithIntervalInSeconds(15).OnEveryDay();

        public override Action<CalendarIntervalScheduleBuilder> Action { get; }
        public override void Execute(IJobExecutionContext context)
        {
        }
    }
}