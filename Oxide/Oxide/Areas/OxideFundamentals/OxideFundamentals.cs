﻿using Oxide.Core.Elements;

namespace Oxide.Fundamentals
{
    public class OxideFundamentals : IOxideModule
    {
        public int Id { get; } = 1;

        public string Name { get; } = "Oxide.Fundamentals";

        public string Title { get; } = "Oxide Fundamentals";

        public string Description { get; } = "Fundamentals widgets and admin pages for the cms";

        public string Author { get; } = "Inevera Studios";

        public string Category { get; } = "Fundamentals";

        public string PreviewImageUrl { get; } = "/Areas/OxideFundamentals/Content/images/oxide.png";

        public bool Active { get; set; }

        public string Dependencies { get; set; } = "ModuleX,Front";
    }
}