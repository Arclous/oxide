﻿ko.bindingHandlers.bootstrapModal = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var props = valueAccessor(),
            vm = bindingContext.createChildContext(viewModel);
        ko.utils.extend(vm, props);
        vm.close = function() {
            vm.show(false);
            vm.onClose();
        };
        vm.action = function() {
            vm.onAction();
        };
        ko.utils.toggleDomNodeCssClass(element, "modal fade", true);
        ko.renderTemplate("deleteConfirmTemplate", vm, null, element);
        var showHide = ko.computed(function() {
            $(element).modal(vm.show() ? "show" : "hide");
        });
        return {
            controlsDescendantBindings: true
        };
    }
};

function NodeView(urlBase, addUrl, removeUrl, editUrl) {
    var self = this;
    self.selectedNode = ko.observable();
    self.menuId = ko.observable();
    self.elementClass = ko.observable();
    self.menuAddUrl = ko.observable(addUrl);
    self.menuEditUrl = ko.observable(editUrl);
    self.menuRemoveUrl = ko.observable(removeUrl);
    self.nodes = ko.observableArray([]),
        self.isLoading = ko.observable(false);
    self.loadData = function(menuId) {
        self.isLoading(true);
        self.menuId(menuId);
        self.selectedNode(menuId);
        $.get(urlBase, { menuId: menuId }, function(returnData) {
            self.nodes(returnData.Menus);
            self.isLoading(false);
        });
    };

    self.getAddUrl = function() {
        return self.menuAddUrl() + "/?parentId=" + self.selectedNode().Id + "&menuId=" + self.menuId() + "&top=0";
    };
    self.getAddParentUrl = function() {
        return self.menuAddUrl() + "/?parentId=" + self.menuId() + "&menuId=" + self.menuId() + "&top=1";
    };
    self.getEditUrl = function() {
        return self.menuEditUrl() + "/?id=" + self.selectedNode().Id;
    };

    self.removeNode = function() {
        self.isLoading(true);
        $.get(self.menuRemoveUrl(), { id: self.selectedNode().Id, menuId: self.menuId() }, function(returnData) {
            self.nodes(returnData.Menus);
            self.isLoading(false);
            self.modal.show(false);
        });
    };
    self.selectNode = function(node) {
        self.resetSelecteds();
        self.selectedNode(node);
        $("#item_" + node.Id).toggleClass("btn-success btn-xs");

    };
    self.resetSelecteds = function() {
        $(".nodeItem").removeClass("btn-success btn-xs");
    };
    self.modal = {
        header: DisplayMenuItems.Please_Confirm,
        message: ko.observable(DisplayMenuItems.Do_you_want_to_delete_),
        comment: ko.observable(""),
        closeLabel: DisplayMenuItems.Cancel,
        primaryLabel: DisplayMenuItems.Delete,
        show: ko.observable(false),
        onClose: function() {
            self.onModalClose();
        },
        onAction: function() {
            self.removeNode();
        }
    };
    self.showModal = function(menu) {
        self.modal.message(DisplayMenuItems.Do_you_want_to_delete_this_menu_);
        self.modal.show(true);
    };
    self.onModalClose = function() {

    };
}

function TreeViewItemModel(id, name, childeren) {
    var self = this;
    self.Id = ko.observable(id);
    self.Name = ko.observable(name);
    self.Childeren = ko.observableArray(childeren);

}