﻿function AuditItemsModel(urlBase, clearUrlGeneral) {
    var self = this;
    self.isLoading = ko.observable(false);
    self.totalRecords = ko.observable();
    self.totalPage = ko.observable();
    self.currentPage = ko.observable();
    self.displayinRecords = ko.observable();
    self.nextEnable = ko.observable(true);
    self.previousEnable = ko.observable(true);
    self.auditItems = ko.observableArray([]);
    self.clearAuditsUrl = ko.observable("");
    self.valueUpdateUrl = ko.observable("");
    self.pageSize = ko.observable();
    self.actionFilter = ko.observable("");
    self.actionCurrentPage = ko.observable(0);
    self.actionPageSize = ko.observable(10);
    self.clearAllAuditsUrl = ko.observable(clearUrlGeneral);
    self.loadData = function(pagesize, page, filter) {
        self.isLoading(true);
        $.get(urlBase, { pageSize: pagesize, page: page, filter: filter }, function(data) {
            self.auditItems.removeAll();
            for (var i = 0; i <= data.Records.length - 1; i++) {
                var auditItemModel = new AuditItemModel(
                    data.Records[i].Id,
                    data.Records[i].Name,
                    data.Records[i].Title,
                    data.Records[i].ContentTitle,
                    data.Records[i].ContentId,
                    data.Records[i].AdminUser,
                    data.Records[i].ContentType,
                    data.Records[i].ActionDateTime
                );
                self.addCacheItem(auditItemModel);
            }
            self.clearAuditsUrl(data.ClearCacheUrl);
            self.valueUpdateUrl(data.UpdateValueUrl);
            self.totalRecords(data.TotalRecords);
            self.totalPage(data.TotalPages);
            self.currentPage(data.CurrentPage);
            self.displayinRecords(data.DisplayingRecords);
            self.nextEnable(data.NextEnable);
            self.previousEnable(data.PreviousEnable);
            self.isLoading(false);
            self.setControls();
        });
    };
    self.addCacheItem = function(model) {
        self.auditItems.push(model);
    }.bind(self);

    self.clearCache = function(model) {
        model.isWorking(true);

        $.get(self.clearAuditsUrl(), { cacheKey: model.name() }, function(data) {
            model.status(false);
            model.isWorking(false);
        });
    }.bind(self);

    self.clearAllAudits = function(model) {
        model.isLoading(true);

        $.get(self.clearAllAuditsUrl(), function(data) {
            self.loadData(self.pageSize(), self.currentPage(), self.actionFilter());
            model.isLoading(false);
        });
    }.bind(self);

    self.updateValue = function(model) {
        model.isWorking(true);
        $.get(self.valueUpdateUrl(), { cacheKey: model.name(), value: model.valueOrj() }, function(data) {
            model.isWorking(false);
            new PNotify({
                title: Audit.Cache_Settings_Updated,
                text: Audit.Cache_settings_updated_successfully_,
                type: "success",
                styling: "bootstrap3"
            });
        });
    }.bind(self);
    self.setControls = function() {
        $("#datatable-checkbox_info").html(Audit.Showing + " " + (self.currentPage() + 1) + " / " + self.totalPage());
        $(".dataTables_empty").html("");

        $("#datatable-checkbox_next").unbind();
        $("#datatable-checkbox_previous").unbind();
        $("#datatable-checkbox_next").addClass("disabled");
        $("#datatable-checkbox_previous").addClass("disabled");

        if (self.nextEnable()) {
            $("#datatable-checkbox_next").removeClass("disabled");
            $("#datatable-checkbox_next").click(function() {
                self.actionCurrentPage(self.actionCurrentPage() + 1);
                self.loadData(self.actionPageSize(), self.actionCurrentPage(), self.actionFilter());
            });
        }
        if (self.previousEnable()) {
            $("#datatable-checkbox_previous").removeClass("disabled");
            $("#datatable-checkbox_previous").click(function() {
                self.actionCurrentPage(self.actionCurrentPage() - 1);
                self.loadData(self.actionPageSize(), self.actionCurrentPage(), self.actionFilter());
            });
        }
    }.bind(self);
}

function AuditItemModel(id, name, title, contentTitle, contentId, adminUser, contentType, actionDateTime) {
    var self = this;
    self.isWorking = ko.observable(false);
    self.id = ko.observable(id);
    self.name = ko.observable(name);
    self.title = ko.observable(title);
    self.contentTitle = ko.observable(contentTitle);
    self.contentId = ko.observable(contentId);
    self.adminUser = ko.observable(adminUser);
    self.contentType = ko.observable(contentType);
    self.actionDateTime = ko.observable(actionDateTime);

}