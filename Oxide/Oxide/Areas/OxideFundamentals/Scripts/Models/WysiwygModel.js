﻿var language;
ko.bindingHandlers.ckeditor = {
    init: function(element, valueAccessor, allBindingsAccessor, context) {
        var options = allBindingsAccessor().ckeditorOptions || {};
        var modelValue = valueAccessor();
        var value = ko.utils.unwrapObservable(valueAccessor());

        $(element).html(value);
        $(element).ckeditor();

        var editor = $(element).ckeditorGet();

        editor.config.extraPlugins = "pastetext";
        editor.config.forcePasteAsPlainText = true;
        editor.config.skin = "kama";
        editor.config.language = language;
        editor.config.filebrowserBrowseUrl = "/Oxide.Media/FileManager/DialogFileManager";
        editor.config.filebrowserImageBrowseUrl = "/Oxide.Media/FileManager/DialogMediaManager";

        //handle disposal (if KO removes by the template binding)
        ko.utils.domNodeDisposal.addDisposeCallback(element, function() {
            if (editor) {
                CKEDITOR.remove(editor);
            };
        });
    },
    update: function(element, valueAccessor, allBindingsAccessor, context) {
        // handle programmatic updates to the observable
        var newValue = ko.utils.unwrapObservable(valueAccessor());
        if ($(element).ckeditorGet().getData() != newValue)
            $(element).ckeditorGet().setData(newValue);
    }
};

function WysiwygModel(lng) {
    var self = this;
    language = lng;
    self.htmlData = ko.observable();
}