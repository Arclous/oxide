﻿using System;
using System.Dynamic;
using Oxide.Core.Elements;
using Oxide.Core.Extesions;
using Oxide.Core.Managers.CacheManager;
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.Models.Base;
using Oxide.Core.ResultModels;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Syntax;
using Oxide.Fundamentals.Services.Interface;
using Oxide.Fundamentals.ViewModels.Menu;

namespace Oxide.Fundamentals.Widgets
{
    public class NavigationMenu : IOxideWidget
    {
        #region WidgetBasic

        public int Id => 1;

        public string Name => "Navigation Menu";

        public string Title => "Navigation Menu";

        public string Description
            => "The widget which can be used to create and display manage menus for navigation of the website.";

        public string Category => "Navigation";

        public string PreviewImageUrl => "/areas/OxideFundamentals/content/images/widgets/Navigation.png";

        public bool Active { get; set; }

        public string ModuleName => "Oxide.Fundamentals";

        public string FrontView { get; set; } = "NavigationMenu";

        public string EditorView { get; set; } = "NavigationMenu_Editor";

        public string WidgetKey { get; set; }

        public string Dependencies { get; set; }

        #endregion

        #region Renders

        WidgetResult IOxideWidget.RenderFront(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var menuWidgetService = providerManager.Provide<IMenuService>();
            var cacheManager = providerManager.Provide<IOxideCacheManager>();
            var model = cacheManager.GetOrSet(widgetInfoPackage.GetUniqueHashWithPageId().ToString(),
                widgetInfoPackage.Title, "Navigation Widget",
                () => menuWidgetService.LoadSettings(widgetInfoPackage.UniqeHash));
            var oxideModel = new OxideModel {RequestType = Syntax.Requests.Dynamic, Header = Name, Title = Title};
            if (model.Menu != null)
                oxideModel.Parameters = menuWidgetService.MenuItemListFront(model.Menu.Id, widgetInfoPackage.MainPageId);
            var widgetResult = new WidgetResult {ViewName = FrontView, Model = oxideModel};
            return widgetResult;
        }

        public WidgetResult RenderAdmin(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var menuWidgetService = providerManager.Provide<IMenuService>();
            var model = menuWidgetService.LoadSettings(widgetInfoPackage.UniqeHash);
            widgetInfoPackage.CopyPropertiesTo(model);
            var widgetResult = new WidgetResult
            {
                ViewName = EditorView,
                Model =
                    new OxideModel
                    {
                        RequestType = Syntax.Requests.Dynamic,
                        Header = widgetInfoPackage.Title,
                        Title = Title,
                        Parameters = model
                    }
            };
            return widgetResult;
        }

        public WidgetUpdatePackage RunUpdate(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager,
            ExpandoObject model)
        {
            var updateResult = new WidgetUpdatePackage();
            var menuService = providerManager.Provide<IMenuService>();
            var oxideServices = providerManager.Provide<IOxideServices>();
            var menuWidgetViewModel = new MenuWidgetViewModel {Key = widgetInfoPackage.UniqeHash};
            if (((dynamic) model).SelectedContents.ToString() != string.Empty)
                menuWidgetViewModel.Menu = menuService.GetMenu(Convert.ToInt32(((dynamic) model).SelectedContents));
            menuService.UpdateSettings(menuWidgetViewModel);
            updateResult.SuccessMessage = oxideServices.GetText("Navigation Menu saved successfully");
            return updateResult;
        }

        public void OnDeleted(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var menuService = providerManager.Provide<IMenuService>();
            menuService.DeleteSettings(widgetInfoPackage.UniqeHash);
        }

        #endregion
    }
}