﻿using Oxide.Core.Elements;
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.ResultModels;
using Oxide.Fundamentals.ViewModels;

namespace Oxide.Fundamentals.Widgets.Dashboard
{
    public class OnlineUsers : IOxideDashboardWidget
    {
        protected OnlineUserStatistic CreateViewModel()
        {
            var statistic = new OnlineUserStatistic();
            statistic.TotalOnlineUsers = Core.Helpers.OnlineUsers.TotalOnlineUser();
            statistic.TotalOfflineUsers = Core.Helpers.OnlineUsers.TotalOfflineUser();
            statistic.AverageTime = Core.Helpers.OnlineUsers.GetAverageTime();
            return statistic;
        }

        #region WidgetBasic
        public int Id => 1;
        public string Name => "Online Users";
        public string Title => "Online Users";
        public string Description => "Displays online users number";
        public string Category => "Online Statistics";
        public string PreviewImageUrl => "/areas/OxideFundamentals/content/images/widgets/Carousel.png";
        public bool Active { get; set; }
        public string ModuleName => "Oxide.Fundamentals";
        public string FrontView { get; set; } = "Dashboard_OnlineUsers";
        public string EditorView { get; set; } = "Dashboard_Dashboard_OnlineUsers_Editor";
        public string WidgetKey { get; set; }
        public bool Manageable { get; set; }
        #endregion

        #region Renders
        public WidgetResult RenderDashboard(DashboardWidgetInfoPackage widgetInfoPacket,
            IProviderManager providerManager)
        {
            var widgetResult = new WidgetResult {ViewName = FrontView, Model = CreateViewModel()};
            return widgetResult;
        }
        public WidgetResult RenderDashboardManagement(DashboardWidgetInfoPackage widgetInfoPacket,
            IProviderManager providerManager)
        {
            var widgetResult = new WidgetResult {ViewName = EditorView, Model = null};
            return widgetResult;
        }
        #endregion
    }
}