﻿using System.Linq;
using Oxide.Core.Elements;
using Oxide.Core.Helpers;
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.ResultModels;
using Oxide.Core.Services.PageService;
using Oxide.Fundamentals.ViewModels;

namespace Oxide.Fundamentals.Widgets.Dashboard
{
    public class TopPages : IOxideDashboardWidget
    {
        protected TopPagesPackageViewModel CreateViewModel(IProviderManager providerManager)
        {
            var viewModel = new TopPagesPackageViewModel();
            var pageService = providerManager.Provide<IPageService>();
            var popularPages = pageService.GetAllPages().OrderByDescending(x => x.TotalVisited).Take(5).ToList();
            viewModel.TotalVisit = popularPages.Sum(x => x.TotalVisited);
            var pages =
                popularPages.Select(
                    page =>
                        new TopPagesViewModel
                        {
                            PageName = page.Name,
                            TotalVisit = page.TotalVisited,
                            Url = page.Url,
                            Percent = OxideUtils.CalculatePercent(viewModel.TotalVisit, page.TotalVisited)
                        }).ToList();
            viewModel.Pages = pages;
            return viewModel;
        }

        #region WidgetBasic
        public int Id => 1;
        public string Name => "Top Pages";
        public string Title => "Top 5 Pages";
        public string Description => "Displays Top 5 popular pages.";
        public string Category => "Page Statistics";
        public string PreviewImageUrl => "/areas/OxideFundamentals/content/images/widgets/Carousel.png";
        public bool Active { get; set; }
        public string ModuleName => "Oxide.Fundamentals";
        public string FrontView { get; set; } = "Dashboard_TopPages";
        public string EditorView { get; set; } = "Dashboard_TopPages_Editor";
        public string WidgetKey { get; set; }
        public bool Manageable { get; set; }
        #endregion

        #region Renders
        public WidgetResult RenderDashboard(DashboardWidgetInfoPackage widgetInfoPacket,
            IProviderManager providerManager)
        {
            var widgetResult = new WidgetResult {ViewName = FrontView, Model = CreateViewModel(providerManager)};
            return widgetResult;
        }
        public WidgetResult RenderDashboardManagement(DashboardWidgetInfoPackage widgetInfoPacket,
            IProviderManager providerManager)
        {
            var widgetResult = new WidgetResult {ViewName = EditorView, Model = null};
            return widgetResult;
        }
        #endregion
    }
}