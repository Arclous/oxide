﻿using System;
using System.Dynamic;
using Oxide.Core.Elements;
using Oxide.Core.Extesions;
using Oxide.Core.Managers.CacheManager;
using Oxide.Core.Managers.CultureManager;
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.Models.Base;
using Oxide.Core.ResultModels;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Services.OxideUiService;
using Oxide.Core.Syntax;
using Oxide.Fundamentals.Services.Interface;
using Oxide.Fundamentals.ViewModels.Menu;

namespace Oxide.Fundamentals.Widgets
{
    public class CoolMenu : IOxideWidget
    {
        #region WidgetBasic

        public int Id => 1;

        public string Name => "Cool Menu";

        public string Title => "Cool Menu";

        public string Description
            => "The widget which can be used to create and display manage menus for navigation of the website.";

        public string Category => "Navigation";

        public string PreviewImageUrl => "/areas/OxideFundamentals/content/images/widgets/CoolMenu.png";

        public bool Active { get; set; }

        public string ModuleName => "Oxide.Fundamentals";

        public string FrontView { get; set; } = "CoolMenu";

        public string EditorView { get; set; } = "CoolMenu_Editor";

        public string WidgetKey { get; set; }

        public string Dependencies { get; set; }

        #endregion

        #region Renders

        WidgetResult IOxideWidget.RenderFront(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var menuWidgetService = providerManager.Provide<IMenuService>();
            var cacheManager = providerManager.Provide<IOxideCacheManager>();
            var oxideUiService = providerManager.Provide<IOxideUiService>();
            var cultureManager = providerManager.Provide<ICultureManager>();
            var model = cacheManager.GetOrSet(widgetInfoPackage.GetUniqueHashWithPageId().ToString(),
                widgetInfoPackage.Title, "Cool Menu Widget",
                () => menuWidgetService.LoadCoolMenuSettings(widgetInfoPackage.UniqeHash));
            var oxideModel = new OxideModel {RequestType = Syntax.Requests.Dynamic, Header = Name, Title = Title};
            var logo = oxideUiService.GetContent(Constants.FileContent, model.SelectedMedias);
            if (logo != null)
                model.LogoUrl = ((dynamic) logo).Path;
            if (model.Menu != null)
            {
                var coolMenuViewModelPackage = new CoolMenuViewModelPackage
                {
                    Menus =
                        cacheManager.GetOrSet(
                            "Cool Menu Widget Menus_" + cultureManager.GetFrontCurrentCulture().EnglishName,
                            widgetInfoPackage.Title + "_" + cultureManager.GetFrontCurrentCulture().EnglishName,
                            "Cool Menu Widget Menus ",
                            () => menuWidgetService.CollMenuItemListFront(model.Menu.Id, widgetInfoPackage.MainPageId)),
                    LogoUrl = model.LogoUrl
                };
                oxideModel.Parameters = coolMenuViewModelPackage;
            }
            var widgetResult = new WidgetResult {ViewName = FrontView, Model = oxideModel};
            return widgetResult;
        }

        public WidgetResult RenderAdmin(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var menuWidgetService = providerManager.Provide<IMenuService>();
            var model = menuWidgetService.LoadCoolMenuSettings(widgetInfoPackage.UniqeHash);
            widgetInfoPackage.CopyPropertiesTo(model);
            var widgetResult = new WidgetResult
            {
                ViewName = EditorView,
                Model =
                    new OxideModel
                    {
                        RequestType = Syntax.Requests.Dynamic,
                        Header = widgetInfoPackage.Title,
                        Title = Title,
                        Parameters = model
                    }
            };
            return widgetResult;
        }

        public WidgetUpdatePackage RunUpdate(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager,
            ExpandoObject model)
        {
            var updateResult = new WidgetUpdatePackage();
            var menuService = providerManager.Provide<IMenuService>();
            var oxideServices = providerManager.Provide<IOxideServices>();
            var menuWidgetViewModel = new CoolMenuWidgetViewModel {Key = widgetInfoPackage.UniqeHash};
            if (((dynamic) model).SelectedContents.ToString() != string.Empty)
                menuWidgetViewModel.Menu = menuService.GetMenu(Convert.ToInt32(((dynamic) model).SelectedContents));
            if (((dynamic) model).SelectedMedia.ToString() != string.Empty)
                menuWidgetViewModel.Logo = Convert.ToInt32(((dynamic) model).SelectedMedia.ToString());
            menuService.UpdateCoolMenuSettings(menuWidgetViewModel);
            updateResult.SuccessMessage = oxideServices.GetText("Cool Menu saved successfully");
            return updateResult;
        }

        public void OnDeleted(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var menuService = providerManager.Provide<IMenuService>();
            menuService.DeleteCoolMenuSettings(widgetInfoPackage.UniqeHash);
        }

        #endregion
    }
}