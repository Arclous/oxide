﻿using System.Dynamic;
using Oxide.Core.Elements;
using Oxide.Core.Managers.CacheManager;
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.Models.Base;
using Oxide.Core.ResultModels;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Syntax;
using Oxide.Fundamentals.Services.Interface;
using Oxide.Fundamentals.ViewModels.Wysiwyg;

namespace Oxide.Fundamentals.Widgets
{
    public class CkEditor : IOxideWidget
    {
        #region WidgetBasic

        public int Id => 1;

        public string Name => "CkEditor";

        public string Title => "CkEditor";

        public string Description
            =>
                "What You See Is What You Get! Editor for managing your pages, you can use it to add text, images,link etc."
            ;

        public string Category => "Content";

        public string PreviewImageUrl => "/areas/OxideFundamentals/content/images/widgets/Wysiwyg.png";

        public bool Active { get; set; }

        public string ModuleName => "Oxide.Fundamentals";

        public string FrontView { get; set; } = "CkEditor";

        public string EditorView { get; set; } = "CkEditor_Editor";

        public string WidgetKey { get; set; }

        public string Dependencies { get; set; }

        #endregion

        #region Renders

        WidgetResult IOxideWidget.RenderFront(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var wysiwygWidgetService = providerManager.Provide<IWysiwygWidgetService>();
            var cacheManager = providerManager.Provide<IOxideCacheManager>();
            var oxideModel = new OxideModel {RequestType = Syntax.Requests.Dynamic, Header = Name, Title = Title};
            var htmlContent = cacheManager.GetOrSet(widgetInfoPackage.UniqeHash.ToString(), widgetInfoPackage.Title,
                "Wysiwyg Widget", () => wysiwygWidgetService.LoadContent(widgetInfoPackage.UniqeHash));
            if (htmlContent != null)
                oxideModel.Parameters = htmlContent;
            var widgetResult = new WidgetResult {ViewName = FrontView, Model = oxideModel};
            return widgetResult;
        }

        public WidgetResult RenderAdmin(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var wysiwygWidgetService = providerManager.Provide<IWysiwygWidgetService>();
            var htmlContent = wysiwygWidgetService.LoadContent(widgetInfoPackage.UniqeHash);
            var wysiwygViewModel = new WysiwygViewModel {HtmlData = htmlContent};
            var widgetResult = new WidgetResult
            {
                ViewName = EditorView,
                Model =
                    new OxideModel
                    {
                        RequestType = Syntax.Requests.Dynamic,
                        Header = widgetInfoPackage.Title,
                        Title = Title,
                        Parameters = wysiwygViewModel
                    }
            };
            return widgetResult;
        }

        public WidgetUpdatePackage RunUpdate(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager,
            ExpandoObject model)
        {
            var updateResult = new WidgetUpdatePackage();
            var wysiwygWidgetService = providerManager.Provide<IWysiwygWidgetService>();
            var oxideServices = providerManager.Provide<IOxideServices>();
            updateResult.SuccessMessage = oxideServices.GetText("Content saved successfully");
            wysiwygWidgetService.SaveContent(((dynamic) model).HtmlData.ToString(), widgetInfoPackage.UniqeHash);
            return updateResult;
        }

        public void OnDeleted(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var wysiwygWidgetService = providerManager.Provide<IWysiwygWidgetService>();
            wysiwygWidgetService.DeleteContent(widgetInfoPackage.UniqeHash);
        }

        #endregion
    }
}