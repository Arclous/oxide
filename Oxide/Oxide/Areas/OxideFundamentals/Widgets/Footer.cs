﻿using System;
using System.Dynamic;
using Oxide.Core.Elements;
using Oxide.Core.Extesions;
using Oxide.Core.Managers.CacheManager;
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.Models.Base;
using Oxide.Core.ResultModels;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Services.OxideUiService;
using Oxide.Core.Syntax;
using Oxide.Fundamentals.Keywords;
using Oxide.Fundamentals.Services.Interface;
using Oxide.Fundamentals.ViewModels.Footer;

namespace Oxide.Fundamentals.Widgets
{
    public class Footer : IOxideWidget
    {
        #region WidgetBasic

        public int Id => 1;

        public string Name => "Footer";

        public string Title => "Footer";

        public string Description
            => "The widget which can be used to create and footer and its content for the website.";

        public string Category => "Navigation";

        public string PreviewImageUrl => "/areas/OxideFundamentals/content/images/widgets/CoolMenu.png";

        public bool Active { get; set; }

        public string ModuleName => "Oxide.Fundamentals";

        public string FrontView { get; set; } = "Footer";

        public string FrontViewDark { get; set; } = "FooterDark";

        public string EditorView { get; set; } = "Footer_Editor";

        public string WidgetKey { get; set; }

        public string Dependencies { get; set; }

        #endregion

        #region Renders

        WidgetResult IOxideWidget.RenderFront(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var activeView = FrontView;
            var menuWidgetService = providerManager.Provide<IMenuService>();
            var cacheManager = providerManager.Provide<IOxideCacheManager>();
            var oxideUiService = providerManager.Provide<IOxideUiService>();
            var footerService = providerManager.Provide<IFooterWidgetService>();
            var model = cacheManager.GetOrSet(widgetInfoPackage.GetUniqueHashWithPageId().ToString(),
                widgetInfoPackage.Title, "Footer Widget", () => footerService.LoadSettings(widgetInfoPackage.UniqeHash));
            var oxideModel = new OxideModel {RequestType = Syntax.Requests.Dynamic, Header = Name, Title = Title};
            if (model.Menu != null)
            {
                var coolMenuViewModelPackage = new FooterWidgetViewModel
                {
                    Name = model.Name,
                    Description = model.Description,
                    CopyRight = model.CopyRight,
                    FacebookLink = model.FacebookLink,
                    TwiterLink = model.TwiterLink,
                    LinkedInLink = model.LinkedInLink
                };
                coolMenuViewModelPackage.MenuItemListFront = menuWidgetService.MenuItemListFront(model.Menu.Id,
                    widgetInfoPackage.MainPageId);
                oxideModel.Parameters = coolMenuViewModelPackage;
            }
            switch (model.Type)
            {
                case Keys.FooterWidgetStyle.Light:
                {
                    activeView = FrontView;
                    break;
                }
                case Keys.FooterWidgetStyle.Dark:
                {
                    activeView = FrontViewDark;
                    break;
                }
            }
            var widgetResult = new WidgetResult {ViewName = activeView, Model = oxideModel};
            return widgetResult;
        }

        public WidgetResult RenderAdmin(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var footerService = providerManager.Provide<IFooterWidgetService>();
            var model = footerService.LoadSettings(widgetInfoPackage.UniqeHash);
            widgetInfoPackage.CopyPropertiesTo(model);
            model.WidgetStyle = (int) model.Type;
            var widgetResult = new WidgetResult
            {
                ViewName = EditorView,
                Model =
                    new OxideModel
                    {
                        RequestType = Syntax.Requests.Dynamic,
                        Header = widgetInfoPackage.Title,
                        Title = Title,
                        Parameters = model
                    }
            };
            return widgetResult;
        }

        public WidgetUpdatePackage RunUpdate(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager,
            ExpandoObject model)
        {
            var updateResult = new WidgetUpdatePackage();
            var footerService = providerManager.Provide<IFooterWidgetService>();
            var oxideServices = providerManager.Provide<IOxideServices>();
            var footerWidgetEditorViewModel = new FooterWidgetEditorViewModel {Key = widgetInfoPackage.UniqeHash};
            if (((dynamic) model).SelectedContents.ToString() != string.Empty)
            {
                footerWidgetEditorViewModel.MenuId = Convert.ToInt32(((dynamic) model).SelectedContents);
            }
            var widgetType = Convert.ToInt32(((dynamic) model).WidgetStyle.ToString());
            footerWidgetEditorViewModel.Type = (Keys.FooterWidgetStyle) widgetType;
            footerWidgetEditorViewModel.Name = ((dynamic) model).Title;
            footerWidgetEditorViewModel.Description = ((dynamic) model).Description;
            footerWidgetEditorViewModel.CopyRight = ((dynamic) model).CopyRight;
            footerWidgetEditorViewModel.FacebookLink = ((dynamic) model).FacebookLink;
            footerWidgetEditorViewModel.TwiterLink = ((dynamic) model).TwiterLink;
            footerWidgetEditorViewModel.LinkedInLink = ((dynamic) model).LinkedInLink;
            footerService.UpdateSettings(footerWidgetEditorViewModel);
            updateResult.SuccessMessage = oxideServices.GetText("Footer saved successfully");
            return updateResult;
        }

        public void OnDeleted(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var footerService = providerManager.Provide<IFooterWidgetService>();
            footerService.DeleteSettings(widgetInfoPackage.UniqeHash);
        }

        #endregion
    }
}