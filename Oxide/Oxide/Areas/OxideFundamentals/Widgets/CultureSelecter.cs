﻿using System.Dynamic;
using System.Web;
using Oxide.Core.Elements;
using Oxide.Core.Managers.CacheManager;
using Oxide.Core.Managers.CultureManager;
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.Models.Base;
using Oxide.Core.ResultModels;
using Oxide.Core.Services.LanguageService;
using Oxide.Core.Syntax;
using Oxide.Fundamentals.ViewModels.Culture;

namespace Oxide.Fundamentals.Widgets
{
    public class CultureSelecter : IOxideWidget
    {
        #region WidgetBasic

        public int Id => 2;

        public string Name => "Culture Selecter";

        public string Title => "Culture Selecter";

        public string Description
            =>
                "Select list for changing active culture according to activate cultures from the admin site, you can use this widget to give users chance to change interface language"
            ;

        public string Category => "Localization";

        public string PreviewImageUrl => "/areas/OxideFundamentals/content/images/widgets/CultureSelecter.png";

        public bool Active { get; set; }

        public string ModuleName => "Oxide.Fundamentals";

        public string FrontView { get; set; } = "CultureSelecter";

        public string EditorView { get; set; } = "CultureSelecter";

        public string WidgetKey { get; set; }

        public string Dependencies { get; set; }

        #endregion

        #region Renders

        WidgetResult IOxideWidget.RenderFront(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var languageService = providerManager.Provide<ILanguageService>();
            var cacheManager = providerManager.Provide<IOxideCacheManager>();
            var cultureManager = providerManager.Provide<ICultureManager>();
            var model = new CultureViewModel
            {
                Languages =
                    cacheManager.GetOrSet("Culture Selecter", "CultureSelecter", "Culter Selecter Widget",
                        () => languageService.GetAllActiveLanguages()),
                ActiveCulture = cultureManager.GetFrontCurrentCulture().ThreeLetterISOLanguageName,
                ActiveCultureName = cultureManager.GetFrontCurrentCulture().NativeName,
                ActiveUrl = HttpContext.Current.Request.RawUrl
            };
            var oxideModel = new OxideModel
            {
                RequestType = Syntax.Requests.Dynamic,
                Header = Name,
                Title = Title,
                Parameters = model
            };
            var widgetResult = new WidgetResult {ViewName = FrontView, Model = oxideModel};
            return widgetResult;
        }

        public WidgetResult RenderAdmin(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var widgetResult = new WidgetResult
            {
                ViewName = EditorView,
                Model =
                    new OxideModel
                    {
                        RequestType = Syntax.Requests.Dynamic,
                        Header = widgetInfoPackage.Title,
                        Title = Title
                    }
            };
            return widgetResult;
        }

        public WidgetUpdatePackage RunUpdate(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager,
            ExpandoObject model)
        {
            return new WidgetUpdatePackage();
        }

        public void OnDeleted(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
        }

        #endregion
    }
}