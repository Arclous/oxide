﻿using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using Oxide.Core.Elements;
using Oxide.Core.Managers.CacheManager;
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.Models.Base;
using Oxide.Core.ResultModels;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Services.OxideUiService;
using Oxide.Core.Syntax;
using Oxide.Fundamentals.Services.Interface;
using Oxide.Fundamentals.ViewModels.Slider;

namespace Oxide.Fundamentals.Widgets
{
    public class Slider : IOxideWidget
    {
        #region WidgetBasic

        public int Id => 1;

        public string Name => "Slider";

        public string Title => "Slider";

        public string Description
            =>
                "Widget which can display your images as grid view image gallery with the ability to display image bigger when clicks."
            ;

        public string Category => "Media";

        public string PreviewImageUrl => "/areas/OxideFundamentals/content/images/widgets/Slider.png";

        public bool Active { get; set; }

        public string ModuleName => "Oxide.Fundamentals";

        public string FrontView { get; set; } = "Slider";

        public string EditorView { get; set; } = "Slider_Editor";

        public string WidgetKey { get; set; }

        public string Dependencies { get; set; }

        #endregion

        #region Renders

        WidgetResult IOxideWidget.RenderFront(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var sliderService = providerManager.Provide<ISliderService>();
            var cacheManager = providerManager.Provide<IOxideCacheManager>();
            var oxideUiService = providerManager.Provide<IOxideUiService>();
            var oxideModel = new OxideModel {RequestType = Syntax.Requests.Dynamic, Header = Name, Title = Title};
            var selectedMedias = cacheManager.GetOrSet(widgetInfoPackage.UniqeHash.ToString(), widgetInfoPackage.Title,
                "Slider Widget", () => sliderService.GetMedia(widgetInfoPackage.UniqeHash));
            var mediaUrls = new List<string>();
            if (selectedMedias != null)
            {
                mediaUrls.AddRange(from media in selectedMedias
                                   select oxideUiService.GetContent(Constants.FileContent, media)
                                   into content
                                   select string.Format(@"{0}", ((dynamic) content).Path)
                                   into imageSrc
                                   select (string) imageSrc);
            }
            oxideModel.Parameters = mediaUrls;
            var widgetResult = new WidgetResult {ViewName = FrontView, Model = oxideModel};
            return widgetResult;
        }

        public WidgetResult RenderAdmin(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var sliderService = providerManager.Provide<ISliderService>();
            var selectedMedias = sliderService.GetMedia(widgetInfoPackage.UniqeHash);
            var carouselViewModel = new SliderViewModel
            {
                UniqeHash = widgetInfoPackage.UniqeHash,
                SelectedMedias = string.Join(",", selectedMedias),
                ActiveUrl = HttpContext.Current.Request.RawUrl
            };
            var widgetResult = new WidgetResult
            {
                ViewName = EditorView,
                Model =
                    new OxideModel
                    {
                        RequestType = Syntax.Requests.Dynamic,
                        Header = widgetInfoPackage.Title,
                        Title = Title,
                        Parameters = carouselViewModel
                    }
            };
            return widgetResult;
        }

        public WidgetUpdatePackage RunUpdate(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager,
            ExpandoObject model)
        {
            var updateResult = new WidgetUpdatePackage();
            var sliderService = providerManager.Provide<ISliderService>();
            var oxideServices = providerManager.Provide<IOxideServices>();
            var clearMedia = sliderService.ClearMedia(widgetInfoPackage.UniqeHash);
            var saveMedia = sliderService.SaveMedia(widgetInfoPackage.UniqeHash, ((dynamic) model).MediaSelecter);
            if (clearMedia && saveMedia)
                updateResult.SuccessMessage = oxideServices.GetText("Slider saved successfully");
            else
                updateResult.ErrorMessage =
                    oxideServices.GetText(
                        "Problem while saving slider, operation is not succesfull, please check the logs");
            return updateResult;
        }

        public void OnDeleted(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var sliderService = providerManager.Provide<ISliderService>();
            sliderService.ClearMedia(widgetInfoPackage.UniqeHash);
        }

        #endregion
    }
}