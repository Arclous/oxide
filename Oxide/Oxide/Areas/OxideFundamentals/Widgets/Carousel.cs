﻿using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using Oxide.Core.Elements;
using Oxide.Core.Extesions;
using Oxide.Core.Managers.CacheManager;
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.Models.Base;
using Oxide.Core.ResultModels;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Services.OxideUiService;
using Oxide.Core.Syntax;
using Oxide.Fundamentals.Models.Carousel;
using Oxide.Fundamentals.Services.Interface;
using Oxide.Fundamentals.ViewModels.Carousel;

namespace Oxide.Fundamentals.Widgets
{
    public class Carousel : IOxideWidget
    {
        #region WidgetBasic

        public int Id => 1;

        public string Name => "Carousel";

        public string Title => "Carousel";

        public string Description
            =>
                "Widget which can display your images as grid view image gallery with the ability to display image bigger when clicks."
            ;

        public string Category => "Media";

        public string PreviewImageUrl => "/areas/OxideFundamentals/content/images/widgets/Carousel.png";

        public bool Active { get; set; }

        public string ModuleName => "Oxide.Fundamentals";

        public string FrontView { get; set; } = "Carousel";

        public string EditorView { get; set; } = "Carousel_Editor";

        public string WidgetKey { get; set; }

        public string Dependencies { get; set; } = "CkEditor,Lightbox Gallery";

        #endregion

        #region Renders

        WidgetResult IOxideWidget.RenderFront(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var carouselService = providerManager.Provide<ICarouselService>();
            var cacheManager = providerManager.Provide<IOxideCacheManager>();
            var oxideUiService = providerManager.Provide<IOxideUiService>();
            var oxideModel = new OxideModel {RequestType = Syntax.Requests.Dynamic, Header = Name, Title = Title};
            var selectedMedias = cacheManager.GetOrSet(widgetInfoPackage.UniqeHash.ToString(), widgetInfoPackage.Title,
                "Carousel Widget", () => carouselService.GetMedia(widgetInfoPackage.UniqeHash));
            var carouselSettings = carouselService.GetSettings(widgetInfoPackage.UniqeHash) ?? new CarouselSetting();
            var mediaUrls = new List<string>();
            if (selectedMedias != null)
            {
                mediaUrls.AddRange(from media in selectedMedias
                                   select oxideUiService.GetContent(Constants.FileContent, media)
                                   into content
                                   select string.Format(@"{0}", ((dynamic) content).Path)
                                   into imageSrc
                                   select (string) imageSrc);
            }
            var carouselViewModel = new CarouselViewModel
            {
                UniqeHash = widgetInfoPackage.UniqeHash,
                Carousels = mediaUrls,
                ActiveUrl = HttpContext.Current.Request.RawUrl,
                Settings = carouselSettings,
                IsKeyboardControl = carouselSettings.KeyboardControl ? "" : "data-keyboard=false",
                IsPauseOnHover = carouselSettings.PauseOnHover ? "" : "data-pause=false",
                IsWrapSlides = carouselSettings.WrapSlides ? "" : "data-wrap=false",
                MaxWidthValue =
                    carouselSettings.MaxWidth == 0 ? "" : string.Format(@"width={0}", carouselSettings.MaxWidth),
                MaxHeightValue =
                    carouselSettings.MaxHeight == 0 ? "" : string.Format(@"height={0}", carouselSettings.MaxHeight)
            };
            oxideModel.Parameters = carouselViewModel;
            var widgetResult = new WidgetResult {ViewName = FrontView, Model = oxideModel};
            return widgetResult;
        }

        public WidgetResult RenderAdmin(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var carouselService = providerManager.Provide<ICarouselService>();
            var selectedMedias = carouselService.GetMedia(widgetInfoPackage.UniqeHash);
            var carouselSettings = carouselService.GetSettings(widgetInfoPackage.UniqeHash) ?? new CarouselSetting();
            var carouselViewModel = new CarouselViewModel
            {
                UniqeHash = widgetInfoPackage.UniqeHash,
                SelectedMedias = string.Join(",", selectedMedias),
                ActiveUrl = HttpContext.Current.Request.RawUrl,
                Settings = carouselSettings,
                IsKeyboardControl = carouselSettings.KeyboardControl ? "checked" : "",
                IsPauseOnHover = carouselSettings.PauseOnHover ? "checked" : "",
                IsShowIndicators = carouselSettings.ShowIndicators ? "checked" : "",
                IsWrapSlides = carouselSettings.WrapSlides ? "checked" : ""
            };
            var widgetResult = new WidgetResult
            {
                ViewName = EditorView,
                Model =
                    new OxideModel
                    {
                        RequestType = Syntax.Requests.Dynamic,
                        Header = widgetInfoPackage.Title,
                        Title = Title,
                        Parameters = carouselViewModel
                    }
            };
            return widgetResult;
        }

        public WidgetUpdatePackage RunUpdate(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager,
            ExpandoObject model)
        {
            var updateResult = new WidgetUpdatePackage();
            var carouselService = providerManager.Provide<ICarouselService>();
            var oxideServices = providerManager.Provide<IOxideServices>();
            var settingsModel = new CarouselSetting();
            Mapper<CarouselSetting>.Map(model, settingsModel);
            settingsModel.Key = widgetInfoPackage.UniqeHash;
            var clearMedia = carouselService.ClearMedia(widgetInfoPackage.UniqeHash);
            var saveMedia = carouselService.SaveMedia(widgetInfoPackage.UniqeHash, ((dynamic) model).MediaSelecter);
            var saveSettings = carouselService.SaveSettings(settingsModel);
            if (clearMedia && saveMedia && saveSettings)
                updateResult.SuccessMessage = oxideServices.GetText("Carousel saved successfully");
            else
                updateResult.ErrorMessage =
                    oxideServices.GetText(
                        "Problem while saving carousel, operation is not succesfull, please check the logs");
            return updateResult;
        }

        public void OnDeleted(WidgetInfoPackage widgetInfoPackage, IProviderManager providerManager)
        {
            var carouselService = providerManager.Provide<ICarouselService>();
            carouselService.ClearMedia(widgetInfoPackage.UniqeHash);
            carouselService.DeleteSettings(widgetInfoPackage.UniqeHash);
        }

        #endregion
    }
}