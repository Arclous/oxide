﻿using System;
using System.Collections.Generic;
using System.Linq;
using Oxide.Core.Base.OxideEvents;
using Oxide.Core.Managers.EventsManager;
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.Syntax;
using Oxide.Core.ViewModels.PageViewModels;
using Oxide.Fundamentals.Models.Audit;
using Oxide.Fundamentals.Services.Interface;

namespace Oxide.Fundamentals.Events
{
    public class Event : IOxideEventManager
    {
        private IAuditService _auditService;
        public void Initalize()
        {
            IProviderManager providerManager = new ProviderManager();
            _auditService = providerManager.Provide<IAuditService>();
            var registeredAudits = _auditService.GetAllActiveAuditSettings();
            foreach (var audit in registeredAudits)
                OxideEvents.RegisterEvent(_auditService.GetModelByName(audit.Name), audit.EventType);
        }
        public void OnModelCreated(object[] entities)
        {
            var listAudits = new List<Audit>();
            foreach (var audit in
                entities.Select(
                    entity =>
                        new Audit
                        {
                            ActionDateTime = DateTime.Now,
                            Name = "Create",
                            EventType = Syntax.EventTypes.OnCreated,
                            ContentType = entity.GetType().BaseType.Name,
                            Title = ((dynamic) entity).Name,
                            ContentTitle = ((dynamic) entity).Title,
                            ContentId = ((dynamic) entity).Id
                        }))
            {
                listAudits.Add(audit);
            }
            _auditService.CreateAudit(listAudits);
        }
        public void OnModelUpdated(object[] entities)
        {
            var listAudits = new List<Audit>();
            foreach (
                var audit in
                    entities.Select(
                        entity =>
                            new Audit
                            {
                                ActionDateTime = DateTime.Now,
                                Name = "Update",
                                EventType = Syntax.EventTypes.OnUpdated,
                                ContentType = entity.GetType().BaseType.Name,
                                Title = ((dynamic) entity).Name,
                                ContentTitle = ((dynamic) entity).Title,
                                ContentId = ((dynamic) entity).Id
                            }))
            {
                listAudits.Add(audit);
            }
            _auditService.CreateAudit(listAudits);
        }
        public void OnModelDeleted(object[] entities)
        {
            var listAudits = new List<Audit>();
            foreach (var audit in
                entities.Select(
                    entity =>
                        new Audit
                        {
                            ActionDateTime = DateTime.Now,
                            Name = "Delete",
                            EventType = Syntax.EventTypes.OnDeleted,
                            ContentType = entity.GetType().BaseType.Name,
                            Title = ((dynamic) entity).Name,
                            ContentTitle = ((dynamic) entity).Title,
                            ContentId = ((dynamic) entity).Id
                        }))
            {
                listAudits.Add(audit);
            }
            _auditService.CreateAudit(listAudits);
        }
        public void OnPageRender(RenderPage page)
        {
        }
    }
}