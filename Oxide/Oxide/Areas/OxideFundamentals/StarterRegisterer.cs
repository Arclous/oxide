﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Oxide.Core.Helpers;
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.Services.Interface.Registers.OxideStarter;
using Oxide.Core.Services.OxideDataService;
using Oxide.Core.Services.PageService;
using Oxide.Core.Services.SiteSettings;
using Oxide.Core.Services.WidgetService;
using Oxide.Core.Syntax;
using Oxide.Fundamentals.Keywords;
using Oxide.Fundamentals.Models.Menu;
using Oxide.Fundamentals.Services.Interface;
using Oxide.Fundamentals.ViewModels.Footer;
using Oxide.Fundamentals.ViewModels.Menu;
using Oxide.Media.Services.Directory;
using Oxide.Media.Services.File;
using Directory = Oxide.Media.Models.Directory.Directory;
using File = Oxide.Media.Models.File.File;

namespace Oxide.Fundamentals
{
    public class StarterRegisterer : IOxideStarterRegisterer
    {
        private IOxideDataService _oxideDataService;
        private IProviderManager _providerManager;

        public void Start(IProviderManager providerManager)
        {
            _providerManager = providerManager;

            //Create audit settings and register
            CreateAudits();

            //Create Menus and add navigation widget
            CreateMenus();
        }

        private void CreateAudits()
        {
            _oxideDataService = _providerManager.Provide<IOxideDataService>();
            var auditService = _providerManager.Provide<IAuditService>();
            if (!_oxideDataService.IsUpdateRequired("DefaultAudit")) return;
            auditService.CreateNewAuditSetting("Page", (int) Syntax.EventTypes.OnCreated, true);
            auditService.CreateNewAuditSetting("Page", (int) Syntax.EventTypes.OnUpdated, true);
            auditService.CreateNewAuditSetting("Page", (int) Syntax.EventTypes.OnDeleted, true);
            _oxideDataService.WriteUpdateItem("DefaultAudit");
        }

        private void CreateMenus()
        {
            if (!_oxideDataService.IsInitilizationMode()) return;
            var menuService = _providerManager.Provide<IMenuService>();
            var siteSettingsService = _providerManager.Provide<ISiteSettingsService>();
            var pageService = _providerManager.Provide<IPageService>();
            var siteSettings = siteSettingsService.GetSettings();
            var widgetService = _providerManager.Provide<IWidgetService>();
            var wysiwygWidgetService = _providerManager.Provide<IWysiwygWidgetService>();
            var footerService = _providerManager.Provide<IFooterWidgetService>();
            var directoryService = _providerManager.Provide<IDirectoryService>();
            var fileService = _providerManager.Provide<IFileService>();
            var defaultLanguageId = siteSettings.DefaultSiteCulture.Id;

            //Create Menus
            var mainMenu = new Menu {Title = "Main Menu English", Name = "Main Menu", Menus = new List<MenuItem>()};
            menuService.CreateMenu(mainMenu);
            var homeMenu = new MenuItem
            {
                Name = "Home",
                Title = "Home",
                LanguageId = defaultLanguageId,
                LinkType = LinkTypes.Internal,
                MenuId = mainMenu.Id,
                PageId = pageService.GetPage("/").Id,
                IsChild = false,
                ParentId = mainMenu.Id
            };
            menuService.CreateMenuItem(homeMenu);
            var aboutMenu = new MenuItem
            {
                Name = "About",
                Title = "About",
                LanguageId = defaultLanguageId,
                LinkType = LinkTypes.Internal,
                MenuId = mainMenu.Id,
                PageId = pageService.GetPage("/about").Id,
                ParentId = mainMenu.Id,
                IsChild = false
            };
            menuService.CreateMenuItem(aboutMenu);
            var communityMenu = new MenuItem
            {
                Name = "Community",
                Title = "Community",
                LanguageId = defaultLanguageId,
                LinkType = LinkTypes.Internal,
                MenuId = mainMenu.Id,
                PageId = pageService.GetPage("/community").Id,
                ParentId = mainMenu.Id,
                IsChild = false
            };
            menuService.CreateMenuItem(communityMenu);
            var contactMenu = new MenuItem
            {
                Name = "Contact",
                Title = "Contact",
                LanguageId = defaultLanguageId,
                LinkType = LinkTypes.Internal,
                MenuId = mainMenu.Id,
                PageId = pageService.GetPage("/contact").Id,
                ParentId = mainMenu.Id,
                IsChild = false
            };
            menuService.CreateMenuItem(contactMenu);

            //Add Navigation Widget
            var mainLayout =
                Core.Oxide.LoadedLayouts.FirstOrDefault(x => x.LayoutType == Syntax.LayoutTypes.Main && x.Id == 1);
            var navigationWidget = OxideWidgetHelper.GetWidget(x => x.Name == "Cool Menu");

            //Activate widget
            widgetService.ChangeStatus(navigationWidget.WidgetKey, false);
            if (mainLayout == null) return;
            var mainLayutKey = mainLayout.GetLayoutsKeys().FirstOrDefault(x => x.Value == "Header").Key;
            var widgetInfoPckage = widgetService.InsertMainLayoutWidget(mainLayout.Id, mainLayutKey,
                navigationWidget.WidgetKey, siteSettings.DefaultSiteCultureId, 0, "Navigation Widget");

            //Set Logo for Cool Menu
            var mediaDirectory = directoryService.GetMediaDirectory();
            var logoDirectory =
                (Directory) directoryService.CreateDirectory(mediaDirectory.Id, Keys.DefaultLogoName).Data;
            var logoFile = new FileInfo(HttpContext.Current.Server.MapPath(Keys.DefaultLogoPath));
            var logoFileId = ((File) fileService.UploadLocalFile(logoDirectory.Id, logoFile.FullName).Data).Id;

            //Set Navigation Widget Settings
            var menuWidgetViewModel = new CoolMenuWidgetViewModel
            {
                Key = widgetInfoPckage.UniqeHash,
                Menu = mainMenu,
                Logo = logoFileId
            };
            menuService.UpdateCoolMenuSettings(menuWidgetViewModel);

            //Add Wyswig widget
            var innerLayout =
                Core.Oxide.LoadedLayouts.FirstOrDefault(x => x.LayoutType == Syntax.LayoutTypes.Inner && x.Id == 1);
            if (innerLayout == null) return;
            var innerLayoutKey = innerLayout.GetLayoutsKeys().FirstOrDefault(x => x.Value == "Content").Key;
            var wsywigWidget = OxideWidgetHelper.GetWidget(x => x.Name == "CkEditor");

            //Activate widget
            widgetService.ChangeStatus(wsywigWidget.WidgetKey, false);

            //Get pages
            var homePage = pageService.GetPage(homeMenu.PageId);
            var aboutPage = pageService.GetPage(aboutMenu.PageId);
            var communityPage = pageService.GetPage(communityMenu.PageId);
            var contactPage = pageService.GetPage(contactMenu.PageId);

            //Add widgets to pages
            //Home Page
            widgetInfoPckage = widgetService.InsertPageWidget(homePage.Id, innerLayout.Id, innerLayoutKey,
                wsywigWidget.WidgetKey, 0, "Content");
            wysiwygWidgetService.SaveContent(Keys.DefaultHomeContent, widgetInfoPckage.UniqeHash);

            //About Page
            widgetInfoPckage = widgetService.InsertPageWidget(aboutPage.Id, innerLayout.Id, innerLayoutKey,
                wsywigWidget.WidgetKey, 0, "Content");
            wysiwygWidgetService.SaveContent(Keys.DefaultAboutContent, widgetInfoPckage.UniqeHash);

            //Community Page
            widgetInfoPckage = widgetService.InsertPageWidget(communityPage.Id, innerLayout.Id, innerLayoutKey,
                wsywigWidget.WidgetKey, 0, "Content");
            wysiwygWidgetService.SaveContent(Keys.DefaultCommunityContent, widgetInfoPckage.UniqeHash);

            //Contact Page
            widgetInfoPckage = widgetService.InsertPageWidget(contactPage.Id, innerLayout.Id, innerLayoutKey,
                wsywigWidget.WidgetKey, 0, "Content");
            wysiwygWidgetService.SaveContent(Keys.DefaultContactContent, widgetInfoPckage.UniqeHash);
            var footerWidget = OxideWidgetHelper.GetWidget(x => x.Name == "Footer");

            //Activate widget
            widgetService.ChangeStatus(footerWidget.WidgetKey, false);
            var mainLayutKeyForFooter = mainLayout.GetLayoutsKeys().FirstOrDefault(x => x.Value == "Footer").Key;
            widgetInfoPckage = widgetService.InsertMainLayoutWidget(mainLayout.Id, mainLayutKeyForFooter,
                footerWidget.WidgetKey, siteSettings.DefaultSiteCultureId, 0, "Footer");
            var footerWidgetEditorViewModel = new FooterWidgetEditorViewModel
            {
                Key = widgetInfoPckage.UniqeHash,
                Type = Keys.FooterWidgetStyle.Dark,
                Name = Keys.DefaultFooterTitle,
                Description = Keys.DefaultFooterDescription,
                CopyRight = Keys.DefaultFooterCopyRight,
                FacebookLink = Keys.DefaultFooterFaceBookLink,
                TwiterLink = Keys.DefaultFooterTwiterLink,
                LinkedInLink = Keys.DefaultFooterLinkedInLink,
                MenuId = mainMenu.Id
            };
            footerService.UpdateSettings(footerWidgetEditorViewModel);
        }
    }
}