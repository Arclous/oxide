﻿using System.Web.Mvc;
using System.Web.Routing;
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.Models.Menus;
using Oxide.Core.Services.AdminMenuService;
using Oxide.Core.Services.Interface.Registers.AdminMenuRegister;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Syntax;

namespace Oxide.Fundamentals.Admin
{
    public class AdminMenu : IAdminMenuRegisterer
    {
        private const string DisplayMenuPermissionKey = "Can_Display_Menu_List";
        private const string CreateMenuPermissionKey = "Can_Create_Menu";
        private const string DisplayAuditSettingsPermissionKey = "Can_Manage_Audit_Settings";
        private const string DisplayAuditsPermissionKey = "Can_Display_Audits";
        private const string AreaName = "Oxide.Fundamentals";

        private readonly IProviderManager _providerManager = new ProviderManager();

        public void BuildMenus(RequestContext requestContext, IAdminMenuService adminMenuService)
        {
            var urlHelper = new UrlHelper(requestContext);
            var menuItem = new MenuItem();
            var oxideService = _providerManager.Provide<IOxideServices>();

            //*******************************************************************************************************************************************
            //Navigation Header
            var navigationMenu = adminMenuService.AddHeaderMenu("Navigation",
                oxideService.GetText("Navigation", AreaName), Syntax.CssClass.Menu, 8, PermissionKeys.Empty);

            //Menus
            menuItem.Key = "Menus";
            menuItem.Title = oxideService.GetText("Menus", AreaName);
            menuItem.ControllerName = "Menu";
            menuItem.ActionName = "DisplayMenus";
            menuItem.Order = 0;
            menuItem.PermissionKey = DisplayMenuPermissionKey;
            menuItem.UrlAddress = urlHelper.Action(menuItem.ActionName, menuItem.ControllerName, new {area = AreaName});
            adminMenuService.AddMenuItem(navigationMenu, menuItem);

            //*******************************************************************************************************************************************
            //Navigation Header
            //Create New Menu
            menuItem = new MenuItem
            {
                Key = "Create New Menu",
                Title = oxideService.GetText("Create New Menu", AreaName),
                ControllerName = "Menu",
                ActionName = "NewMenu",
                Order = 0,
                PermissionKey = CreateMenuPermissionKey
            };
            menuItem.UrlAddress = urlHelper.Action(menuItem.ActionName, menuItem.ControllerName, new {area = AreaName});
            adminMenuService.AddMenuItem(navigationMenu, menuItem);

            //*******************************************************************************************************************************************
            //Audit Header
            var auditHeader = adminMenuService.AddHeaderMenu("Audit", oxideService.GetText("Audit", AreaName),
                Syntax.CssClass.FaUsers, 8, PermissionKeys.Empty);

            //Menus
            menuItem = new MenuItem
            {
                Key = "Audit Settings",
                Title = oxideService.GetText("Audit Settings", AreaName),
                ControllerName = "Audit",
                ActionName = "AuditSettings",
                Order = 0,
                PermissionKey = DisplayAuditSettingsPermissionKey
            };
            menuItem.UrlAddress = urlHelper.Action(menuItem.ActionName, menuItem.ControllerName, new {area = AreaName});
            adminMenuService.AddMenuItem(auditHeader, menuItem);

            //*******************************************************************************************************************************************
            //Navigation Header
            //Create New Menu
            menuItem = new MenuItem
            {
                Key = "Display Audits",
                Title = oxideService.GetText("Display Audits", AreaName),
                ControllerName = "Audit",
                ActionName = "DisplayAudits",
                Order = 1,
                PermissionKey = DisplayAuditsPermissionKey
            };
            menuItem.UrlAddress = urlHelper.Action(menuItem.ActionName, menuItem.ControllerName, new {area = AreaName});
            adminMenuService.AddMenuItem(auditHeader, menuItem);

            //*******************************************************************************************************************************************
        }
    }
}