﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oxide.Core.ViewModels.FilterViewModels;

namespace OxideFundamentals.ViewModels.FilterModels
{
    public class MenuFilterViewModel: FilterModel<MenuViewModel>
    {
        public string DeleteUrl { get; set; }
    }
}