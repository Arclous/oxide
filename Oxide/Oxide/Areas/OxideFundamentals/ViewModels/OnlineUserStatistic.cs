﻿namespace Oxide.Fundamentals.ViewModels
{
    public class OnlineUserStatistic
    {
        public int TotalOnlineUsers { get; set; }
        public int TotalOfflineUsers { get; set; }
        public double AverageTime { get; set; }
    }
}