﻿using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Fundamentals.ViewModels.LightboxGallery
{
    public class LightboxGalleryViewModel : OxideWidgetViewModel
    {
        public int Key { get; set; }
        public string SelectedMedias { get; set; }
        public string ActiveUrl { get; set; }
    }
}