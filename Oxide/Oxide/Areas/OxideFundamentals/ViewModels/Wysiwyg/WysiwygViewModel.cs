﻿using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Fundamentals.ViewModels.Wysiwyg
{
    public class WysiwygViewModel : OxideWidgetViewModel
    {
        public string HtmlData { get; set; }
    }
}