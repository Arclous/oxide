﻿using System.Collections.Generic;
using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Fundamentals.ViewModels.Menu
{
    public class CoolMenuViewModel : OxideViewModel
    {
        public string Url { get; set; }
        public bool Active { get; set; }
        public string Target { get; set; }
        public bool External { get; set; }
        public List<CoolMenuViewModel> SubMenus { get; set; }
    }
}