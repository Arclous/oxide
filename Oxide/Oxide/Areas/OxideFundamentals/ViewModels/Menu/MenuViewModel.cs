﻿using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Fundamentals.ViewModels.Menu
{
    public class MenuViewModel : OxideViewModel
    {
        public string EditUrl { get; set; }
        public string MenuItemsUrl { get; set; }
    }
}