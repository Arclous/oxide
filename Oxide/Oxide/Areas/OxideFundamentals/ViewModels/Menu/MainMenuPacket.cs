﻿namespace Oxide.Fundamentals.ViewModels.Menu
{
    public class MainMenuPacket
    {
        public int MenuId { get; set; }
        public string AddUrl { get; set; }
        public string EditUrl { get; set; }
        public string RemoveUrl { get; set; }
    }
}