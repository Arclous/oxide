﻿using System.Collections.Generic;
using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Fundamentals.ViewModels.Menu
{
    public class CoolMenuViewModelPackage : OxideViewModel
    {
        public string LogoUrl { get; set; }
        public List<CoolMenuViewModel> Menus { get; set; }
    }
}