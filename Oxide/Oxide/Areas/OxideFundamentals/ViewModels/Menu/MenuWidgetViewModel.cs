﻿using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Fundamentals.ViewModels.Menu
{
    public class MenuWidgetViewModel : OxideWidgetViewModel
    {
        public int Key { get; set; }
        public virtual Models.Menu.Menu Menu { get; set; }
        public string SelectedContents { get; set; }
    }
}