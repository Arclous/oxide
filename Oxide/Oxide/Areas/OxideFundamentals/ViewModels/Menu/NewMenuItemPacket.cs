﻿using System.Collections;

namespace Oxide.Fundamentals.ViewModels.Menu
{
    public class NewMenuItemPacket
    {
        public int ParentId { get; set; }
        public int MenuId { get; set; }
        public int Top { get; set; }
        public IEnumerable LanguageSelectList { get; set; }
    }
}