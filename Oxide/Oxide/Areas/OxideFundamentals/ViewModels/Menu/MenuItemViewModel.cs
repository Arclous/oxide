﻿using System.Collections;
using System.Collections.Generic;
using Oxide.Areas.Admin.Models;
using Oxide.Core.Syntax;

namespace Oxide.Fundamentals.ViewModels.Menu
{
    public class MenuItemViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public int MenuId { get; set; }
        public int PageId { get; set; }
        public int ParentId { get; set; }
        public LinkTypes LinkType { get; set; }
        public int LinkTypeSelected { get; set; }
        public LinkOpenTypes OpenType { get; set; }
        public int OpenTypeSelected { get; set; }
        public string Url { get; set; }
        public int LanguageSelected { get; set; }
        public virtual Language Language { get; set; }
        public List<MenuItemViewModel> Childeren { get; set; }
        public IEnumerable LanguageSelectList { get; set; }
        public string SelectedContents { get; set; }
        public int Top { get; set; }
    }
}