﻿using System.Collections.Generic;
using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Fundamentals.ViewModels.Menu
{
    public class TreeViewItemModel : OxideViewModel
    {
        public List<TreeViewItemModel> Childeren { get; set; }
    }
}