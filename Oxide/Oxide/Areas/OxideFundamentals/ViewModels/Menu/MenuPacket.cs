﻿using System.Collections.Generic;

namespace Oxide.Fundamentals.ViewModels.Menu
{
    public class MenuPacket
    {
        public List<TreeViewItemModel> Menus = new List<TreeViewItemModel>();
        public string AddUrl { get; set; }
    }
}