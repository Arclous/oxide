﻿using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Fundamentals.ViewModels.Menu
{
    public class CoolMenuWidgetViewModel : OxideWidgetViewModel
    {
        public int Key { get; set; }
        public virtual Models.Menu.Menu Menu { get; set; }
        public string SelectedContents { get; set; }
        public string SelectedMedias { get; set; }
        public int Logo { get; set; }
        public string LogoUrl { get; set; }
    }
}