﻿using System.Collections.Generic;

namespace Oxide.Fundamentals.ViewModels
{
    public class TopPagesViewModel
    {
        public string PageName { get; set; }
        public int TotalVisit { get; set; }
        public string Url { get; set; }
        public int Percent { get; set; }
    }
    public class TopPagesPackageViewModel
    {
        public int TotalVisit { get; set; }
        public List<TopPagesViewModel> Pages { get; set; }
    }
}