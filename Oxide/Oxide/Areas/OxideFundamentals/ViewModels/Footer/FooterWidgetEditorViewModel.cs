﻿using Oxide.Core.Base.OxideViewModel;
using Oxide.Fundamentals.Keywords;

namespace Oxide.Fundamentals.ViewModels.Footer
{
    public class FooterWidgetEditorViewModel : OxideWidgetViewModel
    {
        public int Key { get; set; }
        public virtual Keys.FooterWidgetStyle Type { get; set; }
        public string Description { get; set; }
        public string TwiterLink { get; set; }
        public string FacebookLink { get; set; }
        public string LinkedInLink { get; set; }
        public string CopyRight { get; set; }
        public virtual Models.Menu.Menu Menu { get; set; }
        public string SelectedContents { get; set; }
        public int WidgetStyle { get; set; }
        public int MenuId { get; set; }
    }
}