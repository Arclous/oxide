﻿using System.Collections.Generic;
using Oxide.Core.Base.OxideViewModel;
using Oxide.Fundamentals.Keywords;
using Oxide.Fundamentals.ViewModels.Menu;

namespace Oxide.Fundamentals.ViewModels.Footer
{
    public class FooterWidgetViewModel : OxideWidgetViewModel
    {
        public List<NavigationMenuViewModel> MenuItemListFront { get; set; }
        public int Key { get; set; }
        public virtual Keys.FooterWidgetStyle Type { get; set; }
        public string Description { get; set; }
        public string TwiterLink { get; set; }
        public string FacebookLink { get; set; }
        public string LinkedInLink { get; set; }
        public string CopyRight { get; set; }
    }
}