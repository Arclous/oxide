﻿using System.Collections.Generic;
using Oxide.Areas.Admin.Models;

namespace Oxide.Fundamentals.ViewModels.Culture
{
    public class CultureViewModel
    {
        public List<Language> Languages { get; set; }
        public string ActiveCulture { get; set; }
        public string ActiveCultureName { get; set; }
        public string ActiveUrl { get; set; }
    }
}