﻿using System.Collections.Generic;
using Oxide.Core.Base.OxideViewModel;
using Oxide.Fundamentals.Models.Carousel;

namespace Oxide.Fundamentals.ViewModels.Carousel
{
    public class CarouselViewModel : OxideWidgetViewModel
    {
        public int Key { get; set; }
        public string SelectedMedias { get; set; }
        public List<string> Carousels { get; set; }
        public string ActiveUrl { get; set; }
        public CarouselSetting Settings { get; set; }
        public string IsPauseOnHover { get; set; }
        public string IsWrapSlides { get; set; }
        public string IsKeyboardControl { get; set; }
        public string IsShowIndicators { get; set; }
        public string MaxWidthValue { get; set; }
        public string MaxHeightValue { get; set; }
    }
}