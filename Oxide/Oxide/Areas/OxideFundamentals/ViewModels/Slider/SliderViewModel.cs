﻿using System.Collections.Generic;
using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Fundamentals.ViewModels.Slider
{
    public class SliderViewModel : OxideWidgetViewModel
    {
        public int Key { get; set; }
        public string SelectedMedias { get; set; }
        public List<string> Slides { get; set; }
        public string ActiveUrl { get; set; }
    }
}