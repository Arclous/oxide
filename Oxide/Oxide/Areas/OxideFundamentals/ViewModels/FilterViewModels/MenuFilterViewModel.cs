﻿using Oxide.Core.ViewModels.FilterViewModels;
using Oxide.Fundamentals.ViewModels.Menu;

namespace Oxide.Fundamentals.ViewModels.FilterViewModels
{
    public class MenuFilterViewModel : FilterModel<MenuViewModel>
    {
        public string DeleteUrl { get; set; }
    }
}