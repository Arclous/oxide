﻿using System.Collections;
using Oxide.Core.Base.OxideViewModel;
using Oxide.Core.Syntax;

namespace Oxide.Fundamentals.ViewModels.Audit
{
    public class AuditSettingEditorViewModel : OxideViewModel
    {
        public IEnumerable Models { get; set; }
        public Syntax.EventTypes EventType { get; set; }
        public bool Active { get; set; }
        public int EventSelected { get; set; }
        public string ContentSelected { get; set; }
        public string IsActive { get; set; }
    }
}