﻿using System;
using Oxide.Core.Base.OxideViewModel;
using Oxide.Core.Syntax;

namespace Oxide.Fundamentals.ViewModels.Audit
{
    public class AuditViewModel : OxideViewModel
    {
        public Syntax.EventTypes EventType { get; set; }
        public DateTime ActionDateTime { get; set; }
        public string ContentType { get; set; }
        public string AdminUser { get; set; }
        public int ContentId { get; set; }
        public string ContentTitle { get; set; }
    }
}