﻿using System.Web.Mvc;
using System.Web.Routing;
using Oxide.Core.Services.Interface.Registers.RouteRegister;

namespace Oxide.Fundamentals.Routes
{
    public class RouteRegister : IRouteRegisterer
    {
        public void RegisterRoute(RouteCollection routes)
        {
            routes.MapRoute("testtest", "testtest", new {controller = "Front", action = "TestTest"},
                new[] {"Oxide.Fundamentals.Controllers"});
        }
    }
}