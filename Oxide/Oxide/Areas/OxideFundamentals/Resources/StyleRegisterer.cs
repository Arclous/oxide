﻿using System.Web.Optimization;
using Oxide.Core.Services.Interface.Registers.ResourceRegister;

namespace Oxide.Fundamentals.Resources
{
    public class StyleRegister : IResourceRegisterer
    {
        public void RegisterResource(BundleCollection boundleCollection)
        {
            boundleCollection.Add(
                new StyleBundle("~/OxideFundamentals/oxidefundamentals").Include(
                    "~/Areas/OxideFundamentals/content/oxidefundamentals.css"));
            boundleCollection.Add(
                new StyleBundle("~/OxideFundamentals/lightbox").Include("~/Areas/OxideFundamentals/content/lightbox.css"));
            boundleCollection.Add(
                new StyleBundle("~/OxideFundamentals/swiper").Include("~/Areas/OxideFundamentals/content/swiper.css"));
        }
    }
}