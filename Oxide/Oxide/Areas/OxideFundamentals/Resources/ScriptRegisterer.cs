﻿using System.Web.Optimization;
using Oxide.Core.Services.Interface.Registers.ResourceRegister;

namespace Oxide.Fundamentals.Resources
{
    public class ScriptRegisterer : IResourceRegisterer
    {
        public void RegisterResource(BundleCollection boundleCollection)
        {
            boundleCollection.Add(
                new ScriptBundle("~/bundles/lightbox").Include("~/Areas/OxideFundamentals/Scripts/lightbox.min.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/swiper").Include("~/Areas/OxideFundamentals/Scripts/swiper.jquery.umd.js"));
            boundleCollection.Add(
                new ScriptBundle("~/OxideFundamentals/bootstrap").Include(
                    "~/Areas/OxideFundamentals/Scripts/bootstrap.js"));
            boundleCollection.Add(
                new ScriptBundle("~/OxideFundamentals/jquery").Include(
                    "~/Areas/OxideFundamentals/Scripts/jquery-1.10.2.js"));
        }
    }
}