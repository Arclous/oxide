﻿using System.Web.Optimization;
using Oxide.Core.Services.Interface.Registers.ResourceRegister;

namespace Oxide.Fundamentals.Resources
{
    public class ModelRegister : IResourceRegisterer
    {
        public void RegisterResource(BundleCollection boundleCollection)
        {
            boundleCollection.Add(
                new ScriptBundle("~/models/MenusModel").Include("~/Areas/OxideFundamentals/Scripts/Models/MenusModel.js"));
            boundleCollection.Add(
                new ScriptBundle("~/models/MenuTree").Include("~/Areas/OxideFundamentals/Scripts/Models/MenuTree.js"));
            boundleCollection.Add(
                new ScriptBundle("~/models/WysiwygModel").Include(
                    "~/Areas/OxideFundamentals/Scripts/Models/WysiwygModel.js"));
            boundleCollection.Add(
                new ScriptBundle("~/models/AuditItemsModel").Include(
                    "~/Areas/OxideFundamentals/Scripts/Models/AuditItemsModel.js"));
        }
    }
}