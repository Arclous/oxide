﻿using Oxide.Areas.Admin.Models;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Fundamentals.Models.Wysiwyg
{
    public class Wysiwyg : OxideDatabaseModel, IOxideDataBaseModel
    {
        public string Html { get; set; }
        public int Key { get; set; }
        public virtual Language Language { get; set; }
    }
}