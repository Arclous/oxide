﻿using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Fundamentals.Models.Carousel
{
    [OxideDatabase("OxideFundamentalsContext")]
    public class CarouselSetting : OxideDatabaseModel, IOxideDataBaseModel
    {
        public int Key { get; set; }
        public int Interval { get; set; }
        public int StartSlideIndex { get; set; }
        public bool PauseOnHover { get; set; }
        public bool WrapSlides { get; set; }
        public bool KeyboardControl { get; set; }
        public bool ShowIndicators { get; set; }
        public int MaxWidth { get; set; }
        public int MaxHeight { get; set; }
    }
}