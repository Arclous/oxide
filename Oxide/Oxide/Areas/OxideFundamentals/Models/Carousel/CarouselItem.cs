﻿using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Fundamentals.Models.Carousel
{
    [OxideDatabase("OxideFundamentalsContext")]
    public class CarouselItem : OxideDatabaseModel, IOxideDataBaseModel
    {
        public int Key { get; set; }
        public int ImageId { get; set; }
    }
}