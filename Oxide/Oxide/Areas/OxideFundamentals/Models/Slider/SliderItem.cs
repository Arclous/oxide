﻿using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Fundamentals.Models.Slider
{
    [OxideDatabase("OxideFundamentalsContext")]
    public class SliderItem : OxideDatabaseModel, IOxideDataBaseModel
    {
        public int Key { get; set; }
        public int ImageId { get; set; }
    }
}