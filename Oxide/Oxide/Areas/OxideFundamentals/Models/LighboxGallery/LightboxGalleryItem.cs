﻿using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Fundamentals.Models.LighboxGallery
{
    [OxideDatabase("OxideFundamentalsContext")]
    public class LightboxGalleryItem : OxideDatabaseModel, IOxideDataBaseModel
    {
        public int Key { get; set; }
        public int ImageId { get; set; }
    }
}