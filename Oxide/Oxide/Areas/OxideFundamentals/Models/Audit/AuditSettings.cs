﻿using Oxide.Core.Base.OxideDataBaseModel;
using Oxide.Core.Syntax;

namespace Oxide.Fundamentals.Models.Audit
{
    public class AuditSettings : OxideDatabaseModel, IOxideDataBaseModel
    {
        public Syntax.EventTypes EventType { get; set; }
        public bool Active { get; set; }
    }
}