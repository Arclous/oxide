﻿using System;
using Oxide.Core.Base.OxideDataBaseModel;
using Oxide.Core.Models.AdminUser;
using Oxide.Core.Syntax;

namespace Oxide.Fundamentals.Models.Audit
{
    public class Audit : OxideDatabaseModel, IOxideDataBaseModel
    {
        public Syntax.EventTypes EventType { get; set; }
        public DateTime ActionDateTime { get; set; }
        public string ContentType { get; set; }
        public int ContentId { get; set; }
        public string ContentTitle { get; set; }
        public virtual AdminUser User { get; set; }
    }
}