﻿using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;
using Oxide.Fundamentals.Keywords;

namespace Oxide.Fundamentals.Models.Footer
{
    [OxideDatabase("OxideExtraContext")]
    public class FooterWidget : OxideDatabaseModel, IOxideDataBaseModel
    {
        public int Key { get; set; }
        public virtual Keys.FooterWidgetStyle Type { get; set; }
        public string Description { get; set; }
        public string TwiterLink { get; set; }
        public string FacebookLink { get; set; }
        public string LinkedInLink { get; set; }
        public string CopyRight { get; set; }
        public virtual Menu.Menu Menu { get; set; }
    }
}