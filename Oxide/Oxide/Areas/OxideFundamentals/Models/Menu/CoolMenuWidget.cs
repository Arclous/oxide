﻿using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Fundamentals.Models.Menu
{
    public class CoolMenuWidget : OxideDatabaseModel, IOxideDataBaseModel
    {
        public int Key { get; set; }
        public virtual Menu Menu { get; set; }
        public int Logo { get; set; }
    }
}