﻿using System.Collections.Generic;
using Oxide.Areas.Admin.Models;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;
using Oxide.Core.Syntax;

namespace Oxide.Fundamentals.Models.Menu
{
    [OxideDatabase("OxideFundamentalsContext")]
    public class MenuItem : OxideDatabaseModel, IOxideDataBaseModel
    {
        public int MenuId { get; set; }
        public int PageId { get; set; }
        public int ParentId { get; set; }
        public LinkTypes LinkType { get; set; }
        public LinkOpenTypes OpenType { get; set; }
        public string Url { get; set; }
        public virtual Language Language { get; set; }
        public int LanguageId { get; set; }
        public virtual ICollection<MenuItem> Menus { get; set; }
        public bool IsChild { get; set; }
    }
}