﻿using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Fundamentals.Models.Menu
{
    public class MenuWidget : OxideDatabaseModel, IOxideDataBaseModel
    {
        public int Key { get; set; }
        public virtual Menu Menu { get; set; }
    }
}