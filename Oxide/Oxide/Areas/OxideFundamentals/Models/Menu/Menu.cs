﻿using System.Collections.Generic;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Fundamentals.Models.Menu
{
    [OxideDatabase("OxideFundamentalsContext")]
    public class Menu : OxideDatabaseModel, IOxideDataBaseModel
    {
        public virtual ICollection<MenuItem> Menus { get; set; } = new HashSet<MenuItem>();
    }
}