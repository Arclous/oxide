﻿using Oxide.Core.Helpers;
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.Services.Interface.Registers.OxideStarter;
using Oxide.Media.Keys;
using Oxide.Media.Services.Setting;

namespace Oxide.Media
{
    public class StarterRegisterer : IOxideStarterRegisterer
    {
        public void Start(IProviderManager providerManager)
        {
            var mediaSettingService = providerManager.Provide<IMediaSettingService>();
            OxideUtils.AddProperty(MediaKeys.MediaFolder, mediaSettingService.LoadSetting(MediaKeys.MediaFolder));
        }
    }
}