﻿using System.Web.Optimization;
using Oxide.Core.Services.Interface.Registers.ResourceRegister;

namespace Oxide.Media.Resources
{
    public class ScriptRegisterer : IResourceRegisterer
    {
        public void RegisterResource(BundleCollection boundleCollection)
        {
            boundleCollection.Add(
                new ScriptBundle("~/bundles/bootstraptree").Include("~/Areas/Oxide.Media/Scripts/bootstrap-tree.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/fileBrowser").Include("~/Areas/Oxide.Media/Scripts/models/file-browser.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/nprogress").Include("~/Areas/Admin/vendors/nprogress/nprogress.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/dropzone").Include("~/Areas/Admin/vendors/dropzone/dist/dropzone.js"));
        }
    }
}