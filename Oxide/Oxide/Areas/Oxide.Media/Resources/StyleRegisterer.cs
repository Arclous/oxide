﻿using System.Web.Optimization;
using Oxide.Core.Services.Interface.Registers.ResourceRegister;

namespace Oxide.Media.Resources
{
    public class StyleRegisterer : IResourceRegisterer
    {
        public void RegisterResource(BundleCollection boundleCollection)
        {
            boundleCollection.Add(
                new StyleBundle("~/Oxide.Media/oxidemedia").Include("~/Areas/Oxide.Media/content/oxidemedia.css"));
            boundleCollection.Add(
                new StyleBundle("~/AdminContent/nprogress").Include("~/Areas/Admin/vendors/nprogress/nprogress.css"));
            boundleCollection.Add(
                new StyleBundle("~/AdminContent/dropzone").Include("~/Areas/Admin/vendors/dropzone/dist/dropzone.css"));
        }
    }
}