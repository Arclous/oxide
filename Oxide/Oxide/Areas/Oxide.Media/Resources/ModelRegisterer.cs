﻿using System.Web.Optimization;
using Oxide.Core.Services.Interface.Registers.ResourceRegister;

namespace Oxide.Media.Resources
{
    public class ModelRegisterer : IResourceRegisterer
    {
        public void RegisterResource(BundleCollection boundleCollection)
        {
            boundleCollection.Add(
                new ScriptBundle("~/models/SettingsModel").Include(
                    "~/Areas/Oxide.Media/Scripts/Models/Setting/SettingModels.js"));
        }
    }
}