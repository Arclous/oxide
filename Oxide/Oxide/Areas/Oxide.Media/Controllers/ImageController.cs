﻿using System.Web.Helpers;
using Oxide.Core.Base.OxideController;
using Oxide.Core.Managers.CacheManager;
using Oxide.Core.Services.OxideService;

namespace Oxide.Media.Controllers
{
    public class ImageController : OxideController
    {
        private readonly IOxideCacheManager _oxideCacheManager;

        public ImageController()
        {
        }

        public ImageController(IOxideServices oxideServices, IOxideCacheManager oxideCacheManager) : base(oxideServices)
        {
            _oxideCacheManager = oxideCacheManager;
        }

        public void GetResizedImage(string path, int width, int height)
        {
            var fileName = Server.MapPath(path);
            var cacheKey = $@"{path}{width}{height}";
            var res = _oxideCacheManager.GetOrSet(cacheKey, path, "System",
                () => new WebImage(fileName).Resize(width, height, false, true).Crop(1, 1), 120, false);
            res.Write();
        }
    }
}