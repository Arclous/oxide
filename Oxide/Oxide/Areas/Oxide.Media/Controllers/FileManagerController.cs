﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideController;
using Oxide.Core.Helpers;
using Oxide.Core.Models.Base;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Services.OxideUiService;
using Oxide.Core.Syntax;
using Oxide.Core.ViewModels.FilterViewModels;
using Oxide.Media.Keys;
using Oxide.Media.Models.Directory;
using Oxide.Media.Models.File;
using Oxide.Media.Services.Directory;
using Oxide.Media.Services.File;
using Oxide.Media.ViewModels.Directories;

namespace Oxide.Media.Controllers
{
    public class FileManagerController : OxideController
    {
        private readonly IDirectoryService _directoryService;
        private readonly IFileService _fileService;
        private readonly IOxideUiService _oxideUiService;

        public FileManagerController()
        {
        }

        public FileManagerController(IOxideServices oxideServices, IDirectoryService directoryService,
            IFileService fileService, IOxideUiService oxideUiService) : base(oxideServices)
        {
            _directoryService = directoryService;
            _fileService = fileService;
            _oxideUiService = oxideUiService;
        }

        [OxideAdmin]
        [OxideAdminPermission(Group = "File Manager", Permission = "Can Display File Manager",
            Key = "Can_Display_File_Manager")]
        public virtual ActionResult FileManager()
        {
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = "File Manager",
                    Title = Constants.Empty
                });
        }

        #region Dialogs

        [BlankFrame]
        [OxideAdminPermission(Group = "File Manager", Permission = "Can Display Media Dialog",
            Key = "Can_Display_Media_Dialog")]
        public virtual ActionResult DialogMediaManager()
        {
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = "File Manager",
                    Title = Constants.Empty
                });
        }

        [BlankFrame]
        [OxideAdminPermission(Group = "File Manager", Permission = "Can Display Media Dialog",
            Key = "Can_Display_Media_Dialog")]
        public virtual ActionResult SelectDialogMediaFile(string mediaSelecter, string funcNum)
        {
            dynamic content = _oxideUiService.GetContent(Constants.FileContent, mediaSelecter);
            string imageSrc = string.Format(@"~/{0}", content.Path);
            var dialogSelectFile = new DialogSelectFile {Path = Url.Content(imageSrc), FuncNum = funcNum};
            return View(dialogSelectFile);
        }

        [BlankFrame]
        [OxideAdminPermission(Group = "File Manager", Permission = "Can Display File Dialog",
            Key = "Can_Display_Media_Dialog")]
        public virtual ActionResult DialogFileManager()
        {
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = "File Manager",
                    Title = Constants.Empty
                });
        }

        [BlankFrame]
        [OxideAdminPermission(Group = "File Manager", Permission = "Can Display File Dialog",
            Key = "Can_Display_Media_Dialog")]
        public virtual ActionResult SelectDialogFile(string fileSelecter, string funcNum)
        {
            dynamic content = _oxideUiService.GetContent(Constants.FileContent, fileSelecter);
            string imageSrc = string.Format(@"~/{0}", content.Path);
            var dialogSelectFile = new DialogSelectFile {Path = Url.Content(imageSrc), FuncNum = funcNum};
            return View(dialogSelectFile);
        }

        #endregion

        #region DirectoryBrowser

        #region Helpers

        [HttpGet]
        [OxideAdminPermission(Group = "File Manager", Permission = "Can Display File Manager",
            Key = "Can_Display_File_Manager")]
        public virtual JsonResult DirectoryList()
        {
            var directoryList = new DirectoriesViewModel {Directories = new List<Directory>()};
            var mediaPath = OxideUtils.GetProperty(MediaKeys.MediaFolder);
            var result = _directoryService.GetDirectory(mediaPath);
            if (result.Success)
            {
                directoryList.Directories.Add(((Directory) result.Data));
                return Json(new {success = true, data = ((Directory) result.Data)}, JsonRequestBehavior.AllowGet);
            }
            return Json(new {success = false}, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = "File Manager", Permission = "Can Display File Manager",
            Key = "Can_Display_File_Manager")]
        public virtual JsonResult DirectorySubList(int id)
        {
            var directoryList = new DirectoriesViewModel {Directories = new List<Directory>()};
            var result = _directoryService.GetSubDirectories(id);
            if (result.Success)
            {
                directoryList.Directories = (List<Directory>) result.Data;
                return Json(new {success = true, data = directoryList.Directories}, JsonRequestBehavior.AllowGet);
            }
            return Json(new {success = false}, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = "File Manager", Permission = "Can Create Directory", Key = "Can_Create_Directory")
        ]
        public virtual JsonResult CreateDirectory(int parentId, string name)
        {
            var result = _directoryService.CreateDirectory(parentId, name);
            return result.Success
                ? Json(new {success = true, data = ((Directory) result.Data)}, JsonRequestBehavior.AllowGet)
                : Json(new {success = false}, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = "File Manager", Permission = "Can Rename Directory", Key = "Can_Rename_Directory")
        ]
        public virtual JsonResult RenameDirectory(int id, string name)
        {
            var result = _directoryService.RenameDirectory(id, name);
            return Json(result.Success ? new {success = true} : new {success = false}, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = "File Manager", Permission = "Can Delete Directory", Key = "Can_Delete_Directory")
        ]
        public virtual JsonResult DeleteDirectory(int id)
        {
            var result = _directoryService.DeleteDirectory(id);
            return Json(result.Success ? new {success = true} : new {success = false}, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #endregion

        #region FileBrowser

        #region Helpers

        [HttpPost]
        [OxideAdminPermission(Group = "File Manager", Permission = "Can Upload File", Key = "Can_Upload_File")]
        public virtual JsonResult UploadFile(HttpPostedFileBase file, int parentId)
        {
            var result = new Response();
            if (file != null && file.ContentLength > 0)
                result = _fileService.UploadFile(parentId, file);
            else
                result.Success = false;
            return Json(result.Success ? new {success = true} : new {success = false}, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = "File Manager", Permission = "Can Display File Manager",
            Key = "Can_Display_File_Manager")]
        public virtual JsonResult FileList(int? pageSize, int? page, string filter, int directoryId)
        {
            if (pageSize == null) pageSize = 10;
            if (page == null) page = 0;
            var result = _fileService.GetFilesWithFilter((int) pageSize, (int) page, filter, directoryId);
            return result.Success
                ? Json(new {success = true, data = (FilterModel<File>) result.Data}, JsonRequestBehavior.AllowGet)
                : Json(new {success = false}, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = "File Manager", Permission = "Can Rename File", Key = "Can_Rename_File")]
        public virtual JsonResult RenameFile(int id, string name)
        {
            var result = _fileService.RenameFile(id, name);
            return
                Json(
                    result.Success
                        ? new {success = true, path = result.Data}
                        : new {success = false, path = result.Data}, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = "File Manager", Permission = "Can Delete File", Key = "Can_Delete_File")]
        public virtual JsonResult DeleteFiles(string ids)
        {
            var fileIds = Array.ConvertAll(ids.Split(Constants.Splitter), int.Parse);
            var result = _fileService.DeleteAllFiles(fileIds);
            return Json(result.Success ? new {success = true} : new {success = false}, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = "File Manager", Permission = "Can Delete File", Key = "Can_Delete_File")]
        public virtual JsonResult DeleteAllFiles(int directoryId)
        {
            var result = _fileService.DeleteDirectoryFiles(directoryId);
            return Json(result.Success ? new {success = true} : new {success = false}, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = "File Manager", Permission = "Can Manage Files", Key = "Can_Manage_Files")]
        public virtual JsonResult PasteFiles(string ids, int actionType, int targetDirectory)
        {
            var result = new Response();
            var fileIds = Array.ConvertAll(ids.Split(Constants.Splitter), int.Parse);
            if (fileIds.Length == 0) return Json(new {success = false}, JsonRequestBehavior.AllowGet);
            switch (actionType)
            {
                case 0:
                    result = _fileService.CopyFiles(fileIds, targetDirectory);
                    break;
                case 1:
                    result = _fileService.MoveFiles(fileIds, targetDirectory);
                    break;
            }
            return Json(result.Success ? new {success = true} : new {success = false}, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = "File Manager", Permission = "Can Manage Files", Key = "Can_Manage_Files")]
        public virtual JsonResult ArchiveFiles(string ids, int targetDirectory, string archiveName)
        {
            var fileIds = Array.ConvertAll(ids.Split(Constants.Splitter), int.Parse);
            var result = _fileService.ArchiveFiles(fileIds, archiveName, targetDirectory);
            return Json(result.Success ? new {success = true} : new {success = false}, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = "File Manager", Permission = "Can Manage Files", Key = "Can_Manage_Files")]
        public virtual JsonResult ExtractArchiveFile(int targetDirectory, int archiveFile)
        {
            var result = _fileService.ExtractArchiveFile(targetDirectory, archiveFile);
            return Json(result.Success ? new {success = true} : new {success = false}, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = "File Manager", Permission = "Can Download Files", Key = "Can_Download_Files")]
        public virtual JsonResult DownloadFiles(string ids, int targetDirectory)
        {
            var fileIds = Array.ConvertAll(ids.Split(Constants.Splitter), int.Parse);
            var result = _fileService.DownloadFiles(fileIds, targetDirectory);
            return
                Json(
                    result.Success
                        ? new {success = true, path = result.Data}
                        : new {success = false, path = result.Data}, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #endregion
    }
}