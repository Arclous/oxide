﻿using System.Web.Mvc;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideController;
using Oxide.Core.Models.Base;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Syntax;
using Oxide.Media.Services.Setting;

namespace Oxide.Media.Controllers
{
    public class SettingsController : OxideController
    {
        private readonly IMediaSettingService _mediaSettingService;

        public SettingsController()
        {
        }

        public SettingsController(IOxideServices oxideServices, IMediaSettingService mediaSettingService)
            : base(oxideServices)
        {
            _mediaSettingService = mediaSettingService;
        }

        [OxideAdmin]
        [OxideAdminPermission(Group = "File Manager Settings", Permission = "Can Manage Settings",
            Key = "Can_Manage_File_Manager_Settings")]
        public virtual ActionResult Settings()
        {
            var urlHelper = new UrlHelper(Request.RequestContext);
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = "Media",
                    Title = "Media Settings",
                    Parameters = urlHelper.Action("SettingsList", "Settings", new {area = "Oxide.Media"})
                });
        }

        [OxideAdmin]
        [OxideAdminPermission(Group = "File Manager Settings", Permission = "Can Manage Settings",
            Key = "Can_Manage_File_Manager_Settings")]
        public virtual ActionResult UpdateSettings(FormCollection collection)
        {
            foreach (var v in collection)
            {
                var value = collection[v.ToString()];
                _mediaSettingService.SaveSetting(v.ToString(), value);
            }
            return RedirectToAction("Settings");
        }

        #region Helpers

        [HttpGet]
        [OxideAdminPermission(Group = "File Manager Settings", Permission = "Can Manage Settings",
            Key = "Can_Manage_File_Manager_Settings")]
        public virtual JsonResult SettingsList()
        {
            var settings = _mediaSettingService.LoadAllSettings();
            return Json(settings, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}