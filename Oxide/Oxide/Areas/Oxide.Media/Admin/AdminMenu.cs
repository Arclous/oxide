﻿using System.Web.Mvc;
using System.Web.Routing;
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.Models.Menus;
using Oxide.Core.Services.AdminMenuService;
using Oxide.Core.Services.Interface.Registers.AdminMenuRegister;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Syntax;

namespace Oxide.Media.Admin
{
    public class AdminMenu : IAdminMenuRegisterer
    {
        private const string MedieSettingsPermissionKey = "Can_Manage_File_Manager_Settings";
        private const string FileManagerPermissionKey = "Can_Display_File_Manager";
        private const string AreaName = "Oxide.Media";
        private readonly IProviderManager _providerManager = new ProviderManager();
        public void BuildMenus(RequestContext requestContext, IAdminMenuService adminMenuService)
        {
            var urlHelper = new UrlHelper(requestContext);
            var oxideService = _providerManager.Provide<IOxideServices>();

            //*******************************************************************************************************************************************
            //Media Header
            var mediaMenuHeader = adminMenuService.AddHeaderMenu("Media", oxideService.GetText("Media", AreaName),
                Syntax.CssClass.Media, 10, PermissionKeys.Empty);
            //Settings
            var menuItem = new MenuItem
            {
                Key = "Settings",
                Title = oxideService.GetText("Settings", AreaName),
                ControllerName = "Settings",
                ActionName = "Settings",
                PermissionKey = MedieSettingsPermissionKey,
                Order = 0
            };
            menuItem.UrlAddress = urlHelper.Action(menuItem.ActionName, menuItem.ControllerName, new {area = AreaName});
            adminMenuService.AddMenuItem(mediaMenuHeader, menuItem);

            //*******************************************************************************************************************************************
            //Navigation Header
            var fileExpolorereHeader = adminMenuService.AddHeaderMenu("File Manager",
                oxideService.GetText("File Manager", AreaName), Syntax.CssClass.FileBrowser, 9, PermissionKeys.Empty);
            //Menus
            menuItem = new MenuItem
            {
                Key = "File Manager",
                Title = oxideService.GetText("File Manager", AreaName),
                ControllerName = "FileManager",
                ActionName = "FileManager",
                PermissionKey = FileManagerPermissionKey,
                Order = 0
            };
            menuItem.UrlAddress = urlHelper.Action(menuItem.ActionName, menuItem.ControllerName, new {area = AreaName});
            adminMenuService.AddMenuItem(fileExpolorereHeader, menuItem);
            //*******************************************************************************************************************************************
        }
    }
}