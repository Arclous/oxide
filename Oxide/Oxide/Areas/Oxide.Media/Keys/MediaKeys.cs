﻿namespace Oxide.Media.Keys
{
    public static class MediaKeys
    {
        public const string MediaFolder = "Media_Folder";
        public const string Provider = "Provider";
    }
    public enum FileType
    {
        Image = 0,
        Video = 1,
        File = 2
    }
    public class FileExtensions
    {
        public static string[] ImageExtensions = {".png", ".jpg", ".jpeg", ".gif", ".bmp", ".tif"};
        public static string[] VideoExtensions = {".avi", ".mp4", ".mov"};
    }
}