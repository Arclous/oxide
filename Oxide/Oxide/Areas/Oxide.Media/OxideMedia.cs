﻿using Oxide.Core.Elements;

namespace Oxide.Media
{
    public class OxideMedia : IOxideModule
    {
        public int Id { get; } = 1;
        public string Name { get; } = "Oxide.Media";
        public string Title { get; } = "Oxide Media";
        public string Description { get; } = "Media structure for the cms and front pages";
        public string Author { get; } = "Inevera Studios";
        public string Category { get; } = "Media";
        public string PreviewImageUrl { get; } = "/Areas/Oxide.Media/Content/oxide.png";
        public bool Active { get; set; }
        public string Dependencies { get; set; }
    }
}