﻿using System.Collections.Generic;
using System.Linq;
using Oxide.Core.Elements;
using Oxide.Core.Helpers;
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.ResultModels;
using Oxide.Media.Services.File;
using Oxide.Media.ViewModels;

namespace Oxide.Fundamentals.Widgets.Dashboard
{
    public class FileStatistics : IOxideDashboardWidget
    {
        #region WidgetBasic
        public int Id => 1;
        public string Name => "File Statistics";
        public string Title => "Top 5 File Types";
        public string Description => "Displays Top 5 big size files.";
        public string Category => "Statistics";
        public string PreviewImageUrl => "/areas/OxideFundamentals/content/images/widgets/Carousel.png";
        public bool Active { get; set; }
        public string ModuleName => "Oxide.Media";
        public string FrontView { get; set; } = "Dashboard_FileStatistics";
        public string EditorView { get; set; } = "Dashboard_FileStatistics_Editor";
        public string WidgetKey { get; set; }
        public bool Manageable { get; set; }
        #endregion

        #region Renders
        public WidgetResult RenderDashboard(DashboardWidgetInfoPackage widgetInfoPacket,
            IProviderManager providerManager)
        {
            var widgetResult = new WidgetResult {ViewName = FrontView, Model = CreateViewModel(providerManager)};
            return widgetResult;
        }
        public WidgetResult RenderDashboardManagement(DashboardWidgetInfoPackage widgetInfoPacket,
            IProviderManager providerManager)
        {
            var widgetResult = new WidgetResult {ViewName = EditorView, Model = null};
            return widgetResult;
        }
        protected FileStatisticsViewModelPackage CreateViewModel(IProviderManager providerManager)
        {
            var fileService = providerManager.Provide<IFileService>();
            var fileStatisticsViewModelPackage = new FileStatisticsViewModelPackage
            {
                Files = new List<FileStatisticsViewModel>()
            };
            var mimeTypes =
                fileService.GetFiles()
                    .OrderByDescending(x => x.Size)
                    .Select(x => x.MimeType)
                    .Distinct()
                    .Take(5)
                    .ToList();
            var totalFiles = fileService.GetFiles().Count();
            foreach (var file in mimeTypes)
            {
                var mimeType = new FileStatisticsViewModel();
                mimeType.Type = file;
                mimeType.Total = fileService.GetFiles(x => x.MimeType == file).Count();
                var percent = OxideUtils.CalculatePercent(totalFiles, mimeType.Total);
                mimeType.Percent = string.Format(@"{0}%", percent);
                fileStatisticsViewModelPackage.Files.Add(mimeType);
            }
            fileStatisticsViewModelPackage.Total = totalFiles;
            return fileStatisticsViewModelPackage;
        }
        #endregion
    }
}