﻿ko.bindingHandlers.multiSelection = {
    init: function(element, valueAccessor, allBindingsAccessor) {
        //initialize bootstrapSwitch
        $(element).bootstrapSwitch();

        // setting initial value
        $(element).bootstrapSwitch("state", valueAccessor()());

        //handle the field changing
        $(element).on("switchChange.bootstrapSwitch", function(event, state) {
            var observable = valueAccessor();
            observable(state);
        });

        // Adding component options
        var options = allBindingsAccessor().bootstrapSwitchOptions || {};
        for (var property in options) {
            $(element).bootstrapSwitch(property, ko.utils.unwrapObservable(options[property]));
        }

        //handle disposal (if KO removes by the template binding)
        ko.utils.domNodeDisposal.addDisposeCallback(element, function() {
            $(element).bootstrapSwitch("destroy");
        });

    },
    //update the control when the view model changes
    update: function(element, valueAccessor, allBindingsAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        // Adding component options
        var options = allBindingsAccessor().bootstrapSwitchOptions || {};
        for (var property in options) {
            $(element).bootstrapSwitch(property, ko.utils.unwrapObservable(options[property]));
        }

        $(element).bootstrapSwitch("state", value);
    }
};
ko.bindingHandlers.bootstrapInputModal = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var props = valueAccessor(),
            vm = bindingContext.createChildContext(viewModel);
        ko.utils.extend(vm, props);
        vm.close = function() {
            vm.show(false);
            vm.onClose();
        };
        vm.action = function() {
            vm.onAction();
        };
        ko.utils.toggleDomNodeCssClass(element, "modal fade", true);
        ko.renderTemplate("inputTemplate", vm, null, element);
        var showHide = ko.computed(function() {
            $(element).modal(vm.show() ? "show" : "hide");
        });
        return {
            controlsDescendantBindings: true
        };
    }
};
ko.bindingHandlers.bootstrapModal = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var props = valueAccessor(),
            vm = bindingContext.createChildContext(viewModel);
        ko.utils.extend(vm, props);
        vm.close = function() {
            vm.show(false);
            vm.onClose();
        };
        vm.action = function() {
            vm.onAction();
        };
        ko.utils.toggleDomNodeCssClass(element, "modal fade", true);
        ko.renderTemplate("deleteConfirmTemplate", vm, null, element);
        var showHide = ko.computed(function() {
            $(element).modal(vm.show() ? "show" : "hide");
        });
        return {
            controlsDescendantBindings: true
        };
    }
};
ko.bindingHandlers.uploadModal = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var props = valueAccessor(),
            vm = bindingContext.createChildContext(viewModel);
        ko.utils.extend(vm, props);
        vm.close = function() {
            vm.show(false);
            vm.onClose();
        };
        vm.action = function() {
            vm.onAction();
        };
        ko.utils.toggleDomNodeCssClass(element, "modal fade", true);
        ko.renderTemplate("uploadPanelTemplate", vm, null, element);
        var showHide = ko.computed(function() {
            if (vm.show())
                $("#uplPanel").find(".dz-preview").remove();

            $(element).modal(vm.show() ? "show" : "hide");
        });
        return {
            controlsDescendantBindings: true
        };
    }
};
ko.bindingHandlers.selectedFilesModal = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var props = valueAccessor(),
            vm = bindingContext.createChildContext(viewModel);
        ko.utils.extend(vm, props);
        vm.close = function() {
            vm.show(false);
            vm.onClose();
        };
        vm.action = function() {
            vm.onAction();
        };
        ko.utils.toggleDomNodeCssClass(element, "modal fade", true);
        ko.renderTemplate("selectedFilesPanelTemplate", vm, null, element);
        var showHide = ko.computed(function() {
            if (vm.show())
                $("#uplPanel").find(".dz-preview").remove();

            $(element).modal(vm.show() ? "show" : "hide");
        });
        return {
            controlsDescendantBindings: true
        };
    }
};

//#Todo Needs to convert javascript to knockout
function DirectoriesViewModel(urlBase, addUrl, removeUrl, renameUrl, fileListUrl, fileRenameUrl, fileDeleteUrl, filePasteUrl, fileAllDeleteUrl, fileArchiveUrl, extractFileArchiveUrl, downloadFilesUrl, drectorySubList) {
    var self = this;
    //Folder
    self.addUrl = ko.observable(addUrl);
    self.removeUrl = ko.observable(removeUrl);
    self.renameUrl = ko.observable(renameUrl);
    self.directories = ko.observableArray([]);
    self.selectedNode = ko.observable();
    self.fileViewModel = ko.observable();
    self.isLoading = ko.observable(false);
    self.directoryLabel = ko.observable();
    //File
    self.totalRecords = ko.observable();
    self.totalPage = ko.observable();
    self.currentPage = ko.observable();
    self.displayinRecords = ko.observable();
    self.nextEnable = ko.observable(true);
    self.previousEnable = ko.observable(true);
    self.files = ko.observableArray([]);
    self.pageSize = ko.observable();
    self.actionFilter = ko.observable("");
    self.actionCurrentPage = ko.observable(0);
    self.actionPageSize = ko.observable(10);
    self.selectedFiles = ko.observableArray([]);
    self.multiSelectionOption = ko.observable(true);
    self.multiSelectionOption.subscribe(function(newValue) {
        self.selectedFiles.removeAll();
        self.updateRows();
    });
    self.actionType = ko.observable(0);
    //Labels
    self.lblName = ko.observable(FileManager.Name);

    self.loadDataDirectory = function() {
        self.isLoading(true);
        $.get(urlBase, {}, function(returnData) {
            if (returnData.success) {
                var directory = new DirectoryItem(returnData.data, this);
                self.directories.push(directory);
                self.isLoading(false);
                prepeareTreeView();
            } else
                showWarning(FileManager.Loading_directories_is_not_successful_, FileManager.Warning);
        });

    };
    self.loadSubData = function(data, object) {
        self.isLoading(true);
        $.get(drectorySubList, { id: data.id() }, function(returnData) {
            if (returnData.success) {
                for (var index = 0; index <= returnData.data.length - 1; index++) {
                    var directory = new DirectoryItem(returnData.data[index], object);
                    if (IsDirectoryAdded(object.directories(), directory.id()) == null)
                        object.directories.push(directory);
                }
                self.isLoading(false);
                prepeareTreeView();
                self.loadFiles(self.actionPageSize(), 0, self.actionFilter());
            } else
                showWarning(FileManager.Loading_directories_is_not_successful_, FileManager.Warning);
        });

    };

    self.selectDirectory = function(data, object) {
        self.selectedNode(data);
        self.directoryLabel(data.path);
        self.loadSubData(data, object);
    };
    //Dialogs
    //DirectoryItem Dialogs
    self.newDirectoryModal = {
        header: ko.observable(FileManager.New_Directory),
        message: ko.observable(FileManager.Please_enter_name_for_new_directory),
        currentValue: ko.observable(""),
        closeLabel: FileManager.Cancel,
        primaryLabel: FileManager.Create,
        show: ko.observable(false),
        onClose: function() {
        },
        onAction: function() {
            if (self.selectedNode() != undefined)
                self.createDirectory(self.selectedNode().id(), self.newDirectoryModal.currentValue());
            else
                self.createDirectory(-1, self.newDirectoryModal.currentValue());
        }
    };
    self.renameDirectoryModel = {
        header: ko.observable(FileManager.New_Directory),
        message: ko.observable(FileManager.Please_enter_name_for_new_directory),
        currentValue: ko.observable(),
        closeLabel: FileManager.Cancel,
        primaryLabel: FileManager.Rename,
        show: ko.observable(false),
        onClose: function() {
        },
        onAction: function() {
            self.renameDirectory(self.selectedNode().id(), self.renameDirectoryModel.currentValue());
        }
    };
    self.confirmModal = {
        header: ko.observable(FileManager.Please_Confirm),
        message: ko.observable(FileManager.Do_you_want_to_delete_selected_folder_),
        currentValue: ko.observable(),
        closeLabel: FileManager.Cancel,
        primaryLabel: FileManager.Delete,
        selectedFolder: ko.observable(),
        show: ko.observable(false),
        onClose: function() {

        },
        onAction: function() {
            self.deleteDirectory(self.selectedNode().id());
        }
    };
    //File Dialogs
    self.uploadPanel = {
        header: ko.observable(FileManager.Upload_Files),
        message: ko.observable(FileManager.Please_click_panel_or_drag_files_for_uploading),
        closeLabel: FileManager.Cancel,
        primaryLabel: FileManager.Ok,
        parentId: ko.observable(),
        show: ko.observable(false),
        onClose: function() {
            self.refresh();
        },
        onAction: function() {
            self.refresh();
        }
    };
    self.selectedFilesModal = {
        header: ko.observable(FileManager.Selected_Files),
        message: ko.observable(FileManager.You_can_manage_selected_files),
        selectedFiles: ko.observable([]),
        closeLabel: FileManager.Cancel,
        primaryLabel: FileManager.Ok,
        show: ko.observable(false),
        onClose: function() {

        },
        onAction: function() {

        }
    };
    self.renameFileModel = {
        header: ko.observable(FileManager.Rename_File),
        message: ko.observable(FileManager.Please_enter_new_name_for_file),
        currentValue: ko.observable(),
        closeLabel: FileManager.Cancel,
        primaryLabel: FileManager.Rename,
        show: ko.observable(false),
        onClose: function() {
        },
        onAction: function() {
            self.renameFile(self.selectedFiles()[0].id, self.renameFileModel.currentValue());
        }
    };
    self.fileDeleteConfirmModal = {
        header: ko.observable(FileManager.Please_Confirm),
        message: ko.observable(FileManager.Do_you_want_to_delete_selected_files_s__),
        currentValue: ko.observable(),
        closeLabel: FileManager.Cancel,
        primaryLabel: FileManager.Delete,
        selectedFiles: ko.observable(),
        show: ko.observable(false),
        onClose: function() {

        },
        onAction: function() {
            self.deleteFile(self.selectedFiles());
        }
    };
    self.fileDeleteAllConfirmModal = {
        header: ko.observable(FileManager.Please_Confirm),
        message: ko.observable(FileManager.Do_you_want_to_delete_all_file_s_under_this_direcotry_),
        currentValue: ko.observable(),
        closeLabel: FileManager.Cancel,
        primaryLabel: FileManager.Delete,
        selectedNode: ko.observable(),
        show: ko.observable(false),
        onClose: function() {

        },
        onAction: function() {
            self.deleteAllFiles(self.selectedNode());
        }
    };
    self.archiveInputModal = {
        header: ko.observable(FileManager.Archive_Name),
        message: ko.observable(FileManager.Please_enter_name_for_archive_file),
        currentValue: ko.observable(),
        closeLabel: FileManager.Cancel,
        primaryLabel: FileManager.Ok,
        show: ko.observable(false),
        onClose: function() {
        },
        onAction: function() {
            self.archiveFiles();
        }
    };

    //Dialog Helpers
    //DirectoryItem Dialogs
    self.showCreateDirectoryDialog = function() {
        self.newDirectoryModal.header(FileManager.New_Directory);
        self.newDirectoryModal.message(FileManager.Please_enter_name_for_new_directory);
        self.newDirectoryModal.show(true);
    };
    self.showRenameDialog = function() {
        if (self.isRoot()) {
            showWarning(FileManager.Root_is_selected_you_cannot_rename_the_root_directory__please_select_any_sub_directory_to_rename_, FileManager.Warning);
            return;
        }
        if (self.selectedNode() === undefined) {
            showWarning(FileManager.Please_select_folder_to_rename, FileManager.Warning);
            return;
        }
        self.renameDirectoryModel.header(FileManager.Rename_Directory);
        self.renameDirectoryModel.message(FileManager.Please_enter_name_for_directory);
        self.renameDirectoryModel.currentValue(self.selectedNode().name());
        self.renameDirectoryModel.show(true);
    };
    self.showConfirmModal = function() {
        if (self.isRoot()) {
            showWarning(FileManager.Root_is_selected_you_cannot_delete_the_root_directory__please_select_any_sub_directory_to_delete_, FileManager.Warning);
            return;
        }
        if (self.selectedNode() === undefined) {
            showWarning(FileManager.Please_select_folder_to_delete);
            return;
        }
        self.confirmModal.header(FileManager.Please_Confirm, FileManager.Warning);
        self.confirmModal.message(FileManager.Do_you_want_to_delete_selected_folder_);
        self.confirmModal.selectedFolder(self.selectedNode());
        self.confirmModal.show(true);
    };
    //File Dialogs
    self.showUploadPanel = function() {
        self.uploadPanel.parentId(self.selectedNode().id());
        self.uploadPanel.header(FileManager.Upload_Files);
        self.uploadPanel.header(FileManager.Upload_Files);
        self.uploadPanel.header(FileManager.Upload_Files);
        self.uploadPanel.message(FileManager.Please_click_panel_or_drag_files_for_uploading);
        self.uploadPanel.show(true);
    };
    self.showSelectedFilesModal = function() {
        if (self.selectedFiles().length === 0) {
            showWarning(FileManager.There_is_no_any_selected_file_, FileManager.Warning);
            return;
        }
        self.selectedFilesModal.selectedFiles(self.selectedFiles());
        self.selectedFilesModal.header(FileManager.Selected_Files);
        self.selectedFilesModal.message(FileManager.You_can_manage_selected_files);
        self.selectedFilesModal.show(true);
    };
    self.showRenameFileDialog = function() {
        if (self.selectedFiles().length === 0) {
            showWarning(FileManager.Please_select_file_to_rename_, FileManager.Warning);
            return;
        }
        if (self.selectedFiles().length > 1) {
            showWarning(FileManager.More_than_one_files_selected_you_cannot_rename_more_than_one_file_at_the_same_time, FileManager.Warning);
            return;
        }
        self.renameFileModel.header(FileManager.Rename_File);
        self.renameFileModel.message(FileManager.Please_enter_new_name_for_file);
        self.renameFileModel.currentValue(self.selectedFiles()[0].name());
        self.renameFileModel.show(true);
    };
    self.showFileDeleteConfirmModal = function() {
        if (self.selectedFiles().length === 0) {
            showWarning(FileManager.Please_select_file_to_delete_, FileManager.Warning);
            return;
        }
        if (self.selectedFiles().length > 1) {
            showInfo(FileManager.Please_pay_attention_that_more_than_one_files_selected, FileManager.Info);
        }
        self.fileDeleteConfirmModal.header(FileManager.Please_Confirm, FileManager.Warning);
        if (self.selectedFiles().length > 1)
            self.fileDeleteConfirmModal.message(FileManager.Total + " " + self.selectedFiles().length + " " + FileManager.files_selected_Do_you_want_to_delete_selected_file_s__);
        else
            self.fileDeleteConfirmModal.message(FileManager.Total + " " + self.selectedFiles().length + " " + FileManager.files_selected_Do_you_want_to_delete_selected_file_s__);

        self.fileDeleteConfirmModal.selectedFiles(self.selectedFiles());
        self.fileDeleteConfirmModal.show(true);
    };
    self.showFileDeleteAllConfirmModal = function() {
        if (self.selectedNode() === undefined) {
            showWarning(FileManager.Please_select_directory_to_delete_files_, FileManager.Warning);
            return;
        }
        if (self.selectedNode().directories().length === 0) {
            showWarning(FileManager.There_is_no_any_file_inside_the_selected_directory, FileManager.Warning);
            return;
        }
        self.fileDeleteAllConfirmModal.header(FileManager.Please_Confirm, FileManager.Warning);
        self.fileDeleteAllConfirmModal.selectedNode(self.selectedNode());
        self.fileDeleteAllConfirmModal.show(true);
    };
    self.showArchiveFileDialog = function() {
        if (self.selectedFiles().length === 0) {
            showWarning(FileManager.Please_select_file_to_archive_, FileManager.Warning);
            return;
        }
        if (self.selectedFiles().length === 0) {
            showInfo(FileManager.Total + " " + self.selectedFiles().length + " " + FileManager.file_is_going_to_be_archived, FileManager.Info);
        } else if (self.selectedFiles().length > 1) {
            showInfo(FileManager.Total + " " + self.selectedFiles().length + " " + FileManager.files_are_going_to_be_archived, FileManager.Info);
        }
        self.archiveInputModal.header(FileManager.Archive_Name);
        self.archiveInputModal.message(FileManager.Please_enter_name_for_archive_file);
        self.archiveInputModal.currentValue(self.selectedNode().name());
        self.archiveInputModal.show(true);
    };

    //Functions
    self.createDirectory = function(parentId, name) {
        self.isLoading(true);
        self.newDirectoryModal.show(false);
        $.get(addUrl, { parentId: parentId, name: name }, function(returnData) {
            if (returnData.success) {
                var directoryModel = new DirectoryItem(returnData.data, self.selectedNode());
                self.selectedNode().directories.push(directoryModel);
                attachClickEvent(directoryModel.id());
            } else
                showWarning(FileManager.Creating_directory_is_not_success_, FileManager.Warning);
            self.isLoading(false);
        });

    };
    self.renameDirectory = function(id, name) {
        self.isLoading(true);
        self.renameDirectoryModel.show(false);
        $.get(renameUrl, { id: id, name: name }, function(returnData) {
            if (returnData.success)
                self.selectedNode().name(name);
            else
                showWarning(FileManager.Renaming_directory_is_not_successful_, FileManager.Warning);

            self.isLoading(false);
        });

    };
    self.deleteDirectory = function(id) {
        self.isLoading(true);
        self.confirmModal.show(false);
        $.get(removeUrl, { id: id }, function(returnData) {
            if (returnData.success)
                self.selectedNode().baseModel().directories.remove(self.selectedNode());
            else
                showWarning(FileManager.Deleting_directory_is_not_successful_, FileManager.Warning);

            self.isLoading(false);

        });
    };
    self.isRoot = function() {
        if (self.selectedNode() == self.directories()[0])
            return true;
        else
            return false;
    };
    //Files
    self.loadFiles = function(pagesize, page, filter) {
        if (self.selectedNode() == undefined) {
            return;
        }
        self.isLoading(true);
        $.get(fileListUrl, { pageSize: pagesize, page: page, filter: filter, directoryId: self.selectedNode().id() }, function(returnData) {
            self.files.removeAll();
            for (var i = 0; i <= returnData.data.Records.length - 1; i++) {
                var fileModel = new FileModel(
                    returnData.data.Records[i].Id,
                    returnData.data.Records[i].Name,
                    returnData.data.Records[i].Size,
                    returnData.data.Records[i].MimeType,
                    returnData.data.Records[i].ModifyDateTime,
                    returnData.data.Records[i].Path
                );
                self.addFile(fileModel);
            }
            self.totalRecords(returnData.data.TotalRecords);
            self.totalPage(returnData.data.TotalPages);
            self.currentPage(returnData.data.CurrentPage);
            self.displayinRecords(returnData.data.DisplayingRecords);
            self.nextEnable(returnData.data.NextEnable);
            self.previousEnable(returnData.data.PreviousEnable);
            self.isLoading(false);
            self.setControls();
            self.updateRows();
        });
    };
    self.addFile = function(model) {
        self.files.push(model);
    };
    self.refresh = function() {
        self.loadFiles(self.actionPageSize(), self.actionCurrentPage(), self.actionFilter());
    };
    self.setControls = function() {
        $("#datatable-checkbox_info").html(FileManager.Showing + " " + (self.currentPage() + 1) + " / " + self.totalPage());
        $(".dataTables_empty").html("");
        $("#datatable-checkbox_next").unbind();
        $("#datatable-checkbox_previous").unbind();
        $("#datatable-checkbox_next").addClass("disabled");
        $("#datatable-checkbox_previous").addClass("disabled");

        if (self.nextEnable()) {
            $("#datatable-checkbox_next").removeClass("disabled");
            $("#datatable-checkbox_next").click(function() {
                self.actionCurrentPage(self.actionCurrentPage() + 1);
                self.loadFiles(self.actionPageSize(), self.actionCurrentPage(), self.actionFilter());
            });
        }
        if (self.previousEnable()) {
            $("#datatable-checkbox_previous").removeClass("disabled");
            $("#datatable-checkbox_previous").click(function() {
                self.actionCurrentPage(self.actionCurrentPage() - 1);
                self.loadFiles(self.actionPageSize(), self.actionCurrentPage(), self.actionFilter());
            });
        }
    };
    self.selectFile = function(data) {
        var active = self.isFileSelected(data);
        if (self.multiSelectionOption()) {
            if (active === null)
                self.selectedFiles.push(data);
            else
                self.selectedFiles.remove(active);
        } else {
            if (active === null) {
                self.selectedFiles.removeAll();
                self.selectedFiles.push(data);
            } else
                self.selectedFiles.remove(active);
        }
        self.updateRows();
    };
    self.updateRows = function() {
        $("#datatable-checkbox").find(".fileRow").removeClass("selectedRow");
        for (var i = 0; i <= self.selectedFiles().length - 1; i++) {
            $("#row_" + self.selectedFiles()[i].id()).addClass("selectedRow");
        }
    };
    self.multiSelection = function() {
        self.multiSelectionOption(!self.multiSelectionOption());
    };
    self.renameFile = function(id, name) {
        self.isLoading(true);
        self.renameFileModel.show(false);
        $.get(fileRenameUrl, { id: id, name: name }, function(returnData) {
            if (returnData.success) {
                self.selectedFiles()[0].name(name);
                self.selectedFiles()[0].path(returnData.path);
                self.selectedFiles()[0].modifyDateTime(Date.now());
            } else
                showWarning(FileManager.Renaming_file_is_not_successful_, FileManager.Warning);

            self.isLoading(false);
        });

    };
    self.deleteFile = function() {
        self.isLoading(true);
        self.fileDeleteConfirmModal.show(false);
        $.get(fileDeleteUrl, { ids: self.joinValues(self.selectedFiles()) }, function(returnData) {
            if (returnData.success) {
                for (var i = 0; i <= self.selectedFiles().length - 1; i++) {
                    self.files.remove(self.selectedFiles()[i]);
                }
                self.selectedFiles.removeAll();
                self.updateRows();
            } else
                showWarning(FileManager.Deleting_file_is_not_successful_, FileManager.Warning);

            self.isLoading(false);
        });
    };
    self.keepForCopy = function() {
        if (self.selectedFiles().length === 0) {
            showWarning(FileManager.Please_select_file_to_copy_, FileManager.Warning);
            return;
        }
        if (self.selectedFiles().length > 0) {
            //Set action type for copy
            self.actionType(0);
            if (self.selectedFiles().length > 1)
                showInfo(FileManager.Total + " " + self.selectedFiles().length + " " + FileManager.files_ready_to_copy_Please_select_directory_to_paste, FileManager.Info);
            else
                showInfo(FileManager.Total + " " + self.selectedFiles().length + " " + FileManager.files_ready_to_copy_Please_select_directory_to_paste, FileManager.Info);
        }
    };
    self.keepForCut = function() {
        if (self.selectedFiles().length === 0) {
            showWarning(FileManager.Please_select_file_to_cut_, FileManager.Warning);
            return;
        }
        if (self.selectedFiles().length > 0) {
            //Set action type for cut
            self.actionType(1);
            showInfo(FileManager.Total + " " + self.selectedFiles().length + " " + FileManager.files_ready_to_cut_Please_select_directory_to_paste, FileManager.Info);
        }
    };
    self.pasteFiles = function() {
        self.isLoading(true);
        $.get(filePasteUrl, { ids: self.joinValues(self.selectedFiles()), actionType: self.actionType(), targetDirectory: self.selectedNode().id }, function(returnData) {
            if (returnData.success) {
                self.refresh();
                if (self.actionType() === 0) {
                    if (self.selectedFiles().length > 1)
                        showInfo(FileManager.Total + " " + self.selectedFiles().length + " " + FileManager.files_copied_to_folder + " " + self.selectedNode().name(), FileManager.Info);
                    else
                        showInfo(FileManager.Total + " " + self.selectedFiles().length + " " + FileManager.file_copied_to_folder + " " + self.selectedNode().name(), FileManager.Info);
                } else if (self.actionType() === 1) {
                    if (self.selectedFiles().length > 1)
                        showInfo(FileManager.Total + " " + self.selectedFiles().length + " " + FileManager.files_moved_to_folder + " " + self.selectedNode().name(), FileManager.Info);
                    else
                        showInfo(FileManager.Total + " " + self.selectedFiles().length + " " + FileManager.file_moved_to_folder + " " + self.selectedNode().name(), FileManager.Info);
                }
                self.selectedFiles.removeAll();
                self.updateRows();

            } else {
                if (self.actionType() === 0) {
                    if (self.selectedFiles().length === 1)
                        showWarning(FileManager.Copying_file_is_not_successful_, FileManager.Warning);
                    else if (self.selectedFiles().length > 1)
                        showWarning(FileManager.Copying_file_are_not_successful_, FileManager.Warning);
                } else if (self.actionType() === 1) {
                    if (self.selectedFiles().length === 1)
                        showWarning(FileManager.Moving_file_is_not_successful_, FileManager.Warning);
                    else if (self.selectedFiles().length > 1)
                        showWarning(FileManager.Moving_files_is_not_successful_, FileManager.Warning);
                }
            }

            self.isLoading(false);
        });
    };
    self.deleteAllFiles = function() {
        self.isLoading(true);
        self.fileDeleteAllConfirmModal.show(false);
        $.get(fileAllDeleteUrl, { directoryId: self.selectedNode().id() }, function(returnData) {
            if (returnData.success) {
                self.refresh();
                self.selectedFiles.removeAll();
                self.updateRows();

            } else
                showWarning(FileManager.Deleting_all_files_is_not_successful_, FileManager.Warning);

            self.isLoading(false);
        });
    };
    self.archiveFiles = function() {
        self.isLoading(true);
        self.archiveInputModal.show(false);
        $.get(fileArchiveUrl, { ids: self.joinValues(self.selectedFiles()), targetDirectory: self.selectedNode().id(), archiveName: self.archiveInputModal.currentValue() }, function(returnData) {
            if (returnData.success) {
                if (self.selectedFiles().length === 1)
                    showInfo(FileManager.Total + " " + self.selectedFiles().length + " " + FileManager.file_archived, FileManager.Info);
                else if (self.selectedFiles().length > 1)
                    showInfo(FileManager.Total + " " + self.selectedFiles().length + " " + FileManager.files_archived, FileManager.Info);

                self.refresh();
                self.selectedFiles.removeAll();
                self.updateRows();

            } else
                showWarning(FileManager.Creating_archive_file_is_not_successful_, FileManager.Warning);

            self.isLoading(false);
        });
    };
    self.extractArchiveFile = function() {
        if (self.selectedFiles().length === 0) {
            showWarning(FileManager.Please_select_archive_file_to_extract, FileManager.Warning);
            return;
        }
        if (self.selectedFiles().length > 1) {
            showWarning(FileManager.There_are_more_than_one_file_selected_please_select_only_archive_file, FileManager.Warning);
            return;
        }
        if (!self.isArchiveFile(self.selectedFiles()[0])) {
            showWarning(FileManager.Selected_file_is_not_archive_file_, FileManager.Warning);
            return;
        }
        self.isLoading(true);
        $.get(extractFileArchiveUrl, { targetDirectory: self.selectedNode().id(), archiveFile: self.selectedFiles()[0].id() }, function(returnData) {
            if (returnData.success) {
                showInfo(FileManager.Archive_file_extracted, FileManager.Info);
                self.refresh();
                self.selectedFiles.removeAll();
                self.updateRows();

            } else
                showWarning(FileManager.Extracting_archive_file_is_not_successful_, FileManager.Warning);

            self.isLoading(false);
        });
    };
    self.isArchiveFile = function(file) {
        if (file.type() === "application/x-zip-compressed")
            return true;
        else
            return false;
    };
    self.isFileSelected = function(file) {
        return ko.utils.arrayFirst(self.selectedFiles(), function(fileFound) {
            return fileFound.id() === file.id();
        });
    };
    self.downloadFiles = function() {
        if (self.selectedFiles().length === 0) {
            showWarning(FileManager.Please_select_file_to_download, FileManager.Warning);
            return;
        }

        if (self.selectedFiles().length === 1) {
            showInfo(FileManager.Download_startted, FileManager.Info);
            window.open(self.selectedFiles()[0].path().replace("~", ""), self.selectedFiles()[0].name());
            return;
        }

        self.isLoading(true);
        $.get(downloadFilesUrl, { ids: self.joinValues(self.selectedFiles()), targetDirectory: self.selectedNode().id() }, function(returnData) {
            if (returnData.success) {
                showInfo(FileManager.Download_startted, FileManager.Info);
                window.open(returnData.path.replace("~", ""), self.selectedNode().name());

            } else
                showWarning(FileManager.Download_is_not_successful_, FileManager.Warning);

            self.refresh();
            self.selectedFiles.removeAll();
            self.updateRows();
            self.isLoading(false);
        });
    };
    self.displayFile = function() {
        if (self.selectedFiles().length === 0) {
            showWarning(FileManager.Please_select_image_file_to_display, FileManager.Warning);
            return;
        }

        if (self.selectedFiles().length > 1) {
            showWarning(FileManager.There_are_more_than_one_file_selected_please_select_only_one_file_to_display, FileManager.Warning);
            return;
        }
        if (!self.isImageFile(self.selectedFiles()[0])) {
            showWarning(FileManager.Selected_file_is_not_image_file_please_select_image_to_display, FileManager.Warning);
            return;
        }
        if (self.selectedFiles().length === 1) {
            showInfo(FileManager.Download_startted, FileManager.Info);
            window.open(self.selectedFiles()[0].path().replace("~", ""), self.selectedFiles()[0].name());
            return;
        }

    };
    self.isImageFile = function(file) {
        if (file.type() === "image/jpeg")
            return true;
        else if (file.type() === "image/jpg")
            return true;
        else if (file.type() === "image/gif")
            return true;
        else if (file.type() === "image/tiff")
            return true;
        else
            return false;
    };
    self.joinValues = function(array) {
        var res = "";
        for (var i = 0; i <= array.length - 1; i++) {
            if (i === array.length - 1)
                res += array[i].id();
            else
                res += array[i].id() + ",";
        }
        return res;
    };
}

function DirectoryItem(data, baseModel) {
    var self = this;
    self.id = ko.observable(data.Id);
    self.name = ko.observable(data.Name);
    self.path = ko.observable(data.Path);
    self.parentId = ko.observable(data.ParentId);
    self.baseModel = ko.observable(baseModel);
    self.directories = ko.observableArray([]);
}

function IsDirectoryAdded(directories, id) {
    return ko.utils.arrayFirst(directories, function(directoryFound) {
        return directoryFound.id() === id;
    });
}

function attachClickEvent(id) {
    $("#lbl_" + id).on("click", function(e) {
        $(".directoryLabel").removeClass("btn-sm btn-primary");
        $(this).addClass("btn-sm btn-primary");
    });
}

function prepeareTreeView() {
    $(".tree > ul").attr("role", "tree").find("ul").attr("role", "group");
    $(".tree").find("li:has(ul)").find(" > span").off("click");
    $(".tree").find("li:has(ul)").addClass("parent_li").attr("role", "treeitem").find(" > span").attr("title", FileManager.Collapse_this_folder).on("click", function(e) {
        openTree($(this));
        e.stopPropagation();
    });

    $(".directoryLabel").on("click", function(e) {
        $(".directoryLabel").removeClass("btn-sm btn-primary");
        $(this).addClass("btn-sm btn-primary");
    });
}

function openTree(element) {
    var children = element.parent("li.parent_li").find(" > ul > li");
    if (children.is(":visible")) {
        children.hide("fast");
        element.attr("title", FileManager.Expand_this_folder).find(" > i").removeClass("fa fa-folder-open").addClass("fa fa-folder");
    } else {
        children.show("fast");
        element.attr("title", FileManager.Collapse_this_folder).find(" > i").removeClass("fa fa-folder").addClass("fa fa-folder-open");
    }
}

function FileModel(id, name, size, type, modifyDateTime, path) {
    var self = this;
    self.id = ko.observable(id);
    self.name = ko.observable(name);
    self.size = ko.observable(size);
    self.type = ko.observable(type);
    self.modifyDateTime = ko.observable(modifyDateTime);
    self.path = ko.observable(path);
    self.mB = ko.observable();

    self.mB = ko.computed(function() {
        if (self.size() >= 1000000000) {
            return (self.size() / 1000000000).toFixed(2) + " GB";
        } else if (self.size() >= 1000000) {
            return (self.size() / 1000000).toFixed(2) + " MB";
        } else if (self.size() >= 1000) {
            return (self.size() / 1000).toFixed(2) + " KB";
        } else if (self.size() > 1) {
            return self.size() + " " + FileManager.bytes;
        } else if (self.size() === 1) {
            return self.size() + " " + FileManager.byte;
        } else {
            return "0 " + FileManager.byte;
        }
    }, self);

    self.imageCss = ko.computed(function() {
        if (self.type() === "image/jpeg")
            return "glyphicon glyphicon-picture";
        if (self.type() === "image/jpg")
            return "glyphicon glyphicon-picture";
        if (self.type() === "image/gif")
            return "glyphicon glyphicon-picture";
        if (self.type() === "image/tiff")
            return "glyphicon glyphicon-picture";
        if (self.type() === "video/avi")
            return "glyphicon glyphicon-facetime-video";
        if (self.type() === "video/msvideo")
            return "glyphicon glyphicon-facetime-video";
        if (self.type() === "video/x-msvideo")
            return "glyphicon glyphicon-facetime-video";
        if (self.type() === "video/avs-video")
            return "glyphicon glyphicon-facetime-video";
        if (self.type() === "audio/mpeg")
            return "glyphicon glyphicon-headphones";
        if (self.type() === "audio/midi")
            return "glyphicon glyphicon-headphones";
        if (self.type() === "audio/mpeg3")
            return "glyphicon glyphicon-headphones";
        if (self.type() === "audio/wav")
            return "glyphicon glyphicon-headphones";
        if (self.type() === "application/x-compressed")
            return "glyphicon glyphicon-briefcase";
        if (self.type() === "application/x-zip-compressed")
            return "glyphicon glyphicon-briefcase";
        if (self.type() === "application/zip")
            return "glyphicon glyphicon-briefcase";
        if (self.type() === "multipart/x-zip")
            return "glyphicon glyphicon-briefcase";
        if (self.type() === "text/html")
            return "glyphicon glyphicon-globe";
        if (self.type() === "text/plain")
            return "glyphicon glyphicon-text-color";
        else
            return "glyphicon glyphicon-list-alt";

    }, self);

}