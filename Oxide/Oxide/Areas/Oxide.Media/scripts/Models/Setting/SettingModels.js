﻿function SettingsModel(urlBase) {
    var self = this;
    self.isLoading = ko.observable(false);
    self.settings = ko.observableArray([]);
    self.loadData = function() {
        self.isLoading(true);
        $.get(urlBase, {}, function(data) {
            self.settings.removeAll();
            for (var i = 0; i <= data.Settings.length - 1; i++) {
                var settingModel = new SettingModel(
                    data.Settings[i].Id,
                    data.Settings[i].Name,
                    data.Settings[i].Title,
                    data.Settings[i].Key,
                    data.Settings[i].Value
                );
                self.addSetting(settingModel);

            }
            self.isLoading(false);
        });
    };
    self.addSetting = function(model) {
        self.settings.push(model);
    }.bind(self);
}

function SettingModel(id, name, title, key, value) {
    var self = this;
    self.isWorking = ko.observable(false);
    self.id = ko.observable(id);
    self.name = ko.observable(name);
    self.title = ko.observable(title);
    self.key = ko.observable(key);
    self.value = ko.observable(value);
}