﻿using System.Collections.Generic;
using Oxide.Media.Models.Directory;

namespace Oxide.Media.ViewModels.Directories
{
    public class DirectoriesViewModel
    {
        public List<Directory> Directories;
    }
}