﻿using System.Collections.Generic;

namespace Oxide.Media.ViewModels.Setting
{
    public class SettingsPackage
    {
        public List<Models.Setting.Setting> Settings { get; set; }
    }
}