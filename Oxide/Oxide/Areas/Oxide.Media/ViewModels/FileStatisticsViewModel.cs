﻿using System.Collections.Generic;

namespace Oxide.Media.ViewModels
{
    public class FileStatisticsViewModel
    {
        public string Percent { get; set; }
        public string Type { get; set; }
        public int Total { get; set; }
    }
    public class FileStatisticsViewModelPackage
    {
        public List<FileStatisticsViewModel> Files { get; set; }
        public long Total { get; set; }
    }
}