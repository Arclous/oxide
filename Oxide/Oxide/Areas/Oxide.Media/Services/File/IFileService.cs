﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web;
using Oxide.Core.Base.OxideDependency;
using Oxide.Core.Models.Base;

namespace Oxide.Media.Services.File
{
    /// <summary>
    ///     File service is the service which will handle file actions both on disk and database
    /// </summary>
    public interface IFileService : IOxideDependency
    {
        /// <summary>
        ///     Upload file inside directory and inset to database
        /// </summary>
        /// <param name="directoryId">Id of the container directory</param>
        /// <param name="postedFile">Posted file</param>
        Response UploadFile(int directoryId, HttpPostedFileBase postedFile);
        /// <summary>
        ///     Upload file inside directory and inset to database
        /// </summary>
        /// <param name="directoryId">Id of the container directory</param>
        /// <param name="path">Path of the file</param>
        Response UploadLocalFile(int directoryId, string path);
        /// <summary>
        ///     Deletes all files inside directory
        /// </summary>
        /// <param name="directoryId">Id of the director</param>
        Response DeleteDirectoryFiles(int directoryId);
        /// <summary>
        ///     Deletes files according to ids
        /// </summary>
        /// <param name="ids">Ids of the files for deleting</param>
        Response DeleteAllFiles(int[] ids);
        /// <summary>
        ///     Copy files to target directory
        /// </summary>
        /// <param name="ids">Ids of the files which will be copied</param>
        /// <param name="targetDirectoryId">Id of the target directory</param>
        Response CopyFiles(int[] ids, int targetDirectoryId);
        /// <summary>
        ///     Moves files to target directory
        /// </summary>
        /// <param name="ids">Ids of the file which will be moved</param>
        /// <param name="targetDirectoryId">Id of the target directory</param>
        Response MoveFiles(int[] ids, int targetDirectoryId);
        /// <summary>
        ///     Renames file
        /// </summary>
        /// <param name="id">Id of the file which will be ranamed</param>
        /// <param name="name">New name of the file</param>
        Response RenameFile(int id, string name);
        /// <summary>
        ///     Archives files
        /// </summary>
        /// <param name="ids">Id of the file which will be added to archive</param>
        /// <param name="archiveName">Name of the archive file</param>
        /// <param name="targetDirectory">Id of target directory for creating archive file</param>
        Response ArchiveFiles(int[] ids, string archiveName, int targetDirectory);
        /// <summary>
        ///     Extracts archive files
        /// </summary>
        /// <param name="targetDirectory">Id of target directory for extracting archive file</param>
        /// <param name="archiveFile">Id of the archive file which will be extracted</param>
        Response ExtractArchiveFile(int targetDirectory, int archiveFile);
        /// <summary>
        ///     Gets files
        /// </summary>
        /// <param name="pageSize">Size of the page, count of items for each page</param>
        /// <param name="page">Number of the page which will be requested</param>
        /// <param name="filter">Filter for filtering results</param>
        /// <param name="directoryId">Id of the directory for filtering files</param>
        Response GetFilesWithFilter(int pageSize, int page, string filter, int directoryId);
        /// <summary>
        ///     Downloads files
        /// </summary>
        /// <param name="ids">Ids of the files which will be downloaded</param>
        /// <param name="targetDirectory">
        ///     Id of target directory for archiving files to download in case of more than one file
        ///     requested to download
        /// </param>
        Response DownloadFiles(int[] ids, int targetDirectory);
        /// <summary>
        ///     Gets file information from database
        /// </summary>
        /// <param name="id">Id of the requested file</param>
        Response GetFile(int id);
        /// <summary>
        ///     Gets files according to predicate
        /// </summary>
        /// <param name="predicate">Predicate of the filter</param>
        IEnumerable<Models.File.File> GetFiles(Expression<Func<Models.File.File, bool>> predicate);
        /// <summary>
        ///     Gets files
        /// </summary>
        IEnumerable<Models.File.File> GetFiles();
    }
}