﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using Oxide.Core.Data.Repository;
using Oxide.Core.Extesions;
using Oxide.Core.Models.Base;
using Oxide.Core.Services.StorageService;
using Oxide.Core.Syntax;
using Oxide.Core.ViewModels.FilterViewModels;
using Oxide.Media.Keys;

namespace Oxide.Media.Services.File
{
    public class FileService : IFileService
    {
        private readonly IOxideRepository<Models.Directory.Directory> _directoryRepository;
        private readonly IOxideRepository<Models.File.File> _fileRepository;
        private readonly Response _response = new Response();
        private readonly IStoregeService _storegeService;

        public FileService(IOxideRepository<Models.File.File> fileRepository,
            IOxideRepository<Models.Directory.Directory> directoryRepository, IStoregeService storegeService)
        {
            _fileRepository = fileRepository;
            _directoryRepository = directoryRepository;
            _storegeService = storegeService;
        }

        public Response UploadFile(int directoryId, HttpPostedFileBase postedFile)
        {
            var newFile = new Models.File.File {Name = postedFile.FileName.GetSafeName(), Title = postedFile.FileName.GetSafeName() };
            var directory = _directoryRepository.GetById(directoryId);
            newFile.Path = Path.Combine(directory.Path, newFile.Name);
            var pyshicalPath = HttpContext.Current.Server.MapPath(newFile.Path);
            if (!_storegeService.IsFileExist(pyshicalPath))
            {
                if (pyshicalPath != null) postedFile.SaveAs(pyshicalPath);
            }
            else
            {
                newFile.Name = Path.GetRandomFileName().GetSafeName() + Path.GetExtension(postedFile.FileName);
                newFile.Title = newFile.Name;
                newFile.Path = Path.Combine(directory.Path, newFile.Name);
                pyshicalPath = HttpContext.Current.Server.MapPath(newFile.Path);
                if (pyshicalPath != null) postedFile.SaveAs(pyshicalPath);
            }
            var fileInfo = _storegeService.GetFileInfo(pyshicalPath);
            newFile.DirectoryId = directoryId;

            //Set file type
            if (FileExtensions.ImageExtensions.Contains(fileInfo.Extension))
                newFile.FileType = FileType.Image;
            else if (FileExtensions.VideoExtensions.Contains(fileInfo.Extension))
                newFile.FileType = FileType.Video;
            else
                newFile.FileType = FileType.File;
            newFile.UploaDateTime = DateTime.Now;
            newFile.ModifyDateTime = DateTime.Now;
            newFile.Size = postedFile.ContentLength;
            newFile.MimeType = MimeMapping.GetMimeMapping(newFile.Path);
            newFile.Path = newFile.Path.GetSafeUrl();
            _fileRepository.Insert(newFile);
            _response.Data = newFile;
            _response.Success = true;
            return _response;
        }

        public Response UploadLocalFile(int directoryId, string path)
        {
            var directory = _directoryRepository.GetById(directoryId);
            var newFile = new Models.File.File {Name = "Logo.png", Title = "Logo.png"};
            newFile.Path = Path.Combine(directory.Path, newFile.Name);
            var pyshicalPath = HttpContext.Current.Server.MapPath(newFile.Path);
            if (!_storegeService.IsFileExist(pyshicalPath))
            {
                if (pyshicalPath != null) System.IO.File.Copy(path, pyshicalPath);
            }
            else
            {
                newFile.Name = Path.GetRandomFileName().GetSafeName() + Path.GetExtension(newFile.Name);
                newFile.Title = newFile.Name;
                newFile.Path = Path.Combine(directory.Path, newFile.Name);
                pyshicalPath = HttpContext.Current.Server.MapPath(newFile.Path);
                if (pyshicalPath != null) System.IO.File.Copy(path, pyshicalPath);
            }
            var fileInfo = _storegeService.GetFileInfo(pyshicalPath);
            newFile.DirectoryId = directory.Id;

            //Set file type
            if (FileExtensions.ImageExtensions.Contains(fileInfo.Extension))
                newFile.FileType = FileType.Image;
            else if (FileExtensions.VideoExtensions.Contains(fileInfo.Extension))
                newFile.FileType = FileType.Video;
            else
                newFile.FileType = FileType.File;
            newFile.UploaDateTime = DateTime.Now;
            newFile.ModifyDateTime = DateTime.Now;
            newFile.Size = fileInfo.Length;
            newFile.MimeType = MimeMapping.GetMimeMapping(newFile.Path);
            newFile.Path = newFile.Path.GetSafeUrl();
            _fileRepository.Insert(newFile);
            _response.Data = newFile;
            _response.Success = true;
            return _response;
        }

        public Response DeleteDirectoryFiles(int directoryId)
        {
            try
            {
                var fileList = _fileRepository.GetAll().Where(x => x.DirectoryId == directoryId);
                foreach (var file in fileList)
                {
                    if (!_storegeService.DeleteFile(file.Path))
                    {
                        _response.Success = false;
                        return _response;
                    }
                    _fileRepository.LazyDelete(file);
                }
                _fileRepository.SaveChanges();
                _response.Success = true;
                return _response;
            }
            catch (Exception)
            {
                _response.Success = false;
                return _response;
            }
        }

        public Response DeleteAllFiles(int[] ids)
        {
            try
            {
                foreach (var file in
                    ids.Select(id => _fileRepository.GetById(Convert.ToInt32(id))).Where(file => file != null))
                {
                    if (!_storegeService.DeleteFile(file.Path))
                    {
                        _response.Success = false;
                        return _response;
                    }
                    _fileRepository.LazyDelete(file);
                }
                _fileRepository.SaveChanges();
                _response.Success = true;
                return _response;
            }
            catch (Exception)
            {
                _response.Success = false;
                return _response;
            }
        }

        public Response CopyFiles(int[] ids, int targetDirectoryId)
        {
            var targetPath = _directoryRepository.GetById(targetDirectoryId).Path;
            foreach (var fileId in ids)
            {
                var file = _fileRepository.GetById(fileId);
                var newPath = Path.Combine(targetPath, file.Name);
                var newName = file.Name;
                var path = newPath;
                if (_fileRepository.Get(x => x.Path == path).Any())
                {
                    newName = Path.GetRandomFileName() + Path.GetExtension(file.Path);
                    newPath = Path.Combine(targetPath, newName);
                }
                if (!_storegeService.CopyFile(file.Path, newPath))
                {
                    _response.Success = false;
                    return _response;
                }
                var newfile = new Models.File.File();
                file.CopyPropertiesTo(newfile, new[] {"Id", "DirectoryId"});
                newfile.Name = newName;
                newfile.Title = newName;
                newfile.DirectoryId = targetDirectoryId;
                newfile.Path = newPath;
                newfile.ModifyDateTime = DateTime.Now;
                newfile.UploaDateTime = DateTime.Now;
                newfile.Path = newfile.Path.GetSafeUrl();
                _fileRepository.LazyInsert(newfile);
            }
            _fileRepository.SaveChanges();
            _response.Success = true;
            return _response;
        }

        public Response MoveFiles(int[] ids, int targetDirectoryId)
        {
            var targetPath = _directoryRepository.GetById(targetDirectoryId).Path;
            foreach (var fileId in ids)
            {
                var file = _fileRepository.GetById(fileId);
                var newPath = Path.Combine(targetPath, file.Name);
                var newName = file.Name;
                var path = newPath;
                if (_fileRepository.Get(x => x.Path == path).Any())
                {
                    newName = Path.GetRandomFileName() + Path.GetExtension(file.Path);
                    newPath = Path.Combine(targetPath, newName);
                }
                if (!_storegeService.MoveFile(file.Path, newPath))
                {
                    _response.Success = false;
                    return _response;
                }
                file.Name = newName;
                file.Title = newName;
                file.DirectoryId = targetDirectoryId;
                file.Path = newPath;
                file.ModifyDateTime = DateTime.Now;
                file.UploaDateTime = DateTime.Now;
                file.Path = file.Path.GetSafeUrl();
                _fileRepository.LazyUpdate(file);
            }
            _fileRepository.SaveChanges();
            _response.Success = true;
            return _response;
        }

        public Response RenameFile(int id, string name)
        {
            try
            {
                name = name.GetSafeName();
                var file = _fileRepository.GetById(id);
                var parentFolder = _directoryRepository.GetById(file.DirectoryId);
                file.Name = name;
                file.ModifyDateTime = DateTime.Now;
                if (!_storegeService.RenameFile(name, file.Path))
                {
                    _response.Success = false;
                    return _response;
                }
                file.Path = Path.Combine(parentFolder.Path, file.Name);
                file.Path = file.Path.GetSafeUrl();
                _fileRepository.SaveChanges();
                _response.Data = file.Path;
                _response.Success = true;
                return _response;
            }
            catch (Exception)
            {
                _response.Success = false;
                return _response;
            }
        }

        public Response ArchiveFiles(int[] ids, string archiveName, int targetDirectory)
        {
            archiveName = archiveName.GetSafeName();
            var directory = _directoryRepository.GetById(targetDirectory);
            var zipFileName = Path.Combine(directory.Path, archiveName + Constants.ArchiveExtension);
            var zipName = archiveName + Constants.ArchiveExtension;
            if (_storegeService.IsFileExist(zipFileName))
            {
                zipName = Path.GetRandomFileName().GetSafeName() + Constants.ArchiveExtension;
                zipFileName = Path.Combine(directory.Path, zipName);
            }

            //Get path for all files which is going to be archived
            var arhiveFileList = _fileRepository.Get(x => ids.Contains(x.Id)).Select(x => x.Path).ToList();

            //Start to create archive file
            if (!_storegeService.CreateArchiveFile(arhiveFileList, zipFileName))
            {
                _response.Success = false;
                return _response;
            }
            if (_storegeService.IsFileExist(zipFileName))
            {
                var fileInfo = _storegeService.GetFileInfo(zipFileName);
                var archiveFile = new Models.File.File
                {
                    Name = zipName,
                    Title = zipName,
                    DirectoryId = targetDirectory,
                    UploaDateTime = DateTime.Now,
                    ModifyDateTime = DateTime.Now,
                    Path = zipFileName,
                    Size = fileInfo.Length
                };
                if (FileExtensions.ImageExtensions.Contains(fileInfo.Extension))
                    archiveFile.FileType = FileType.Image;
                else if (FileExtensions.VideoExtensions.Contains(fileInfo.Extension))
                    archiveFile.FileType = FileType.Video;
                else
                    archiveFile.FileType = FileType.File;
                archiveFile.MimeType = MimeMapping.GetMimeMapping(zipFileName);
                archiveFile.Path = archiveFile.Path.GetSafeUrl();
                _fileRepository.Insert(archiveFile);
                _response.Data = zipFileName;
                _response.Success = true;
                return _response;
            }
            _response.Success = false;
            return _response;
        }

        public Response ExtractArchiveFile(int targetDirectory, int archiveFile)
        {
            try
            {
                var directory = _directoryRepository.GetById(targetDirectory);
                var archive = _fileRepository.GetById(archiveFile);
                var result = _storegeService.ExtractArchiveFile(archive.Path, archive.Name, directory.Path);
                if (!result.Success)
                {
                    _response.Success = false;
                    return _response;
                }
                var fileList = (List<string>) result.Data;
                foreach (var file in fileList)
                {
                    var fileInfo = _storegeService.GetFileInfo(file);
                    var newFile = new Models.File.File
                    {
                        Name = fileInfo.Name,
                        Title = fileInfo.Name,
                        DirectoryId = targetDirectory,
                        UploaDateTime = DateTime.Now,
                        ModifyDateTime = DateTime.Now,
                        Path = Path.Combine(directory.Path, fileInfo.Name),
                        Size = fileInfo.Length
                    };
                    if (FileExtensions.ImageExtensions.Contains(fileInfo.Extension))
                        newFile.FileType = FileType.Image;
                    else if (FileExtensions.VideoExtensions.Contains(fileInfo.Extension))
                        newFile.FileType = FileType.Video;
                    else
                        newFile.FileType = FileType.File;
                    newFile.MimeType = MimeMapping.GetMimeMapping(file);
                    newFile.Path = newFile.Path.GetSafeUrl();
                    _fileRepository.LazyInsert(newFile);
                }
                _fileRepository.SaveChanges();
                _response.Success = true;
                return _response;
            }
            catch (Exception)
            {
                _response.Success = false;
                return _response;
            }
        }

        public Response GetFilesWithFilter(int pageSize, int page, string filter, int directoryId)
        {
            try
            {
                var fileFilterViewModel = new FilterModel<Models.File.File>();
                Expression<Func<Models.File.File, bool>> criteriaFilter =
                    x => x.Name.ToLower().Contains(filter.ToLower());
                Expression<Func<Models.File.File, bool>> criteriaDirectory = p => p.DirectoryId == directoryId;
                criteriaFilter = criteriaFilter.And(criteriaDirectory);
                if (filter != string.Empty)
                    fileFilterViewModel.Records =
                        _fileRepository.Get(criteriaFilter, o => o.OrderBy(x => x.Id), null, page, pageSize).ToList();
                else
                    fileFilterViewModel.Records =
                        _fileRepository.Get(criteriaDirectory, o => o.OrderBy(x => x.Id), null, page, pageSize).ToList();
                fileFilterViewModel.CurrentPage = page;
                fileFilterViewModel.DisplayingRecords = fileFilterViewModel.Records.Count();
                fileFilterViewModel.TotalRecords = _fileRepository.TotalRecords(x => x.DirectoryId == directoryId);
                if (filter != string.Empty)
                    fileFilterViewModel.TotalPages = (_fileRepository.Get(criteriaFilter).Count() + pageSize - 1)/
                                                     pageSize;
                else
                    fileFilterViewModel.TotalPages = (_fileRepository.Get(criteriaDirectory).Count() + pageSize - 1)/
                                                     pageSize;
                fileFilterViewModel.NextEnable = fileFilterViewModel.CurrentPage < fileFilterViewModel.TotalPages - 1;
                fileFilterViewModel.PreviousEnable = fileFilterViewModel.CurrentPage > 0;
                _response.Success = true;
                _response.Data = fileFilterViewModel;
                return _response;
            }
            catch (Exception)
            {
                _response.Success = false;
                return _response;
            }
        }

        public Response DownloadFiles(int[] ids, int targetDirectory)
        {
            return ArchiveFiles(ids, Path.GetRandomFileName(), targetDirectory);
        }

        public Response GetFile(int id)
        {
            try
            {
                var file = _fileRepository.GetById(id);
                if (_storegeService.IsFileExist(file.Path))
                {
                    _response.Success = true;
                    _response.Data = file;
                    return _response;
                }
                _response.Success = false;
                _response.Data = file;
                return _response;
            }
            catch (Exception)
            {
                _response.Success = false;
                return _response;
            }
        }

        public IEnumerable<Models.File.File> GetFiles(Expression<Func<Models.File.File, bool>> predicate)
        {
            return _fileRepository.Get(predicate).ToList();
        }

        public IEnumerable<Models.File.File> GetFiles()
        {
            return _fileRepository.Get().ToList();
        }
    }
}