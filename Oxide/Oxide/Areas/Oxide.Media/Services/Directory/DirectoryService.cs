﻿using System;
using System.IO;
using System.Linq;
using Oxide.Core.Data.Repository;
using Oxide.Core.Helpers;
using Oxide.Core.Models.Base;
using Oxide.Core.Services.StorageService;
using Oxide.Media.Keys;
using Oxide.Core.Extesions;
namespace Oxide.Media.Services.Directory
{
    public class DirectoryService : IDirectoryService
    {
        private readonly IOxideRepository<Models.Directory.Directory> _directoryRepository;
        private readonly IOxideRepository<Models.File.File> _fileRepository;
        private readonly Response _response = new Response();
        private readonly IStoregeService _storegeService;

        public DirectoryService(IOxideRepository<Models.Directory.Directory> directoryRepository,
            IOxideRepository<Models.File.File> fileRepository, IStoregeService storegeService)
        {
            _directoryRepository = directoryRepository;
            _fileRepository = fileRepository;
            _storegeService = storegeService;
        }

        public Response CreateDirectory(int parentId, string name)
        {
            try
            {
                
                var folder = new Models.Directory.Directory {ParentId = parentId, Name = name.GetSafeName(), Title = name.GetSafeName() };
                if (parentId != -1)
                {
                    var parentFolder = _directoryRepository.GetById(parentId, x => x.Directories);
                    folder.Path = Path.Combine(parentFolder.Path, folder.Name);
                    if (IsDirectoryExist(folder.Path))
                    {
                        _response.Success = false;
                        return _response;
                    }
                    parentFolder.Directories.Add(folder);
                    _directoryRepository.Update(parentFolder);
                    _response.Success = true;
                    _response.Data = folder;
                    if (_storegeService.CreateDirectory(folder.Path))
                        return _response;
                    parentFolder.Directories.Remove(folder);
                    _directoryRepository.Update(parentFolder);
                    _response.Success = false;
                    return _response;
                }
                folder.Path = Path.Combine(OxideUtils.GetProperty(MediaKeys.MediaFolder), folder.Name);
                if (IsDirectoryExist(folder.Path))
                {
                    _response.Success = false;
                    return _response;
                }
                _directoryRepository.Insert(folder);
                _response.Success = true;
                _response.Data = folder;
                if (_storegeService.CreateDirectory(folder.Path))
                    return _response;
                _directoryRepository.Delete(folder);
                _response.Success = false;
                return _response;
            }
            catch (Exception)
            {
                _response.Success = false;
                return _response;
            }
        }

        public Response RenameDirectory(int id, string name)
        {
            try
            {
                name = name.GetSafeName();
                var folder = _directoryRepository.GetById(id);
                var parentFolder = _directoryRepository.GetById(folder.ParentId);
                folder.Name = name;
                folder.Title = name;
                var targetPath = Path.Combine(parentFolder.Path, folder.Name);
                if (IsDirectoryExist(targetPath))
                {
                    _response.Success = false;
                    return _response;
                }
                if (!_storegeService.RenameDirectory(folder.Path, targetPath))
                {
                    _response.Success = false;
                    return _response;
                }
                folder.Path = Path.Combine(parentFolder.Path, folder.Name);
                _directoryRepository.Update(folder);
                UpdatePaths(folder);
                _directoryRepository.SaveChanges();
                _fileRepository.SaveChanges();
                _response.Success = true;
                return _response;
            }
            catch (Exception)
            {
                _response.Success = false;
                return _response;
            }
        }

        public Response DeleteDirectory(int id)
        {
            try
            {
                var folder = _directoryRepository.GetById(id);
                if (!_storegeService.DeleteDirectory(folder.Path))
                {
                    _response.Success = false;
                    return _response;
                }
                DeleteFolder(folder.Id);
                _directoryRepository.SaveChanges();
                _fileRepository.SaveChanges();
                _directoryRepository.Delete(folder);
                _fileRepository.DeleteAll(x => x.DirectoryId == folder.Id);
                _response.Success = true;
                return _response;
            }
            catch (Exception)
            {
                _response.Success = false;
                return _response;
            }
        }

        public Response GetDirectory(int id)
        {
            try
            {
                _response.Data = _directoryRepository.GetById(id, x => x.Directories);
                _response.Success = true;
                return _response;
            }
            catch (Exception)
            {
                _response.Success = false;
                return _response;
            }
        }

        public Response GetDirectory(string path)
        {
            try
            {
                _response.Data = _directoryRepository.SearchFor(x => x.Path == path).FirstOrDefault();
                _response.Success = true;
                return _response;
            }
            catch (Exception)
            {
                _response.Success = false;
                return _response;
            }
        }

        public Response GetSubDirectories(int id)
        {
            try
            {
                _response.Data = _directoryRepository.SearchFor(x => x.ParentId == id).ToList();
                _response.Success = true;
                return _response;
            }
            catch (Exception)
            {
                _response.Success = false;
                return _response;
            }
        }

        public bool IsDirectoryExist(string path)
        {
            return _directoryRepository.Table.Any(x => x.Path == path);
        }

        public Models.Directory.Directory GetMediaDirectory()
        {
            return _directoryRepository.Table.FirstOrDefault(x => x.ParentId == -1);
        }

        private void UpdatePaths(Models.Directory.Directory directory)
        {
            var folder = _directoryRepository.GetById(directory.Id);
            foreach (var file in _fileRepository.Get(x => x.DirectoryId == folder.Id))
            {
                file.Path = Path.Combine(folder.Path, file.Name);
                file.ModifyDateTime = DateTime.Now;
            }
            foreach (var fld in folder.Directories)
            {
                fld.Path = Path.Combine(folder.Path, fld.Name);
                foreach (var file in _fileRepository.Get(x => x.DirectoryId == fld.Id))
                {
                    file.Path = Path.Combine(fld.Path, file.Name);
                    file.ModifyDateTime = DateTime.Now;
                }
                UpdatePaths(fld);
            }
        }

        private void DeleteFolder(int id)
        {
            foreach (var subDirectory in _directoryRepository.Get(x => x.ParentId == id).ToList())
            {
                _directoryRepository.LazyDelete(subDirectory);
                _fileRepository.BulkDeleteAll(x => x.DirectoryId == subDirectory.Id);
                DeleteFolder(subDirectory.Id);
            }
        }
    }
}