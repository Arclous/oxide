﻿using Oxide.Core.Base.OxideDependency;
using Oxide.Core.Models.Base;

namespace Oxide.Media.Services.Directory
{
    /// <summary>
    ///     Directory service is the service which will handle directory actions both on disk and database
    /// </summary>
    public interface IDirectoryService : IOxideDependency
    {
        /// <summary>
        ///     Creates directory under parent directory
        /// </summary>
        /// <param name="parentId">Id of parent directory</param>
        /// <param name="name">Name of new directory</param>
        Response CreateDirectory(int parentId, string name);
        /// <summary>
        ///     Renames directory
        /// </summary>
        /// <param name="id">Id of the directory which will be renamed</param>
        /// <param name="name">New name of directory</param>
        Response RenameDirectory(int id, string name);
        /// <summary>
        ///     Deletes directory and its own sub directories
        /// </summary>
        /// <param name="id">Id of the directory which will be deleted</param>
        Response DeleteDirectory(int id);
        /// <summary>
        ///     Get directory
        /// </summary>
        /// <param name="id">Id of the directory which will be fetched from database</param>
        Response GetDirectory(int id);
        /// <summary>
        ///     Get directory
        /// </summary>
        /// <param name="path">Path of the wanted directory to fetch</param>
        Response GetDirectory(string path);
        /// <summary>
        ///     Get sub directories
        /// </summary>
        /// <param name="id">Id of the directory which will be fetched from database</param>
        Response GetSubDirectories(int id);
        /// <summary>
        ///     Checkes if the directory is exist according to path
        /// </summary>
        /// <param name="path">Path of the directory to check</param>
        bool IsDirectoryExist(string path);
        /// <summary>
        ///     Gets media directory
        /// </summary>
        Models.Directory.Directory GetMediaDirectory();
    }
}