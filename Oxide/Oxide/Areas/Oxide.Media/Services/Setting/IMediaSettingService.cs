﻿using Oxide.Core.Base.OxideDependency;
using Oxide.Media.ViewModels.Setting;

namespace Oxide.Media.Services.Setting
{
    public interface IMediaSettingService : IOxideDependency
    {
        void SaveSetting(string key, string value, string displayName = "");
        string LoadSetting(string key);
        SettingsPackage LoadAllSettings();
    }
}