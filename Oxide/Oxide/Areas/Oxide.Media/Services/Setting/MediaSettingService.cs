﻿using System.Collections.Generic;
using System.Linq;
using Oxide.Core.Data.Repository;
using Oxide.Core.Helpers;
using Oxide.Media.ViewModels.Setting;

namespace Oxide.Media.Services.Setting
{
    public class MediaSettingService : IMediaSettingService
    {
        private readonly IOxideRepository<Models.Setting.Setting> _settingRepository;

        public MediaSettingService(IOxideRepository<Models.Setting.Setting> settingRepository)
        {
            _settingRepository = settingRepository;
        }

        public void SaveSetting(string key, string value, string displayName = "")
        {
            var setting = _settingRepository.Get(x => x.Key.ToLower() == key.ToLower()).FirstOrDefault();
            if (setting != null)
            {
                setting.Value = value;
                _settingRepository.Update(setting);
            }
            else
            {
                setting = new Models.Setting.Setting {Key = key, Value = value, Name = displayName, Title = displayName};
                _settingRepository.Insert(setting);
            }
            OxideUtils.AddProperty(setting.Key, setting.Value);
        }

        public string LoadSetting(string key)
        {
            var firstOrDefault = _settingRepository.Get(x => x.Key.ToLower() == key.ToLower()).FirstOrDefault();
            return firstOrDefault?.Value;
        }

        public SettingsPackage LoadAllSettings()
        {
            var settingsPackage = new SettingsPackage {Settings = new List<Models.Setting.Setting>()};
            var settings = _settingRepository.GetAll();
            foreach (var setting in settings)
            {
                settingsPackage.Settings.Add(setting);
            }
            return settingsPackage;
        }
    }
}