﻿using System.Data.Entity;
using System.Linq;
using Oxide.Core.Base.OxideModelManager;
using Oxide.Core.Data.Context;
using Oxide.Core.Data.Repository;
using Oxide.Core.Helpers;
using Oxide.Media.Keys;
using Oxide.Media.Models.Directory;
using Oxide.Media.Models.Setting;

namespace Oxide.Media.Data
{
    public class ModelManager : IOxideModelManager
    {
        public void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
        public void Seed(OxideContext context)
        {
            IOxideRepository<Setting> oxideRepository = new OxideRepository<Setting>(context);
            IOxideRepository<Directory> oxideDirectoryRepository = new OxideRepository<Directory>(context);
            if (oxideRepository.Get(x => x.Key == MediaKeys.MediaFolder).FirstOrDefault() == null)
            {
                var setting = new Setting
                {
                    Key = MediaKeys.MediaFolder,
                    Name = "Media Location",
                    Title = "Media Location",
                    Value = @"\Media"
                };
                oxideRepository.Insert(setting);
                OxideUtils.AddProperty(setting.Key, setting.Value);
            }
            if (oxideRepository.Get(x => x.Key == MediaKeys.Provider).FirstOrDefault() == null)
            {
                var setting = new Setting
                {
                    Key = MediaKeys.Provider,
                    Name = "Provider",
                    Title = "Provider",
                    Value = "self"
                };
                oxideRepository.Insert(setting);
            }
            var mediaFolder = oxideRepository.GetAll().FirstOrDefault(x => x.Key == MediaKeys.MediaFolder).Value;
            if (oxideDirectoryRepository.GetAll().FirstOrDefault(x => x.Path == mediaFolder) == null)
            {
                var mainDirectory = new Directory();
                mainDirectory.Name = "Media";
                mainDirectory.Title = "Media";
                mainDirectory.Path = mediaFolder;
                mainDirectory.ParentId = -1;
                oxideDirectoryRepository.Insert(mainDirectory);
            }
        }
    }
}