﻿using System;
using Oxide.Core.Base.OxideDataBaseModel;
using Oxide.Media.Keys;

namespace Oxide.Media.Models.File
{
    public class File : OxideDatabaseModel, IOxideDataBaseModel
    {
        public int DirectoryId { get; set; }
        public string Path { get; set; }
        public FileType FileType { get; set; }
        public DateTime UploaDateTime { get; set; }
        public DateTime ModifyDateTime { get; set; }
        public long Size { get; set; }
        public string MimeType { get; set; }
    }
}