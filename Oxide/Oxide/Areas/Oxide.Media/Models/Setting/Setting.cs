﻿using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Media.Models.Setting
{
    public class Setting : OxideDatabaseModel, IOxideDataBaseModel
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}