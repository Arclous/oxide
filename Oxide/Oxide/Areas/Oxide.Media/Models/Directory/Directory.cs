﻿using System.Collections.Generic;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Media.Models.Directory
{
    public class Directory : OxideDatabaseModel, IOxideDataBaseModel
    {
        public int ParentId { get; set; }
        public virtual List<Directory> Directories { get; set; }
        public string Path { get; set; }
    }
}