﻿using System.Web.Mvc;
using System.Web.Routing;
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.Models.Menus;
using Oxide.Core.Services.AdminMenuService;
using Oxide.Core.Services.Interface.Registers.AdminMenuRegister;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Syntax;

namespace Oxide.Areas.Admin
{
    public class AdminMenu : IAdminMenuRegisterer
    {
        private readonly IProviderManager _providerManager = new ProviderManager();

        public void BuildMenus(RequestContext request, IAdminMenuService adminMenuService)
        {
            var urlHelper = new UrlHelper(request);
            var menuItem = new MenuItem();
            var oxideService = _providerManager.Provide<IOxideServices>();

            //*******************************************************************************************************************************************
            //Home Header
            var homeHeaderMenu = adminMenuService.AddHeaderMenu("Home", oxideService.GetText("Home", "Admin"),
                Syntax.CssClass.FaHome, 0, PermissionKeys.Empty);

            //DashBoard
            menuItem.Key = "Dashboard";
            menuItem.Title = oxideService.GetText("Dashboard", "Admin");
            menuItem.ControllerName = MVC.Admin.Dashboard.Name;
            menuItem.ActionName = MVC.Admin.Dashboard.ActionNames.Index;
            menuItem.Order = 0;
            menuItem.PermissionKey = PermissionKeys.Empty;
            menuItem.UrlAddress = urlHelper.Action(menuItem.ActionName, menuItem.ControllerName,
                new {area = Constants.DefaultArea});
            homeHeaderMenu.SubMenuItems.Add(menuItem);

            //*******************************************************************************************************************************************
            //Manage Pages
            var createHeaderMenu = adminMenuService.AddHeaderMenu("Pages", oxideService.GetText("Pages", "Admin"),
                Syntax.CssClass.FaEdit, 1, "");

            //Create Pages
            menuItem = new MenuItem
            {
                Title = oxideService.GetText("New Page", "Admin"),
                Key = "NewPage",
                ControllerName = MVC.Admin.Page.Name,
                ActionName = MVC.Admin.Page.ActionNames.NewPage,
                Order = 0,
                PermissionKey = PermissionKeys.CanCreatePage,
                Params = new {request = Syntax.Requests.Static}
            };
            menuItem.UrlAddress = urlHelper.Action(menuItem.ActionName, menuItem.ControllerName,
                new {area = Constants.DefaultArea});
            adminMenuService.AddMenuItem(createHeaderMenu, menuItem);

            //Display Pages
            menuItem = new MenuItem
            {
                Title = oxideService.GetText("Display Pages", "Admin"),
                Key = "DisplayPage",
                ControllerName = MVC.Admin.Page.Name,
                ActionName = MVC.Admin.Page.ActionNames.DisplayPages,
                Order = 0,
                PermissionKey = PermissionKeys.CanDisplayPage,
                Params = new {request = Syntax.Requests.Static}
            };
            menuItem.UrlAddress = urlHelper.Action(menuItem.ActionName, menuItem.ControllerName,
                new {area = Constants.DefaultArea});
            adminMenuService.AddMenuItem(createHeaderMenu, menuItem);

            //*******************************************************************************************************************************************
            //Manage Layouts
            var manageMainLayouts = adminMenuService.AddHeaderMenu("Layouts", oxideService.GetText("Layouts", "Admin"),
                Syntax.CssClass.FaLayout, 2, PermissionKeys.Empty);
            menuItem = new MenuItem
            {
                Title = oxideService.GetText("Master Page Layouts", "Admin"),
                Key = "Main Layouts",
                ControllerName = MVC.Admin.Layouts.Name,
                ActionName = MVC.Admin.Layouts.ActionNames.DisplayLayouts,
                Order = 0,
                PermissionKey = PermissionKeys.CanDisplayLayouts,
                Params = new {request = Syntax.Requests.Static}
            };
            menuItem.UrlAddress = urlHelper.Action(menuItem.ActionName, menuItem.ControllerName,
                new {area = Constants.DefaultArea});
            adminMenuService.AddMenuItem(manageMainLayouts, menuItem);

            //*******************************************************************************************************************************************
            //Manage Widgets
            var manageWidgets = adminMenuService.AddHeaderMenu("Widgets", oxideService.GetText("Widgets", "Admin"),
                Syntax.CssClass.FaPuzzle, 3, "");
            menuItem = new MenuItem
            {
                Title = oxideService.GetText("Widgets", "Admin"),
                Key = "Widgets",
                ControllerName = MVC.Admin.Widget.Name,
                ActionName = MVC.Admin.Widget.ActionNames.DisplayWidgets,
                Order = 0,
                PermissionKey = PermissionKeys.CanDisplayWidgets,
                Params = new {request = Syntax.Requests.Static}
            };
            menuItem.UrlAddress = urlHelper.Action(menuItem.ActionName, menuItem.ControllerName,
                new {area = Constants.DefaultArea});
            adminMenuService.AddMenuItem(manageWidgets, menuItem);
            menuItem = new MenuItem
            {
                Title = oxideService.GetText("Dashboard Widgets", "Admin"),
                Key = "DashboardWidgets",
                ControllerName = MVC.Admin.Dashboard.Name,
                ActionName = MVC.Admin.Dashboard.ActionNames.DisplayDashboardWidgets,
                Order = 1,
                PermissionKey = PermissionKeys.CanManageDashboardWidgets,
                Params = new {request = Syntax.Requests.Static}
            };
            menuItem.UrlAddress = urlHelper.Action(menuItem.ActionName, menuItem.ControllerName,
                new {area = Constants.DefaultArea});
            adminMenuService.AddMenuItem(manageWidgets, menuItem);

            //*******************************************************************************************************************************************
            //Manage Modules
            var manageModules = adminMenuService.AddHeaderMenu("Modules", oxideService.GetText("Modules", "Admin"),
                Syntax.CssClass.FaSiteMap, 4, "");
            menuItem = new MenuItem
            {
                Title = oxideService.GetText("Modules", "Admin"),
                Key = "Modules",
                ControllerName = MVC.Admin.Module.Name,
                ActionName = MVC.Admin.Module.ActionNames.DisplayModules,
                Order = 0,
                PermissionKey = PermissionKeys.CanDisplayModules,
                Params = new {request = Syntax.Requests.Static}
            };
            menuItem.UrlAddress = urlHelper.Action(menuItem.ActionName, menuItem.ControllerName,
                new {area = Constants.DefaultArea});
            adminMenuService.AddMenuItem(manageModules, menuItem);

            //*******************************************************************************************************************************************
            //Manage Themes
            var manageThemes = adminMenuService.AddHeaderMenu("Themes", oxideService.GetText("Themes", "Admin"),
                Syntax.CssClass.FaPaint, 5, "");
            menuItem = new MenuItem
            {
                Title = oxideService.GetText("Themes", "Admin"),
                Key = "Themes",
                ControllerName = MVC.Admin.Theme.Name,
                ActionName = MVC.Admin.Theme.ActionNames.DisplayThemes,
                Order = 0,
                PermissionKey = PermissionKeys.CanDisplayThemes,
                Params = new {request = Syntax.Requests.Static}
            };
            menuItem.UrlAddress = urlHelper.Action(menuItem.ActionName, menuItem.ControllerName,
                new {area = Constants.DefaultArea});
            adminMenuService.AddMenuItem(manageThemes, menuItem);

            //*******************************************************************************************************************************************
            //Admin Users
            var adminUsersHeader = adminMenuService.AddHeaderMenu("Users", oxideService.GetText("Users", "Admin"),
                Syntax.CssClass.FaUser, 98, "");
            menuItem = new MenuItem
            {
                Key = "New User",
                Title = oxideService.GetText("New User", "Admin"),
                Order = 0,
                PermissionKey = PermissionKeys.CanCreateUsers,
                ControllerName = MVC.Admin.AdminUser.Name,
                ActionName = MVC.Admin.AdminUser.ActionNames.NewUser
            };
            menuItem.UrlAddress = urlHelper.Action(menuItem.ActionName, menuItem.ControllerName,
                new {area = Constants.DefaultArea});
            adminMenuService.AddMenuItem(adminUsersHeader, menuItem);
            menuItem = new MenuItem
            {
                Key = "New Role",
                Title = oxideService.GetText("New Role", "Admin"),
                Order = 1,
                PermissionKey = PermissionKeys.CanCreateRoles,
                ControllerName = MVC.Admin.AdminUser.Name,
                ActionName = MVC.Admin.AdminUser.ActionNames.NewRole
            };
            menuItem.UrlAddress = urlHelper.Action(menuItem.ActionName, menuItem.ControllerName,
                new {area = Constants.DefaultArea});
            adminMenuService.AddMenuItem(adminUsersHeader, menuItem);
            menuItem = new MenuItem
            {
                Key = "Display Users",
                Title = oxideService.GetText("Display Users", "Admin"),
                Order = 20,
                PermissionKey = PermissionKeys.CanDisplayUsers,
                ControllerName = MVC.Admin.AdminUser.Name,
                ActionName = MVC.Admin.AdminUser.ActionNames.DisplayUsers
            };
            menuItem.UrlAddress = urlHelper.Action(menuItem.ActionName, menuItem.ControllerName,
                new {area = Constants.DefaultArea});
            adminMenuService.AddMenuItem(adminUsersHeader, menuItem);
            menuItem = new MenuItem
            {
                Key = "Display Roles",
                Title = oxideService.GetText("Display Roles", "Admin"),
                Order = 3,
                PermissionKey = PermissionKeys.CanDisplayRoles,
                ControllerName = MVC.Admin.AdminUser.Name,
                ActionName = MVC.Admin.AdminUser.ActionNames.DisplayRoles
            };
            menuItem.UrlAddress = urlHelper.Action(menuItem.ActionName, menuItem.ControllerName,
                new {area = Constants.DefaultArea});
            adminMenuService.AddMenuItem(adminUsersHeader, menuItem);

            //*******************************************************************************************************************************************
            //Site Settings
            var siteSettingsHeaderMenu = adminMenuService.AddHeaderMenu("Settings",
                oxideService.GetText("Settings", "Admin"), Syntax.CssClass.FaSettings, 99, "");
            menuItem = new MenuItem
            {
                Key = "General",
                Title = oxideService.GetText("General", "Admin"),
                Order = 0,
                PermissionKey = PermissionKeys.CanDisplaySettings,
                ControllerName = MVC.Admin.Settings.Name,
                ActionName = MVC.Admin.Settings.ActionNames.Index
            };
            menuItem.UrlAddress = urlHelper.Action(menuItem.ActionName, menuItem.ControllerName,
                new {area = Constants.DefaultArea});
            adminMenuService.AddMenuItem(siteSettingsHeaderMenu, menuItem);
            menuItem = new MenuItem
            {
                Key = "Languages",
                Title = oxideService.GetText("Languages", "Admin"),
                Order = 0,
                PermissionKey = PermissionKeys.CanDisplayLanguaeSettings,
                ControllerName = MVC.Admin.Languages.Name,
                ActionName = MVC.Admin.Languages.ActionNames.Index
            };
            menuItem.UrlAddress = urlHelper.Action(menuItem.ActionName, menuItem.ControllerName,
                new {area = Constants.DefaultArea});
            adminMenuService.AddMenuItem(siteSettingsHeaderMenu, menuItem);
            menuItem = new MenuItem
            {
                Key = "Email",
                Title = oxideService.GetText("Email", "Admin"),
                Order = 0,
                PermissionKey = PermissionKeys.CanDisplayEmailSettings,
                ControllerName = MVC.Admin.Settings.Name,
                ActionName = MVC.Admin.Settings.ActionNames.EmailSettings
            };
            menuItem.UrlAddress = urlHelper.Action(menuItem.ActionName, menuItem.ControllerName,
                new {area = Constants.DefaultArea});
            adminMenuService.AddMenuItem(siteSettingsHeaderMenu, menuItem);

            //*******************************************************************************************************************************************
            //Seo 
            var siteSeo = adminMenuService.AddHeaderMenu("Seo", oxideService.GetText("Seo", "Admin"),
                Syntax.CssClass.Seo, 100, "");
            menuItem = new MenuItem
            {
                Key = "Robot",
                Title = oxideService.GetText("Robot", "Admin"),
                Order = 0,
                PermissionKey = PermissionKeys.CanDisplayRobotSettings,
                ControllerName = MVC.Admin.Robot.Name,
                ActionName = MVC.Admin.Robot.ActionNames.Index
            };
            menuItem.UrlAddress = urlHelper.Action(menuItem.ActionName, menuItem.ControllerName,
                new {area = Constants.DefaultArea});
            adminMenuService.AddMenuItem(siteSeo, menuItem);

            //Redirection 
            menuItem = new MenuItem
            {
                Key = "Redirections",
                Title = oxideService.GetText("Redirections", "Admin"),
                Order = 1,
                PermissionKey = PermissionKeys.CanManageRedirectionSettings,
                ControllerName = MVC.Admin.Redirection.Name,
                ActionName = MVC.Admin.Redirection.ActionNames.Index
            };
            menuItem.UrlAddress = urlHelper.Action(menuItem.ActionName, menuItem.ControllerName,
                new { area = Constants.DefaultArea });
            adminMenuService.AddMenuItem(siteSeo, menuItem);

            //*******************************************************************************************************************************************
            //Email Templates
            var emailTemplateHeader = adminMenuService.AddHeaderMenu("Email Templates",
                oxideService.GetText("Email Templates", "Admin"), Syntax.CssClass.Templates, 100, "");
            menuItem = new MenuItem
            {
                Key = "Email Templates",
                Title = oxideService.GetText("Email Templates", "Admin"),
                Order = 0,
                PermissionKey = PermissionKeys.CanDisplayEmailTemplate,
                ControllerName = MVC.Admin.EmailTemplate.Name,
                ActionName = MVC.Admin.EmailTemplate.ActionNames.DisplayEmailTemplates
            };
            menuItem.UrlAddress = urlHelper.Action(menuItem.ActionName, menuItem.ControllerName,
                new {area = Constants.DefaultArea});
            adminMenuService.AddMenuItem(emailTemplateHeader, menuItem);
            menuItem = new MenuItem
            {
                Key = "New Email Templae",
                Title = oxideService.GetText("New Email Template", "Admin"),
                Order = 0,
                PermissionKey = PermissionKeys.CanCreateEmailTemplate,
                ControllerName = MVC.Admin.EmailTemplate.Name,
                ActionName = MVC.Admin.EmailTemplate.ActionNames.NewEmailTemplate
            };
            menuItem.UrlAddress = urlHelper.Action(menuItem.ActionName, menuItem.ControllerName,
                new {area = Constants.DefaultArea});
            adminMenuService.AddMenuItem(emailTemplateHeader, menuItem);

            //*******************************************************************************************************************************************
            //Cache Management
            var cacheManagementHeadermenu = adminMenuService.AddHeaderMenu("Manage Cache",
                oxideService.GetText("Manage Cache", "Admin"), Syntax.CssClass.FaCache, 101, "");
            menuItem = new MenuItem
            {
                Key = "Caches",
                Title = oxideService.GetText("Caches", "Admin"),
                Order = 0,
                PermissionKey = PermissionKeys.CanDisplayCacheItems,
                ControllerName = MVC.Admin.CacheItem.Name,
                ActionName = MVC.Admin.CacheItem.ActionNames.CacheItems
            };
            menuItem.UrlAddress = urlHelper.Action(menuItem.ActionName, menuItem.ControllerName,
                new {area = Constants.DefaultArea});
            adminMenuService.AddMenuItem(cacheManagementHeadermenu, menuItem);
            menuItem = new MenuItem
            {
                Key = "Cache Settings",
                Title = oxideService.GetText("Cache Settings", "Admin"),
                Order = 0,
                PermissionKey = PermissionKeys.CanDisplayCacheSettings,
                ControllerName = MVC.Admin.CacheItem.Name,
                ActionName = MVC.Admin.CacheItem.ActionNames.CacheItemSettings
            };
            menuItem.UrlAddress = urlHelper.Action(menuItem.ActionName, menuItem.ControllerName,
                new {area = Constants.DefaultArea});
            adminMenuService.AddMenuItem(cacheManagementHeadermenu, menuItem);

            //*******************************************************************************************************************************************
            //Scheduled Job Management
            var scheduledJobManagementHeader = adminMenuService.AddHeaderMenu("Scheduled Jobs",
                oxideService.GetText("Scheduled Jobs", "Admin"), Syntax.CssClass.Schedule, 102, "");
            menuItem = new MenuItem
            {
                Key = "Scheduled Jobs",
                Title = oxideService.GetText("Scheduled Jobs", "Admin"),
                Order = 0,
                PermissionKey = PermissionKeys.CanDisplayScheduleJobs,
                ControllerName = MVC.Admin.ScheduledJob.Name,
                ActionName = MVC.Admin.ScheduledJob.ActionNames.DisplayScheduledJobs
            };
            menuItem.UrlAddress = urlHelper.Action(menuItem.ActionName, menuItem.ControllerName,
                new {area = Constants.DefaultArea});
            adminMenuService.AddMenuItem(scheduledJobManagementHeader, menuItem);

            //*******************************************************************************************************************************************
            var logHeaderMenu = adminMenuService.AddHeaderMenu("Logs", oxideService.GetText("Logs", "Admin"),
                Syntax.CssClass.Code, 120, "");
            menuItem = new MenuItem
            {
                Key = "Scheduled Jobs",
                Title = oxideService.GetText("Logs", "Admin"),
                Order = 0,
                PermissionKey = PermissionKeys.CanDisplayLogs,
                ControllerName = MVC.Admin.Log.Name,
                ActionName = MVC.Admin.Log.ActionNames.DisplayLogs
            };
            menuItem.UrlAddress = urlHelper.Action(menuItem.ActionName, menuItem.ControllerName,
                new {area = Constants.DefaultArea});
            adminMenuService.AddMenuItem(logHeaderMenu, menuItem);

            //*******************************************************************************************************************************************
        }
    }
}