﻿using System.Collections.Generic;

namespace Oxide.Areas.Admin.ViewModels
{
    public class ContentViewModelHolder
    {
        public string LoadContentUrl { get; set; }
        public string ContentName { get; set; }
        public string ElementId { get; set; }
        public object SelectedRecords { get; set; }
        public bool? Multiple { get; set; }
        public string EditUrl { get; set; }
        public string DeleteUrl { get; set; }
        public List<string> Fields { get; set; }
        public List<string> FieldTitles { get; set; }
        public bool WithPictures { get; set; }
        public string PictureField { get; set; }
        public string Field1 { get; set; }
        public string Field2 { get; set; }
    }
}