﻿namespace Oxide.Areas.Admin.ViewModels
{
    public class EditWidgetViewModel
    {
        public object WidgetData { get; set; }
        public int PageId { get; set; }
    }
}