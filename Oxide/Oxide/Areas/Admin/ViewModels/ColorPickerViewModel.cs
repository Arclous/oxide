﻿namespace Oxide.Areas.Admin.ViewModels
{
    public class ColorPickerViewModel
    {
        public string Value { get; set; }
        public string ElementId { get; set; }
    }
}