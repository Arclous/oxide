﻿namespace Oxide.Areas.Admin.ViewModels
{
    public class EditMainLayoutWidgetViewModel : EditWidgetViewModel
    {
        public string LayoutName { get; set; }
    }
}