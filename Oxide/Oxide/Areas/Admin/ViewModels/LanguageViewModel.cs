﻿using System.Web.Mvc;

namespace Oxide.Areas.Admin.ViewModels
{
    public class LanguageViewModel
    {
        public SelectedIds[] SelectedLanguages { get; set; }
        public SelectList LanguageSelectList { get; set; }
    }
}