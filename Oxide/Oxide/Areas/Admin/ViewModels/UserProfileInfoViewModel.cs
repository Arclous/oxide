﻿namespace Oxide.Areas.Admin.ViewModels
{
    public class UserProfileInfoViewModel
    {
        public int Id { get; set; }
        public string UserRealName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string ProfileImage { get; set; }
        public string UserProfileImageUrl { get; set; }
    }
}