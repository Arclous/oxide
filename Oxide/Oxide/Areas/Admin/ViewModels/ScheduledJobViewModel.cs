﻿using System;
using Oxide.Core.Base.OxideViewModel;
using Oxide.Core.Syntax;

namespace Oxide.Areas.Admin.ViewModels
{
    public class ScheduledJobViewModel : OxideViewModel
    {
        public string Owner { get; set; }
        public bool Active { get; set; }
        public bool CanManage { get; set; }
        public DateTime LastStart { get; set; }
        public DateTime LastFinish { get; set; }
        public DateTime NextStart { get; set; }
        public string Key { get; set; }
        public string Group { get; set; }
        public string ManageUrl { get; set; }
        public Syntax.ScheduledJobRunPlatforms CanRunOn { get; set; }
    }
}