﻿using System.Collections.Generic;

namespace Oxide.Areas.Admin.ViewModels
{
    public class FilterContentViewModelHolder : ContentViewModelHolder
    {
        public string LoadDirectoriesUrl { get; set; }
        public string LoadSubDirectoriesUrl { get; set; }
        public ICollection<object> Records { get; set; }
        public int TotalRecords { get; set; }
        public int DisplayingRecords { get; set; }
        public int TotalPages { get; set; }
        public int CurrentPage { get; set; }
        public bool NextEnable { get; set; }
        public bool PreviousEnable { get; set; }
    }
}