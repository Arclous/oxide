﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Oxide.Areas.Admin.ViewModels
{
    public class ManageMainWidgetViewModel
    {
        public int MainLayoutId { get; set; }
        public List<SelectListItem> LanguageSelectList { get; set; }
        public int Culture { get; set; }
    }
}