﻿using System.Collections;
using System.Web.Mvc;
using Oxide.Areas.Admin.Models;
using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Areas.Admin.ViewModels
{
    public class SiteSettingsViewModel : OxideViewModel
    {
        public int DefaultTimeZoneSelected { get; set; }
        public int DefaultSiteCultureSelected { get; set; }
        public SelectList TimeZoneSelectList { get; set; }
        public IEnumerable LanguageSelectList { get; set; }
        public int DefaultSiteCultureId { get; set; }
        public virtual Language DefaultSiteCulture { get; set; }
        public int DefaultTimeZoneId { get; set; }
        public virtual TimeZoneItem DefaultTimeZone { get; set; }
    }
}