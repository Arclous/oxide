﻿using System.ComponentModel.DataAnnotations.Schema;
using Oxide.Areas.Admin.Models;
using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Areas.Admin.ViewModels
{
    public class SelectedLanguageViewModel : OxideViewModel
    {
        public int LanguageId { get; set; }
        public virtual Language Language { get; set; }
        public int[] SelectedLanguages { get; set; }
    }
    public class SelectedIds
    {
        [NotMapped]
        public string Id { get; set; }
    }
}