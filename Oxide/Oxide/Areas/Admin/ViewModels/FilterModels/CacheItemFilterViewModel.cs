﻿using Oxide.Core.ViewModels.CacheItemViewModels;
using Oxide.Core.ViewModels.FilterViewModels;

namespace Oxide.Areas.Admin.ViewModels.FilterModels
{
    public class CacheItemFilterViewModel : FilterModel<CacheItemViewModel>
    {
        public string ClearCacheUrl { get; set; }
        public string UpdateValueUrl { get; set; }
    }
}