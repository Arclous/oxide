﻿using Oxide.Core.ViewModels.FilterViewModels;
using Oxide.Core.ViewModels.PageViewModels;

namespace Oxide.Areas.Admin.ViewModels.FilterModels
{
    public class PageFilterViewModel : FilterModel<PageViewModel>
    {
        public string PublishUrl { get; set; }
        public string DeleteUrl { get; set; }
        public int TotalCreated { get; set; }
        public int TotalPublished { get; set; }
        public int TotalDraft { get; set; }
        public int TotalWaiting { get; set; }
    }
}