﻿namespace Oxide.Areas.Admin.ViewModels
{
    public class CkEditorViewModel
    {
        public string Content { get; set; }
        public string ElementId { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }
        public string Language { get; set; }
    }
}