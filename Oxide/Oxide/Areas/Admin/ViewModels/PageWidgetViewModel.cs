﻿using Oxide.Core.ViewModels.WidgeViewModels;

namespace Oxide.Areas.Admin.ViewModels
{
    public class PageWidgetViewModel : WidgetViewModel
    {
        public string ManageWidgetUrl { get; set; }
        public string EditWidgetUrl { get; set; }
        public string DeleteWidgetUrl { get; set; }
    }
}