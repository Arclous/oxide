﻿using System;
using System.Net;
using Oxide.Core.Managers.SettingsManager;
using Oxide.Core.Services.Implemantation;
using Oxide.Core.Syntax;
using Quartz;

namespace Oxide.Areas.Admin.Tasks
{
    public class Cacher : OxideScheduledBackgroundJob
    {
        public override string Name { get; } = "Cacher";

        public override string Title { get; } = "Cacher";

        public override bool CanForceToRun { get; } = false;

        public override bool CanManage { get; } = true;

        public override Syntax.BackgroundTaskActionType ActionType { get; } = Syntax.BackgroundTaskActionType.Daily;

        public override Action<DailyTimeIntervalScheduleBuilder> DailyAction { get; } =
            s => s.WithIntervalInMinutes(10).OnEveryDay();

        public override Action<CalendarIntervalScheduleBuilder> Action { get; }

        public override void Execute(IJobExecutionContext context)
        {
            RequestPage(SettingsManager.Domain());
        }

        public void RequestPage(string url, WebProxy proxy = null)
        {
            try
            {
                var webRequest = WebRequest.Create(url);
                webRequest.Method = Constants.Get;
                webRequest.GetResponse();
            }
            catch (Exception)
            {
                // ignored
            }
        }
    }
}