﻿using System.Web.Mvc;
using System.Web.Routing;
using Oxide.Core.Services.Interface.Registers.RouteRegister;

namespace Oxide.Areas.Admin.Routes
{
    public class RouteRegister : IRouteRegisterer
    {
        public void RegisterRoute(RouteCollection routes)
        {
            routes.MapRoute("SiteMap", "sitemap.xml",
                new {controller = MVC.Admin.SiteMap.Name, action = MVC.Admin.SiteMap.ActionNames.SitemapXml});
            routes.MapRoute("Robot", "robots.txt",
                new {controller = MVC.Admin.Robot.Name, action = MVC.Admin.Robot.ActionNames.RobotsText});
        }
    }
}