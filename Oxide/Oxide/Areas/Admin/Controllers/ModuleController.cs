﻿using System.Web.Mvc;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideController;
using Oxide.Core.Models;
using Oxide.Core.Models.Base;
using Oxide.Core.Services.ModuleService;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Syntax;

namespace Oxide.Areas.Admin.Controllers
{
    public partial class ModuleController : OxideController
    {
        private readonly IModuleService _moduleService;

        public ModuleController()
        {
        }

        public ModuleController(IOxideServices oxideServices, IModuleService moduleService) : base(oxideServices)
        {
            _moduleService = moduleService;
        }

        [OxideAdminPermission(Group = PermissionKeys.ModuleManagement, Permission = "Can Display Modules",
            Key = PermissionKeys.CanDisplayModules)]
        public virtual ActionResult DisplayModules()
        {
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Modules"),
                    Title = Constants.Empty
                });
        }

        #region Helpers

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.ModuleManagement, Permission = "Can Display Modules",
            Key = PermissionKeys.CanDisplayModules)]
        public virtual JsonResult ModuleList(string filter)
        {
            return Json(_moduleService.ModuleList(filter), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.ModuleManagement, Permission = "Can Manage Modules",
            Key = PermissionKeys.CanManageModules)]
        public virtual JsonResult ChangeStatus(string moduleName, bool status)
        {
            var result = new JsonWidgetActionResultModel {Active = _moduleService.ChangeStatus(moduleName, status)};
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}