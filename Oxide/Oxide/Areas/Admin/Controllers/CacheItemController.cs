﻿using System;
using System.Web.Mvc;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideController;
using Oxide.Core.Managers.CacheManager;
using Oxide.Core.Models;
using Oxide.Core.Models.Base;
using Oxide.Core.Services.CacheItemService;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Syntax;

namespace Oxide.Areas.Admin.Controllers
{
    public partial class CacheItemController : OxideController
    {
        private readonly ICacheItemService _cacheItemService;
        private readonly IOxideCacheManager _oxideCacheManager;

        public CacheItemController()
        {
        }

        public CacheItemController(IOxideServices oxideServices, ICacheItemService cacheItemService,
            IOxideCacheManager oxideCacheManager) : base(oxideServices)
        {
            _cacheItemService = cacheItemService;
            _oxideCacheManager = oxideCacheManager;
        }

        [OxideAdminPermission(Group = PermissionKeys.CacheManagement, Permission = "Can Display Cache Items",
            Key = PermissionKeys.CanDisplayCacheItems)]
        public virtual ActionResult CacheItems()
        {
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Cache Items"),
                    Title = OxideServices.GetText("Manage Cache Items")
                });
        }

        [OxideAdminPermission(Group = PermissionKeys.CacheManagement, Permission = "Can Display Cache Settings",
            Key = PermissionKeys.CanDisplayCacheSettings)]
        public virtual ActionResult CacheItemSettings()
        {
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Cache Settings"),
                    Title = OxideServices.GetText("Manage Cache Settings")
                });
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.CacheManagement, Permission = "Can Display Cache Items",
            Key = PermissionKeys.CanDisplayCacheItems)]
        public virtual JsonResult CacheItemList(int? pageSize, int? page, string filter)
        {
            if (pageSize == null) pageSize = 10;
            if (page == null) page = 0;
            var cacheItemList = _cacheItemService.GetCacheItemsWithFilter((int) pageSize, (int) page, filter);
            return Json(cacheItemList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.CacheManagement, Permission = "Can Clear Cache Items",
            Key = PermissionKeys.CanClearCacheItems)]
        public virtual JsonResult ClearCacheItem(string cacheKey)
        {
            _oxideCacheManager.ClearCache(cacheKey);
            var result = new JsonPageActionResultModel {Success = true};
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.CacheManagement, Permission = "Can Clear Cache Items",
            Key = PermissionKeys.CanClearCacheItems)]
        public virtual JsonResult ClearAllCacheItems()
        {
            _oxideCacheManager.ClearAllCache();
            var result = new JsonPageActionResultModel {Success = true};
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.CacheManagement, Permission = "Can Update Cache Items Settings",
            Key = PermissionKeys.CanUpdateCacheItemSettings)]
        public virtual JsonResult UpdateCacheSetting(string cacheKey, string value)
        {
            var result = new JsonPageActionResultModel();
            var cacheItem = _cacheItemService.GetCacheItemByName(cacheKey);
            if (cacheItem != null)
            {
                cacheItem.Value = Convert.ToInt32(value);
                _cacheItemService.UpdateCacheItem(cacheItem);
                result.Success = true;
            }
            else
                result.Success = false;
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}