﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideController;
using Oxide.Core.Extesions;
using Oxide.Core.Managers.CacheManager;
using Oxide.Core.Models.Base;
using Oxide.Core.Models.Redirection;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Services.Redirection;
using Oxide.Core.Services.Robot;
using Oxide.Core.Syntax;
using Oxide.Core.ViewModels.Redirection;

namespace Oxide.Areas.Admin.Controllers
{
    public partial class RedirectionController : OxideController
    {
        private readonly IOxideCacheManager _cacheManager;
        private readonly IRedirectionService _redirectionService;

        public RedirectionController()
        {
        }


        public RedirectionController(IOxideServices oxideServices, IRedirectionService redirectionService, IOxideCacheManager cacheManager)
            : base(oxideServices)
        {
            _cacheManager = cacheManager;
            _redirectionService = redirectionService;
        }

        [OxideAdminPermission(Group = PermissionKeys.RedirectionManagement, Permission = "Can Display Redirections",
         Key = PermissionKeys.CanDisplayRedirectionSettings)]
        public virtual ActionResult Index()
        {
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Redirections"),
                    Title = Constants.Empty
                });
        }


        [OxideAdmin]
        [OxideAdminPermission(Group = PermissionKeys.RedirectionManagement, Permission = "Can Manage Redirections", Key = PermissionKeys.CanManageRedirectionSettings)]
        public virtual ActionResult NewRedireciton()
        {
            var redirectionViewModel = new RedirectionViewModel { ReturUrl = HttpContext.Request.RawUrl };
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Redirections"),
                    Title = OxideServices.GetText("Create New Redirection"),
                    Parameters = redirectionViewModel
                });
        }

        [OxideAdmin]
        [OxideAdminPermission(Group = PermissionKeys.RedirectionManagement, Permission = "Can Manage Redirections", Key = PermissionKeys.CanManageRedirectionSettings)]
        public virtual ActionResult CreateRedirection(string title, string name, string url, string newUrl, int code, string pageUrl,bool active)
        {
            var redirection = new Redirection { Name = name, Title = title,Url = url,NewUrl = newUrl,StatusCode = (Syntax.RedirectionStatusCode)code,Active = active};
            var item = _redirectionService.CreateRedirection(redirection);
            var oxideModel = new OxideModel { RequestType = Syntax.Requests.Static };
            if (item > 0)
                oxideModel.SuccessMessage = OxideServices.GetText("Redirection saved successfully");
            else
                oxideModel.ErrorMessage =
                    OxideServices.GetText(
                        "Problem while saving redirection, operation is not succesfull, please check the logs");
            TempData[Syntax.Keywords.RedirectionItem] = oxideModel;
            return Redirect(pageUrl);
        }

        [OxideAdminPermission(Group = PermissionKeys.RedirectionManagement, Permission = "Can Manage Redirections",
           Key = PermissionKeys.CanManageRedirectionSettings)]
        public virtual ActionResult EditRedirection(int id, string returnUrl = null)
        {
            var redirectionViewModel = GetRedirectionViewModel(id, returnUrl);
                return
                    View(new OxideModel
                    {
                        RequestType = Syntax.Requests.Static,
                        Header = OxideServices.GetText("Edit Redirection"),
                        Title = Constants.Empty,
                        Parameters = redirectionViewModel
                    });
            

        }

        [OxideAdminPermission(Group = PermissionKeys.RedirectionManagement, Permission = "Can Manage Redirections",
        Key = PermissionKeys.CanManageRedirectionSettings)]
        public virtual ActionResult UpdateRedirection(RedirectionViewModel viewModel)
        {
            var model = _redirectionService.GetRedirection(viewModel.Id);
            viewModel.StatusCode = (Syntax.RedirectionStatusCode)viewModel.StatusCodeSelected;
            viewModel.CopyPropertiesTo(model, new[] {"Id"});
            _redirectionService.UpdateRedirection(model);
            var oxideModel = new OxideModel
            {
                RequestType = Syntax.Requests.Static,
                SuccessMessage = OxideServices.GetText("Redirection updated successfully")
            };

            TempData[Syntax.Keywords.RedirectionItem] = oxideModel;

            return RedirectToAction("Index");
        }


        [HttpGet]
        [OxideAdminPermission(Group = "Articles", Permission = "Can Delete Article", Key = "Can_Delete_Article")]
        public virtual JsonResult DeleteArticle(int id)
        {
             _redirectionService.DeleteRedirection(id);
            return Json("", JsonRequestBehavior.AllowGet);
        }


        private RedirectionViewModel GetRedirectionViewModel(int id, string returnUrl)
        {
            var viewModel = new RedirectionViewModel();
            var model=_redirectionService.GetRedirection(id);

            model.CopyPropertiesTo(viewModel);

            viewModel.ReturUrl = returnUrl;
            viewModel.StatusCodeSelected = (int)model.StatusCode;
            if (model.Active) viewModel.IsActive = Constants.Checked;
            return viewModel;
            

        }
    }
}