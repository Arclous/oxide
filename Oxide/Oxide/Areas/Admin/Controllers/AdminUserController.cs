﻿using System.Web.Mvc;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideController;
using Oxide.Core.Extesions;
using Oxide.Core.Models;
using Oxide.Core.Models.Base;
using Oxide.Core.Services.AdminPermissionService;
using Oxide.Core.Services.AdminRoleService;
using Oxide.Core.Services.AdminUserService;
using Oxide.Core.Services.LoginService;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Services.OxideUiService;
using Oxide.Core.Syntax;
using Oxide.Core.ViewModels.AdminRoleViewModels;
using Oxide.Core.ViewModels.AdminUserViewModels;

namespace Oxide.Areas.Admin.Controllers
{
    public partial class AdminUserController : OxideController
    {
        private readonly IAdminPermissionService _adminPermissionService;
        private readonly IAdminRoleService _adminRoleService;
        private readonly IAdminUserService _adminUserService;
        private readonly ILoginService _loginService;
        private readonly IOxideUiService _oxideUiService;

        public AdminUserController()
        {
        }

        public AdminUserController(IOxideServices oxideServices, IAdminUserService adminUserService,
            IAdminRoleService adminRoleService, IAdminPermissionService adminPermissionService,
            ILoginService loginService, IOxideUiService oxideUiService) : base(oxideServices)
        {
            _adminUserService = adminUserService;
            _adminRoleService = adminRoleService;
            _adminPermissionService = adminPermissionService;
            _loginService = loginService;
            _oxideUiService = oxideUiService;
        }

        #region Users

        [OxideAdminPermission(Group = PermissionKeys.UserManagement, Permission = "Can Display Users",
            Key = PermissionKeys.CanDisplayUsers)]
        public virtual ActionResult DisplayUsers()
        {
            var resultModel = TempData[Constants.NewAdminUser] as OxideModel;
            var oxideModel = resultModel ??
                             new OxideModel
                             {
                                 RequestType = Syntax.Requests.Static,
                                 Header = OxideServices.GetText("Admin Users"),
                                 Title = Constants.Empty
                             };
            return View(oxideModel);
        }

        [OxideAdminPermission(Group = PermissionKeys.UserManagement, Permission = "Can Create Users",
            Key = PermissionKeys.CanCreateUsers)]
        public virtual ActionResult NewUser()
        {
            var resultModel = TempData[Constants.NewAdminUser] as OxideModel;
            var oxideModel = resultModel ??
                             new OxideModel
                             {
                                 RequestType = Syntax.Requests.Static,
                                 Header = OxideServices.GetText("New User"),
                                 Title = Constants.Empty
                             };
            return View(oxideModel);
        }

        [OxideAdminPermission(Group = PermissionKeys.UserManagement, Permission = "Can Update Users",
            Key = PermissionKeys.CanUpdateUsers)]
        public virtual ActionResult EditUser(int adminUserId)
        {
            var resultModel = TempData[Constants.NewAdminUser] as OxideModel;
            OxideModel oxideModel;
            if (resultModel == null)
            {
                oxideModel = new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Edit User"),
                    Title = Constants.Empty
                };
                var userInfo = new AdminUserViewModel();
                _adminUserService.GetUserWithLanguage(adminUserId).CopyPropertiesTo(userInfo);
                userInfo.UserImageIdValue = userInfo.UserImageId.ToString();
                userInfo.UserLanguageId = userInfo.UserLanguage.Id.ToString();
                oxideModel.Parameters = userInfo;
            }
            else
                oxideModel = resultModel;
            return View(oxideModel);
        }

        [OxideAdminPermission(Group = PermissionKeys.UserManagement, Permission = "Can Manage Roles",
            Key = PermissionKeys.CanManageRoles)]
        public virtual ActionResult ManageRoles(int adminUserId)
        {
            var resultModel = TempData[Constants.NewAdminUser] as OxideModel;
            var oxideModel = resultModel ??
                             new OxideModel
                             {
                                 RequestType = Syntax.Requests.Static,
                                 Header = OxideServices.GetText("Manage User Roles"),
                                 Title = Constants.Empty,
                                 Parameters = adminUserId
                             };
            return View(oxideModel);
        }

        [ValidateAntiForgeryToken]
        [OxideAdminPermission(Group = PermissionKeys.UserManagement, Permission = "Can Create Users",
            Key = PermissionKeys.CanCreateUsers)]
        public virtual ActionResult SaveUser(AdminUserViewModel model)
        {
            var oxideModel = new OxideModel {RequestType = Syntax.Requests.Static};

            //Check password
            if (model.Password != model.ConfirmPassword)
            {
                oxideModel.Header = OxideServices.GetText("New User");
                oxideModel.ErrorMessage = OxideServices.GetText("Password does not match");
                oxideModel.Parameters = model;
                TempData[Constants.NewAdminUser] = oxideModel;
                return RedirectToAction(MVC.Admin.AdminUser.ActionNames.NewUser, MVC.Admin.AdminUser.Name);
            }
            if (_adminUserService.IsAdminUserExist(model.UserName))
            {
                oxideModel.Header = OxideServices.GetText("New User");
                oxideModel.ErrorMessage = OxideServices.GetText("User already exist");
                oxideModel.Parameters = model;
                TempData[Constants.NewAdminUser] = oxideModel;
                return RedirectToAction(MVC.Admin.AdminUser.ActionNames.NewUser, MVC.Admin.AdminUser.Name);
            }
            if (model.UserLanguageId == null)
            {
                oxideModel.Header = OxideServices.GetText("New User");
                oxideModel.ErrorMessage = OxideServices.GetText("Please select language for the user");
                oxideModel.Parameters = model;
                TempData[Constants.NewAdminUser] = oxideModel;
                return RedirectToAction(MVC.Admin.AdminUser.ActionNames.NewUser, MVC.Admin.AdminUser.Name);
            }
            model.Id = _adminUserService.CreateUser(model);
            oxideModel.SuccessMessage = OxideServices.GetText("User created successful");
            TempData[Constants.NewAdminUser] = oxideModel;
            return RedirectToAction(MVC.Admin.AdminUser.ActionNames.DisplayUsers, MVC.Admin.AdminUser.Name);
        }

        [OxideAdminPermission(Group = PermissionKeys.UserManagement, Permission = "Can Update Users",
            Key = PermissionKeys.CanUpdateUsers)]
        public virtual ActionResult UpdateUser(int id, string name, string email, string userImage, string languageId,
            string password, string confirmPassword)
        {
            var oxideModel = new OxideModel {RequestType = Syntax.Requests.Static};
            var userInfo = new AdminUserViewModel
            {
                Id = id,
                Name = name,
                Email = email,
                UserImageIdValue = userImage,
                UserLanguageId = languageId
            };

            //Check password
            if (password != string.Empty)
            {
                if (password != confirmPassword)
                {
                    oxideModel.Header = OxideServices.GetText("User Update");
                    oxideModel.ErrorMessage = OxideServices.GetText("Password does not match");
                    oxideModel.Parameters = oxideModel;
                    TempData[Constants.NewAdminUser] = oxideModel;
                    return RedirectToAction(MVC.Admin.AdminUser.ActionNames.NewUser, MVC.Admin.AdminUser.Name);
                }
                userInfo.Password = password;
            }

            _adminUserService.UpdateUser(userInfo);
            if (_loginService.GetUserInfo().Id == id)
            {
                _loginService.SetUserAdminUserLanguage(userInfo.Id);
                _loginService.SetUserAdminUserProfileImage(_oxideUiService.GetImageContentUrl(userImage));
            }
            oxideModel.SuccessMessage = OxideServices.GetText("User updated successful");
            TempData[Constants.NewAdminUser] = oxideModel;
            return RedirectToAction(MVC.Admin.AdminUser.ActionNames.DisplayUsers, MVC.Admin.AdminUser.Name);
        }

        #endregion

        #region Roles

        [OxideAdminPermission(Group = PermissionKeys.UserManagement, Permission = "Can Display Roles",
            Key = PermissionKeys.CanDisplayRoles)]
        public virtual ActionResult DisplayRoles()
        {
            var resultModel = TempData[Constants.AdminRole] as OxideModel;
            var oxideModel = resultModel ??
                             new OxideModel
                             {
                                 RequestType = Syntax.Requests.Static,
                                 Header = OxideServices.GetText("User Roles"),
                                 Title = Constants.Empty
                             };
            return View(oxideModel);
        }

        [OxideAdminPermission(Group = PermissionKeys.UserManagement, Permission = "Can Create Roles",
            Key = PermissionKeys.CanCreateRoles)]
        public virtual ActionResult NewRole()
        {
            var resultModel = TempData[Constants.AdminRole] as OxideModel;
            var oxideModel = resultModel ??
                             new OxideModel
                             {
                                 RequestType = Syntax.Requests.Static,
                                 Header = OxideServices.GetText("New User Role"),
                                 Title = Constants.Empty
                             };
            return View(oxideModel);
        }

        [OxideAdminPermission(Group = PermissionKeys.UserManagement, Permission = "Can Update Roles",
            Key = PermissionKeys.CanUpdateRoles)]
        public virtual ActionResult EditRole(int roleId)
        {
            var resultModel = TempData[Constants.AdminRole] as OxideModel;
            OxideModel oxideModel;
            if (resultModel == null)
            {
                oxideModel = new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Edit Role"),
                    Title = Constants.Empty
                };
                var roleInfo = new AdminRoleViewModel();
                _adminRoleService.GetRole(roleId).CopyPropertiesTo(roleInfo);
                oxideModel.Parameters = roleInfo;
            }
            else
                oxideModel = resultModel;
            return View(oxideModel);
        }

        [OxideAdminPermission(Group = PermissionKeys.UserManagement, Permission = "Can Manage Permissions",
            Key = PermissionKeys.CanManagePermissions)]
        public virtual ActionResult ManagePermissions(int roleId)
        {
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Manage Permissions"),
                    Title = Constants.Empty,
                    Parameters = roleId
                });
        }

        [OxideAdminPermission(Group = PermissionKeys.UserManagement, Permission = "Can Create Roles",
            Key = PermissionKeys.CanCreateRoles)]
        public virtual ActionResult SaveRole(AdminRoleViewModel model)
        {
            var oxideModel = new OxideModel {RequestType = Syntax.Requests.Static};
            if (_adminRoleService.IsRoleExist(model.Name))
            {
                oxideModel.Header = OxideServices.GetText("New Role");
                oxideModel.ErrorMessage = OxideServices.GetText("Role already created");
                oxideModel.Parameters = model;
                TempData[Constants.AdminRole] = oxideModel;
                return RedirectToAction(MVC.Admin.AdminUser.ActionNames.NewRole, MVC.Admin.AdminUser.Name);
            }
            _adminRoleService.CreateRole(model);
            oxideModel.SuccessMessage = OxideServices.GetText("Role created successful");
            TempData[Constants.AdminRole] = oxideModel;
            return RedirectToAction(MVC.Admin.AdminUser.ActionNames.DisplayRoles, MVC.Admin.AdminUser.Name);
        }

        [OxideAdminPermission(Group = PermissionKeys.UserManagement, Permission = "Can Update Roles",
            Key = PermissionKeys.CanUpdateRoles)]
        public virtual ActionResult UpdateRole(int id, string name, string title)
        {
            var oxideModel = new OxideModel {RequestType = Syntax.Requests.Static};
            var userRole = new AdminRoleViewModel {Id = id, Name = name, Title = title};
            _adminRoleService.UpdateRole(userRole);
            oxideModel.SuccessMessage = OxideServices.GetText("User role updated successful");
            TempData[Constants.AdminRole] = oxideModel;
            return RedirectToAction(MVC.Admin.AdminUser.ActionNames.DisplayRoles, MVC.Admin.AdminUser.Name);
        }

        #endregion

        #region Helpers

        #region Users

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.UserManagement, Permission = "Can Display Users",
            Key = PermissionKeys.CanDipslayUsers)]
        public virtual JsonResult AdminUserList(int? pageSize, int? page, string filter)
        {
            if (pageSize == null) pageSize = 10;
            if (page == null) page = 0;
            var adminUserList = _adminUserService.GetAdminUsersWithFilter((int) pageSize, (int) page, filter);
            return Json(adminUserList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.UserManagement, Permission = "Can Delete Users",
            Key = PermissionKeys.CanDeleteUsers)]
        public virtual JsonResult DeleteAdminUser(int adminUserId)
        {
            _adminUserService.DeleteUser(adminUserId);
            var result = new JsonPageActionResultModel {Success = true};
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Roles

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.UserManagement, Permission = "Can Display Roles",
            Key = PermissionKeys.CanDisplayRoles)]
        public virtual JsonResult AdminRoleList(int? pageSize, int? page, string filter)
        {
            if (pageSize == null) pageSize = 10;
            if (page == null) page = 0;
            var adminRoleList = _adminRoleService.GetAdminRolesWithFilter((int) pageSize, (int) page, filter);
            return Json(adminRoleList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.UserManagement, Permission = "Can Delete Roles",
            Key = PermissionKeys.CanDeleteRoles)]
        public virtual JsonResult DeleteAdminRole(int adminRoleId)
        {
            _adminRoleService.DeleteRole(adminRoleId);
            var result = new JsonPageActionResultModel {Success = true};
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.UserManagement, Permission = "Can Display Roles",
            Key = PermissionKeys.CanDisplayRoles)]
        public virtual JsonResult RoleList(int userId)
        {
            var roleList = _adminRoleService.GetAllAdminRolesOrganized(userId);
            return Json(roleList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.UserManagement, Permission = "Can Set Roles",
            Key = PermissionKeys.CanSetRoles)]
        public virtual JsonResult SetRole(int userId, int roleId, bool status)
        {
            _adminRoleService.SetRole(userId, roleId, status);
            return Json("", JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Permissions

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.UserManagement, Permission = "Can Display Permissions",
            Key = PermissionKeys.CanDisplayPermissions)]
        public virtual JsonResult PermisionList(int roleId)
        {
            var permissionList = _adminPermissionService.GetAllAdminPermissionsOrganized(roleId);
            return Json(permissionList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.UserManagement, Permission = "Can Set Permissions",
            Key = PermissionKeys.CanSetPermissions)]
        public virtual JsonResult SetPermission(int roleId, int permissionId, bool status)
        {
            _adminRoleService.SetPermission(roleId, permissionId, status);
            return Json("", JsonRequestBehavior.AllowGet);
        }

        #endregion

        #endregion
    }
}