﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideController;
using Oxide.Core.Managers.WidgetManager;
using Oxide.Core.Models;
using Oxide.Core.Models.Base;
using Oxide.Core.Services.DashboardService;
using Oxide.Core.Services.LoginService;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Syntax;

namespace Oxide.Areas.Admin.Controllers
{
    public partial class DashboardController : OxideController
    {
        private readonly IDashboardService _dashboardService;
        private readonly ILoginService _loginService;
        private readonly IWidgetManager _widgetManager;

        public DashboardController()
        {
        }

        public DashboardController(IOxideServices oxideServices, IDashboardService dashboardService,
            ILoginService loginService, IWidgetManager widgetManager) : base(oxideServices)
        {
            _dashboardService = dashboardService;
            _loginService = loginService;
            _widgetManager = widgetManager;
        }

        public virtual ActionResult Index()
        {
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Oxide Dashboard"),
                    Title = OxideServices.GetText("Dashboard")
                });
        }

        [OxideAdminPermission(Group = PermissionKeys.DashaboardWidgetManagement,
            Permission = "Can Display Dashboard Widgets", Key = PermissionKeys.CanDisplayDashboardWidgets)]
        public virtual ActionResult DisplayDashboardWidgets()
        {
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Dashboard Widgets"),
                    Title = Constants.Empty
                });
        }

        [OxideAdminPermission(Group = PermissionKeys.DashaboardWidgetManagement,
            Permission = "Can Manage Dashboard Widgets", Key = PermissionKeys.CanManageDashboardWidgets)]
        public virtual ActionResult ManageDashboardWidgets(int widgetId, string widgetKey)
        {
            var userId = _loginService.GetUserInfo().Id;
            var widgetReturnResult = _widgetManager.RenderDashboardWidgetManagement(widgetKey, widgetId, userId,
                ControllerContext);
            return View(MVC.Admin.Dashboard.Views.ManageDashboardWidgets, widgetReturnResult);
        }

        #region Helpers

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.DashaboardWidgetManagement,
            Permission = "Can Manage Dashboard Widgets", Key = PermissionKeys.CanManageDashboardWidgets)]
        public virtual JsonResult GetActiveAdminWidgets(string filter)
        {
            return Json(_dashboardService.DashboardWidgetList(filter).Records.Where(x => x.Active),
                JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.DashaboardWidgetManagement,
            Permission = "Can Manage Dashboard Widgets", Key = PermissionKeys.CanManageDashboardWidgets)]
        public virtual JsonResult ChangeStatus(string widgetKey, bool status)
        {
            var result = new JsonWidgetActionResultModel {Active = _dashboardService.ChangeStatus(widgetKey, status)};
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.CanManageDashboardWidgets,
            Permission = "Can Manage Dashboard Widgets", Key = PermissionKeys.CanManageDashboardWidgets)]
        public virtual JsonResult WidgetList(string filter)
        {
            return Json(_dashboardService.DashboardWidgetList(filter), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.CanManageDashboardWidgets,
            Permission = "Can Manage Dashboard Widgets", Key = PermissionKeys.CanManageDashboardWidgets)]
        public virtual JsonResult DashboardWidgetList()
        {
            var userId = _loginService.GetUserInfo().Id;
            return Json(_dashboardService.GetDashboardWidgetsForUser(userId), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.CanManageDashboardWidgets,
            Permission = "Can Manage Dashboard Widgets", Key = PermissionKeys.CanManageDashboardWidgets)]
        public virtual JsonResult DashboardWidgetLisForPopup(string filter)
        {
            return Json(_dashboardService.GetDashboardWidgetsForUser(filter), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.CanManageDashboardWidgets,
            Permission = "Can Manage Dashboard Widgets", Key = PermissionKeys.CanManageDashboardWidgets)]
        public virtual JsonResult AddDashboardWidget(string widgetKey)
        {
            var userId = _loginService.GetUserInfo().Id;
            var widgetId = _dashboardService.AttachAdminWidget(userId, widgetKey);
            var urlHelper = new UrlHelper(System.Web.HttpContext.Current.Request.RequestContext);
            var manageUrl = urlHelper.Action(MVC.Admin.Dashboard.ActionNames.ManageDashboardWidgets,
                MVC.Admin.Dashboard.Name, new {area = Constants.DefaultArea, widgetId, widgetKey});
            var result = new Dictionary<string, string> {{"Id", widgetId.ToString()}, {"ManageUrl", manageUrl}};
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.CanManageDashboardWidgets,
            Permission = "Can Manage Dashboard Widgets", Key = PermissionKeys.CanManageDashboardWidgets)]
        public virtual JsonResult DashboardWidgetRun(string widgetKey, int widgetId)
        {
            var userId = _loginService.GetUserInfo().Id;
            return Json(_widgetManager.RenderDashboardWidget(widgetKey, widgetId, userId).ToHtmlString(),
                JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.CanManageDashboardWidgets,
            Permission = "Can Manage Dashboard Widgets", Key = PermissionKeys.CanManageDashboardWidgets)]
        public virtual JsonResult DeleteDashboardWidget(int id)
        {
            _dashboardService.DetachAdminWidget(id);
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.CanManageDashboardWidgets,
            Permission = "Can Manage Dashboard Widgets", Key = PermissionKeys.CanManageDashboardWidgets)]
        public virtual JsonResult OrderDashboardWidget(int widgetId, string orderList)
        {
            var userId = _loginService.GetUserInfo().Id;
            _dashboardService.UpdateOrderValue(userId, widgetId, orderList);
            return Json("", JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}