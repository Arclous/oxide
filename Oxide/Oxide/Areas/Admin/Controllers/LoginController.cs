﻿using System.Net;
using System.Web.Mvc;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideController;
using Oxide.Core.Managers.SettingsManager;
using Oxide.Core.Services.LoginService;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Syntax;
using Oxide.Core.ViewModels.LoginViewModels;

namespace Oxide.Areas.Admin.Controllers
{
    public partial class LoginController : OxideController
    {
        private readonly ILoginService _loginService;

        public LoginController()
        {
        }

        public LoginController(IOxideServices oxideServices, ILoginService loginService) : base(oxideServices)
        {
            _loginService = loginService;
        }

        [AdminLogin]
        [OutputCache(NoStore = true, Duration = 0)]
        public virtual ActionResult LoginPage(string returnUrl)
        {
            //Check domain role
            if (!SettingsManager.IsAdmin())
                return RedirectPermanent("/");
            var resultModel = TempData[Constants.NewAdminUser] as LoginViewModel;
            if (resultModel != null)
            {
                resultModel.ReturnUrl = returnUrl;
                var loginModel = resultModel;
                return View(MVC.Admin.Login.Views.LoginPage, loginModel);
            }
            var loginViewModel = new LoginViewModel {ReturnUrl = returnUrl};
            return View(MVC.Admin.Login.Views.LoginPage, loginViewModel);
        }

        public virtual ActionResult AccessDenied()
        {
            HttpContext.Response.StatusCode = (int) HttpStatusCode.Forbidden;
            return View(MVC.Admin.Login.Views.AccessDenied);
        }

        [ValidateAntiForgeryToken]
        public virtual ActionResult Login(string userName, string password, string returnUrl)
        {
            var loginResult = Syntax.AdminLoginResult.NotSuccess;
            var loginModel = new LoginViewModel {UserName = userName};
            if ((userName != string.Empty) && (password != string.Empty))
                loginResult = _loginService.LoginUser(userName, password);
            switch (loginResult)
            {
                case Syntax.AdminLoginResult.Success:
                    if (returnUrl == string.Empty)
                        return RedirectToAction(MVC.Admin.Admin.ActionNames.Index, MVC.Admin.Admin.Name,
                            new {area = Constants.DefaultArea});
                    return Redirect(returnUrl);
                case Syntax.AdminLoginResult.UserNotFound:
                    loginModel.Message = OxideServices.GetText("User not found");
                    TempData[Constants.NewAdminUser] = loginModel;
                    return RedirectToAction(MVC.Admin.Login.ActionNames.LoginPage, MVC.Admin.Login.Name,
                        new {area = Constants.DefaultArea});
                default:
                    loginModel.Message =
                        OxideServices.GetText("Incorrect username or password,please check your login information");
                    TempData[Constants.NewAdminUser] = loginModel;
                    return RedirectToAction(MVC.Admin.Login.ActionNames.LoginPage, MVC.Admin.Login.Name,
                        new {area = Constants.DefaultArea});
            }
        }

        public virtual ActionResult LogOut()
        {
            _loginService.SignOut();
            return Redirect(@"\");
        }
    }
}