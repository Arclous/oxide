﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Web.Mvc;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideController;
using Oxide.Core.Extesions;
using Oxide.Core.Helpers;
using Oxide.Core.Managers.CacheManager;
using Oxide.Core.Managers.CultureManager;
using Oxide.Core.Managers.PageManager;
using Oxide.Core.Managers.ThemeManager;
using Oxide.Core.Models.Base;
using Oxide.Core.Models.Log;
using Oxide.Core.Services.LogService;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Services.PageService;
using Oxide.Core.Services.Redirection;
using Oxide.Core.Syntax;
using Oxide.Core.ViewModels.GeneralViewModels;

namespace Oxide.Areas.Admin.Controllers
{
    public partial class FrontController : OxideController
    {
        private readonly ICultureManager _cultureManager;
        private readonly ILogService _logWriter;
        private readonly IPageManager _pageManager;
        private readonly IPageService _pageService;
        private readonly IRedirectionService _redirectionService;
        private readonly IThemeManager _themeManager;
        private readonly IOxideCacheManager _cacheManager;
        public FrontController()
        {
        }

        public FrontController(IOxideServices oxideServices, IPageService pageService, IThemeManager themeManager,
            IPageManager pageManager, ICultureManager cultureManager, ILogService logWriter,
            IRedirectionService redirectionService, IOxideCacheManager cacheManager) : base(oxideServices)
        {
            _pageService = pageService;
            _themeManager = themeManager;
            _pageManager = pageManager;
            _cultureManager = cultureManager;
            _logWriter = logWriter;
            _redirectionService = redirectionService;
            _cacheManager = cacheManager;
        }

        [Front]
        public virtual ActionResult Index()
        {
            var model = new OxideModel
            {
                RequestType = Syntax.Requests.Static,
                Header = OxideServices.GetText("Error"),
                Title = OxideServices.GetText("Page Error"),
                Parameters = ""
            };

            try
            {
                var currentCuture = _cultureManager.GetFrontCurrentCulture();
                var requestedUrl = System.Web.HttpContext.Current.Request.Url.LocalPath;

                //Check if there is any redirection settings
                string cacheName = $@"cacheRedirections_{requestedUrl}";
                string cacheNameTitle = $@"Redirection cache for {requestedUrl}";
                string cultureSettingsCacheName = "frontCultureSet";

                var url = requestedUrl;
                var redirectionSetting = _cacheManager.GetOrSet(cacheName, cacheNameTitle,
                 Constants.Redirections, () => _redirectionService.GetRedirection(url));

                if (redirectionSetting != null)
                {
                    Response.StatusCode = (int) redirectionSetting.StatusCode;

                    if (redirectionSetting.StatusCode == Syntax.RedirectionStatusCode.MovedPermanently)
                        Response.RedirectPermanent(redirectionSetting.NewUrl);

                    if (redirectionSetting.StatusCode == Syntax.RedirectionStatusCode.Found)
                        Response.Redirect(redirectionSetting.NewUrl);
                }

                var result = requestedUrl.IsUrlFinishWithSlash();

                if (result && !requestedUrl.IsBaseUrl())
                    requestedUrl = requestedUrl.ClearUrlFromSlash();

                var page = _pageService.GetPageWithLanguage(requestedUrl);

                //If the page requested with addtional slash, redirect it with status code 301 to the original page
                if (result && page != null && requestedUrl.Length > 1)
                    Response.StatusCode = 301;

                var orjPage = page;

                if (page != null)
                {
                    //Check if the client browser culture is same the with requested page culture
                    if (orjPage.Language.Code3 != currentCuture.ThreeLetterISOLanguageName)
                    {
                        //Check if there is page with the same culture which client browser has
                        orjPage = _pageService.GetPage(requestedUrl, currentCuture.ThreeLetterISOLanguageName);
                        if (orjPage != null)
                        {
                            if (orjPage.Published && orjPage.VisibleForSite)
                                Response.Redirect(orjPage.Url);
                            else
                            {
                                Response.StatusCode = 404;
                                return View(_themeManager.GetErrorPage(ErrorTypes.Error404), model);
                            }
                        }
                        else
                            //Write page culture into client cookie, so all other operations related with the culture can be done correctly because setting culture as default
                            _cultureManager.SetFrontCulture(new CultureInfo(page.Language.Code), Response);

                    }
                    if (page.Published && page.VisibleForSite)
                        return View(_themeManager.GetMainLayout(page.MainLayoutId),
                            _pageManager.GetRenderPageModel(page));
                    Response.StatusCode = 404;
                    return View(_themeManager.GetErrorPage(ErrorTypes.Error404), model);
                }
                Response.StatusCode = 404;
                return View(_themeManager.GetErrorPage(ErrorTypes.Error404), model);
            }
            catch (Exception ex)
            {
                var log = new Log
                {
                    DateTimeInfo = DateTime.Now,
                    LogType = Syntax.LogTypes.Error,
                    Title = ex.Message,
                    ModuleName = OxideUtils.GetCallerModuleName(new StackFrame(1)),
                    Description = ex.StackTrace
                };
                if (ex.InnerException != null)
                    log.InnerException = ex.InnerException.Message;
                _logWriter.CreateLog(log);
            }
            Response.StatusCode = 404;
            return View(_themeManager.GetErrorPage(ErrorTypes.Error404), model);
        }
    }
}