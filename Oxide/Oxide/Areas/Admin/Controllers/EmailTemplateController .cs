﻿using System.Web.Mvc;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideController;
using Oxide.Core.Models.Base;
using Oxide.Core.Models.EmailTemplate;
using Oxide.Core.Services.EmailTemplateService;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Syntax;

namespace Oxide.Areas.Admin.Controllers
{
    public partial class EmailTemplateController : OxideController
    {
        private readonly IEmailTemplateService _emailTemplateService;

        public EmailTemplateController()
        {
        }

        public EmailTemplateController(IOxideServices oxideServices, IEmailTemplateService emailTemplateService)
            : base(oxideServices)
        {
            _emailTemplateService = emailTemplateService;
        }

        [OxideAdminPermission(Group = PermissionKeys.EmailTemplateManagement, Permission = "Can Display Email Templates",
            Key = PermissionKeys.CanDisplayEmailTemplate)]
        public virtual ActionResult DisplayEmailTemplates()
        {
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Email Templates"),
                    Title = Constants.Empty
                });
        }

        [OxideAdminPermission(Group = PermissionKeys.EmailTemplateManagement, Permission = "Can Create Email Templates",
            Key = PermissionKeys.CanCreateEmailTemplate)]
        public virtual ActionResult NewEmailTemplate()
        {
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Email Template"),
                    Title = Constants.Empty
                });
        }

        [OxideAdminPermission(Group = PermissionKeys.EmailTemplateManagement, Permission = "Can Edit Email Templates",
            Key = PermissionKeys.CanEditEmailTemplate)]
        public virtual ActionResult EditEmailTemplate(int templateId)
        {
            var emailTemplate = _emailTemplateService.GetEmailTemplate(templateId);
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Email Template"),
                    Title = Constants.Empty,
                    Parameters = emailTemplate
                });
        }

        [OxideAdminPermission(Group = PermissionKeys.EmailTemplateManagement, Permission = "Can Create Email Templates",
            Key = PermissionKeys.CanCreateEmailTemplate)]
        [ValidateInput(false)]
        public virtual ActionResult SaveEmailTemplate(string name, string title, string template)
        {
            var emailTemplate = new EmailTemplate {Name = name, Title = title, Template = template};
            _emailTemplateService.CreateEmailTemplate(emailTemplate);
            return RedirectToAction(MVC.Admin.EmailTemplate.ActionNames.DisplayEmailTemplates);
        }

        [OxideAdminPermission(Group = PermissionKeys.EmailTemplateManagement, Permission = "Can Edit Email Templates",
            Key = PermissionKeys.CanEditEmailTemplate)]
        [ValidateInput(false)]
        public virtual ActionResult UpdateEmailTemplate(string name, string title, string template, int id)
        {
            var emailTemplate = _emailTemplateService.GetEmailTemplate(id);
            if (emailTemplate != null)
            {
                emailTemplate.Name = name;
                emailTemplate.Title = title;
                emailTemplate.Template = template;
                _emailTemplateService.UpdateEmailTemplate(emailTemplate);
            }
            return RedirectToAction(MVC.Admin.EmailTemplate.ActionNames.DisplayEmailTemplates);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.EmailTemplateManagement, Permission = "Can Display Email Templates",
            Key = PermissionKeys.CanDisplayEmailTemplate)]
        public virtual JsonResult EmailTemplateList(int? pageSize, int? page, string filter)
        {
            if (pageSize == null) pageSize = 10;
            if (page == null) page = 0;
            var emailTemplateWithFilter = _emailTemplateService.GetEmailTemplateWithFilter((int) pageSize, (int) page,
                filter);
            return Json(emailTemplateWithFilter, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.LogManagement, Permission = "Can Delete Email Template",
            Key = PermissionKeys.CanDeleteEmailTemplate)]
        public virtual JsonResult DeleteEmailTemplate(int id)
        {
            _emailTemplateService.DeleteEmailTemplate(id);
            return Json(new {success = true}, JsonRequestBehavior.AllowGet);
        }
    }
}