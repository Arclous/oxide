﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Castle.Components.DictionaryAdapter;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideController;
using Oxide.Core.Extesions;
using Oxide.Core.Models.Base;
using Oxide.Core.Models.Messages;
using Oxide.Core.Services.AdminRoleService;
using Oxide.Core.Services.AdminUserService;
using Oxide.Core.Services.LoginService;
using Oxide.Core.Services.MessageService;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Syntax;
using Oxide.Core.ViewModels.ModuleViewModels;

namespace Oxide.Areas.Admin.Controllers
{
    public partial class MessagesController : OxideController
    {
        private readonly IAdminRoleService _adminRoleService;
        private readonly IAdminUserService _adminUserService;
        private readonly ILoginService _loginService;
        private readonly IMessageService _messageService;

        public MessagesController()
        {
        }

        public MessagesController(IOxideServices oxideServices, IMessageService messageService,
            ILoginService loginService, IAdminUserService adminUserService, IAdminRoleService adminRoleService)
            : base(oxideServices)
        {
            _messageService = messageService;
            _loginService = loginService;
            _adminUserService = adminUserService;
            _adminRoleService = adminRoleService;
        }

        [OxideAdminPermission(Group = PermissionKeys.MessageManagement, Permission = "Can Display Messages",
            Key = PermissionKeys.CanDisplayMessages)]
        public virtual ActionResult Inbox(int? id)
        {
            if (id != null)
                return
                    View(new OxideModel
                    {
                        RequestType = Syntax.Requests.Static,
                        Header = OxideServices.GetText("Messages"),
                        Title = Constants.Empty,
                        Parameters = id
                    });
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Messages"),
                    Title = Constants.Empty
                });
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.MessageManagement, Permission = "Can Display Messages",
            Key = PermissionKeys.CanDisplayMessages)]
        public virtual JsonResult MessageList(int? pageSize, int? page, string filter)
        {
            var loginedUser = _loginService.GetUserInfo();
            if (pageSize == null) pageSize = 10;
            if (page == null) page = 0;
            var logsList = _messageService.GetMessageWithFilter((int) pageSize, (int) page, filter, loginedUser.Id);
            return Json(logsList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.MessageManagement, Permission = "Can Display Messages",
            Key = PermissionKeys.CanDisplayMessages)]
        public virtual JsonResult QuickMessageList()
        {
            var loginedUser = _loginService.GetUserInfo();
            var logsList = _messageService.GetQuickMessageWithFilter(5, 0, loginedUser.Id);
            return Json(logsList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.MessageManagement, Permission = "Can Display Messages",
            Key = PermissionKeys.CanDisplayMessages)]
        public virtual JsonResult DeleteMessage(int id)
        {
            _messageService.DeleteMessage(id);
            return Json(new {success = true}, JsonRequestBehavior.AllowGet);
        }

        [OxideAdminPermission(Group = PermissionKeys.MessageManagement, Permission = "Can Send Messages",
            Key = PermissionKeys.CanSendMessages)]
        public virtual ActionResult NewMessage()
        {
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("New Message"),
                    Title = Constants.Empty
                });
        }

        [OxideAdminPermission(Group = PermissionKeys.MessageManagement, Permission = "Can Send Messages",
            Key = PermissionKeys.CanSendMessages)]
        public virtual ActionResult ReplyMessage(int messageId)
        {
            var model = new OxideModel
            {
                RequestType = Syntax.Requests.Static,
                Header = OxideServices.GetText("New Message"),
                Title = Constants.Empty
            };
            var messageModel = new MessageAdminViewModel();
            var message = _messageService.GetMessage(messageId);
            if (message == null) return View(model);
            messageModel.Title = message.Title;
            messageModel.Reciver = message.Sender.Id.ToString();
            messageModel.Message = message.MessageBody;
            model.Parameters = messageModel;
            return View(model);
        }

        [OxideAdminPermission(Group = PermissionKeys.MessageManagement, Permission = "Can Send Messages",
            Key = PermissionKeys.CanSendMessages)]
        public virtual ActionResult ForwardMessage(int forwardMessageId)
        {
            var model = new OxideModel
            {
                RequestType = Syntax.Requests.Static,
                Header = OxideServices.GetText("New Message"),
                Title = Constants.Empty
            };
            var messageModel = new MessageAdminViewModel();
            var message = _messageService.GetMessage(forwardMessageId);
            if (message == null) return View(model);
            messageModel.Title = message.Title;
            messageModel.Message = message.MessageBody;
            model.Parameters = messageModel;
            return View(model);
        }

        [OxideAdminPermission(Group = PermissionKeys.MessageManagement, Permission = "Can Send Messages",
            Key = PermissionKeys.CanSendMessages)]
        [HttpPost, ValidateInput(false)]
        public virtual ActionResult SendMessage(string title, string to, string template, string toRole)
        {
            var senderUser = _loginService.GetUserInfo();
            if (senderUser == null)
                return RedirectToAction("Inbox");
            List<int> recivers;
            if (to != "")
                recivers = to.GetIntList();
            else
            {
                var roles = toRole.GetIntList();
                recivers = new EditableList<int>();
                foreach (var v in roles)
                {
                    var users = _adminRoleService.GetAllUsers(v);
                    foreach (var user in users)
                    {
                        recivers.Add(user);
                    }
                }
            }
            var userSender = _adminUserService.GetUser(senderUser.Id);
            foreach (var message in
                recivers.Select(
                    reciver =>
                        new Message
                        {
                            IsMessageRead = false,
                            MessageDateTime = DateTime.Now,
                            Title = title,
                            MessageBody = template,
                            Sender = userSender,
                            Reciever = _adminUserService.GetUser(reciver)
                        }))
            {
                _messageService.SendMessage(message);
            }
            return RedirectToAction("Inbox");
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.MessageManagement, Permission = "Can Display Messages",
            Key = PermissionKeys.CanDisplayMessages)]
        public virtual JsonResult GetMessageContent(int id)
        {
            var message = _messageService.GetMessage(id);
            _messageService.MarkAsReaded(id);
            return Json(message.MessageBody, JsonRequestBehavior.AllowGet);
        }
    }
}