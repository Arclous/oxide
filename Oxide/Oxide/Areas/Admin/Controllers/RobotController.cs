﻿using System.Text;
using System.Web.Mvc;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideController;
using Oxide.Core.Managers.CacheManager;
using Oxide.Core.Models.Base;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Services.Robot;
using Oxide.Core.Syntax;

namespace Oxide.Areas.Admin.Controllers
{
    public partial class RobotController : OxideController
    {
        private readonly IOxideCacheManager _cacheManager;
        private readonly IRobotService _robotService;

        public RobotController()
        {
        }

        public RobotController(IOxideServices oxideServices, IRobotService robotService, IOxideCacheManager cacheManager)
            : base(oxideServices)
        {
            _robotService = robotService;
            _cacheManager = cacheManager;
        }

        [OxideAdminPermission(Group = PermissionKeys.RobotManagement, Permission = "Can Display Robot.txt",
            Key = PermissionKeys.CanDisplayRobotSettings)]
        public virtual ActionResult Index()
        {
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Robot.txt"),
                    Title = Constants.Empty,
                    Parameters = _robotService.GetRobotValue()
                });
        }

        [OxideAdminPermission(Group = PermissionKeys.RobotManagement, Permission = "Can Update Robot.txt",
            Key = PermissionKeys.CanManageRobotSettings)]
        public virtual ActionResult SaveRobotValue(string robot)
        {
            _robotService.SaveRobotValue(robot);
            return RedirectToAction(MVC.Admin.Robot.ActionNames.Index);
        }

        public virtual ContentResult RobotsText()
        {
            var robotValue = _cacheManager.GetOrSet("Robot", "Robot", "Seo", () => _robotService.GetRobotValue(), 1534);
            return Content(robotValue, "text/plain", Encoding.UTF8);
        }
    }
}