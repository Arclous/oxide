﻿using System.Linq;
using System.Web.Mvc;
using Oxide.Areas.Admin.ViewModels;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideController;
using Oxide.Core.Models.Base;
using Oxide.Core.Services.LanguageService;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Syntax;

namespace Oxide.Areas.Admin.Controllers
{
    public partial class LanguagesController : OxideController
    {
        private readonly ILanguageService _languageService;

        public LanguagesController()
        {
        }

        public LanguagesController(IOxideServices oxideServices, ILanguageService languageService) : base(oxideServices)
        {
            _languageService = languageService;
        }

        [OxideAdminPermission(Group = PermissionKeys.LanguageManagement, Permission = "Can Display Languages Settings",
            Key = PermissionKeys.CanDisplayLanguaeSettings)]
        public virtual ActionResult Index()
        {
            var list = _languageService.GetAllActiveLanguages();
            var languageViewModel = new LanguageViewModel
            {
                SelectedLanguages = list.Select(v => new SelectedIds {Id = v.Id.ToString()}).ToArray(),
                LanguageSelectList =
                    new SelectList(
                        _languageService.GetAllLanguages()
                                        .Select(x => new SelectListItem {Text = x.Title, Value = x.Id.ToString()}),
                        "Value", "Text")
            };
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Language Settings"),
                    Title = Constants.Empty,
                    Parameters = languageViewModel
                });
        }

        [OxideAdminPermission(Group = PermissionKeys.LanguageManagement, Permission = "Can Update Languages Settings",
            Key = PermissionKeys.CanUpdateLanguaeSettings)]
        public virtual ActionResult SaveSelectedLanguages(SelectedLanguageViewModel model)
        {
            _languageService.MakeAllLanguagesDeactive();
            foreach (var v in model.SelectedLanguages)
            {
                _languageService.ActivateLanguage(v);
            }
            return RedirectToAction(MVC.Admin.Languages.ActionNames.Index);
        }
    }
}