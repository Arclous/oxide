﻿using System.Web.Mvc;
using Oxide.Core.Base.OxideActionFilters;
using Oxide.Core.Base.OxideController;
using Oxide.Core.Models.Base;
using Oxide.Core.Services.AdminMenuService;
using Oxide.Core.Services.LoginService;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Syntax;

namespace Oxide.Areas.Admin.Controllers
{
    [OxideAuthorize]
    public partial class AdminController : OxideController
    {
        private readonly IAdminMenuService _adminMenuService;
        private readonly ILoginService _loginService;

        public AdminController()
        {
        }

        public AdminController(IOxideServices oxideServices, ILoginService loginService,
            IAdminMenuService adminMenuService) : base(oxideServices)
        {
            _loginService = loginService;
            _adminMenuService = adminMenuService;
        }

        public virtual ActionResult Index()
        {
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Oxide Admin Page"),
                    Title = OxideServices.GetText("Welcome to Oxide Admin Page")
                });
        }

        [ChildActionOnly]
        public virtual ActionResult RenderHeader()
        {
            return PartialView(MVC.Admin.Admin.Views.Header);
        }

        [ChildActionOnly]
        public virtual ActionResult RenderLeftMenu()
        {
            return PartialView(MVC.Admin.Admin.Views.LeftSide, _adminMenuService.GetAdminMenu());
        }

        [ChildActionOnly]
        public virtual ActionResult RenderFooter()
        {
            return PartialView(MVC.Admin.Admin.Views.Footer);
        }

        [ChildActionOnly]
        public virtual ActionResult RenderUserMenu()
        {
            var userInfo = _loginService.GetUserInfo();
            return PartialView(MVC.Admin.Admin.Views.User, userInfo);
        }

        [ChildActionOnly]
        public virtual ActionResult RenderProfile()
        {
            var userInfo = _loginService.GetUserInfo();
            return PartialView(MVC.Admin.Admin.Views.Profile, userInfo);
        }

        [ChildActionOnly]
        public virtual ActionResult RenderMenuFooter()
        {
            return PartialView(MVC.Admin.Admin.Views.MenuFooter);
        }
    }
}