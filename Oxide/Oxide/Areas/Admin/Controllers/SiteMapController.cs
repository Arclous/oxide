﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Xml.Linq;
using Oxide.Areas.Admin.Models;
using Oxide.Core.Base.OxideController;
using Oxide.Core.Helpers;
using Oxide.Core.Managers.CacheManager;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Services.PageService;

namespace Oxide.Areas.Admin.Controllers
{
    public partial class SiteMapController : OxideController
    {
        private readonly IOxideCacheManager _cacheManager;
        private readonly IPageService _pageService;

        public SiteMapController()
        {
        }

        public SiteMapController(IOxideServices oxideServices, IPageService pageService, IOxideCacheManager cacheManager)
            : base(oxideServices)
        {
            _pageService = pageService;
            _cacheManager = cacheManager;
        }

        [Route("sitemap.xml")]
        public virtual ActionResult SitemapXml()
        {
            var sitemapNodes = GetSitemapNodes(Url);
            var xml = GetSitemap(sitemapNodes);
            return Content(xml, "text/xml, application/xml", Encoding.UTF8);
        }

        public string GetSitemap(IEnumerable<SitemapNode> sitemapNodes)
        {
            XNamespace xmlns = "http://www.sitemaps.org/schemas/sitemap/0.9";
            var root = new XElement(xmlns + "urlset");
            foreach (var urlElement in
                sitemapNodes.Select(
                    sitemapNode =>
                        new XElement(xmlns + "url", new XElement(xmlns + "loc", Uri.EscapeUriString(sitemapNode.Url)),
                            sitemapNode.LastModified == null
                                ? null
                                : new XElement(xmlns + "lastmod",
                                    sitemapNode.LastModified.Value.ToLocalTime().ToString("yyyy-MM-ddTHH:mm:sszzz")),
                            sitemapNode.Frequency == null
                                ? null
                                : new XElement(xmlns + "changefreq",
                                    sitemapNode.Frequency.Value.ToString().ToLowerInvariant()),
                            sitemapNode.Priority == null
                                ? null
                                : new XElement(xmlns + "priority",
                                    sitemapNode.Priority.Value.ToString("F1", CultureInfo.InvariantCulture)))))
            {
                root.Add(urlElement);
            }
            var document = new XDocument(root);
            return document.ToString();
        }

        public IReadOnlyCollection<SitemapNode> GetSitemapNodes(UrlHelper urlHelper)
        {
            var pages = _cacheManager.GetOrSet("SiteMap", "SiteMap", "Seo",
                () => _pageService.GetAllPages(x => x.VisibleForSeo), 1534);
            return
                pages.Select(
                    page =>
                        new SitemapNode
                        {
                            Url = OxideUtils.GetFullUrl(page.Url),
                            Frequency = page.SiteMapFrequncy,
                            Priority = page.Priority,
                            LastModified = page.LastUpdate
                        }).ToList();
        }
    }
}