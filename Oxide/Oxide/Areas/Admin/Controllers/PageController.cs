﻿using System.Linq;
using System.Web.Mvc;
using Oxide.Areas.Admin.ViewModels.FilterModels;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideController;
using Oxide.Core.Extesions;
using Oxide.Core.Helpers;
using Oxide.Core.Models;
using Oxide.Core.Models.Base;
using Oxide.Core.Models.Pages;
using Oxide.Core.Services.LanguageService;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Services.PageService;
using Oxide.Core.Services.WidgetService;
using Oxide.Core.Syntax;
using Oxide.Core.ViewModels.LayoutViewModels;
using Oxide.Core.ViewModels.PageViewModels;

namespace Oxide.Areas.Admin.Controllers
{
    public partial class PageController : OxideController
    {
        private readonly ILanguageService _languageService;
        private readonly IPageService _pageservice;
        private readonly IWidgetService _widgetService;

        public PageController()
        {
        }

        public PageController(IOxideServices oxideServices, IPageService pageService, IWidgetService widgetService,
            ILanguageService languageService) : base(oxideServices)
        {
            _pageservice = pageService;
            _widgetService = widgetService;
            _languageService = languageService;
        }

        [OxideAdminPermission(Group = PermissionKeys.PageManagement, Permission = "Can Create Pages",
            Key = PermissionKeys.CanCreatePage)]
        public virtual ActionResult NewPage()
        {
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Create New Page"),
                    Title = Constants.Empty,
                    Parameters = GetNewPageViewModel()
                });
        }

        [OxideAdminPermission(Group = PermissionKeys.PageManagement, Permission = "Can Display Pages",
            Key = PermissionKeys.CanDisplayPage)]
        public virtual ActionResult DisplayPages()
        {
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Pages"),
                    Title = ""
                });
        }

        [OxideAdminPermission(Group = PermissionKeys.PageManagement, Permission = "Can Edit Pages",
            Key = PermissionKeys.CanEditPages)]
        public virtual ActionResult EditPage(int pageId, string returnUrl = null)
        {
            var pageModel = GetEditPageViewModel(pageId, returnUrl);

            if(pageModel.PageType != Syntax.PageTypes.Template)
            {
                return
                    View(new OxideModel
                    {
                        RequestType = Syntax.Requests.Static,
                        Header = OxideServices.GetText("Edit Page"),
                        Title = Constants.Empty,
                        Parameters = pageModel
                    });
            }
            else
            {
                return RedirectToAction("EditTemplatePage", new {pageId, returnUrl });
            }
        }

        [OxideAdminPermission(Group = PermissionKeys.PageManagement, Permission = "Can Edit Template Pages",
    Key = PermissionKeys.CanEditPages)]
        public virtual ActionResult EditTemplatePage(int pageId, string returnUrl = null)
        {
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Edit Template"),
                    Title = Constants.Empty,
                    Parameters = GetEditTemplatePageViewModel(pageId)
                });
        }


        [OxideAdminPermission(Group = PermissionKeys.PageManagement, Permission = "Can Manage Page Widgets",
            Key = PermissionKeys.CanManagePageWidgets)]
        public virtual ActionResult ManageWidgets(int pageId)
        {
            var model = _pageservice.GetPageSimply(pageId);

            //Widgets should be filled inside to each elements
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Manage Widgets"),
                    Title = model.Title,
                    Parameters = pageId
                });
        }

        #region Helpers

        [OxideAdminPermission(Group = PermissionKeys.PageManagement, Permission = "Can Create Pages",
            Key = PermissionKeys.CanCreatePages)]
        public virtual ActionResult CreatePage(PageViewModel model)
        {
            //Set page type as standart
            model.PageType= Syntax.PageTypes.Standard;
            _pageservice.CreatePage(model);
            return RedirectToAction("DisplayPages");
        }

        [OxideAdminPermission(Group = PermissionKeys.PageManagement, Permission = "Can Update Pages",
            Key = PermissionKeys.CanUpdatePages)]
        public virtual ActionResult UpdatePage(PageViewModel model)
        {
            model.Language = _languageService.GetLanguage(model.LanguageSelected);
            _pageservice.UpdatePage(model);

            if (model.ReturUrl != null)
                return Redirect(model.ReturUrl);

            return RedirectToAction("DisplayPages");
        }

        [OxideAdminPermission(Group = PermissionKeys.PageManagement, Permission = "Can Update Template Pages",
        Key = PermissionKeys.CanUpdatePages)]
        public virtual ActionResult UpdateTemplatePage(TemplatePageViewModel model)
        {
            model.Language = _languageService.GetLanguage(model.LanguageSelected);
            _pageservice.UpdateTemplatePage(model);

            return Redirect(model.ReturUrl ?? model.ActivePageUrl);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.PageManagement, Permission = "Can Display Pages",
            Key = PermissionKeys.CanDisplayPages)]
        public virtual JsonResult PageList(int? pageSize, int? page, string filter, int filterType)
        {
            if (pageSize == null) pageSize = 10;
            if (page == null) page = 0;
            PageFilterViewModel pageList;
            switch (filterType)
            {
                case 0:
                    pageList = _pageservice.GetPagesWithFilterWithLanguage((int) pageSize, (int) page, filter);
                    break;
                case 1:
                    pageList = _pageservice.GetPagesWithFilterWithLanguage((int) pageSize, (int) page, filter,
                        x => x.Published);
                    break;
                case 2:
                    pageList = _pageservice.GetPagesWithFilterWithLanguage((int) pageSize, (int) page, filter,
                        x => !x.Published);
                    break;
                case 3:
                    pageList = _pageservice.GetPagesWithFilterWithLanguage((int) pageSize, (int) page, filter,
                        x => x.PublishLater);
                    break;
                default:
                    pageList = _pageservice.GetPagesWithFilterWithLanguage((int) pageSize, (int) page, filter);
                    break;
            }
            return Json(pageList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.PageManagement, Permission = "Can Change Publish Status",
            Key = PermissionKeys.CanChangePublishStatus)]
        public virtual JsonResult ChangePublishStatus(int pageId, bool status)
        {
            if (status)
                _pageservice.Publish(pageId);
            else
                _pageservice.UnPublish(pageId);
            var result = new JsonPageActionResultModel {Published = status, Success = true};
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.PageManagement, Permission = "Can Delete Page",
            Key = PermissionKeys.CanDeletePage)]
        public virtual JsonResult DeletePage(int pageId)
        {
            _pageservice.DeletePage(pageId);
            var result = new JsonPageActionResultModel {Success = true};
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.PageManagement, Permission = "Can Display Pages",
            Key = PermissionKeys.CanDisplayPage)]
        public virtual JsonResult LanguageList()
        {
            return
                Json(
                    _languageService.GetAllActiveLanguages()
                                    .Select(v => new {Name = v.Native, Id = v.Id.ToString()})
                                    .ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.PageManagement, Permission = "Can Display Pages",
            Key = PermissionKeys.CanDisplayPage)]
        public virtual JsonResult CheckLanguageAvailability(int pageId, int laguageId)
        {
            var page = _pageservice.GetPageSimply(pageId);
            return Json(page.LanguageId != laguageId, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.PageManagement, Permission = "Can Display Pages",
            Key = PermissionKeys.CanDisplayPage)]
        public virtual JsonResult CheckNameAvailability(int pageId, string name)
        {
            var page = _pageservice.GetPageSimply(pageId);
            return Json(page.Name != name, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.PageManagement, Permission = "Can Clone Pages",
            Key = PermissionKeys.CanClonePage)]
        public virtual JsonResult ClonePage(int pageId, int laguageId, string name)
        {
            var page = _pageservice.GetPage(pageId);
            _pageservice.CreateClone(page, laguageId, name);
            return Json(page.LanguageId != laguageId, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual JsonResult GetLayoutsWithWidgets(int pageId)
        {
            var model = _pageservice.GetPage(pageId);
            var page = new Page();
            model.CopyPropertiesTo(page);
            var elemetsHolder = new LayoutElementViewHolder
            {
                Elements = OxideLayoutHelper.GetElements(page.InnerLayoutId, Syntax.LayoutTypes.Inner)
            };
            foreach (var element in elemetsHolder.Elements)
            {
                element.Widgets = _widgetService.GetPageWidgets(pageId, element.Id);
            }
            elemetsHolder.AddWidgetUrl = Url.Action(MVC.Admin.Widget.ActionNames.AddWidgetToPage, MVC.Admin.Widget.Name,
                T4MVCHelpers.DefaultArea());
            elemetsHolder.EditWidgetUrl = Url.Action(MVC.Admin.Widget.ActionNames.EditRegularWidget,
                MVC.Admin.Widget.Name, T4MVCHelpers.DefaultArea());
            elemetsHolder.DeleteWidgetUrl = Url.Action(MVC.Admin.Widget.ActionNames.DeleteWidgetFromPage,
                MVC.Admin.Widget.Name, T4MVCHelpers.DefaultArea());
            elemetsHolder.UpdateWidgetUrl = Url.Action(MVC.Admin.Widget.ActionNames.UpdateWidgetOnPage,
                MVC.Admin.Widget.Name, T4MVCHelpers.DefaultArea());
            elemetsHolder.Id = page.InnerLayoutId;
            return Json(elemetsHolder, JsonRequestBehavior.AllowGet);
        }

        public virtual PageViewModel GetNewPageViewModel()
        {
            var pageViewModel = new PageViewModel();
            var selectList =
                _languageService.GetAllActiveLanguages()
                                .Select(v => new SelectListItem {Text = v.Native, Value = v.Id.ToString()})
                                .ToList();
            pageViewModel.LanguageSelectList = selectList;
            return pageViewModel;
        }

        public virtual PageViewModel GetEditPageViewModel(int pageId, string returnUrl = null)
        {
            var pageViewModel = new PageViewModel();
            _pageservice.GetPageWithLanguage(pageId).CopyPropertiesTo(pageViewModel);
            pageViewModel.SiteMapFrequncySelected = (int) pageViewModel.SiteMapFrequncy;
            if (pageViewModel.VisibleForSeo) pageViewModel.IsVisibleForSeo = Constants.Checked;
            if (pageViewModel.VisibleForSite) pageViewModel.IsVisibleForSite = Constants.Checked;
            if (pageViewModel.PublishImd) pageViewModel.IsPublishImd = Constants.Checked;
            if (pageViewModel.PublishLater) pageViewModel.IsPublishLater = Constants.Checked;
            if (pageViewModel.HomePage) pageViewModel.IsHomePage = Constants.Checked;
            if (pageViewModel.AggresiveCacheEnabled) pageViewModel.IsAggresiveCacheEnabled = Constants.Checked;
            pageViewModel.PageType = pageViewModel.PageType;
            pageViewModel.SelectedDate = pageViewModel.PublishDate.ToShortDateString();
            pageViewModel.ReturUrl = returnUrl;

            var selectList =
                _languageService.GetAllActiveLanguages()
                                .Select(v => new SelectListItem {Text = v.Native, Value = v.Id.ToString()})
                                .ToList();
            pageViewModel.LanguageSelectList = selectList;
            return pageViewModel;
        }

        public virtual TemplatePageViewModel GetEditTemplatePageViewModel(int pageId, string returnUrl = null)
        {
            var pageViewModel = new TemplatePageViewModel {ActivePageUrl = HttpContext.Request.RawUrl};
            _pageservice.GetPageWithLanguage(pageId).CopyPropertiesTo(pageViewModel);
            pageViewModel.SiteMapFrequncySelected = (int)pageViewModel.SiteMapFrequncy;
            if (pageViewModel.VisibleForSeo) pageViewModel.IsVisibleForSeo = Constants.Checked;
            if (pageViewModel.VisibleForSite) pageViewModel.IsVisibleForSite = Constants.Checked;
            if (pageViewModel.PublishImd) pageViewModel.IsPublishImd = Constants.Checked;
            if (pageViewModel.PublishLater) pageViewModel.IsPublishLater = Constants.Checked;
            if (pageViewModel.HomePage) pageViewModel.IsHomePage = Constants.Checked;
            if (pageViewModel.AggresiveCacheEnabled) pageViewModel.IsAggresiveCacheEnabled = Constants.Checked;
            pageViewModel.SelectedDate = pageViewModel.PublishDate.ToShortDateString();
            pageViewModel.PageType = pageViewModel.PageType;
            pageViewModel.ReturUrl = returnUrl;
            var selectList =
                _languageService.GetAllActiveLanguages()
                                .Select(v => new SelectListItem { Text = v.Native, Value = v.Id.ToString() })
                                .ToList();
            pageViewModel.LanguageSelectList = selectList;
            return pageViewModel;
        }

        #endregion
    }
}