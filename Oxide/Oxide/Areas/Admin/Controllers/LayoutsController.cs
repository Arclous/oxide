﻿using System.Linq;
using System.Web.Mvc;
using Oxide.Areas.Admin.ViewModels;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideController;
using Oxide.Core.Helpers;
using Oxide.Core.Models.Base;
using Oxide.Core.Services.LanguageService;
using Oxide.Core.Services.LayoutService;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Services.WidgetService;
using Oxide.Core.Syntax;
using Oxide.Core.ViewModels.LayoutViewModels;

namespace Oxide.Areas.Admin.Controllers
{
    public partial class LayoutsController : OxideController
    {
        private readonly ILanguageService _languageService;
        private readonly ILayoutService _layoutService;
        private readonly IWidgetService _widgetService;

        public LayoutsController()
        {
        }

        public LayoutsController(IOxideServices oxideServices, ILayoutService layoutService,
            IWidgetService widgetService, ILanguageService languageService) : base(oxideServices)
        {
            _layoutService = layoutService;
            _widgetService = widgetService;
            _languageService = languageService;
        }

        [OxideAdminPermission(Group = PermissionKeys.LayoutManagement, Permission = "Can Display Layouts",
            Key = PermissionKeys.CanDisplayLayouts)]
        public virtual ActionResult DisplayLayouts()
        {
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Master Page Layouts"),
                    Title = Constants.Empty
                });
        }

        [OxideAdminPermission(Group = PermissionKeys.LayoutManagement, Permission = "Can Manage Layout Widgets",
            Key = PermissionKeys.CanDisplayLayoutWidgets)]
        public virtual ActionResult ManageWidgets(int mainLayoutId, string layoutName)
        {
            var activeLanguages = _languageService.GetAllActiveLanguages();
            var selectList =
                activeLanguages.Select(v => new SelectListItem {Text = v.Native, Value = v.Id.ToString()}).ToList();
            var firstCulture = activeLanguages[0].Id;
            var manageMainWidgetViewModel = new ManageMainWidgetViewModel
            {
                MainLayoutId = mainLayoutId,
                LanguageSelectList = selectList,
                Culture = firstCulture
            };
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Manage Widgets"),
                    Title = layoutName,
                    Parameters = manageMainWidgetViewModel
                });
        }

        #region Helpers

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.LayoutManagement, Permission = "Can Display Layouts",
            Key = PermissionKeys.CanDisplayLayouts)]
        public virtual JsonResult LayoutList(int pageSize, int page, string filter)
        {
            return Json(_layoutService.LayoutList(pageSize, page, filter), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.LayoutManagement, Permission = "Can Display Layouts",
            Key = PermissionKeys.CanDisplayLayouts)]
        public virtual JsonResult GetLayoutsWithWidgets(int mainLayoutId, int culture)
        {
            var elemetsHolder = new LayoutElementViewHolder
            {
                Elements = OxideLayoutHelper.GetElements(mainLayoutId, Syntax.LayoutTypes.Main)
            };
            foreach (var element in elemetsHolder.Elements)
            {
                element.Widgets = _widgetService.GetMainLayoutWidgets(mainLayoutId, element.Id, culture);
            }
            elemetsHolder.AddWidgetUrl = Url.Action(MVC.Admin.Widget.ActionNames.AddWidgetToMainLayout,
                MVC.Admin.Widget.Name, T4MVCHelpers.DefaultArea());
            elemetsHolder.EditWidgetUrl = Url.Action(MVC.Admin.Widget.ActionNames.EditMainLayoutWidget,
                MVC.Admin.Widget.Name, T4MVCHelpers.DefaultArea());
            elemetsHolder.DeleteWidgetUrl = Url.Action(MVC.Admin.Widget.ActionNames.DeleteWidgetFromMainLayout,
                MVC.Admin.Widget.Name, T4MVCHelpers.DefaultArea());
            elemetsHolder.UpdateWidgetUrl = Url.Action(MVC.Admin.Widget.ActionNames.UpdateWidgetOnMainLayout,
                MVC.Admin.Widget.Name, T4MVCHelpers.DefaultArea());
            elemetsHolder.Id = mainLayoutId;
            return Json(elemetsHolder, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.LayoutManagement, Permission = "Can Display Layouts",
            Key = PermissionKeys.CanDisplayLayouts)]
        public virtual JsonResult GetLayoutsPreview(int type, int id)
        {
            var img = "";
            switch (type)
            {
                case 0:
                {
                    var firstOrDefault = OxideLayoutHelper.GetMainLayoutList().Single(x => x.Id == id);
                    if (firstOrDefault != null)
                        img = firstOrDefault.PreviewImageUrl;
                }
                    break;
                case 1:
                {
                    var firstOrDefault = OxideLayoutHelper.GetMainLayoutList().Single(x => x.Id == id);
                    if (firstOrDefault != null)
                        img = firstOrDefault.PreviewImageUrl;
                }
                    break;
            }
            return Json(img, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}