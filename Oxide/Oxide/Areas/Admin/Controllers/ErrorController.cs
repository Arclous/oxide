﻿using System.Web.Mvc;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideController;
using Oxide.Core.Managers.ThemeManager;
using Oxide.Core.Models.Base;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Syntax;

namespace Oxide.Areas.Admin.Controllers
{
    public partial class ErrorController : OxideController
    {
        private readonly IThemeManager _themeManager;

        public ErrorController()
        {
        }

        public ErrorController(IOxideServices oxideServices, IThemeManager themeManager) : base(oxideServices)
        {
            _themeManager = themeManager;
        }

        public virtual ActionResult WidgetNotFound()
        {
            Response.StatusCode = 404;
            return View("Error",
                new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Error"),
                    Title = OxideServices.GetText("Widget Error"),
                    Parameters =
                        OxideServices.GetText("Widget not found, please check if the owner module is activated")
                });
        }

        public virtual ActionResult AdminError()
        {
            Response.StatusCode = 500;
            return View("Error",
                new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Error"),
                    Title = OxideServices.GetText("Server Error"),
                    Parameters = OxideServices.GetText("Server Error, please check the details")
                });
        }

        [Front]
        public virtual ActionResult PageNotFound()
        {
            Response.StatusCode = 404;
            return View(_themeManager.GetErrorPage(ErrorTypes.Error404),
                new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Error"),
                    Title = OxideServices.GetText("Page Error"),
                    Parameters = OxideServices.GetText("Page not found")
                });
        }
    }
}