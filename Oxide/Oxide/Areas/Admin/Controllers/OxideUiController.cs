﻿using System.Web.Mvc;
using Oxide.Core.Base.OxideController;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Services.OxideUiService;
using Oxide.Core.Syntax;

namespace Oxide.Areas.Admin.Controllers
{
    public partial class OxideUiController : OxideController
    {
        private readonly IOxideUiService _oxideUiService;

        public OxideUiController()
        {
        }

        public OxideUiController(IOxideServices oxideServices, IOxideUiService oxideUiService) : base(oxideServices)
        {
            _oxideUiService = oxideUiService;
        }

        public virtual ActionResult RenderContentSelecter(string contentName)
        {
            return PartialView(MVC.Admin.OxideUi.Views.ContentSelecter, contentName);
        }

        [HttpGet]
        public virtual JsonResult GetContents(string contentName, string filter,
            int filterCount = Constants.DefaultTopCount)
        {
            return Json(_oxideUiService.GetContents(contentName, filter, filterCount), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual JsonResult GetDataListContents(string contentName, int page, string filter,
            int pageSize = Constants.DefaultTopCount, bool withPicture = false, string pictureField = null)
        {
            return Json(_oxideUiService.GetContents(contentName, pageSize, page, filter, withPicture, pictureField),
                JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual JsonResult GetMediaContentsWithImage(string contentName, string filter, string imageField,
            int filterCount = Constants.DefaultTopCount)
        {
            return Json(_oxideUiService.GetContentsWithImage(contentName, filter, filterCount, imageField),
                JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual JsonResult GetFileContents(string contentName, string filter,
            int filterCount = Constants.DefaultTopCount)
        {
            return Json(_oxideUiService.GetFileContents(contentName, filter, filterCount), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual JsonResult GetMediaContents(int page, int directoryId, int pageSize = Constants.DefaultTopCount)
        {
            return Json(_oxideUiService.GetMediaContents(pageSize, page, directoryId), JsonRequestBehavior.AllowGet);
        }
    }
}