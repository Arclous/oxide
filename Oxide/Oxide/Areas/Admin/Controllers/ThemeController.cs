﻿using System.Web.Mvc;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideController;
using Oxide.Core.Models.Base;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Services.ThemeService;
using Oxide.Core.Syntax;

namespace Oxide.Areas.Admin.Controllers
{
    public partial class ThemeController : OxideController
    {
        private readonly IThemeService _themeService;

        public ThemeController()
        {
        }

        public ThemeController(IOxideServices oxideServices, IThemeService themeService) : base(oxideServices)
        {
            _themeService = themeService;
        }

        [OxideAdminPermission(Group = PermissionKeys.ThemeManagement, Permission = "Can Display Themes",
            Key = PermissionKeys.CanDisplayThemes)]
        public virtual ActionResult DisplayThemes()
        {
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Themes"),
                    Title = Constants.Empty
                });
        }

        #region Helpers

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.ThemeManagement, Permission = "Can Display Themes",
            Key = PermissionKeys.CanDisplayThemes)]
        public virtual JsonResult ThemeList(string filter)
        {
            return Json(_themeService.ThemeList(filter), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.ThemeManagement, Permission = "Can Manage Themes",
            Key = PermissionKeys.CanManageThemes)]
        public virtual JsonResult ChangeStatus(string name, bool status)
        {
            _themeService.ChangeStatus(name, status);
            return Json(_themeService.ThemeList(Constants.Empty), JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}