﻿using System.Linq;
using System.Web.Mvc;
using Oxide.Areas.Admin.Models;
using Oxide.Areas.Admin.ViewModels;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideController;
using Oxide.Core.Extesions;
using Oxide.Core.Helpers;
using Oxide.Core.Models.Base;
using Oxide.Core.Models.Settings;
using Oxide.Core.Services.LanguageService;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Services.SiteSettings;
using Oxide.Core.Services.TimeZoneService;
using Oxide.Core.Syntax;
using Oxide.Core.ViewModels.SettingsViewModels;

namespace Oxide.Areas.Admin.Controllers
{
    public partial class SettingsController : OxideController
    {
        private readonly IEmailSettingsService _emailSettingsService;
        private readonly ILanguageService _languageService;
        private readonly ISiteSettingsService _siteSettingsService;
        private readonly ITimeZoneService _timeZoneService;

        public SettingsController()
        {
        }

        public SettingsController(IOxideServices oxideServices, ISiteSettingsService siteSettingsService,
            ILanguageService languageService, ITimeZoneService timeZoneService,
            IEmailSettingsService emailSettingsService) : base(oxideServices)
        {
            _siteSettingsService = siteSettingsService;
            _languageService = languageService;
            _timeZoneService = timeZoneService;
            _emailSettingsService = emailSettingsService;
        }

        [OxideAdminPermission(Group = PermissionKeys.SettingsManagement, Permission = "Can Display Settings",
            Key = PermissionKeys.CanDisplaySettings)]
        public virtual ActionResult Index()
        {
            var currentSettings = _siteSettingsService.GetSettings();
            var siteSettings = new SiteSettingsViewModel();
            if (currentSettings != null)
            {
                siteSettings.Name = currentSettings.Name;
                siteSettings.DefaultTimeZone = currentSettings.DefaultTimeZone;
                siteSettings.DefaultSiteCulture = currentSettings.DefaultSiteCulture;
                siteSettings.DefaultTimeZoneId = currentSettings.DefaultTimeZone.Id;
                siteSettings.DefaultSiteCultureSelected = currentSettings.DefaultSiteCulture.Id;
                siteSettings.Name = currentSettings.Name;
            }
            else
            {
                siteSettings.Name = "";
                siteSettings.DefaultTimeZone = _timeZoneService.GetTimeZone(64);
                siteSettings.DefaultSiteCulture = _languageService.GetLanguage("eng");
                siteSettings.DefaultTimeZoneId = siteSettings.DefaultTimeZone.Id;
                siteSettings.DefaultSiteCultureSelected = siteSettings.DefaultSiteCulture.Id;
            }
            siteSettings.TimeZoneSelectList =
                new SelectList(
                    _timeZoneService.GetAllTimeZones()
                                    .Select(x => new SelectListItem {Text = x.Name, Value = x.Id.ToString()}), "Value",
                    "Text");
            var selectList =
                _languageService.GetAllActiveLanguages()
                                .Select(v => new SelectListItem {Text = v.Native, Value = v.Id.ToString()})
                                .ToList();
            siteSettings.LanguageSelectList = selectList;
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("General Settings"),
                    Title = Constants.Empty,
                    Parameters = siteSettings
                });
        }

        [OxideAdminPermission(Group = PermissionKeys.SettingsManagement, Permission = "Can Manage Settings",
            Key = PermissionKeys.CanManageSettings)]
        public virtual ActionResult SaveSettings(SiteSettingsViewModel siteSettingsGeneralViewModel)
        {
            var siteSettingsGeneral = new SiteSettings();
            siteSettingsGeneral = siteSettingsGeneralViewModel.CopyPropertiesTo(siteSettingsGeneral);
            siteSettingsGeneral.DefaultSiteCultureId = siteSettingsGeneralViewModel.DefaultSiteCultureSelected;
            siteSettingsGeneral.DefaultTimeZoneId = siteSettingsGeneralViewModel.DefaultTimeZoneSelected;
            _siteSettingsService.SaveSettings(siteSettingsGeneral);
            return RedirectToAction(MVC.Admin.Settings.ActionNames.Index);
        }

        [OxideAdminPermission(Group = PermissionKeys.SettingsManagement, Permission = "Can Display Email Settings",
            Key = PermissionKeys.CanDisplayEmailSettings)]
        public virtual ActionResult EmailSettings()
        {
            var emailSettings = _emailSettingsService.GetEmailSettings();
            var emailSettingsViewModel = new EmailSettingsViewModel();
            if (emailSettings != null)
            {
                emailSettings.CopyPropertiesTo(emailSettingsViewModel);
                emailSettingsViewModel.IsRequireCredentials = emailSettings.RequireCredentials ? "checked" : "";
                emailSettingsViewModel.IsEnableSsl = emailSettings.EnableSsl ? "checked" : "";
                emailSettingsViewModel.Password = OxideUtils.Decrypt(emailSettingsViewModel.Password);
            }
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Email Settings"),
                    Title = Constants.Empty,
                    Parameters = emailSettingsViewModel
                });
        }

        [OxideAdminPermission(Group = PermissionKeys.SettingsManagement, Permission = "Can Manage Email Settings",
            Key = PermissionKeys.CanManageEmailSettings)]
        public virtual ActionResult SaveEmailSettings(EmailSettingsViewModel emailSettings)
        {
            var emailSettingsValues = new EmailSettings();
            emailSettings.CopyPropertiesTo(emailSettingsValues);
            emailSettingsValues.Password = OxideUtils.Encrypt(emailSettings.Password);
            _emailSettingsService.SaveEmailSettings(emailSettingsValues);
            return RedirectToAction(MVC.Admin.Settings.ActionNames.EmailSettings);
        }
    }
}