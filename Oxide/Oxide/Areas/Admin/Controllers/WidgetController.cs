﻿using System.Linq;
using System.Web.Mvc;
using Oxide.Areas.Admin.ViewModels;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideController;
using Oxide.Core.Managers.WidgetManager;
using Oxide.Core.Models;
using Oxide.Core.Models.Base;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Services.WidgetService;
using Oxide.Core.Syntax;

namespace Oxide.Areas.Admin.Controllers
{
    public partial class WidgetController : OxideController
    {
        private readonly IWidgetManager _widgetManager;
        private readonly IWidgetService _widgetService;

        public WidgetController()
        {
        }

        public WidgetController(IOxideServices oxideServices, IWidgetService widgetService, IWidgetManager widgetManager)
            : base(oxideServices)
        {
            _widgetService = widgetService;
            _widgetManager = widgetManager;
        }

        [OxideAdminPermission(Group = PermissionKeys.WidgetManagement, Permission = "Can Display Widgets",
            Key = PermissionKeys.CanDisplayWidgets)]
        public virtual ActionResult DisplayWidgets()
        {
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Widgets"),
                    Title = Constants.Empty
                });
        }

        [OxideAdminPermission(Group = PermissionKeys.WidgetManagement, Permission = "Can Edit Widgets",
            Key = PermissionKeys.CanEditWidgets)]
        public virtual ActionResult EditRegularWidget(int widgetId, int pageId)
        {
            var editWidgetViewModel = new EditWidgetViewModel
            {
                PageId = pageId,
                WidgetData = _widgetService.GetPageWidget(widgetId)
            };
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Edit Widget"),
                    Title = Constants.Empty,
                    Parameters = editWidgetViewModel
                });
        }

        [OxideAdminPermission(Group = PermissionKeys.WidgetManagement, Permission = "Can Edit Widgets",
            Key = PermissionKeys.CanEditWidgets)]
        public virtual ActionResult EditMainLayoutWidget(int widgetId, int pageId, string layoutName)
        {
            var editWidgetViewModel = new EditMainLayoutWidgetViewModel
            {
                PageId = pageId,
                WidgetData = _widgetService.GetMainLayoutWidget(widgetId),
                LayoutName = layoutName
            };
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Edit Widget"),
                    Title = Constants.Empty,
                    Parameters = editWidgetViewModel
                });
        }

        [OxideAdminPermission(Group = PermissionKeys.WidgetManagement, Permission = "Can Manage Widgets",
            Key = PermissionKeys.CanManageWidgets)]
        public virtual ActionResult UpdateRegularWidget(string title, int widgetId, int pageId)
        {
            _widgetService.UpdateWidgetTitle(widgetId, title);
            return RedirectToAction(MVC.Admin.Page.ActionNames.ManageWidgets, MVC.Admin.Page.Name,
                new {Area = T4MVCHelpers.DefaultArea(), pageId});
        }

        [OxideAdminPermission(Group = PermissionKeys.WidgetManagement, Permission = "Can Manage Widgets",
            Key = PermissionKeys.CanManageWidgets)]
        public virtual ActionResult UpdateMainLayoutWidget(string title, int widgetId, int pageId, string layoutName)
        {
            _widgetService.UpdateMainLayotWidgetTitle(widgetId, title);
            return RedirectToAction(MVC.Admin.Layouts.ActionNames.ManageWidgets, MVC.Admin.Layouts.Name,
                new {Area = T4MVCHelpers.DefaultArea(), mainLayoutId = pageId, layoutName});
        }

        [OxideAdminPermission(Group = PermissionKeys.WidgetManagement, Permission = "Can Manage Widgets",
            Key = PermissionKeys.CanManageWidgets)]
        public virtual ActionResult RenderWidgetAdmin(string widgetKey, int widgetId, int containerPageId, int layoutId,
            int layoutKeyId, string dynamicTitle, string successMessage = null, string errorMessage = null)
        {
            var widgetReturnResult = _widgetManager.RenderAdmin(widgetKey, widgetId, layoutId, layoutKeyId,
                ControllerContext, dynamicTitle, containerPageId);
            widgetReturnResult.ErrorMessage = errorMessage;
            widgetReturnResult.SuccessMessage = successMessage;
            return View(MVC.Admin.Widget.Views.ManageWidget, widgetReturnResult);
        }

        [OxideAdminPermission(Group = PermissionKeys.WidgetManagement, Permission = "Can Manage Widgets",
            Key = PermissionKeys.CanManageWidgets)]
        public virtual ActionResult RenderMainWidgetAdmin(string widgetKey, int widgetId, int layoutId, int layoutKeyId,
            string dynamicTitle, int widgetCulture, string successMessage = null, string errorMessage = null)
        {
            var widgetReturnResult = _widgetManager.RenderAdmin(widgetKey, widgetId, layoutId, layoutKeyId,
                ControllerContext, dynamicTitle, -1, widgetCulture);
            widgetReturnResult.ErrorMessage = errorMessage;
            widgetReturnResult.SuccessMessage = successMessage;
            return View(MVC.Admin.Widget.Views.ManageWidget, widgetReturnResult);
        }

        [HttpPost, ValidateInput(false)]
        [OxideAdminPermission(Group = PermissionKeys.WidgetManagement, Permission = "Can Manage Widgets",
            Key = PermissionKeys.CanManageWidgets)]
        public virtual ActionResult UpdateWidget(FormCollection form)
        {
            var widgetData = _widgetManager.GetWidgetData(form);
            var updatedModel = _widgetManager.GenerateExpandoModel(form);
            var widgetReturnResult = _widgetManager.UpdateAdmin(widgetData.WidgetKey, widgetData.WidgetId,
                widgetData.LayoutId, widgetData.LayoutKeyId, ControllerContext, widgetData.DynamicTitle, updatedModel,
                widgetData.ContainerPageId, widgetData.WidgetCulture);
            if (widgetData.Regular)
            {
                return RedirectToAction(MVC.Admin.Widget.ActionNames.RenderWidgetAdmin,
                    new
                    {
                        Area = Constants.DefaultArea,
                        widgetKey = widgetData.WidgetKey,
                        widgetId = widgetData.WidgetId,
                        layoutId = widgetData.LayoutId,
                        layoutKeyId = widgetData.LayoutKeyId,
                        dynamicTitle = widgetData.DynamicTitle,
                        containerPageId = widgetData.ContainerPageId,
                        successMessage = widgetReturnResult.SuccessMessage,
                        errorMessage = widgetReturnResult.ErrorMessage
                    });
            }
            return RedirectToAction(MVC.Admin.Widget.ActionNames.RenderMainWidgetAdmin,
                new
                {
                    Area = Constants.DefaultArea,
                    widgetKey = widgetData.WidgetKey,
                    widgetId = widgetData.WidgetId,
                    layoutId = widgetData.LayoutId,
                    layoutKeyId = widgetData.LayoutKeyId,
                    dynamicTitle = widgetData.DynamicTitle,
                    widgetCulture = widgetData.WidgetCulture,
                    successMessage = widgetReturnResult.SuccessMessage,
                    errorMessage = widgetReturnResult.ErrorMessage
                });
        }

        #region Helpers

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.WidgetManagement, Permission = "Can Manage Widgets",
            Key = PermissionKeys.CanManageWidgets)]
        public virtual JsonResult WidgetList(string filter)
        {
            return Json(_widgetService.WidgetList(filter), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.WidgetManagement, Permission = "Can Manage Widgets",
            Key = PermissionKeys.CanManageWidgets)]
        public virtual JsonResult GetActiveWidgets(string filter)
        {
            return Json(_widgetService.WidgetList(filter).Records.Where(x => x.Active), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.WidgetManagement, Permission = "Can Add Widgets to Page",
            Key = PermissionKeys.CanAddWidgetsToPage)]
        public virtual JsonResult AddWidgetToPage(int pageId, int layoutId, int elementId, string widgetKey)
        {
            var newWidget = _widgetService.AddWidgetToPage(pageId, layoutId, elementId, widgetKey);
            return Json(newWidget, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.WidgetManagement, Permission = "Can Update Widgets on Page",
            Key = PermissionKeys.CanUpdateWidgetsOnPage)]
        public virtual JsonResult UpdateWidgetOnPage(int id, int pageId, int layoutId, int elementId, string widgetKey,
            string orderList)
        {
            _widgetService.UpdateWidgetOnPage(id, pageId, layoutId, elementId, widgetKey, orderList);
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.WidgetManagement, Permission = "Can Delete Widgets from Page",
            Key = PermissionKeys.CanDeleteWidgetsFromPage)]
        public virtual JsonResult DeleteWidgetFromPage(int id)
        {
            _widgetService.DeleteWidgetFromPage(id);
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.WidgetManagement, Permission = "Can Add Widgets to Layout",
            Key = PermissionKeys.CanAddWidgetsToLayout)]
        public virtual JsonResult AddWidgetToMainLayout(int layoutId, int elementId, string widgetKey, int culture)
        {
            var newWidget = _widgetService.AddWidgetToMainLayout(layoutId, elementId, widgetKey, culture);
            return Json(newWidget, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.WidgetManagement, Permission = "Can Update Widgets on Layout",
            Key = PermissionKeys.CanUpdateWidgetsOnLayout)]
        public virtual JsonResult UpdateWidgetOnMainLayout(int id, int layoutId, int elementId, string widgetKey,
            string orderList)
        {
            _widgetService.UpdateWidgetOnMainLayout(id, layoutId, elementId, widgetKey, orderList);
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.WidgetManagement, Permission = "Can Delete Widgets from Layout",
            Key = PermissionKeys.CanDeleteWidgetsFromLayout)]
        public virtual JsonResult DeleteWidgetFromMainLayout(int id)
        {
            _widgetService.DeleteWidgetFromMainLayout(id);
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.WidgetManagement, Permission = "Can Manage Widgets",
            Key = PermissionKeys.CanManageWidgets)]
        public virtual JsonResult ChangeStatus(string widgetKey, bool status)
        {
            var result = new JsonWidgetActionResultModel {Active = _widgetService.ChangeStatus(widgetKey, status)};
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}