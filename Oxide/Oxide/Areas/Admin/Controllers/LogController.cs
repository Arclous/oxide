﻿using System.Web.Mvc;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideController;
using Oxide.Core.Models.Base;
using Oxide.Core.Services.LogService;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Syntax;

namespace Oxide.Areas.Admin.Controllers
{
    public partial class LogController : OxideController
    {
        private readonly ILogService _logService;

        public LogController()
        {
        }

        public LogController(IOxideServices oxideServices, ILogService logService) : base(oxideServices)
        {
            _logService = logService;
        }

        [OxideAdminPermission(Group = PermissionKeys.LogManagement, Permission = "Can Display Logs",
            Key = PermissionKeys.CanDisplayLogs)]
        public virtual ActionResult DisplayLogs()
        {
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Logs"),
                    Title = Constants.Empty
                });
        }

        [OxideAdminPermission(Group = PermissionKeys.LogManagement, Permission = "Can Display Logs",
            Key = PermissionKeys.CanDisplayLogs)]
        public virtual ActionResult DisplayLogInfo(int id)
        {
            var logInfo = _logService.GetLog(id);
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("Log Info"),
                    Title = Constants.Empty,
                    Parameters = logInfo
                });
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.LogManagement, Permission = "Can Display Logs",
            Key = PermissionKeys.CanDisplayLogs)]
        public virtual JsonResult LogList(int? pageSize, int? page, string filter)
        {
            if (pageSize == null) pageSize = 10;
            if (page == null) page = 0;
            var logsList = _logService.GetLogsWithFilter((int) pageSize, (int) page, filter);
            return Json(logsList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.LogManagement, Permission = "Can Clear Logs",
            Key = PermissionKeys.CanClearLogs)]
        public virtual JsonResult ClearLogs()
        {
            _logService.ClearLogs();
            return Json(new {success = true}, JsonRequestBehavior.AllowGet);
        }
    }
}