﻿using System.Web.Mvc;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideController;
using Oxide.Core.Managers.ScheduledJobManager;
using Oxide.Core.Models.Base;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Services.ScheduledJobService;
using Oxide.Core.Syntax;

namespace Oxide.Areas.Admin.Controllers
{
    public partial class ScheduledJobController : OxideController
    {
        private readonly IScheduledJobManager _scheduledJobManager;
        private readonly IScheduledJobService _scheduledJobService;

        public ScheduledJobController()
        {
        }

        public ScheduledJobController(IOxideServices oxideServices, IScheduledJobService scheduledJobService,
            IScheduledJobManager scheduledJobManager) : base(oxideServices)
        {
            _scheduledJobService = scheduledJobService;
            _scheduledJobManager = scheduledJobManager;
        }

        [OxideAdminPermission(Group = PermissionKeys.ScheduledJobManagement, Permission = "Can Display Scheduled Jobs",
            Key = PermissionKeys.CanDisplayScheduleJobs)]
        public virtual ActionResult DisplayScheduledJobs()
        {
            var resultModel = TempData[Constants.RescheduledJob] as OxideModel;
            var oxideModel = resultModel ??
                             new OxideModel
                             {
                                 RequestType = Syntax.Requests.Static,
                                 Header = OxideServices.GetText("Scheduled Jobs"),
                                 Title = Constants.Empty
                             };
            return View(oxideModel);
        }

        [OxideAdminPermission(Group = PermissionKeys.ScheduledJobManagement, Permission = "Can ReSchedule Jobs",
            Key = PermissionKeys.CanReScheduleJobs)]
        public virtual ActionResult ReSchedule(int id)
        {
            var scheduledJob = _scheduledJobService.GetJob(id);
            return
                View(new OxideModel
                {
                    RequestType = Syntax.Requests.Static,
                    Header = OxideServices.GetText("ReSchedule Job"),
                    Title = Constants.Empty,
                    Parameters = scheduledJob
                });
        }

        [OxideAdminPermission(Group = PermissionKeys.ScheduledJobManagement, Permission = "Can ReSchedule Jobs",
            Key = PermissionKeys.CanReScheduleJobs)]
        public virtual ActionResult SaveSchduledJobSettings(int id, int intervalType, int interval, int type,
            int canRunOn)
        {
            var oxideModel = new OxideModel {RequestType = Syntax.Requests.Static};
            var scheduledJob = _scheduledJobService.GetJob(id);
            scheduledJob.IntervalType = intervalType;
            scheduledJob.Interval = interval;
            scheduledJob.Type = type;
            scheduledJob.CanRunOn = (Syntax.ScheduledJobRunPlatforms) canRunOn;
            _scheduledJobService.UpdateJob(scheduledJob);
            _scheduledJobManager.ReScheduled(scheduledJob.Name);
            oxideModel.SuccessMessage = OxideServices.GetText("Job rescheduled successful");
            TempData[Constants.RescheduledJob] = oxideModel;
            return RedirectToAction(MVC.Admin.ScheduledJob.ActionNames.DisplayScheduledJobs, MVC.Admin.ScheduledJob.Name);
        }

        #region Helpers

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.ScheduledJobManagement, Permission = "Can Display Scheduled Jobs",
            Key = PermissionKeys.CanDisplayScheduleJobs)]
        public virtual JsonResult ScheduledJobList(int? pageSize, int? page, string filter)
        {
            if (pageSize == null) pageSize = 10;
            if (page == null) page = 0;
            var jobList = _scheduledJobService.GetScheduledJobListWithFilter((int) pageSize, (int) page, filter);
            return Json(jobList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.ScheduledJobManagement, Permission = "Can Start Scheduled Jobs",
            Key = PermissionKeys.CanStartJobs)]
        public virtual JsonResult StartJob(string name)
        {
            return Json(_scheduledJobManager.StartJob(name) ? new {success = true} : new {success = false},
                JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.ScheduledJobManagement, Permission = "Can Stop Scheduled Jobs",
            Key = PermissionKeys.CanStopJob)]
        public virtual JsonResult StopJob(string name)
        {
            return Json(_scheduledJobManager.StopJob(name) ? new {success = true} : new {success = false},
                JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OxideAdminPermission(Group = PermissionKeys.ScheduledJobManagement,
            Permission = "Can Force Start Scheduled Jobs", Key = PermissionKeys.CanForceStartJob)]
        public virtual JsonResult ForceStart(string name)
        {
            return Json(_scheduledJobManager.ForceStart(name) ? new {success = true} : new {success = false},
                JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}