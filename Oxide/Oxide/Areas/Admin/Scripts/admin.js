﻿function showWarning(message, title) {
    new PNotify({
        title: title,
        text: message,
        type: "error",
        styling: "bootstrap3"
    });
}

function showInfo(message, title) {
    new PNotify({
        title: title,
        text: message,
        type: "info",
        styling: "bootstrap3"
    });
}

function showAccessDenied() {
    new PNotify({
        title: "Access Denied",
        text: "You dont have enough permission to complette this action, please contact with your administrator to extend your permission to complete this action.",
        type: "error",
        styling: "bootstrap3"
    });
}

//Global permission handler
$(document).ajaxError(function(e, xhr) {
    if (xhr.status === 403)
        showAccessDenied();
});

function toggleFullScreen() {
    if ((document.fullScreenElement && document.fullScreenElement !== null) ||
    (!document.mozFullScreen && !document.webkitIsFullScreen)) {

        if (document.documentElement.requestFullScreen) {
            document.documentElement.requestFullScreen();
        } else if (document.documentElement.mozRequestFullScreen) {
            document.documentElement.mozRequestFullScreen();
        } else if (document.documentElement.webkitRequestFullScreen) {
            document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
        }
    } else {

        if (document.cancelFullScreen) {
            document.cancelFullScreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
        }
    }
}