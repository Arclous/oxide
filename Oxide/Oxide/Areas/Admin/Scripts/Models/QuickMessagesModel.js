﻿function QuickMessagesModel(urlQuickMessageInfo) {
    var self = this;
    self.isLoading = ko.observable(false);
    self.quickMessages = ko.observableArray([]);
    self.totalUnreadMessages = ko.observable();
    self.lastId = ko.observable(0);
    self.loadQuickMessagePackage = function() {
        $.get(urlQuickMessageInfo, {}, function(data) {
            self.quickMessages.removeAll();
            for (var i = 0; i <= data.Records.length - 1; i++) {
                var logModel = new QuickMessageModel(
                    data.Records[i].Id,
                    data.Records[i].Title,
                    data.Records[i].MessageBody,
                    data.Records[i].MessageDateTime,
                    data.Records[i].IsMessageRead,
                    data.Records[i].SenderUserName,
                    data.Records[i].SenderUserImage
                );
                self.totalUnreadMessages(data.TotalRecords);
                self.addQuickMessageItem(logModel);
            }
            if (data.Records.length > 0)
                self.lastId(self.quickMessages()[data.Records.length - 1].id());
        });
    };
    self.addQuickMessageItem = function(model) {
        self.quickMessages.push(model);
    }.bind(self);
}

function QuickMessageModel(id, title, message, dateTime, status, senderUser, senderUserImage) {
    var self = this;
    self.id = ko.observable(id);
    self.title = ko.observable(title);
    self.message = ko.observable(message);
    self.dateTime = ko.observable(dateTime);
    self.status = ko.observable(status);
    self.senderUser = ko.observable(senderUser);
    self.senderUserImage = ko.observable(senderUserImage);
}