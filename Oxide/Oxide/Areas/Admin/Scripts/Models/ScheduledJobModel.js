﻿ko.bindingHandlers.bootstrapModal = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var props = valueAccessor(),
            vm = bindingContext.createChildContext(viewModel);
        ko.utils.extend(vm, props);
        vm.close = function() {
            vm.show(false);
            vm.onClose();
        };
        vm.action = function() {
            vm.onAction();
        };
        ko.utils.toggleDomNodeCssClass(element, "modal fade", true);
        ko.renderTemplate("deleteConfirmTemplate", vm, null, element);
        var showHide = ko.computed(function() {
            $(element).modal(vm.show() ? "show" : "hide");
        });
        return {
            controlsDescendantBindings: true
        };
    }
};

function ScheduledJobsModel(urlBase, forceStartUrl, startUrl, stopUrl) {
    var self = this;
    self.isLoading = ko.observable(false);
    self.totalRecords = ko.observable();
    self.totalPage = ko.observable();
    self.currentPage = ko.observable();
    self.displayinRecords = ko.observable();
    self.nextEnable = ko.observable(true);
    self.previousEnable = ko.observable(true);
    self.scheduledJobs = ko.observableArray([]);
    self.pageSize = ko.observable();
    self.actionFilter = ko.observable("");
    self.actionCurrentPage = ko.observable(0);
    self.actionPageSize = ko.observable(10);
    self.forceStartUrl = ko.observable(forceStartUrl);
    self.startUrl = ko.observable(startUrl);
    self.stopUrl = ko.observable(stopUrl);

    self.loadData = function(pagesize, page, filter) {
        self.isLoading(true);
        $.get(urlBase, { pageSize: pagesize, page: page, filter: filter }, function(data) {
            self.scheduledJobs.removeAll();
            for (var i = 0; i <= data.Records.length - 1; i++) {
                var jobModel = new JobModel(
                    data.Records[i].Id,
                    data.Records[i].Name,
                    data.Records[i].Title,
                    data.Records[i].Key,
                    data.Records[i].Group,
                    data.Records[i].Owner,
                    data.Records[i].Active,
                    data.Records[i].LastStart,
                    data.Records[i].LastFinish,
                    data.Records[i].NextStart,
                    data.Records[i].ManageUrl,
                    data.Records[i].CanManage
                );
                self.addJob(jobModel);
            }
            self.totalRecords(data.TotalRecords);
            self.totalPage(data.TotalPages);
            self.currentPage(data.CurrentPage);
            self.displayinRecords(data.DisplayingRecords);
            self.nextEnable(data.NextEnable);
            self.previousEnable(data.PreviousEnable);
            self.isLoading(false);
            self.setControls();
        });
    };
    self.addJob = function(model) {
        self.scheduledJobs.push(model);
    }.bind(self);

    self.stopJob = function(model) {
        model.buttonTextActive(ScheduledJobs.Please_Wait___);
        model.buttonTextNotActive(ScheduledJobs.Please_Wait___);
        $.get(self.stopUrl(), { name: model.name() }, function(data) {
            model.isWorking(false);
            if (data.success)
                model.active(false);
            else {
                showWarning(ScheduledJobs.Cannot_complette_the_requested_process, ScheduledJobs.Warning);
            }
            model.buttonTextActive(ScheduledJobs.Active);
            model.buttonTextNotActive(ScheduledJobs.Not_Active);
        });
    }.bind(self);

    self.startJob = function(model) {
        model.buttonTextActive(ScheduledJobs.Please_Wait___);
        model.buttonTextNotActive(ScheduledJobs.Please_Wait___);
        $.get(self.startUrl(), { name: model.name() }, function(data) {
            if (data.success)
                model.active(true);
            else {
                showWarning(ScheduledJobs.Cannot_complette_the_requested_process, ScheduledJobs.Warning);
            }
            model.buttonTextActive(ScheduledJobs.Active);
            model.buttonTextNotActive(ScheduledJobs.Not_Active);
        });
    }.bind(self);

    self.forceStartJob = function(model) {
        model.buttonTextForce(ScheduledJobs.Please_Wait___);
        $.get(self.forceStartUrl(), { name: model.name() }, function(data) {
            if (data.success) {
                if (!model.active())
                    model.active(true);
            } else {
                showWarning(ScheduledJobs.Cannot_complette_the_requested_process, ScheduledJobs.Warning);
            }
            model.buttonTextForce(ScheduledJobs.Force_Start);
        });
    }.bind(self);

    self.modal = {
        header: ScheduledJobs.Please_Confirm,
        message: ko.observable(ScheduledJobs.Do_you_want_to_stop_job_),
        comment: ko.observable(""),
        closeLabel: ScheduledJobs.Cancel,
        primaryLabel: ScheduledJobs.Stop,
        selectedJob: ko.observable(),
        show: ko.observable(false),
        onClose: function() {
            self.onModalClose();
        },
        onAction: function() {
            self.onModalAction();
        }
    };
    self.showModal = function(job) {
        self.modal.message("'" + job.title() + "' " + ScheduledJobs.Do_you_want_to_stop_job_);
        self.modal.selectedJob(job);
        self.modal.show(true);
    };
    self.onModalClose = function() {

    };
    self.onModalAction = function() {
        self.stopJob(self.modal.selectedJob());
        self.modal.show(false);
    };
    self.setControls = function() {
        $("#datatable-checkbox_info").html(ScheduledJobs.Showing + (self.currentPage() + 1) + " / " + self.totalPage());
        $(".dataTables_empty").html("");
        $("#datatable-checkbox_next").unbind();
        $("#datatable-checkbox_previous").unbind();
        $("#datatable-checkbox_next").addClass("disabled");
        $("#datatable-checkbox_previous").addClass("disabled");

        if (self.nextEnable()) {
            $("#datatable-checkbox_next").removeClass("disabled");
            $("#datatable-checkbox_next").click(function() {
                self.actionCurrentPage(self.actionCurrentPage() + 1);
                self.loadData(self.actionPageSize(), self.actionCurrentPage(), self.actionFilter());
            });
        }
        if (self.previousEnable()) {
            $("#datatable-checkbox_previous").removeClass("disabled");
            $("#datatable-checkbox_previous").click(function() {
                self.actionCurrentPage(self.actionCurrentPage() - 1);
                self.loadData(self.actionPageSize(), self.actionCurrentPage(), self.actionFilter());
            });
        }
    }.bind(self);

}

function JobModel(id, name, title, key, group, owner, active, lastStart, lastFinish, nextStart, manageUrl, canManage) {
    var self = this;
    self.isWorking = ko.observable(false);
    self.id = ko.observable(id);
    self.name = ko.observable(name);
    self.title = ko.observable(title);
    self.key = ko.observable(key);
    self.group = ko.observable(group);
    self.owner = ko.observable(owner);
    self.active = ko.observable(active);
    self.lastStart = ko.observable(lastStart);
    self.lastFinish = ko.observable(lastFinish);
    self.nextStart = ko.observable(nextStart);
    self.manageUrl = ko.observable(manageUrl);
    self.buttonTextActive = ko.observable(ScheduledJobs.Active);
    self.buttonTextNotActive = ko.observable(ScheduledJobs.Not_Active);
    self.buttonTextForce = ko.observable(ScheduledJobs.Force_Start);
    self.canManage = ko.observable(canManage);
}