﻿function ContentModelMedia(elementId, url, multiple, direcotryLoadUrl, selectedContentsModels, drectorySubList) {
    var self = this;
    self.elementId = ko.observable(elementId);
    self.pageSize = ko.observable(20);
    self.pageNumber = ko.observable(0);
    self.totalRecords = ko.observable();
    self.totalPage = ko.observable();
    self.currentPage = ko.observable();
    self.displayinRecords = ko.observable();
    self.nextEnable = ko.observable(true);
    self.previousEnable = ko.observable(true);
    self.contentListTemplate = ko.observable();
    self.contentSelectListTemplate = ko.observable();
    self.contentItemTemplate = ko.observable();
    self.multiple = ko.observable();
    self.url = ko.observable(url);
    self.selectedContent = ko.observableArray([]);
    self.directories = ko.observableArray([]);
    self.selectedNode = ko.observable();
    self.isLoading = ko.observable();
    self.contents = ko.observableArray([]);
    self.selectedContentValues = ko.observable();
    self.deleteButtonTitle = ko.observable(MediaSelecter.Delete);
    self.buttonNext = ko.observable(MediaSelecter.Next);
    self.buttonPrevious = ko.observable(MediaSelecter.Previous);
    self.selectedContent.subscribe(function() {
        self.selectedContentValues(joinMediaValues(self.selectedContent()));
    });

    //Load selected content models
    self.selectedContent(ko.utils.arrayMap(selectedContentsModels, function(item) {
        var newItem = new ContentModeItem(item);
        newItem.selected(true);
        return newItem;
    }));

    if (multiple.toLowerCase() === "true")
        self.multiple(true);
    else
        self.multiple(false);

    self.loadDataDirectory = function() {
        self.isLoading(true);
        $.get(direcotryLoadUrl, {}, function(returnData) {
            if (returnData.success) {
                var directory = new DirectoryItem(returnData.data, this);
                if (IsDirectoryAdded(self.directories(), directory.id()) == null)
                    self.directories.push(directory);

                self.isLoading(false);
                prepeareTreeViewForContents();
            } else
                showWarning(MediaSelecter.Loading_directories_is_not_successful_, MediaSelecter.Warning);
        });

    };
    self.loadSubData = function(data, object) {
        self.isLoading(true);
        $.get(drectorySubList, { id: data.id() }, function(returnData) {
            if (returnData.success) {
                for (var index = 0; index <= returnData.data.length - 1; index++) {
                    var directory = new DirectoryItem(returnData.data[index], object);
                    if (IsDirectoryAdded(object.directories(), directory.id()) == null)
                        object.directories.push(directory);
                }
                self.isLoading(false);
                prepeareTreeViewForContents();
                self.pageNumber(0);
                self.loadContentList();
            } else
                showWarning(MediaSelecter.Loading_directories_is_not_successful_, MediaSelecter.Warning);
        });

    };
    self.selectDirectory = function(data, object) {
        self.selectedNode(data);
        self.loadSubData(data, object);
    };
    self.loadContentList = function() {
        self.modalContent.contentList.removeAll();
        self.modalContent.isContentLoading(true);
        $.get(self.url(), { pageSize: self.pageSize(), page: self.pageNumber(), directoryId: self.selectedNode().id() }, function(returnData) {
            self.modalContent.contentList.removeAll();
            for (var i = 0; i <= returnData.Records.length - 1; i++) {
                var contentItem = new ContentModeItem(returnData.Records[i]);
                if (self.isContentSelected(contentItem))
                    contentItem.selected(true);
                else if (self.isNewSelected(contentItem))
                    contentItem.selected(true);

                self.modalContent.contentList.push(contentItem);
            }

            self.totalRecords(returnData.TotalRecords);
            self.totalPage(returnData.TotalPages);
            self.currentPage(returnData.CurrentPage);
            self.displayinRecords(returnData.DisplayingRecords);
            self.nextEnable(returnData.NextEnable);
            self.previousEnable(returnData.PreviousEnable);
            self.modalContent.isContentLoading(false);
            $("img.lazy").lazyload();
        });
    };
    self.modalContent = {
        header: MediaSelecter.Media_Contents,
        message: ko.observable(MediaSelecter.Please_select_the_content),
        actionLabel: MediaSelecter.Ok,
        closeLabel: MediaSelecter.Cancel,
        primaryLabel: MediaSelecter.Select,
        isContentLoading: ko.observable(false),
        contentList: ko.observableArray([]),
        selectedContent: ko.observableArray([]),
        show: ko.observable(false),
        onCancel: function() {
            self.modalContent.selectedContent.removeAll();
            self.modalContent.show(false);
        },
        onClose: function() {

        },
        onAction: function() {
            self.transferSelectContent();
            self.modalContent.show(false);
        },
        selectContent: function(content) {
            self.selectContent(content);
        }
    };
    self.selectContent = function(content) {
        if (self.isContentSelected(content) && !self.isNewSelected(content) && self.multiple()) {
            showWarning(MediaSelecter.Content_is_already_selected_please_delete_it_from_list, MediaSelecter.Warning);
            return;
        } else if (self.isNewSelected(content)) {
            content.selected(false);
            self.modalContent.selectedContent.remove(content);
            return;
        }

        if (self.multiple()) {
            content.selected(true);
            self.modalContent.selectedContent.push(content);
        } else {
            self.modalContent.selectedContent.removeAll();
            for (var i = 0; i <= self.modalContent.contentList().length - 1; i++)
                self.modalContent.contentList()[i].selected(false);

            content.selected(true);
            self.modalContent.selectedContent.push(content);
            self.transferSelectContent();
            self.modalContent.show(false);
        }
    };
    self.transferSelectContent = function() {
        if (!self.multiple()) {
            self.selectedContent.removeAll();
        }
        for (var i = 0; i <= self.modalContent.selectedContent().length - 1; i++) {
            self.selectedContent.push(self.modalContent.selectedContent()[i]);
        }
        self.selectedContentValues(joinMediaValues(self.selectedContent()));
        self.modalContent.selectedContent.removeAll();
    };
    self.removeSelected = function(content) {
        self.selectedContent.remove(content);
        for (var i = 0; i <= self.modalContent.contentList().length - 1; i++)
            self.modalContent.contentList()[i].selected(false);
    };
    self.showModal = function() {
        self.modalContent.selectedContent.removeAll();
        self.modalContent.message(MediaSelecter.Please_select_the_content);
        self.modalContent.show(true);
        self.loadDataDirectory();
    };
    self.isContentSelected = function(content) {
        return ko.utils.arrayFirst(self.selectedContent(), function(contentFound) {
            return contentFound.content().Id === content.content().Id;
        });
    };
    self.isNewSelected = function(content) {
        return ko.utils.arrayFirst(self.modalContent.selectedContent(), function(contentFound) {
            return contentFound.content().Id === content.content().Id;
        });
    };
    self.next = function() {
        if (self.nextEnable()) {
            self.pageNumber(self.pageNumber() + 1);
            self.loadContentList();
        }
    }.bind(self);
    self.prev = function() {
        if (self.previousEnable()) {
            self.pageNumber(self.pageNumber() - 1);
            self.loadContentList();
        }
    }.bind(self);
    self.pageInfo = ko.computed(function() {
        return MediaSelecter.Showing + " " + (self.currentPage() + 1) + " / " + self.totalPage();
    }, self);
    self.canDrop = function(parent) {
        return false;
    };
    ko.bindingHandlers.modalContent = {
        init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var props = valueAccessor(),
                vm = bindingContext.createChildContext(viewModel);
            ko.utils.extend(vm, props);
            vm.close = function() {
                vm.show(false);
                vm.onClose();
            };
            vm.action = function() {
                vm.onAction();
            };
            ko.utils.toggleDomNodeCssClass(element, "modal fade", true);
            ko.renderTemplate(self.contentListTemplate(), vm, null, element);
            var showHide = ko.computed(function() {
                $(element).modal(vm.show() ? "show" : "hide");
            });
            return {
                controlsDescendantBindings: true
            };
        }
    };

}

function DirectoryItem(data, baseModel) {
    var self = this;
    self.id = ko.observable(data.Id);
    self.name = ko.observable(data.Name);
    self.path = ko.observable(data.Path);
    self.parentId = ko.observable(data.ParentId);
    self.baseModel = ko.observable(baseModel);
    self.directories = ko.observableArray([]);
}

function IsDirectoryAdded(directories, id) {
    return ko.utils.arrayFirst(directories, function(directoryFound) {
        return directoryFound.id() === id;
    });
}

function ContentModeItem(data) {
    var self = this;
    self.content = ko.observable(data);
    self.selected = ko.observable(false);
}

function joinMediaValues(array) {
    var res = "";
    for (var i = 0; i <= array.length - 1; i++) {
        if (i === array.length - 1)
            res += array[i].content().Id;
        else
            res += array[i].content().Id + ",";
    }
    return res;
}

function prepeareTreeViewForContents() {
    $(".tree > ul").attr("role", "tree").find("ul").attr("role", "group");
    $(".tree").find("li:has(ul)").find(" > span").off("click");
    $(".tree").find("li:has(ul)").addClass("parent_li").attr("role", "treeitem").find(" > span").attr("title", MediaSelecter.Collapse_this_folder).on("click", function(e) {
        openTreeForContent($(this));
        e.stopPropagation();
    });

    $(".directoryLabel").on("click", function(e) {
        $(".directoryLabel").removeClass("btn-sm btn-primary");
        $(this).addClass("btn-sm btn-primary");
    });
}

function openTreeForContent(element) {
    var children = element.parent("li.parent_li").find(" > ul > li");
    if (children.is(":visible")) {
        children.hide("fast");
        element.attr("title", MediaSelecter.Expand_this_folder).find(" > i").removeClass("fa fa-folder-open").addClass("fa fa-folder");
    } else {
        children.show("fast");
        element.attr("title", MediaSelecter.Collapse_this_folder).find(" > i").removeClass("fa fa-folder").addClass("fa fa-folder-open");
    }
}

function addDirectories(baseDirectory, directories, model) {

    for (var i = 0; i < directories.length; i++) {
        var directory = directories[i];
        var directoryModel = new Directory(directory, model);
        baseDirectory.push(directoryModel);
        addDirectories(directoryModel.directories, directory.Directories, directoryModel);
    }
}