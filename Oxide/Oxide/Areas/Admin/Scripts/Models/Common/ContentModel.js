﻿
function ContentModel(elementId, url, contentName, multiple, filterCount, selectedContentsModels) {
    var self = this;
    self.elementId = ko.observable(elementId);
    self.contentListTemplate = ko.observable();
    self.contentSelectListTemplate = ko.observable();
    self.contentItemTemplate = ko.observable();
    self.multiple = ko.observable();
    self.filter = ko.observable("");
    self.contentName = ko.observable(contentName);
    self.filterCount = ko.observable(filterCount);
    self.url = ko.observable(url);
    self.selectedContent = ko.observableArray([]);
    self.selectedContentValues = ko.observable();
    self.deleteButtonTitle = ko.observable(ContentSelecter.Delete);
    self.searchInfo = ko.observable(ContentSelecter.Search_for___);
    self.searchButton = ko.observable(ContentSelecter.Search);
    self.selectedContent.subscribe(function() {
        self.selectedContentValues(joinValues(self.selectedContent()));
    });

    //Load selected content models
    self.selectedContent(ko.utils.arrayMap(selectedContentsModels, function(item) {
        var newItem = new ContentItem(item);
        newItem.selected = true;
        return newItem;
    }));

    //for (var i = selectedContentsModels.length; i--;) {
    //    var contentItem = new ContentItem(JSON.parse(JSON.stringify(selectedContentsModels[i])));
    //    contentItem.selected(true);
    //    self.selectedContent.push(contentItem);
    //}

    if (multiple.toLowerCase() === "true")
        self.multiple(true);
    else
        self.multiple(false);

    self.loadContentList = function(filter) {
        self.modalContent.isContentLoading(true);
        $.get(self.url(), { contentName: self.contentName(), filter: filter, filterCount: self.filterCount() }, function(data) {
            self.modalContent.contentList.removeAll();
            for (var i = 0; i <= data.length - 1; i++) {
                var contentItem = new ContentItem(data[i]);
                if (self.isContentSelected(contentItem))
                    contentItem.selected(true);
                else if (self.isNewSelected(contentItem))
                    contentItem.selected(true);

                self.modalContent.contentList.push(contentItem);
            }
            self.modalContent.isContentLoading(false);
        });
    };
    self.modalContent = {
        header: ContentSelecter.Contents,
        message: ko.observable(ContentSelecter.Please_select_the_content),
        actionLabel: ContentSelecter.Ok,
        closeLabel: ContentSelecter.Cancel,
        primaryLabel: ContentSelecter.Select,
        isContentLoading: ko.observable(false),
        contentList: ko.observableArray([]),
        selectedContent: ko.observableArray([]),
        filterText: ko.observable(""),
        show: ko.observable(false),
        onCancel: function() {
            self.modalContent.selectedContent.removeAll();
            self.modalContent.show(false);
        },
        onClose: function() {

        },
        onAction: function() {
            self.transferSelectContent();
            self.modalContent.show(false);
        },
        selectContent: function(content) {
            self.selectContent(content);
        }
    };
    self.selectContent = function(content) {
        if (self.isContentSelected(content) && !self.isNewSelected(content) && self.multiple()) {
            showWarning(ContentSelecter.Content_is_already_selected_please_delete_it_from_list, ContentSelecter.Warning);
            return;
        } else if (self.isNewSelected(content)) {
            content.selected(false);
            self.modalContent.selectedContent.remove(content);
            return;
        }
        content.selected(true);
        if (self.multiple()) {
            self.modalContent.selectedContent.push(content);
        } else {
            self.modalContent.selectedContent.removeAll();
            self.modalContent.selectedContent.push(content);
            self.transferSelectContent();
            self.modalContent.show(false);
        }
    };
    self.transferSelectContent = function() {
        if (!self.multiple()) {
            self.selectedContent.removeAll();
        }

        for (var i = 0; i <= self.modalContent.selectedContent().length - 1; i++) {
            self.selectedContent.push(self.modalContent.selectedContent()[i]);
        }
        self.selectedContentValues(joinValues(self.selectedContent()));
        self.modalContent.selectedContent.removeAll();
    };
    self.removeSelected = function(content) {
        self.selectedContent.remove(content);
    };
    self.filter = function(filter) {
        self.loadContentList(filter);
    };
    self.showModal = function() {
        self.modalContent.message(ContentSelecter.Please_select_the_content);
        self.modalContent.show(true);
        self.loadContentList("");
    };
    self.isContentSelected = function(content) {
        return ko.utils.arrayFirst(self.selectedContent(), function(contentFound) {
            return contentFound.content().Id === content.content().Id;
        });
    };
    self.isNewSelected = function(content) {
        return ko.utils.arrayFirst(self.modalContent.selectedContent(), function(contentFound) {
            return contentFound.content().Id === content.content().Id;
        });
    };
    self.canDrop = function(parent) {
        return false;
    };
    ko.bindingHandlers.modalContent = {
        init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var props = valueAccessor(),
                vm = bindingContext.createChildContext(viewModel);
            ko.utils.extend(vm, props);
            vm.close = function() {
                vm.show(false);
                vm.onClose();
            };
            vm.action = function() {
                vm.onAction();
            };
            ko.utils.toggleDomNodeCssClass(element, "modal fade", true);
            ko.renderTemplate(self.contentListTemplate(), vm, null, element);
            var showHide = ko.computed(function() {
                $(element).modal(vm.show() ? "show" : "hide");
            });
            return {
                controlsDescendantBindings: true
            };
        }
    };
}

function ContentItem(data) {
    var self = this;
    self.content = ko.observable(data);
    self.selected = ko.observable(false);

}

function joinValues(array) {
    var res = "";
    for (var i = 0; i <= array.length - 1; i++) {
        if (i === array.length - 1)
            res += array[i].content().Id;
        else
            res += array[i].content().Id + ",";
    }
    return res;
}