﻿var mainContentModel;

function ImageContentModel(elementId, url, contentName, multiple, filterCount, selectedContentsModels, imageField, field1, field2, withPictures) {
    var self = this;
    self.elementId = ko.observable(elementId);
    self.contentListTemplate = ko.observable();
    self.contentSelectListTemplate = ko.observable();
    self.contentItemTemplate = ko.observable();
    self.multiple = ko.observable();
    self.filter = ko.observable("");
    self.contentName = ko.observable(contentName);
    self.filterCount = ko.observable(filterCount);
    self.url = ko.observable(url);
    self.selectedContent = ko.observableArray([]);
    self.selectedContentValues = ko.observable();
    self.deleteButtonTitle = ko.observable(ImageContentSelecter.Delete);
    self.searchInfo = ko.observable(ImageContentSelecter.Search_for___);
    self.searchButton = ko.observable(ImageContentSelecter.Search);
    self.pictureEnabled = ko.observable();
    self.fieldImage = ko.observable(imageField);
    self.fieldName1 = ko.observable(field1);
    self.fieldName2 = ko.observable(field2);
    self.selectedContent.subscribe(function() {
        self.selectedContentValues(joinValuesForImageContents(self.selectedContent()));
    });
    mainContentModel = this;
    //Load selected content models
    self.selectedContent(ko.utils.arrayMap(selectedContentsModels, function(item) {
        var newItem = new ImageContent(item);
        newItem.selected = true;
        return newItem;
    }));

    if (multiple.toLowerCase() === "true")
        self.multiple(true);
    else
        self.multiple(false);

    if (withPictures.toLowerCase() === "true")
        self.pictureEnabled(true);
    else
        self.pictureEnabled(false);

    self.loadContentList = function(filter) {
        self.modalContent.isContentLoading(true);
        $.get(self.url(), { contentName: self.contentName(), filter: filter, imageField: self.fieldImage(), filterCount: self.filterCount() }, function(data) {
            self.modalContent.contentList.removeAll();
            self.modalContent.contentList(ko.utils.arrayMap(data, function(item) {
                var newItem = new ImageContent(item);
                if (self.isContentSelected(newItem))
                    newItem.selected(true);
                else if (self.isNewSelected(newItem))
                    newItem.selected(true);
                return newItem;
            }));
            self.modalContent.isContentLoading(false);
        });
    };
    self.modalContent = {
        header: ImageContentSelecter.Contents,
        message: ko.observable(ImageContentSelecter.Please_select_the_content),
        actionLabel: ImageContentSelecter.Ok,
        closeLabel: ImageContentSelecter.Cancel,
        primaryLabel: ImageContentSelecter.Select,
        isContentLoading: ko.observable(false),
        pictureEnabled: self.pictureEnabled(),
        contentList: ko.observableArray([]),
        selectedContent: ko.observableArray([]),
        filterText: ko.observable(""),
        show: ko.observable(false),
        onCancel: function() {
            self.modalContent.selectedContent.removeAll();
            self.modalContent.show(false);
        },
        onClose: function() {

        },
        onAction: function() {
            self.transferSelectContent();
            self.modalContent.show(false);
        },
        selectContent: function(content) {
            self.selectContent(content);
        }
    };
    self.selectContent = function(content) {
        if (self.isContentSelected(content) && !self.isNewSelected(content) && self.multiple()) {
            showWarning(ImageContentSelecter.Content_is_already_selected_please_delete_it_from_list, ImageContentSelecter.Warning);
            return;
        } else if (self.isNewSelected(content)) {
            content.selected(false);
            self.modalContent.selectedContent.remove(content);
            return;
        }
        content.selected(true);
        if (self.multiple()) {
            self.modalContent.selectedContent.push(content);
        } else {
            self.modalContent.selectedContent.removeAll();
            self.modalContent.selectedContent.push(content);
            self.transferSelectContent();
            self.modalContent.show(false);
        }
    };
    self.transferSelectContent = function() {
        if (!self.multiple()) {
            self.selectedContent.removeAll();
        }

        for (var i = 0; i <= self.modalContent.selectedContent().length - 1; i++) {
            self.selectedContent.push(self.modalContent.selectedContent()[i]);
        }
        self.selectedContentValues(joinValuesForImageContents(self.selectedContent()));
        self.modalContent.selectedContent.removeAll();
    };
    self.removeSelected = function(content) {
        self.selectedContent.remove(content);
    };
    self.filter = function(filter) {
        self.loadContentList(filter);
    };
    self.showModal = function() {
        self.modalContent.message(ImageContentSelecter.Please_select_the_content);
        self.modalContent.show(true);
        self.loadContentList("");
    };
    self.isContentSelected = function(content) {
        return ko.utils.arrayFirst(self.selectedContent(), function(contentFound) {
            return contentFound.Id() === content.Id();
        });
    };
    self.isNewSelected = function(content) {
        return ko.utils.arrayFirst(self.modalContent.selectedContent(), function(contentFound) {
            return contentFound.Id() === content.Id();
        });
    };
    this.getFieldName1 = function() {
        return self.fieldName1();
    };
    self.canDrop = function(parent) {
        return false;
    };
    ko.bindingHandlers.modalContent = {
        init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var props = valueAccessor(),
                vm = bindingContext.createChildContext(viewModel);
            ko.utils.extend(vm, props);
            vm.close = function() {
                vm.show(false);
                vm.onClose();
            };
            vm.action = function() {
                vm.onAction();
            };
            ko.utils.toggleDomNodeCssClass(element, "modal fade", true);
            ko.renderTemplate(self.contentListTemplate(), vm, null, element);
            var showHide = ko.computed(function() {
                $(element).modal(vm.show() ? "show" : "hide");
            });
            return {
                controlsDescendantBindings: true
            };
        }
    };

}

function ImageContent(data) {
    var self = this;
    self.selected = ko.observable(false);
    ko.utils.arrayForEach(data, function(pair) {
        self[pair.Key] = ko.observable(pair.Value);
    });
    self.field1 = self[mainContentModel.fieldName1()];
    self.field2 = self[mainContentModel.fieldName2()];
}

function joinValuesForImageContents(array) {
    var res = "";
    for (var i = 0; i <= array.length - 1; i++) {
        if (i === array.length - 1)
            res += array[i].Id();
        else
            res += array[i].Id() + ",";
    }
    return res;
}