﻿
ko.bindingHandlers.formattedTime = {
    update: function(element, valueAccessor) {
        $(element).text(moment(ko.unwrap(valueAccessor())).format("DD.MM.YYYY HH:mm:ss"));
    }
};

ko.bindingHandlers.bootstrapModalDataList = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var props = valueAccessor(),
            vm = bindingContext.createChildContext(viewModel);
        ko.utils.extend(vm, props);
        vm.close = function() {
            vm.show(false);
            vm.onClose();
        };
        vm.action = function() {
            vm.onAction();
        };
        ko.utils.toggleDomNodeCssClass(element, "modal fade", true);
        ko.renderTemplate("deleteConfirmTemplate", vm, null, element);
        var showHide = ko.computed(function() {
            $(element).modal(vm.show() ? "show" : "hide");
        });
        return {
            controlsDescendantBindings: true
        };
    }
};

function DataListModel(urlBase, urlEdit, urlDelete, contentName, withPicture, pictureField) {
    var self = this;
    self.isLoading = ko.observable(false);
    self.totalRecords = ko.observable();
    self.totalPage = ko.observable();
    self.currentPage = ko.observable();
    self.displayinRecords = ko.observable();
    self.nextEnable = ko.observable(true);
    self.previousEnable = ko.observable(true);
    self.contents = ko.observableArray([]);
    self.pageSize = ko.observable();
    self.actionFilter = ko.observable("");
    self.pictureEnabled = ko.observable(withPicture);
    self.fieldOfPicture = ko.observable(pictureField);
    self.actionCurrentPage = ko.observable(0);
    self.actionPageSize = ko.observable(10);
    self.contentName = ko.observable(contentName);
    self.loadData = function(contentName, pagesize, page, filter) {
        self.isLoading(true);
        $.get(urlBase, { contentName: contentName, pageSize: pagesize, page: page, filter: filter, withPicture: self.pictureEnabled(), pictureField: self.fieldOfPicture() }, function(data) {
            self.contents.removeAll();
            self.contents(ko.utils.arrayMap(data.Records, function(item) {
                return new Content(item);
            }));

            self.totalRecords(data.TotalRecords);
            self.totalPage(data.TotalPages);
            self.currentPage(data.CurrentPage);
            self.displayinRecords(data.DisplayingRecords);
            self.nextEnable(data.NextEnable);
            self.previousEnable(data.PreviousEnable);
            self.setControls();
            self.isLoading(false);
        });
    };
    self.addContent = function(model) {
        self.contents.push(model);
    }.bind(self);
    self.removeContent = function(id) {
        self.contents.remove(function(content) {
            return content.Id === id;
        });
    }.bind(self);
    self.deleteContent = function(model) {
        self.isLoading(true);
        $.get(urlDelete, { id: model.Id }, function() {
            self.isLoading(false);
            self.removeContent(model.Id);
        });
    }.bind(self);

    self.modal = {
        header: DataList.Please_Confirm,
        message: ko.observable(DataList.Do_you_want_to_delete_selected_content_),
        comment: ko.observable(""),
        closeLabel: DataList.Cancel,
        primaryLabel: DataList.Delete,
        selectedContent: ko.observable(),
        show: ko.observable(false),
        onClose: function() {
            self.onModalClose();
        },
        onAction: function() {
            self.onModalAction();
        }
    };
    self.showModal = function(modal) {
        self.modal.message(DataList.Do_you_want_to_delete_selected_content_);
        self.modal.selectedContent(modal);
        self.modal.show(true);
    };
    self.onModalClose = function() {

    };
    self.onModalAction = function() {
        self.deleteContent(self.modal.selectedContent());
        self.modal.show(false);
    };
    self.setControls = function() {
        $("#datatable-checkbox_info").html(DataList.Showing + " " + (self.currentPage() + 1) + " / " + self.totalPage());
        $(".dataTables_empty").html("");
        $("#datatable-checkbox_next").unbind();
        $("#datatable-checkbox_previous").unbind();
        $("#datatable-checkbox_next").addClass("disabled");
        $("#datatable-checkbox_previous").addClass("disabled");

        if (self.nextEnable()) {
            $("#datatable-checkbox_next").removeClass("disabled");
            $("#datatable-checkbox_next").click(function() {
                self.actionCurrentPage(self.actionCurrentPage() + 1);
                self.loadData(self.contentName, self.actionPageSize(), self.actionCurrentPage(), self.actionFilter(), self.pictureEnabled(), self.fieldOfPicture());
            });
        }
        if (self.previousEnable()) {
            $("#datatable-checkbox_previous").removeClass("disabled");
            $("#datatable-checkbox_previous").click(function() {
                self.actionCurrentPage(self.actionCurrentPage() - 1);
                self.loadData(self.contentName, self.actionPageSize(), self.actionCurrentPage(), self.actionFilter(), self.pictureEnabled(), self.fieldOfPicture());
            });
        }
    }.bind(self);

    this.getEditUl = function(id) {
        return urlEdit + "/" + id;
    };

}

function Content(data) {
    var self = this;
    // assuming item is an array of key/value pairs
    ko.utils.arrayForEach(data, function(pair) {
        // doesn't necessarily have to be mapped as an observable
        self[pair.Key] = ko.observable(pair.Value);
    });
}