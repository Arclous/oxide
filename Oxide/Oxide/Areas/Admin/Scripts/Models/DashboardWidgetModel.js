﻿
function DashboardWidgetsModel(urlBase) {
    var self = this;
    self.isLoading = ko.observable(false);
    self.filter = ko.observable("");
    self.widgets = ko.observableArray([]);
    self.loadData = function(filter) {
        self.isLoading(true);
        $.get(urlBase, { filter: filter }, function(data) {
            self.widgets.removeAll();
            for (var i = 0; i <= data.Records.length - 1; i++) {
                var widgetModel = new WidgetModel(
                    data.Records[i].Id,
                    data.Records[i].Name,
                    data.Records[i].Title,
                    data.Records[i].Description,
                    data.Records[i].Category,
                    data.Records[i].PreviewImageUrl,
                    data.Records[i].Active,
                    data.Records[i].ChangeStatusUrl,
                    data.Records[i].InfoUrl,
                    data.Records[i].WidgetKey
                );
                self.addWidget(widgetModel);
            }
            self.isLoading(false);
        });
    };
    self.addWidget = function(model) {
        self.widgets.push(model);
    }.bind(self);
}

function WidgetModel(id, name, title, description, category, previewImageUrl, active, changeStatusUrl, infoUrl, widgetKey) {
    var self = this;
    self.isActivating = ko.observable(false);
    self.id = ko.observable(id);
    self.name = ko.observable(name);
    self.title = ko.observable(title);
    self.description = ko.observable(description);
    self.category = ko.observable(category);
    self.previewImageUrl = ko.observable(previewImageUrl);
    self.active = ko.observable(active);
    self.changeStatusUrl = ko.observable(changeStatusUrl);
    self.infoUrl = ko.observable(infoUrl);
    self.widgetKey = ko.observable(widgetKey);

    self.activateButton = ko.observable(false);
    self.deActivateButton = ko.observable(true);
    if (self.active()) {
        self.activateButton(false);
        self.deActivateButton(true);
    } else {
        self.activateButton(true);
        self.deActivateButton(false);
    }
    self.changeStatus = function() {
        self.isActivating(true);
        $.get(changeStatusUrl, { widgetKey: self.widgetKey, status: self.active() }, function(data) {
            self.active(data.Active);
            if (self.active()) {
                self.activateButton(false);
                self.deActivateButton(true);
            } else {
                self.activateButton(true);
                self.deActivateButton(false);
            }
            self.isActivating(false);
        });
    };
}