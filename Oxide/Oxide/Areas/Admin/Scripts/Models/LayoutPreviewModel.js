﻿ko.bindingHandlers.bootstrapModal = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var props = valueAccessor(),
            vm = bindingContext.createChildContext(viewModel);
        ko.utils.extend(vm, props);
        vm.close = function() {
            vm.show(false);
            vm.onClose();
        };
        vm.action = function() {
            vm.onAction();
        };
        ko.utils.toggleDomNodeCssClass(element, "modal fade", true);
        ko.renderTemplate("imagePreviewTemplate", vm, null, element);
        var showHide = ko.computed(function() {
            $(element).modal(vm.show() ? "show" : "hide");
        });
        return {
            controlsDescendantBindings: true
        };
    }
};

function LayoutsPreviewModel(previewLoadUrl) {
    var self = this;
    self.isLoading = ko.observable(false);
    self.layoutToDisplay = ko.observable();

    self.loadPrivewImage = function(type) {
        self.isLoading(true);
        $.get(previewLoadUrl, { type: type, id: self.layoutToDisplay() }, function(data) {
            self.modal.previewImage(data);
            self.modal.show(true);
            $("img.lazy").lazyload();
            self.isLoading(false);
        });
    };

    self.modal = {
        previewImage: ko.observable(""),
        closeLabel: NewPage.Ok,
        show: ko.observable(false),
        onClose: function() {
            self.onModalClose();
        },
        onAction: function() {
            self.onModalAction();
        }
    };
    self.showModal = function(type) {
        if (type === 0)
            self.layoutToDisplay($("#mainLayoutId").val());
        else if (type === 1)
            self.layoutToDisplay($("#innerLayoutId").val());

        if (self.layoutToDisplay() === "") return;
        self.loadPrivewImage(type);

    };
    self.onModalClose = function() {

    };
    self.onModalAction = function() {
        self.modal.show(false);
    };
}