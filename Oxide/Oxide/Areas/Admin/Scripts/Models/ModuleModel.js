﻿
function ModulesModel(urlBase) {
    var self = this;
    self.isLoading = ko.observable(false);
    self.filter = ko.observable("");
    self.modules = ko.observableArray([]);
    self.loadData = function(filter) {
        self.isLoading(true);
        $.get(urlBase, { filter: filter }, function(data) {
            self.modules.removeAll();
            for (var i = 0; i <= data.Records.length - 1; i++) {
                var moduleModel = new ModuleModel(
                    data.Records[i].Id,
                    data.Records[i].Name,
                    data.Records[i].Title,
                    data.Records[i].Description,
                    data.Records[i].Category,
                    data.Records[i].PreviewImageUrl,
                    data.Records[i].Active,
                    data.Records[i].ChangeStatusUrl,
                    data.Records[i].InfoUrl,
                    data.Records[i].Dependencies,
                    self
                );
                self.addModule(moduleModel);
            }
            self.isLoading(false);
        });
    };
    self.addModule = function(model) {
        self.modules.push(model);
    }.bind(self);
    self.activateModule = function(name) {
        var targetModule = ko.utils.arrayFirst(self.modules(), function(module) {
            return module.name() === name;
        });
        targetModule.active(true);
        targetModule.activateButton(false);
        targetModule.deActivateButton(true);
    }.bind(self);
}

function ModuleModel(id, name, title, description, category, previewImageUrl, active, changeStatusUrl, infoUrl, dependencies, parent) {
    var self = this;
    self.isActivating = ko.observable(false);
    self.id = ko.observable(id);
    self.name = ko.observable(name);
    self.title = ko.observable(title);
    self.description = ko.observable(description);
    self.category = ko.observable(category);
    self.previewImageUrl = ko.observable(previewImageUrl);
    self.active = ko.observable(active);
    self.changeStatusUrl = ko.observable(changeStatusUrl);
    self.infoUrl = ko.observable(infoUrl);
    self.dependencies = ko.observableArray([]);
    self.activateButton = ko.observable(false);
    self.deActivateButton = ko.observable(true);
    self.parent = ko.observable(parent);
    for (var i = 0; i <= dependencies.length - 1; i++) {
        self.dependencies.push(dependencies[i]);
    }

    if (self.active()) {
        self.activateButton(false);
        self.deActivateButton(true);
    } else {
        self.activateButton(true);
        self.deActivateButton(false);
    }
    self.changeStatus = function() {
        self.isActivating(true);
        $.get(changeStatusUrl, { moduleName: self.name, status: self.active() }, function(data) {
            self.active(data.Active);
            if (self.active()) {
                self.activateButton(false);
                self.deActivateButton(true);

                if (self.dependencies().length > 0) {
                    for (var i = 0; i <= self.dependencies().length - 1; i++) {
                        self.parent().activateModule(self.dependencies()[i]);
                    }
                }

            } else {
                self.activateButton(true);
                self.deActivateButton(false);
            }
            self.isActivating(false);
        });
    };
}