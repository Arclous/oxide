﻿ko.bindingHandlers.multiSelection = {
    init: function(element, valueAccessor, allBindingsAccessor) {
        //initialize bootstrapSwitch
        $(element).bootstrapSwitch();

        // setting initial value
        $(element).bootstrapSwitch("state", valueAccessor()());

        //handle the field changing
        $(element).on("switchChange.bootstrapSwitch", function(event, state) {
            var observable = valueAccessor();
            observable(state);
        });

        // Adding component options
        var options = allBindingsAccessor().bootstrapSwitchOptions || {};
        for (var property in options) {
            $(element).bootstrapSwitch(property, ko.utils.unwrapObservable(options[property]));
        }

        //handle disposal (if KO removes by the template binding)
        ko.utils.domNodeDisposal.addDisposeCallback(element, function() {
            $(element).bootstrapSwitch("destroy");
        });

    },
    //update the control when the view model changes
    update: function(element, valueAccessor, allBindingsAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        // Adding component options
        var options = allBindingsAccessor().bootstrapSwitchOptions || {};
        for (var property in options) {
            $(element).bootstrapSwitch(property, ko.utils.unwrapObservable(options[property]));
        }

        $(element).bootstrapSwitch("state", value);
    }
};

function AdminPermissionsModel(urlBase, updateUrl, roleId) {
    var self = this;
    self.isLoading = ko.observable(false);
    self.owners = ko.observableArray([]);
    self.roleId = ko.observable(roleId);
    self.updateUrl = ko.observable(updateUrl);
    self.loadPermissions = function() {
        self.isLoading(true);
        $.get(urlBase, { roleId: roleId }, function(data) {
            self.owners.removeAll();
            for (var i = 0; i <= data.length - 1; i++) {
                var owner = new AdminOwner(data[i].Name);

                for (var grpIndex = 0; grpIndex <= data[i].Groups.length - 1; grpIndex++) {
                    var group = new AdminGroup(data[i].Groups[grpIndex].Name);

                    for (var prsIndex = 0; prsIndex <= data[i].Groups[grpIndex].Permissions.length - 1; prsIndex++) {
                        var permission = new AdminPermissions(
                            data[i].Groups[grpIndex].Permissions[prsIndex].Id,
                            data[i].Groups[grpIndex].Permissions[prsIndex].Name,
                            data[i].Groups[grpIndex].Permissions[prsIndex].Title,
                            data[i].Groups[grpIndex].Permissions[prsIndex].Key,
                            data[i].Groups[grpIndex].Permissions[prsIndex].Owner,
                            data[i].Groups[grpIndex].Permissions[prsIndex].Group,
                            data[i].Groups[grpIndex].Permissions[prsIndex].Status,
                            self
                        );

                        group.addPermission(permission);

                    }

                    owner.addGroups(group);
                }

                self.addOwner(owner);
            }
            self.isLoading(false);
            $("[class='multiSelection']").bootstrapSwitch();
        });
    };

    self.addOwner = function(model) {
        self.owners.push(model);
    }.bind(self);

    self.updatePermission = function(model) {
        self.isLoading(true);
        $.get(updateUrl, { roleId: self.roleId(), permissionId: model.id(), status: model.status() }, function(data) {
            self.isLoading(false);
        });

    }.bind(self);
}

function AdminOwner(title) {
    var self = this;
    self.title = ko.observable(title);
    self.groups = ko.observableArray([]);

    self.addGroups = function(model) {
        self.groups.push(model);
    }.bind(self);
}

function AdminGroup(title) {
    var self = this;
    self.title = ko.observable(title);
    self.permissions = ko.observableArray([]);

    self.addPermission = function(model) {
        self.permissions.push(model);
    }.bind(self);
}

function AdminPermissions(id, name, title, key, owner, group, status, parentModel) {
    var self = this;
    self.updateStatus = ko.observable();
    self.id = ko.observable(id);
    self.name = ko.observable(name);
    self.title = ko.observable(title);
    self.owner = ko.observable(owner);
    self.key = ko.observable(key);
    self.group = ko.observable(group);
    self.status = ko.observable(status);
    self.parentModel = ko.observable(parentModel);
    self.status.subscribe(function() {
        self.updateStatus(ManagePermissions.updating___);
        $.get(parentModel.updateUrl(), { roleId: parentModel.roleId(), permissionId: self.id(), status: self.status() }, function(data) {
            self.updateStatus(ManagePermissions.updated);
        });
    });

}