﻿ko.bindingHandlers.bootstrapModal = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var props = valueAccessor(),
            vm = bindingContext.createChildContext(viewModel);
        ko.utils.extend(vm, props);
        vm.close = function() {
            vm.show(false);
            vm.onClose();
        };
        vm.action = function() {
            vm.onAction();
        };
        ko.utils.toggleDomNodeCssClass(element, "modal fade", true);
        ko.renderTemplate("widgetDashboardListTemplate", vm, null, element);
        var showHide = ko.computed(function() {
            $(element).modal(vm.show() ? "show" : "hide");
        });
        return {
            controlsDescendantBindings: true
        };
    }
};
ko.bindingHandlers.confirmModal = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var props = valueAccessor(),
            vm = bindingContext.createChildContext(viewModel);
        ko.utils.extend(vm, props);
        vm.close = function() {
            vm.show(false);
            vm.onClose();
        };
        vm.action = function() {
            vm.onAction();
        };
        ko.utils.toggleDomNodeCssClass(element, "modal fade", true);
        ko.renderTemplate("deleteConfirmTemplate", vm, null, element);
        var showHide = ko.computed(function() {
            $(element).modal(vm.show() ? "show" : "hide");
        });
        return {
            controlsDescendantBindings: true
        };
    }
};

function DashboardModel(urlDashboardLoad, urlLoadWidgets, urlAddWidget, urlDeleteWidget, orderUpdateUrl) {
    var self = this;
    self.isLoading = ko.observable(false);
    self.widgets = ko.observableArray([]);

    self.loadWidgets = function() {
        self.isLoading(true);
        $.get(urlDashboardLoad, {}, function(data) {
            for (var i = 0; i <= data.length - 1; i++) {
                var dashboardWidgetModel = new DashboardWidgetModel(
                    data[i].Id,
                    data[i].Title,
                    data[i].Name,
                    data[i].WidgetKey,
                    data[i].UserId,
                    data[i].DashboardWidgetRunUrl,
                    data[i].Manageable,
                    data[i].Description,
                    data[i].PreviewImageUrl,
                    data[i].Category,
                    data[i].ManageDashboardWidgetUrl,
                    true
                );
                self.addWidget(dashboardWidgetModel);
            }
            self.isLoading(false);
        });
    };
    self.addWidget = function(model) {
        self.widgets.push(model);
    }.bind(self);
    //Widget modal window
    self.modal = {
        header: Dashboard.Widgets,
        message: ko.observable(Dashboard.Please_select_the_widget_which_you_want_to_add_to_page),
        closeLabel: Dashboard.Cancel,
        primaryLabel: Dashboard.Add_Widget,
        searchLabel: Dashboard.Search,
        searchLabelPlaceHolder: Dashboard.Search_for___,
        widgetList: ko.observableArray([]),
        isWidgetLoading: ko.observable(false),
        filterText: ko.observable(""),
        selectedElement: ko.observable(),
        pageId: ko.observable(),
        layoutId: ko.observable(),
        elementId: ko.observable(),
        show: ko.observable(false),
        onClose: function() {
            self.onModalClose();
        },
        loadWidgetList: function() {
            self.modal.loadWidgets(self.filterText);
        },
        addWidget: function(widget) {
            self.addWidgetToDashboard(widget);
        }
    };
    self.showModal = function(element) {
        self.modal.message(Dashboard.Please_select_the_widget_which_you_want_to_add_to_page);
        self.modal.loadWidgetList();
        self.modal.show(true);
    };
    self.onModalClose = function() {
    };
    self.filter = function(filter) {
        self.modal.loadWidgets(filter);
    };
    self.addWidgetToDashboard = function(widget) {
        self.addWidgetToPage(widget);
        self.modal.show(false);
    };
    self.modal.loadWidgets = function(filter) {
        $.get(urlLoadWidgets, { filter: filter }, function(data) {
            self.modal.isWidgetLoading(true);
            self.modal.widgetList.removeAll();
            for (var i = 0; i <= data.length - 1; i++) {
                var dashboardWidgetModel = new DashboardWidgetModel(
                    0,
                    data[i].Title,
                    data[i].Name,
                    data[i].WidgetKey,
                    data[i].UserId,
                    data[i].DashboardWidgetRunUrl,
                    data[i].Manageable,
                    data[i].Description,
                    data[i].PreviewImageUrl,
                    data[i].Category,
                    data[i].ManageDashboardWidgetUrl,
                    false
                );
                self.modal.widgetList.push(dashboardWidgetModel);
            }
            self.modal.isWidgetLoading(false);
        });
    };
    self.addWidgetToPage = function(widget) {
        self.isLoading(true);
        $.get(urlAddWidget, { widgetKey: widget.widgetKey() }, function(data) {
            self.isLoading(false);
            widget.id(data.Id);
            widget.manageUrl(data.ManageUrl);
            widget.run(true);
            self.addWidget(widget);
        });
    };

    //Confirm modal window
    self.confirmModal = {
        header: Dashboard.Please_Confirm,
        message: ko.observable(Dashboard.Do_you_want_to_delete_widget_from_page),
        closeLabel: Dashboard.Cancel,
        primaryLabel: Dashboard.Delete_Widget,
        selectedWidget: ko.observable(),
        show: ko.observable(false),
        onClose: function() {
            self.onModalConfrimClose();
        },
        onAction: function() {
            self.deleteWidget();
        }
    };
    self.showConfirmModal = function(widget) {
        self.confirmModal.message(Dashboard.Do_you_want_to_delete_widget_from_page);
        self.confirmModal.selectedWidget = widget;
        self.confirmModal.show(true);
    };
    self.onModalConfrimClose = function() {
    };
    self.deleteWidget = function() {

        self.deleteWidgetFromPage(self.confirmModal.selectedWidget);
        self.confirmModal.show(false);
    };
    self.deleteWidgetFromPage = function(widget) {
        self.isLoading(true);
        $.get(urlDeleteWidget, { id: widget.id() }, function(data) {
            self.isLoading(false);
            self.widgets.remove(widget);
        });
    };

    self.afterMove = function(args) {
        self.updateOrderValue(args.item.userId(), args.item.id());
    };

    self.beforeMove = function(args) {
    };

    self.updateOrderValue = function(userId, widgetId) {
        self.isLoading(true);
        $.get(orderUpdateUrl, { widgetId: widgetId, orderList: joinDashboardWidgetValues(self.widgets()) }, function(data) {
            self.isLoading(false);
        });
    };
}

function DashboardWidgetModel(id, title, name, widgetKey, userId, runWidgetUrl, manageable, description, previewImage, category, manageUrl, run) {
    var self = this;
    self.isActivating = ko.observable(false);
    self.id = ko.observable(id);
    self.title = ko.observable(title);
    self.name = ko.observable(name);
    self.widgetKey = ko.observable(widgetKey);
    self.userId = ko.observable(userId);
    self.manageable = ko.observable(manageable);
    self.description = ko.observable(description);
    self.previewImageUrl = ko.observable(previewImage);
    self.category = ko.observable(category);
    self.manageUrl = ko.observable(manageUrl);
    self.manageUrl = ko.observable(manageUrl);
    self.data = ko.observable();
    self.run = ko.observable(run);

    self.loadWidgetData = function() {
        self.isActivating(true);
        $.get(runWidgetUrl, { widgetKey: self.widgetKey, widgetId: self.id }, function(data) {
            self.data(data);
            self.isActivating(false);
        });
    };
    if (self.run())
        self.loadWidgetData();
}

function joinDashboardWidgetValues(array) {
    var res = "";
    for (var i = 0; i <= array.length - 1; i++) {
        if (i === array.length - 1)
            res += array[i].id();
        else
            res += array[i].id() + ",";
    }
    return res;
}