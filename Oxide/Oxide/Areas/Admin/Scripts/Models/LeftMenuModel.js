﻿var MenuModel = function(key, title, iconCss, subMenuItems) {
    var self = this;
    self.Key = ko.observable(key);
    self.Title = ko.observable(title);
    self.IconCss = ko.observable(iconCss);
    self.SubMenuItems = ko.observableArray(subMenuItems);
};

var MenuItem = function(key, title, actionName, controllerName, subMenuItems) {
    var self = this;
    self.Key = ko.observable(key);
    self.Title = ko.observable(title);
    self.ActionName = ko.observable(actionName);
    self.ControllerName = ko.observable(controllerName);
    self.SubMenuItems = ko.observableArray(subMenuItems);
};