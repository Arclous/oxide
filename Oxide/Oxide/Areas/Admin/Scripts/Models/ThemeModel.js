﻿var themes = ko.observableArray([]);;

function ThemesModel(urlBase) {
    var self = this;
    self.isLoading = ko.observable(false);
    self.filter = ko.observable("");
    self.themes = ko.observableArray([]);
    themes = self.themes;
    self.loadData = function(filter) {
        self.isLoading(true);
        $.get(urlBase, { filter: filter }, function(data) {
            self.themes.removeAll();
            for (var i = 0; i <= data.Records.length - 1; i++) {
                var themeModel = new ThemeModel(
                    data.Records[i].Id,
                    data.Records[i].Name,
                    data.Records[i].PreviewImageUrl,
                    data.Records[i].Active,
                    data.Records[i].ChangeStatusUrl
                );
                self.addTheme(themeModel);
            }
            self.isLoading(false);
        });
    };
    self.addTheme = function(model) {
        self.themes.push(model);
    }.bind(self);
}

function ThemeModel(id, name, previewImageUrl, active, changeStatusUrl) {
    var self = this;
    self.isActivating = ko.observable(false);
    self.id = ko.observable(id);
    self.name = ko.observable(name);
    self.previewImageUrl = ko.observable(previewImageUrl);
    self.active = ko.observable(active);
    self.changeStatusUrl = ko.observable(changeStatusUrl);

    self.activeLabel = ko.observable(false);
    self.deActiveLabel = ko.observable(true);
    if (self.active()) {
        self.activeLabel(true);
        self.deActiveLabel(false);
    } else {
        self.activeLabel(false);
        self.deActiveLabel(true);
    }
    self.changeStatus = function() {
        self.isActivating(true);
        $.get(changeStatusUrl, { name: self.name, status: self.active() }, function(data) {
            themes.removeAll();
            for (var i = 0; i <= data.Records.length - 1; i++) {
                var themeModel = new ThemeModel(
                    data.Records[i].Id,
                    data.Records[i].Name,
                    data.Records[i].PreviewImageUrl,
                    data.Records[i].Active,
                    data.Records[i].ChangeStatusUrl
                );
                themes.push(themeModel);
            }

            self.isActivating(false);
        });
    };
}