﻿ko.bindingHandlers.bootstrapModal = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var props = valueAccessor(),
            vm = bindingContext.createChildContext(viewModel);
        ko.utils.extend(vm, props);
        vm.close = function() {
            vm.show(false);
            vm.onClose();
        };
        vm.action = function() {
            vm.onAction();
        };
        ko.utils.toggleDomNodeCssClass(element, "modal fade", true);
        ko.renderTemplate("deleteConfirmTemplate", vm, null, element);
        var showHide = ko.computed(function() {
            $(element).modal(vm.show() ? "show" : "hide");
        });
        return {
            controlsDescendantBindings: true
        };
    }
};

function LogsModel(urlBase, clearLogsUrl) {
    var self = this;
    self.isLoading = ko.observable(false);
    self.totalRecords = ko.observable();
    self.totalPage = ko.observable();
    self.currentPage = ko.observable();
    self.displayinRecords = ko.observable();
    self.nextEnable = ko.observable(true);
    self.previousEnable = ko.observable(true);
    self.logItems = ko.observableArray([]);
    self.pageSize = ko.observable();
    self.actionFilter = ko.observable("");
    self.actionCurrentPage = ko.observable(0);
    self.actionPageSize = ko.observable(10);
    self.loadData = function(pagesize, page, filter) {
        self.isLoading(true);
        $.get(urlBase, { pageSize: pagesize, page: page, filter: filter }, function(data) {
            self.logItems.removeAll();
            for (var i = 0; i <= data.Records.length - 1; i++) {
                var logModel = new LogModel(
                    data.Records[i].Id,
                    data.Records[i].Name,
                    data.Records[i].Title,
                    data.Records[i].LogType,
                    data.Records[i].ModuleName,
                    data.Records[i].Description,
                    data.Records[i].InnerException,
                    data.Records[i].DateTimeInfo
                );
                self.addLogItem(logModel);
            }
            self.totalRecords(data.TotalRecords);
            self.totalPage(data.TotalPages);
            self.currentPage(data.CurrentPage);
            self.displayinRecords(data.DisplayingRecords);
            self.nextEnable(data.NextEnable);
            self.previousEnable(data.PreviousEnable);
            self.isLoading(false);
            self.setControls();
        });
    };
    self.addLogItem = function(model) {
        self.logItems.push(model);
    }.bind(self);
    self.clearLogs = function() {

        $.get(clearLogsUrl, {}, function(data) {
            self.logItems.removeAll();
            self.loadData(self.actionPageSize(), self.actionCurrentPage(), self.actionFilter());
        });
    };
    self.setControls = function() {
        $("#datatable-checkbox_info").html(Logs.Showing + " " + (self.currentPage() + 1) + " / " + self.totalPage());
        $(".dataTables_empty").html("");

        $("#datatable-checkbox_next").unbind();
        $("#datatable-checkbox_previous").unbind();
        $("#datatable-checkbox_next").addClass("disabled");
        $("#datatable-checkbox_previous").addClass("disabled");

        if (self.nextEnable()) {
            $("#datatable-checkbox_next").removeClass("disabled");
            $("#datatable-checkbox_next").click(function() {
                self.actionCurrentPage(self.actionCurrentPage() + 1);
                self.loadData(self.actionPageSize(), self.actionCurrentPage(), self.actionFilter());
            });
        }
        if (self.previousEnable()) {
            $("#datatable-checkbox_previous").removeClass("disabled");
            $("#datatable-checkbox_previous").click(function() {
                self.actionCurrentPage(self.actionCurrentPage() - 1);
                self.loadData(self.actionPageSize(), self.actionCurrentPage(), self.actionFilter());
            });
        }
    }.bind(self);
    self.confirmModal = {
        header: ko.observable(Logs.Please_Confirm),
        message: ko.observable(Logs.Do_you_want_to_delete_clear_all_logs_),
        closeLabel: Logs.Cancel,
        primaryLabel: Logs.Clear,
        show: ko.observable(false),
        onClose: function() {

        },
        onAction: function() {
            self.confirmModal.show(false);
            self.clearLogs();
        }
    };
    self.showConfirmModal = function() {
        self.confirmModal.header(Logs.Please_Confirm, Logs.Warning);
        self.confirmModal.message(Logs.Do_you_want_to_delete_clear_all_logs_);
        self.confirmModal.show(true);
    };
}

function LogModel(id, name, title, logType, moduleName, description, innerException, dateTimeInfo) {
    var self = this;
    self.id = ko.observable(id);
    self.name = ko.observable(name);
    self.title = ko.observable(title);
    self.logType = ko.observable(logType);
    self.logTypeTitle = ko.observable();
    self.moduleName = ko.observable(moduleName);
    self.description = ko.observable(description);
    self.innerException = ko.observable(innerException);
    self.dateTimeInfo = ko.observable(dateTimeInfo);

    self.descriptionGeneral = ko.computed(function() {
        return (self.description().length > 250 ? self.description().substring(0, 249) + "..." : self.description);
    });

    if (logType === 0)
        self.logTypeTitle(Logs.Error);
    else if (logType === 1)
        self.logTypeTitle(Logs.Warning);
    else if (logType === 2)
        self.logTypeTitle(Logs.Info);

}