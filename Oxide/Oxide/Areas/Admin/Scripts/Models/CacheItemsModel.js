﻿function CacheItemsModel(urlBase, clearUrlGeneral) {
    var self = this;
    self.isLoading = ko.observable(false);
    self.totalRecords = ko.observable();
    self.totalPage = ko.observable();
    self.currentPage = ko.observable();
    self.displayinRecords = ko.observable();
    self.nextEnable = ko.observable(true);
    self.previousEnable = ko.observable(true);
    self.cacheItems = ko.observableArray([]);
    self.clearCacheUrl = ko.observable("");
    self.valueUpdateUrl = ko.observable("");
    self.pageSize = ko.observable();
    self.actionFilter = ko.observable("");
    self.actionCurrentPage = ko.observable(0);
    self.actionPageSize = ko.observable(10);
    self.clearAllCacheUrl = ko.observable(clearUrlGeneral);
    self.loadData = function(pagesize, page, filter) {
        self.isLoading(true);
        $.get(urlBase, { pageSize: pagesize, page: page, filter: filter }, function(data) {
            self.cacheItems.removeAll();
            for (var i = 0; i <= data.Records.length - 1; i++) {
                var cacheItemModel = new CacheItemModel(
                    data.Records[i].Id,
                    data.Records[i].Name,
                    data.Records[i].Title,
                    data.Records[i].LastUpdate,
                    data.Records[i].Value,
                    data.Records[i].IsCached
                );
                self.addCacheItem(cacheItemModel);
            }
            self.clearCacheUrl(data.ClearCacheUrl);
            self.valueUpdateUrl(data.UpdateValueUrl);
            self.totalRecords(data.TotalRecords);
            self.totalPage(data.TotalPages);
            self.currentPage(data.CurrentPage);
            self.displayinRecords(data.DisplayingRecords);
            self.nextEnable(data.NextEnable);
            self.previousEnable(data.PreviousEnable);
            self.isLoading(false);
            self.setControls();
        });
    };
    self.addCacheItem = function(model) {
        self.cacheItems.push(model);
    }.bind(self);

    self.clearCache = function(model) {
        model.isWorking(true);

        $.get(self.clearCacheUrl(), { cacheKey: model.name() }, function(data) {
            model.status(false);
            model.isWorking(false);
        });
    }.bind(self);

    self.clearAllCache = function(model) {
        model.isLoading(true);

        $.get(self.clearAllCacheUrl(), function(data) {
            self.loadData(self.actionPageSize(), self.actionCurrentPage(), self.actionFilter());
            model.isLoading(false);
        });
    }.bind(self);

    self.updateValue = function(model) {
        model.isWorking(true);
        $.get(self.valueUpdateUrl(), { cacheKey: model.name(), value: model.valueOrj() }, function(data) {
            model.isWorking(false);
            new PNotify({
                title: CacheItems.Cache_Settings_Updated,
                text: CacheItems.Cache_settings_updated_successfully_,
                type: "success",
                styling: "bootstrap3"
            });
        });
    }.bind(self);
    self.setControls = function() {
        $("#datatable-checkbox_info").html(CacheItems.Showing + " " + (self.currentPage() + 1) + " / " + self.totalPage());
        $(".dataTables_empty").html("");

        $("#datatable-checkbox_next").unbind();
        $("#datatable-checkbox_previous").unbind();
        $("#datatable-checkbox_next").addClass("disabled");
        $("#datatable-checkbox_previous").addClass("disabled");

        if (self.nextEnable()) {
            $("#datatable-checkbox_next").removeClass("disabled");
            $("#datatable-checkbox_next").click(function() {
                self.actionCurrentPage(self.actionCurrentPage() + 1);
                self.loadData(self.actionPageSize(), self.actionCurrentPage(), self.actionFilter());
            });
        }
        if (self.previousEnable()) {
            $("#datatable-checkbox_previous").removeClass("disabled");
            $("#datatable-checkbox_previous").click(function() {
                self.actionCurrentPage(self.actionCurrentPage() - 1);
                self.loadData(self.actionPageSize(), self.actionCurrentPage(), self.actionFilter());
            });
        }
    }.bind(self);
}

function CacheItemModel(id, name, title, lastUpdate, value, status, isCached) {
    var self = this;
    self.isWorking = ko.observable(false);
    self.id = ko.observable(id);
    self.name = ko.observable(name);
    self.title = ko.observable(title);
    self.lastUpdate = ko.observable(lastUpdate);
    self.value = ko.observable(value + " " + CacheItems.min);
    self.valueOrj = ko.observable(value);
    self.status = ko.observable(status);
    self.isCached = ko.observable(isCached);

}