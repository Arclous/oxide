﻿ko.bindingHandlers.bootstrapModal = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var props = valueAccessor(),
            vm = bindingContext.createChildContext(viewModel);
        ko.utils.extend(vm, props);
        vm.close = function() {
            vm.show(false);
            vm.onClose();
        };
        vm.action = function() {
            vm.onAction();
        };
        ko.utils.toggleDomNodeCssClass(element, "modal fade", true);
        ko.renderTemplate("widgetListTemplate", vm, null, element);
        var showHide = ko.computed(function() {
            $(element).modal(vm.show() ? "show" : "hide");
        });
        return {
            controlsDescendantBindings: true
        };
    }
};
ko.bindingHandlers.confirmModal = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var props = valueAccessor(),
            vm = bindingContext.createChildContext(viewModel);
        ko.utils.extend(vm, props);
        vm.close = function() {
            vm.show(false);
            vm.onClose();
        };
        vm.action = function() {
            vm.onAction();
        };
        ko.utils.toggleDomNodeCssClass(element, "modal fade", true);
        ko.renderTemplate("deleteConfirmTemplate", vm, null, element);
        var showHide = ko.computed(function() {
            $(element).modal(vm.show() ? "show" : "hide");
        });
        return {
            controlsDescendantBindings: true
        };
    }
};

function LayoutModel(url, pageId, widgetLoadUrl) {
    var self = this;
    self.isLoading = ko.observable(false);
    self.layoutId = ko.observable();
    self.addWidgetUrl = ko.observable();
    self.manageWidgetUrl = ko.observable();
    self.editWidgetUrl = ko.observable();
    self.deleteWidgetUrl = ko.observable();
    self.updateWidgetUrl = ko.observable();
    self.elements = ko.observableArray([]);
    self.newElementId = ko.observable();
    self.currentElementId = ko.observable();

//Load Elements
    self.loadData = function() {
        self.isLoading(true);
        $.get(url, { pageId: pageId }, function(data) {
            for (var i = 0; i <= data.Elements.length - 1; i++) {
                var elementModel = new ElementModel(
                    data.Elements[i].Id,
                    data.Elements[i].Title,
                    data.Elements[i].Widgets,
                    pageId, data.Id
                );
                self.addElement(elementModel);
            }
            self.isLoading(false);
            self.layoutId = data.Id;
            self.addWidgetUrl = data.AddWidgetUrl;
            self.manageWidgetUrl = data.ManageWidgetUrl;
            self.editWidgetUrl = data.EditWidgetUrl;
            self.deleteWidgetUrl = data.DeleteWidgetUrl;
            self.updateWidgetUrl = data.UpdateWidgetUrl;
        });
    };
    self.addElement = function(model) {
        self.elements.push(model);
    }.bind(self);

    //Widget modal window
    self.modal = {
        header: ManageWidgets.Widgets,
        message: ko.observable(ManageWidgets.Please_select_the_widget_which_you_want_to_add_to_page),
        closeLabel: ManageWidgets.Cancel,
        primaryLabel: ManageWidgets.Add_Widget,
        searchLabel: ManageWidgets.Search,
        searchLabelPlaceHolder: ManageWidgets.Search_for___,
        widgetList: ko.observableArray([]),
        isWidgetLoading: ko.observable(false),
        filterText: ko.observable(""),
        selectedElement: ko.observable(),
        pageId: ko.observable(),
        layoutId: ko.observable(),
        elementId: ko.observable(),
        show: ko.observable(false),
        onClose: function() {
            self.onModalClose();
        },
        loadWidgetList: function() {
            self.loadWidgets(self.filterText);
        },
        addWidget: function(widget) {
            self.addWidget(widget);
        }
    };
    self.showModal = function(element) {
        self.modal.message(ManageWidgets.Please_select_the_widget_which_you_want_to_add_to_page);
        self.modal.selectedElement = element.id;
        self.modal.pageId = pageId;
        self.modal.elementId = element.id;
        self.modal.layoutId = self.layoutId;
        self.modal.loadWidgetList();
        self.modal.show(true);
    };
    self.onModalClose = function() {
    };
    self.filter = function(filter) {
        self.modal.loadWidgetList(filter);
    };
    self.addWidget = function(widget) {
        self.addWidgetToPage(self.modal.pageId, self.modal.layoutId, self.modal.elementId, widget.widgetKey, widget);
        self.modal.show(false);
    };
    self.modal.loadWidgetList = function(filter) {
        $.get(widgetLoadUrl, { filter: filter }, function(data) {
            self.modal.isWidgetLoading(true);
            self.modal.widgetList.removeAll();
            for (var i = 0; i <= data.length - 1; i++) {
                var widgetModel = new WidgetItemModel(
                    data[i].Id,
                    data[i].Name,
                    data[i].Title,
                    data[i].Description,
                    data[i].Category,
                    data[i].PreviewImageUrl,
                    data[i].Order,
                    data[i].ManageWidgetUrl,
                    data[i].EditWidgetUrl,
                    data[i].WidgetKey
                );
                self.modal.widgetList.push(widgetModel);
            }
            self.modal.isWidgetLoading(false);
        });
    };
    self.addWidgetToPage = function(pageId, layoutId, elementId, widgetKey, widget) {
        self.isLoading(true);
        $.get(self.addWidgetUrl, { pageId: pageId, layoutId: layoutId, elementId: elementId, widgetKey: widgetKey, widget }, function(data) {
            self.isLoading(false);
            var targetElement = ko.utils.arrayFirst(self.elements(), function(element) {
                return element.id === elementId;
            });
            var widgetModel = new WidgetItemModel(
                data.Id,
                data.Name,
                data.Title,
                data.Description,
                data.Category,
                data.PreviewImageUrl,
                data.Order,
                data.ManageWidgetUrl,
                data.EditWidgetUrl,
                data.WidgetKey
            );
            widgetModel.pageId(pageId);
            widgetModel.elementId(elementId);
            widgetModel.layoutId(layoutId);
            targetElement.widgets.push(widgetModel);

        });
    };

    //Confirm modal window
    self.confirmModal = {
        header: ManageWidgets.Please_Confirm,
        message: ko.observable(ManageWidgets.Do_you_want_to_delete_widget_from_page),
        closeLabel: ManageWidgets.Cancel,
        primaryLabel: ManageWidgets.Delete_Widget,
        selectedElement: ko.observable(),
        selectedWidget: ko.observable(),
        show: ko.observable(false),
        onClose: function() {
            self.onModalConfrimClose();
        },
        onAction: function() {
            self.deleteWidget();
        }
    };
    self.showConfirmModal = function(widget, element) {
        self.confirmModal.message(ManageWidgets.Do_you_want_to_delete_widget_from_page);
        self.confirmModal.selectedElement = element;
        self.confirmModal.selectedWidget = widget;
        self.confirmModal.show(true);
    };
    self.onModalConfrimClose = function() {
    };
    self.deleteWidget = function() {
        self.deleteWidgetFromPage(self.confirmModal.selectedWidget.id(), self.confirmModal.selectedElement.id, self.confirmModal.selectedWidget.widgetKey);
        self.confirmModal.show(false);
    };
    self.deleteWidgetFromPage = function(id, elementId, widgetKey) {
        self.isLoading(true);
        $.get(self.deleteWidgetUrl, { id: id }, function(data) {
            self.isLoading(false);
            var targetElement = ko.utils.arrayFirst(self.elements(), function(element) {
                return element.id === elementId;
            });
            var targetWidget = ko.utils.arrayFirst(targetElement.widgets(), function(widget) {
                return widget.widgetKey === widgetKey;
            });

            targetElement.widgets.remove(targetWidget);
        });
    };
    //Ordering Events
    self.afterMove = function(args) {
        self.currentElementId(args.sourceParent()[0].elementId());
        self.updateWidget(args.item.id(), args.item.pageId(), args.item.layoutId(), self.newElementId(), args.item.widgetKey);
    };
    self.beforeMove = function(args) {
        self.newElementId(args.targetParent()[0].elementId());
    };
    //Update
    self.updateWidget = function(id, pageId, layoutId, elementId, widgetKey) {
        self.isLoading(true);
        var targetElement = ko.utils.arrayFirst(self.elements(), function(element) {
            return element.id() === elementId;
        });
        $.get(self.updateWidgetUrl, { id: id, pageId: pageId, layoutId: layoutId, elementId: elementId, widgetKey: widgetKey, orderList: joinWidgetValues(targetElement.widgets()) }, function(data) {
            self.isLoading(false);
        });
    };
}

function ElementModel(id, title, widgetList, pageId, layoutId) {
    var self = this;
    self.id = ko.observable(id);
    self.title = ko.observable(title);
    self.widgets = ko.observableArray([]);
    self.pageId = ko.observable(pageId);
    self.layoutId = ko.observable(layoutId);

    //Add DummyData
    var dummyData = new WidgetItemModel(-1, "", "", "", "", "", -1, "", "", "", true);
    dummyData.elementId(self.id());
    dummyData.layoutId(self.layoutId());
    self.widgets.push(dummyData);

    for (var i = 0; i <= widgetList.length - 1; i++) {
        var widgetModel = new WidgetItemModel(
            widgetList[i].Id,
            widgetList[i].Name,
            widgetList[i].Title,
            widgetList[i].Description,
            widgetList[i].Category,
            widgetList[i].PreviewImageUrl,
            widgetList[i].Order,
            widgetList[i].ManageWidgetUrl,
            widgetList[i].EditWidgetUrl,
            widgetList[i].WidgetKey
        );
        widgetModel.pageId(self.pageId());
        widgetModel.elementId(self.id());
        widgetModel.layoutId(self.layoutId());

        self.widgets.push(widgetModel);
    }
    self.deleteWidget = function(model) {
        self.widgets.remove(model);
    }.bind(self);

}

function WidgetItemModel(id, name, title, description, category, previewImageUrl, order, manageWidgetUrl, editWidgetUrl, widgetKey, isDummy) {
    var self = this;
    self.id = ko.observable(id);
    self.name = ko.observable(name);
    self.title = ko.observable(title);
    self.description = ko.observable(description);
    self.category = ko.observable(category);
    self.previewImageUrl = ko.observable(previewImageUrl);
    self.order = ko.observable(order);
    self.manageWidgetUrl = ko.observable(manageWidgetUrl);
    self.editWidgetUrl = ko.observable(editWidgetUrl);
    self.widgetKey = ko.observable(widgetKey);
    self.isDummy = ko.observable(!isDummy);
    self.pageId = ko.observable();
    self.elementId = ko.observable();
    self.layoutId = ko.observable();
}

function joinWidgetValues(array) {
    var res = "";
    for (var i = 0; i <= array.length - 1; i++) {
        if (i === array.length - 1)
            res += array[i].id();
        else
            res += array[i].id() + ",";
    }
    return res;
}