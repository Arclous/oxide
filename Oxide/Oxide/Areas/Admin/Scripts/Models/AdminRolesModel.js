﻿
ko.bindingHandlers.bootstrapModal = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var props = valueAccessor(),
            vm = bindingContext.createChildContext(viewModel);
        ko.utils.extend(vm, props);
        vm.close = function() {
            vm.show(false);
            vm.onClose();
        };
        vm.action = function() {
            vm.onAction();
        };
        ko.utils.toggleDomNodeCssClass(element, "modal fade", true);
        ko.renderTemplate("deleteConfirmTemplate", vm, null, element);
        var showHide = ko.computed(function() {
            $(element).modal(vm.show() ? "show" : "hide");
        });
        return {
            controlsDescendantBindings: true
        };
    }
};

function AdminRolesModel(urlBase) {
    var self = this;
    self.isLoading = ko.observable(false);
    self.totalRecords = ko.observable();
    self.totalPage = ko.observable();
    self.currentPage = ko.observable();
    self.displayinRecords = ko.observable();
    self.nextEnable = ko.observable(true);
    self.previousEnable = ko.observable(true);
    self.adminRoles = ko.observableArray([]);
    self.pageSize = ko.observable();
    self.actionFilter = ko.observable("");
    self.actionCurrentPage = ko.observable(0);
    self.actionPageSize = ko.observable(10);
    self.deleteUrl = ko.observable("");
    self.loadData = function(pagesize, page, filter) {
        self.isLoading(true);
        $.get(urlBase, { pageSize: pagesize, page: page, filter: filter }, function(data) {
            self.adminRoles.removeAll();
            for (var i = 0; i <= data.Records.length - 1; i++) {
                var adminRoleModel = new AdminRoleModel(
                    data.Records[i].Id,
                    data.Records[i].Name,
                    data.Records[i].Title,
                    data.Records[i].EditUrl,
                    data.Records[i].ManagePermissionUrl
                );
                self.addAdminRole(adminRoleModel);
            }
            self.deleteUrl(data.DeleteUrl);
            self.totalRecords(data.TotalRecords);
            self.totalPage(data.TotalPages);
            self.currentPage(data.CurrentPage);
            self.displayinRecords(data.DisplayingRecords);
            self.nextEnable(data.NextEnable);
            self.previousEnable(data.PreviousEnable);
            self.isLoading(false);
            self.setControls();
        });
    };
    self.addAdminRole = function(model) {
        self.adminRoles.push(model);
    }.bind(self);

    self.removeAdminRole = function(id) {
        self.adminRoles.remove(function(adminRole) {
            return adminRole.id === id;
        });
    }.bind(self);

    self.deleteAdminRole = function(model) {
        model.isWorking(true);
        $.get(self.deleteUrl(), { adminRoleId: model.id() }, function(data) {
            model.isWorking(false);
            self.removeAdminRole(model.id);
        });
    }.bind(self);

    self.modal = {
        header: AdminRoles.Please_Confirm,
        message: ko.observable(AdminRoles.Do_you_want_to_delete_),
        comment: ko.observable(""),
        closeLabel: AdminRoles.Cancel,
        primaryLabel: AdminRoles.Delete,
        selecedAdminRole: ko.observable(),
        show: ko.observable(false),
        onClose: function() {
            self.onModalClose();
        },
        onAction: function() {
            self.onModalAction();
        }
    };
    self.showModal = function(adminRole) {
        self.modal.message("'" + adminRole.name() + "' " + AdminRoles.Do_you_want_to_delete_this_admin_role_);
        self.modal.selecedAdminRole(adminRole);
        self.modal.show(true);
    };
    self.onModalClose = function() {

    };
    self.onModalAction = function() {
        self.deleteAdminRole(self.modal.selecedAdminRole());
        self.modal.show(false);
    };
    self.setControls = function() {
        $("#datatable-checkbox_info").html(AdminRoles.Showing + " " + (self.currentPage() + 1) + " / " + self.totalPage());
        $(".dataTables_empty").html("");
        $("#datatable-checkbox_next").unbind();
        $("#datatable-checkbox_previous").unbind();
        $("#datatable-checkbox_next").addClass("disabled");
        $("#datatable-checkbox_previous").addClass("disabled");

        if (self.nextEnable()) {
            $("#datatable-checkbox_next").removeClass("disabled");
            $("#datatable-checkbox_next").click(function() {
                self.actionCurrentPage(self.actionCurrentPage() + 1);
                self.loadData(self.actionPageSize(), self.actionCurrentPage(), self.actionFilter());
            });
        }
        if (self.previousEnable()) {
            $("#datatable-checkbox_previous").removeClass("disabled");
            $("#datatable-checkbox_previous").click(function() {
                self.actionCurrentPage(self.actionCurrentPage() - 1);
                self.loadData(self.actionPageSize(), self.actionCurrentPage(), self.actionFilter());
            });
        }
    }.bind(self);

}

function AdminRoleModel(id, name, title, editRoleUrl, manegeRolesUrl) {
    var self = this;
    self.isWorking = ko.observable(false);
    self.id = ko.observable(id);
    self.name = ko.observable(name);
    self.title = ko.observable(title);
    self.editUrl = ko.observable(editRoleUrl);
    self.manegeRolesUrl = ko.observable(manegeRolesUrl);
}