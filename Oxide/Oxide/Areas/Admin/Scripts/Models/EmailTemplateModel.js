﻿ko.bindingHandlers.bootstrapModal = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var props = valueAccessor(),
            vm = bindingContext.createChildContext(viewModel);
        ko.utils.extend(vm, props);
        vm.close = function() {
            vm.show(false);
            vm.onClose();
        };
        vm.action = function() {
            vm.onAction();
        };
        ko.utils.toggleDomNodeCssClass(element, "modal fade", true);
        ko.renderTemplate("deleteConfirmTemplate", vm, null, element);
        var showHide = ko.computed(function() {
            $(element).modal(vm.show() ? "show" : "hide");
        });
        return {
            controlsDescendantBindings: true
        };
    }
};

function EmailTemplatesModel(urlBase, deleteUrl) {
    var self = this;
    self.isLoading = ko.observable(false);
    self.totalRecords = ko.observable();
    self.totalPage = ko.observable();
    self.currentPage = ko.observable();
    self.displayinRecords = ko.observable();
    self.nextEnable = ko.observable(true);
    self.previousEnable = ko.observable(true);
    self.templateItems = ko.observableArray([]);
    self.pageSize = ko.observable();
    self.actionFilter = ko.observable("");
    self.actionCurrentPage = ko.observable(0);
    self.actionPageSize = ko.observable(10);
    self.loadData = function(pagesize, page, filter) {
        self.isLoading(true);
        $.get(urlBase, { pageSize: pagesize, page: page, filter: filter }, function(data) {
            self.templateItems.removeAll();
            for (var i = 0; i <= data.Records.length - 1; i++) {
                var model = new EmailTemplateModel(
                    data.Records[i].Id,
                    data.Records[i].Name,
                    data.Records[i].Title,
                    data.Records[i].EditUrl
                );
                self.addTemplateItem(model);
            }
            self.totalRecords(data.TotalRecords);
            self.totalPage(data.TotalPages);
            self.currentPage(data.CurrentPage);
            self.displayinRecords(data.DisplayingRecords);
            self.nextEnable(data.NextEnable);
            self.previousEnable(data.PreviousEnable);
            self.isLoading(false);
            self.setControls();
        });
    };
    self.addTemplateItem = function(model) {
        self.templateItems.push(model);
    }.bind(self);
    self.deleteTemplate = function(elementId) {
        self.isLoading(true);
        $.get(deleteUrl, { id: elementId.id() }, function(data) {
            self.isLoading(false);
            var targetElement = ko.utils.arrayFirst(self.templateItems(), function(element) {
                return element.id === elementId;
            });
            self.templateItems.remove(targetElement);
            self.loadData(self.actionPageSize(), self.actionCurrentPage(), self.actionFilter());
        });
    };
    self.setControls = function() {
        $("#datatable-checkbox_info").html(EmailTmp.Showing + " " + (self.currentPage() + 1) + " / " + self.totalPage());
        $(".dataTables_empty").html("");

        $("#datatable-checkbox_next").unbind();
        $("#datatable-checkbox_previous").unbind();
        $("#datatable-checkbox_next").addClass("disabled");
        $("#datatable-checkbox_previous").addClass("disabled");

        if (self.nextEnable()) {
            $("#datatable-checkbox_next").removeClass("disabled");
            $("#datatable-checkbox_next").click(function() {
                self.actionCurrentPage(self.actionCurrentPage() + 1);
                self.loadData(self.actionPageSize(), self.actionCurrentPage(), self.actionFilter());
            });
        }
        if (self.previousEnable()) {
            $("#datatable-checkbox_previous").removeClass("disabled");
            $("#datatable-checkbox_previous").click(function() {
                self.actionCurrentPage(self.actionCurrentPage() - 1);
                self.loadData(self.actionPageSize(), self.actionCurrentPage(), self.actionFilter());
            });
        }
    }.bind(self);
    self.confirmModal = {
        header: ko.observable(EmailTmp.Please_Confirm),
        message: ko.observable(EmailTmp.Do_you_want_to_delete_clear_all_logs_),
        closeLabel: EmailTmp.Cancel,
        primaryLabel: EmailTmp.Clear,
        selectedElement: ko.observable(),
        show: ko.observable(false),
        onClose: function() {

        },
        onAction: function() {
            self.confirmModal.show(false);
            self.deleteTemplate(self.confirmModal.selectedElement);
        }
    };
    self.showConfirmModal = function(element) {
        self.confirmModal.selectedElement = element;
        self.confirmModal.header(EmailTmp.Please_Confirm, EmailTmp.Warning);
        self.confirmModal.message(EmailTmp.Do_you_want_to_delete_clear_all_logs_);
        self.confirmModal.show(true);
    };
}

function EmailTemplateModel(id, name, title, editUrl) {
    var self = this;
    self.id = ko.observable(id);
    self.name = ko.observable(name);
    self.title = ko.observable(title);
    self.editUrl = ko.observable(editUrl);
}