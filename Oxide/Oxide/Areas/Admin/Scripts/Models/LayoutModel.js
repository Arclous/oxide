﻿ko.bindingHandlers.bootstrapModal = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var props = valueAccessor(),
            vm = bindingContext.createChildContext(viewModel);
        ko.utils.extend(vm, props);
        vm.close = function() {
            vm.show(false);
            vm.onClose();
        };
        vm.action = function() {
            vm.onAction();
        };
        ko.utils.toggleDomNodeCssClass(element, "modal fade", true);
        ko.renderTemplate("imagePreviewTemplate", vm, null, element);
        var showHide = ko.computed(function() {
            $(element).modal(vm.show() ? "show" : "hide");
        });
        return {
            controlsDescendantBindings: true
        };
    }
};

function LayoutsModel(urlBase) {
    var self = this;
    self.isLoading = ko.observable(false);
    self.totalRecords = ko.observable();
    self.totalPage = ko.observable();
    self.currentPage = ko.observable();
    self.displayinRecords = ko.observable();
    self.nextEnable = ko.observable(true);
    self.previousEnable = ko.observable(true);
    self.mainLayouts = ko.observableArray([]);
    self.pageSize = ko.observable();
    self.actionFilter = ko.observable("");
    self.actionCurrentPage = ko.observable(0);
    self.actionPageSize = ko.observable(10);
    self.loadData = function(pagesize, page, filter) {
        self.isLoading(true);
        $.get(urlBase, { pageSize: pagesize, page: page, filter: filter }, function(data) {
            self.mainLayouts.removeAll();
            for (var i = 0; i <= data.Records.length - 1; i++) {
                var layoutModel = new LayoutModel(
                    data.Records[i].Id,
                    data.Records[i].Name,
                    data.Records[i].Layouts,
                    data.Records[i].ManageWidgetsUrl,
                    data.Records[i].PreviewImage
                );
                self.addLayout(layoutModel);
            }

            self.totalRecords(data.TotalRecords);
            self.totalPage(data.TotalPages);
            self.currentPage(data.CurrentPage);
            self.displayinRecords(data.DisplayingRecords);
            self.nextEnable(data.NextEnable);
            self.previousEnable(data.PreviousEnable);
            self.isLoading(false);
            self.setControls();

        });
    };
    self.addLayout = function(model) {
        self.mainLayouts.push(model);
    }.bind(self);
    self.setControls = function() {
        $("#datatable-layouts_info").html(Layouts.Showing + " " + (self.currentPage() + 1) + " / " + self.totalPage());
        $(".dataTables_empty").html("");
        $("#datatable-checkbox_next").unbind();
        $("#datatable-checkbox_previous").unbind();
        $("#datatable-checkbox_next").addClass("disabled");
        $("#datatable-checkbox_previous").addClass("disabled");

        if (self.nextEnable()) {
            $("#datatable-checkbox_next").removeClass("disabled");
            $("#datatable-checkbox_next").click(function() {
                self.actionCurrentPage(self.actionCurrentPage() + 1);
                self.loadFiles(self.actionPageSize(), self.actionCurrentPage(), self.actionFilter());
            });
        }
        if (self.previousEnable()) {
            $("#datatable-checkbox_previous").removeClass("disabled");
            $("#datatable-checkbox_previous").click(function() {
                self.actionCurrentPage(self.actionCurrentPage() - 1);
                self.loadFiles(self.actionPageSize(), self.actionCurrentPage(), self.actionFilter());
            });
        }
    }.bind(self);

    self.modal = {
        previewImage: ko.observable(""),
        closeLabel: Layouts.Ok,
        show: ko.observable(false),
        onClose: function() {
            self.onModalClose();
        },
        onAction: function() {
            self.onModalAction();
        }
    };
    self.showModal = function(model) {
        self.modal.previewImage(model.previewImage());
        $("img.lazy").lazyload();
        self.modal.show(true);
    };
    self.onModalClose = function() {

    };
    self.onModalAction = function() {
        self.modal.show(false);
    };
}

function LayoutModel(id, name, layouts, manageWidgetsUrl, previewImage) {
    var self = this;
    self.id = ko.observable(id);
    self.name = ko.observable(name);
    self.layouts = ko.observable(layouts);
    self.manageWidgetsUrl = ko.observable(manageWidgetsUrl);
    self.previewImage = ko.observable(previewImage);
}