﻿ko.bindingHandlers.multiSelection = {
    init: function(element, valueAccessor, allBindingsAccessor) {
        //initialize bootstrapSwitch
        $(element).bootstrapSwitch();

        // setting initial value
        $(element).bootstrapSwitch("state", valueAccessor()());

        //handle the field changing
        $(element).on("switchChange.bootstrapSwitch", function(event, state) {
            var observable = valueAccessor();
            observable(state);
        });

        // Adding component options
        var options = allBindingsAccessor().bootstrapSwitchOptions || {};
        for (var property in options) {
            $(element).bootstrapSwitch(property, ko.utils.unwrapObservable(options[property]));
        }

        //handle disposal (if KO removes by the template binding)
        ko.utils.domNodeDisposal.addDisposeCallback(element, function() {
            $(element).bootstrapSwitch("destroy");
        });

    },
    //update the control when the view model changes
    update: function(element, valueAccessor, allBindingsAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        // Adding component options
        var options = allBindingsAccessor().bootstrapSwitchOptions || {};
        for (var property in options) {
            $(element).bootstrapSwitch(property, ko.utils.unwrapObservable(options[property]));
        }

        $(element).bootstrapSwitch("state", value);
    }
};

function AdminPermissionsModel(urlBase, updateUrl, userId) {
    var self = this;
    self.isLoading = ko.observable(false);
    self.roles = ko.observableArray([]);
    self.userId = ko.observable(userId);
    self.updateUrl = ko.observable(updateUrl);
    self.loadRoles = function() {
        self.isLoading(true);
        $.get(urlBase, { userId: userId }, function(data) {
            self.roles.removeAll();
            for (var i = 0; i <= data.length - 1; i++) {
                var role = new AdminRole(data[i].Id, data[i].Name, data[i].Status, self);
                self.addRole(role);
            }
            self.isLoading(false);
            $("[class='multiSelection']").bootstrapSwitch();
        });
    };

    self.addRole = function(model) {
        self.roles.push(model);
    }.bind(self);
}

function AdminRole(id, title, status, parentModel) {
    var self = this;
    self.id = ko.observable(id);
    self.title = ko.observable(title);
    self.parentModel = ko.observable(parentModel);
    self.status = ko.observable(status);
    self.updateStatus = ko.observable();
    self.status.subscribe(function() {
        self.updateStatus(ManageRoles.updating___);
        $.get(parentModel.updateUrl(), { userId: parentModel.userId(), roleId: self.id(), status: self.status() }, function(data) {
            self.updateStatus(ManageRoles.updated);
        });
    });

}