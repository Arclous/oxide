﻿
ko.bindingHandlers.bootstrapModal = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var props = valueAccessor(),
            vm = bindingContext.createChildContext(viewModel);
        ko.utils.extend(vm, props);
        vm.close = function() {
            vm.show(false);
            vm.onClose();
        };
        vm.action = function() {
            vm.onAction();
        };
        ko.utils.toggleDomNodeCssClass(element, "modal fade", true);
        ko.renderTemplate("deleteConfirmTemplate", vm, null, element);
        var showHide = ko.computed(function() {
            $(element).modal(vm.show() ? "show" : "hide");
        });
        return {
            controlsDescendantBindings: true
        };
    }
};
ko.bindingHandlers.bootstrapcloneModal = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var props = valueAccessor(),
            vm = bindingContext.createChildContext(viewModel);
        ko.utils.extend(vm, props);
        vm.close = function() {
            vm.show(false);
            vm.onClose();
        };
        vm.action = function() {
            vm.onAction();
        };
        ko.utils.toggleDomNodeCssClass(element, "modal fade", true);
        ko.renderTemplate("inputCloneTemplate", vm, null, element);
        var showHide = ko.computed(function() {
            $(element).modal(vm.show() ? "show" : "hide");
        });
        return {
            controlsDescendantBindings: true
        };
    }
};

function PagesModel(urlBase, urlLanguage, checkLanguageUrl, checkNameUrl, clonePageUrl) {
    var self = this;
    self.isLoading = ko.observable(false);
    self.isChecking = ko.observable(false);
    self.totalRecords = ko.observable();
    self.totalPage = ko.observable();
    self.currentPage = ko.observable();
    self.displayinRecords = ko.observable();
    self.nextEnable = ko.observable(true);
    self.previousEnable = ko.observable(true);
    self.pages = ko.observableArray([]);
    self.pageSize = ko.observable();
    self.actionFilter = ko.observable("");
    self.actionCurrentPage = ko.observable(0);
    self.actionPageSize = ko.observable(10);
    self.publishUrl = ko.observable("");
    self.editUrl = ko.observable("");
    self.deleteUrl = ko.observable("");
    self.manageWidgetsUrl = ko.observable("");
    self.languages = ko.observableArray([]);
    self.totalPublished = ko.observable();
    self.totalDraft = ko.observable();
    self.totalCreated = ko.observable();
    self.totalWaiting = ko.observable();

    self.isTotalPublished = ko.observable();
    self.isTotalDraft = ko.observable();
    self.isTotalCreated = ko.observable(true);
    self.isTotalWaiting = ko.observable();
    self.filterType = ko.observable(0);
    self.loadData = function(pagesize, page, filter) {
        self.isLoading(true);
        $.get(urlBase, { pageSize: pagesize, page: page, filter: filter, filterType: self.filterType() }, function(data) {
            self.pages.removeAll();
            for (var i = 0; i <= data.Records.length - 1; i++) {
                var pageModel = new PageModel(
                    data.Records[i].Id,
                    data.Records[i].Name,
                    data.Records[i].Title,
                    data.Records[i].PageKey,
                    data.Records[i].Url,
                    data.Records[i].Description,
                    data.Records[i].Tag,
                    data.Records[i].Order,
                    data.Records[i].VisibleForSite,
                    data.Records[i].VisibleForSeo,
                    data.Records[i].Language,
                    data.Records[i].Widgets,
                    data.Records[i].ManageWidgetsUrl,
                    data.Records[i].EditPageUrl,
                    data.Records[i].DeletePageUrl,
                    data.Records[i].Published
                );
                self.addPage(pageModel);
            }
            self.publishUrl(data.PublishUrl);
            self.editUrl(data.EditUrl);
            self.manageWidgetsUrl(data.ManageWidgetsUrl);
            self.deleteUrl(data.DeleteUrl);
            self.totalRecords(data.TotalRecords);
            self.totalPage(data.TotalPages);
            self.currentPage(data.CurrentPage);
            self.displayinRecords(data.DisplayingRecords);
            self.nextEnable(data.NextEnable);
            self.previousEnable(data.PreviousEnable);
            self.totalCreated(data.TotalCreated);
            self.totalPublished(data.TotalPublished);
            self.totalDraft(data.TotalDraft);
            self.totalWaiting(data.TotalWaiting);

            self.setControls();
            self.isLoading(false);
        });
    };
    self.loadLanguages = function() {
        self.isLoading(true);
        $.get(urlLanguage, {}, function(data) {
            self.languages.removeAll();
            for (var i = 0; i <= data.length - 1; i++) {
                var languageModel = new LanguageMdl(
                    data[i].Id,
                    data[i].Name
                );
                self.addLanguage(languageModel);
            }
            self.isLoading(false);
        });
    };
    self.addPage = function(model) {
        self.pages.push(model);
    }.bind(self);
    self.removePage = function(id) {
        self.pages.remove(function(page) {
            return page.id === id;
        });
    }.bind(self);
    self.deletePage = function(model) {
        model.isWorking(true);
        $.get(self.deleteUrl(), { pageId: model.id() }, function(data) {
            model.isWorking(false);
            self.removePage(model.id);
        });
    }.bind(self);
    self.publishPage = function(model) {
        model.buttonTextPublish(DisplayPages.Please_Wait___);
        model.buttonTextNotPublish(DisplayPages.Please_Wait___);
        $.get(self.publishUrl(), { pageId: model.id(), status: !model.published() }, function(data) {
            model.published(data.Published);
            model.buttonTextPublish(DisplayPages.Published);
            model.buttonTextNotPublish(DisplayPages.Not_Published);
        });
    }.bind(self);
    self.addLanguage = function(model) {
        self.languages.push(model);
    }.bind(self);
    self.modal = {
        header: DisplayPages.Please_Confirm,
        message: ko.observable(DisplayPages.Do_you_want_to_delete_),
        comment: ko.observable(""),
        closeLabel: DisplayPages.Cancel,
        primaryLabel: DisplayPages.Delete,
        selectedPage: ko.observable(),
        show: ko.observable(false),
        onClose: function() {
            self.onModalClose();
        },
        onAction: function() {
            self.onModalAction();
        }
    };
    self.showModal = function(page) {
        self.modal.message("'" + page.title() + "' " + DisplayPages.Do_you_want_to_delete_this_page_);
        self.modal.selectedPage(page);
        self.modal.show(true);
    };
    self.onModalClose = function() {

    };
    self.onModalAction = function() {
        self.deletePage(self.modal.selectedPage());
        self.modal.show(false);
    };
    self.cloneModal = {
        header: DisplayPages.Please_Enter_Information,
        message: ko.observable(DisplayPages.Please_enter_name_and_select_language_for_clone_page),
        comment: ko.observable(""),
        currentValue: ko.observable(""),
        languages: ko.observableArray([]),
        closeLabel: DisplayPages.Cancel,
        primaryLabel: DisplayPages.Clone,
        selectedPage: ko.observable(),
        languageValid: ko.observable(),
        selectText: ko.observable(DisplayPages.Please_Select_Lanaguage),
        nameValid: ko.observable(),
        info: ko.observable(),
        selectedLanguage: ko.observable(),
        show: ko.observable(false),
        onClose: function() {
            self.onCloneModalClose();
        },
        onAction: function() {
            self.onCloneModalAction();
        }
    };
    self.showCloneModal = function(page) {
        self.cloneModal.selectedPage(page);
        self.cloneModal.currentValue("");
        self.cloneModal.selectedLanguage();
        self.isChecking(false);
        self.cloneModal.languages(self.languages());
        self.cloneModal.show(true);
    };
    self.onCloneModalClose = function() {

    };
    self.onCloneModalAction = function() {
        if (self.cloneModal.currentValue() === "")
            showWarning(DisplayPages.Please_enter_name_for_the_page, DisplayPages.Warning);
        else if (self.cloneModal.nameValid() === false)
            showWarning(DisplayPages.Name_is_already_using__please_select_another_name, DisplayPages.Warning);
        else if (self.cloneModal.languageValid() === false)
            showWarning(DisplayPages.Language_is_already_using__please_select_another_language, DisplayPages.Warning);
        else {
            self.isChecking(true);
            $.get(clonePageUrl, { pageId: self.cloneModal.selectedPage().id(), laguageId: self.cloneModal.selectedLanguage().id(), name: self.cloneModal.currentValue() }, function(data) {
                self.isChecking(false);
                self.cloneModal.show(false);
                self.loadData(self.actionPageSize(), self.currentPage(), self.actionFilter());
            });
        }

    };
    self.cloneModal.selectedLanguage.subscribe(function(newValue) {
        if (newValue == undefined) return;
        self.isChecking(true);
        $.get(checkLanguageUrl, { pageId: self.cloneModal.selectedPage().id(), laguageId: newValue.id() }, function(data) {
            if (data === false) {
                self.cloneModal.languageValid(false);
                showWarning(DisplayPages.Language_is_already_using__please_select_another_language, DisplayPages.Warning);
            } else
                self.cloneModal.languageValid(true);

            self.isChecking(false);
        });
    });
    self.cloneModal.currentValue.subscribe(function(newValue) {
        self.isChecking(true);
        $.get(checkNameUrl, { pageId: self.cloneModal.selectedPage().id(), name: newValue }, function(data) {
            if (data === false) {
                self.cloneModal.nameValid(false);
                showWarning(DisplayPages.Name_is_already_using__please_select_another_name, DisplayPages.Warning);
            } else
                self.cloneModal.nameValid(true);

            self.isChecking(false);
        });
    });
    self.setControls = function() {
        $("#datatable-checkbox_info").html(DisplayPages.Showing + " " + (self.currentPage() + 1) + " / " + self.totalPage());
        $(".dataTables_empty").html("");
        $("#datatable-checkbox_next").unbind();
        $("#datatable-checkbox_previous").unbind();
        $("#datatable-checkbox_next").addClass("disabled");
        $("#datatable-checkbox_previous").addClass("disabled");

        if (self.nextEnable()) {
            $("#datatable-checkbox_next").removeClass("disabled");
            $("#datatable-checkbox_next").click(function() {
                self.actionCurrentPage(self.actionCurrentPage() + 1);
                self.loadData(self.actionPageSize(), self.actionCurrentPage(), self.actionFilter());
            });
        }
        if (self.previousEnable()) {
            $("#datatable-checkbox_previous").removeClass("disabled");
            $("#datatable-checkbox_previous").click(function() {
                self.actionCurrentPage(self.actionCurrentPage() - 1);
                self.loadData(self.actionPageSize(), self.actionCurrentPage(), self.actionFilter());
            });
        }
    }.bind(self);

    self.setFilter = function(filterType) {
        switch (filterType) {
        case 0:
            self.isTotalCreated(true);
            self.isTotalPublished(false);
            self.isTotalDraft(false);
            self.isTotalWaiting(false);
            self.filterType(0);
            break;
        case 1:
            self.isTotalCreated(false);
            self.isTotalPublished(true);
            self.isTotalDraft(false);
            self.isTotalWaiting(false);
            self.filterType(1);
            break;
        case 2:
            self.isTotalCreated(false);
            self.isTotalPublished(false);
            self.isTotalDraft(true);
            self.isTotalWaiting(false);
            self.filterType(2);
            break;
        case 3:
            self.isTotalCreated(false);
            self.isTotalPublished(false);
            self.isTotalDraft(false);
            self.isTotalWaiting(true);
            self.filterType(3);
            break;
        default:
            self.isTotalCreated(false);
            self.isTotalPublished(false);
            self.isTotalDraft(false);
            self.isTotalWaiting(false);
            self.filterType(0);

        }
        self.currentPage(0);
        self.actionCurrentPage(0);
        self.loadData(self.actionPageSize(), self.currentPage(), self.actionFilter());
    };
}

function PageModel(id, name, title, pageKey, url, description, tag, order, visibleForSite, visibleForSeo, language, widgets, manageWidgetsUrl, editPageUrl, deletePageUrl, published) {
    var self = this;
    self.isWorking = ko.observable(false);
    self.id = ko.observable(id);
    self.name = ko.observable(name);
    self.title = ko.observable(title);
    self.pageKey = ko.observable(pageKey);
    self.url = ko.observable(url);
    self.description = ko.observable(description);
    self.tag = ko.observable(tag);
    self.order = ko.observable(order);
    self.visibleForSite = ko.observable(visibleForSite);
    self.visibleForSeo = ko.observable(visibleForSeo);
    self.language = ko.observable(language);
    self.widgets = ko.observableArray(widgets);
    self.manageWidgetsUrl = ko.observable(manageWidgetsUrl);
    self.editPageUrl = ko.observable(editPageUrl);
    self.deletePageUrl = ko.observable(deletePageUrl);
    self.published = ko.observable(published);
    self.buttonTextPublish = ko.observable(DisplayPages.Published);
    self.buttonTextNotPublish = ko.observable(DisplayPages.Not_Published);

}

function LanguageMdl(id, name) {
    var self = this;
    self.id = ko.observable(id);
    self.name = ko.observable(name);
}