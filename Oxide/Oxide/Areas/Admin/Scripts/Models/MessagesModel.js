﻿ko.bindingHandlers.bootstrapModal = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var props = valueAccessor(),
            vm = bindingContext.createChildContext(viewModel);
        ko.utils.extend(vm, props);
        vm.close = function() {
            vm.show(false);
            vm.onClose();
        };
        vm.action = function() {
            vm.onAction();
        };
        ko.utils.toggleDomNodeCssClass(element, "modal fade", true);
        ko.renderTemplate("deleteConfirmTemplate", vm, null, element);
        var showHide = ko.computed(function() {
            $(element).modal(vm.show() ? "show" : "hide");
        });
        return {
            controlsDescendantBindings: true
        };
    }
};

function MessagesModel(urlBase, deleteUrl, loadMessageUrl) {
    var self = this;
    self.isLoading = ko.observable(false);
    self.totalRecords = ko.observable();
    self.totalPage = ko.observable();
    self.currentPage = ko.observable();
    self.displayinRecords = ko.observable();
    self.nextEnable = ko.observable(true);
    self.previousEnable = ko.observable(true);
    self.messages = ko.observableArray([]);
    self.loadFirst = ko.observable(0);
    self.pageSize = ko.observable(8);
    self.actionFilter = ko.observable("");
    self.actionCurrentPage = ko.observable(0);
    self.actionPageSize = ko.observable(10);
    self.activeMessage = ko.observable("");
    self.activeMessageId = ko.observable("");
    self.activeMessageTitle = ko.observable("");
    self.activeMessageDate = ko.observable("");
    self.activeMessageUserName = ko.observable("");
    self.activeMessageUserImage = ko.observable("");
    self.loadMessages = function(pagesize, page, filter) {
        self.isLoading(true);
        $.get(urlBase, { pageSize: pagesize, page: page, filter: filter }, function(data) {
            self.messages.removeAll();
            for (var i = 0; i <= data.Records.length - 1; i++) {
                var logModel = new MessageModel(
                    data.Records[i].Id,
                    data.Records[i].Title,
                    data.Records[i].MessageBody,
                    data.Records[i].MessageDateTime,
                    data.Records[i].IsMessageRead,
                    data.Records[i].SenderUserName,
                    data.Records[i].SenderUserImage
                );
                self.addMessageItem(logModel);
            }
            self.totalRecords(data.TotalRecords);
            self.totalPage(data.TotalPages);
            self.currentPage(data.CurrentPage);
            self.displayinRecords(data.DisplayingRecords);
            self.nextEnable(data.NextEnable);
            self.previousEnable(data.PreviousEnable);
            self.isLoading(false);
            self.setControls();
            if (self.loadFirst !== 0) {
                self.loadMessageDetails(self.loadFirst());
            }

        });
    };

    self.addMessageItem = function(model) {
        self.messages.push(model);
    }.bind(self);
    self.deleteMessage = function() {
        self.isLoading(true);
        $.get(deleteUrl, { id: self.activeMessageId() }, function(data) {
            var targetMessage = ko.utils.arrayFirst(self.messages(), function(message) {
                return message.id() === self.activeMessageId();
            });
            self.messages.remove(targetMessage);
            self.isLoading(false);
        });
    };

    self.confirmModal = {
        header: ko.observable(Messages.Please_Confirm),
        message: ko.observable(Messages.Do_you_want_to_delete_selected_message_),
        closeLabel: Messages.Cancel,
        primaryLabel: Messages.Delete,
        show: ko.observable(false),
        onClose: function() {

        },
        onAction: function() {
            self.confirmModal.show(false);
            self.deleteMessage();
        }
    };
    self.showConfirmModal = function() {
        self.confirmModal.header(Messages.Please_Confirm, Messages.Warning);
        self.confirmModal.message(Messages.Do_you_want_to_delete_selected_message_);
        self.confirmModal.show(true);
    };

    self.displayMessage = function(message) {
        self.activeMessageId(message.id());
        self.activeMessageTitle(message.title());
        self.activeMessageDate(message.dateTime());
        self.activeMessageUserName(message.senderUser());
        self.activeMessageUserImage(message.senderUserImage());
        self.isLoading(true);
        $.get(loadMessageUrl, { id: message.id() }, function(data) {
            self.activeMessage(data);
            message.status(true);
            self.isLoading(false);
        });
    };

    self.loadMessageDetails = function(id) {
        var message = ko.utils.arrayFirst(self.messages(), function(message) {
            return message.id() === id;
        });
        self.activeMessageId(message.id());
        self.activeMessageTitle(message.title());
        self.activeMessageDate(message.dateTime());
        self.activeMessageUserName(message.senderUser());
        self.activeMessageUserImage(message.senderUserImage());
        self.isLoading(true);
        $.get(loadMessageUrl, { id: message.id() }, function(data) {
            self.activeMessage(data);
            message.status(true);
            self.isLoading(false);
        });
    };

    self.setControls = function() {
        $("#datatable-checkbox_info").html(Messages.Showing + " " + (self.currentPage() + 1) + " / " + self.totalPage());
        $(".dataTables_empty").html("");

        $("#datatable-checkbox_next").unbind();
        $("#datatable-checkbox_previous").unbind();
        $("#datatable-checkbox_next").addClass("disabled");
        $("#datatable-checkbox_previous").addClass("disabled");

        if (self.nextEnable()) {
            $("#datatable-checkbox_next").removeClass("disabled");
            $("#datatable-checkbox_next").click(function() {
                self.actionCurrentPage(self.actionCurrentPage() + 1);
                self.loadData(self.actionPageSize(), self.actionCurrentPage(), self.actionFilter());
            });
        }
        if (self.previousEnable()) {
            $("#datatable-checkbox_previous").removeClass("disabled");
            $("#datatable-checkbox_previous").click(function() {
                self.actionCurrentPage(self.actionCurrentPage() - 1);
                self.loadData(self.actionPageSize(), self.actionCurrentPage(), self.actionFilter());
            });
        }
    }.bind(self);

}

function MessageModel(id, title, message, dateTime, status, senderUser, senderUserImage) {
    var self = this;
    self.id = ko.observable(id);
    self.title = ko.observable(title);
    self.message = ko.observable(message);
    self.dateTime = ko.observable(dateTime);
    self.status = ko.observable(status);
    self.senderUser = ko.observable(senderUser);
    self.senderUserImage = ko.observable(senderUserImage);
}