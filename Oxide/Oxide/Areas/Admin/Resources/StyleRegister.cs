﻿using System.Web.Optimization;
using Oxide.Core.Services.Interface.Registers.ResourceRegister;

namespace Oxide.Areas.Admin.Resources
{
    public class StyleRegister : IResourceRegisterer
    {
        public void RegisterResource(BundleCollection boundleCollection)
        {
            BundleTable.EnableOptimizations = true;
            boundleCollection.Add(
                new StyleBundle("~/AdminContent/css").Include("~/Areas/Admin/vendors/bootstrap/dist/css/bootstrap.css"));
            boundleCollection.Add(
                new StyleBundle("~/AdminContent/fontawesome").Include(
                    "~/Areas/Admin/vendors/font-awesome/css/font-awesome.css"));
            boundleCollection.Add(
                new StyleBundle("~/AdminContent/green").Include("~/Areas/Admin/vendors/iCheck/skins/flat/green.css"));
            boundleCollection.Add(
                new StyleBundle("~/AdminContent/greeCustome").Include("~/Areas/Admin/Content/Styles/greenCustome.css"));
            boundleCollection.Add(
                new StyleBundle("~/AdminContent/iCheckGreen").Include(
                    "~/Areas/Admin/vendors/iCheck/skins/flat/green.css"));
            boundleCollection.Add(
                new StyleBundle("~/AdminContent/pnotify").Include("~/Areas/Admin/vendors/pnotify/dist/pnotify.css"));
            boundleCollection.Add(
                new StyleBundle("~/AdminContent/pnotifyButtons").Include(
                    "~/Areas/Admin/vendors/pnotify/dist/pnotify.buttons.css"));
            boundleCollection.Add(
                new StyleBundle("~/AdminContent/pnotifyNonblock").Include(
                    "~/Areas/Admin/vendors/pnotify/dist/pnotify.nonblock.css"));
            boundleCollection.Add(
                new StyleBundle("~/AdminContent/custom").Include("~/Areas/Admin/Content/Styles/custom.css"));
            boundleCollection.Add(
                new StyleBundle("~/AdminContent/customScrollbar").Include(
                    "~/Areas/Admin/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css"));
            boundleCollection.Add(
                new StyleBundle("~/AdminContent/adminSite").Include("~/Areas/Admin/Content/Styles/adminSite.css"));
            boundleCollection.Add(
                new StyleBundle("~/AdminContent/dataTables-bootstrap").Include(
                    "~/Areas/Admin/vendors/datatables.net-bs/css/dataTables.bootstrap.css"));
            boundleCollection.Add(
                new StyleBundle("~/AdminContent/responsive-bootstrap").Include(
                    "~/Areas/Admin/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.css"));
            boundleCollection.Add(
                new StyleBundle("~/AdminContent/scroller-bootstrap").Include(
                    "~/Areas/Admin/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.css"));
            boundleCollection.Add(
                new StyleBundle("~/AdminContent/switchery").Include("~/Areas/Admin/vendors/switchery/dist/switchery.css"));
            boundleCollection.Add(
                new StyleBundle("~/AdminContent/select2").Include("~/Areas/Admin/vendors/select2/dist/css/select2.css"));
            boundleCollection.Add(
                new StyleBundle("~/AdminContent/ionrangeSlider").Include(
                    "~/Areas/Admin/vendors/select2/ion.rangeSlider/css/ion.rangeSlider.css"));
            boundleCollection.Add(
                new StyleBundle("~/AdminContent/ionrangeSliderskinFlat").Include(
                    "~/Areas/Admin/vendors/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css"));
            boundleCollection.Add(
                new StyleBundle("~/AdminContent/normalize").Include("~/Areas/Admin/vendors/normalize-css/normalize.css"));
            boundleCollection.Add(
                new StyleBundle("~/AdminContent/bootstrapslider").Include(
                    "~/Areas/Admin/Content/Styles/bootstrap-slider"));
            boundleCollection.Add(
                new StyleBundle("~/AdminContent/switch").Include("~/Areas/Admin/Content/Styles/bootstrap-switch.css"));
            boundleCollection.Add(
                new StyleBundle("~/AdminContent/colorpicker").Include(
                    "~/Areas/Admin/vendors/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css"));
            boundleCollection.Add(
                new StyleBundle("~/AdminContent/cropper").Include("~/Areas/Admin/vendors/cropper/dist/cropper.min.css"));
        }
    }
}