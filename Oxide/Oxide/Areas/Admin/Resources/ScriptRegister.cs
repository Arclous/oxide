﻿using System.Web.Optimization;
using Oxide.Core.Services.Interface.Registers.ResourceRegister;

namespace Oxide.Areas.Admin.Resources
{
    public class ScriptRegister : IResourceRegisterer
    {
        public void RegisterResource(BundleCollection boundleCollection)
        {
            boundleCollection.Add(new ScriptBundle("~/bundles/admin").Include("~/Areas/Admin/scripts/admin.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/jqueryx").Include("~/Areas/Admin/vendors/jquery/dist/jquery.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/bootstrap").Include("~/Areas/Admin/vendors/bootstrap/dist/js/bootstrap.js"));
            boundleCollection.Add(new ScriptBundle("~/bundles/custom").Include("~/Areas/Admin/Scripts/custom.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/fastClick").Include("~/Areas/Admin/vendors/fastclick/lib/fastclick.js"));
            boundleCollection.Add(new ScriptBundle("~/bundles/knockout").Include("~/Scripts/knockout-3.4.0*"));
            boundleCollection.Add(new ScriptBundle("~/bundles/knockoutMapping").Include("~/Scripts/knockout.mapping*"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/validator").Include("~/Areas/Admin/vendors/validator/validator.js"));
            boundleCollection.Add(new ScriptBundle("~/bundles/icheck").Include("~/Areas/Admin/vendors/iCheck/icheck.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/pnotify").Include("~/Areas/Admin/vendors/pnotify/dist/pnotify.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/pnotifyButtons").Include(
                    "~/Areas/Admin/vendors/pnotify/dist/pnotify.buttons.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/pnotifyNonblock").Include(
                    "~/Areas/Admin/vendors/pnotify/dist/pnotify.nonblock.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/nprogress").Include("~/Areas/Admin/vendors/nprogress/nprogress.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/bootstrap-progressbar").Include(
                    "~/Areas/Admin/vendors/bootstrap-progressbar/bootstrap-progressbar.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/customScrollBar").Include(
                    "~/Areas/Admin/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/dataTables").Include(
                    "~/Areas/Admin/vendors/datatables.net/js/jquery.dataTables.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/dataTables-bootstrap").Include(
                    "~/Areas/Admin/vendors/datatables.net-bs/js/dataTables.bootstrap.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/dataTables-keyTable").Include(
                    "~/Areas/Admin/vendors/datatables.net-keytable/js/dataTables.keyTable.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/dataTables-responsive").Include(
                    "~/Areas/Admin/vendors/datatables.net-responsive/js/dataTables.responsive.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/responsive-bootstrap").Include(
                    "~/Areas/Admin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/datatables-scroller").Include(
                    "~/Areas/Admin/vendors/datatables.net-scroller/js/datatables.scroller.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/tagsinput").Include(
                    "~/Areas/Admin/vendors/jquery.tagsinput/src/jquery.tagsinput.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/switchery").Include("~/Areas/Admin/vendors/switchery/dist/switchery.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/select2").Include("~/Areas/Admin/vendors/select2/dist/js/select2.full.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/moment").Include("~/Areas/Admin/scripts/moment/moment.min.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/daterangepicker").Include(
                    "~/Areas/Admin/scripts/datepicker/daterangepicker.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/ionrangeSlider").Include(
                    "~/Areas/Admin/vendors/ion.rangeSlider/js/ion.rangeSlider.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/bootstrapslider").Include("~/Areas/AdminScripts/bootstrap-slider.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/pnotify").Include("~/Areas/Admin/vendors/pnotify/dist/pnotify.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/pnotifybuttons").Include(
                    "~/Areas/Admin/vendors/pnotify/dist/pnotify.buttons.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/pnotifybuttons").Include(
                    "~/Areas/Admin/vendors/pnotify/dist/pnotify.buttons.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/bootstrap-switch").Include("~/Areas/Admin/Scripts/bootstrap-switch.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/jqueryui").Include("~/Areas/Admin/Scripts/jquery-ui.min.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/knockoutsortable").Include("~/Areas/Admin/Scripts/knockout-sortable.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/lazyLoad").Include("~/Areas/Admin/Scripts/jquery.lazyload.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/ckeditor").Include("~/Areas/Admin/Scripts/ckeditor/ckeditor.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/ckeditoradapter").Include(
                    "~/Areas/Admin/Scripts/ckeditor/adapters/jquery.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/highlight").Include(
                    "~/Areas/Admin/Scripts/ckeditor/plugins/codesnippet/lib/highlight/highlight.pack.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/colorpicker").Include(
                    "~/Areas/Admin/vendors/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"));
            boundleCollection.Add(
                new ScriptBundle("~/bundles/cropper").Include("~/Areas/Admin/vendors/cropper/dist/cropper.min.js"));
        }
    }
}