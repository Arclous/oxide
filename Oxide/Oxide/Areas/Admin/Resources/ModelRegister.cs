﻿using System.Web.Optimization;
using Oxide.Core.Services.Interface.Registers.ResourceRegister;

namespace Oxide.Areas.Admin.Resources
{
    public class ModelRegister : IResourceRegisterer
    {
        public void RegisterResource(BundleCollection boundleCollection)
        {
            boundleCollection.Add(
                new ScriptBundle("~/models/LeftMenuModel").Include("~/Areas/Admin/Scripts/Models/LeftMenuModel.js"));
            boundleCollection.Add(
                new ScriptBundle("~/models/PagesModel").Include("~/Areas/Admin/Scripts/Models/PagesModel.js"));
            boundleCollection.Add(
                new ScriptBundle("~/models/LayoutModel").Include("~/Areas/Admin/Scripts/Models/LayoutModel.js"));
            boundleCollection.Add(
                new ScriptBundle("~/models/WidgetsModel").Include("~/Areas/Admin/Scripts/Models/WidgetModel.js"));
            boundleCollection.Add(
                new ScriptBundle("~/models/WidgetPageModel").Include("~/Areas/Admin/Scripts/Models/WidgetPageModel.js"));
            boundleCollection.Add(
                new ScriptBundle("~/models/ModuleModel").Include("~/Areas/Admin/Scripts/Models/ModuleModel.js"));
            boundleCollection.Add(
                new ScriptBundle("~/models/ThemeModel").Include("~/Areas/Admin/Scripts/Models/ThemeModel.js"));
            boundleCollection.Add(
                new ScriptBundle("~/models/WidgetPageModelMain").Include(
                    "~/Areas/Admin/Scripts/Models/WidgetPageModelMain.js"));
            boundleCollection.Add(
                new ScriptBundle("~/models/TreeView").Include("~/Areas/Admin/Scripts/Models/Common/Treeview.js"));
            boundleCollection.Add(
                new ScriptBundle("~/models/ContentSelecter").Include(
                    "~/Areas/Admin/Scripts/Models/Common/ContentModel.js"));
            boundleCollection.Add(
                new ScriptBundle("~/models/ContentSelecterMedia").Include(
                    "~/Areas/Admin/Scripts/Models/Common/ContentModelMedia.js"));
            boundleCollection.Add(
                new ScriptBundle("~/models/CacheItemsModel").Include("~/Areas/Admin/Scripts/Models/CacheItemsModel.js"));
            boundleCollection.Add(
                new ScriptBundle("~/models/AdminUsersModel").Include("~/Areas/Admin/Scripts/Models/AdminUsersModel.js"));
            boundleCollection.Add(
                new ScriptBundle("~/models/AdminRolesModel").Include("~/Areas/Admin/Scripts/Models/AdminRolesModel.js"));
            boundleCollection.Add(
                new ScriptBundle("~/models/AdminPermissionsModel").Include(
                    "~/Areas/Admin/Scripts/Models/AdminPermissionsModel.js"));
            boundleCollection.Add(
                new ScriptBundle("~/models/AdminUserRolesModel").Include(
                    "~/Areas/Admin/Scripts/Models/AdminUserRolesModel.js"));
            boundleCollection.Add(
                new ScriptBundle("~/models/ScheduledJobModel").Include(
                    "~/Areas/Admin/Scripts/Models/ScheduledJobModel.js"));
            boundleCollection.Add(
                new ScriptBundle("~/models/LogsModel").Include("~/Areas/Admin/Scripts/Models/LogsModel.js"));
            boundleCollection.Add(
                new ScriptBundle("~/models/DataList").Include("~/Areas/Admin/Scripts/Models/Common/DataListModel.js"));
            boundleCollection.Add(
                new ScriptBundle("~/models/ImageContentSelecter").Include(
                    "~/Areas/Admin/Scripts/Models/Common/ImageContentModel.js"));
            boundleCollection.Add(
                new ScriptBundle("~/models/EmailTemplatesModel").Include(
                    "~/Areas/Admin/Scripts/Models/EmailTemplateModel.js"));
            boundleCollection.Add(
                new ScriptBundle("~/models/DashboardModel").Include("~/Areas/Admin/Scripts/Models/DashboardModel.js"));
            boundleCollection.Add(
                new ScriptBundle("~/models/DashboardWidgetsModel").Include(
                    "~/Areas/Admin/Scripts/Models/DashboardWidgetModel.js"));
            boundleCollection.Add(
                new ScriptBundle("~/models/LayoutPreviewModel").Include(
                    "~/Areas/Admin/Scripts/Models/LayoutPreviewModel.js"));
            boundleCollection.Add(
                new ScriptBundle("~/models/MessagesModel").Include("~/Areas/Admin/Scripts/Models/MessagesModel.js"));
            boundleCollection.Add(
                new ScriptBundle("~/models/QuickMessagesModel").Include(
                    "~/Areas/Admin/Scripts/Models/QuickMessagesModel.js"));
        }
    }
}