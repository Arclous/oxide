﻿namespace Oxide.Areas.Admin
{
    public class Module
    {
        public int Id { get; } = 0;

        public string Name { get; } = "Oxide";

        public string Title { get; } = "Oxide";

        public string Description { get; } = "Oxide";

        public string Author { get; } = "Inevera Studios";

        public string Category { get; } = "Oxide";

        public string PreviewImageUrl { get; } = "Oxide";

        public bool Active { get; set; }
    }
}