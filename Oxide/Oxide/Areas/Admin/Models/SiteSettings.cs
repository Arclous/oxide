﻿using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Areas.Admin.Models
{
    [OxideDatabase("OxideContext")]
    public class SiteSettings : OxideDatabaseModel, IOxideDataBaseModel
    {
        public int DefaultSiteCultureId { get; set; }
        public virtual Language DefaultSiteCulture { get; set; }
        public int DefaultTimeZoneId { get; set; }
        public virtual TimeZoneItem DefaultTimeZone { get; set; }
    }
}