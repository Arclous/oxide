﻿using System;
using Oxide.Core.Syntax;

namespace Oxide.Areas.Admin.Models
{
    public class SitemapNode
    {
        public Syntax.SiteMapFrequncy? Frequency { get; set; }
        public DateTime? LastModified { get; set; }
        public double? Priority { get; set; }
        public string Url { get; set; }
    }
}