﻿using System.ComponentModel.DataAnnotations;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Areas.Admin.Models
{
    [OxideDatabase("OxideContext")]
    public class TimeZoneItem : OxideDatabaseModel, IOxideDataBaseModel
    {
        public double Offset { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 1,
            ErrorMessage = "Zone Id of the time zone needs be between 1-255 characters.")]
        [DataType(DataType.Text, ErrorMessage = "Zone Id of the time zone needs be text format")]
        public string TimeZoneId { get; set; }

        [Required]
        [StringLength(255, MinimumLength = 1,
            ErrorMessage = "Standard Name of the time zone needs be between 1-255 characters.")]
        [DataType(DataType.Text, ErrorMessage = "Standard Name of the time zone needs be text format")]
        public string StandardName { get; set; }
    }
}