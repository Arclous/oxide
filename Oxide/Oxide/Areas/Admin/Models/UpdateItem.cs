﻿using System;
using System.ComponentModel.DataAnnotations;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Areas.Admin.Models
{
    [OxideDatabase("OxideContext")]
    public class UpdateItem : OxideDatabaseModel, IOxideDataBaseModel
    {
        [Required]
        [StringLength(255, MinimumLength = 1, ErrorMessage = "Key of the update item needs be between 1-255 characters."
            )]
        [DataType(DataType.Text, ErrorMessage = "Key of the update item needs be text format")]
        public string Key { get; set; }

        [Required]
        [DataType(DataType.DateTime, ErrorMessage = "Update Date Time of the update item needs be text format")]
        public DateTime UpdateDateTime { get; set; }
    }
}