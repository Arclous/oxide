﻿using System.ComponentModel.DataAnnotations;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Areas.Admin.Models
{
    [OxideDatabase("OxideContext")]
    public class Language : OxideDatabaseModel, IOxideDataBaseModel
    {
        [Required]
        [StringLength(10, MinimumLength = 2, ErrorMessage = "Code of the language needs be between 1-10 characters.")]
        [DataType(DataType.Text, ErrorMessage = "Code of the language needs be text format")]
        public string Code { get; set; }

        [Required]
        [StringLength(10, MinimumLength = 3, ErrorMessage = "Code of the language needs be between 1-10 characters.")]
        [DataType(DataType.Text, ErrorMessage = "Code of the language needs be text format")]
        public string Code3 { get; set; }

        [Required]
        [StringLength(255, MinimumLength = 1,
            ErrorMessage = "Native Name of the language needs be between 1-255 characters.")]
        [DataType(DataType.Text, ErrorMessage = "Native Nameof the language needs be text format")]
        public string Native { get; set; }

        public bool Active { get; set; }
    }
}