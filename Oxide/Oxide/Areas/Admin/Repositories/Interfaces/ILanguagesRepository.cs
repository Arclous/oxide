﻿using Oxide.Areas.Admin.Models;
using Oxide.Core.Data.Repository;

namespace Oxide.Areas.Admin.Repositories.Interfaces
{
    public interface ILanguagesRepository : IOxideRepository<Language>
    {
    }
}