﻿using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using Oxide.Core.Data.Context;
using Oxide.Core.Managers.OxideMigrationManager;
using Oxide.Core.Syntax;
using Oxide.Migrations;

namespace Oxide.Data
{
    public class AutoMigration : IOxideMigrationManager
    {
        public void UpdateDatabase()
        {
            using (var oxideContext = new OxideContext())
            {
                oxideContext.Database.CreateIfNotExists();
                var configuration = new Configuration
                {
                    TargetDatabase =
                        new DbConnectionInfo(oxideContext.Database.Connection.ConnectionString,
                            Syntax.ProviderNames.SystemDataSqlClient)
                };
                var migrator = new DbMigrator(configuration);
                migrator.Update();
            }
        }
    }
}