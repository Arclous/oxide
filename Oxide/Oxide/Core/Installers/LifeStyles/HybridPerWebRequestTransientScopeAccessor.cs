﻿namespace Oxide.Core.Installers.LifeStyles
{
    public class HybridPerWebRequestTransientScopeAccessor : HybridPerWebRequestScopeAccessor
    {
        public HybridPerWebRequestTransientScopeAccessor() : base(new TransientScopeAccessor())
        {
        }
    }
}