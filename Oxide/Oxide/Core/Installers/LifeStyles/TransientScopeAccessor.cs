﻿using Castle.MicroKernel.Context;
using Castle.MicroKernel.Lifestyle.Scoped;

namespace Oxide.Core.Installers.LifeStyles
{
    public class TransientScopeAccessor : IScopeAccessor
    {
        public ILifetimeScope GetScope(CreationContext context)
        {
            return new DefaultLifetimeScope();
        }
        public void Dispose()
        {
        }
    }
}