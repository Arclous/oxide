﻿using System.Web;
using Castle.MicroKernel.Context;
using Castle.MicroKernel.Lifestyle;
using Castle.MicroKernel.Lifestyle.Scoped;

namespace Oxide.Core.Installers.LifeStyles
{
    public class HybridPerWebRequestScopeAccessor : IScopeAccessor
    {
        private readonly IScopeAccessor secondaryScopeAccessor;
        private readonly IScopeAccessor webRequestScopeAccessor = new WebRequestScopeAccessor();
        public HybridPerWebRequestScopeAccessor(IScopeAccessor secondaryScopeAccessor)
        {
            this.secondaryScopeAccessor = secondaryScopeAccessor;
        }
        public ILifetimeScope GetScope(CreationContext context)
        {
            if (HttpContext.Current != null && PerWebRequestLifestyleModuleUtils.IsInitialized)
                return webRequestScopeAccessor.GetScope(context);
            return secondaryScopeAccessor.GetScope(context);
        }
        public void Dispose()
        {
            webRequestScopeAccessor.Dispose();
            secondaryScopeAccessor.Dispose();
        }
    }
}