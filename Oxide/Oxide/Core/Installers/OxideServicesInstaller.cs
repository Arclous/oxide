﻿using System;
using Castle.MicroKernel.Lifestyle;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Oxide.Core.Base.OxideDependency;
using Oxide.Core.Syntax;

namespace Oxide.Core.Installers
{
    public class OxideServicesInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            var path = string.Format(AppDomain.CurrentDomain.BaseDirectory + "{0}", Constants.Bin);
            var assemblyFilter = new AssemblyFilter(path, Constants.DllPatern);
            container.Register(
                Classes.FromAssemblyInDirectory(assemblyFilter)
                    .BasedOn(typeof (IOxideDependency))
                    .WithService.AllInterfaces()
                    .LifestyleCustom<ScopedLifestyleManager>());
        }
    }
}