﻿using Castle.MicroKernel.Lifestyle;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Oxide.Core.Data.Repository;

namespace Oxide.Core.Installers
{
    public class OxideRepositoryInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For(typeof (IOxideRepository<>))
                    .ImplementedBy(typeof (OxideRepository<>))
                    .LifestyleCustom<ScopedLifestyleManager>());
        }
    }
}