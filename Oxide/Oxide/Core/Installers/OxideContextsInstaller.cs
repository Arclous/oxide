﻿using System.Data.Entity;
using Castle.MicroKernel.Lifestyle;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Oxide.Core.Data.Context;

namespace Oxide.Core.Installers
{
    public class OxideContextsInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For(typeof (DbContext))
                    .ImplementedBy(typeof (OxideContext))
                    .LifestyleCustom<ScopedLifestyleManager>());

            //foreach (var instance in from a in Oxide.LoadedAssemblies
            //    from t in
            //        a.GetTypes()
            //            .Where(
            //                x =>
            //                    x.GetInterfaces().Contains(typeof (IOxideBaseContext)) &&
            //                    x.GetConstructor(Type.EmptyTypes) != null)
            //    select Activator.CreateInstance(t) as IOxideBaseContext)
            //{
            //    container.Register(Component.For(instance.GetType()).LifeStyle.PerWebRequest);
            //    Oxide.LoadedContexts.Add(instance);
            //}
            //container.Register(Component.For(typeof (IOxideRepository<>)).UsingFactoryMethod((kernel, context) =>
            //{
            //    var genericType = context.RequestedType.GetGenericArguments()[0];
            //    var type = typeof (OxideRepository<>).MakeGenericType(genericType);
            //    var repository = Activator.CreateInstance(type);
            //    var dbContextProperty = type.GetProperty(OxideSystem.DbContext);
            //    var attribute =
            //        genericType.CustomAttributes.FirstOrDefault(x => x.AttributeType.Name == OxideSystem.OxideDatabase);
            //    if (dbContextProperty != null)
            //    {
            //        var regInstance =
            //            Oxide.LoadedContexts.Find(
            //                x =>
            //                    attribute != null &&
            //                    x.GetType().Name == (string) attribute.ConstructorArguments[0].Value);
            //        if (regInstance != null)
            //            dbContextProperty.SetValue(repository, kernel.Resolve(regInstance.GetType()));
            //    }
            //    return repository;
            //}).LifeStyle.PerWebRequest);
        }
    }
}