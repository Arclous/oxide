﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using Oxide.Core.Elements;
using Oxide.Core.Helpers;
using Oxide.Core.Models.Base;

namespace Oxide.Core.Managers.AlternateManager
{
    public class AlternateManager : IAlternateManager
    {
        public void RegisterWidgetAlternate(IOxideWidget target, IOxideWidget alternate,
            Syntax.Syntax.AlternateTypes alternateTypes, bool modifyContext = false)
        {
            var targetWidgetKey = OxideWidgetHelper.GetWidgetUniqueKey(target);
            if (Oxide.WidgetAlternates.ContainsKey(targetWidgetKey)) return;
            target.WidgetKey = targetWidgetKey;
            alternate.WidgetKey = OxideWidgetHelper.GetWidgetUniqueKey(alternate);
            var alternateWidget = new AlternateWidget
            {
                Widget = alternate,
                AlternateTypes = alternateTypes,
                ModifyContext = modifyContext
            };
            Oxide.WidgetAlternates.Add(targetWidgetKey, alternateWidget);
        }
        public void RegisterAlternateView(string targetViewName, string alternateViewName)
        {
            if (Oxide.ViewAlternates.ContainsKey(targetViewName)) return;
            Oxide.ViewAlternates.Add(targetViewName, alternateViewName);
        }
    }
}