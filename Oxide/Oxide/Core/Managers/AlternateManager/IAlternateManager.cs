﻿using Oxide.Core.Base.OxideDependency;
using Oxide.Core.Elements;

namespace Oxide.Core.Managers.AlternateManager
{
    /// <summary>
    ///     Responsible for alternate process
    /// </summary>
    public interface IAlternateManager : IOxideDependency
    {
        /// <summary>
        ///     Set alternate widget
        /// </summary>
        /// <param name="target">Target widget which will change with alternate</param>
        /// <param name="alternate">Alternate widget which will be using instead of original one</param>
        /// <param name="alternateTypes">Type of the alternate process</param>
        /// <param name="modifyContext">Modify context or not</param>
        void RegisterWidgetAlternate(IOxideWidget target, IOxideWidget alternate,
            Syntax.Syntax.AlternateTypes alternateTypes, bool modifyContext = false);
        /// <summary>
        ///     Set alternate widget
        /// </summary>
        /// <param name="targetViewName">Target view which will change with alternate</param>
        /// <param name="alternateViewName">Alternate view which will be using instead of orginal one</param>
        void RegisterAlternateView(string targetViewName, string alternateViewName);
    }
}