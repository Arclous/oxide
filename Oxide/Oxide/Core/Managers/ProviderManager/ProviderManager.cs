﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using Castle.MicroKernel.Lifestyle;

namespace Oxide.Core.Managers.ProviderManager
{
    public class ProviderManager : IProviderManager
    {
        public T Provide<T>() where T : class
        {
            Oxide.Container.BeginScope();
            return Oxide.Container.Kernel.Resolve<T>();
        }
        public T[] ProvideAll<T>() where T : class
        {
            Oxide.Container.BeginScope();
            return Oxide.Container.Kernel.ResolveAll<T>();
        }
        public object Provide(Type service)
        {
            Oxide.Container.BeginScope();
            return Oxide.Container.Kernel.Resolve(service);
        }
    }
}