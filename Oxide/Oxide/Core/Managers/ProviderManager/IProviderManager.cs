﻿using System;
using Oxide.Core.Base.OxideDependency;

namespace Oxide.Core.Managers.ProviderManager
{
    public interface IProviderManager : IOxideDependency
    {
        T Provide<T>() where T : class;
        T[] ProvideAll<T>() where T : class;
        object Provide(Type service);
    }
}