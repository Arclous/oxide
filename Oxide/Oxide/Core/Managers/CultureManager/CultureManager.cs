﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Oxide.Core.Managers.CacheManager;
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.Models.Base;
using Oxide.Core.Services.LanguageService;
using Oxide.Core.Services.SiteSettings;
using Oxide.Core.Syntax;
using Oxide.Core.ViewModels.GeneralViewModels;

namespace Oxide.Core.Managers.CultureManager
{
    public class CultureManager : ICultureManager
    {
        private readonly ILanguageService _languageService;
        private readonly IOxideCacheManager _oxideCacheManager;
        private readonly IProviderManager _providerManager;
        public CultureManager(IProviderManager providerManager, IOxideCacheManager oxideCacheManager,
            ILanguageService languageService)
        {
            _providerManager = providerManager;
            _oxideCacheManager = oxideCacheManager;
            _languageService = languageService;
        }
        public int GetFrontCurrentCultureId()
        {
            var cultureInfo = GetFrontCurrentCulture();
            var languageId =
                _oxideCacheManager.GetOrSet(Constants.FrontLanguageId + cultureInfo.ThreeLetterISOLanguageName,
                    Constants.FrontLanguageId, Constants.FrontLanguage,
                    () => _languageService.GetLanguage(cultureInfo.ThreeLetterISOLanguageName), CacheLength.Day, false);
            return languageId.Id;
        }
        public CultureInfo GetFrontCurrentCulture()
        {
            return _oxideCacheManager.GetOrSet(Constants.FrontLanguage, Constants.FrontLanguage, Constants.FrontLanguage,
                GetFrontCulture, CacheLength.Day, false);
            ;
        }
        public void SetFrontCulture(CultureInfo culture, HttpResponseBase response)
        {
            var currentCookie = HttpContext.Current.Request.Cookies[Constants.FrontLanguage];
            if (currentCookie != null)
            {
                currentCookie.Value = culture.TwoLetterISOLanguageName;
                response.AppendCookie(currentCookie);
            }
            else
            {
                var langCookie = new HttpCookie(Constants.FrontLanguage, culture.TwoLetterISOLanguageName)
                {
                    HttpOnly = true
                };
                response.AppendCookie(langCookie);
            }
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;
            GetFrontCurrentCulture();
        }

        private CultureInfo GetFrontCulture()
        {
            var siteSettingsService = _providerManager.Provide<ISiteSettingsService>();
            var handler = HttpContext.Current.Request.RequestContext.HttpContext.Handler as MvcHandler;
            var routeData = handler?.RequestContext.RouteData;
            var routeCulture = routeData?.Values[Constants.LanguageCulture]?.ToString();
            var languageCookie = HttpContext.Current.Request.Cookies[Constants.FrontLanguage];
            var userLanguages = HttpContext.Current.Request.UserLanguages;
            var defaultSetting = siteSettingsService.GetSettingsWithCulture();
            var defaultCulture = defaultSetting != null
                ? defaultSetting.DefaultSiteCulture.Code
                : Constants.DefaultLanguageCode;

            // Set the Culture based on a route, a cookie or the browser settings,
            // or default value if something went wrong
            var cultureInfo =
                new CultureInfo(routeCulture ??
                                (languageCookie != null
                                    ? languageCookie.Value
                                    : userLanguages != null ? userLanguages[0] : defaultCulture));
            return cultureInfo;
        }
    }
}