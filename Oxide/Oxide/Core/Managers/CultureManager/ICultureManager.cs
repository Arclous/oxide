﻿using System.Globalization;
using System.Web;
using Oxide.Core.Base.OxideDependency;
using Oxide.Core.ViewModels.GeneralViewModels;

namespace Oxide.Core.Managers.CultureManager
{
    public interface ICultureManager : IOxideDependency
    {
        int GetFrontCurrentCultureId();
        CultureInfo GetFrontCurrentCulture();
        void SetFrontCulture(CultureInfo culture, HttpResponseBase response);

    }
}