﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Data.Context;
using Oxide.Core.Data.Repository;
using Oxide.Core.Models.AdminPermission;
using Oxide.Core.Models.AdminRole;
using Oxide.Core.Services.AdminPermissionService;

namespace Oxide.Core.Managers.AttiributeManager
{
    public static class AttiributeManager
    {
        /// <summary>
        ///     Organize all admin permissions
        /// </summary>
        public static void OrganizeAdminRoles()
        {
            var foundPermissions = GetPermissions();
            SavePermissions(foundPermissions);
        }
        /// <summary>
        ///     Gets all permission attiributes
        /// </summary>
        private static IEnumerable<AdminPermission> GetPermissions()
        {
            var foundPermissions = new List<AdminPermission>();
            foreach (var controlleractionlist in
                Oxide.LoadedAssemblies.Select(
                    asm =>
                        asm.GetTypes()
                            .Where(type => typeof (Controller).IsAssignableFrom(type))
                            .SelectMany(
                                type =>
                                    type.GetMethods(BindingFlags.Instance | BindingFlags.DeclaredOnly |
                                                    BindingFlags.Public))
                            .Where(m => m.GetCustomAttributes(typeof (OxideAdminPermission), true).Any())
                            .Select(
                                x =>
                                    x.DeclaringType != null
                                        ? new
                                        {
                                            Controller = x.DeclaringType.Name,
                                            Action = x.Name,
                                            Module = x.Module.Name,
                                            ReturnType = x.ReturnType.Name,
                                            Attr = x.GetCustomAttribute<OxideAdminPermission>()
                                        }
                                        : null)
                            .OrderBy(x => x.Controller)
                            .ThenBy(x => x.Action)
                            .ToList()))
            {
                foundPermissions.AddRange(
                    controlleractionlist.Select(
                        v =>
                            new AdminPermission
                            {
                                Key = v.Attr.Key,
                                Owner = AssemblyManager.AssemblyManager.GetAppropriateModuleName(v.Module),
                                Title = v.Attr.Permission,
                                Name = v.Attr.Permission,
                                Group = v.Attr.Group
                            })
                        .Where(
                            role =>
                                AssemblyManager.AssemblyManager.IsModuleActive(role.Owner) &&
                                foundPermissions.All(x => x.Key != role.Key)));
            }
            return foundPermissions;
        }
        /// <summary>
        ///     Saves permissions
        /// </summary>
        /// <param name="permissions">Permissions to save</param>
        private static void SavePermissions(IEnumerable<AdminPermission> permissions)
        {
            using (var context = new OxideContext())
            {
                var permissionService = new AdminPermissionServices(new OxideRepository<AdminPermission>(context),
                    new OxideRepository<AdminRolePermission>(context));
                foreach (var permission in
                    permissions.Where(
                        permission => !permissionService.IsAdminPermissionExist(permission.Key, permission.Owner)))
                {
                    //Check if the permission key is exist
                    if (permissionService.IsAdminPermissionExist(permission.Key))
                        throw new ArgumentException("Permission key already added, key has to be unique");
                    permissionService.CreateAdminPermission(permission);
                }
            }
        }
    }
}