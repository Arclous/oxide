﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Linq;
using Oxide.Core.Elements;
using Oxide.Core.Services.ModuleService;

namespace Oxide.Core.Managers.LayoutManager
{
    public class LayoutManager : ILayoutManager
    {
        private readonly IModuleService _moduleService;
        public LayoutManager(IModuleService moduleService)
        {
            _moduleService = moduleService;
        }
        public void LoadLayouts()
        {
            foreach (var layout in AssemblyManager.AssemblyManager.GetFor<IOxideLayout>())
            {
                if (Oxide.LoadedLayouts.Any(x => x.Id == layout.Id && x.LayoutType == layout.LayoutType))
                    throw new Exception("Layout id is already using for another layout.Layout: " + layout.Name);
                if (!Oxide.LoadedLayouts.Contains(layout))
                    Oxide.LoadedLayouts.Add(layout);
            }
        }
        public void LoadLayouts(string moduleName)
        {
            foreach (var layout in AssemblyManager.AssemblyManager.GetFor<IOxideLayout>(moduleName))
            {
                if (Oxide.LoadedLayouts.Any(x => x.Id == layout.Id && x.LayoutType == layout.LayoutType))
                    throw new Exception("Layout id is already using for another layout.Layout: " + layout.Name);
                if (!Oxide.LoadedLayouts.Contains(layout))
                    Oxide.LoadedLayouts.Add(layout);
            }
        }
        public IOxideLayout GetMasterLayout(int id)
        {
            return Oxide.LoadedLayouts.FirstOrDefault(x => x.LayoutType == Syntax.Syntax.LayoutTypes.Main && x.Id == id);
        }
        public IOxideLayout GetMasterLayout(string layoutName)
        {
            return
                Oxide.LoadedLayouts.FirstOrDefault(
                    x => x.LayoutType == Syntax.Syntax.LayoutTypes.Main && x.Name == layoutName);
        }
        public IOxideLayout GetInnerLayout(int id)
        {
            return Oxide.LoadedLayouts.FirstOrDefault(x => x.LayoutType == Syntax.Syntax.LayoutTypes.Inner && x.Id == id);
        }
        public IOxideLayout GetInnerLayout(string layoutName)
        {
            return
                Oxide.LoadedLayouts.FirstOrDefault(
                    x => x.LayoutType == Syntax.Syntax.LayoutTypes.Inner && x.Name == layoutName);
        }
    }
}