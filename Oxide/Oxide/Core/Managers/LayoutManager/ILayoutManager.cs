﻿using Oxide.Core.Base.OxideDependency;
using Oxide.Core.Elements;

namespace Oxide.Core.Managers.LayoutManager
{
    public interface ILayoutManager : IOxideDependency
    {
        void LoadLayouts();
        void LoadLayouts(string moduleName);
        IOxideLayout GetMasterLayout(int id);
        IOxideLayout GetMasterLayout(string layoutName);
        IOxideLayout GetInnerLayout(int id);
        IOxideLayout GetInnerLayout(string layoutName);
    }
}