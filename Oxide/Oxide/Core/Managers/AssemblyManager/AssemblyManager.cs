﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Oxide.Core.Syntax;
using Module = Oxide.Core.Models.Modules.Module;

namespace Oxide.Core.Managers.AssemblyManager
{
    public static class AssemblyManager
    {
        public static List<Module> ModuleActivationList = new List<Module>();
        /// <summary>
        ///     Loads all assemblied for solution
        /// </summary>
        public static void LoadAssemblies()
        {
            var path = string.Format(AppDomain.CurrentDomain.BaseDirectory + "{0}", "bin");
            var tempList = new List<Assembly>();
            tempList.AddRange(Directory.GetFiles(path, "*.dll").Select(Assembly.LoadFrom));
            Oxide.LoadedAssemblies = tempList.ToArray();
            if (Oxide.LoadedAssemblies == null)
                throw new ArgumentException("Parameter cannot be null", "Loaded Assemblies");
        }
        /// <summary>
        ///     Gets all modules for solution
        /// </summary>
        public static List<T> GetModules<T>() where T : class
        {
            return (from a in Oxide.LoadedAssemblies
                from t in
                    a.GetTypes()
                        .Where(x => x.GetInterfaces().Contains(typeof (T)) && x.GetConstructor(Type.EmptyTypes) != null)
                select Activator.CreateInstance(t) as T).ToList();
        }
        /// <summary>
        ///     Gets all implemented class from the generic interface in the solution for the activated modules.
        ///     If the module is not active, none of the class will return
        /// </summary>
        public static T[] GetFor<T>() where T : class
        {
            return (from a in Oxide.LoadedAssemblies
                from t in
                    a.GetTypes()
                        .Where(x => x.GetInterfaces().Contains(typeof (T)) && x.GetConstructor(Type.EmptyTypes) != null)
                select Activator.CreateInstance(t) as T).Where(
                    instance => IsModuleActive(instance.GetType().Module.Name)).ToArray();
        }
        /// <summary>
        ///     Gets all implemented class from the generic interface in the solution for the all modules.
        ///     If the module is active or not, class will return
        /// </summary>
        public static T[] GetAllFor<T>() where T : class
        {
            try
            {
                return (from a in Oxide.LoadedAssemblies
                        from t in
                            a.GetTypes()
                                .Where(x => (x.GetInterfaces().Contains(typeof(T)) && x.GetConstructor(Type.EmptyTypes) != null))
                        select Activator.CreateInstance(t) as T).ToArray();
            }
            catch (Exception ex)
            {
                return null;
            }
     
        }
        /// <summary>
        ///     Gets all implemented class from the generic interface in the solution for the specific modules.
        /// </summary>
        public static T[] GetFor<T>(string moduleName) where T : class
        {
            var moduleFile = string.Format(AppDomain.CurrentDomain.BaseDirectory + "{0}" + "\\" + "{1}.dll", "bin",
                moduleName);
            return
                (from t in
                    Assembly.LoadFrom(moduleFile)
                        .GetTypes()
                        .Where(x => x.GetInterfaces().Contains(typeof (T)) && x.GetConstructor(Type.EmptyTypes) != null)
                    select Activator.CreateInstance(t) as T).ToArray();
        }
        public static bool IsModuleActive(string nameSpace)
        {
            return nameSpace.Replace(".dll", "") == Constants.DefaultNameSpace ||
                   ModuleActivationList.Any(x => x.Namespace == nameSpace.Replace(".dll", "") && x.Active);
        }
        public static string GetAppropriateModuleName(string name)
        {
            return name.Replace(Constants.Dll, Constants.Empty);
        }
    }
}