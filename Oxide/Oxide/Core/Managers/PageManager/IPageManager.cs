﻿using Oxide.Core.Base.OxideDependency;
using Oxide.Core.Models.Pages;
using Oxide.Core.ViewModels.PageViewModels;

namespace Oxide.Core.Managers.PageManager
{
    public interface IPageManager : IOxideDependency
    {
        RenderPage GetRenderPageModel(Page page );
    }
}