﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Oxide.Core.Helpers;
using Oxide.Core.Managers.CacheManager;
using Oxide.Core.Managers.CultureManager;
using Oxide.Core.Managers.WidgetManager;
using Oxide.Core.Models.Pages;
using Oxide.Core.Services.PageService;
using Oxide.Core.Services.WidgetService;
using Oxide.Core.Syntax;
using Oxide.Core.ViewModels.PageViewModels;

namespace Oxide.Core.Managers.PageManager
{
    public class PageManager : IPageManager
    {
        private readonly IOxideCacheManager _cacheManager;
        private readonly ICultureManager _cultureManager;
        private readonly IPageService _pageService;
        private readonly IWidgetManager _widgetManager;
        private readonly IWidgetService _widgetService;
        public PageManager(IPageService pageService, IWidgetManager widgetManager, IWidgetService widgetService,
            IOxideCacheManager cacheManager, ICultureManager cultureManager)
        {
            _pageService = pageService;
            _widgetManager = widgetManager;
            _widgetService = widgetService;
            _cacheManager = cacheManager;
            _cultureManager = cultureManager;
        }
        public RenderPage GetRenderPageModel(Page page)
        {
            var layout =
                Oxide.LoadedLayouts.FirstOrDefault(
                    x => x.LayoutType == Syntax.Syntax.LayoutTypes.Inner && x.Id == page.InnerLayoutId);
            var mainlayout =
                Oxide.LoadedLayouts.FirstOrDefault(
                    x => x.LayoutType == Syntax.Syntax.LayoutTypes.Main && x.Id == page.MainLayoutId);
            var mainLayoutName = "";
            if (mainlayout != null)
                mainLayoutName = mainlayout.Name;

            var currentCultureId =  _cultureManager.GetFrontCurrentCultureId();
   
            if (layout != null)
            {
                var renderPage = new RenderPage
                {
                    Page = page,
                    InnerElements = new Dictionary<string, ElementModel>(),
                    MainElements = new Dictionary<string, ElementModelMain>(),
                    InnerPageLayout = layout.View,
                    //Add Meta Tags
                    Meta =
                        new Dictionary<string, string>
                        {
                            {Constants.Description, page.Description},
                            {Constants.Keywords, page.Tag}
                        },
                    LanguageCode=page.Language.Code
                };
                foreach (
                    var v in OxideLayoutHelper.GetElementsForRender(page.InnerLayoutId, Syntax.Syntax.LayoutTypes.Inner)
                    )
                {
                    renderPage.InnerElements.Add(v.Title, v);
                }
                foreach (
                    var v in
                        OxideLayoutHelper.GetMainElementsForRender(page.MainLayoutId, Syntax.Syntax.LayoutTypes.Main))
                {
                    renderPage.MainElements.Add(v.Title, v);
                }

                var currectCulture = _cultureManager.GetFrontCurrentCulture();
         
                foreach (var element in renderPage.InnerElements)
                {
                    var cacheKey = string.Format(@"{0}_Page_Widgets_{1}_{2}_{3}", page.Name, page.Id, element.Value.Id,
                        currentCultureId);
                    var cacheTitle = string.Format(@"{1} {0} {4} On {2} - {3}", Constants.PageWidgetsOn, page.Name,
                        element.Key, currectCulture.NativeName, layout.Name);
                    var widgets = _cacheManager.GetOrSet(cacheKey, cacheTitle, "Inner Widgets",
                        () => _widgetService.GetPageWidgetsForRender(page.Id, element.Value.Id));
                    element.Value.Widgets = widgets;
                }
                foreach (var element in renderPage.MainElements)
                {
                    var cacheKey = string.Format(@"{0}_Main_Widgets_{1}_{2}_{3}_{4}", layout.Name, page.MainLayoutId,
                        element.Value.Id, currentCultureId, page.Id);
                    var cacheTitle = string.Format(@"{1} {0} {2} On {3} - {4}", Constants.PageWidgetsOn, page.Name,
                        mainLayoutName, element.Key, currectCulture.NativeName);
                    var widgets = _cacheManager.GetOrSet(cacheKey, cacheTitle, "Main Widgets",
                        () =>
                            _widgetService.GetMainLayoutWidgetsForRender(page.MainLayoutId, element.Value.Id,
                                currentCultureId, page.Id));
                    element.Value.Widgets = widgets;
                }
                //Fire page render events
                var events = Oxide.LoadedEvents.ToList();
                events.ForEach(s => s.OnPageRender(renderPage));
                return renderPage;
            }
            return null;
        }
    }
}