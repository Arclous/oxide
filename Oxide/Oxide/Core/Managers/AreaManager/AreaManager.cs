﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System.Collections.Generic;
using System.Web;
using Oxide.Core.Syntax;

namespace Oxide.Core.Managers.AreaManager
{
    public static class AreaManager
    {
        private static IDictionary<string, object> ActiveArea
            => HttpContext.Current.Request.GetOwinContext().Environment;

        public static void SetActiveArea(RequestType requestType)
        {
            switch (requestType)
            {
                case RequestType.Front:
                    if (!ActiveArea.ContainsKey(Constants.AdminArea))
                        ActiveArea.Add(Constants.AdminArea, false);
                    else
                        ActiveArea[Constants.AdminArea] = false;
                    break;
                case RequestType.Admin:
                    if (!ActiveArea.ContainsKey(Constants.AdminArea))
                        ActiveArea.Add(Constants.AdminArea, true);
                    else
                        ActiveArea[Constants.AdminArea] = true;
                    break;
                case RequestType.Login:
                    if (!ActiveArea.ContainsKey(Constants.AdminArea))
                        ActiveArea.Add(Constants.AdminArea, false);
                    else
                        ActiveArea[Constants.AdminArea] = false;
                    break;
            }
        }
        public static RequestType GetActiveArea()
        {
            if (!ActiveArea.ContainsKey(Constants.AdminArea))
                return RequestType.Front;
            var value = ActiveArea[Constants.AdminArea].ToString();
            return bool.Parse(value) ? RequestType.Admin : RequestType.Front;
        }
        public static string GetAdminUserLanguageCode()
        {
            var languageCookie = HttpContext.Current.Request.Cookies[Constants.AdminLanguageCookieName];
            return languageCookie?.Value.ToUpper() ?? Constants.AdminDefaultLanguageCode;
        }
        public static string GetAdminUserLanguageCode2()
        {
            var languageCookie = HttpContext.Current.Request.Cookies[Constants.AdminLanguageCookieName2];
            return languageCookie?.Value.ToUpper() ?? Constants.AdminDefaultLanguageCode;
        }
    }
}