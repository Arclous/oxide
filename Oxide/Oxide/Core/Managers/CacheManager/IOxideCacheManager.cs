﻿using System;
using Oxide.Core.Base.OxideDependency;

namespace Oxide.Core.Managers.CacheManager
{
    public interface IOxideCacheManager : IOxideDependency
    {
        T GetOrSet<T>(string cacheKey, string title, string group, Func<T> getItemCallback, int defaultMinutes = 60,
            bool register = true) where T : class;
        void ClearCache(string cacheKey);
        void ClearAllCache();
    }
}