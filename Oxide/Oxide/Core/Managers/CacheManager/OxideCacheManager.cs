﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Linq;
using System.Runtime.Caching;
using Oxide.Core.Services.CacheItemService;
using CacheItem = Oxide.Core.Models.Cache.CacheItem;

namespace Oxide.Core.Managers.CacheManager
{
    public class OxideCacheManager : IOxideCacheManager
    {
        private readonly ICacheItemService _cacheItemService;
        public OxideCacheManager(ICacheItemService cacheItemService)
        {
            _cacheItemService = cacheItemService;
        }
        public T GetOrSet<T>(string cacheKey, string title, string group, Func<T> getItemCallback,
            int defaultMinutes = 60, bool register = true) where T : class
        {
            var item = MemoryCache.Default.Get(cacheKey) as T;
            if (item != null) return item;
            var cacheLength = defaultMinutes;
            if (register)
                cacheLength = GetCacheItemLength(cacheKey, title, @group, defaultMinutes);
            item = getItemCallback();
            if (MemoryCache.Default.Contains(cacheKey)) return item;
            if (item != null)
                MemoryCache.Default.Add(cacheKey, item, DateTime.Now.AddMinutes(cacheLength));
            return item;
        }
        public void ClearCache(string cacheKey)
        {
            MemoryCache.Default.Remove(cacheKey);
        }
        public void ClearAllCache()
        {
            for (var dIndex = 0; dIndex <= Oxide.LoadedCacheItems.Count - 1; dIndex++)
                MemoryCache.Default.Remove(Oxide.LoadedCacheItems[dIndex].Name);
        }
        private int GetCacheItemLength(string name, string title, string group, int defaultMinutes = 60)
        {
            var cacheItem =
                Oxide.LoadedCacheItems.FirstOrDefault(
                    x => x != null && x.Name == name && x.Instance == SettingsManager.SettingsManager.InstanceName);
            if (cacheItem != null)
            {
                _cacheItemService.UpdateCacheDateTime(name);
                UpdateCachePool(cacheItem, cacheItem.Name);
                return cacheItem.Value;
            }
            cacheItem = new CacheItem
            {
                Name = name,
                Title = title,
                Group = group,
                Value = defaultMinutes,
                LastUpdate = DateTime.Now,
                Instance = SettingsManager.SettingsManager.InstanceName
            };
            _cacheItemService.CreateCacheItem(cacheItem);
            UpdateCachePool(cacheItem, cacheItem.Name);
            return cacheItem.Value;
        }
        private static void UpdateCachePool(CacheItem cacheItem, string name)
        {
            lock (Oxide.LoadedCacheItems)
            {
                var cach = Oxide.LoadedCacheItems.FirstOrDefault(x => x != null && x.Name.ToLower() == name.ToLower());
                Oxide.LoadedCacheItems.Remove(cach);
                Oxide.LoadedCacheItems.Add(cacheItem);
            }
        }
    }
}