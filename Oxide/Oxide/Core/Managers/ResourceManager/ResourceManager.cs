﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using Oxide.Core.Managers.CultureManager;
using Oxide.Core.Services.ResourceService;
using Oxide.Core.Services.SiteSettings;
using Oxide.Core.Syntax;

namespace Oxide.Core.Managers.ResourceManager
{
    public static class ResourceManager
    {
        public static readonly Dictionary<string, Dictionary<string, Dictionary<string, string>>> Resources =
            new Dictionary<string, Dictionary<string, Dictionary<string, string>>>();

        private static IResourceService _resources;
        private static ICultureManager _cultureManager;
        private static ISiteSettingsService _siteSettingsService;

        private static readonly string[] IllegalCharactersStart =
        {
            "\"", "'", ",", "-", "/", @"\", "#", "$", "%", "^",
            "&", "(", ")", "."
        };

        private static readonly string[] IllegalCharactersFinish = {"\"", "'", ",", "-", "/", @"\", "#", "$", "^", "&"};
        static ResourceManager()
        {
        }
        public static void LoadTranslations()
        {
            var path = string.Format(AppDomain.CurrentDomain.BaseDirectory + "\\{0}", Constants.AreasName);
            var folders = Directory.GetDirectories(path);
            var languageFiles =
                folders.Select(
                    v =>
                        @"~/" + Constants.AreasName + "/" + GetFolderName(v) + "/" + Constants.LanguageFolder + "/" +
                        Constants.LanguageFile).ToList();
            foreach (var file in
                languageFiles.Select(languageFile => HttpContext.Current.Server.MapPath(languageFile))
                    .Where(File.Exists))
                OpenAndStoreResource(file);
        }
        private static string GetFolderName(string path)
        {
            return
                (path.Substring(path.LastIndexOf("\\", StringComparison.Ordinal),
                    path.Length - path.LastIndexOf("\\", StringComparison.Ordinal))).Replace("\\", "/");
        }
        private static bool IsValueLegal(string value)
        {
            return !IllegalCharactersStart.Any(value.StartsWith) && !IllegalCharactersFinish.Any(value.EndsWith);
        }
        private static void OpenAndStoreResource(string fileName)
        {
            try
            {
                var doc = XDocument.Load(fileName);
                {
                    var languagesResource = new Dictionary<string, string>();
                    var translationResource = new Dictionary<string, Dictionary<string, string>>();
                    var resources = doc.Descendants(Constants.Resource).ToList();
                    foreach (var module in resources)
                    {
                        var translations = module.Descendants(Constants.Translation).ToList();
                        translationResource = new Dictionary<string, Dictionary<string, string>>();
                        foreach (var translation in translations)
                        {
                            var languages = translation.Descendants(Constants.ResourceLanguage).ToList();
                            languagesResource = new Dictionary<string, string>();
                            foreach (var language in languages)
                            {
                                var attribute = language.Attribute(Constants.ResourceKey);
                                var value = language.Value;
                                if (!IsValueLegal(value))
                                    throw new Exception("Illegal value detected, Key Name: " + attribute.Value +
                                                        ", Value: " + value);
                                if (attribute != null)
                                {
                                    if (!languagesResource.ContainsKey(attribute.Value))
                                        languagesResource.Add(attribute.Value, value);
                                    else
                                        throw new Exception("Key already added, Key Name: " + attribute.Value);
                                }
                            }
                            var translateionAttiribute = translation.Attribute(Constants.ResourceKey);
                            if (translateionAttiribute != null)
                            {
                                if (!translationResource.ContainsKey(translateionAttiribute.Value))
                                    translationResource.Add(translateionAttiribute.Value, languagesResource);
                                else
                                    throw new Exception("Key already added, Key Name: " + translateionAttiribute.Value);
                            }
                        }
                        var moduleAttiribute = module.Attribute(Constants.ResourceName);
                        if (moduleAttiribute != null)
                        {
                            if (!Resources.ContainsKey(moduleAttiribute.Value))
                                Resources.Add(moduleAttiribute.Value, translationResource);
                            else
                                throw new Exception("Key already added, Key Name: " + moduleAttiribute.Value);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
                // ignored
            }
        }
        public static void RegisterProvider(IResourceService provider, ICultureManager cultureManager,
            ISiteSettingsService siteSettingsService)
        {
            _resources = provider;
            _cultureManager = cultureManager;
            _siteSettingsService = siteSettingsService;
        }
        private static Dictionary<string, string> GetLanguage(IEnumerable<XElement> elements)
        {
            var langList = new Dictionary<string, string>();
            elements.ToList().ForEach(o => langList.Add(o.Attribute(Constants.ResourceKey).Value, o.Value));
            return langList;
        }
        public static string T(this HtmlHelper helper, string resourceName, string resourceKey)
        {
            return _resources.GetText(resourceName, resourceKey);
        }
        public static string T(this HtmlHelper helper, string resourceKey)
        {
            var area = Constants.Empty;
            if (helper.ViewContext.RouteData.DataTokens[Constants.ResourceAreaKey] != null)
                area = helper.ViewContext.RouteData.DataTokens[Constants.ResourceAreaKey].ToString();
            if (area == Constants.Empty)
                area =
                    HttpContext.Current.Request.RequestContext.RouteData.DataTokens[Constants.ResourceAreaKey].ToString();
            return area != Constants.Empty ? _resources.GetText(resourceKey, area) : resourceKey;
        }
        public static MvcHtmlString GetJsonResources(this HtmlHelper helper, string[] resourcesName,
            string objectName = null)
        {
            var activeArea = AreaManager.AreaManager.GetActiveArea();
            var lang = _cultureManager.GetFrontCurrentCulture().ThreeLetterISOLanguageName.ToUpper();
            if (activeArea == RequestType.Admin)
                lang = AreaManager.AreaManager.GetAdminUserLanguageCode();
            var builder = new TagBuilder("script");
            builder.MergeAttribute("type", "text/javascript");
            var strBuilder = new StringBuilder();
            strBuilder.AppendLine();
            if (objectName == null)
            {
                strBuilder.AppendLine("var Translations = Translations || {};");
                strBuilder.AppendLine("Translations =");
            }
            else
            {
                strBuilder.AppendLine("var " + objectName + "=" + objectName + " || {};");
                strBuilder.AppendLine(objectName + " =");
            }
            strBuilder.AppendLine("{");
            resourcesName.ToList().ForEach(resourceName =>
            {
                var ressourceCollection = _resources.GetRessourcesByName(resourceName);
                if (null != ressourceCollection && ressourceCollection.Count > 0)
                {
                    var nbElements = ressourceCollection.Count;
                    var i = 1;
                    foreach (var item in ressourceCollection)
                    {
                        var value = string.Empty;
                        try
                        {
                            value = item.Value[lang];
                        }
                        catch
                        {
                            try
                            {
                                value = item.Value[_siteSettingsService.GetSettings().DefaultSiteCulture.Code3];
                            }
                            catch
                            {
                                // ignored
                            }
                        }
                        strBuilder.AppendFormat(@"""{0}"" : ""{1}""",
                            item.Key.Replace(' ', '_')
                                .Replace('.', '_')
                                .Replace('?', '_')
                                .Replace('!', '_')
                                .Replace(',', '_')
                                .Replace("'", "_"), value);
                        strBuilder.Append(",");
                        strBuilder.AppendLine();
                        i++;
                    }
                }
            });
            strBuilder.Remove(strBuilder.Length - 3, 1);
            strBuilder.AppendLine("}");
            builder.InnerHtml = strBuilder.ToString();
            return new MvcHtmlString(builder.ToString());
        }
    }
}