﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Extesions;
using Oxide.Core.Syntax;
using Oxide.Core.ViewModels.ViewEngineModels;

namespace Oxide.Core.Managers.ViewEngineManager
{
    public static class ViewEngineManager
    {
        /// <summary>
        ///     Gets master layout view according to request
        /// </summary>
        public static ViewEngineResultData GetViewData(ControllerContext controllerContext, string viewPath,
            string masterPath)
        {
            var viewEngineResult = new ViewEngineResultData();
            var requestType = controllerContext.GetTypeOfRequest();
            //Set where active area of the request
            // AreaManager.AreaManager.SetActiveArea(requestType);
            var nameSpace = controllerContext.Controller.GetType().Namespace;
            if ((nameSpace != Constants.OxideController && nameSpace != null))
            {
                if ((requestType == RequestType.Admin))
                    viewEngineResult.MasterPath = Constants.AdminLayout;
                if ((requestType == RequestType.Front))
                {
                    //viewEngineResult.MasterPath = Constants.FrontLayout;
                    viewEngineResult.ViewPath = viewPath.Replace("%1", nameSpace);
                    viewEngineResult.MasterPath = GetFrontViewDocument();
                }
                if ((requestType == RequestType.BlankFrame))
                    viewEngineResult.MasterPath = Constants.BlankLayout;
                if (!viewPath.Contains(Constants.AreasKeyword))
                {
                    viewEngineResult.ViewPath = viewPath.Replace("~/",
                        Constants.AreasKeyword + "/" + nameSpace.Replace(Constants.Controllers, "") + "/");
                }
                else
                {
                    viewEngineResult.ViewPath = viewPath.Replace("%1", nameSpace);
                    return viewEngineResult;
                }
                viewEngineResult.ViewPath = viewPath.Replace("%1", nameSpace);
                viewEngineResult.MasterPath = "";
                return viewEngineResult;
            }
            if (nameSpace == Constants.OxideController)
            {
                switch (requestType)
                {
                    case RequestType.Front:
                        viewEngineResult.ViewPath = viewPath.Replace("%1", nameSpace);
                        viewEngineResult.MasterPath = GetFrontViewDocument();
                        break;
                    case RequestType.Admin:
                        viewEngineResult.ViewPath = viewPath.Replace("%1", nameSpace);
                        viewEngineResult.MasterPath = Constants.AdminLayout;
                        break;
                    case RequestType.Login:
                        viewEngineResult.ViewPath = viewPath.Replace("%1", nameSpace);
                        viewEngineResult.MasterPath = Constants.LoginLayout;
                        break;
                    case RequestType.BlankFrame:
                        viewEngineResult.ViewPath = viewPath.Replace("%1", nameSpace);
                        viewEngineResult.MasterPath = Constants.BlankLayout;
                        break;
                }
                return viewEngineResult;
            }
            if (requestType == RequestType.Front)
            {
                viewEngineResult.ViewPath = viewPath.Replace("%1", nameSpace);
                viewEngineResult.MasterPath = GetFrontViewDocument();
                return viewEngineResult;
            }
            viewEngineResult.ViewPath = viewPath.Replace("%1", nameSpace);
            viewEngineResult.MasterPath = masterPath.Replace("%1", nameSpace);
            return viewEngineResult;
        }
        /// <summary>
        ///     Gets type of request
        /// </summary>
        public static RequestType GetTypeOfRequest(ControllerContext controllerContext)
        {
            var nameSpace = controllerContext.Controller.GetType().Namespace;
            var controllerType = controllerContext.Controller.GetType();
            var actionDescriptor = new ReflectedControllerDescriptor(controllerType).FindAction(controllerContext,
                controllerContext.RouteData.GetRequiredString(Constants.AttributeKey));
            var attributesAdmin = actionDescriptor.GetCustomAttributes(typeof (OxideAdmin), false);
            var attributesFront = actionDescriptor.GetCustomAttributes(typeof (Front), false);
            var attributesLogin = actionDescriptor.GetCustomAttributes(typeof (AdminLogin), false);
            var attributesBlankFrame = actionDescriptor.GetCustomAttributes(typeof (BlankFrame), false);
            if (attributesAdmin.Length > 0)
                return RequestType.Admin;
            if (attributesFront.Length > 0)
                return RequestType.Front;
            if (attributesLogin.Length > 0)
                return RequestType.Login;
            if (attributesBlankFrame.Length > 0)
                return RequestType.BlankFrame;
            return nameSpace == Constants.OxideController ? RequestType.Admin : RequestType.Front;
        }
        /// <summary>
        ///     Gets view locations
        /// </summary>
        public static string[] GetViewLocations()
        {
            var path = string.Format(AppDomain.CurrentDomain.BaseDirectory + "\\{0}", Constants.AreasName);
            var viewFolders = Directory.GetDirectories(path);
            var viewLocations = new List<string>();
            viewLocations.AddRange(GetBasicViewLocations());
            foreach (var v in viewFolders)
            {
                viewLocations.Add(@"~/" + Constants.AreasName + "/" + GetFolderName(v) + "/Views/Widgets/Editor" +
                                  "/{0}.cshtml");
                viewLocations.Add(@"~/" + Constants.AreasName + "/" + GetFolderName(v) + "/Views/Widgets/Front" +
                                  "/{0}.cshtml");
                viewLocations.Add(@"~/" + Constants.AreasName + "/" + GetFolderName(v) +
                                  "/Views/Widgets/Editor/Dashboard" + "/{0}.cshtml");
                viewLocations.Add(@"~/" + Constants.AreasName + "/" + GetFolderName(v) +
                                  "/Views/Widgets/Front/Dashboard" + "/{0}.cshtml");
                viewLocations.Add(@"~/" + Constants.AreasName + "/" + GetFolderName(v) + "/Views" + "/{1}/{0}.cshtml");
                viewLocations.Add(@"~/" + Constants.AreasName + "/" + GetFolderName(v) + "/Views/Shared" + "/{0}.cshtml");
            }
            return viewLocations.ToArray();
        }
        /// <summary>
        ///     Gets view partival view locations
        /// </summary>
        public static string[] GetPartialViewLocations()
        {
            var path = string.Format(AppDomain.CurrentDomain.BaseDirectory + "\\{0}", Constants.AreasName);
            var viewFolders = Directory.GetDirectories(path);
            var viewLocations = new List<string>();
            viewLocations.AddRange(GetBasicViewLocations());
            foreach (var v in viewFolders)
            {
                viewLocations.Add(@"~/" + Constants.AreasName + "/" + GetFolderName(v) + "/Views/Widgets/Editor" +
                                  "/{0}.cshtml");
                viewLocations.Add(@"~/" + Constants.AreasName + "/" + GetFolderName(v) + "/Views/Widgets/Front" +
                                  "/{0}.cshtml");
                viewLocations.Add(@"~/" + Constants.AreasName + "/" + GetFolderName(v) +
                                  "/Views/Widgets/Editor/Dashboard" + "/{0}.cshtml");
                viewLocations.Add(@"~/" + Constants.AreasName + "/" + GetFolderName(v) +
                                  "/Views/Widgets/Front/Dashboard" + "/{0}.cshtml");
                viewLocations.Add(@"~/" + Constants.AreasName + "/" + GetFolderName(v) + "/Views" + "/{1}/{0}.cshtml");
                viewLocations.Add(@"~/" + Constants.AreasName + "/" + GetFolderName(v) + "/Views/Shared" + "/{0}.cshtml");
            }
            viewLocations.AddRange(GetMasterLocations());
            return viewLocations.ToArray();
        }
        /// <summary>
        ///     Gets master view locations
        /// </summary>
        public static string[] GetMasterLocations()
        {
            var viewLocations = new List<string>();
            viewLocations.AddRange(GetBasicViewLocations());
            var path = string.Format(AppDomain.CurrentDomain.BaseDirectory + "{0}\\{1}", Constants.AreasName,
                Constants.ThemeArea);
            foreach (var themeName in Oxide.LoadedThemes)
            {
                if (Directory.Exists(path + "\\" + themeName.Name + "\\" + "Views\\Inner"))
                    viewLocations.Add(@"~/" + Constants.AreasName + "/" + Constants.ThemeArea + "/" + themeName.Name +
                                      "/Views/Inner" + "/{0}.cshtml");
                if (Directory.Exists(path + "\\" + themeName.Name + "\\" + "Views\\Main"))
                    viewLocations.Add(@"~/" + Constants.AreasName + "/" + Constants.ThemeArea + "/" + themeName.Name +
                                      "/Views/Main" + "/{0}.cshtml");
                if (Directory.Exists(path + "\\" + themeName.Name + "\\" + "Views\\Errors"))
                    viewLocations.Add(@"~/" + Constants.AreasName + "/" + Constants.ThemeArea + "/" + themeName.Name +
                                      "/Views/Errors" + "/{0}.cshtml");
            }
            return viewLocations.ToArray();
        }
        /// <summary>
        ///     Gets area master view locations
        /// </summary>
        public static string[] GetAreaMasterLocations()
        {
            var viewLocations = new List<string>();
            viewLocations.AddRange(GetBasicViewLocations());
            var path = string.Format(AppDomain.CurrentDomain.BaseDirectory + "{0}\\{1}", Constants.AreasName,
                Constants.ThemeArea);
            foreach (var themeName in Oxide.LoadedThemes)
            {
                if (Directory.Exists(path + "\\" + themeName.Name + "\\" + "Views\\Inner"))
                    viewLocations.Add(@"~/" + Constants.AreasName + "/" + Constants.ThemeArea + "/" + themeName.Name +
                                      "/Views/Inner" + "/{0}.cshtml");
                if (Directory.Exists(path + "\\" + themeName.Name + "\\" + "Views\\Main"))
                    viewLocations.Add(@"~/" + Constants.AreasName + "/" + Constants.ThemeArea + "/" + themeName.Name +
                                      "/Views/Main" + "/{0}.cshtml");
                if (Directory.Exists(path + "\\" + themeName.Name + "\\" + "Views\\Errors"))
                    viewLocations.Add(@"~/" + Constants.AreasName + "/" + Constants.ThemeArea + "/" + themeName.Name +
                                      "/Views/Errors" + "/{0}.cshtml");
            }
            return viewLocations.ToArray();
        }
        /// <summary>
        ///     Gets basic view locations
        /// </summary>
        public static string[] GetBasicViewLocations()
        {
            var basicLocation = new List<string>
            {
                "~/Areas/Admin/Views/{1}/{0}.cshtml",
                "~/Areas/Admin/Shared/{0}.cshtml",
                "~/Areas/Admin/Views/Templates/{0}.cshtml",
                "~/Views/{0}.cshtml",
                "~/Shared/{0}.cshtml"
            };
            return basicLocation.ToArray();
        }
        /// <summary>
        ///     Gets folder name
        /// </summary>
        private static string GetFolderName(string path)
        {
            return
                (path.Substring(path.LastIndexOf("\\", StringComparison.Ordinal),
                    path.Length - path.LastIndexOf("\\", StringComparison.Ordinal))).Replace("\\", "/");
        }
        /// <summary>
        ///     Gets front view document
        /// </summary>
        private static string GetFrontViewDocument()
        {
            var firstOrDefault = Oxide.LoadedThemes.FirstOrDefault(x => x.Active);
            return firstOrDefault != null ? firstOrDefault.MasterDocumennt : Constants.FrontLayout;
        }
        /// <summary>
        ///     Gets alternate view and context if exist
        /// </summary>
        public static string GetAlternateView(string viewPath, ControllerContext context)
        {
            var firstOrDefault = Oxide.ViewAlternates.FirstOrDefault(x => x.Key == viewPath);
            return firstOrDefault.Value ?? viewPath;
        }
    }
}