﻿using Oxide.Core.ViewModels.PageViewModels;

namespace Oxide.Core.Managers.EventsManager
{
    public interface IOxideEventManager
    {
        /// <summary>
        ///     Initalizer for events
        /// </summary>
        void Initalize();
        /// <summary>
        ///     Fires when model or models created
        /// </summary>
        void OnModelCreated(object[] entities);
        /// <summary>
        ///     Fires when model or models updated
        /// </summary>
        void OnModelUpdated(object[] entities);
        /// <summary>
        ///     Fires when model or models deleted
        /// </summary>
        void OnModelDeleted(object[] entities);
        /// <summary>
        ///     Fires when pages renders
        /// </summary>
        void OnPageRender(RenderPage page);
    }
}