﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Web;

namespace Oxide.Core.Managers.CookieManager
{
    public class CookieManager : ICookieManager
    {
        public void SetCookie(string cookieName, string key, string value, DateTime expires)
        {
            var myCookie = new HttpCookie(cookieName) {[key] = value, Expires = expires};
            HttpContext.Current.Response.Cookies.Add(myCookie);
        }
        public string ReadCookie(string cookieName, string key)
        {
            var myCookie = HttpContext.Current.Request.Cookies[cookieName];
            return myCookie?.Values[key];
        }
        public void ClearAllCookies()
        {
            HttpContext.Current.Request.Cookies.Clear();
        }
    }
}