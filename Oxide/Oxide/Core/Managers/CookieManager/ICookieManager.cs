﻿using System;
using Oxide.Core.Base.OxideDependency;

namespace Oxide.Core.Managers.CookieManager
{
    /// <summary>
    ///     Manager for cookies
    /// </summary>
    public interface ICookieManager : IOxideDependency
    {
        /// <summary>
        ///     Sets cookies
        /// </summary>
        /// <param name="cookieName">Name of the cookie</param>
        /// <param name="key">Key of cookie</param>
        /// <param name="value">Value of cookie</param>
        /// <param name="expires">Expires value for cookie</param>
        void SetCookie(string cookieName, string key, string value, DateTime expires);
        /// <summary>
        ///     Read cookies
        /// </summary>
        /// <param name="cookieName">Name of the cookie</param>
        /// <param name="key">Key of cookie</param>
        string ReadCookie(string cookieName, string key);
        /// <summary>
        ///     Clears all cookies
        /// </summary>
        void ClearAllCookies();
    }
}