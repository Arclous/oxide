﻿using Oxide.Core.Base.OxideDependency;

namespace Oxide.Core.Managers.MediaManager
{
    public interface IMediaManager : IOxideDependency
    {
        string GetModuleImage(string moduleName);
        string GetWidgetImage(string widgetName);
    }
}