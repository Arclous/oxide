﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Web;
using System.Web.Hosting;

namespace Oxide.Core.Managers.MediaManager
{
    public class MediaManager : IMediaManager
    {
        public string GetModuleImage(string moduleName)
        {
            var rootPath = GetRootPath();
            var filePath = string.Format(@"{0}\{1}", rootPath, moduleName.Substring(0, 1) + ".png");
            if (IsFileExist(filePath))
                return "~/" +
                       filePath.Replace(HostingEnvironment.ApplicationPhysicalPath, string.Empty).Replace(@"\", "/");
            using (var rectangleFont = new Font("Arial", 14, FontStyle.Bold))
                using (var bitmap = new Bitmap(320, 110, PixelFormat.Format24bppRgb))
                    using (var g = Graphics.FromImage(bitmap))
                    {
                        g.SmoothingMode = SmoothingMode.AntiAlias;
                        var backgroundColor = Color.Black;
                        g.Clear(backgroundColor);
                        g.DrawString(moduleName.Substring(0, 1), rectangleFont, SystemBrushes.HighlightText,
                            new PointF(10, 40));
                        bitmap.Save(filePath, ImageFormat.Png);
                    }
            return "~/" + filePath.Replace(HostingEnvironment.ApplicationPhysicalPath, string.Empty).Replace(@"\", "/");
        }
        public string GetWidgetImage(string widgetName)
        {
            return widgetName;
        }
        private static bool IsFileExist(string filePath)
        {
            return File.Exists(filePath);
        }
        private static string GetRootPath()
        {
            var rootPath = HttpContext.Current.Server.MapPath("~/SysImages");
            if (!Directory.Exists(rootPath))
                Directory.CreateDirectory(rootPath);
            return rootPath;
        }
    }
}