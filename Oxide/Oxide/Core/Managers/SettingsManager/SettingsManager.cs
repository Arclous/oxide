﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System.Configuration;
using System.Linq;
using Oxide.Core.Helpers;
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.Services.SiteSettings;
using Oxide.Core.Syntax;

namespace Oxide.Core.Managers.SettingsManager
{
    public static class SettingsManager
    {
        public static string[] DomainRoles = new string[0];
        public static string InstanceName;
        public static void LoadDefaultSettings()
        {
            IProviderManager providerManager = new ProviderManager.ProviderManager();
            var siteSettingsService = providerManager.Provide<ISiteSettingsService>();

            //Load site name
            var currentSettings = siteSettingsService.GetSettings();
            OxideUtils.AddProperty(Constants.SiteName,
                currentSettings != null ? currentSettings.Name : Constants.DefaultSiteName);
        }
        public static void LoadApplicationSettings()
        {
            var role = ConfigurationManager.AppSettings["Domain.Role"];
            DomainRoles = role != "" ? role.ToLower().Split(Constants.Splitter) : new[] {"cms"};
            InstanceName = ConfigurationManager.AppSettings["InstanceName"];
        }
        public static bool IsAdmin()
        {
            return DomainRoles.Contains("cms") || DomainRoles.Contains("both");
        }
        public static bool IsFront()
        {
            return DomainRoles.Contains("front") || DomainRoles.Contains("both");
        }
        public static bool IsDataLossEnabled()
        {
            var value = ConfigurationManager.AppSettings["DataLossAllowed"];
            return value != null && bool.Parse(ConfigurationManager.AppSettings["DataLossAllowed"]);
        }
        public static bool IsAutoMigrationEnabled()
        {
            var value = ConfigurationManager.AppSettings["AutoMigrationEnabled"];
            return value == null || bool.Parse(ConfigurationManager.AppSettings["AutoMigrationEnabled"]);
        }

        public static bool IsOutputMinifyEnable()
        {
            var value = ConfigurationManager.AppSettings["OutputMinifyEnable"];
            return value == null || bool.Parse(ConfigurationManager.AppSettings["OutputMinifyEnable"]);
        }

        public static string Domain()
        {
            var value = ConfigurationManager.AppSettings["Domain"];
            return value ?? string.Empty;
        }
    }
}