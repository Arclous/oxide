﻿namespace Oxide.Core.Managers.OxideMigrationManager
{
    public interface IOxideMigrationManager
    {
        void UpdateDatabase();
    }
}