﻿using System.Dynamic;
using System.Web;
using System.Web.Mvc;
using Oxide.Core.Base.OxideDependency;
using Oxide.Core.Models.Layouts;
using Oxide.Core.Models.Pages;
using Oxide.Core.ResultModels;

namespace Oxide.Core.Managers.WidgetManager
{
    public interface IWidgetManager : IOxideDependency
    {
        void LoadWidgets();
        void LoadWidgets(string moduleName);
        void LoadAdminWidgets();
        void LoadAdminWidgets(string moduleName);
        void FireOnWidgetDeleted(PageWidget pageWidget);
        void FireOnWidgetDeleted(MainLayoutWidget mainLayoutWidget);
        WidgetReturnResult RenderAdmin(string widgetKey, int widgetId, int layoutId, int layoutKeyId,
            ControllerContext context, string dynamicTitle, int containerPageId = -1, int widgetCulture = -1);
        WidgetUpdatePackage UpdateAdmin(string widgetKey, int widgetId, int layoutId, int layoutKeyId,
            ControllerContext context, string dynamicTitle, ExpandoObject model, int containerPageId = -1,
            int widgetCulture = -1);
        IHtmlString RenderFront(PageWidget pageWidget);
        IHtmlString RenderFront(MainLayoutWidget mainLayoutWidget);
        WidgetReturnResult RenderDashboardWidgetManagement(string widgetKey, int widgetId, int userId,
            ControllerContext context);
        IHtmlString RenderDashboardWidget(string widgetKey, int widgetId, int userId);
        ExpandoObject GenerateExpandoModel(FormCollection form);
        WidgetData GetWidgetData(FormCollection form);
    }
}