﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Oxide.Areas.Admin.Controllers;
using Oxide.Core.Elements;
using Oxide.Core.Extesions;
using Oxide.Core.Helpers;
using Oxide.Core.Managers.CultureManager;
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.Models.Base;
using Oxide.Core.Models.Layouts;
using Oxide.Core.Models.Pages;
using Oxide.Core.ResultModels;
using Oxide.Core.Syntax;

namespace Oxide.Core.Managers.WidgetManager
{
    public class WidgetManager : IWidgetManager
    {
        private readonly ICultureManager _cultureManager;
        private readonly IProviderManager _providerManager;
        public WidgetManager()
        {
        }
        public WidgetManager(IProviderManager providerManager, ICultureManager cultureManager)
        {
            _providerManager = providerManager;
            _cultureManager = cultureManager;
        }
        public void LoadWidgets()
        {
            Oxide.LoadedWidgets = new List<IOxideWidget>();
            foreach (var instance in AssemblyManager.AssemblyManager.GetFor<IOxideWidget>())
            {
                instance.WidgetKey = OxideWidgetHelper.GetWidgetUniqueKey(instance);
                if (!Oxide.LoadedWidgets.Contains(instance))
                    Oxide.LoadedWidgets.Add(instance);
            }
        }
        public void LoadWidgets(string moduleName)
        {
            foreach (var instance in AssemblyManager.AssemblyManager.GetFor<IOxideWidget>(moduleName))
            {
                instance.Active = true;
                instance.WidgetKey = OxideWidgetHelper.GetWidgetUniqueKey(instance);
                if (!Oxide.LoadedWidgets.Contains(instance))
                    Oxide.LoadedWidgets.Add(instance);
            }
        }
        public void LoadAdminWidgets()
        {
            Oxide.LoadedDashboardWidgets = new List<IOxideDashboardWidget>();
            foreach (var instance in AssemblyManager.AssemblyManager.GetFor<IOxideDashboardWidget>())
            {
                instance.WidgetKey = OxideWidgetHelper.GetWidgetUniqueKey(instance);
                if (!Oxide.LoadedDashboardWidgets.Contains(instance))
                    Oxide.LoadedDashboardWidgets.Add(instance);
            }
        }
        public void LoadAdminWidgets(string moduleName)
        {
            foreach (var instance in AssemblyManager.AssemblyManager.GetFor<IOxideDashboardWidget>(moduleName))
            {
                instance.Active = true;
                instance.WidgetKey = OxideWidgetHelper.GetWidgetUniqueKey(instance);
                if (!Oxide.LoadedDashboardWidgets.Contains(instance))
                    Oxide.LoadedDashboardWidgets.Add(instance);
            }
        }
        public void FireOnWidgetDeleted(PageWidget pageWidget)
        {
            OxideWidgetHelper.GetWidget(x => x.WidgetKey == pageWidget.WidgetKey);
            var widget = OxideWidgetHelper.GetWidget(x => x.WidgetKey == pageWidget.WidgetKey);
            var widgetInfoPacket = GetWidgetInfoPacket(pageWidget.Id, pageWidget.LayoutId, pageWidget.LayoutKeyId,
                widget, pageWidget.Title, pageWidget.PageId);
            widget.OnDeleted(widgetInfoPacket, _providerManager);
        }
        public void FireOnWidgetDeleted(MainLayoutWidget mainLayoutWidget)
        {
            var widget = OxideWidgetHelper.GetWidget(x => x.WidgetKey == mainLayoutWidget.WidgetKey);
            var widgetInfoPacket = GetWidgetInfoPacket(mainLayoutWidget.Id, mainLayoutWidget.LayoutId,
                mainLayoutWidget.LayoutKeyId, widget, mainLayoutWidget.MainPageId, mainLayoutWidget.Title, -1, -1);
            widget.OnDeleted(widgetInfoPacket, _providerManager);
        }
        public WidgetReturnResult RenderAdmin(string widgetKey, int widgetId, int layoutId, int layoutKeyId,
            ControllerContext context, string dynamicTitle, int containerPageId = -1, int widgetCulture = -1)
        {
            //Get requested widget
            var widget = OxideWidgetHelper.GetWidget(x => x.WidgetKey == widgetKey);
            WidgetResult widgetResult = null;
            //Get package for widget info which will be passed to widget render admin
            var widgetInfoPacket = GetWidgetInfoPacket(widgetId, layoutId, layoutKeyId, widget, "", containerPageId,
                widgetCulture);
            widgetInfoPacket.Title = dynamicTitle ?? widget.Title;
            var alternateWidget = Oxide.WidgetAlternates.FirstOrDefault(x => x.Key == widget.WidgetKey).Value;
            widgetResult = alternateWidget == null
                ? widget.RenderAdmin(widgetInfoPacket, _providerManager)
                : AlternateAdmin(alternateWidget, widget, widgetInfoPacket);

            //Add the area info for the module to the route 
            if (alternateWidget == null)
                OxideWidgetHelper.FixRoute(widget.ModuleName, context);
            else if (alternateWidget.ModifyContext)
                OxideWidgetHelper.FixRoute(alternateWidget.Widget.ModuleName, context);
            else
                OxideWidgetHelper.FixRoute(widget.ModuleName, context);

            //Get and return widget result
            return GetWidgetReturnResult(widgetResult, context, widget, widgetInfoPacket);
        }
        public WidgetUpdatePackage UpdateAdmin(string widgetKey, int widgetId, int layoutId, int layoutKeyId,
            ControllerContext context, string dynamicTitle, ExpandoObject model, int containerPageId = -1,
            int widgetCulture = -1)
        {
            //Get requested widget
            var widget = OxideWidgetHelper.GetWidget(x => x.WidgetKey == widgetKey);
            WidgetUpdatePackage widgetResult = null;
            //Get packe for widget info which will be passed to widget render admin
            var widgetInfoPacket = GetWidgetInfoPacket(widgetId, layoutId, layoutKeyId, widget, "", containerPageId,
                widgetCulture);
            if (dynamicTitle != string.Empty)
                widgetInfoPacket.Title = dynamicTitle;
            var alternateWidget = Oxide.WidgetAlternates.FirstOrDefault(x => x.Key == widget.WidgetKey).Value;
            widgetResult = alternateWidget == null
                ? widget.RunUpdate(widgetInfoPacket, _providerManager, model)
                : AlternateAdminForUpdate(alternateWidget, widget, widgetInfoPacket, model);

            //Add the area info for the module to the route 
            if (alternateWidget == null)
                OxideWidgetHelper.FixRoute(widget.ModuleName, context);
            else if (alternateWidget.ModifyContext)
                OxideWidgetHelper.FixRoute(alternateWidget.Widget.ModuleName, context);
            else
                OxideWidgetHelper.FixRoute(widget.ModuleName, context);

            //Get and return widget result
            return widgetResult;
        }
        public IHtmlString RenderFront(PageWidget pageWidget)
        {
            //Get controller context
            var context = GetControllerContext();
            OxideWidgetHelper.GetWidget(x => x.WidgetKey == pageWidget.WidgetKey);
            var widget = OxideWidgetHelper.GetWidget(x => x.WidgetKey == pageWidget.WidgetKey);
            WidgetResult widgetResult = null;
            //Get package for widget info which will be passed to widget render front
            var widgetInfoPacket = GetWidgetInfoPacket(pageWidget.Id, pageWidget.LayoutId, pageWidget.LayoutKeyId,
                widget, pageWidget.Title, pageWidget.PageId);
            var alternateWidget = Oxide.WidgetAlternates.FirstOrDefault(x => x.Key == pageWidget.WidgetKey).Value;
            //widget = (IOxideWidget) _providerManager.Provide(widget.GetType());
            //  widget =(IOxideWidget) Activator.CreateInstance(widget.GetType());
            widgetResult = alternateWidget == null
                ? widget.RenderFront(widgetInfoPacket, _providerManager)
                : AlternateFront(alternateWidget, widget, widgetInfoPacket);

            //Add the area info for the module to the route 
            if (alternateWidget == null)
                OxideWidgetHelper.FixRoute(widget.ModuleName, context);
            else if (alternateWidget.ModifyContext)
                OxideWidgetHelper.FixRoute(alternateWidget.Widget.ModuleName, context);
            else
                OxideWidgetHelper.FixRoute(widget.ModuleName, context);

            //Get and return widget result
            return GetWidgetReturnResult(widgetResult, context);
        }
        public IHtmlString RenderFront(MainLayoutWidget mainLayoutWidget)
        {
            //Get controller context
            var context = GetControllerContext();
            var widget = OxideWidgetHelper.GetWidget(x => x.WidgetKey == mainLayoutWidget.WidgetKey);
            WidgetResult widgetResult = null;

            //Get package for widget info which will be passed to widget render front
            var widgetInfoPacket = GetWidgetInfoPacket(mainLayoutWidget.Id, mainLayoutWidget.LayoutId,
                mainLayoutWidget.LayoutKeyId, widget, mainLayoutWidget.MainPageId, mainLayoutWidget.Title, -1, -1);
            var alternateWidget = Oxide.WidgetAlternates.FirstOrDefault(x => x.Key == mainLayoutWidget.WidgetKey).Value;
            widgetResult = alternateWidget == null
                ? widget.RenderFront(widgetInfoPacket, _providerManager)
                : AlternateFront(alternateWidget, widget, widgetInfoPacket);

            //Add the area info for the module to the route 
            if (alternateWidget == null)
                OxideWidgetHelper.FixRoute(widget.ModuleName, context);
            else if (alternateWidget.ModifyContext)
                OxideWidgetHelper.FixRoute(alternateWidget.Widget.ModuleName, context);
            else
                OxideWidgetHelper.FixRoute(widget.ModuleName, context);

            //Get and return widget result
            return GetWidgetReturnResult(widgetResult, context);
        }
        public WidgetReturnResult RenderDashboardWidgetManagement(string widgetKey, int widgetId, int userId,
            ControllerContext context)
        {
            //Get requested widget
            var widget = Oxide.LoadedDashboardWidgets.FirstOrDefault(x => x.WidgetKey == widgetKey);
            WidgetResult widgetResult = null;
            //Get packe for widget info which will be passed to widget render admin
            var widgetInfoPacket = GetWidgetInfoPacket(widgetId, userId, widget);
            if (widget != null)
            {
                widgetResult = widget.RenderDashboardManagement(widgetInfoPacket, _providerManager);
                OxideWidgetHelper.FixRoute(widget.ModuleName, context);
                //Add the area info for the module to the route 
                OxideWidgetHelper.FixRoute(widget.ModuleName, context);
            }
            //Get and return widget result
            return GetWidgetReturnResult(widgetResult, context, widget);
        }
        public IHtmlString RenderDashboardWidget(string widgetKey, int widgetId, int userId)
        {
            var widget = Oxide.LoadedDashboardWidgets.FirstOrDefault(x => x.WidgetKey == widgetKey);

            //Get controller context
            var context = GetControllerContext();
            WidgetResult widgetResult = null;
            //Get packe for widget info which will be passed to widget render front
            var widgetInfoPacket = GetWidgetInfoPacket(widgetId, userId, widget);
            if (widget != null)
            {
                widgetResult = widget.RenderDashboard(widgetInfoPacket, _providerManager);
                OxideWidgetHelper.FixRoute(widget.ModuleName, context);
                //Add the area info for the module to the route 
                OxideWidgetHelper.FixRoute(widget.ModuleName, context);
            }

            //Get and return widget result
            return GetWidgetReturnResult(widgetResult, context);
        }
        public ExpandoObject GenerateExpandoModel(FormCollection form)
        {
            var expandoModel = new ExpandoObject();
            foreach (var v in from object v in form
                where
                    v.ToString() != "widgetKey" && v.ToString() != "widgetId" && v.ToString() != "layoutId" &&
                    v.ToString() != "layoutKeyId" && v.ToString() != "dynamicTitle" && v.ToString() != "pageId" &&
                    v.ToString() != "regular" && v.ToString() != "currentLanguage"
                select v)
                DynamicHelper.AddProperty(expandoModel, v.ToString().FirstLetterToUpper(), form[v.ToString()]);
            return expandoModel;
        }
        public WidgetData GetWidgetData(FormCollection form)
        {
            return new WidgetData
            {
                WidgetKey = form["widgetKey"],
                WidgetId = Convert.ToInt32(form["widgetId"]),
                LayoutId = Convert.ToInt32(form["layoutId"]),
                LayoutKeyId = Convert.ToInt32(form["layoutKeyId"]),
                DynamicTitle = form["dynamicTitle"],
                ContainerPageId = Convert.ToInt32(form["pageId"]),
                Regular = bool.Parse(form["regular"]),
                WidgetCulture = Convert.ToInt32(form["currentLanguage"])
            };
        }
        private WidgetResult AlternateFront(AlternateWidget alternateWidget, IOxideWidget widget,
            WidgetInfoPackage widgetInfoPackage)
        {
            //Work with alternative widget according to settings
            WidgetResult widgetResult;
            switch (alternateWidget.AlternateTypes)
            {
                case Syntax.Syntax.AlternateTypes.FunctionFront:
                    widgetResult = alternateWidget.Widget.RenderFront(widgetInfoPackage, _providerManager);
                    break;
                case Syntax.Syntax.AlternateTypes.ViewFront:
                    widget.FrontView = alternateWidget.Widget.FrontView;
                    widgetResult = widget.RenderFront(widgetInfoPackage, _providerManager);
                    break;
                case Syntax.Syntax.AlternateTypes.ViewAdminFront:
                    widget.FrontView = alternateWidget.Widget.FrontView;
                    widgetResult = widget.RenderFront(widgetInfoPackage, _providerManager);
                    break;
                case Syntax.Syntax.AlternateTypes.CompletelyFront:
                    widget.FrontView = alternateWidget.Widget.FrontView;
                    widgetResult = alternateWidget.Widget.RenderFront(widgetInfoPackage, _providerManager);
                    break;
                case Syntax.Syntax.AlternateTypes.Completely:
                    widget.FrontView = alternateWidget.Widget.FrontView;
                    widgetResult = alternateWidget.Widget.RenderFront(widgetInfoPackage, _providerManager);
                    break;
                default:
                    widgetResult = widget.RenderFront(widgetInfoPackage, _providerManager);
                    break;
            }
            return widgetResult;
        }
        private WidgetResult AlternateAdmin(AlternateWidget alternateWidget, IOxideWidget widget,
            WidgetInfoPackage widgetInfoPackage)
        {
            //Work with alternative widget according to settings
            WidgetResult widgetResult;
            switch (alternateWidget.AlternateTypes)
            {
                case Syntax.Syntax.AlternateTypes.FunctionAdmin:
                    widgetResult = alternateWidget.Widget.RenderAdmin(widgetInfoPackage, _providerManager);
                    break;
                case Syntax.Syntax.AlternateTypes.ViewAdmin:
                    widget.EditorView = alternateWidget.Widget.EditorView;
                    widgetResult = widget.RenderAdmin(widgetInfoPackage, _providerManager);
                    break;
                case Syntax.Syntax.AlternateTypes.ViewAdminFront:
                    widget.EditorView = alternateWidget.Widget.EditorView;
                    widgetResult = widget.RenderAdmin(widgetInfoPackage, _providerManager);
                    break;
                case Syntax.Syntax.AlternateTypes.CompletelyAdmin:
                    widget.EditorView = alternateWidget.Widget.EditorView;
                    widgetResult = alternateWidget.Widget.RenderAdmin(widgetInfoPackage, _providerManager);
                    break;
                case Syntax.Syntax.AlternateTypes.Completely:
                    widget.EditorView = alternateWidget.Widget.EditorView;
                    widgetResult = alternateWidget.Widget.RenderAdmin(widgetInfoPackage, _providerManager);
                    break;
                default:
                    widgetResult = widget.RenderAdmin(widgetInfoPackage, _providerManager);
                    break;
            }
            return widgetResult;
        }
        private WidgetUpdatePackage AlternateAdminForUpdate(AlternateWidget alternateWidget, IOxideWidget widget,
            WidgetInfoPackage widgetInfoPackage, ExpandoObject model)
        {
            //Work with alternative widget according to settings
            WidgetUpdatePackage widgetResult;
            switch (alternateWidget.AlternateTypes)
            {
                case Syntax.Syntax.AlternateTypes.FunctionAdmin:
                    widgetResult = alternateWidget.Widget.RunUpdate(widgetInfoPackage, _providerManager, model);
                    break;
                case Syntax.Syntax.AlternateTypes.ViewAdmin:
                    widget.EditorView = alternateWidget.Widget.EditorView;
                    widgetResult = widget.RunUpdate(widgetInfoPackage, _providerManager, model);
                    break;
                case Syntax.Syntax.AlternateTypes.ViewAdminFront:
                    widget.EditorView = alternateWidget.Widget.EditorView;
                    widgetResult = widget.RunUpdate(widgetInfoPackage, _providerManager, model);
                    break;
                case Syntax.Syntax.AlternateTypes.CompletelyAdmin:
                    widget.EditorView = alternateWidget.Widget.EditorView;
                    widgetResult = alternateWidget.Widget.RunUpdate(widgetInfoPackage, _providerManager, model);
                    break;
                case Syntax.Syntax.AlternateTypes.Completely:
                    widget.EditorView = alternateWidget.Widget.EditorView;
                    widgetResult = alternateWidget.Widget.RunUpdate(widgetInfoPackage, _providerManager, model);
                    break;
                default:
                    widgetResult = widget.RunUpdate(widgetInfoPackage, _providerManager, model);
                    break;
            }
            return widgetResult;
        }
        private static ControllerContext GetControllerContext()
        {
            var context = new HttpContextWrapper(HttpContext.Current);
            var routeData = new RouteData();
            routeData.Values.Add(Constants.RenderController, Constants.RenderValue);
            return new ControllerContext(new RequestContext(context, routeData), new FrontController());
        }

        #region Helpers
        private WidgetInfoPackage GetWidgetInfoPacket(int widgetId, int layoutId, int layoutKeyId, IOxideWidget widget,
            string widgetTitle = "", int containerPageId = -1, int widgetCulture = -1)
        {
            if (widget == null)
                throw new ArgumentException(
                    "Cannot get the widget info, there can be problem with the name of the module which is defined for the widget,may module names does not match or container module of the widget is not activated");

            //Regular value for shows us if the widget regular or for main layout
            //According to this value, unique hash will generate with culture or without it
            var widgetInfoPacket = new WidgetInfoPackage
            {
                WidgetId = widgetId,
                CurrentLanguage = widgetCulture == -1 ? _cultureManager.GetFrontCurrentCultureId() : widgetCulture,
                PageId = containerPageId,
                WidgetKey = widget.WidgetKey,
                LayoutId = layoutId,
                LayoutKeyId = layoutKeyId,
                Title = widgetTitle,
                Regular = containerPageId != -1
            };
            widgetInfoPacket.UniqeHash = widgetInfoPacket.GetUniqueHash();
            return widgetInfoPacket;
        }
        private WidgetInfoPackage GetWidgetInfoPacket(int widgetId, int layoutId, int layoutKeyId, IOxideWidget widget,
            int pageId, string widgetTitle = "", int containerPageId = -1, int widgetCulture = -1)
        {
            if (widget == null)
                throw new ArgumentException(
                    "Cannot get the widget info, there can be problem with the name of the module which is defined for the widget,may module names does not match or container module of the widget is not activated");

            //Regular value for shows us if the widget regular or for main layout
            //According to this value, unique hash will generate with culture or without it
            var widgetInfoPacket = new WidgetInfoPackage
            {
                WidgetId = widgetId,
                CurrentLanguage = widgetCulture == -1 ? _cultureManager.GetFrontCurrentCultureId() : widgetCulture,
                PageId = containerPageId,
                MainPageId = pageId,
                WidgetKey = widget.WidgetKey,
                LayoutId = layoutId,
                LayoutKeyId = layoutKeyId,
                Title = widgetTitle,
                Regular = containerPageId != -1
            };
            widgetInfoPacket.UniqeHash = widgetInfoPacket.GetUniqueHash();
            return widgetInfoPacket;
        }
        private DashboardWidgetInfoPackage GetWidgetInfoPacket(int widgetId, int userId, IOxideDashboardWidget widget)
        {
            if (widget == null)
                throw new ArgumentException(
                    "Cannot get the widget info, there can be problem with the name of the module which is defined for the widget,may module names does not match or container module of the widget is not activated");
            var widgetInfoPacket = new DashboardWidgetInfoPackage
            {
                UserId = userId,
                WidgetKey = widget.WidgetKey,
                Title = widget.Title,
                WidgetId = widgetId
            };
            widgetInfoPacket.UniqeHash = widgetInfoPacket.GetUniqueHash();
            return widgetInfoPacket;
        }
        private static WidgetReturnResult GetWidgetReturnResult(WidgetResult widgetResult, ControllerContext context,
            IOxideWidget widget, WidgetInfoPackage infoPackage)
        {
            var widgetReturnResult = new WidgetReturnResult
            {
                Header = ((OxideModel) widgetResult.Model).Header,
                Title = widget.Title,
                Data = OxideWidgetHelper.RenderViewToHtml(context, widgetResult.ViewName, widgetResult.Model),
                Parameters = infoPackage
            };
            return widgetReturnResult;
        }
        private static WidgetReturnResult GetWidgetReturnResult(WidgetResult widgetResult, ControllerContext context,
            IOxideDashboardWidget widget)
        {
            var widgetReturnResult = new WidgetReturnResult
            {
                Header = widgetResult.Model != null ? ((OxideModel) widgetResult.Model).Header : widget.Title,
                Title = widget.Title,
                Data = OxideWidgetHelper.RenderViewToHtml(context, widgetResult.ViewName, widgetResult.Model)
            };
            return widgetReturnResult;
        }
        private static IHtmlString GetWidgetReturnResult(WidgetResult widgetResult, ControllerContext context)
        {
            return OxideWidgetHelper.RenderViewToHtml(context, widgetResult.ViewName, widgetResult.Model);
        }
        #endregion
    }
}