﻿namespace Oxide.Core.Managers.ModuleManager
{
    public interface IModuleManager
    {
        void LoadModules();
    }
}