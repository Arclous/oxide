﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System.Linq;
using Oxide.Core.Elements;
using Oxide.Core.Models.Modules;
using Oxide.Core.Services.ModuleService;

namespace Oxide.Core.Managers.ModuleManager
{
    public class ModuleManager : IModuleManager
    {
        private readonly IModuleService _moduleService;
        public ModuleManager(IModuleService moduleService)
        {
            _moduleService = moduleService;
        }
        public void LoadModules()
        {
            Oxide.LoadedModules = AssemblyManager.AssemblyManager.GetModules<IOxideModule>();
            var registeredModules = _moduleService.GetAllModules();
            foreach (var newModule in Oxide.LoadedModules)
            {
                if (registeredModules.All(x => x.Namespace != newModule.GetType().Module.Name.Replace(".dll", "")))
                {
                    _moduleService.CreateModule(new Module
                    {
                        Name = newModule.Name,
                        Active = false,
                        Namespace = newModule.GetType().Module.Name.Replace(".dll", ""),
                        Title = newModule.Title,
                        Author = newModule.Author,
                        Category = newModule.Category,
                        Description = newModule.Description,
                        PreviewImageUrl = newModule.PreviewImageUrl
                    });
                }
            }
        }
    }
}