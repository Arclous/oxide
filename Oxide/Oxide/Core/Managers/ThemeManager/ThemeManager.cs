﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System.Linq;
using Oxide.Core.Base.OxideDependency;
using Oxide.Core.Elements;
using Oxide.Core.Models.Themes;
using Oxide.Core.Services.Interface.Registers.ResourceRegister;
using Oxide.Core.Services.ThemeService;
using Oxide.Core.Syntax;

namespace Oxide.Core.Managers.ThemeManager
{
    public class ThemeManager : IThemeManager, IOxideDependency
    {
        private readonly IThemeService _themeService;
        public ThemeManager(IThemeService themeService)
        {
            _themeService = themeService;
        }
        public void LoadThemes(string moduleName)
        {
            var registeredThemes = _themeService.GetAllThemes();
            foreach (var instance in AssemblyManager.AssemblyManager.GetFor<IOxideTheme>(moduleName))
            {
                var theme = registeredThemes.FirstOrDefault(x => x.Name == instance.Name);
                if (theme != null)
                {
                    instance.Active = theme.Active;
                }
                else
                {
                    var newTheme = new Theme {Name = instance.Name, Active = instance.Active};
                    _themeService.CreateTheme(newTheme);
                }
                Oxide.LoadedThemes.Add(instance);
            }
            foreach (var instance in AssemblyManager.AssemblyManager.GetFor<IResourceRegisterer>(moduleName))
            {
                var themeName = ((dynamic) instance).ThemeName;
                if (themeName == GetActiveThemeName())
                    instance.RegisterResource(Oxide.BoundleCollection);
            }
        }
        public string GetErrorPage(ErrorTypes errorType)
        {
            var activeTheme = Oxide.LoadedThemes.FirstOrDefault(x => x.Active);
            if (activeTheme != null)
            {
                switch (errorType)
                {
                    case ErrorTypes.Error403:
                        return activeTheme.Error403;
                    case ErrorTypes.Error404:
                        return activeTheme.Error404;
                    case ErrorTypes.Error500:
                        return activeTheme.Error500;
                }
            }
            return Constants.DefaultErrorPage;
        }
        public string GetMainLayout(int layoutId)
        {
            var activeTheme = Oxide.LoadedThemes.FirstOrDefault(x => x.Active);
            if (activeTheme != null)
            {
                var layoutMain =
                    Oxide.LoadedLayouts.FirstOrDefault(x => x.ThemeName == activeTheme.Name && x.Id == layoutId);
                return layoutMain != null ? layoutMain.View : Constants.DefaultMasterPage;
            }
            return Constants.DefaultMasterPage;
        }
        private static string GetActiveThemeName()
        {
            var firstOrDefault = Oxide.LoadedThemes.FirstOrDefault(x => x.Active);
            return firstOrDefault != null ? firstOrDefault.Name : Constants.Empty;
        }
    }
}