﻿using Oxide.Core.Syntax;

namespace Oxide.Core.Managers.ThemeManager
{
    public interface IThemeManager
    {
        void LoadThemes(string moduleName);
        string GetErrorPage(ErrorTypes errorType);
        string GetMainLayout(int layoutId);
    }
}