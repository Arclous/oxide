﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Linq;
using Oxide.Core.Models.ScheduledJob;
using Oxide.Core.Services.Implemantation;
using Oxide.Core.Services.Interface.Registers.JobRegisterer;
using Oxide.Core.Services.ScheduledJobService;
using Quartz;
using Quartz.Impl;

namespace Oxide.Core.Managers.ScheduledJobManager
{
    public class ScheduledJobManager : IScheduledJobManager
    {
        private readonly IScheduledJobService _scheduledJobService;
        public ScheduledJobManager()
        {
          
        }
        public ScheduledJobManager(IScheduledJobService scheduledJobService)
        {
            _scheduledJobService = scheduledJobService;
        }
        public void RegisterAllBackgroundJobs()
        {
            var scheduler = StdSchedulerFactory.GetDefaultScheduler();
            var listener = new ScheduledJobListener();
            scheduler.Start();
            foreach (var ins in
                AssemblyManager.AssemblyManager.GetFor<IOxideScheduledBackgroundJob>()
                    .Where(instance => instance != null)
                    .Select(instance => instance as OxideScheduledBackgroundJob)
                    .Where(ins => ins?.Name != null))
            {
                var asm = ins.GetType();
                var registeredJob = _scheduledJobService.IsJobExist(ins.Name);
                if (registeredJob != null)
                {
                    if (!IsSuitable((registeredJob))) continue;
                }
                else
                {
                    if (!IsSuitable((ins))) continue;
                }
                switch (ins.ActionType)
                {
                    case Syntax.Syntax.BackgroundTaskActionType.Daily:
                        if (registeredJob == null)
                        {
                            var scheduledJob = new ScheduledJob
                            {
                                Active = true,
                                Name = ins.Name,
                                Title = ins.Title,
                                LastStart = DateTime.Now,
                                LastFinish = DateTime.Now,
                                NextStart = DateTime.Now,
                                Owner = asm.Namespace,
                                CanManage = ins.CanManage,
                                CanRunOn = ins.CanRunOn
                            };
                            var jobKey = RegisterBackgroundJob(ins, ins.DailyAction, scheduler);
                            scheduledJob.Key = jobKey.Name;
                            scheduledJob.Group = jobKey.Group;
                            _scheduledJobService.CreateJob(scheduledJob);
                        }
                        else
                        {
                            if (registeredJob.Active)
                            {
                                var jobKey = RegisterBackgroundJob(ins,
                                    registeredJob.Interval > 0 ? GetInterval(registeredJob) : ins.DailyAction, scheduler);
                                registeredJob.Key = jobKey.Name;
                                registeredJob.Group = jobKey.Group;
                                _scheduledJobService.UpdateJob(registeredJob);
                            }
                            else
                            {
                                var jobKey = RegisterBackgroundJob(ins,
                                    registeredJob.Interval > 0 ? GetInterval(registeredJob) : ins.DailyAction, scheduler);
                                registeredJob.Key = jobKey.Name;
                                registeredJob.Group = jobKey.Group;
                                scheduler.PauseJob(jobKey);
                                _scheduledJobService.UpdateJob(registeredJob);
                            }
                        }
                        break;
                    case Syntax.Syntax.BackgroundTaskActionType.Calendar:
                        if (registeredJob == null)
                        {
                            var scheduledJob = new ScheduledJob
                            {
                                Active = true,
                                Name = ins.Name,
                                Title = ins.Title,
                                LastStart = DateTime.Now,
                                LastFinish = DateTime.Now,
                                NextStart = DateTime.Now,
                                Owner = asm.Namespace,
                                CanManage = ins.CanManage,
                                CanRunOn = ins.CanRunOn
                            };
                            var jobKey = RegisterBackgroundJob(ins, ins.Action, scheduler);
                            scheduledJob.Key = jobKey.Name;
                            scheduledJob.Group = jobKey.Group;
                            _scheduledJobService.CreateJob(scheduledJob);
                        }
                        else
                        {
                            if (registeredJob.Active)
                            {
                                var jobKey = RegisterBackgroundJob(ins, ins.Action, scheduler);
                                registeredJob.Key = jobKey.Name;
                                registeredJob.Group = jobKey.Group;
                                _scheduledJobService.UpdateJob(registeredJob);
                            }
                            else
                            {
                                var jobKey = RegisterBackgroundJob(ins, ins.Action, scheduler);
                                registeredJob.Key = jobKey.Name;
                                registeredJob.Group = jobKey.Group;
                                scheduler.PauseJob(jobKey);
                                _scheduledJobService.UpdateJob(registeredJob);
                            }
                        }
                        break;
                }
            }
            scheduler.ListenerManager.AddJobListener(listener);
        }
        public void RegisterAllBackgroundJobs(string moduleName)
        {
            var scheduler = StdSchedulerFactory.GetDefaultScheduler();
            var listener = new ScheduledJobListener();
            scheduler.Start();
            foreach (var ins in
                AssemblyManager.AssemblyManager.GetFor<IOxideScheduledBackgroundJob>(moduleName)
                    .Where(instance => instance != null)
                    .Select(instance => instance as OxideScheduledBackgroundJob)
                    .Where(ins => ins?.Name != null))
            {
                var asm = ins.GetType();
                var registeredJob = _scheduledJobService.IsJobExist(ins.Name);
                if (registeredJob != null)
                {
                    if (!IsSuitable((registeredJob))) continue;
                }
                else
                {
                    if (!IsSuitable((ins))) continue;
                }
                switch (ins.ActionType)
                {
                    case Syntax.Syntax.BackgroundTaskActionType.Daily:
                        if (registeredJob == null)
                        {
                            var scheduledJob = new ScheduledJob
                            {
                                Active = true,
                                Name = ins.Name,
                                Title = ins.Title,
                                LastStart = DateTime.Now,
                                LastFinish = DateTime.Now,
                                NextStart = DateTime.Now,
                                Owner = asm.Namespace,
                                CanManage = ins.CanManage,
                                CanRunOn = ins.CanRunOn
                            };
                            var jobKey = RegisterBackgroundJob(ins, ins.DailyAction, scheduler);
                            scheduledJob.Key = jobKey.Name;
                            scheduledJob.Group = jobKey.Group;
                            _scheduledJobService.CreateJob(scheduledJob);
                        }
                        else
                        {
                            if (registeredJob.Active)
                            {
                                var jobKey = RegisterBackgroundJob(ins,
                                    registeredJob.Interval > 0 ? GetInterval(registeredJob) : ins.DailyAction, scheduler);
                                registeredJob.Key = jobKey.Name;
                                registeredJob.Group = jobKey.Group;
                                _scheduledJobService.UpdateJob(registeredJob);
                            }
                            else
                            {
                                var jobKey = RegisterBackgroundJob(ins,
                                    registeredJob.Interval > 0 ? GetInterval(registeredJob) : ins.DailyAction, scheduler);
                                registeredJob.Key = jobKey.Name;
                                registeredJob.Group = jobKey.Group;
                                scheduler.PauseJob(jobKey);
                                _scheduledJobService.UpdateJob(registeredJob);
                            }
                        }
                        break;
                    case Syntax.Syntax.BackgroundTaskActionType.Calendar:
                        if (registeredJob == null)
                        {
                            var scheduledJob = new ScheduledJob
                            {
                                Active = true,
                                Name = ins.Name,
                                Title = ins.Title,
                                LastStart = DateTime.Now,
                                LastFinish = DateTime.Now,
                                NextStart = DateTime.Now,
                                Owner = asm.Namespace,
                                CanManage = ins.CanManage,
                                CanRunOn = ins.CanRunOn
                            };
                            var jobKey = RegisterBackgroundJob(ins, ins.Action, scheduler);
                            scheduledJob.Key = jobKey.Name;
                            scheduledJob.Group = jobKey.Group;
                            _scheduledJobService.CreateJob(scheduledJob);
                        }
                        else
                        {
                            if (registeredJob.Active)
                            {
                                var jobKey = RegisterBackgroundJob(ins, ins.Action, scheduler);
                                registeredJob.Key = jobKey.Name;
                                registeredJob.Group = jobKey.Group;
                                _scheduledJobService.UpdateJob(registeredJob);
                            }
                            else
                            {
                                var jobKey = RegisterBackgroundJob(ins, ins.Action, scheduler);
                                registeredJob.Key = jobKey.Name;
                                registeredJob.Group = jobKey.Group;
                                scheduler.PauseJob(jobKey);
                                _scheduledJobService.UpdateJob(registeredJob);
                            }
                        }
                        break;
                }
            }
            if (scheduler.ListenerManager.GetJobListeners().Count == 0)
                scheduler.ListenerManager.AddJobListener(listener);
        }
        public void UnRegisterScheduledAllJobs()
        {
            var scheduler = StdSchedulerFactory.GetDefaultScheduler();
            var jobs = _scheduledJobService.GetAllJobs();
            foreach (var jobKey in jobs.Select(job => new JobKey(job.Key, job.Group)))
                scheduler.DeleteJob(jobKey);
            scheduler.Shutdown();
            _scheduledJobService.DeleteAllJobs();
        }
        public void UnRegisterScheduledAllJobs(string moduleName)
        {
            var scheduler = StdSchedulerFactory.GetDefaultScheduler();
            foreach (var ins in
                AssemblyManager.AssemblyManager.GetFor<IOxideScheduledBackgroundJob>(moduleName)
                    .Where(instance => instance != null)
                    .Select(instance => instance as OxideScheduledBackgroundJob)
                    .Where(ins => ins?.Name != null))
            {
                var job = _scheduledJobService.IsJobExist(ins.Name);
                var jobKey = new JobKey(job.Key, job.Group);
                scheduler.DeleteJob(jobKey);
                _scheduledJobService.DeleteJob(job.Id);
            }
        }
        public JobKey RegisterBackgroundJob<T>(T jobToRun, Action<DailyTimeIntervalScheduleBuilder> action,
            IScheduler scheduler) where T : class
        {
            var job = JobBuilder.Create(jobToRun.GetType()).Build();
            var trigger = TriggerBuilder.Create().WithDailyTimeIntervalSchedule(action).Build();
            scheduler.ScheduleJob(job, trigger);
            return job.Key;
        }
        public JobKey RegisterBackgroundJob<T>(T jobToRun, Action<CalendarIntervalScheduleBuilder> action,
            IScheduler scheduler) where T : class
        {
            var job = JobBuilder.Create(jobToRun.GetType()).Build();
            var trigger = TriggerBuilder.Create().WithCalendarIntervalSchedule(action).Build();
            scheduler.ScheduleJob(job, trigger);
            return job.Key;
        }
        public bool ForceStart(string name)
        {
            try
            {
                var newJobInstance =
                    AssemblyManager.AssemblyManager.GetFor<IOxideScheduledBackgroundJob>()
                        .Where(instance => instance != null)
                        .Select(instance => instance as OxideScheduledBackgroundJob)
                        .Where(ins => ins?.Name != null)
                        .FirstOrDefault(ins => ins.Name == name);
                if (newJobInstance == null) return true;
                var scheduler = StdSchedulerFactory.GetDefaultScheduler();
                var minutesToFireDouble = (DateTime.UtcNow - DateTime.UtcNow).TotalMinutes;
                var minutesToFire = (int) Math.Floor(minutesToFireDouble);
                var timeToFire = DateBuilder.FutureDate(minutesToFire, IntervalUnit.Minute);
                var trigger =
                    TriggerBuilder.Create()
                        .StartAt(timeToFire)
                        .WithSimpleSchedule(x => x.WithMisfireHandlingInstructionFireNow())
                        .Build();
                var newJob = JobBuilder.Create(newJobInstance.GetType()).Build();
                scheduler.ScheduleJob(newJob, trigger);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool StartJob(string name)
        {
            try
            {
                var registeredJob = _scheduledJobService.IsJobExist(name);
                if (registeredJob == null) return false;
                var jobKey = new JobKey(registeredJob.Key, registeredJob.Group);
                var scheduler = StdSchedulerFactory.GetDefaultScheduler();
                registeredJob.Active = true;
                registeredJob.LastStart = DateTime.Now;
                _scheduledJobService.UpdateJob(registeredJob);
                scheduler.ResumeJob(jobKey);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool StopJob(string name)
        {
            try
            {
                var registeredJob = _scheduledJobService.IsJobExist(name);
                if (registeredJob == null) return false;
                var jobKey = new JobKey(registeredJob.Key, registeredJob.Group);
                var scheduler = StdSchedulerFactory.GetDefaultScheduler();
                registeredJob.Active = false;
                _scheduledJobService.UpdateJob(registeredJob);
                scheduler.PauseJob(jobKey);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool ReScheduled(string name)
        {
            try
            {
                var registeredJob = _scheduledJobService.IsJobExist(name);
                if (registeredJob != null)
                {
                    var scheduler = StdSchedulerFactory.GetDefaultScheduler();
                    var jobKey = new JobKey(registeredJob.Key, registeredJob.Group);
                    scheduler.DeleteJob(jobKey);
                    var newJob =
                        AssemblyManager.AssemblyManager.GetFor<IOxideScheduledBackgroundJob>()
                            .Where(instance => instance != null)
                            .Select(instance => instance as OxideScheduledBackgroundJob)
                            .Where(ins => ins?.Name != null)
                            .FirstOrDefault(ins => ins.Name == name);
                    jobKey = RegisterBackgroundJob(newJob, GetInterval(registeredJob), scheduler);
                    registeredJob.Key = jobKey.Name;
                    registeredJob.Group = jobKey.Group;
                    _scheduledJobService.UpdateJob(registeredJob);
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }
        public bool IsSuitable(ScheduledJob job)
        {
            if (SettingsManager.SettingsManager.IsAdmin())
                return job.CanRunOn == Syntax.Syntax.ScheduledJobRunPlatforms.Cms;
            return job.CanRunOn == Syntax.Syntax.ScheduledJobRunPlatforms.Front;
        }
        public bool IsSuitable(OxideScheduledBackgroundJob job)
        {
            if (SettingsManager.SettingsManager.IsAdmin())
                return job.CanRunOn == Syntax.Syntax.ScheduledJobRunPlatforms.Cms;
            return job.CanRunOn == Syntax.Syntax.ScheduledJobRunPlatforms.Front;
        }
        public bool IsSuitableToRun(ScheduledJob job, OxideScheduledBackgroundJob jobInstance)
        {
            return job != null ? IsSuitable(job) : IsSuitable(jobInstance);
        }
        public JobKey RegisterBackgroundJobSchedule<T>(T jobToRun, Action<CalendarIntervalScheduleBuilder> action,
            IScheduler scheduler) where T : class
        {
            var job = JobBuilder.Create(jobToRun.GetType()).Build();
            var trigger = TriggerBuilder.Create().WithCalendarIntervalSchedule(action).Build();
            scheduler.ScheduleJob(job, trigger);
            return job.Key;
        }
        private static Action<DailyTimeIntervalScheduleBuilder> GetInterval(ScheduledJob registeredJob)
        {
            var defaultInterval =
                new Action<DailyTimeIntervalScheduleBuilder>(s => s.WithIntervalInSeconds(15).OnEveryDay());
            if (registeredJob.Interval > 0)
            {
                switch (registeredJob.IntervalType)
                {
                    //EverySeconds
                    case 0:
                    {
                        switch (registeredJob.Type)
                        {
                            //Everyday
                            case 0:
                                defaultInterval = s => s.WithIntervalInSeconds(registeredJob.Interval).OnEveryDay();
                                break;
                            //Weekdays
                            case 1:
                                defaultInterval =
                                    s => s.WithIntervalInSeconds(registeredJob.Interval).OnMondayThroughFriday();
                                break;
                            //Weekend
                            case 2:
                                defaultInterval =
                                    s => s.WithIntervalInSeconds(registeredJob.Interval).OnSaturdayAndSunday();
                                break;
                        }
                        break;
                    }
                    //EveryMinutes
                    case 1:
                    {
                        switch (registeredJob.Type)
                        {
                            //Everyday
                            case 0:
                                defaultInterval = s => s.WithIntervalInMinutes(registeredJob.Interval).OnEveryDay();
                                break;
                            //Weekdays
                            case 1:
                                defaultInterval =
                                    s => s.WithIntervalInMinutes(registeredJob.Interval).OnMondayThroughFriday();
                                break;
                            //Weekend
                            case 2:
                                defaultInterval =
                                    s => s.WithIntervalInMinutes(registeredJob.Interval).OnSaturdayAndSunday();
                                break;
                        }
                        break;
                    }
                    //EveryHours
                    case 2:
                    {
                        switch (registeredJob.Type)
                        {
                            //Everyday
                            case 0:
                                defaultInterval = s => s.WithIntervalInHours(registeredJob.Interval).OnEveryDay();
                                break;
                            //Weekdays
                            case 1:
                                defaultInterval =
                                    s => s.WithIntervalInHours(registeredJob.Interval).OnMondayThroughFriday();
                                break;
                            //Weekend
                            case 2:
                                defaultInterval =
                                    s => s.WithIntervalInHours(registeredJob.Interval).OnSaturdayAndSunday();
                                break;
                        }
                        break;
                    }
                }
            }
            return defaultInterval;
        }
    }
}