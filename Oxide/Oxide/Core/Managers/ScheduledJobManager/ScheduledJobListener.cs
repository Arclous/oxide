﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using Oxide.Core.Data.Context;
using Oxide.Core.Data.Repository;
using Oxide.Core.Models.ScheduledJob;
using Oxide.Core.Services.Implemantation;
using Oxide.Core.Services.ScheduledJobService;
using Quartz;

namespace Oxide.Core.Managers.ScheduledJobManager
{
    public class ScheduledJobListener : IJobListener
    {
        public void JobToBeExecuted(IJobExecutionContext context)
        {
            var provider = new ProviderManager.ProviderManager();
            var scheduledJobService = provider.Provide<IScheduledJobService>();
            var jobDetail = context.JobDetail.JobType;
            var jobInstance = (OxideScheduledBackgroundJob) context.JobInstance;
            var registeredJob = scheduledJobService.IsJobExist(jobInstance.Name);
            if (registeredJob != null)
            {
                var jobKey = new JobKey(registeredJob.Key, registeredJob.Group);
                if (!registeredJob.Active)
                {
                    context.Scheduler.PauseJob(jobKey);
                    return;
                }
                var triggers = context.Scheduler.GetTriggersOfJob(jobKey);
                if (triggers.Count > 0)
                {
                    var nextFireTimeUtc = triggers[0].GetNextFireTimeUtc();
                    if (nextFireTimeUtc != null)
                        registeredJob.NextStart = TimeZone.CurrentTimeZone.ToLocalTime(nextFireTimeUtc.Value.DateTime);
                }
                registeredJob.LastStart = DateTime.Now;
                scheduledJobService.UpdateJob(registeredJob);
            }
        }
        public void JobExecutionVetoed(IJobExecutionContext context)
        {
        }
        public void JobWasExecuted(IJobExecutionContext context, JobExecutionException jobException)
        {
            var scheduledJobService = new ScheduledJobService(new OxideRepository<ScheduledJob>(new OxideContext()));
            var jobDetail = context.JobDetail.JobType;
            var jobInstance = (OxideScheduledBackgroundJob) context.JobInstance;
            var registeredJob = scheduledJobService.IsJobExist(jobInstance.Name);
            if (registeredJob != null)
            {
                registeredJob.LastFinish = DateTime.Now;
                scheduledJobService.UpdateJob(registeredJob);
            }
        }
        public string Name { get; } = "OxideJobListener";
    }
}