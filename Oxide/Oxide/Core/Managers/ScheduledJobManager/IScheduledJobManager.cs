﻿using System;
using Oxide.Core.Base.OxideDependency;
using Oxide.Core.Models.ScheduledJob;
using Oxide.Core.Services.Implemantation;
using Quartz;

namespace Oxide.Core.Managers.ScheduledJobManager
{
    /// <summary>
    ///     Interface for scheduled job manager
    /// </summary>
    public interface IScheduledJobManager : IOxideDependency
    {
        /// <summary>
        ///     Registers all scheduled backgroud jobs in the solution for the activated modules
        /// </summary>
        void RegisterAllBackgroundJobs();
        /// <summary>
        ///     Registers all scheduled backgroud jobs in the give module
        /// </summary>
        /// <param name="moduleName">Name of the module</param>
        void RegisterAllBackgroundJobs(string moduleName);
        /// <summary>
        ///     UnRegisters all scheduled backgroud jobs
        /// </summary>
        void UnRegisterScheduledAllJobs();
        /// <summary>
        ///     Registers all scheduled backgroud jobs in the give module
        /// </summary>
        /// <param name="moduleName">Name of the module</param>
        void UnRegisterScheduledAllJobs(string moduleName);
        /// <summary>
        ///     Registers scheduled backgroud jobs
        /// </summary>
        /// <param name="jobToRun">Job to run</param>
        /// <param name="action">Timing action for job</param>
        /// <param name="scheduler">Scheduled for job</param>
        JobKey RegisterBackgroundJob<T>(T jobToRun, Action<DailyTimeIntervalScheduleBuilder> action,
            IScheduler scheduler) where T : class;
        /// <summary>
        ///     Registers scheduled backgroud jobs
        /// </summary>
        /// <param name="jobToRun">Job to run</param>
        /// <param name="action">Timing action for job</param>
        /// <param name="scheduler">Scheduled for job</param>
        JobKey RegisterBackgroundJob<T>(T jobToRun, Action<CalendarIntervalScheduleBuilder> action, IScheduler scheduler)
            where T : class;
        /// <summary>
        ///     Force start
        /// </summary>
        /// <param name="name">Name of the job</param>
        bool ForceStart(string name);
        /// <summary>
        ///     Stops job
        /// </summary>
        /// <param name="name">Name of the job</param>
        bool StartJob(string name);
        /// <summary>
        ///     Stops job
        /// </summary>
        /// <param name="name">Name of the job</param>
        bool StopJob(string name);
        /// <summary>
        ///     Re Schedules job
        /// </summary>
        /// <param name="name">Name of the job</param>
        bool ReScheduled(string name);
        /// <summary>
        ///     Check schedule job if it is ok to run on current domain role
        /// </summary>
        /// <param name="job">Scheduled job</param>
        bool IsSuitable(ScheduledJob job);
        /// <summary>
        ///     Check schedule job if it is ok to run on current domain role
        /// </summary>
        /// <param name="job">Scheduled job</param>
        bool IsSuitable(OxideScheduledBackgroundJob job);
        /// <summary>
        ///     Check schedule job if it is ok to run on current domain role according to one of the job
        /// </summary>
        /// <param name="job">Scheduled job</param>
        /// <param name="jobInstance">
        ///     Scheduled job<</param>
        bool IsSuitableToRun(ScheduledJob job, OxideScheduledBackgroundJob jobInstance);
    }
}