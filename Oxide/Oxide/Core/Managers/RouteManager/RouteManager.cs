﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System.Web.Mvc;
using System.Web.Routing;
using Oxide.Core.Services.Interface.Registers.RouteRegister;
using Oxide.Core.Syntax;

namespace Oxide.Core.Managers.RouteManager
{
    public static class RouteManager
    {
        public static void Register(RouteCollection routeCollection)
        {
            routeCollection.IgnoreRoute("{resource}.axd/{*pathInfo}");
            var adminAreaContext = RegisterAdminArea(routeCollection);
            RegisterRoutes(routeCollection);
            RegisterAreas(routeCollection);
            RegisterDefault(adminAreaContext);
        }
        private static void RegisterAreas(RouteCollection routeCollection)
        {
            foreach (var instance in Oxide.LoadedModules)
            {
                var areaContext = new AreaRegistrationContext(instance.Name, routeCollection);
                string routeName = $@"{instance.Name}{Constants.Default}";
                areaContext.MapRoute(routeName,
                    $@"{instance.Name}/{Constants.Controller}/{Constants.Action}/{Constants.Id}",
                    new {action = Constants.Index, id = UrlParameter.Optional},
                    new[] {instance.Name + Constants.Controllers});
            }
        }
        private static AreaRegistrationContext RegisterAdminArea(RouteCollection routeCollection)
        {
            var areaContext = new AreaRegistrationContext(Constants.Admin, routeCollection);
            areaContext.MapRoute(Constants.Admin,
                $@"{Constants.Admin}/{Constants.Controller}/{Constants.Action}/{Constants.Id}",
                new {controller = Constants.Admin, action = Constants.Index, id = UrlParameter.Optional},
                new[] {Constants.AdminController});
            return areaContext;
        }
        private static void RegisterDefault(AreaRegistrationContext adminAreaRegistrationContext)
        {
            adminAreaRegistrationContext.MapRoute(Constants.DefaultKeyword, Constants.UrlPattern,
                new {controller = Constants.Front, action = Constants.Index}, new[] {Constants.AdminController});
        }
        /// <summary>
        ///     Runs all class which is inherited from IRouteRegisterEngine interface
        /// </summary>
        private static void RegisterRoutes(RouteCollection routes)
        {
            Oxide.Routes = routes;
            foreach (var instance in AssemblyManager.AssemblyManager.GetFor<IRouteRegisterer>())
            {
                instance.RegisterRoute(routes);
            }
        }
    }
}