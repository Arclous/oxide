﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Transactions;
using EntityFramework.Utilities;
using Oxide.Core.Base.OxideEvents;
using Oxide.Core.Base.OxideEvents.Events;
using Oxide.Core.Data.Context;
using Oxide.Core.Extesions;
using Oxide.Core.Syntax;
using IsolationLevel = System.Transactions.IsolationLevel;

namespace Oxide.Core.Data.Repository
{
    public class OxideRepository<TEntity> : IOxideRepository<TEntity> where TEntity : class
    {
        public OxideRepository()
        {
        }
        public OxideRepository(DbContext dbContext)
        {
            DbContext = dbContext;
        }
        public DbContext DbContext { get; set; }
        public bool TrigerEventForBulkActions { get; set; }
        public IQueryable<TEntity> Table => DbContext.Set<TEntity>();
        public void Insert(TEntity entity)
        {
            DbContext.Set<TEntity>().Add(entity);
            DbContext.SaveChanges();
            OxideEvents.Raise(new OnCreated<TEntity> {Entity = new object[] {entity}});
        }
        public void Insert(TEntity entity, IsolationLevel isolationLevel)
        {
            using (
                var scope = new TransactionScope(TransactionScopeOption.Required,
                    new TransactionOptions {IsolationLevel = isolationLevel}))
            {
                DbContext.Set<TEntity>().Add(entity);
                DbContext.SaveChanges();
                scope.Complete();
                OxideEvents.Raise(new OnCreated<TEntity> {Entity = new object[] {entity}});
            }
        }
        public void InsertRange(List<TEntity> entities)
        {
            DbContext.Set<TEntity>().AddRange(entities);
            DbContext.SaveChanges();
            if (TrigerEventForBulkActions)
                OxideEvents.Raise(new OnCreated<TEntity> {Entity = entities.ToArray()});
        }
        public void InsertRange(List<TEntity> entities, IsolationLevel isolationLevel)
        {
            using (
                var scope = new TransactionScope(TransactionScopeOption.Required,
                    new TransactionOptions {IsolationLevel = isolationLevel}))
            {
                DbContext.Set<TEntity>().AddRange(entities);
                DbContext.SaveChanges();
                scope.Complete();
                if (TrigerEventForBulkActions)
                    OxideEvents.Raise(new OnCreated<TEntity> {Entity = entities.ToArray()});
            }
        }
        public void BulkInsert(List<TEntity> entities)
        {
            var tableName = typeof (TEntity).GetTableName();
            var oxideContext=new OxideContext();
           
            var conn = new SqlConnection(oxideContext.Database.Connection.ConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            using (var bulkCopy = new SqlBulkCopy(conn))
            {
                bulkCopy.BatchSize = entities.Count;
                bulkCopy.DestinationTableName = tableName;
                var table = ListToDataTable(entities);
                var properties = entities[0].GetType().GetProperties();
                foreach (var prp in properties)
                {
                    if (prp.GetMethod.IsVirtual)
                        bulkCopy.ColumnMappings.Add(prp.Name, prp.Name + Constants.DefaultIdentity);
                    else
                        bulkCopy.ColumnMappings.Add(prp.Name, prp.Name);
                }
                bulkCopy.WriteToServer(table);
            }
            if (TrigerEventForBulkActions)
                OxideEvents.Raise(new OnCreated<TEntity> {Entity = entities.ToArray()});
        }
        public void BulkInsert(List<TEntity> entities, System.Data.IsolationLevel isolationLevel, int timeOut)
        {
            var tableName = typeof (TEntity).GetTableName();
            var conn = new SqlConnection(DbContext.Database.Connection.ConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            using (var tran = conn.BeginTransaction(isolationLevel))
            {
                using (var bulkCopy = new SqlBulkCopy(conn, SqlBulkCopyOptions.Default, tran))
                {
                    bulkCopy.BatchSize = entities.Count;
                    bulkCopy.DestinationTableName = tableName;
                    bulkCopy.BulkCopyTimeout = timeOut;
                    var table = ListToDataTable(entities);
                    var properties = entities[0].GetType().GetProperties();
                    foreach (var prp in properties)
                    {
                        if (prp.GetMethod.IsVirtual)
                            bulkCopy.ColumnMappings.Add(prp.Name, prp.Name + Constants.DefaultIdentity);
                        else
                            bulkCopy.ColumnMappings.Add(prp.Name, prp.Name);
                    }
                    bulkCopy.WriteToServer(table);
                }
                tran.Commit();
                if (TrigerEventForBulkActions)
                    OxideEvents.Raise(new OnCreated<TEntity> {Entity = entities.ToArray()});
            }

        }
        public void LazyInsert(TEntity entity)
        {
            DbContext.Set<TEntity>().Add(entity);
            OxideEvents.Raise(new OnCreated<TEntity> {Entity = new object[] {entity}});
        }
        public void InsertSlient(TEntity entity)
        {
            DbContext.Set<TEntity>().Add(entity);
            DbContext.SaveChanges();
        }
        public void Delete(TEntity entity)
        {
            DbContext.Set<TEntity>().Remove(entity);
            DbContext.SaveChanges();
            OxideEvents.Raise(new OnDeleted<TEntity> {Entity = new object[] {entity}});
        }
        public void Delete(TEntity entity, IsolationLevel isolationLevel)
        {
            using (
                var scope = new TransactionScope(TransactionScopeOption.Required,
                    new TransactionOptions {IsolationLevel = isolationLevel}))
            {
                DbContext.Set<TEntity>().Remove(entity);
                DbContext.SaveChanges();
                scope.Complete();
                OxideEvents.Raise(new OnDeleted<TEntity> {Entity = new object[] {entity}});
            }
        }
        public void LazyDelete(TEntity entity)
        {
            DbContext.Set<TEntity>().Remove(entity);
            OxideEvents.Raise(new OnDeleted<TEntity> {Entity = new object[] {entity}});
        }
        public void Delete(int id)
        {
            var targetModel = GetById(id);
            DbContext.Set<TEntity>().Remove(DbContext.Set<TEntity>().Find(id));
            DbContext.SaveChanges();
            OxideEvents.Raise(new OnDeleted<TEntity> {Entity = new object[] {targetModel}});
        }
        public void Delete(int id, IsolationLevel isolationLevel)
        {
            var targetModel = GetById(id);
            using (
                var scope = new TransactionScope(TransactionScopeOption.Required,
                    new TransactionOptions {IsolationLevel = isolationLevel}))
            {
                DbContext.Set<TEntity>().Remove(DbContext.Set<TEntity>().Find(id));
                DbContext.SaveChanges();
                scope.Complete();
                OxideEvents.Raise(new OnDeleted<TEntity> {Entity = new object[] {targetModel}});
            }
        }
        public void LazyDelete(int id)
        {
            var entity = DbContext.Set<TEntity>().Find(id);
            DbContext.Set<TEntity>().Remove(entity);
            OxideEvents.Raise(new OnDeleted<TEntity> {Entity = new object[] {entity}});
        }
        public void DeleteAll()
        {
            var deleteSet = DbContext.Set<TEntity>();
            DbContext.Set<TEntity>().RemoveRange(deleteSet);
            if (TrigerEventForBulkActions)
                OxideEvents.Raise(new OnDeleted<TEntity> {Entity = deleteSet.ToArray()});
            DbContext.SaveChanges();
        }
        public void DeleteAll(IsolationLevel isolationLevel)
        {
            using (
                var scope = new TransactionScope(TransactionScopeOption.Required,
                    new TransactionOptions {IsolationLevel = isolationLevel}))
            {
                var deleteSet = DbContext.Set<TEntity>();
                DbContext.Set<TEntity>().RemoveRange(deleteSet);
                if (TrigerEventForBulkActions)
                    OxideEvents.Raise(new OnDeleted<TEntity> {Entity = deleteSet.ToArray()});
                DbContext.SaveChanges();
                scope.Complete();
            }
        }
        public void DeleteAll(Expression<Func<TEntity, bool>> predicate)
        {
            var deleteSet = DbContext.Set<TEntity>().Where(predicate);
            DbContext.Set<TEntity>().RemoveRange(deleteSet);
            if (TrigerEventForBulkActions)
                OxideEvents.Raise(new OnDeleted<TEntity> {Entity = deleteSet.ToArray()});
            DbContext.SaveChanges();
        }
        public void DeleteAll(Expression<Func<TEntity, bool>> predicate, IsolationLevel isolationLevel)
        {
            using (
                var scope = new TransactionScope(TransactionScopeOption.Required,
                    new TransactionOptions {IsolationLevel = isolationLevel}))
            {
                var deleteSet = DbContext.Set<TEntity>().Where(predicate);
                DbContext.Set<TEntity>().RemoveRange(deleteSet);
                if (TrigerEventForBulkActions)
                    OxideEvents.Raise(new OnDeleted<TEntity> {Entity = deleteSet.ToArray()});
                DbContext.SaveChanges();
                scope.Complete();
            }
        }
        public void BulkDeleteAll(Expression<Func<TEntity, bool>> predicate)
        {
            var deleteSet = DbContext.Set<TEntity>().Where(predicate);
            DbContext.Set<TEntity>().RemoveRange(deleteSet);
            if (TrigerEventForBulkActions)
                OxideEvents.Raise(new OnDeleted<TEntity> {Entity = deleteSet.ToArray()});
        }
        public void BulkDeleteAll(Expression<Func<TEntity, bool>> predicate, IsolationLevel isolationLevel)
        {
            using (
                var scope = new TransactionScope(TransactionScopeOption.Required,
                    new TransactionOptions {IsolationLevel = isolationLevel}))
            {
                var deleteSet = DbContext.Set<TEntity>().Where(predicate);
                DbContext.Set<TEntity>().RemoveRange(deleteSet);
                if (TrigerEventForBulkActions)
                    OxideEvents.Raise(new OnDeleted<TEntity> {Entity = deleteSet.ToArray()});
                scope.Complete();
            }
        }
        public void DeleteSlient(TEntity entity)
        {
            DbContext.Set<TEntity>().Remove(entity);
            DbContext.SaveChanges();
        }
        public void Update(TEntity entity)
        {
            DbContext.Set<TEntity>().AddOrUpdate(entity);
            DbContext.SaveChanges();
            OxideEvents.Raise(new OnUpdated<TEntity> {Entity = new object[] {entity}});
        }
        public void Update(TEntity entity, IsolationLevel isolationLevel)
        {
            using (
                var scope = new TransactionScope(TransactionScopeOption.Required,
                    new TransactionOptions {IsolationLevel = isolationLevel}))
            {
                DbContext.Set<TEntity>().AddOrUpdate(entity);
                DbContext.SaveChanges();
                scope.Complete();
                OxideEvents.Raise(new OnUpdated<TEntity> {Entity = new object[] {entity}});
            }
        }
        public void LazyUpdate(TEntity entity)
        {
            DbContext.Set<TEntity>().AddOrUpdate(entity);
            OxideEvents.Raise(new OnUpdated<TEntity> {Entity = new object[] {entity}});
        }
        public void BulkUpdate(List<TEntity> entities, Expression<Func<TEntity, object>> properties)
        {
            EFBatchOperation.For(DbContext, DbContext.Set<TEntity>())
                .UpdateAll(entities, x => x.ColumnsToUpdate(properties));
            if (TrigerEventForBulkActions)
                OxideEvents.Raise(new OnUpdated<TEntity> {Entity = entities.ToArray()});
        }
        public void BulkUpdate(List<TEntity> entities, Expression<Func<TEntity, object>> properties,
            IsolationLevel isolationLevel)
        {
            using (
                var scope = new TransactionScope(TransactionScopeOption.Required,
                    new TransactionOptions {IsolationLevel = isolationLevel}))
            {
                EFBatchOperation.For(DbContext, DbContext.Set<TEntity>())
                    .UpdateAll(entities, x => x.ColumnsToUpdate(properties));
                if (TrigerEventForBulkActions)
                    OxideEvents.Raise(new OnUpdated<TEntity> {Entity = entities.ToArray()});
                scope.Complete();
            }
        }
        public void BulkUpdate<TP>(List<TEntity> entities, Expression<Func<TEntity, bool>> predicate,
            Expression<Func<TEntity, TP>> prop, Expression<Func<TEntity, TP>> modifier)
        {
            EFBatchOperation.For(DbContext, DbContext.Set<TEntity>()).Where(predicate).Update(prop, modifier);
            if (TrigerEventForBulkActions)
                OxideEvents.Raise(new OnUpdated<TEntity> {Entity = entities.ToArray()});
        }
        public void BulkUpdate<TP>(List<TEntity> entities, Expression<Func<TEntity, bool>> predicate,
            Expression<Func<TEntity, TP>> prop, Expression<Func<TEntity, TP>> modifier, IsolationLevel isolationLevel)
        {
            using (
                var scope = new TransactionScope(TransactionScopeOption.Required,
                    new TransactionOptions {IsolationLevel = isolationLevel}))
            {
                EFBatchOperation.For(DbContext, DbContext.Set<TEntity>()).Where(predicate).Update(prop, modifier);
                if (TrigerEventForBulkActions)
                    OxideEvents.Raise(new OnUpdated<TEntity> {Entity = entities.ToArray()});
                scope.Complete();
            }
        }
        public void UpdateSlient(TEntity entity)
        {
            DbContext.Set<TEntity>().AddOrUpdate(entity);
            DbContext.SaveChanges();
        }
        public IEnumerable<TEntity> SearchFor(Expression<Func<TEntity, bool>> predicate,
            params Expression<Func<TEntity, object>>[] include)
        {
            return !include.Any() ? DbContext.Set<TEntity>().Where(predicate) : GetIncludeExpression(include).Where(predicate);
        }

        public IEnumerable<TEntity> SearchFor(Expression<Func<TEntity, bool>> predicate, IsolationLevel isolationLevel,
            params Expression<Func<TEntity, object>>[] include)
        {
            using (
                var scope = new TransactionScope(TransactionScopeOption.Required,
                    new TransactionOptions {IsolationLevel = isolationLevel}))
            {
                scope.Complete();
                if (!include.Any()) return DbContext.Set<TEntity>().Where(predicate);
                return GetIncludeExpression(include).Where(predicate);
            }
        }
        public IQueryable<TEntity> GetAll(params Expression<Func<TEntity, object>>[] include)
        {
            if (!include.Any()) return DbContext.Set<TEntity>();
            return GetIncludeExpression(include);
        }
        public IQueryable<TEntity> GetAll(IsolationLevel isolationLevel,
            params Expression<Func<TEntity, object>>[] include)
        {
            using (
                var scope = new TransactionScope(TransactionScopeOption.Required,
                    new TransactionOptions {IsolationLevel = isolationLevel}))
            {
                scope.Complete();
                if (!include.Any()) return DbContext.Set<TEntity>();
                return GetIncludeExpression(include);
            }
        }
        public TEntity GetById(int id, params Expression<Func<TEntity, object>>[] include)
        {
            if (!include.Any()) return id == 0 ? null : DbContext.Set<TEntity>().Find(id);
            return GetIncludeExpression(include).FirstOrDefault(GetIdEpxression(id));
        }
        public TEntity GetById(int id, IsolationLevel isolationLevel, params Expression<Func<TEntity, object>>[] include)
        {
            using (
                var scope = new TransactionScope(TransactionScopeOption.Required,
                    new TransactionOptions {IsolationLevel = isolationLevel}))
            {
                scope.Complete();
                if (!include.Any()) return id == 0 ? null : DbContext.Set<TEntity>().Find(id);
                return GetIncludeExpression(include).FirstOrDefault(GetIdEpxression(id));
            }
        }
        public IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            List<Expression<Func<TEntity, object>>> includeProperties = null, int? page = null, int? pageSize = null)
        {
            IQueryable<TEntity> query = DbContext.Set<TEntity>();
            includeProperties?.ForEach(i => { query = query.Include(i); });
            if (filter != null)
                query = query.Where(filter);
            if (orderBy != null)
                query = orderBy(query);
            if (page != null && pageSize != null)
                query = query.Skip((page.Value)*pageSize.Value).Take(pageSize.Value);
            return query;
        }
        public IEnumerable<TEntity> Get(IsolationLevel isolationLevel, Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            List<Expression<Func<TEntity, object>>> includeProperties = null, int? page = null, int? pageSize = null)
        {
            using (
                var scope = new TransactionScope(TransactionScopeOption.Required,
                    new TransactionOptions {IsolationLevel = isolationLevel}))
            {
                scope.Complete();
                IQueryable<TEntity> query = DbContext.Set<TEntity>();
                includeProperties?.ForEach(i => { query = query.Include(i); });
                if (filter != null)
                    query = query.Where(filter);
                if (orderBy != null)
                    query = orderBy(query);
                if (page != null && pageSize != null)
                    query = query.Skip((page.Value)*pageSize.Value).Take(pageSize.Value);
                return query;
            }
        }
        public void SaveChanges()
        {
            DbContext.SaveChanges();
        }
        public void SaveChanges(IsolationLevel isolationLevel)
        {
            using (
                var scope = new TransactionScope(TransactionScopeOption.Required,
                    new TransactionOptions {IsolationLevel = isolationLevel}))
            {
                DbContext.SaveChanges();
                scope.Complete();
            }
        }
        public int TotalRecords()
        {
            return DbContext.Set<TEntity>().Count();
        }
        public int TotalRecords(Expression<Func<TEntity, bool>> predicate)
        {
            return DbContext.Set<TEntity>().Where(predicate).Count();
        }
        private IQueryable<TEntity> GetIncludeExpression(params Expression<Func<TEntity, object>>[] include)
        {
            return include.Aggregate<Expression<Func<TEntity, object>>, IQueryable<TEntity>>(DbContext.Set<TEntity>(),
                (current, expression) => current.Include(expression));
        }
        private static Expression<Func<TEntity, bool>> GetIdEpxression(int id)
        {
            var param = Expression.Parameter(typeof (TEntity), "Id");
            var exp =
                Expression.Lambda<Func<TEntity, bool>>(
                    Expression.Equal(Expression.Property(param, "Id"), Expression.Constant(id)), param);
            return exp;
        }
        private static DataTable ListToDataTable(IList list)
        {
            var dt = new DataTable();
            if (list.Count <= 0) return dt;
            var properties = list[0].GetType().GetProperties();
            foreach (var pi in properties.Where(pi => !dt.Columns.Contains(pi.Name)))
            {
                if (!pi.GetMethod.IsVirtual)
                    dt.Columns.Add(pi.Name, Nullable.GetUnderlyingType(pi.PropertyType) ?? pi.PropertyType);
                else
                    dt.Columns.Add(pi.Name, typeof (int));
            }
            foreach (var item in list)
            {
                var row = dt.NewRow();
                foreach (var prp in properties)
                {
                    if (!prp.GetMethod.IsVirtual)
                        row[prp.Name] = prp.GetValue(item, null) ?? DBNull.Value;
                    else
                    {
                        var value = prp.GetValue(item, null) ?? DBNull.Value;
                        var valueId =
                            value.GetType()
                                .GetProperties()
                                .FirstOrDefault(x => x.Name == Constants.DefaultIdentityField);
                        if (valueId != null) value = valueId.GetValue(value, null) ?? DBNull.Value;
                        row[prp.Name] = value;
                    }
                }
                dt.Rows.Add(row);
            }
            return dt;
        }
    }
}