﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Transactions;

namespace Oxide.Core.Data.Repository
{
    /// <summary>
    ///     Base layer which cominicates with database, all database process should be done via this class
    /// </summary>
    public interface IOxideRepository<T>
    {
        /// <summary>
        ///     Get entitiy table
        /// </summary>
        /// <returns>Entity as IQueryable</returns>
        IQueryable<T> Table { get; }

        /// <summary>
        ///     Inserts entitiy and call save changes automatically
        /// </summary>
        /// <param name="entity">Type of entitiy</param>
        void Insert(T entity);
        /// <summary>
        ///     Inserts entitiy and call save changes automatically
        /// </summary>
        /// <param name="entity">Type of entitiy</param>
        /// <param name="isolationLevel">Isolation level</param>
        void Insert(T entity, IsolationLevel isolationLevel);
        /// <summary>
        ///     Inserts range of entitiy and call save changes automatically
        /// </summary>
        /// <param name="entities">List of entitiy</param>
        void InsertRange(List<T> entities);
        /// <summary>
        ///     Inserts range of entitiy and call save changes automatically
        /// </summary>
        /// <param name="entities">List of entitiy</param>
        /// <param name="isolationLevel">Isolation level</param>
        void InsertRange(List<T> entities, IsolationLevel isolationLevel);
        /// <summary>
        ///     Bulk Inserts range of entitiy and call save changes automatically
        /// </summary>
        /// <param name="entities">List of entitiy</param>
        void BulkInsert(List<T> entities);
        /// <summary>
        ///     Bulk Inserts range of entitiy and call save changes automatically
        /// </summary>
        /// <param name="entities">List of entitiy</param>
        /// <param name="isolationLevel">Isolation level</param>
        /// <param name="timeOut">Timeout for bulk operation</param>
        void BulkInsert(List<T> entities, System.Data.IsolationLevel isolationLevel, int timeOut);
        /// <summary>
        ///     Inserts entitiy,will not call save changes automatically
        /// </summary>
        /// <param name="entity">Type of entitiy</param>
        void LazyInsert(T entity);
        /// <summary>
        ///     Inserts entitiy and call save changes automatically without event
        /// </summary>
        /// <param name="entity">Type of entitiy</param>
        void InsertSlient(T entity);
        /// <summary>
        ///     Deletes entitiy and call save changes automatically
        /// </summary>
        /// <param name="entity">Type of entitiy</param>
        void Delete(T entity);
        /// <summary>
        ///     Deletes entitiy and call save changes automatically
        /// </summary>
        /// <param name="entity">Type of entitiy</param>
        /// <param name="isolationLevel">Isolation level</param>
        void Delete(T entity, IsolationLevel isolationLevel);
        /// <summary>
        ///     Deletes entitiy,will not call save changes automatically
        /// </summary>
        /// <param name="entity">Type of entitiy</param>
        void LazyDelete(T entity);
        /// <summary>
        ///     Deletes entitiy and call save changes automatically
        /// </summary>
        /// <param name="id">Id of entitiy</param>
        void Delete(int id);
        /// <summary>
        ///     Deletes entitiy and call save changes automatically
        /// </summary>
        /// <param name="id">Id of entitiy</param>
        /// <param name="isolationLevel">Isolation level</param>
        void Delete(int id, IsolationLevel isolationLevel);
        /// <summary>
        ///     Deletes entitiy will not call save changes automatically
        /// </summary>
        /// <param name="id">Id of entitiy</param>
        void LazyDelete(int id);
        /// <summary>
        ///     Deletes all entities and call save changes automatically
        /// </summary>
        void DeleteAll();
        /// <summary>
        ///     Deletes all entities and call save changes automatically
        /// </summary>
        /// <param name="isolationLevel">Isolation level</param>
        void DeleteAll(IsolationLevel isolationLevel);
        /// <summary>
        ///     Deletes all entities according to criteria and call save changes automatically
        /// </summary>
        /// <param name="predicate">Predicate</param>
        void DeleteAll(Expression<Func<T, bool>> predicate);
        /// <summary>
        ///     Deletes all entities according to criteria and call save changes automatically
        /// </summary>
        /// <param name="predicate">Predicate</param>
        /// <param name="isolationLevel">Isolation level</param>
        void DeleteAll(Expression<Func<T, bool>> predicate, IsolationLevel isolationLevel);
        /// <summary>
        ///     Deletes all entities according to criteria, will not call save changes automatically
        /// </summary>
        /// <param name="predicate">Predicate</param>
        void BulkDeleteAll(Expression<Func<T, bool>> predicate);
        /// <summary>
        ///     Deletes all entities according to criteria, will not call save changes automatically
        /// </summary>
        /// <param name="predicate">Predicate</param>
        /// <param name="isolationLevel">Isolation level</param>
        void BulkDeleteAll(Expression<Func<T, bool>> predicate, IsolationLevel isolationLevel);
        /// <summary>
        ///     Deletes entitiy and call save changes automatically without event
        /// </summary>
        /// <param name="entity">Type of entitiy</param>
        void DeleteSlient(T entity);
        /// <summary>
        ///     Updates entitiy and call save changes automatically
        /// </summary>
        /// <param name="entity">Entity to update</param>
        void Update(T entity);
        /// <summary>
        ///     Updates entitiy and call save changes automatically
        /// </summary>
        /// <param name="entity">Entity to update</param>
        /// <param name="isolationLevel">Isolation level</param>
        void Update(T entity, IsolationLevel isolationLevel);
        /// <summary>
        ///     Updates entitiy will not call save changes automatically
        /// </summary>
        /// <param name="entity">Entity to update</param>
        void LazyUpdate(T entity);
        /// <summary>
        ///     Bulk Updates range of entitiy and call save changes automatically
        /// </summary>
        /// <param name="entities">Entities to update</param>
        /// <param name="properties">Properities to update</param>
        void BulkUpdate(List<T> entities, Expression<Func<T, object>> properties);
        /// <summary>
        ///     Bulk Updates range of entitiy and call save changes automatically
        /// </summary>
        /// <param name="entities">Entities to update</param>
        /// <param name="properties">Properities to update</param>
        /// <param name="isolationLevel">Isolation level</param>
        void BulkUpdate(List<T> entities, Expression<Func<T, object>> properties, IsolationLevel isolationLevel);
        /// <summary>
        ///     Bulk Updates range of entitiy and call save changes automatically
        /// </summary>
        /// <param name="entities">Entities to update</param>
        /// <param name="predicate">Predicate</param>
        /// <param name="prop">Prop</param>
        /// <param name="modifier">Modifier</param>
        void BulkUpdate<TP>(List<T> entities, Expression<Func<T, bool>> predicate, Expression<Func<T, TP>> prop,
            Expression<Func<T, TP>> modifier);
        /// <summary>
        ///     Bulk Updates range of entitiy and call save changes automatically
        /// </summary>
        /// <param name="entities">Entities to update</param>
        /// <param name="predicate">Predicate</param>
        /// <param name="prop">Prop</param>
        /// <param name="modifier">Modifier</param>
        /// <param name="isolationLevel">Isolation level</param>
        void BulkUpdate<TP>(List<T> entities, Expression<Func<T, bool>> predicate, Expression<Func<T, TP>> prop,
            Expression<Func<T, TP>> modifier, IsolationLevel isolationLevel);
        /// <summary>
        ///     Updates entitiy and call save changes automatically without event
        /// </summary>
        /// <param name="entity">Entity to update</param>
        void UpdateSlient(T entity);
        /// <summary>
        ///     Search for entitiy
        /// </summary>
        /// <param name="predicate">Expression for searching</param>
        /// <param name="include">Expression for including</param>
        /// <returns>Entities as IEnumerable</returns>
        IEnumerable<T> SearchFor(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] include);
        /// <summary>
        ///     Search for entitiy
        /// </summary>
        /// <param name="predicate">Expression for searching</param>
        /// <param name="isolationLevel">Isolation level</param>
        /// <param name="include">Expression for including</param>
        /// <returns>Entities as IEnumerable</returns>
        IEnumerable<T> SearchFor(Expression<Func<T, bool>> predicate, IsolationLevel isolationLevel,
            params Expression<Func<T, object>>[] include);
        /// <summary>
        ///     Get all entities
        /// </summary>
        /// <param name="include">Expression for including</param>
        /// <returns>Entities as IQueryable</returns>
        IQueryable<T> GetAll(params Expression<Func<T, object>>[] include);
        /// <summary>
        ///     Get all entities
        /// </summary>
        /// <param name="isolationLevel">Isolation level</param>
        /// <param name="include">Expression for including</param>
        /// <returns>Entities as IQueryable</returns>
        IQueryable<T> GetAll(IsolationLevel isolationLevel, params Expression<Func<T, object>>[] include);
        /// <summary>
        ///     Get entitiy by id
        /// </summary>
        /// <param name="id">Id of entitiy</param>
        /// <param name="include">Expression for including</param>
        /// <returns>Entity as type</returns>
        T GetById(int id, params Expression<Func<T, object>>[] include);
        /// <summary>
        ///     Get entitiy by id
        /// </summary>
        /// <param name="id">Id of entitiy</param>
        /// <param name="isolationLevel">Isolation level</param>
        /// <param name="include">Expression for including</param>
        /// <returns>Entity as type</returns>
        T GetById(int id, IsolationLevel isolationLevel, params Expression<Func<T, object>>[] include);
        /// <summary>
        ///     Get entities according to filter
        /// </summary>
        /// <param name="filter">Filter for getting entities</param>
        /// <param name="orderBy">Order criteria</param>
        /// <param name="includeProperties">Included properties</param>
        /// <param name="page">Page nmber</param>
        /// <param name="pageSize">Item number of the page</param>
        /// <param name="include">Expression for including</param>
        /// <returns>Entities as IEnumerable</returns>
        IEnumerable<T> Get(Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            List<Expression<Func<T, object>>> includeProperties = null, int? page = null, int? pageSize = null);
        /// <summary>
        ///     Get entities according to filter
        /// </summary>
        /// <param name="isolationLevel">Isolation level</param>
        /// <param name="filter">Filter for getting entities</param>
        /// <param name="orderBy">Order criteria</param>
        /// <param name="includeProperties">Included properties</param>
        /// <param name="page">Page nmber</param>
        /// <param name="pageSize">Item number of the page</param>
        /// <param name="include">Expression for including</param>
        /// <returns>Entities as IEnumerable</returns>
        IEnumerable<T> Get(IsolationLevel isolationLevel, Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            List<Expression<Func<T, object>>> includeProperties = null, int? page = null, int? pageSize = null);
        /// <summary>
        ///     Save changes
        /// </summary>
        void SaveChanges();
        /// <summary>
        ///     Save changes
        /// </summary>
        /// <param name="isolationLevel">Isolation level</param>
        void SaveChanges(IsolationLevel isolationLevel);
        /// <summary>
        ///     Total records
        /// </summary>
        int TotalRecords();
        /// <summary>
        ///     Total records according to createria
        /// </summary>
        int TotalRecords(Expression<Func<T, bool>> predicate);
    }
}