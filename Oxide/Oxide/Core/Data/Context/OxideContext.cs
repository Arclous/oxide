﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System.Data.Entity;
using Oxide.Core.Base.OxideBaseContext;
using Oxide.Core.Base.OxideDataBaseModel;
using Oxide.Core.Base.OxideModelManager;
using Oxide.Core.Managers.AssemblyManager;
using Oxide.Core.Syntax;

namespace Oxide.Core.Data.Context
{
    public class OxideContext : DbContext, IOxideBaseContext
    {
        public OxideContext() : base("OxideContext")
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<OxideContext>());
            Configuration.LazyLoadingEnabled = false;
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            var instances = AssemblyManager.GetAllFor<IOxideDataBaseModel>();
            for (var index = 0; index < instances.Length; index++)
            {
                var instance = instances[index];
                var type = instance.GetType();
                var method = modelBuilder.GetType().GetMethod(Constants.TableCreaterMethodName);
                method = method.MakeGenericMethod(instance.GetType());
                method.Invoke(modelBuilder, null);
                var releatedModelBuilder = modelBuilder.Types().Where(x => x.Name == type.Name);
                releatedModelBuilder.Configure(
                    c =>
                        c.ToTable(
                            type.Module.Name.Replace(Constants.Dll, Constants.Empty)
                                .Replace(Constants.Dot, Constants.TableNameSpereator) + Constants.TableNameSpereator +
                            c.ClrType.Name));
            }
            var list = AssemblyManager.GetAllFor<IOxideModelManager>();
            for (var index = 0; index < list.Length; index++)
            {
                var instance = list[index];
                instance.OnModelCreating(modelBuilder);
            }
        }
    }
}