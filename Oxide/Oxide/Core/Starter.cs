﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Castle.Windsor;
using Oxide.Core.Base.OxideEvents;
using Oxide.Core.Engines;
using Oxide.Core.Installers;
using Oxide.Core.Managers.AssemblyManager;

namespace Oxide.Core
{
    public class Starter
    {
        private static IWindsorContainer _container;

        public void Application_Start(object sender, EventArgs e)
        {
            AssemblyManager.LoadAssemblies();
            _container = new WindsorContainer();
            _container.Install(new OxideServicesInstaller());
            _container.Install(new OxideControllersInstaller());
            _container.Install(new OxideContextsInstaller());
            _container.Install(new OxideRepositoryInstaller());
            var controllerFactory = new OxideControllerFactory(_container.Kernel);
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);
            Oxide.Container = _container;
            StartOxide.Start(BundleTable.Bundles, GlobalFilters.Filters, RouteTable.Routes);
            OxideEvents.Dispatcher = new OxideEventContainer();
        }

        public void Session_Start(object sender, EventArgs e)
        {
        }

        public void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        public void Application_AuthenticateRequest(object sender, EventArgs e)
        {
        }

        public void Application_Error(object sender, EventArgs e, HttpServerUtility server)
        {
        }

        public void Session_End(object sender, EventArgs e)
        {
        }

        public void Application_End(object sender, EventArgs e)
        {
            _container?.Dispose();
        }
    }
}