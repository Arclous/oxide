﻿using Oxide.Core.Managers.ProviderManager;

namespace Oxide.Core.Services.Interface.Registers.OxideStarter
{
    /// <summary>
    ///     Interface for registering starters
    /// </summary>
    public interface IOxideStarterRegisterer
    {
        /// <summary>
        ///     Starts with the stating of the web site
        /// </summary>
        /// <param name="providerManager">Provider manager</param>
        void Start(IProviderManager providerManager);
    }
}