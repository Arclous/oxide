﻿using System.Web.Mvc;

namespace Oxide.Core.Services.Interface.Registers.FilterRegister
{
    /// <summary>
    ///     Interface for registering filter
    /// </summary>
    public interface IFilterRegisterer
    {
        /// <summary>
        ///     Registers filters
        /// </summary>
        /// <param name="filters">Global filter collections</param>
        void RegisterFilter(GlobalFilterCollection filters);
    }
}