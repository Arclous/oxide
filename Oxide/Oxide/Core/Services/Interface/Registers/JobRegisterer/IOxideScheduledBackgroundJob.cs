﻿using Quartz;

namespace Oxide.Core.Services.Interface.Registers.JobRegisterer
{
    /// <summary>
    ///     Interface for registering scheduled background jobs
    /// </summary>
    public interface IOxideScheduledBackgroundJob : IJob
    {
    }
}