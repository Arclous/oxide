﻿namespace Oxide.Core.Services.Interface.Registers.LayoutRegister
{
    /// <summary>
    ///     Interface for registering layouts
    /// </summary>
    public interface ILayoutRegisterer
    {
        /// <summary>
        ///     Registers layouts
        /// </summary>
        void RegisterLayouts();
    }
}