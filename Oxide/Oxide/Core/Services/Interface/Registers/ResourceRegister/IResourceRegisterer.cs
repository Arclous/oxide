﻿using System.Web.Optimization;

namespace Oxide.Core.Services.Interface.Registers.ResourceRegister
{
    /// <summary>
    ///     Interface for registering resource
    /// </summary>
    public interface IResourceRegisterer
    {
        /// <summary>
        ///     Registers resources
        /// </summary>
        /// <param name="boundleCollection">Bundle collection</param>
        void RegisterResource(BundleCollection boundleCollection);
    }
}