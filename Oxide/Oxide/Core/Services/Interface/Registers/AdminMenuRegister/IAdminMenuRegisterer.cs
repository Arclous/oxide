﻿using System.Web.Routing;
using Oxide.Core.Services.AdminMenuService;

namespace Oxide.Core.Services.Interface.Registers.AdminMenuRegister
{
    /// <summary>
    ///     Interface for registering admin menu
    /// </summary>
    public interface IAdminMenuRegisterer
    {
        /// <summary>
        ///     Builds menus
        /// </summary>
        /// <param name="requestContext">Request context</param>
        /// <param name="adminMenuService">Admin menu service</param>
        void BuildMenus(RequestContext requestContext, IAdminMenuService adminMenuService);
    }
}