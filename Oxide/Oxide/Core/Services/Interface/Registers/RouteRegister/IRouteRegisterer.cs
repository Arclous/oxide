﻿using System.Web.Routing;

namespace Oxide.Core.Services.Interface.Registers.RouteRegister
{
    /// <summary>
    ///     Interface for registering routes
    /// </summary>
    public interface IRouteRegisterer
    {
        /// <summary>
        ///     Builds menus
        /// </summary>
        /// <param name="routes">route collection</param>
        void RegisterRoute(RouteCollection routes);
    }
}