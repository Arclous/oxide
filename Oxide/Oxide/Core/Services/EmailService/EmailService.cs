﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using Oxide.Core.Helpers;
using Oxide.Core.Models.Email;
using Oxide.Core.Models.Log;
using Oxide.Core.Services.LogService;
using Oxide.Core.Services.SiteSettings;

namespace Oxide.Core.Services.EmailService
{
    public class EmailService : IEmailService
    {
        private readonly IEmailSettingsService _emailSettingsService;
        private readonly ILogService _logService;

        public EmailService()
        {
        }

        public EmailService(IEmailSettingsService emailSettingsService, ILogService logService)
        {
            _emailSettingsService = emailSettingsService;
            _logService = logService;
        }

        public bool SendEmail(Email model)
        {
            try
            {
                //Get Email Settings
                var emailSettings = _emailSettingsService.GetEmailSettings();
                if (emailSettings == null)
                {
                    var log = new Log
                    {
                        DateTimeInfo = DateTime.Now,
                        LogType = Syntax.Syntax.LogTypes.Error,
                        Description = "Email Settings Missing",
                        ModuleName = OxideUtils.GetCallerModuleName(new StackFrame(1))
                    };
                    _logService.CreateLog(log);
                    return false;
                }
                var mail = new MailMessage();
                mail.To.Add(model.To);
                mail.From = new MailAddress(model.From);
                mail.Subject = model.Subject;
                var body = model.Body;
                mail.Body = body;
                mail.IsBodyHtml = true;
                var smtp = new SmtpClient
                {
                    Host = emailSettings.HostName,
                    Port = emailSettings.PortNumber,
                    UseDefaultCredentials = false,
                    Credentials =
                        new NetworkCredential(emailSettings.UserName, OxideUtils.Decrypt(emailSettings.Password)),
                    EnableSsl = emailSettings.EnableSsl,
                    DeliveryMethod = SmtpDeliveryMethod.Network
                };
                smtp.Send(mail);
                return true;
            }
            catch (Exception exception)
            {
                var log = new Log
                {
                    DateTimeInfo = DateTime.Now,
                    LogType = Syntax.Syntax.LogTypes.Error,
                    Description = exception.Message,
                    ModuleName = OxideUtils.GetCallerModuleName(new StackFrame(1))
                };
                if (exception.InnerException != null) log.InnerException = exception.InnerException.Message;
                _logService.CreateLog(log);
                return false;
            }
        }

        public string FillParameters(string template, Dictionary<string, string> keyWords)
        {
            return keyWords.Aggregate(template, (current, keyword) => current.Replace(keyword.Key, keyword.Value));
        }
    }
}