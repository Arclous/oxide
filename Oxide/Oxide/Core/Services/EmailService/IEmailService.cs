﻿using System.Collections.Generic;
using Oxide.Core.Base.OxideDependency;
using Oxide.Core.Models.Email;

namespace Oxide.Core.Services.EmailService
{
    /// <summary>
    ///     Service which is reponsile for sending email
    /// </summary>
    public interface IEmailService : IOxideDependency
    {
        /// <summary>
        ///     Sends email according to model
        /// </summary>
        /// <param name="model">Email model</param>
        bool SendEmail(Email model);
        /// <summary>
        ///     Fill email template according to keyWord
        /// </summary>
        /// <param name="template">Email content</param>
        /// <param name="keyWords">Keywords to fill email template</param>
        string FillParameters(string template, Dictionary<string, string> keyWords);
    }
}