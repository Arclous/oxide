﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System.Linq;
using Oxide.Core.Data.Repository;
using Oxide.Core.Models.Settings;

namespace Oxide.Core.Services.SiteSettings
{
    public class EmailSettingsService : IEmailSettingsService
    {
        private readonly IOxideRepository<EmailSettings> _emailSettingsRepository;

        public EmailSettingsService(IOxideRepository<EmailSettings> emailSettingsRepository)
        {
            _emailSettingsRepository = emailSettingsRepository;
        }

        public void SaveEmailSettings(EmailSettings model)
        {
            _emailSettingsRepository.DeleteAll();
            _emailSettingsRepository.Insert(model);
        }

        public EmailSettings GetEmailSettings()
        {
            return _emailSettingsRepository.GetAll().FirstOrDefault();
        }
    }
}