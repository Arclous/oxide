﻿using Oxide.Core.Base.OxideDependency;

namespace Oxide.Core.Services.SiteSettings
{
    /// <summary>
    ///     Service for global site settings
    /// </summary>
    public interface ISiteSettingsService : IOxideDependency
    {
        /// <summary>
        ///     Saves settings according to settings model
        /// </summary>
        /// <param name="model">Settings model</param>
        void SaveSettings(Areas.Admin.Models.SiteSettings model);
        /// <summary>
        ///     Gets all settings
        /// </summary>
        Areas.Admin.Models.SiteSettings GetSettings();
        /// <summary>
        ///     Gets all settings
        /// </summary>
        Areas.Admin.Models.SiteSettings GetSettingsWithCulture();
    }
}