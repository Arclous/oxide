﻿using Oxide.Core.Base.OxideDependency;
using Oxide.Core.Models.Settings;

namespace Oxide.Core.Services.SiteSettings
{
    public interface IEmailSettingsService : IOxideDependency
    {
        /// <summary>
        ///     Saves settings according to email settings model
        /// </summary>
        /// <param name="model">Settings model</param>
        void SaveEmailSettings(EmailSettings model);
        /// <summary>
        ///     Gets all email settings
        /// </summary>
        EmailSettings GetEmailSettings();
    }
}