﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System.Linq;
using Oxide.Core.Data.Repository;

namespace Oxide.Core.Services.SiteSettings
{
    public class SiteSettingsService : ISiteSettingsService
    {
        private readonly IOxideRepository<Areas.Admin.Models.SiteSettings> _siteSettingsRepository;

        public SiteSettingsService(IOxideRepository<Areas.Admin.Models.SiteSettings> siteSettingsRepository)
        {
            _siteSettingsRepository = siteSettingsRepository;
        }

        public void SaveSettings(Areas.Admin.Models.SiteSettings model)
        {
            _siteSettingsRepository.DeleteAll();
            _siteSettingsRepository.Insert(model);
        }

        public Areas.Admin.Models.SiteSettings GetSettings()
        {
            return _siteSettingsRepository.GetAll(x => x.DefaultSiteCulture, x => x.DefaultTimeZone).FirstOrDefault();
        }

        public Areas.Admin.Models.SiteSettings GetSettingsWithCulture()
        {
            return _siteSettingsRepository.GetAll(x => x.DefaultSiteCulture).FirstOrDefault();
        }
    }
}