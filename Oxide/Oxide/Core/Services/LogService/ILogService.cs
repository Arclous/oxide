﻿using Oxide.Core.Base.OxideDependency;
using Oxide.Core.Models.Log;
using Oxide.Core.ViewModels.FilterViewModels;
using Oxide.Core.ViewModels.LogViewModels;

namespace Oxide.Core.Services.LogService
{
    /// <summary>
    ///     Service which is reponsile for loging
    /// </summary>
    public interface ILogService : IOxideDependency
    {
        /// <summary>
        ///     Creates log model according to model
        /// </summary>
        /// <param name="model">Log model</param>
        int CreateLog(Log model);
        /// <summary>
        ///     Deletes log model according to model
        /// </summary>
        /// <param name="model">Log model</param>
        void DeleteLog(Log model);
        /// <summary>
        ///     Deletes log model according to id
        /// </summary>
        /// <param name="id">Id of the log</param>
        void DeleteLog(int id);
        /// <summary>
        ///     Gets log model according to id
        /// </summary>
        /// <param name="id">Id of the log</param>
        Log GetLog(int id);
        /// <summary>
        ///     Deletes all logs
        /// </summary>
        void ClearLogs();
        /// <summary>
        ///     Deletes all logs
        /// </summary>
        /// <param name="days">Day number for the last log</param>
        void ClearLogs(int days);
        /// <summary>
        ///     Gets logs as filter view model
        /// </summary>
        /// <param name="pageSize">Size of the page for each page</param>
        /// <param name="page">Requested page index</param>
        /// <param name="filter">Filter criterias to filtering logs</param>
        FilterModel<LogViewModel> GetLogsWithFilter(int pageSize, int page, string filter);
    }
}