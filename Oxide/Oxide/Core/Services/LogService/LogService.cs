﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Oxide.Core.Data.Repository;
using Oxide.Core.Models.Log;
using Oxide.Core.ViewModels.FilterViewModels;
using Oxide.Core.ViewModels.LogViewModels;

namespace Oxide.Core.Services.LogService
{
    public class LogService : ILogService
    {
        private readonly IOxideRepository<Log> _logRepository;

        public LogService()
        {
        }

        public LogService(IOxideRepository<Log> logRepository)
        {
            _logRepository = logRepository;
        }

        public int CreateLog(Log model)
        {
            _logRepository.Insert(model);
            return model.Id;
        }

        public void DeleteLog(Log model)
        {
            _logRepository.Delete(model);
        }

        public void DeleteLog(int id)
        {
            _logRepository.Delete(id);
        }

        public Log GetLog(int id)
        {
            return _logRepository.GetById(id);
        }

        public void ClearLogs()
        {
            _logRepository.DeleteAll();
        }

        public void ClearLogs(int days)
        {
            _logRepository.DeleteAll(x => DbFunctions.DiffDays(x.DateTimeInfo, DateTime.Today) >= days);
        }

        public FilterModel<LogViewModel> GetLogsWithFilter(int pageSize, int page, string filter)
        {
            var layoutFilterViewModel = new FilterModel<LogViewModel>();
            Expression<Func<Log, bool>> criteriaFilter = x => x.Description.ToLower().Contains(filter.ToLower());
            if (filter != string.Empty)
                layoutFilterViewModel.Records =
                    GetLogViewModelList(_logRepository.Get(criteriaFilter, o => o.OrderBy(x => x.Id), null, page,
                        pageSize));
            else
                layoutFilterViewModel.Records =
                    GetLogViewModelList(_logRepository.Get(null, o => o.OrderBy(x => x.Id), null, page, pageSize));
            layoutFilterViewModel.CurrentPage = page;
            layoutFilterViewModel.DisplayingRecords = layoutFilterViewModel.Records.Count();
            layoutFilterViewModel.TotalRecords = _logRepository.TotalRecords();
            if (filter != string.Empty)
                layoutFilterViewModel.TotalPages = (_logRepository.Get(criteriaFilter).Count() + pageSize - 1)/pageSize;
            else
                layoutFilterViewModel.TotalPages = (_logRepository.TotalRecords() + pageSize - 1)/pageSize;
            layoutFilterViewModel.NextEnable = layoutFilterViewModel.CurrentPage < layoutFilterViewModel.TotalPages - 1;
            layoutFilterViewModel.PreviousEnable = layoutFilterViewModel.CurrentPage > 0;
            return layoutFilterViewModel;
        }

        public ICollection<LogViewModel> GetLogViewModelList(IEnumerable<Log> logsList)
        {
            return
                logsList.Select(
                    log =>
                        new LogViewModel
                        {
                            Id = log.Id,
                            Title = log.Title,
                            Name = log.Name,
                            DateTimeInfo = log.DateTimeInfo,
                            Description = log.Description,
                            InnerException = log.InnerException,
                            ModuleName = log.ModuleName
                        }).ToList();
        }
    }
}