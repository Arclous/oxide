﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using Oxide.Areas.Admin.ViewModels;
using Oxide.Core.Data.Repository;
using Oxide.Core.Models.ScheduledJob;
using Oxide.Core.ViewModels.FilterViewModels;

namespace Oxide.Core.Services.ScheduledJobService
{
    public class ScheduledJobService : IScheduledJobService
    {
        private readonly IOxideRepository<ScheduledJob> _scheduledJobRepository;

        public ScheduledJobService()
        {
        }

        public ScheduledJobService(IOxideRepository<ScheduledJob> scheduledJobRepository)
        {
            _scheduledJobRepository = scheduledJobRepository;
        }

        public void CreateJob(ScheduledJob model)
        {
            _scheduledJobRepository.Insert(model);
        }

        public void UpdateJob(ScheduledJob model)
        {
            _scheduledJobRepository.Update(model);
        }

        public void DeleteJob(ScheduledJob model)
        {
            _scheduledJobRepository.Delete(model);
        }

        public void DeleteJob(int id)
        {
            _scheduledJobRepository.Delete(id);
        }

        public void DeleteAllJobs()
        {
            _scheduledJobRepository.DeleteAll();
        }

        public ScheduledJob GetJob(int id)
        {
            return _scheduledJobRepository.GetById(id);
        }

        public ScheduledJob GetJob(string key)
        {
            return _scheduledJobRepository.SearchFor(x => x.Name == key).FirstOrDefault();
        }

        public List<ScheduledJob> GetAllJobs()
        {
            return _scheduledJobRepository.GetAll().ToList();
        }

        public List<ScheduledJob> GetAllJobsAccordingToStatus(bool status)
        {
            return _scheduledJobRepository.SearchFor(x => x.Active == status).ToList();
        }

        public FilterModel<ScheduledJobViewModel> GetScheduledJobListWithFilter(int pageSize, int page, string filter)
        {
            var layoutFilterViewModel = new FilterModel<ScheduledJobViewModel>();
            Expression<Func<ScheduledJob, bool>> criteriaFilter = x => x.Name.ToLower().Contains(filter.ToLower());
            if (filter != string.Empty)
                layoutFilterViewModel.Records =
                    GetScheduledJobViewModelList(_scheduledJobRepository.Get(criteriaFilter, o => o.OrderBy(x => x.Id),
                        null, page, pageSize));
            else
                layoutFilterViewModel.Records =
                    GetScheduledJobViewModelList(_scheduledJobRepository.Get(null, o => o.OrderBy(x => x.Id), null, page,
                        pageSize));
            layoutFilterViewModel.CurrentPage = page;
            layoutFilterViewModel.DisplayingRecords = layoutFilterViewModel.Records.Count();
            layoutFilterViewModel.TotalRecords = _scheduledJobRepository.TotalRecords();
            if (filter != string.Empty)
                layoutFilterViewModel.TotalPages = (_scheduledJobRepository.Get(criteriaFilter).Count() + pageSize - 1)/
                                                   pageSize;
            else
                layoutFilterViewModel.TotalPages = (layoutFilterViewModel.TotalRecords + pageSize - 1)/pageSize;
            layoutFilterViewModel.NextEnable = layoutFilterViewModel.CurrentPage < layoutFilterViewModel.TotalPages - 1;
            layoutFilterViewModel.PreviousEnable = layoutFilterViewModel.CurrentPage > 0;
            return layoutFilterViewModel;
        }

        public ScheduledJob IsJobExist(string key)
        {
            return _scheduledJobRepository.GetAll().FirstOrDefault(x => x.Name == key);
        }

        public ICollection<ScheduledJobViewModel> GetScheduledJobViewModelList(
            IEnumerable<ScheduledJob> scheduledJobList)
        {
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            return
                scheduledJobList.Select(
                    scheduledJob =>
                        new ScheduledJobViewModel
                        {
                            Id = scheduledJob.Id,
                            Title = scheduledJob.Title,
                            Name = scheduledJob.Name,
                            Active = scheduledJob.Active,
                            Key = scheduledJob.Key,
                            Group = scheduledJob.Group,
                            Owner = scheduledJob.Owner,
                            LastStart = scheduledJob.LastStart,
                            LastFinish = scheduledJob.LastFinish,
                            NextStart = scheduledJob.NextStart,
                            ManageUrl =
                                urlHelper.Action(MVC.Admin.ScheduledJob.ActionNames.ReSchedule,
                                    MVC.Admin.ScheduledJob.Name, new {id = scheduledJob.Id}),
                            CanManage = scheduledJob.CanManage,
                            CanRunOn = scheduledJob.CanRunOn
                        }).ToList();
        }
    }
}