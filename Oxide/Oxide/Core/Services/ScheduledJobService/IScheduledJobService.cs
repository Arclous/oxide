﻿using System.Collections.Generic;
using Oxide.Areas.Admin.ViewModels;
using Oxide.Core.Base.OxideDependency;
using Oxide.Core.Models.ScheduledJob;
using Oxide.Core.ViewModels.FilterViewModels;

namespace Oxide.Core.Services.ScheduledJobService
{
    /// <summary>
    ///     Responsible service for scheduled jobs
    /// </summary>
    public interface IScheduledJobService : IOxideDependency
    {
        /// <summary>
        ///     Creates scheduled job
        /// </summary>
        /// <param name="model">Scheduled job to create</param>
        void CreateJob(ScheduledJob model);
        /// <summary>
        ///     Updates scheduled job
        /// </summary>
        /// <param name="model">Scheduled job to update</param>
        void UpdateJob(ScheduledJob model);
        /// <summary>
        ///     Deletes scheduled job
        /// </summary>
        /// <param name="model">Scheduled job to delete</param>
        void DeleteJob(ScheduledJob model);
        /// <summary>
        ///     Deletes scheduled job according to id
        /// </summary>
        /// <param name="id">Id of the scheduled job</param>
        void DeleteJob(int id);
        /// <summary>
        ///     Deletes scheduled job according to id
        /// </summary>
        void DeleteAllJobs();
        /// <summary>
        ///     Gets scheduled job according to id
        /// </summary>
        /// <param name="id">Id of the scheduled job</param>
        ScheduledJob GetJob(int id);
        /// <summary>
        ///     Gets scheduled job according to key
        /// </summary>
        /// <param name="key">Key of the scheduled job</param>
        ScheduledJob GetJob(string key);
        /// <summary>
        ///     Gets all scheduled jobs
        /// </summary>
        List<ScheduledJob> GetAllJobs();
        /// <summary>
        ///     Gets all scheduled jobs
        /// </summary>
        /// <param name="status">Status of the scheduled job</param>
        List<ScheduledJob> GetAllJobsAccordingToStatus(bool status);
        /// <summary>
        ///     Gets scheduled jobs as filter model
        /// </summary>
        /// <param name="pageSize">Size of the page for each page</param>
        /// <param name="page">Requested page index</param>
        /// <param name="filter">Filter criterias to filtering layouts</param>
        FilterModel<ScheduledJobViewModel> GetScheduledJobListWithFilter(int pageSize, int page, string filter);
        /// <summary>
        ///     Checks if scheduled job exist according to key
        /// </summary>
        /// <param name="key">Key of the scheduled job</param>
        ScheduledJob IsJobExist(string key);
    }
}