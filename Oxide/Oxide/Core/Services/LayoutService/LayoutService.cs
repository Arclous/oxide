﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Oxide.Core.Elements;
using Oxide.Core.Helpers;
using Oxide.Core.ViewModels.FilterViewModels;
using Oxide.Core.ViewModels.LayoutViewModels;

namespace Oxide.Core.Services.LayoutService
{
    public class LayoutService : ILayoutService
    {
        public FilterModel<LayoutViewModel> LayoutList(int pageSize, int page, string filter)
        {
            var layoutFilterViewModel = new FilterModel<LayoutViewModel>();
            var layoutList = GetLayoutViewModelList(OxideLayoutHelper.GetMainLayoutList().OrderBy(x => x.Id));
            if (filter != string.Empty)
                layoutFilterViewModel.Records =
                    layoutList.OrderBy(x => x.Name)
                              .Where((x => x.Name.ToLower().Contains(filter.ToLower())))
                              .Skip(pageSize*page)
                              .Take(pageSize)
                              .ToList();
            else
                layoutFilterViewModel.Records =
                    layoutList.OrderBy(x => x.Name).Skip(pageSize*page).Take(pageSize).ToList();
            layoutFilterViewModel.CurrentPage = page;
            layoutFilterViewModel.DisplayingRecords = layoutList.Count();
            layoutFilterViewModel.TotalRecords = layoutList.Count();
            if (filter != string.Empty)
                layoutFilterViewModel.TotalPages = (layoutList.Count(x => x.Name.Contains(filter)) + pageSize - 1)/
                                                   pageSize;
            else
                layoutFilterViewModel.TotalPages = (layoutFilterViewModel.TotalRecords + pageSize - 1)/pageSize;
            layoutFilterViewModel.NextEnable = layoutFilterViewModel.CurrentPage < layoutFilterViewModel.TotalPages - 1;
            layoutFilterViewModel.PreviousEnable = layoutFilterViewModel.CurrentPage > 0;
            return layoutFilterViewModel;
        }

        public ICollection<LayoutViewModel> GetLayoutViewModelList(IEnumerable<IOxideLayout> layoutList)
        {
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            return layoutList.Select(layout =>
            {
                var firstOrDefault = Oxide.LoadedLayouts.FirstOrDefault(x => x.Id == layout.Id);
                return firstOrDefault != null
                    ? new LayoutViewModel
                    {
                        Id = layout.Id,
                        Layouts = layout.GetLayoutNames(),
                        Name = layout.Name,
                        ManageWidgetsUrl =
                            urlHelper.Action(MVC.Admin.Layouts.ActionNames.ManageWidgets, MVC.Admin.Layouts.Name,
                                new {mainLayoutId = layout.Id, layoutName = layout.Name}),
                        PreviewImage = firstOrDefault.PreviewImageUrl
                    }
                    : null;
            }).ToList();
        }
    }
}