﻿using System.Collections.Generic;
using Oxide.Core.Base.OxideDependency;
using Oxide.Core.Elements;
using Oxide.Core.ViewModels.FilterViewModels;
using Oxide.Core.ViewModels.LayoutViewModels;

namespace Oxide.Core.Services.LayoutService
{
    /// <summary>
    ///     Services for managing layouts
    /// </summary>
    public interface ILayoutService : IOxideDependency
    {
        /// <summary>
        ///     Gets layout  view model list as convertted from enumrable layout interface to cloction
        /// </summary>
        /// <param name="layoutList">Layout list which will be convertted</param>
        ICollection<LayoutViewModel> GetLayoutViewModelList(IEnumerable<IOxideLayout> layoutList);
        /// <summary>
        ///     Gets layouts as filter model
        /// </summary>
        /// <param name="pageSize">Size of the page for each page</param>
        /// <param name="page">Requested page index</param>
        /// <param name="filter">Filter criterias to filtering layouts</param>
        FilterModel<LayoutViewModel> LayoutList(int pageSize, int page, string filter);
    }
}