﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Oxide.Core.Data.Repository;
using Oxide.Core.Elements;
using Oxide.Core.Helpers;
using Oxide.Core.Models.Dashboard;
using Oxide.Core.Models.DashboardWidget;
using Oxide.Core.Syntax;
using Oxide.Core.ViewModels.DashboardWidgetViewModels;
using Oxide.Core.ViewModels.FilterViewModels;

namespace Oxide.Core.Services.DashboardService
{
    public class DashboardService : IDashboardService
    {
        private readonly IOxideRepository<Dashboard> _dashboardRepository;
        private readonly IOxideRepository<DashboardWidget> _dashboardWidgetRepository;

        public DashboardService(IOxideRepository<Dashboard> dashboardRepository,
            IOxideRepository<DashboardWidget> dashboardWidgetRepository)
        {
            _dashboardRepository = dashboardRepository;
            _dashboardWidgetRepository = dashboardWidgetRepository;
        }

        public int AddAdminWidget(DashboardWidget dashboardWidget)
        {
            _dashboardWidgetRepository.Insert(dashboardWidget);
            return dashboardWidget.Id;
        }

        public void DeleteAdminWidget(int adminWidgetId)
        {
            _dashboardWidgetRepository.Delete(adminWidgetId);
        }

        public int AttachAdminWidget(int userId, string adminWidgetKey)
        {
            var newDashboardWidget = new Dashboard();
            var dashBoardWidget =
                OxideWidgetHelper.GetDashboardWidgetList().FirstOrDefault(x => x.WidgetKey == adminWidgetKey);
            if (dashBoardWidget == null) return newDashboardWidget.Id;
            newDashboardWidget.Order = 99999;
            newDashboardWidget.UserId = userId;
            newDashboardWidget.WidgetKey = adminWidgetKey;
            newDashboardWidget.Name = dashBoardWidget.Name;
            newDashboardWidget.Title = dashBoardWidget.Title;
            _dashboardRepository.Insert(newDashboardWidget);
            return newDashboardWidget.Id;
        }

        public void DetachAdminWidget(int id)
        {
            _dashboardRepository.Delete(id);
        }

        public void UpdateOrderValue(int userId, int id, string orderList)
        {
            var ids = new List<int>(Array.ConvertAll(orderList.Split(Constants.Splitter), int.Parse));
            var dashBoardWidgets = _dashboardRepository.Get(x => x.UserId == userId);
            var boardWidgets = dashBoardWidgets as Dashboard[] ?? dashBoardWidgets.ToArray();
            for (var dIndex = 0; dIndex <= boardWidgets.Count() - 1; dIndex++)
            {
                boardWidgets[dIndex].Order = ids.IndexOf(boardWidgets[dIndex].Id);
                _dashboardRepository.LazyUpdate(boardWidgets[dIndex]);
            }
            _dashboardRepository.SaveChanges();
        }

        public ICollection<DashboardWidgetViewModel> GetAdminWidgetViewModelList(
            IEnumerable<IOxideDashboardWidget> widgetList)
        {
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            return
                widgetList.Select(
                    widget =>
                        new DashboardWidgetViewModel
                        {
                            Description = widget.Description,
                            Name = widget.Name,
                            Title = widget.Title,
                            Category = widget.Category,
                            PreviewImageUrl = widget.PreviewImageUrl,
                            Active = IsWidgetActive(widget.WidgetKey),
                            InfoUrl = "",
                            WidgetKey = widget.WidgetKey,
                            ChangeStatusUrl =
                                urlHelper.Action(MVC.Admin.Dashboard.ActionNames.ChangeStatus, MVC.Admin.Dashboard.Name,
                                    new {area = Constants.DefaultArea})
                        }).ToList();
        }

        public FilterModel<DashboardWidgetViewModel> DashboardWidgetList(string filter)
        {
            ICollection<DashboardWidgetViewModel> widgetList;
            var widgetFilterViewModel = new FilterModel<DashboardWidgetViewModel>();
            if (filter != null)
            {
                if (filter != string.Empty)
                {
                    widgetList =
                        GetAdminWidgetViewModelList(
                            OxideWidgetHelper.GetDashboardWidgetList()
                                             .Where(
                                                 x =>
                                                     x.Name.ToLower().Contains(filter.ToLower()) ||
                                                     x.Title.ToLower().Contains(filter.ToLower()) ||
                                                     x.Category.ToLower().Contains(filter.ToLower()) ||
                                                     x.Description.ToLower().Contains(filter.ToLower()))
                                             .OrderBy(x => x.Name));
                    widgetFilterViewModel.Records = widgetList;
                }
                else
                {
                    widgetList =
                        GetAdminWidgetViewModelList(OxideWidgetHelper.GetDashboardWidgetList().OrderBy(x => x.Name));
                    widgetFilterViewModel.Records = widgetList;
                }
            }
            else
            {
                widgetList = GetAdminWidgetViewModelList(OxideWidgetHelper.GetDashboardWidgetList().OrderBy(x => x.Name));
                widgetFilterViewModel.Records = widgetList;
            }
            return widgetFilterViewModel;
        }

        public bool ChangeStatus(string widgetKey, bool status)
        {
            if (!status)
            {
                var widget = new DashboardWidget {WidgetKey = widgetKey};
                var widgetInfo = Oxide.LoadedDashboardWidgets.FirstOrDefault(x => x.WidgetKey == widgetKey);
                if (widgetInfo != null)
                {
                    widget.Name = widgetInfo.Name;
                    widget.Title = widgetInfo.Title;
                }
                _dashboardWidgetRepository.Insert(widget);
                return true;
            }
            else
            {
                var widget = _dashboardWidgetRepository.SearchFor(x => x.WidgetKey == widgetKey).FirstOrDefault();
                _dashboardWidgetRepository.Delete(widget);
                return false;
            }
        }

        public ICollection<DashboardWidgetPackageViewModel> GetDashboardWidgetsForUser(int userId)
        {
            ICollection<DashboardWidgetPackageViewModel> dashboardWidgetList =
                new List<DashboardWidgetPackageViewModel>();
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            var dashboardWidgets = _dashboardRepository.Get(x => x.UserId == userId);
            foreach (var widget in dashboardWidgets)
            {
                var dashBoardWidgetBaseInfo =
                    Oxide.LoadedDashboardWidgets.FirstOrDefault(x => x.WidgetKey == widget.WidgetKey);
                if (dashBoardWidgetBaseInfo == null) continue;
                var widgetViewModel = new DashboardWidgetPackageViewModel
                {
                    Id = widget.Id,
                    UserId = widget.UserId,
                    Order = widget.Order,
                    WidgetKey = widget.WidgetKey,
                    Name = widget.Name,
                    Title = widget.Title,
                    Description = dashBoardWidgetBaseInfo.Description,
                    PreviewImageUrl = dashBoardWidgetBaseInfo.PreviewImageUrl,
                    Category = dashBoardWidgetBaseInfo.Category,
                    Manageable = IsDashboardWidgetManageable(widget.WidgetKey),
                    ManageDashboardWidgetUrl =
                        urlHelper.Action(MVC.Admin.Dashboard.ActionNames.ManageDashboardWidgets,
                            MVC.Admin.Dashboard.Name,
                            new {area = Constants.DefaultArea, widgetId = widget.Id, widgetKey = widget.WidgetKey}),
                    DashboardWidgetRunUrl =
                        urlHelper.Action(MVC.Admin.Dashboard.ActionNames.DashboardWidgetRun, MVC.Admin.Dashboard.Name,
                            new {area = Constants.DefaultArea, widgetKey = widget.WidgetKey})
                };
                dashboardWidgetList.Add(widgetViewModel);
            }
            return dashboardWidgetList;
        }

        public ICollection<DashboardWidgetPackageViewModel> GetDashboardWidgetsForUser(string filter)
        {
            ICollection<DashboardWidgetPackageViewModel> dashboardWidgetList =
                new List<DashboardWidgetPackageViewModel>();
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            IEnumerable<IOxideDashboardWidget> dashboardWidgets;
            if (filter != null)
            {
                dashboardWidgets =
                    OxideWidgetHelper.GetDashboardWidgetList()
                                     .Where(
                                         x =>
                                             x.Name.ToLower().Contains(filter.ToLower()) ||
                                             x.Title.ToLower().Contains(filter.ToLower()) ||
                                             x.Category.ToLower().Contains(filter.ToLower()) ||
                                             x.Description.ToLower().Contains(filter.ToLower()))
                                     .OrderBy(x => x.Name);
            }
            else
            {
                dashboardWidgets = OxideWidgetHelper.GetDashboardWidgetList();
            }
            foreach (var widget in dashboardWidgets)
            {
                if (IsWidgetActive(widget.WidgetKey))
                {
                    var widgetViewModel = new DashboardWidgetPackageViewModel
                    {
                        WidgetKey = widget.WidgetKey,
                        Name = widget.Name,
                        Title = widget.Title,
                        Description = widget.Description,
                        PreviewImageUrl = widget.PreviewImageUrl,
                        Category = widget.Category,
                        Manageable = IsDashboardWidgetManageable(widget.WidgetKey),
                        ManageDashboardWidgetUrl = "",
                        DashboardWidgetRunUrl =
                            urlHelper.Action(MVC.Admin.Dashboard.ActionNames.DashboardWidgetRun,
                                MVC.Admin.Dashboard.Name,
                                new {area = Constants.DefaultArea, widgetKey = widget.WidgetKey})
                    };
                    dashboardWidgetList.Add(widgetViewModel);
                }
            }
            return dashboardWidgetList;
        }

        protected bool IsWidgetActive(string widgetKey)
        {
            return _dashboardWidgetRepository.GetAll().Any(x => x.WidgetKey == widgetKey);
        }

        protected bool IsDashboardWidgetManageable(string widgetKey)
        {
            var oxideDashboardWidget = Oxide.LoadedDashboardWidgets.FirstOrDefault(x => x.WidgetKey == widgetKey);
            return oxideDashboardWidget != null && oxideDashboardWidget.Manageable;
        }
    }
}