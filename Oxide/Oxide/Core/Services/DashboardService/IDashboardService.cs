﻿using System.Collections.Generic;
using Oxide.Core.Base.OxideDependency;
using Oxide.Core.Elements;
using Oxide.Core.Models.DashboardWidget;
using Oxide.Core.ViewModels.DashboardWidgetViewModels;
using Oxide.Core.ViewModels.FilterViewModels;

namespace Oxide.Core.Services.DashboardService
{
    /// <summary>
    ///     Responsible service for dashboard
    /// </summary>
    public interface IDashboardService : IOxideDependency
    {
        /// <summary>
        ///     Adds widget
        /// </summary>
        /// <param name="dashboardWidget">Admin widget</param>
        int AddAdminWidget(DashboardWidget dashboardWidget);
        /// <summary>
        ///     Deletes widget
        /// </summary>
        /// <param name="adminWidgetId">Id of the admin widget</param>
        void DeleteAdminWidget(int adminWidgetId);
        /// <summary>
        ///     Adds widget to dashboard for the user
        /// </summary>
        /// <param name="userId">Id of the admin user</param>
        /// <param name="adminWidgetKey">Widget key of the admin widget</param>
        int AttachAdminWidget(int userId, string adminWidgetKey);
        /// <summary>
        ///     Deletes admin widget from dashboard for the user
        /// </summary>
        /// <param name="id">Id of the dashboard widget</param>
        void DetachAdminWidget(int id);
        /// <summary>
        ///     Deletes admin widget from dashboard for the user
        /// </summary>
        /// <param name="userId">User id</param>
        /// <param name="id">Id of the dashboard widget</param>
        /// <param name="orderList">Order values</param>
        void UpdateOrderValue(int userId, int id, string orderList);
        /// <summary>
        ///     Gets view model collection of the items
        /// </summary>
        /// <param name="widgetList">Items to be convertted to view model collection</param>
        /// <returns>Widget view model collection</returns>
        ICollection<DashboardWidgetViewModel> GetAdminWidgetViewModelList(IEnumerable<IOxideDashboardWidget> widgetList);
        /// <summary>
        ///     Gets admin widget list
        /// </summary>
        /// <param name="filter">Filter for getting widget list</param>
        /// <returns>Filter model for widget view model</returns>
        FilterModel<DashboardWidgetViewModel> DashboardWidgetList(string filter);
        /// <summary>
        ///     Changes status of the dashboard widget
        /// </summary>
        /// <param name="widgetKey">Key of the dashboard widget</param>
        /// <param name="status">Status of the dashboard widget</param>
        /// <returns>Boolean as new status</returns>
        bool ChangeStatus(string widgetKey, bool status);
        /// <summary>
        ///     Gets dashboard widgets for user
        /// </summary>
        /// <param name="userId">Id of the user</param>
        /// <returns>Boolean as new status</returns>
        ICollection<DashboardWidgetPackageViewModel> GetDashboardWidgetsForUser(int userId);
        /// <summary>
        ///     Gets dashboard widgets according to filter
        /// </summary>
        /// <param name="filter">Filter for getting widget list</param>
        ICollection<DashboardWidgetPackageViewModel> GetDashboardWidgetsForUser(string filter);
    }
}