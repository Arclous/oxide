﻿using System.Web.Mvc;

namespace Oxide.Core.Services.WidgetEngine
{
    /// <summary>
    ///     Engine for converting widgets to html
    /// </summary>
    public interface IWidgetEngine
    {
        /// <summary>
        ///     Converts widget to html
        /// </summary>
        /// <param name="viewToRender">View file which will be rendered</param>
        /// <param name="viewData">View data</param>
        /// <param name="controllerContext">Controller context</param>
        string ToHtml(string viewToRender, ViewDataDictionary viewData, ControllerContext controllerContext);
    }
}