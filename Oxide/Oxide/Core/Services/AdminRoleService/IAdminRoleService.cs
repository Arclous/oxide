﻿using System.Collections.Generic;
using Oxide.Core.Base.OxideDependency;
using Oxide.Core.Models.AdminRole;
using Oxide.Core.ViewModels.AdminRoleViewModels;

namespace Oxide.Core.Services.AdminRoleService
{
    /// <summary>
    ///     Service for managing admin roles
    /// </summary>
    public interface IAdminRoleService : IOxideDependency
    {
        /// <summary>
        ///     Creates admin user role according to model
        /// </summary>
        /// <param name="model">Admin role model</param>
        void CreateRole(AdminRoleViewModel model);
        /// <summary>
        ///     Updates admin user role according to model
        /// </summary>
        /// <param name="model">Admin role model</param>
        void UpdateRole(AdminRoleViewModel model);
        /// <summary>
        ///     Deletes admin user role according to model
        /// </summary>
        /// <param name="model">Admin role model</param>
        void DeleteRole(AdminRole model);
        /// <summary>
        ///     Deletes admin user role according to id
        /// </summary>
        /// <param name="roleId">Id of role</param>
        void DeleteRole(int roleId);
        /// <summary>
        ///     Checks if role is exist,according role name
        /// </summary>
        /// <param name="roleName">Name of role</param>
        bool IsRoleExist(string roleName);
        /// <summary>
        ///     Gets role according id
        /// </summary>
        /// <param name="roleId">Id of role</param>
        AdminRole GetRole(int roleId);
        /// <summary>
        ///     Gets roles
        /// </summary>
        IEnumerable<AdminRole> GetAllRoles();
        /// <summary>
        ///     Gets permissions
        /// </summary>
        IEnumerable<AdminRolePermission> GetPermissions(int roleId);
        /// <summary>
        ///     Gets user ids according to roles
        /// </summary>
        IEnumerable<int> GetAllUsers(int roleId);
        /// <summary>
        ///     Gets roles as filter view model
        /// </summary>
        /// <param name="pageSize">Size of the page for each page</param>
        /// <param name="page">Requested page index</param>
        /// <param name="filter">Filter criterias to filtering pages</param>
        AdminRoleFilerViewModel GetAdminRolesWithFilter(int pageSize, int page, string filter);
        /// <summary>
        ///     Gets all roles as organized
        /// </summary>
        /// <param name="userId">Id of the admin user which is currently viewing</param>
        IEnumerable<AdminUserRoleViewModel> GetAllAdminRolesOrganized(int? userId);
        /// <summary>
        ///     Sets permission according to role id
        /// </summary>
        /// <param name="roleId">Id of the admin role</param>
        /// <param name="permissionId">Id of the admin permission</param>
        /// <param name="status">Status of the admin role permission</param>
        void SetPermission(int roleId, int permissionId, bool status);
        /// <summary>
        ///     Sets role according to role id
        /// </summary>
        /// <param name="userId">Id of the admin user</param>
        /// <param name="roleId">Id of the admin role</param>
        /// <param name="status">Status of the admin role permission</param>
        void SetRole(int userId, int roleId, bool status);
    }
}