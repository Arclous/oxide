﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using Castle.Components.DictionaryAdapter;
using Oxide.Core.Data.Repository;
using Oxide.Core.Extesions;
using Oxide.Core.Models.AdminPermission;
using Oxide.Core.Models.AdminRole;
using Oxide.Core.Models.AdminUser;
using Oxide.Core.Syntax;
using Oxide.Core.ViewModels.AdminRoleViewModels;

namespace Oxide.Core.Services.AdminRoleService
{
    public class AdminRoleService : IAdminRoleService
    {
        private readonly IOxideRepository<AdminRolePermission> _adminRolePermissionRepository;
        private readonly IOxideRepository<AdminRole> _adminRoleRepository;
        private readonly IOxideRepository<AdminUser> _adminUserRepository;
        private readonly IOxideRepository<AdminUserRole> _adminUserRoleRepository;
        private readonly IOxideRepository<AdminPermission> _permissionRepository;

        public AdminRoleService(IOxideRepository<AdminRole> adminRoleRepository,
            IOxideRepository<AdminPermission> permissionRepository,
            IOxideRepository<AdminRolePermission> adminRolePermissionRepository,
            IOxideRepository<AdminUserRole> adminUserRoleRepository, IOxideRepository<AdminUser> adminUserRepository)
        {
            _adminRoleRepository = adminRoleRepository;
            _permissionRepository = permissionRepository;
            _adminRolePermissionRepository = adminRolePermissionRepository;
            _adminUserRoleRepository = adminUserRoleRepository;
            _adminUserRepository = adminUserRepository;
        }

        public void CreateRole(AdminRoleViewModel model)
        {
            var adminRole = new AdminRole();
            adminRole = model.CopyPropertiesTo(adminRole);
            _adminRoleRepository.Insert(adminRole);
        }

        public void UpdateRole(AdminRoleViewModel model)
        {
            var updateMode = _adminRoleRepository.SearchFor(x => x.Id == model.Id, x => x.Permissions).FirstOrDefault();
            model.CopyPropertiesTo(updateMode, new[] {"Id"});
            _adminRoleRepository.Update(updateMode);
        }

        public void DeleteRole(AdminRole model)
        {
            _adminRoleRepository.Delete(model);
        }

        public void DeleteRole(int roleId)
        {
            _adminRoleRepository.Delete(roleId);
        }

        public bool IsRoleExist(string roleName)
        {
            return _adminRoleRepository.GetAll().Any(x => x.Name == roleName);
        }

        public AdminRole GetRole(int roleId)
        {
            return _adminRoleRepository.GetById(roleId);
        }

        public IEnumerable<AdminRole> GetAllRoles()
        {
            return _adminRoleRepository.GetAll().ToList();
        }

        public IEnumerable<AdminRolePermission> GetPermissions(int roleId)
        {
            return _adminRolePermissionRepository.GetAll().Where(x => x.Role.Id == roleId);
        }

        public IEnumerable<int> GetAllUsers(int roleid)
        {
            return
                _adminUserRoleRepository.GetAll(i => i.AdminUser)
                                        .Where(x => x.Role.Id == roleid)
                                        .Select(p => p.AdminUser.Id);
        }

        public AdminRoleFilerViewModel GetAdminRolesWithFilter(int pageSize, int page, string filter)
        {
            var rolesFilterViewModel = new AdminRoleFilerViewModel();
            Expression<Func<AdminRole, bool>> criteriaFilter =
                x => x.Name.ToLower().Contains(filter.ToLower()) || x.Title.ToLower().Contains(filter.ToLower());
            if (filter != string.Empty)
                rolesFilterViewModel.Records =
                    GetAdminRoleViewModelList(_adminRoleRepository.Get(criteriaFilter, o => o.OrderBy(x => x.Id), null,
                        page, pageSize));
            else
                rolesFilterViewModel.Records =
                    GetAdminRoleViewModelList(_adminRoleRepository.Get(null, o => o.OrderBy(x => x.Id), null, page,
                        pageSize));
            rolesFilterViewModel.CurrentPage = page;
            rolesFilterViewModel.DisplayingRecords = rolesFilterViewModel.Records.Count();
            rolesFilterViewModel.TotalRecords = _adminRoleRepository.Table.Count();
            if (filter != string.Empty)
                rolesFilterViewModel.TotalPages = (_adminRoleRepository.Get(criteriaFilter).Count() + pageSize - 1)/
                                                  pageSize;
            else
                rolesFilterViewModel.TotalPages = (_adminRoleRepository.Table.Count() + pageSize - 1)/pageSize;
            rolesFilterViewModel.NextEnable = rolesFilterViewModel.CurrentPage < rolesFilterViewModel.TotalPages - 1;
            rolesFilterViewModel.PreviousEnable = rolesFilterViewModel.CurrentPage > 0;
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            rolesFilterViewModel.DeleteUrl = urlHelper.Action(MVC.Admin.AdminUser.ActionNames.DeleteAdminRole,
                MVC.Admin.AdminUser.Name, new {area = Constants.DefaultArea});
            return rolesFilterViewModel;
        }

        public IEnumerable<AdminUserRoleViewModel> GetAllAdminRolesOrganized(int? userId)
        {
            var resultList = new List<AdminUserRoleViewModel>();
            List<int> activeRoleList = new EditableList<int>();
            if (userId != null)
                activeRoleList =
                    _adminUserRoleRepository.GetAll(x => x.Role)
                                            .Where(x => x.AdminUser.Id == userId)
                                            .Select(x => x.Role.Id)
                                            .ToList();
            var rolesList = _adminRoleRepository.GetAll();
            foreach (var v in rolesList)
            {
                var adminUserRoleView = new AdminUserRoleViewModel();
                adminUserRoleView.Id = v.Id;
                adminUserRoleView.Name = v.Name;
                adminUserRoleView.Title = v.Title;
                adminUserRoleView.Status = activeRoleList.Contains(v.Id);
                resultList.Add(adminUserRoleView);
            }
            return resultList;
        }

        public void SetPermission(int roleId, int permissionId, bool status)
        {
            if (status)
            {
                var adminRolePermission = new AdminRolePermission
                {
                    Role = _adminRoleRepository.GetById(roleId),
                    Permission = _permissionRepository.GetById(permissionId)
                };
                adminRolePermission.Name = adminRolePermission.Role.Name;
                adminRolePermission.Title = adminRolePermission.Permission.Key;
                if (
                    !_adminRolePermissionRepository.GetAll()
                                                   .Any(x => (x.Role.Id == roleId) && x.Permission.Id == permissionId))
                    _adminRolePermissionRepository.Insert(adminRolePermission);
            }
            else
            {
                var currentAdminRolePermission =
                    _adminRolePermissionRepository.GetAll()
                                                  .FirstOrDefault(
                                                      x => (x.Role.Id == roleId) && x.Permission.Id == permissionId);
                if (currentAdminRolePermission != null)
                    _adminRolePermissionRepository.Delete(currentAdminRolePermission.Id);
            }
        }

        public void SetRole(int userId, int roleId, bool status)
        {
            if (status)
            {
                var adminRolePermission = new AdminUserRole();
                adminRolePermission.AdminUser = _adminUserRepository.GetById(userId);
                adminRolePermission.Role = _adminRoleRepository.GetById(roleId);
                adminRolePermission.Name = adminRolePermission.AdminUser.UserName;
                adminRolePermission.Title = adminRolePermission.Role.Name;
                if (!_adminUserRoleRepository.GetAll().Any(x => (x.AdminUser.Id == userId) && x.Role.Id == roleId))
                    _adminUserRoleRepository.Insert(adminRolePermission);
            }
            else
            {
                var currentAdminRolePermission =
                    _adminUserRoleRepository.GetAll()
                                            .FirstOrDefault(x => (x.AdminUser.Id == userId) && x.Role.Id == roleId);
                if (currentAdminRolePermission != null) _adminUserRoleRepository.Delete(currentAdminRolePermission.Id);
            }
        }

        private static ICollection<AdminRoleViewModel> GetAdminRoleViewModelList(IEnumerable<AdminRole> roleList)
        {
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            return (from role in roleList
                    let roleViewModel =
                        new AdminRoleViewModel
                        {
                            EditUrl =
                                urlHelper.Action(MVC.Admin.AdminUser.ActionNames.EditRole, MVC.Admin.AdminUser.Name,
                                    new {roleId = role.Id, area = Constants.DefaultArea}),
                            ManagePermissionUrl =
                                urlHelper.Action(MVC.Admin.AdminUser.ActionNames.ManagePermissions,
                                    MVC.Admin.AdminUser.Name, new {roleId = role.Id, area = Constants.DefaultArea})
                        }
                    select role.CopyPropertiesTo(roleViewModel)).ToList();
        }
    }
}