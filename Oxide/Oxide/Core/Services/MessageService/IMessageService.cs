﻿using Oxide.Core.Base.OxideDependency;
using Oxide.Core.Models.Messages;
using Oxide.Core.ViewModels.FilterViewModels;
using Oxide.Core.ViewModels.MessageViewModel;

namespace Oxide.Core.Services.MessageService
{
    /// <summary>
    ///     Responsible service for messaging for admin
    /// </summary>
    public interface IMessageService : IOxideDependency
    {
        /// <summary>
        ///     Send message according to model
        /// </summary>
        /// <param name="model">Model for message</param>
        int SendMessage(Message model);
        /// <summary>
        ///     Deletes message according to model
        /// </summary>
        /// <param name="model">Model for message</param>
        void DeleteMessage(Message model);
        /// <summary>
        ///     Deletes message according to id
        /// </summary>
        /// <param name="id">Id of the message</param>
        void DeleteMessage(int id);
        /// <summary>
        ///     Marks message as readed according to id
        /// </summary>
        /// <param name="id">Id of the message</param>
        void MarkAsReaded(int id);
        /// <summary>
        ///     Gets message according to id
        /// </summary>
        /// <param name="id">Id of the message</param>
        Message GetMessage(int id);
        /// <summary>
        ///     Deletes all messages
        /// </summary>
        void ClearMessages();
        /// <summary>
        ///     Gets message as filter view model
        /// </summary>
        /// <param name="pageSize">Size of the page for each page</param>
        /// <param name="page">Requested page index</param>
        /// <param name="filter">Filter criterias to filtering messages</param>
        /// <param name="userId">Id of the user who is reciver of the messages</param>
        FilterModel<MessageViewModel> GetMessageWithFilter(int pageSize, int page, string filter, int userId);
        /// <summary>
        ///     Gets message as filter view model
        /// </summary>
        /// <param name="pageSize">Size of the page for each page</param>
        /// <param name="page">Requested page index</param>
        /// <param name="userId">Id of the user who is reciver of the messages</param>
        FilterModel<MessageViewModel> GetQuickMessageWithFilter(int pageSize, int page, int userId);
    }
}