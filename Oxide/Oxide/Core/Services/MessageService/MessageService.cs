﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Castle.Components.DictionaryAdapter;
using Oxide.Core.Data.Repository;
using Oxide.Core.Models.Messages;
using Oxide.Core.Services.OxideUiService;
using Oxide.Core.ViewModels.FilterViewModels;
using Oxide.Core.ViewModels.MessageViewModel;

namespace Oxide.Core.Services.MessageService
{
    public class MessageService : IMessageService
    {
        private readonly IOxideRepository<Message> _messageRepository;
        private readonly IOxideUiService _oxideUiService;

        public MessageService()
        {
        }

        public MessageService(IOxideRepository<Message> messageRepository, IOxideUiService oxideUiService)
        {
            _messageRepository = messageRepository;
            _oxideUiService = oxideUiService;
        }

        public int SendMessage(Message model)
        {
            _messageRepository.Insert(model);
            return model.Id;
        }

        public void DeleteMessage(Message model)
        {
            _messageRepository.Delete(model);
        }

        public void DeleteMessage(int id)
        {
            _messageRepository.Delete(id);
        }

        public void MarkAsReaded(int id)
        {
            var message = _messageRepository.GetById(id);
            if (message == null) return;
            message.IsMessageRead = true;
            _messageRepository.Update(message);
        }

        public Message GetMessage(int id)
        {
            return _messageRepository.GetById(id, x => x.Reciever, p => p.Sender);
        }

        public void ClearMessages()
        {
            _messageRepository.DeleteAll();
        }

        public FilterModel<MessageViewModel> GetMessageWithFilter(int pageSize, int page, string filter, int userId)
        {
            var layoutFilterViewModel = new FilterModel<MessageViewModel>();
            List<Expression<Func<Message, object>>> includeProperties =
                new EditableList<Expression<Func<Message, object>>>();
            includeProperties.Add(x => x.Sender);
            Expression<Func<Message, bool>> criteriaFilter =
                x => x.Title.ToLower().Contains(filter.ToLower()) && x.Reciever.Id == userId;
            if (filter != string.Empty)
                layoutFilterViewModel.Records =
                    GetMessageViewModelList(_messageRepository.Get(criteriaFilter,
                        o => o.OrderByDescending(x => x.MessageDateTime), includeProperties, page, pageSize));
            else
                layoutFilterViewModel.Records =
                    GetMessageViewModelList(_messageRepository.Get(x => x.Reciever.Id == userId,
                        o => o.OrderByDescending(x => x.MessageDateTime), includeProperties, page, pageSize));
            layoutFilterViewModel.CurrentPage = page;
            layoutFilterViewModel.DisplayingRecords = layoutFilterViewModel.Records.Count();
            layoutFilterViewModel.TotalRecords = _messageRepository.TotalRecords(x => x.Reciever.Id == userId);
            if (filter != string.Empty)
                layoutFilterViewModel.TotalPages = (_messageRepository.Get(criteriaFilter).Count() + pageSize - 1)/
                                                   pageSize;
            else
                layoutFilterViewModel.TotalPages = (layoutFilterViewModel.TotalRecords + pageSize - 1)/pageSize;
            layoutFilterViewModel.NextEnable = layoutFilterViewModel.CurrentPage < layoutFilterViewModel.TotalPages - 1;
            layoutFilterViewModel.PreviousEnable = layoutFilterViewModel.CurrentPage > 0;
            return layoutFilterViewModel;
        }

        public FilterModel<MessageViewModel> GetQuickMessageWithFilter(int pageSize, int page, int userId)
        {
            var layoutFilterViewModel = new FilterModel<MessageViewModel>();
            List<Expression<Func<Message, object>>> includeProperties =
                new EditableList<Expression<Func<Message, object>>>();
            includeProperties.Add(x => x.Sender);
            Expression<Func<Message, bool>> criteriaFilter = x => x.Reciever.Id == userId;
            layoutFilterViewModel.Records =
                GetMessageViewModelList(_messageRepository.Get(x => x.Reciever.Id == userId,
                    o => o.OrderByDescending(x => x.MessageDateTime), includeProperties, 0, 10));
            layoutFilterViewModel.CurrentPage = page;
            layoutFilterViewModel.DisplayingRecords = layoutFilterViewModel.Records.Count();
            layoutFilterViewModel.TotalRecords = _messageRepository.TotalRecords(x => x.Reciever.Id == userId);
            layoutFilterViewModel.TotalPages = (layoutFilterViewModel.TotalRecords + pageSize - 1)/pageSize;
            layoutFilterViewModel.NextEnable = layoutFilterViewModel.CurrentPage < layoutFilterViewModel.TotalPages - 1;
            layoutFilterViewModel.PreviousEnable = layoutFilterViewModel.CurrentPage > 0;
            layoutFilterViewModel.TotalRecords =
                _messageRepository.TotalRecords(x => x.Reciever.Id == userId && x.IsMessageRead == false);
            return layoutFilterViewModel;
        }

        public ICollection<MessageViewModel> GetMessageViewModelList(IEnumerable<Message> messageList)
        {
            return
                messageList.Select(
                    message =>
                        new MessageViewModel
                        {
                            Id = message.Id,
                            Title = message.Title,
                            Name = message.Name,
                            MessageBody = message.MessageBody,
                            MessageDateTime = message.MessageDateTime,
                            SenderUserName = message.Sender.UserName,
                            SenderUserImage = _oxideUiService.GetImageContentUrl(message.Sender.UserImageId.ToString()),
                            IsMessageRead = message.IsMessageRead
                        }).ToList();
        }
    }
}