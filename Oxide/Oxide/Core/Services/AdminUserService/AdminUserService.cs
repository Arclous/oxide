﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using Castle.Components.DictionaryAdapter;
using Oxide.Core.Data.Repository;
using Oxide.Core.Extesions;
using Oxide.Core.Helpers;
using Oxide.Core.Models.AdminUser;
using Oxide.Core.Services.LanguageService;
using Oxide.Core.Syntax;
using Oxide.Core.ViewModels.AdminUserViewModels;

namespace Oxide.Core.Services.AdminUserService
{
    public class AdminUserService : IAdminUserService
    {
        private readonly IOxideRepository<AdminUser> _adminUserRepository;
        private readonly ILanguageService _languageService;

        public AdminUserService(IOxideRepository<AdminUser> adminUserRepository, ILanguageService languageService)
        {
            _adminUserRepository = adminUserRepository;
            _languageService = languageService;
        }

        public int CreateUser(AdminUserViewModel model)
        {
            var adminUser = new AdminUser();
            adminUser = model.CopyPropertiesTo(adminUser);
            adminUser.UserImageId = Convert.ToInt32(model.UserImageIdValue);
            adminUser.CreateDateTime = DateTime.Now;
            adminUser.LastLogin = DateTime.Now;
            adminUser.IsActive = false;
            adminUser.Roles = new EditableList<AdminUserRole>();
            adminUser.Password = OxideUtils.ToSha256(model.Password);
            adminUser.UserLanguage = _languageService.GetLanguage(Convert.ToInt32(model.UserLanguageId));
            _adminUserRepository.Insert(adminUser);
            return adminUser.Id;
        }

        public void UpdateUser(AdminUserViewModel model)
        {
            var updateMode = _adminUserRepository.SearchFor(x => x.Id == model.Id).FirstOrDefault();
            if (updateMode != null)
            {
                updateMode.Name = model.Name;
                updateMode.Title = model.Title;
                updateMode.Email = model.Email;
                if (model.Password != null)
                    updateMode.Password = OxideUtils.ToSha256(model.Password);
                updateMode.UserImageId = !string.IsNullOrEmpty(model.UserImageIdValue)
                    ? Convert.ToInt32(model.UserImageIdValue)
                    : -1;
                updateMode.UserLanguage = _languageService.GetLanguage(Convert.ToInt32(model.UserLanguageId));
                _adminUserRepository.Update(updateMode);
            }
        }

        public void DeleteUser(AdminUser model)
        {
            _adminUserRepository.Delete(model);
        }

        public void DeleteUser(int adminUserId)
        {
            _adminUserRepository.Delete(adminUserId);
        }

        public bool IsAdminUserExist(string userName)
        {
            return _adminUserRepository.Table.Any(x => x.UserName == userName);
        }

        public AdminUser GetUser(int adminUserId)
        {
            return _adminUserRepository.GetById(adminUserId);
        }

        public AdminUser GetUserWithLanguage(int adminUserId)
        {
            return _adminUserRepository.GetById(adminUserId, x => x.UserLanguage);
        }

        public AdminUser GetUser(string userName)
        {
            return
                _adminUserRepository.SearchFor(x => x.UserName == userName, l => l.UserLanguage,
                    t => t.Roles.Select(pr => pr.Role.Permissions)).FirstOrDefault();
        }

        public void UpdateLastAccessDateTime(string userName)
        {
            var user = _adminUserRepository.SearchFor(x => x.UserName == userName).FirstOrDefault();
            if (user != null)
            {
                user.LastLogin = DateTime.Now;
                _adminUserRepository.Update(user);
            }
        }

        public List<AdminUser> GetAllUsers()
        {
            return _adminUserRepository.GetAll().ToList();
        }

        public AdminUserFilerViewModel GetAdminUsersWithFilter(int pageSize, int page, string filter)
        {
            var pageFilterViewModel = new AdminUserFilerViewModel();
            Expression<Func<AdminUser, bool>> criteriaFilter =
                x =>
                    x.Name.ToLower().Contains(filter.ToLower()) ||
                    x.Title.ToLower().Contains(filter.ToLower()) && x.UserName.ToLower().Contains(filter.ToLower());
            if (filter != string.Empty)
                pageFilterViewModel.Records =
                    GetAdminUserViewModelList(_adminUserRepository.Get(criteriaFilter, o => o.OrderBy(x => x.Id), null,
                        page, pageSize));
            else
                pageFilterViewModel.Records =
                    GetAdminUserViewModelList(_adminUserRepository.Get(null, o => o.OrderBy(x => x.Id), null, page,
                        pageSize));
            pageFilterViewModel.CurrentPage = page;
            pageFilterViewModel.DisplayingRecords = pageFilterViewModel.Records.Count();
            pageFilterViewModel.TotalRecords = _adminUserRepository.Table.Count();
            if (filter != string.Empty)
                pageFilterViewModel.TotalPages = (_adminUserRepository.Get(criteriaFilter).Count() + pageSize - 1)/
                                                 pageSize;
            else
                pageFilterViewModel.TotalPages = (_adminUserRepository.Table.Count() + pageSize - 1)/pageSize;
            pageFilterViewModel.NextEnable = pageFilterViewModel.CurrentPage < pageFilterViewModel.TotalPages - 1;
            pageFilterViewModel.PreviousEnable = pageFilterViewModel.CurrentPage > 0;
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            pageFilterViewModel.DeleteUrl = urlHelper.Action(MVC.Admin.AdminUser.ActionNames.DeleteAdminUser,
                MVC.Admin.AdminUser.Name, new {area = Constants.DefaultArea});
            return pageFilterViewModel;
        }

        private static ICollection<AdminUserViewModel> GetAdminUserViewModelList(IEnumerable<AdminUser> userList)
        {
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            return (from adminUser in userList
                    let adminUserViewModel =
                        new AdminUserViewModel
                        {
                            EditUrl =
                                urlHelper.Action(MVC.Admin.AdminUser.ActionNames.EditUser, MVC.Admin.AdminUser.Name,
                                    new {adminUserId = adminUser.Id, area = Constants.DefaultArea}),
                            ManageRolesUrl =
                                urlHelper.Action(MVC.Admin.AdminUser.ActionNames.ManageRoles, MVC.Admin.AdminUser.Name,
                                    new {adminUserId = adminUser.Id, area = Constants.DefaultArea})
                        }
                    select adminUser.CopyPropertiesTo(adminUserViewModel)).ToList();
        }
    }
}