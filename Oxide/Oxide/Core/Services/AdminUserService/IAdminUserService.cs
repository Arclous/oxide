﻿using System.Collections.Generic;
using Oxide.Core.Base.OxideDependency;
using Oxide.Core.Models.AdminUser;
using Oxide.Core.ViewModels.AdminUserViewModels;

namespace Oxide.Core.Services.AdminUserService
{
    /// <summary>
    ///     Service for managing admin users
    /// </summary>
    public interface IAdminUserService : IOxideDependency
    {
        /// <summary>
        ///     Creates users according to model
        /// </summary>
        /// <param name="model">Admin user model</param>
        int CreateUser(AdminUserViewModel model);
        /// <summary>
        ///     Updates users according to model
        /// </summary>
        /// <param name="model">Admin user model</param>
        void UpdateUser(AdminUserViewModel model);
        /// <summary>
        ///     Deletes users according to model
        /// </summary>
        /// <param name="model">Admin user model</param>
        void DeleteUser(AdminUser model);
        /// <summary>
        ///     Deletes users according to id
        /// </summary>
        /// <param name="adminUserId">Id of admin user</param>
        void DeleteUser(int adminUserId);
        /// <summary>
        ///     Checks if user is exist,according username
        /// </summary>
        /// <param name="userName">Username of admin user</param>
        bool IsAdminUserExist(string userName);
        /// <summary>
        ///     Gets user according id
        /// </summary>
        /// <param name="adminUserId">Id of admin user</param>
        AdminUser GetUser(int adminUserId);
        /// <summary>
        ///     Gets user according id with releated language model
        /// </summary>
        /// <param name="adminUserId">Id of admin user</param>
        AdminUser GetUserWithLanguage(int adminUserId);
        /// <summary>
        ///     Gets user according username
        /// </summary>
        /// <param name="userName">Username of admin user</param>
        AdminUser GetUser(string userName);
        /// <summary>
        ///     Updates last access date and time of the user according to user name
        /// </summary>
        /// <param name="userName">Username of admin user</param>
        void UpdateLastAccessDateTime(string userName);
        /// <summary>
        ///     Gets users
        /// </summary>
        List<AdminUser> GetAllUsers();
        /// <summary>
        ///     Gets pages as filter view model
        /// </summary>
        /// <param name="pageSize">Size of the page for each page</param>
        /// <param name="page">Requested page index</param>
        /// <param name="filter">Filter criterias to filtering pages</param>
        AdminUserFilerViewModel GetAdminUsersWithFilter(int pageSize, int page, string filter);
    }
}