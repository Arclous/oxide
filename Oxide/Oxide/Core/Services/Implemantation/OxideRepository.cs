﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using Oxide.Core.Services.Interface;

namespace Oxide.Core.Services.Implemantation
{
    public class OxideRepository<TEntity> : IOxideRepository<TEntity> where TEntity : class
    {
        public OxideRepository()
        {
        }

        public OxideRepository(DbContext dbContext)
        {
            DbContext = dbContext;
        }

        public DbContext DbContext { get; set; }

        /// <summary>
        ///     Inserts entity
        /// </summary>
        public void Insert(TEntity entity)
        {
            DbContext.Set<TEntity>().Add(entity);
            DbContext.SaveChanges();
        }

        /// <summary>
        ///     Deletes entity by entity
        /// </summary>
        public void Delete(TEntity entity)
        {
            DbContext.Set<TEntity>().Remove(entity);
            DbContext.SaveChanges();
        }

        /// <summary>
        ///     Deletes entity by id
        /// </summary>
        public void Delete(int id)
        {
            DbContext.Set<TEntity>().Remove(DbContext.Set<TEntity>().Find(id));
            DbContext.SaveChanges();
        }

        /// <summary>
        ///     Deletes all entities
        /// </summary>
        public void DeleteAll()
        {
            DbContext.Set<TEntity>().RemoveRange(DbContext.Set<TEntity>());
            DbContext.SaveChanges();
        }

        /// <summary>
        ///     Updates entity
        /// </summary>
        public void Update(TEntity entity)
        {
            DbContext.Set<TEntity>().AddOrUpdate(entity);
            DbContext.SaveChanges();
        }

        /// <summary>
        ///     Searchs for entity
        /// </summary>
        public IQueryable<TEntity> SearchFor(Expression<Func<TEntity, bool>> predicate)
        {
            return DbContext.Set<TEntity>().Where(predicate);
        }

        /// <summary>
        ///     Gets all entity
        /// </summary>
        public IQueryable<TEntity> GetAll()
        {
            return DbContext.Set<TEntity>();
        }

        /// <summary>
        ///     Gets entity by id
        /// </summary>
        public TEntity GetById(int id)
        {
            return DbContext.Set<TEntity>().Find(id);
        }

        /// <summary>
        ///     Gets entityas queryable
        /// </summary>
        public IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            List<Expression<Func<TEntity, object>>> includeProperties = null, int? page = null, int? pageSize = null)
        {
            IQueryable<TEntity> query = DbContext.Set<TEntity>();
            includeProperties?.ForEach(i => { query = query.Include(i); });
            if (filter != null)
                query = query.Where(filter);
            if (orderBy != null)
                query = orderBy(query);
            if (page != null && pageSize != null)
                query = query.Skip((page.Value - 1)*pageSize.Value).Take(pageSize.Value);
            return query;
        }
    }
}