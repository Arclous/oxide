﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using Oxide.Core.Services.Interface.Registers.JobRegisterer;
using Quartz;

namespace Oxide.Core.Services.Implemantation
{
    public class OxideScheduledBackgroundJob : IOxideScheduledBackgroundJob
    {
        /// <summary>
        ///     Name of the scheduled job, the name will be used as key also
        /// </summary>
        public virtual string Name { get; }

        /// <summary>
        ///     Title of the scheduled job
        /// </summary>
        public virtual string Title { get; }

        /// <summary>
        ///     Ability to run job from the admin site
        /// </summary>
        public virtual bool CanForceToRun { get; }

        /// <summary>
        ///     Ability to manage timing for job from the admin site
        /// </summary>
        public virtual bool CanManage { get; }

        /// <summary>
        ///     Type of the timing action
        /// </summary>
        public virtual Syntax.Syntax.BackgroundTaskActionType ActionType { get; }

        /// <summary>
        ///     Settings for timing
        /// </summary>
        public virtual Action<DailyTimeIntervalScheduleBuilder> DailyAction { get; }

        /// <summary>
        ///     Settings for timing
        /// </summary>
        public virtual Action<CalendarIntervalScheduleBuilder> Action { get; }

        /// <summary>
        ///     Triger for schedule
        /// </summary>
        public virtual TriggerBuilder Triger { get; }

        /// ///
        /// <summary>
        ///     Where this job will run
        /// </summary>
        public virtual Syntax.Syntax.ScheduledJobRunPlatforms CanRunOn { get; } =
            Syntax.Syntax.ScheduledJobRunPlatforms.Cms;

        /// <summary>
        ///     Job to run
        /// </summary>
        public virtual void Execute(IJobExecutionContext context)
        {
        }
    }
}