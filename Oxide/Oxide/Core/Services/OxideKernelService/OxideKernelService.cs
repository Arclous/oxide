﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Oxide.Core.Elements;
using Oxide.Core.Models.Base;

namespace Oxide.Core.Services.OxideKernelService
{
    public class OxideKernelService : IOxideKernelService
    {
        public List<IOxideModule> GetLoadedModules()
        {
            return Oxide.LoadedModules;
        }

        public List<IOxideWidget> GetLoadedWidgets()
        {
            return Oxide.LoadedWidgets;
        }

        public List<IOxideLayout> GetLoadedLayouts()
        {
            return Oxide.LoadedLayouts;
        }

        public List<IOxideTheme> GetLoadedThemes()
        {
            return Oxide.LoadedThemes;
        }

        public Dictionary<string, Type> GetLoadedEvents()
        {
            return Oxide.Events;
        }

        public Assembly[] GetLoadedAssemblies()
        {
            return Oxide.LoadedAssemblies;
        }

        public Dictionary<string, AlternateWidget> GetLoadedAlternateWidgets()
        {
            return Oxide.WidgetAlternates;
        }

        public Dictionary<string, string> GetLoadedAlternateViews()
        {
            return Oxide.ViewAlternates;
        }

        public GlobalFilterCollection GetFilterCollection()
        {
            return Oxide.Filters;
        }

        public RouteCollection GetRoutesCollection()
        {
            return Oxide.Routes;
        }

        public BundleCollection GetBundleCollection()
        {
            return Oxide.BoundleCollection;
        }

        public List<Type> GetLoadedModels()
        {
            return Oxide.LoadedModels;
        }
    }
}