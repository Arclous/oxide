﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Oxide.Core.Base.OxideDependency;
using Oxide.Core.Elements;
using Oxide.Core.Models.Base;

namespace Oxide.Core.Services.OxideKernelService
{
    /// <summary>
    ///     Oxide kernel service
    /// </summary>
    public interface IOxideKernelService : IOxideDependency
    {
        /// <summary>
        ///     Gets loaded models which are inherited from IOxideModule
        /// </summary>
        List<IOxideModule> GetLoadedModules();
        /// <summary>
        ///     Gets loaded widgets which are inherited from IOxideWidget
        /// </summary>
        List<IOxideWidget> GetLoadedWidgets();
        /// <summary>
        ///     Gets loaded widgets which are inherited from IOxideLayout
        /// </summary>
        List<IOxideLayout> GetLoadedLayouts();
        /// <summary>
        ///     Gets loaded widgets which are inherited from IOxideTheme
        /// </summary>
        List<IOxideTheme> GetLoadedThemes();
        /// <summary>
        ///     Gets loaded events
        /// </summary>
        Dictionary<string, Type> GetLoadedEvents();
        /// <summary>
        ///     Gets loaded assemblies
        /// </summary>
        Assembly[] GetLoadedAssemblies();
        /// <summary>
        ///     Gets loaded alternate widgets
        /// </summary>
        Dictionary<string, AlternateWidget> GetLoadedAlternateWidgets();
        /// <summary>
        ///     Gets loaded alternate views
        /// </summary>
        Dictionary<string, string> GetLoadedAlternateViews();
        /// <summary>
        ///     Gets filter collection
        /// </summary>
        GlobalFilterCollection GetFilterCollection();
        /// <summary>
        ///     Gets routes collection
        /// </summary>
        RouteCollection GetRoutesCollection();
        /// <summary>
        ///     Gets bundle collection
        /// </summary>
        BundleCollection GetBundleCollection();
        /// <summary>
        ///     Gets loaded models which are inherited from IOxideDataBaseModel and based on OxideDataBaseModel
        /// </summary>
        List<Type> GetLoadedModels();
    }
}