﻿using Oxide.Core.Base.OxideDependency;

namespace Oxide.Core.Services.Redirection
{
    /// <summary>
    ///     Responsible service for redirections
    /// </summary>
    public interface IRedirectionService : IOxideDependency
    {
        /// <summary>
        ///     Gets robot value
        /// </summary>
        string GetRedirectionList();

        /// <summary>
        ///     Creates redirection according to model
        /// </summary>
        /// <param name="model">Model for redirection</param>
        int CreateRedirection(Models.Redirection.Redirection model);

        /// <summary>
        ///     Updates redirection
        /// </summary>
        /// <param name="model">Model for redirection</param>
        void UpdateRedirection(Models.Redirection.Redirection model);

        /// <summary>
        ///     Deletes redirection according to model
        /// </summary>
        /// <param name="model">Model for redirection</param>
        void DeleteRedirection(Models.Redirection.Redirection model);
        /// <summary>
        ///     Deletes redirection according to id
        /// </summary>
        /// <param name="id">Id of the redirection</param>
        void DeleteRedirection(int id);

        /// <summary>
        ///     Gets redirection according to id with all releations
        /// </summary>
        /// <param name="id">Id of the redirection</param>
        Models.Redirection.Redirection GetRedirection(int id);

        /// <summary>
        ///     Gets redirection according to url
        /// </summary>
        /// <param name="url">Url of the redirection</param>
        Models.Redirection.Redirection GetRedirection(string url);
    }
}