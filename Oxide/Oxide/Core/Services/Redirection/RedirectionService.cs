﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Linq;
using Oxide.Core.Data.Repository;

namespace Oxide.Core.Services.Redirection
{
    public class RedirectionService : IRedirectionService
    {
        private readonly IOxideRepository<Models.Redirection.Redirection> _redirectionRepository;

        public RedirectionService()
        {
        }

        public RedirectionService(IOxideRepository<Models.Redirection.Redirection> repositoryRedirection)
        {
            _redirectionRepository = repositoryRedirection;
        }

        public string GetRedirectionList()
        {
            throw new NotImplementedException();
        }

        public int CreateRedirection(Models.Redirection.Redirection model)
        {
            _redirectionRepository.Insert(model);
            return model.Id;
        }

        public void UpdateRedirection(Models.Redirection.Redirection model)
        {
            _redirectionRepository.Update(model);
        }

        public void DeleteRedirection(Models.Redirection.Redirection model)
        {
            _redirectionRepository.Delete(model);
        }

        public void DeleteRedirection(int id)
        {
            _redirectionRepository.Delete(id);
        }

        public Models.Redirection.Redirection GetRedirection(int id)
        {
            return _redirectionRepository.GetById(id);
        }

        public Models.Redirection.Redirection GetRedirection(string url)
        {
            return _redirectionRepository.SearchFor(x => x.Url == url).FirstOrDefault();
        }
    }
}