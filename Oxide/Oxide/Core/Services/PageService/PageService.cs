﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using Oxide.Areas.Admin.Models;
using Oxide.Areas.Admin.ViewModels.FilterModels;
using Oxide.Core.Base.OxideDependency;
using Oxide.Core.Data.Repository;
using Oxide.Core.Extesions;
using Oxide.Core.Helpers;
using Oxide.Core.Models.Pages;
using Oxide.Core.Syntax;
using Oxide.Core.ViewModels.PageViewModels;

namespace Oxide.Core.Services.PageService
{
    public class PageService : IPageService, IOxideDependency
    {
        private readonly IOxideRepository<Language> _languageRepository;
        private readonly IOxideRepository<Page> _pagesRepository;

        public PageService()
        {
        }

        public PageService(IOxideRepository<Page> repositoryPage, IOxideRepository<Language> languageRepository)
        {
            _pagesRepository = repositoryPage;
            _languageRepository = languageRepository;
        }

        public int CreatePage(PageViewModel model)
        {
            //Page is creating by admin site, set type as 'Standard'
            var page = GetPageModel(model);
            _pagesRepository.Insert(page);

            //Update base page id
            UpdateBasePageId(page, model.Id);
            return page.Id;
        }

        public void UpdatePage(PageViewModel model)
        {
            //Get update model according to view model
            var updateModel = UpdateModel(model, new[] {"Id", "CreateDate"});
            if (updateModel != null)
                _pagesRepository.Update(updateModel);
        }

        public void UpdateTemplatePage(TemplatePageViewModel model)
        {
            //Get update model according to view model
            //We dont need to update features below,because of it is template page
            var updateModel = UpdateModel(model,
                new[]
                {
                    "Id", "CreateDate", "PageKey", "Order", "Tag", "Description", "VisibleForSite", "VisibleForSeo",
                    "PageType", "SiteMapFrequncy", "BasePageId", "PublishImd", "PublishLater", "PublishDate",
                    "LastUpdate", "Published", "HomePage", "AggresiveCacheEnabled", "TotalVisited", "Priority"
                });

            if (updateModel != null)
                _pagesRepository.Update(updateModel);
        }

        public void DeletePage(Page model)
        {
            _pagesRepository.Delete(model);
        }

        public void DeletePage(int id)
        {
            _pagesRepository.Delete(id);
        }

        public Page GetPage(int id)
        {
            //Get full model with all releations
            return _pagesRepository.GetById(id, x => x.Language, x => x.Widgets);
        }

        public Page GetPageWithLanguage(int id)
        {
            //Get  model with language releation
            return _pagesRepository.GetById(id, x => x.Language);
        }

        public Page GetPageSimply(int id)
        {
            //Get  model as simple
            return _pagesRepository.GetById(id);
        }

        public Page GetPage(string url)
        {
            var page = _pagesRepository.SearchFor(x => x.Url == url).FirstOrDefault();
            if (page == null) return _pagesRepository.SearchFor(x => x.Url == url).FirstOrDefault();

            page.TotalVisited += 1;
            _pagesRepository.UpdateSlient(page);
            return page;
        }

        public Page GetPageWithLanguage(string url)
        {
            var page = _pagesRepository.SearchFor(x => x.Url == url, x => x.Language).FirstOrDefault();
            if (page == null) return null;

            page.TotalVisited += 1;
            _pagesRepository.UpdateSlient(page);
            return page;
        }

        public Page GetPage(string url, string languageCode)
        {
            var language = _languageRepository.SearchFor(x => x.Code3 == languageCode).FirstOrDefault();
            var requestedPage = _pagesRepository.SearchFor(x => x.Url == url).FirstOrDefault();
            if (language != null)
            {
                if (requestedPage != null && requestedPage.Language.Code3 != languageCode)
                {
                    if (requestedPage.BasePageId != 0)
                    {
                        var page = _pagesRepository.SearchFor(
                            x => x.BasePageId == requestedPage.BasePageId && x.Language.Code3 == languageCode)
                            .FirstOrDefault() ?? _pagesRepository.SearchFor(
                                x => x.Id == requestedPage.BasePageId && x.Language.Code3 == languageCode)
                                .FirstOrDefault();

                        return page;
                    }


                    return
                        _pagesRepository.SearchFor(
                            x => x.BasePageId == requestedPage.Id && x.Language.Code3 == languageCode).FirstOrDefault();
                }
                return requestedPage;
            }
            return null;
        }

        public PageFilterViewModel GetPagesWithFilter(int pageSize, int page, string filter)
        {
            var pageFilterViewModel = new PageFilterViewModel();
            var criteriaFilter = Expressions.GetPageFilterCriteria(filter);
            var critertiaPageType = Expressions.GetPageTypeCriteria(Syntax.Syntax.PageTypes.Standard);

            criteriaFilter = criteriaFilter.And(critertiaPageType);

            if (filter != string.Empty)
                pageFilterViewModel.Records =
                    GetPageViewModelList(_pagesRepository.Get(criteriaFilter, o => o.OrderBy(x => x.Id), null, page,
                        pageSize));
            else
                pageFilterViewModel.Records =
                    GetPageViewModelList(_pagesRepository.Get(critertiaPageType, o => o.OrderBy(x => x.Id), null, page,
                        pageSize));

            pageFilterViewModel.CurrentPage = page;
            pageFilterViewModel.DisplayingRecords = pageFilterViewModel.Records.Count();
            pageFilterViewModel.TotalRecords = _pagesRepository.TotalRecords(critertiaPageType);

            if (filter != string.Empty)
                pageFilterViewModel.TotalPages = (_pagesRepository.Get(criteriaFilter).Count() + pageSize - 1)/pageSize;
            else
                pageFilterViewModel.TotalPages = (_pagesRepository.Get(critertiaPageType).Count() + pageSize - 1)/
                                                 pageSize;

            pageFilterViewModel.NextEnable = pageFilterViewModel.CurrentPage < pageFilterViewModel.TotalPages - 1;
            pageFilterViewModel.PreviousEnable = pageFilterViewModel.CurrentPage > 0;

            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            pageFilterViewModel.PublishUrl = urlHelper.Action(MVC.Admin.Page.ActionNames.ChangePublishStatus,
                MVC.Admin.Page.Name);

            pageFilterViewModel.DeleteUrl = urlHelper.Action(MVC.Admin.Page.ActionNames.DeletePage, MVC.Admin.Page.Name);
            pageFilterViewModel.TotalCreated = pageFilterViewModel.TotalRecords;

            pageFilterViewModel.TotalPublished =
                _pagesRepository.TotalRecords(Expressions.GetPagePublishStatusCriteria(true,
                    Syntax.Syntax.PageTypes.Standard));

            pageFilterViewModel.TotalDraft =
                _pagesRepository.TotalRecords(Expressions.GetPagePublishStatusCriteria(true,
                    Syntax.Syntax.PageTypes.Standard));

            pageFilterViewModel.TotalWaiting =
                _pagesRepository.TotalRecords(
                    Expressions.GetPagePublishStatusWithDateCriteria(Syntax.Syntax.PageTypes.Standard));

            return pageFilterViewModel;
        }

        public PageFilterViewModel GetPagesWithFilterWithLanguage(int pageSize, int page, string filter)
        {
            var criteriaFilter = Expressions.GetPageFilterCriteria(filter);
            var critertiaPageType = Expressions.GetPageTypeCriteria(Syntax.Syntax.PageTypes.Standard);
            criteriaFilter = criteriaFilter.And(critertiaPageType);

            var pageFilterViewModel = new PageFilterViewModel();
            if (filter != string.Empty)
                pageFilterViewModel.Records =
                    GetPageViewModelList(_pagesRepository.Get(criteriaFilter, o => o.OrderBy(x => x.Id),
                        Expressions.PageIncludeProperities(), page, pageSize));
            else
                pageFilterViewModel.Records =
                    GetPageViewModelList(_pagesRepository.Get(critertiaPageType, o => o.OrderBy(x => x.Id),
                        Expressions.PageIncludeProperities(), page, pageSize));

            pageFilterViewModel.CurrentPage = page;
            pageFilterViewModel.DisplayingRecords = pageFilterViewModel.Records.Count();
            pageFilterViewModel.TotalRecords = _pagesRepository.TotalRecords(critertiaPageType);

            if (filter != string.Empty)
                pageFilterViewModel.TotalPages = (_pagesRepository.Get(criteriaFilter).Count() + pageSize - 1)/pageSize;
            else
                pageFilterViewModel.TotalPages = (_pagesRepository.Get(critertiaPageType).Count() + pageSize - 1)/
                                                 pageSize;

            pageFilterViewModel.NextEnable = pageFilterViewModel.CurrentPage < pageFilterViewModel.TotalPages - 1;
            pageFilterViewModel.PreviousEnable = pageFilterViewModel.CurrentPage > 0;

            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            pageFilterViewModel.PublishUrl = urlHelper.Action(MVC.Admin.Page.ActionNames.ChangePublishStatus,
                MVC.Admin.Page.Name);

            pageFilterViewModel.DeleteUrl = urlHelper.Action(MVC.Admin.Page.ActionNames.DeletePage, MVC.Admin.Page.Name);
            pageFilterViewModel.TotalCreated = pageFilterViewModel.TotalRecords;

            pageFilterViewModel.TotalPublished =
                _pagesRepository.TotalRecords(Expressions.GetPagePublishStatusCriteria(true,
                    Syntax.Syntax.PageTypes.Standard));

            pageFilterViewModel.TotalDraft =
                _pagesRepository.TotalRecords(Expressions.GetPagePublishStatusCriteria(false,
                    Syntax.Syntax.PageTypes.Standard));

            pageFilterViewModel.TotalWaiting =
                _pagesRepository.TotalRecords(
                    Expressions.GetPagePublishStatusWithDateCriteria(Syntax.Syntax.PageTypes.Standard));

            return pageFilterViewModel;
        }

        public PageFilterViewModel GetPagesWithFilter(int pageSize, int page, string filter,
            Expression<Func<Page, bool>> predicate)
        {
            var pageFilterViewModel = new PageFilterViewModel();
            var criteriaFilter = Expressions.GetPageFilterCriteria(filter);
            var critertiaPageType = Expressions.GetPageTypeCriteria(Syntax.Syntax.PageTypes.Standard);
            criteriaFilter = criteriaFilter.And(critertiaPageType);

            var newPredicate = predicate.And(critertiaPageType);
            var filterCriteria = criteriaFilter.And(newPredicate);

            if (filter != string.Empty)
                pageFilterViewModel.Records =
                    GetPageViewModelList(_pagesRepository.Get(filterCriteria, o => o.OrderBy(x => x.Id), null, page,
                        pageSize));
            else
                pageFilterViewModel.Records =
                    GetPageViewModelList(_pagesRepository.Get(newPredicate, o => o.OrderBy(x => x.Id), null, page,
                        pageSize));

            pageFilterViewModel.CurrentPage = page;
            pageFilterViewModel.DisplayingRecords = pageFilterViewModel.Records.Count();
            pageFilterViewModel.TotalRecords = _pagesRepository.GetAll().Count(critertiaPageType);

            if (filter != string.Empty)
                pageFilterViewModel.TotalPages = (_pagesRepository.Get(filterCriteria).Count() + pageSize - 1)/pageSize;
            else
                pageFilterViewModel.TotalPages = (_pagesRepository.Get(newPredicate).Count() + pageSize - 1)/pageSize;

            pageFilterViewModel.NextEnable = pageFilterViewModel.CurrentPage < pageFilterViewModel.TotalPages - 1;
            pageFilterViewModel.PreviousEnable = pageFilterViewModel.CurrentPage > 0;

            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            pageFilterViewModel.PublishUrl = urlHelper.Action(MVC.Admin.Page.ActionNames.ChangePublishStatus,
                MVC.Admin.Page.Name);

            pageFilterViewModel.DeleteUrl = urlHelper.Action(MVC.Admin.Page.ActionNames.DeletePage, MVC.Admin.Page.Name);
            pageFilterViewModel.TotalCreated = pageFilterViewModel.TotalRecords;

            pageFilterViewModel.TotalPublished =
                _pagesRepository.GetAll()
                                .Count(Expressions.GetPagePublishStatusCriteria(true, Syntax.Syntax.PageTypes.Standard));

            pageFilterViewModel.TotalDraft =
                _pagesRepository.GetAll()
                                .Count(Expressions.GetPagePublishStatusCriteria(false, Syntax.Syntax.PageTypes.Standard));

            pageFilterViewModel.TotalWaiting =
                _pagesRepository.TotalRecords(
                    Expressions.GetPagePublishStatusWithDateCriteria(Syntax.Syntax.PageTypes.Standard));

            return pageFilterViewModel;
        }

        public PageFilterViewModel GetPagesWithFilterWithLanguage(int pageSize, int page, string filter,
            Expression<Func<Page, bool>> predicate)
        {
            var pageFilterViewModel = new PageFilterViewModel();
            var criteriaFilter = Expressions.GetPageFilterCriteria(filter);
            var critertiaPageType = Expressions.GetPageTypeCriteria(Syntax.Syntax.PageTypes.Standard);
            criteriaFilter = criteriaFilter.And(critertiaPageType);

            var newPredicate = predicate.And(critertiaPageType);
            var filterCriteria = criteriaFilter.And(newPredicate);
            if (filter != string.Empty)
                pageFilterViewModel.Records =
                    GetPageViewModelList(_pagesRepository.Get(filterCriteria, o => o.OrderBy(x => x.Id),
                        Expressions.PageIncludeProperities(), page, pageSize));
            else
                pageFilterViewModel.Records =
                    GetPageViewModelList(_pagesRepository.Get(newPredicate, o => o.OrderBy(x => x.Id),
                        Expressions.PageIncludeProperities(), page, pageSize));

            pageFilterViewModel.CurrentPage = page;
            pageFilterViewModel.DisplayingRecords = pageFilterViewModel.Records.Count();
            pageFilterViewModel.TotalRecords = _pagesRepository.GetAll().Count(critertiaPageType);

            if (filter != string.Empty)
                pageFilterViewModel.TotalPages = (_pagesRepository.Get(filterCriteria).Count() + pageSize - 1)/pageSize;
            else
                pageFilterViewModel.TotalPages = (_pagesRepository.Get(newPredicate).Count() + pageSize - 1)/pageSize;

            pageFilterViewModel.NextEnable = pageFilterViewModel.CurrentPage < pageFilterViewModel.TotalPages - 1;
            pageFilterViewModel.PreviousEnable = pageFilterViewModel.CurrentPage > 0;

            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            pageFilterViewModel.PublishUrl = urlHelper.Action(MVC.Admin.Page.ActionNames.ChangePublishStatus,
                MVC.Admin.Page.Name);

            pageFilterViewModel.DeleteUrl = urlHelper.Action(MVC.Admin.Page.ActionNames.DeletePage, MVC.Admin.Page.Name);
            pageFilterViewModel.TotalCreated = pageFilterViewModel.TotalRecords;

            pageFilterViewModel.TotalPublished =
                _pagesRepository.GetAll()
                                .Count(Expressions.GetPagePublishStatusCriteria(true, Syntax.Syntax.PageTypes.Standard));

            pageFilterViewModel.TotalDraft =
                _pagesRepository.GetAll()
                                .Count(Expressions.GetPagePublishStatusCriteria(false, Syntax.Syntax.PageTypes.Standard));

            pageFilterViewModel.TotalWaiting =
                _pagesRepository.TotalRecords(
                    Expressions.GetPagePublishStatusWithDateCriteria(Syntax.Syntax.PageTypes.Standard));

            return pageFilterViewModel;
        }

        public List<Page> GetAllPages(Syntax.Syntax.PageTypes pageType)
        {
            return _pagesRepository.SearchFor(x => x.PageType == pageType).ToList();
        }

        public List<Page> GetAllPages()
        {
            return _pagesRepository.GetAll().ToList();
        }

        public ICollection<Page> GetAllPages(Expression<Func<Page, bool>> predicate)
        {
            return _pagesRepository.SearchFor(predicate).ToList();
        }

        public void CreateClone(Page model, int languageId, string name)
        {
            var cloneModel = new Page();
            var language = _languageRepository.GetById(languageId);

            //Set properities for the cloned page accordig to referaned page
            cloneModel.Name = name;
            cloneModel.BasePageId = model.BasePageId == 0 ? model.Id : model.BasePageId;
            cloneModel.Title = model.Title + "_" + language.Code;
            cloneModel.PageKey = model.PageKey;
            cloneModel.InnerLayoutId = model.InnerLayoutId;
            cloneModel.MainLayoutId = model.MainLayoutId;
            cloneModel.Order = model.Order;
            cloneModel.Published = false;
            cloneModel.SiteMapFrequncy = model.SiteMapFrequncy;
            cloneModel.PublishImd = false;
            cloneModel.CreateDate = DateTime.Now;
            cloneModel.PublishDate = DateTime.Now;
            cloneModel.LastUpdate = DateTime.Now;
            cloneModel.HomePage = model.HomePage;

            //Set new url address for the cloned page according to selected language
            if (!model.Url.EndsWith("/"))
                cloneModel.Url = model.Url + "_" + language.Code;
            else
                cloneModel.Url = model.Url + cloneModel.Title;
            cloneModel.VisibleForSeo = model.VisibleForSeo;
            cloneModel.VisibleForSite = model.VisibleForSite;
            cloneModel.Priority = model.Priority;
            cloneModel.LanguageId = languageId;
            cloneModel.Widgets = new List<PageWidget>();

            //Insert new cloned page
            _pagesRepository.Insert(cloneModel);

            //Insert all widgets which referance page contains
            foreach (var sourceWidget in model.Widgets)
            {
                var newWidget = new PageWidget();
                sourceWidget.CopyPropertiesTo(newWidget, new[] {"Id"});
                newWidget.PageId = cloneModel.Id;
                cloneModel.Widgets.Add(newWidget);
            }
            _pagesRepository.Update(cloneModel);
            model.BasePageId = cloneModel.BasePageId;
            _pagesRepository.Update(model);
        }

        public void Publish(int pageId)
        {
            var updateMode = _pagesRepository.GetById(pageId);
            if (updateMode != null)
            {
                updateMode.Published = true;
                updateMode.PublishImd = true;
                updateMode.PublishDate = DateTime.Today;
                _pagesRepository.Update(updateMode);
            }
        }

        public void UnPublish(int pageId)
        {
            var updateMode = _pagesRepository.GetById(pageId);
            if (updateMode != null)
            {
                updateMode.Published = false;
                updateMode.PublishImd = false;
                _pagesRepository.Update(updateMode);
            }
        }

        private static ICollection<PageViewModel> GetPageViewModelList(IEnumerable<Page> pageList)
        {
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            return (from page in pageList
                    let pageViewModel =
                        new PageViewModel
                        {
                            ManageWidgetsUrl =
                                urlHelper.Action(MVC.Admin.Page.ActionNames.ManageWidgets, MVC.Admin.Page.Name,
                                    new {pageId = page.Id}),
                            EditPageUrl =
                                urlHelper.Action(MVC.Admin.Page.ActionNames.EditPage, MVC.Admin.Page.Name,
                                    new {pageId = page.Id})
                        }
                    select page.CopyPropertiesTo(pageViewModel)).OrderBy(x => x.Order).ToList();
        }

        private  Page GetPageModel(PageViewModel model)
        {
            DateTime dt;
            var page = new Page();

            model.PageType = model.PageType;
            page = model.CopyPropertiesTo(page);
            page.SiteMapFrequncy = (Syntax.Syntax.SiteMapFrequncy) model.SiteMapFrequncySelected;
            page.LanguageId = model.LanguageSelected;
            page.LastUpdate = DateTime.Now;
            page.PublishDate = DateTime.TryParse(model.SelectedDate, out dt)
                ? DateTime.Parse(model.SelectedDate)
                : DateTime.Now;
            page.CreateDate = DateTime.Now;

            if (model.IsPublishImd != null)
                page.PublishImd = bool.Parse(model.IsPublishImd);
            if (model.IsPublishLater != null)
                page.PublishLater = bool.Parse(model.IsPublishLater);
            if (model.IsAggresiveCacheEnabled != null)
                page.AggresiveCacheEnabled = bool.Parse(model.IsAggresiveCacheEnabled);
            page.Published = !page.PublishLater;
            if (model.HomePage)
            {
                page.Url = Constants.HomePage;
            }
            else
            {
                if (!page.Url.StartsWith(Constants.HomePage))
                    page.Url = Constants.HomePage + page.Url;

                if (IsUrlExist(page.Url))
                    page.Url = GetUniqueUrl(page.Url);
                
            }
            if (page.PublishImd)
                page.Published = true;
            else
                page.Published = page.PublishDate < DateTime.Now;

            return page;
        }

        private Page UpdateModel(PageViewModel model, string[] notUpdate = null)
        {
            var updateModel = _pagesRepository.SearchFor(x => x.Id == model.Id).FirstOrDefault();

            if (updateModel == null) return null;

            model.CopyPropertiesTo(updateModel, notUpdate ?? new[] {"Id", "CreateDate"});

            updateModel.SiteMapFrequncy = (Syntax.Syntax.SiteMapFrequncy) model.SiteMapFrequncySelected;
            updateModel.LanguageId = model.LanguageSelected;
            updateModel.LastUpdate = DateTime.Now;
            DateTime dt;
            updateModel.PublishDate = DateTime.TryParse(model.SelectedDate, out dt)
                ? DateTime.Parse(model.SelectedDate)
                : DateTime.Now;
            if (model.IsPublishImd != null)
                updateModel.PublishImd = bool.Parse(model.IsPublishImd);
            if (model.IsPublishLater != null)
                updateModel.PublishLater = bool.Parse(model.IsPublishLater);
            if (model.IsHomePage != null)
                updateModel.HomePage = bool.Parse(model.IsHomePage);
            if (model.IsAggresiveCacheEnabled != null)
                updateModel.AggresiveCacheEnabled = bool.Parse(model.IsAggresiveCacheEnabled);

            if (model.HomePage)
            {
                var homepage = _pagesRepository.Table.Where(x => x.HomePage).ToList();
                if (homepage.Count == 1)
                    updateModel.Url = Constants.HomePage;
                else
                {
                    var homePages = _pagesRepository.Table.Where(x => x.Url == Constants.HomePage).ToList();

                    if (homePages.Count == 0)
                        updateModel.Url = Constants.HomePage;
                    else
                    {
                        var page = homePages.FirstOrDefault();
                        if (page != null && (homePages.Count == 1 && page.Id == updateModel.Id))
                        {
                            updateModel.Url = Constants.HomePage;
                        }
                    }
                }
            }
            else
            {
                if (!model.Url.StartsWith(Constants.HomePage))
                    updateModel.Url = Constants.HomePage + model.Url;

                if (IsUrlExist(updateModel.Url))
                    updateModel.Url = GetUniqueUrl(updateModel.Url);
            }

            if (updateModel.PublishImd)
                updateModel.Published = true;
            else
                updateModel.Published = updateModel.PublishDate < DateTime.Now;

            return updateModel;
        }

        private void UpdateBasePageId(Page page, int id)
        {
            if (page.BasePageId != 0) return;
            page.BasePageId = id;
            _pagesRepository.Update(page);
        }

        private string GetUniqueUrl(string url)
        {
            var totalUrlFound = GetPageUrlExistCount(url);
            if (totalUrlFound <= 0) return url;
            var pageIndexer = 1;
            var orgUrl = url;
            do
            {
                url = $@"{orgUrl}-{pageIndexer + 1}";
                totalUrlFound = GetPageUrlExistCount(url);
                pageIndexer += 1;

            } while (totalUrlFound > 0);

            return url;
        }
        private bool IsUrlExist(string url)
        {
            return _pagesRepository.Table.Any(x => x.Url == url);
        }

        private int GetPageUrlExistCount(string url)
        {
            return _pagesRepository.Table.Where(x => x.Url == url).ToList().Count;
        }
    }
}