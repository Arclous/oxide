﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Oxide.Areas.Admin.ViewModels.FilterModels;
using Oxide.Core.Models.Pages;
using Oxide.Core.ViewModels.PageViewModels;

namespace Oxide.Core.Services.PageService
{
    /// <summary>
    ///     Responsible service for pages
    /// </summary>
    public interface IPageService
    {
        /// <summary>
        ///     Creates page according to view model
        /// </summary>
        /// <param name="model">View model for page</param>
        int CreatePage(PageViewModel model);
        /// <summary>
        ///     Updates page
        /// </summary>
        /// <param name="model">View model for page</param>
        void UpdatePage(PageViewModel model);
        /// <summary>
        ///     Updates template page
        /// </summary>
        /// <param name="model">View model for template page</param>
        void UpdateTemplatePage(TemplatePageViewModel model);
        /// <summary>
        ///     Deletes page according to model
        /// </summary>
        /// <param name="model">Model for page</param>
        void DeletePage(Page model);
        /// <summary>
        ///     Deletes page according to id
        /// </summary>
        /// <param name="id">Id of the page</param>
        void DeletePage(int id);
        /// <summary>
        ///     Gets page according to id with all releations
        /// </summary>
        /// <param name="id">Id of the page</param>
        Page GetPage(int id);
        /// <summary>
        ///     Gets page according to id with language releated model
        /// </summary>
        /// <param name="id">Id of the page</param>
        Page GetPageWithLanguage(int id);
        /// <summary>
        ///     Gets page according to id without any releated model
        /// </summary>
        /// <param name="id">Id of the page</param>
        Page GetPageSimply(int id);
        /// <summary>
        ///     Gets page according to url
        /// </summary>
        /// <param name="url">Url of the page</param>
        Page GetPage(string url);
        /// <summary>
        ///     Gets page according to url with language related model
        /// </summary>
        /// <param name="url">Url of the page</param>
        Page GetPageWithLanguage(string url);
        /// <summary>
        ///     Gets page according to url and language code
        /// </summary>
        /// <param name="url">Url of the page</param>
        /// <param name="languageCode">Language code of language which is requesting</param>
        Page GetPage(string url, string languageCode);
        /// <summary>
        ///     Gets pages as filter view model
        /// </summary>
        /// <param name="pageSize">Size of the page for each page</param>
        /// <param name="page">Requested page index</param>
        /// <param name="filter">Filter criterias to filtering pages</param>
        PageFilterViewModel GetPagesWithFilter(int pageSize, int page, string filter);
        /// <summary>
        ///     Gets pages as filter view model with releated language model
        /// </summary>
        /// <param name="pageSize">Size of the page for each page</param>
        /// <param name="page">Requested page index</param>
        /// <param name="filter">Filter criterias to filtering pages</param>
        PageFilterViewModel GetPagesWithFilterWithLanguage(int pageSize, int page, string filter);
        /// <summary>
        ///     Gets pages as filter view model without any releated model
        /// </summary>
        /// <param name="pageSize">Size of the page for each page</param>
        /// <param name="page">Requested page index</param>
        /// <param name="filter">Filter criterias to filtering pages</param>
        /// <param name="predicate">Predicate of the filter</param>
        PageFilterViewModel GetPagesWithFilter(int pageSize, int page, string filter,
            Expression<Func<Page, bool>> predicate);
        /// <summary>
        ///     Gets pages as filter view model with releated language model
        /// </summary>
        /// <param name="pageSize">Size of the page for each page</param>
        /// <param name="page">Requested page index</param>
        /// <param name="filter">Filter criterias to filtering pages</param>
        /// <param name="predicate">Predicate of the filter</param>
        PageFilterViewModel GetPagesWithFilterWithLanguage(int pageSize, int page, string filter,
            Expression<Func<Page, bool>> predicate);
        /// <summary>
        ///     Gets all pages according to page type
        /// </summary>
        /// <param name="pageType">Type of the which is requesting</param>
        List<Page> GetAllPages(Syntax.Syntax.PageTypes pageType);
        /// <summary>
        ///     Gets all pages
        /// </summary>
        List<Page> GetAllPages();
        /// <summary>
        ///     Gets all pages
        /// </summary>
        ICollection<Page> GetAllPages(Expression<Func<Page, bool>> predicate);
        /// <summary>
        ///     Clones page according to model
        /// </summary>
        /// <param name="model">Page model which will be cloned</param>
        /// <param name="languageId">Id of the language</param>
        /// <param name="name">Name of the page</param>
        void CreateClone(Page model, int languageId, string name);
        /// <summary>
        ///     Publishes page accroding to page id
        /// </summary>
        /// <param name="pageId">Id of the page which will be published</param>
        void Publish(int pageId);
        /// <summary>
        ///     Unpublishes page according to page id
        /// </summary>
        /// <param name="pageId">Id of the page which will be unpublished</param>
        void UnPublish(int pageId);
    }
}