﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Caching;
using System.Web;
using System.Web.Mvc;
using Oxide.Areas.Admin.ViewModels.FilterModels;
using Oxide.Core.Data.Repository;
using Oxide.Core.Extesions;
using Oxide.Core.Managers.SettingsManager;
using Oxide.Core.ViewModels.CacheItemViewModels;
using CacheItem = Oxide.Core.Models.Cache.CacheItem;

namespace Oxide.Core.Services.CacheItemService
{
    public class CacheItemService : ICacheItemService
    {
        private readonly IOxideRepository<CacheItem> _cacheRepository;

        public CacheItemService(IOxideRepository<CacheItem> cacheRepository)
        {
            _cacheRepository = cacheRepository;
        }

        public void CreateCacheItem(CacheItem model)
        {
            _cacheRepository.Insert(model);
        }

        public void UpdateCacheItem(CacheItem model)
        {
            _cacheRepository.Update(model);
        }

        public void DeleteCacheItem(CacheItem model)
        {
            _cacheRepository.Delete(model);
        }

        public void DeleteCacheItem(int id)
        {
            _cacheRepository.Delete(id);
        }

        public CacheItem GetCacheItem(int id)
        {
            var model = _cacheRepository.SearchFor(x => x.Id == id).FirstOrDefault();
            return model;
        }

        public CacheItem GetCacheItemByName(string name)
        {
            var model =
                _cacheRepository.SearchFor(
                    x => x.Name.ToLower() == name.ToLower() && x.Instance == SettingsManager.InstanceName)
                                .FirstOrDefault();
            return model;
        }

        public List<CacheItem> GetAllCacheItems()
        {
            return _cacheRepository.GetAll().ToList();
        }

        public int GetCacheTimeValue(string name)
        {
            var model =
                _cacheRepository.SearchFor(
                    x =>
                        string.Equals(x.Name, name, StringComparison.CurrentCultureIgnoreCase) &&
                        x.Instance == SettingsManager.InstanceName).FirstOrDefault();
            return model?.Value ?? 60;
        }

        public void UpdateCacheDateTime(string name)
        {
            var cacheItem =
                _cacheRepository.SearchFor(
                    x => x.Name.ToLower() == name.ToLower() && x.Instance == SettingsManager.InstanceName)
                                .FirstOrDefault();
            if (cacheItem != null)
            {
                cacheItem.LastUpdate = DateTime.Now;
                _cacheRepository.Update(cacheItem);
            }
        }

        public CacheItemFilterViewModel GetCacheItemsWithFilter(int pageSize, int page, string filter)
        {
            var pageFilterViewModel = new CacheItemFilterViewModel();
            Expression<Func<CacheItem, bool>> criteriaFilter =
                x =>
                    (x.Name.ToLower().Contains(filter.ToLower()) ||
                     x.Title.ToLower().Contains(filter.ToLower()) && x.Instance == SettingsManager.InstanceName);
            if (filter != string.Empty)
                pageFilterViewModel.Records =
                    GetCacheItemViewModelList(_cacheRepository.Get(criteriaFilter, o => o.OrderBy(x => x.Id), null, page,
                        pageSize));
            else
                pageFilterViewModel.Records =
                    GetCacheItemViewModelList(_cacheRepository.Get(null, o => o.OrderBy(x => x.Id), null, page, pageSize));
            pageFilterViewModel.CurrentPage = page;
            pageFilterViewModel.DisplayingRecords = pageFilterViewModel.Records.Count();
            pageFilterViewModel.TotalRecords =
                _cacheRepository.GetAll().Count(x => x.Instance == SettingsManager.InstanceName);
            if (filter != string.Empty)
                pageFilterViewModel.TotalPages = (_cacheRepository.Get(criteriaFilter).Count() + pageSize - 1)/pageSize;
            else
                pageFilterViewModel.TotalPages = (pageFilterViewModel.TotalRecords + pageSize - 1)/pageSize;
            pageFilterViewModel.NextEnable = pageFilterViewModel.CurrentPage < pageFilterViewModel.TotalPages - 1;
            pageFilterViewModel.PreviousEnable = pageFilterViewModel.CurrentPage > 0;
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            pageFilterViewModel.ClearCacheUrl = urlHelper.Action(MVC.Admin.CacheItem.ActionNames.ClearCacheItem,
                MVC.Admin.CacheItem.Name, new {area = T4MVCHelpers.DefaultArea()});
            pageFilterViewModel.UpdateValueUrl = urlHelper.Action(MVC.Admin.CacheItem.ActionNames.UpdateCacheSetting,
                MVC.Admin.CacheItem.Name, new {area = T4MVCHelpers.DefaultArea()});
            return pageFilterViewModel;
        }

        public void OrganizeCacheItems(int days)
        {
            _cacheRepository.DeleteAll(
                x =>
                    DbFunctions.DiffDays(x.LastUpdate, DateTime.Today) >= days &&
                    x.Instance == SettingsManager.InstanceName);
        }

        private static ICollection<CacheItemViewModel> GetCacheItemViewModelList(IEnumerable<CacheItem> cacheItemList)
        {
            return (from cacheItemViewModel in cacheItemList
                    let pageViewModel =
                        new CacheItemViewModel {IsCached = MemoryCache.Default.Contains(cacheItemViewModel.Name)}
                    select cacheItemViewModel.CopyPropertiesTo(pageViewModel)).ToList();
        }
    }
}