﻿using System.Collections.Generic;
using Oxide.Areas.Admin.ViewModels.FilterModels;
using Oxide.Core.Base.OxideDependency;
using Oxide.Core.Models.Cache;

namespace Oxide.Core.Services.CacheItemService
{
    /// <summary>
    ///     Service for managing cache and cache items
    /// </summary>
    public interface ICacheItemService : IOxideDependency
    {
        /// <summary>
        ///     Creates cache item according to model
        /// </summary>
        /// <param name="model">Cache item model</param>
        void CreateCacheItem(CacheItem model);
        /// <summary>
        ///     Updates cache item according to model
        /// </summary>
        /// <param name="model">Cache item model</param>
        void UpdateCacheItem(CacheItem model);
        /// <summary>
        ///     Deletes cache item according to model
        /// </summary>
        /// <param name="model">Cache item model</param>
        void DeleteCacheItem(CacheItem model);
        /// <summary>
        ///     Deletes cache item according to id
        /// </summary>
        /// <param name="id">Id of the cache item</param>
        void DeleteCacheItem(int id);
        /// <summary>
        ///     Gets cache item according to id
        /// </summary>
        /// <param name="id">Id of the cache item</param>
        CacheItem GetCacheItem(int id);
        /// <summary>
        ///     Gets cache item according to name
        /// </summary>
        /// <param name="name">Name of the cache item </param>
        CacheItem GetCacheItemByName(string name);
        /// <summary>
        ///     Gets all cache items
        /// </summary>
        List<CacheItem> GetAllCacheItems();
        /// <summary>
        ///     Gets cache item time value according to name
        /// </summary>
        /// <param name="name">Name of the cache item </param>
        int GetCacheTimeValue(string name);
        /// <summary>
        ///     Updates cache item time value according to name
        /// </summary>
        /// <param name="name">Name of the cache item </param>
        void UpdateCacheDateTime(string name);
        /// <summary>
        ///     Gets cache items as filter view model
        /// </summary>
        /// <param name="pageSize">Size of the page for each page</param>
        /// <param name="page">Requested page index</param>
        /// <param name="filter">Filter criterias to filtering cache items</param>
        CacheItemFilterViewModel GetCacheItemsWithFilter(int pageSize, int page, string filter);
        /// <summary>
        ///     Clears all cache items which are later than today - days
        /// </summary>
        /// <param name="days">Max days for cleaning all cache items </param>
        void OrganizeCacheItems(int days);
    }
}