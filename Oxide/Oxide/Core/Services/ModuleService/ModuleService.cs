﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Oxide.Core.Base.OxideDataBaseModel;
using Oxide.Core.Data.Repository;
using Oxide.Core.Elements;
using Oxide.Core.Extesions;
using Oxide.Core.Helpers;
using Oxide.Core.Managers.AssemblyManager;
using Oxide.Core.Managers.AttiributeManager;
using Oxide.Core.Managers.LayoutManager;
using Oxide.Core.Managers.OxideMigrationManager;
using Oxide.Core.Managers.ScheduledJobManager;
using Oxide.Core.Managers.WidgetManager;
using Oxide.Core.Models.Modules;
using Oxide.Core.Services.Interface.Registers.FilterRegister;
using Oxide.Core.Services.Interface.Registers.ResourceRegister;
using Oxide.Core.Services.Interface.Registers.RouteRegister;
using Oxide.Core.Syntax;
using Oxide.Core.ViewModels.FilterViewModels;
using Oxide.Core.ViewModels.ModuleViewModels;

namespace Oxide.Core.Services.ModuleService
{
    public class ModuleService : IModuleService
    {
        private readonly ILayoutManager _layoutManager;
        private readonly IOxideRepository<Module> _moduleRepository;
        private readonly IScheduledJobManager _scheduledJobManager;
        private readonly IWidgetManager _widgetManager;

        public ModuleService()
        {
        }

        public ModuleService(IOxideRepository<Module> moduleRepository, IScheduledJobManager scheduledJobManager)
        {
            _moduleRepository = moduleRepository;
            _scheduledJobManager = scheduledJobManager;
            _widgetManager = new WidgetManager();
            _layoutManager = new LayoutManager(this);
        }

        public int CreateModule(Module model)
        {
            _moduleRepository.Insert(model);
            return model.Id;
        }

        public void UpdateModule(Module model)
        {
            var updateMode = _moduleRepository.SearchFor(x => x.Id == model.Id).FirstOrDefault();
            if (updateMode == null) throw new ArgumentNullException(nameof(updateMode));
            updateMode = model.CopyPropertiesTo(model, new[] {"Id"});
            _moduleRepository.Update(updateMode);
        }

        public void DeleteModule(Module model)
        {
            _moduleRepository.Delete(model);
        }

        public void DeleteModule(int id)
        {
            _moduleRepository.Delete(id);
        }

        public Module GetModule(int id)
        {
            return _moduleRepository.GetById(id);
        }

        public Module GetModule(string nameSpace)
        {
            return _moduleRepository.SearchFor(x => x.Namespace == nameSpace).FirstOrDefault();
        }

        public List<Module> GetAllModules()
        {
            return _moduleRepository.GetAll().ToList();
        }

        public List<Module> GetAllActiveModules()
        {
            return _moduleRepository.SearchFor(x => x.Active).ToList();
        }

        public bool IsModuleActive(string nameSpace)
        {
            return nameSpace.Replace(Constants.Dll, Constants.Empty) == Constants.DefaultNameSpace ||
                   _moduleRepository.GetAll()
                                    .Any(
                                        x =>
                                            x.Namespace == nameSpace.Replace(Constants.Dll, Constants.Empty) && x.Active);
        }

        public FilterModel<ModuleViewModel> ModuleList(string filter)
        {
            ICollection<ModuleViewModel> moduleList;
            var moduleFilterViewModel = new FilterModel<ModuleViewModel>();
            if (filter != null)
            {
                if (filter != string.Empty)
                {
                    moduleList =
                        GetViewModulesWithFilter(
                            OxideModuleHelper.GetModuleList()
                                             .Where(
                                                 x =>
                                                     x.Name.ToLower().Contains(filter.ToLower()) ||
                                                     x.Title.ToLower().Contains(filter.ToLower()) ||
                                                     x.Category.ToLower().Contains(filter.ToLower()) ||
                                                     x.Description.ToLower().Contains(filter.ToLower()))
                                             .OrderBy(x => x.Name));
                    moduleFilterViewModel.Records = moduleList;
                }
                else
                {
                    moduleList = GetViewModulesWithFilter(OxideModuleHelper.GetModuleList().OrderBy(x => x.Name));
                    moduleFilterViewModel.Records = moduleList;
                }
            }
            else
            {
                moduleList = GetViewModulesWithFilter(OxideModuleHelper.GetModuleList().OrderBy(x => x.Name));
                moduleFilterViewModel.Records = moduleList;
            }
            return moduleFilterViewModel;
        }

        public bool ChangeStatus(string moduleName, bool status)
        {
            var targetModule = _moduleRepository.SearchFor(x => x.Name == moduleName).FirstOrDefault();
            var module = Oxide.LoadedModules.FirstOrDefault(x => x.Name == moduleName);
            if (targetModule != null)
            {
                targetModule.Active = !status;
                _moduleRepository.Update(targetModule);

                //Run all registerers for the module
                if (targetModule.Active)
                {
                    ReInitalizeModuleCompentents(targetModule.Namespace);
                    AssemblyManager.ModuleActivationList = GetAllModules();
                    AttiributeManager.OrganizeAdminRoles();

                    //Activate Dependencies
                    if (module?.Dependencies != null) ActivateDependencies(module.Dependencies);
                }
                else
                {
                    AssemblyManager.ModuleActivationList = GetAllModules();
                    foreach (var v in AssemblyManager.GetFor<IOxideWidget>(targetModule.Namespace))
                        Oxide.LoadedWidgets.Remove(
                            Oxide.LoadedWidgets.FirstOrDefault(
                                x => x.WidgetKey == OxideWidgetHelper.GetWidgetUniqueKey(v)));
                    foreach (var v in AssemblyManager.GetFor<IOxideLayout>(targetModule.Namespace))
                        Oxide.LoadedLayouts.Remove(v);
                    _scheduledJobManager.UnRegisterScheduledAllJobs(moduleName);
                }
            }
            return !status;
        }

        public bool SetModuleDefault(string moduleName)
        {
            var targetModule = Oxide.LoadedModules.FirstOrDefault(x => x.Name == moduleName);
            if (targetModule != null)
                ChangeStatus(targetModule.Name, false);
            return true;
        }

        public FilterModel<ModuleViewModel> GetModulesWithFilter(int pageSize, int page, string filter)
        {
            var pageFilterViewModel = new FilterModel<ModuleViewModel>();
            if (filter != string.Empty)
                pageFilterViewModel.Records =
                    GetModuleViewModelList(
                        _moduleRepository.GetAll()
                                         .OrderBy(x => x.Name)
                                         .Where(
                                             x =>
                                                 x.Name.ToLower().Contains(filter.ToLower()) ||
                                                 x.Title.ToLower().Contains(filter.ToLower()))
                                         .Skip(pageSize*page)
                                         .Take(pageSize)
                                         .ToList());
            else
                pageFilterViewModel.Records =
                    GetModuleViewModelList(
                        _moduleRepository.GetAll().OrderBy(x => x.Name).Skip(pageSize*page).Take(pageSize).ToList());
            pageFilterViewModel.CurrentPage = page;
            pageFilterViewModel.DisplayingRecords = pageFilterViewModel.Records.Count();
            pageFilterViewModel.TotalRecords = _moduleRepository.GetAll().Count();
            if (filter != string.Empty)
                pageFilterViewModel.TotalPages =
                    (_moduleRepository.GetAll()
                                      .OrderBy(x => x.Name)
                                      .Count(x => x.Name.Contains(filter) || x.Title.Contains(filter)) + pageSize - 1)/
                    pageSize;
            else
                pageFilterViewModel.TotalPages = (pageFilterViewModel.TotalRecords + pageSize - 1)/pageSize;
            pageFilterViewModel.NextEnable = pageFilterViewModel.CurrentPage < pageFilterViewModel.TotalPages - 1;
            pageFilterViewModel.PreviousEnable = pageFilterViewModel.CurrentPage > 0;
            return pageFilterViewModel;
        }

        public ICollection<ModuleViewModel> GetViewModulesWithFilter(IEnumerable<IOxideModule> moduleList)
        {
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            return
                moduleList.Select(
                    module =>
                        new ModuleViewModel
                        {
                            Description = module.Description,
                            Name = module.Name,
                            Title = module.Title,
                            Category = module.Category,
                            PreviewImageUrl = module.PreviewImageUrl,
                            Active = IsModuleActive(module.Name.Replace(" ", Constants.Empty)),
                            InfoUrl = "",
                            Dependencies = module.Dependencies?.Split(Constants.Splitter) ?? new string[] {},
                            ChangeStatusUrl =
                                urlHelper.Action(MVC.Admin.Module.ActionNames.ChangeStatus, MVC.Admin.Module.Name,
                                    new {area = Constants.DefaultArea})
                        }).ToList();
        }

        private void ReInitalizeModuleCompentents(string moduleNamespace)
        {
            foreach (var v in AssemblyManager.GetFor<IOxideMigrationManager>(moduleNamespace))
                v.UpdateDatabase();
            foreach (var v in AssemblyManager.GetFor<IFilterRegisterer>(moduleNamespace))
                v.RegisterFilter(Oxide.Filters);
            foreach (var v in AssemblyManager.GetFor<IRouteRegisterer>(moduleNamespace))
                v.RegisterRoute(Oxide.Routes);
            foreach (var v in AssemblyManager.GetFor<IResourceRegisterer>(moduleNamespace))
                v.RegisterResource(Oxide.BoundleCollection);
            foreach (var instance in
                AssemblyManager.GetFor<IOxideDataBaseModel>(moduleNamespace)
                               .Where(instance => !Oxide.LoadedModels.Contains(instance.GetType())))
                Oxide.LoadedModels.Add(instance.GetType());
            _widgetManager.LoadWidgets(moduleNamespace);
            _layoutManager.LoadLayouts(moduleNamespace);
            _scheduledJobManager.RegisterAllBackgroundJobs(moduleNamespace);
        }

        private static ICollection<ModuleViewModel> GetModuleViewModelList(IEnumerable<Module> recordList)
        {
            return
                (from page in recordList
                 let pageViewModel = new ModuleViewModel()
                 select page.CopyPropertiesTo(pageViewModel)).ToList();
        }

        private void ActivateDependencies(string dependencies)
        {
            var modulesToActivate = dependencies.Split(Constants.Splitter);
            var modules =
                _moduleRepository.SearchFor(x => modulesToActivate.Contains(x.Name) && x.Active == false).ToList();
            foreach (var module in modules)
                ChangeStatus(module.Namespace, false);
        }
    }
}