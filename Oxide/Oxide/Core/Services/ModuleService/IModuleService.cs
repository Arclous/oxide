﻿using System.Collections.Generic;
using Oxide.Core.Base.OxideDependency;
using Oxide.Core.Elements;
using Oxide.Core.Models.Modules;
using Oxide.Core.ViewModels.FilterViewModels;
using Oxide.Core.ViewModels.ModuleViewModels;

namespace Oxide.Core.Services.ModuleService
{
    /// <summary>
    ///     Service which is reponsile for managing modules
    /// </summary>
    public interface IModuleService : IOxideDependency
    {
        /// <summary>
        ///     Creates model according to model
        /// </summary>
        /// <param name="model">Module model</param>
        int CreateModule(Module model);
        /// <summary>
        ///     Updates model according to model
        /// </summary>
        /// <param name="model">Module model</param>
        void UpdateModule(Module model);
        /// <summary>
        ///     Deletes model according to model
        /// </summary>
        /// <param name="model">Module model</param>
        void DeleteModule(Module model);
        /// <summary>
        ///     Deletes model according to id
        /// </summary>
        /// <param name="id">Id of the module</param>
        void DeleteModule(int id);
        /// <summary>
        ///     Gets model according to id
        /// </summary>
        /// <param name="id">Id of the module</param>
        Module GetModule(int id);
        /// <summary>
        ///     Gets module according to namepsace
        /// </summary>
        /// <param name="nameSpace">Namespace of the module</param>
        Module GetModule(string nameSpace);
        /// <summary>
        ///     Gets modules as filter view model
        /// </summary>
        /// <param name="pageSize">Size of the page for each page</param>
        /// <param name="page">Requested page index</param>
        /// <param name="filter">Filter criterias to filtering modules</param>
        FilterModel<ModuleViewModel> GetModulesWithFilter(int pageSize, int page, string filter);
        /// <summary>
        ///     Gets all modules
        /// </summary>
        List<Module> GetAllModules();
        /// <summary>
        ///     Gets all active modules
        /// </summary>
        List<Module> GetAllActiveModules();
        /// <summary>
        ///     Checks if the module is active
        /// </summary>
        /// <param name="nameSpace">Namespace of the module</param>
        bool IsModuleActive(string nameSpace);
        /// <summary>
        ///     Gets modules according to filter as filter view model
        /// </summary>
        /// <param name="filter">Filter to use for filtering module list</param>
        FilterModel<ModuleViewModel> ModuleList(string filter);
        /// <summary>
        ///     Gets modules according to module interface list
        /// </summary>
        /// <param name="moduleList">Interfaces of the module or modules</param>
        ICollection<ModuleViewModel> GetViewModulesWithFilter(IEnumerable<IOxideModule> moduleList);
        /// <summary>
        ///     Changes status of the module according to module name and new status
        /// </summary>
        /// <param name="moduleName">Name of the module</param>
        /// <param name="status">Current status for the module</param>
        bool ChangeStatus(string moduleName, bool status);
        /// <summary>
        ///     Sets status as default for module
        /// </summary>
        /// <param name="moduleName">Name of the module</param>
        bool SetModuleDefault(string moduleName);
    }
}