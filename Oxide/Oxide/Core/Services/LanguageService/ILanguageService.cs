﻿using System.Collections.Generic;
using Oxide.Areas.Admin.Models;
using Oxide.Core.Base.OxideDependency;

namespace Oxide.Core.Services.LanguageService
{
    /// <summary>
    ///     Service for managing languages
    /// </summary>
    public interface ILanguageService : IOxideDependency
    {
        /// <summary>
        ///     Creates language according to model
        /// </summary>
        /// <param name="model">Language model</param>
        void CreateLanguage(Language model);
        /// <summary>
        ///     Updates language according to model
        /// </summary>
        /// <param name="model">Language model</param>
        void UpdateLanguage(Language model);
        /// <summary>
        ///     Deletes language according to model
        /// </summary>
        /// <param name="model">Language model</param>
        void DeleteLanguage(Language model);
        /// <summary>
        ///     Deletes language according to id
        /// </summary>
        /// <param name="id">Id of the language</param>
        void DeleteLanguage(int id);
        /// <summary>
        ///     Gets language according to id
        /// </summary>
        /// <param name="id">Id of the language</param>
        Language GetLanguage(int id);
        /// <summary>
        ///     Deletes language according to code3
        /// </summary>
        /// <param name="code3">Code3 of the language</param>
        Language GetLanguage(string code3);
        /// <summary>
        ///     Gets all languges as list
        /// </summary>
        List<Language> GetAllLanguages();
        /// <summary>
        ///     Gets all active languages as list
        /// </summary>
        List<Language> GetAllActiveLanguages();
        /// <summary>
        ///     Makes all languages deactive
        /// </summary>
        void MakeAllLanguagesDeactive();
        /// <summary>
        ///     Activates languages according to id
        /// </summary>
        /// <param name="id">Id of the language</param>
        void ActivateLanguage(int id);
        /// <summary>
        ///     Deactivates language according to id
        /// </summary>
        /// <param name="id">Id of the language</param>
        void DeactivateLanguage(int id);
        /// <summary>
        ///     Activates according to model
        /// </summary>
        /// <param name="model">Language model</param>
        void ActivateLanguage(Language model);
        /// <summary>
        ///     Deactivates according to model
        /// </summary>
        /// <param name="model">Language model</param>
        void DeactivateLanguage(Language model);
    }
}