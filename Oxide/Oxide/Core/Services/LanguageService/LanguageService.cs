﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System.Collections.Generic;
using System.Linq;
using Oxide.Areas.Admin.Models;
using Oxide.Core.Data.Repository;

namespace Oxide.Core.Services.LanguageService
{
    public class LanguageService : ILanguageService
    {
        private readonly IOxideRepository<Language> _languageRepository;

        public LanguageService(IOxideRepository<Language> languageRepository)
        {
            _languageRepository = languageRepository;
        }

        public void CreateLanguage(Language model)
        {
            _languageRepository.Insert(model);
        }

        public void UpdateLanguage(Language model)
        {
            _languageRepository.Update(model);
        }

        public void DeleteLanguage(Language model)
        {
            _languageRepository.Delete(model);
        }

        public void DeleteLanguage(int id)
        {
            _languageRepository.Delete(id);
        }

        public Language GetLanguage(int id)
        {
            var model = _languageRepository.SearchFor(x => x.Id == id).FirstOrDefault();
            return model;
        }

        public Language GetLanguage(string code3)
        {
            var model = _languageRepository.SearchFor(x => x.Code3 == code3).FirstOrDefault();
            return model;
        }

        public List<Language> GetAllLanguages()
        {
            return _languageRepository.GetAll().ToList();
        }

        public List<Language> GetAllActiveLanguages()
        {
            return _languageRepository.GetAll().Where(x => x.Active).ToList();
        }

        public void MakeAllLanguagesDeactive()
        {
            foreach (var lng in _languageRepository.GetAll().Where(x => x.Active).ToList())
            {
                DeactivateLanguage(lng);
            }
        }

        public void ActivateLanguage(int id)
        {
            var language = _languageRepository.GetById(id);
            language.Active = true;
            _languageRepository.Update(language);
        }

        public void DeactivateLanguage(int id)
        {
            var language = _languageRepository.GetById(id);
            language.Active = false;
            _languageRepository.Update(language);
        }

        public void ActivateLanguage(Language model)
        {
            model.Active = true;
            _languageRepository.Update(model);
        }

        public void DeactivateLanguage(Language model)
        {
            model.Active = false;
            _languageRepository.Update(model);
        }
    }
}