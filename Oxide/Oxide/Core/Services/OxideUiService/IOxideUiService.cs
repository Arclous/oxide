﻿using System.Collections.Generic;
using Oxide.Areas.Admin.ViewModels;
using Oxide.Core.Base.OxideDependency;

namespace Oxide.Core.Services.OxideUiService
{
    /// <summary>
    ///     Service which is providing contents dynamically according to types
    /// </summary>
    public interface IOxideUiService : IOxideDependency
    {
        /// <summary>
        ///     Gets content according to given criterias as enumerable
        /// </summary>
        /// <param name="contentName">Name of the content which will be fetched</param>
        /// <param name="filter">Filter criteria which will be using during fetching</param>
        /// <param name="filterCount">Number of the total result</param>
        IEnumerable<object> GetContents(string contentName, string filter, int filterCount);
        /// <summary>
        ///     Gets media content according to given criterias as enumerable
        /// </summary>
        /// <param name="page">Requested page number</param>
        /// <param name="directoryId">Id of directory</param>
        /// <param name="pageSize">Number of the total result for each page</param>
        FilterContentViewModelHolder GetMediaContents(int pageSize, int page, int directoryId);
        /// <summary>
        ///     Gets media content according to given criterias as enumerable
        /// </summary>
        /// <param name="contentName">Name of the content which will be fetched</param>
        /// <param name="page">Requested page number</param>
        /// <param name="filter">Filter criteria which will be using during fetching</param>
        /// <param name="pageSize">Number of the total result for each page</param>
        /// <param name="withPicture">If the picture will be shown</param>
        /// <param name="pictureField">Field name of the picture id</param>
        FilterContentViewModelHolder GetContents(string contentName, int pageSize, int page, string filter,
            bool withPicture = false, string pictureField = null);
        /// <summary>
        ///     Gets  content with image according to given criterias as enumerable
        /// </summary>
        /// <param name="contentName">Name of the content which will be fetched</param>
        /// <param name="filter">Filter criteria which will be using during fetching</param>
        /// <param name="filterCount">Number of the total result</param>
        /// <param name="imageField">Field name of the picture id</param>
        IEnumerable<object> GetContentsWithImage(string contentName, string filter, int filterCount, string imageField);
        /// <summary>
        ///     Gets content according to given criterias as enumerable
        /// </summary>
        /// <param name="mimeType">Mime type of the content which will be fetched</param>
        /// <param name="filter">Filter criteria which will be using during fetching</param>
        /// <param name="filterCount">Number of the total result</param>
        IEnumerable<object> GetFileContents(string mimeType, string filter, int filterCount);
        /// <summary>
        ///     Gets content according to given criterias and values as enumerable
        /// </summary>
        /// <param name="contentName">Name of the content which will be fetched</param>
        /// <param name="values">Values which will be using during fetching</param>
        IEnumerable<object> GetContents(string contentName, string values);
        /// <summary>
        ///     Gets media content according to given criterias and values as enumerable
        /// </summary>
        /// <param name="values">Values which will be using during fetching</param>
        IEnumerable<object> GetMediaContents(string values);
        /// <summary>
        ///     Gets content according to given criterias and values as object
        /// </summary>
        /// <param name="contentName">Name of the content which will be fetched</param>
        /// <param name="value">Value which will be using during fetching</param>
        object GetContent(string contentName, string value);
        /// <summary>
        ///     Gets urls of the content according id, content should be 'File Content'
        /// </summary>
        /// <param name="id">Id of the content</param>
        /// <param name="urlType">Type of the url</param>
        string GetImageContentUrl(string id, Syntax.Syntax.UrlType urlType = Syntax.Syntax.UrlType.Relative);
        /// <summary>
        ///     Gets urls of the content according ids, contents should be 'File Content'
        /// </summary>
        /// <param name="values">Ids of the contents</param>
        /// <param name="urlType">Type of the url</param>
        IEnumerable<object> GetImageContentsUrl(string values,
            Syntax.Syntax.UrlType urlType = Syntax.Syntax.UrlType.Relative);
        /// <summary>
        ///     Gets urls of the content according id, content should be 'Page'
        /// </summary>
        /// <param name="id">Id of the content</param>
        /// <param name="urlType">Type of the url</param>
        string GetPageContentUrl(string id, Syntax.Syntax.UrlType urlType = Syntax.Syntax.UrlType.Relative);
        /// <summary>
        ///     Gets urls of the content according ids, content should be 'Page'
        /// </summary>
        /// <param name="values">Ids of the content</param>
        /// <param name="urlType">Type of the url</param>
        IEnumerable<object> GetPageContentsUrl(string values,
            Syntax.Syntax.UrlType urlType = Syntax.Syntax.UrlType.Relative);
    }
}