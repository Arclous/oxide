﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Castle.Components.DictionaryAdapter;
using Oxide.Areas.Admin.ViewModels;
using Oxide.Core.Data.Context;
using Oxide.Core.Data.Repository;
using Oxide.Core.Helpers;
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.Syntax;

namespace Oxide.Core.Services.OxideUiService
{
    //#Todo needs to create common getcontroller context function
    public class OxideUiService : IOxideUiService
    {
        private readonly IProviderManager _providerManager;

        public OxideUiService(IProviderManager providerManager)
        {
            _providerManager = providerManager;
        }

        public IEnumerable<object> GetContents(string contentName, string filter, int filterCount)
        {
            IEnumerable<object> contentList;
            var contentType = Oxide.LoadedModels.FirstOrDefault(x => x.Name == contentName);
            if (contentType == null)
                throw new ArgumentException("Cannot find the contet type " + contentName);
            var type = typeof (OxideRepository<>).MakeGenericType(contentType);
            dynamic repository = Activator.CreateInstance(type);
            var dbContextProperty = type.GetProperty(OxideSystem.DbContext);
            dbContextProperty?.SetValue(repository, new OxideContext());
            if (filter != null)
            {
                if (filter != string.Empty)
                {
                    contentList =
                        ((IEnumerable) repository.Table).Cast<dynamic>()
                                                        .Where(x => x.Name.ToLower().Contains(filter.ToLower()))
                                                        .OrderBy(x => x.Name)
                                                        .Take(filterCount);
                }
                else
                {
                    contentList =
                        ((IEnumerable) repository.Table).Cast<dynamic>()
                                                        .Where(x => x.Name.ToLower().Contains(filter.ToLower()))
                                                        .OrderBy(x => x.Name)
                                                        .Take(filterCount);
                }
            }
            else
            {
                return null;
            }
            return contentList;
        }

        public FilterContentViewModelHolder GetMediaContents(int pageSize, int page, int directoryId)
        {
            var resultPackage = new FilterContentViewModelHolder();
            var contentType = Oxide.LoadedModels.FirstOrDefault(x => x.Name == Constants.FileContent);
            if (contentType == null)
                throw new ArgumentException("Cannot find the contet type " + Constants.FileContent);
            var type = typeof (OxideRepository<>).MakeGenericType(contentType);
            dynamic repository = Activator.CreateInstance(type);
            var dbContextProperty = type.GetProperty(OxideSystem.DbContext);
            dbContextProperty?.SetValue(repository, new OxideContext());
            IEnumerable<object> contentList =
                ((IEnumerable) repository.Table).Cast<dynamic>()
                                                .Where(x => (x.FileType == 0) && (x.DirectoryId == directoryId))
                                                .OrderBy(x => x.Name)
                                                .Skip(pageSize*page)
                                                .Take(pageSize)
                                                .ToList();
            resultPackage.Records = contentList.ToList();
            resultPackage.CurrentPage = page;
            resultPackage.DisplayingRecords = contentList.Count();
            resultPackage.TotalRecords =
                ((IEnumerable) repository.Table).Cast<dynamic>()
                                                .Count(x => (x.FileType == 0) && (x.DirectoryId == directoryId));
            resultPackage.TotalPages = (resultPackage.TotalRecords + pageSize - 1)/pageSize;
            resultPackage.NextEnable = resultPackage.CurrentPage < resultPackage.TotalPages - 1;
            resultPackage.PreviousEnable = resultPackage.CurrentPage > 0;
            return resultPackage;
        }

        public IEnumerable<object> GetFileContents(string mimeType, string filter, int filterCount)
        {
            IEnumerable<object> contentList;
            var contentType = Oxide.LoadedModels.FirstOrDefault(x => x.Name == Constants.FileContent);
            var mimeTypes = mimeType.Split(Constants.Splitter).ToList();
            var type = typeof (OxideRepository<>).MakeGenericType(contentType);
            dynamic repository = Activator.CreateInstance(type);
            var dbContextProperty = type.GetProperty(OxideSystem.DbContext);
            if (contentType != null)
            {
                dbContextProperty?.SetValue(repository, new OxideContext());
            }
            else
                throw new ArgumentException("Cannot find the contet type " + Constants.FileContent);
            if (filter != null)
            {
                if (filter != string.Empty)
                {
                    contentList =
                        ((IEnumerable) repository.Table).Cast<dynamic>()
                                                        .Where(
                                                            x =>
                                                                (x.Name.ToLower().Contains(filter.ToLower())) &&
                                                                (mimeTypes.Contains(x.MimeType)))
                                                        .OrderBy(x => x.Name)
                                                        .Take(filterCount);
                }
                else
                {
                    contentList =
                        ((IEnumerable) repository.Table).Cast<dynamic>()
                                                        .Where(
                                                            x =>
                                                                (x.Name.ToLower().Contains(filter.ToLower())) &&
                                                                (mimeTypes.Contains(x.MimeType)))
                                                        .OrderBy(x => x.Name)
                                                        .Take(filterCount);
                }
            }
            else
            {
                return null;
            }
            return contentList;
        }

        public IEnumerable<object> GetContents(string contentName, string values)
        {
            var contentType = Oxide.LoadedModels.FirstOrDefault(x => x.Name == contentName);
            if (contentType == null)
                throw new ArgumentException("Cannot find the contet type " + contentName);
            var type = typeof (OxideRepository<>).MakeGenericType(contentType);
            dynamic repository = Activator.CreateInstance(type);
            var dbContextProperty = type.GetProperty(OxideSystem.DbContext);
            dbContextProperty?.SetValue(repository, new OxideContext());
            var items = values.Split(Constants.Splitter).Select(x => Convert.ToInt32(x)).ToList();
            IEnumerable<object> contentList =
                ((IEnumerable) repository.Table).Cast<dynamic>().Where(x => Enumerable.Contains(items, x.Id));
            return contentList.OrderBy(d => items.IndexOf(((dynamic) d).Id));
        }

        public FilterContentViewModelHolder GetContents(string contentName, int pageSize, int page, string filter,
            bool withPicture = false, string pictureField = null)
        {
            var resultPackage = new FilterContentViewModelHolder();
            var contentType = Oxide.LoadedModels.FirstOrDefault(x => x.Name == contentName);
            if (contentType == null)
                throw new ArgumentException("Cannot find the contet type " + contentName);
            var type = typeof (OxideRepository<>).MakeGenericType(contentType);
            dynamic repository = Activator.CreateInstance(type);
            var dbContextProperty = type.GetProperty(OxideSystem.DbContext);
            dbContextProperty?.SetValue(repository, new OxideContext());
            IEnumerable<object> contentList =
                ((IEnumerable) repository.Table).Cast<dynamic>()
                                                .Where(
                                                    x =>
                                                        x.Name != null
                                                            ? x.Name.ToLower().Contains(filter.ToLower())
                                                            : x.Id > 0)
                                                .OrderBy(x => x.Name)
                                                .Skip(pageSize*page)
                                                .Take(pageSize)
                                                .ToList();
            if (withPicture)
            {
                var newContentList = new List<object>();
                var oxideUiService = _providerManager.Provide<IOxideUiService>();
                foreach (
                    var field in contentType.GetProperties().Where(x => x.CanRead && x.Name == pictureField).ToList())
                {
                    foreach (var v in contentList)
                    {
                        var content = oxideUiService.GetContent(Constants.FileContent, field.GetValue(v).ToString());
                        if (content == null) continue;
                        var newObject = DynamicHelper.ConvertToExpando(v);
                        DynamicHelper.AddProperty(newObject, Constants.ExpandoImageField, ((dynamic) content).Path);
                        newContentList.Add(newObject);
                    }
                }
                resultPackage.Records = newContentList;
            }
            else
            {
                resultPackage.Records = new List<object>();
                foreach (var newObject in contentList.Select(DynamicHelper.ConvertToExpando))
                {
                    resultPackage.Records.Add(newObject);
                }
            }
            resultPackage.CurrentPage = page;
            resultPackage.DisplayingRecords = contentList.Count();
            resultPackage.TotalRecords =
                ((IEnumerable) repository.Table).Cast<dynamic>()
                                                .Count(
                                                    x =>
                                                        x.Name != null
                                                            ? x.Name.ToLower().Contains(filter.ToLower())
                                                            : x.Id > 0);
            resultPackage.TotalPages = (resultPackage.TotalRecords + pageSize - 1)/pageSize;
            resultPackage.NextEnable = resultPackage.CurrentPage < resultPackage.TotalPages - 1;
            resultPackage.PreviousEnable = resultPackage.CurrentPage > 0;
            return resultPackage;
        }

        public IEnumerable<object> GetContentsWithImage(string contentName, string filter, int filterCount,
            string imageField)
        {
            var resultPackage = new FilterContentViewModelHolder();
            var contentType = Oxide.LoadedModels.FirstOrDefault(x => x.Name == contentName);
            if (contentType == null)
                throw new ArgumentException("Cannot find the contet type " + contentName);
            var type = typeof (OxideRepository<>).MakeGenericType(contentType);
            dynamic repository = Activator.CreateInstance(type);
            var dbContextProperty = type.GetProperty(OxideSystem.DbContext);
            dbContextProperty?.SetValue(repository, new OxideContext());
            IEnumerable<object> contentList =
                ((IEnumerable) repository.Table).Cast<dynamic>()
                                                .Where(
                                                    x =>
                                                        x.Name != null
                                                            ? x.Name.ToLower().Contains(filter.ToLower())
                                                            : x.Id > 0)
                                                .OrderBy(x => x.Name)
                                                .Take(filterCount)
                                                .ToList();
            var newContentList = new List<object>();
            var oxideUiService = _providerManager.Provide<IOxideUiService>();
            foreach (var field in contentType.GetProperties().Where(x => x.CanRead && x.Name == imageField).ToList())
            {
                foreach (var v in contentList)
                {
                    var content = oxideUiService.GetContent(Constants.FileContent, field.GetValue(v).ToString());
                    var newObject = DynamicHelper.ConvertToExpando(v);
                    DynamicHelper.AddProperty(newObject, Constants.ExpandoImageField,
                        content != null ? ((dynamic) content).Path : string.Empty);
                    newContentList.Add(newObject);
                }
            }
            resultPackage.Records = newContentList;
            return newContentList;
        }

        public IEnumerable<object> GetMediaContents(string values)
        {
            var contentType = Oxide.LoadedModels.FirstOrDefault(x => x.Name == Constants.FileContent);
            if (contentType == null)
                throw new ArgumentException("Cannot find the contet type " + Constants.FileContent);
            var type = typeof (OxideRepository<>).MakeGenericType(contentType);
            dynamic repository = Activator.CreateInstance(type);
            var dbContextProperty = type.GetProperty(OxideSystem.DbContext);
            dbContextProperty?.SetValue(repository, new OxideContext());
            var items = values.Split(Constants.Splitter).Select(x => Convert.ToInt32(x)).ToList();
            IEnumerable<object> contentList =
                ((IEnumerable) repository.Table).Cast<dynamic>().Where(x => Enumerable.Contains(items, x.Id));
            return contentList.OrderBy(d => items.IndexOf(((dynamic) d).Id));
        }

        public object GetContent(string contentName, string value)
        {
            if (value == "") return null;
            var contentType = Oxide.LoadedModels.FirstOrDefault(x => x.Name == contentName);
            if (contentType == null)
                throw new ArgumentException("Cannot find the contet type " + Constants.FileContent);
            var type = typeof (OxideRepository<>).MakeGenericType(contentType);
            dynamic repository = Activator.CreateInstance(type);
            var dbContextProperty = type.GetProperty(OxideSystem.DbContext);
            dbContextProperty?.SetValue(repository, new OxideContext());
            object content =
                ((IEnumerable) repository.Table).Cast<dynamic>()
                                                .Where(x => x.Id == Convert.ToInt32(value))
                                                .OrderBy(x => x.Name)
                                                .FirstOrDefault();
            return content;
        }

        public string GetImageContentUrl(string id, Syntax.Syntax.UrlType urlType = Syntax.Syntax.UrlType.Relative)
        {
            var content = GetContent(Constants.FileContent, id);
            switch (urlType)
            {
                case Syntax.Syntax.UrlType.Absolute:
                    return content != null ? OxideUtils.GetFullUrl(((dynamic) content).Path) : null;
                case Syntax.Syntax.UrlType.Relative:
                    return content != null ? ((dynamic) content).Path : null;
            }
            return null;
        }

        public IEnumerable<object> GetImageContentsUrl(string values,
            Syntax.Syntax.UrlType urlType = Syntax.Syntax.UrlType.Relative)
        {
            List<string> urlsList = new EditableList<string>();
            var contents = GetContents(Constants.FileContent, values);
            foreach (var content in contents)
            {
                switch (urlType)
                {
                    case Syntax.Syntax.UrlType.Absolute:
                        urlsList.Add(content != null ? OxideUtils.GetFullUrl(((dynamic) content).Path) : null);
                        break;
                    case Syntax.Syntax.UrlType.Relative:
                        urlsList.Add(content != null ? ((dynamic) content).Path : null);
                        break;
                }
            }
            return urlsList;
        }

        public string GetPageContentUrl(string id, Syntax.Syntax.UrlType urlType = Syntax.Syntax.UrlType.Relative)
        {
            var content = GetContent(Constants.PageContent, id);
            switch (urlType)
            {
                case Syntax.Syntax.UrlType.Absolute:
                    return content != null ? OxideUtils.GetFullUrl(((dynamic) content).Url) : null;
                case Syntax.Syntax.UrlType.Relative:
                    return content != null ? ((dynamic) content).Url : null;
            }
            return null;
        }

        public IEnumerable<object> GetPageContentsUrl(string values,
            Syntax.Syntax.UrlType urlType = Syntax.Syntax.UrlType.Relative)
        {
            List<string> urlsList = new EditableList<string>();
            var contents = GetContents(Constants.PageContent, values);
            foreach (var content in contents)
            {
                switch (urlType)
                {
                    case Syntax.Syntax.UrlType.Absolute:
                        urlsList.Add(content != null ? OxideUtils.GetFullUrl(((dynamic) content).Url) : null);
                        break;
                    case Syntax.Syntax.UrlType.Relative:
                        urlsList.Add(content != null ? ((dynamic) content).Url : null);
                        break;
                }
            }
            return urlsList;
        }
    }
}