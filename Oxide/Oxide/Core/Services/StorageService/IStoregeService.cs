﻿using System.Collections.Generic;
using System.IO;
using Oxide.Core.Base.OxideDependency;
using Oxide.Core.Models.Base;

namespace Oxide.Core.Services.StorageService
{
    /// <summary>
    ///     Responsible service for disk actions for both directories and files.
    /// </summary>
    /// <remarks>
    ///     Add more details here.
    /// </remarks>
    public interface IStoregeService : IOxideDependency
    {
        /// <summary>
        ///     Create new directory on disk
        /// </summary>
        /// <param name="path">Path of the directory</param>
        bool CreateDirectory(string path);
        /// <summary>
        ///     Deletes directory on disc
        /// </summary>
        /// <param name="path">Path of the directory</param>
        bool DeleteDirectory(string path);
        /// <summary>
        ///     Renames directory on disc
        /// </summary>
        /// <param name="path">Path of the directory</param>
        /// <param name="targetPath">New path of the directory</param>
        bool RenameDirectory(string path, string targetPath);
        /// <summary>
        ///     Gets file info
        /// </summary>
        /// <param name="path">Path of the file</param>
        FileInfo GetFileInfo(string path);
        /// <summary>
        ///     Renames file on disc
        /// </summary>
        /// <param name="name">New name for file</param>
        /// <param name="path">Path of the file</param>
        bool RenameFile(string name, string path);
        /// <summary>
        ///     Deletes files on disc
        /// </summary>
        /// <param name="path">Path of the file</param>
        bool DeleteFile(string path);
        /// <summary>
        ///     Copy files on disc
        /// </summary>
        /// <param name="path">Path of the file</param>
        /// <param name="targetPath">Targe path of the file</param>
        bool CopyFile(string path, string targetPath);
        /// <summary>
        ///     Move files on disc
        /// </summary>
        /// <param name="path">Path of the file</param>
        /// <param name="targetPath">Targe path of the file</param>
        bool MoveFile(string path, string targetPath);
        /// <summary>
        ///     Checks if file exist on disc
        /// </summary>
        /// <param name="path">Path of the file</param>
        bool IsFileExist(string path);
        /// <summary>
        ///     Create archive file on disc
        /// </summary>
        /// <param name="fileNames">List of files which will be archived</param>
        /// <param name="archiveFileName">Filename of the archive name</param>
        bool CreateArchiveFile(List<string> fileNames, string archiveFileName);
        /// <summary>
        ///     Extracts archive file on disc
        /// </summary>
        /// <param name="archiveFileName">Filename of the archive name</param>
        /// <param name="archiveName">Name for the archive file</param>
        /// <param name="targetDirectory">Director which archive file will be extractted</param>
        Response ExtractArchiveFile(string archiveFileName, string archiveName, string targetDirectory);
    }
}