﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using Ionic.Zip;
using Oxide.Core.Models.Base;
using Oxide.Core.Syntax;

namespace Oxide.Core.Services.StorageService
{
    public class StorageService : IStoregeService
    {
        public bool CreateDirectory(string path)
        {
            try
            {
                var pyschicalPath = HttpContext.Current.Server.MapPath(path);
                if (!Directory.Exists(pyschicalPath))
                    Directory.CreateDirectory(pyschicalPath);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteDirectory(string path)
        {
            try
            {
                var pyschicalPath = HttpContext.Current.Server.MapPath(path);
                if (Directory.Exists(pyschicalPath))
                {
                    DeleteAll(pyschicalPath);
                    Directory.Delete(pyschicalPath);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool RenameDirectory(string path, string targetPath)
        {
            try
            {
                Directory.Move(HttpContext.Current.Server.MapPath(path), HttpContext.Current.Server.MapPath(targetPath));
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public FileInfo GetFileInfo(string path)
        {
            return path.StartsWith(Constants.VirtualStart)
                ? new FileInfo(HttpContext.Current.Server.MapPath(path))
                : new FileInfo(path);
        }

        public bool RenameFile(string name, string path)
        {
            try
            {
                var fileCurentName = new FileInfo(path).Name;
                var targetDest = Path.Combine(path.Replace(fileCurentName, Constants.Empty), name);
                File.Move(HttpContext.Current.Server.MapPath(path), HttpContext.Current.Server.MapPath(targetDest));
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteFile(string path)
        {
            try
            {
                var pyschicalPath = HttpContext.Current.Server.MapPath(path);
                if (File.Exists(pyschicalPath))
                    File.Delete(pyschicalPath);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool CopyFile(string path, string targetPath)
        {
            try
            {
                var pyschicalPath = HttpContext.Current.Server.MapPath(path);
                var pyschicalTargetPath = HttpContext.Current.Server.MapPath(targetPath);
                if (File.Exists(pyschicalPath))
                {
                    File.Copy(pyschicalPath, pyschicalTargetPath);
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool MoveFile(string path, string targetPath)
        {
            try
            {
                var pyschicalPath = HttpContext.Current.Server.MapPath(path);
                var pyschicalTargetPath = HttpContext.Current.Server.MapPath(targetPath);
                if (File.Exists(pyschicalPath))
                {
                    File.Move(pyschicalPath, pyschicalTargetPath);
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool IsFileExist(string path)
        {
            return File.Exists(path.StartsWith(Constants.VirtualStart) ? HttpContext.Current.Server.MapPath(path) : path);
        }

        public bool CreateArchiveFile(List<string> fileNames, string archiveFileName)
        {
            try
            {
                using (var zip = new ZipFile())
                {
                    foreach (var file in fileNames)
                    {
                        zip.AddFile(HttpContext.Current.Server.MapPath(file), Constants.Empty);
                    }
                    zip.Save(HttpContext.Current.Server.MapPath(archiveFileName));
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public Response ExtractArchiveFile(string archiveFileName, string archiveName, string targetDirectory)
        {
            var response = new Response();
            var filesExtractted = new List<string>();
            try
            {
                var zipFile = HttpContext.Current.Server.MapPath(archiveFileName);
                var targetDirectoryPath = HttpContext.Current.Server.MapPath(targetDirectory);
                using (var zip1 = ZipFile.Read(zipFile))
                {
                    foreach (var e in zip1)
                    {
                        var newFilePath = Path.Combine(targetDirectory, e.FileName);
                        newFilePath = HttpContext.Current.Server.MapPath(newFilePath);
                        if (!File.Exists(newFilePath))
                            filesExtractted.Add(newFilePath);
                        e.Extract(targetDirectoryPath, ExtractExistingFileAction.OverwriteSilently);
                    }

           
                }
                response.Success = true;
                response.Data = filesExtractted;
                return response;
            }
            catch (Exception)
            {
                response.Success = false;
                return response;
            }
        }

        /// <summary>
        ///     Deletes directory and its sub directories
        /// </summary>
        /// <param name="path">Path of the directory</param>
        private static void DeleteAll(string path)
        {
            foreach (var file in Directory.GetFiles(path))
                File.Delete(file);
            foreach (var subdirectory in Directory.GetDirectories(path))
            {
                DeleteAll(subdirectory);
                Directory.Delete(subdirectory);
            }
        }
    }
}