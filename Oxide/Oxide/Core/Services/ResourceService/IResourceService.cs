﻿using System.Collections.Generic;
using Oxide.Core.Base.OxideDependency;

namespace Oxide.Core.Services.ResourceService
{
    /// <summary>
    ///     Responsible service for resources
    /// </summary>
    public interface IResourceService : IOxideDependency
    {
        /// <summary>
        ///     Gets resource according to resource name and resource key
        /// </summary>
        /// <param name="resourceName">Resource name</param>
        /// <param name="resourceKey">Resource key</param>
        string GetText(string resourceName, string resourceKey);
        /// <summary>
        ///     Gets resource according to resource key
        /// </summary>
        /// <param name="resourceKey">Resource key</param>
        string GetText(string resourceKey);
        /// <summary>
        ///     Gets resource according to name
        /// </summary>
        /// <param name="resourceName">Resource name</param>
        Dictionary<string, Dictionary<string, string>> GetRessourcesByName(string resourceName);
    }
}