﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oxide.Core.Managers.AreaManager;
using Oxide.Core.Managers.CultureManager;
using Oxide.Core.Managers.ResourceManager;
using Oxide.Core.Syntax;

namespace Oxide.Core.Services.ResourceService
{
    public class ResourceService : IResourceService
    {
        private readonly ICultureManager _cultureManager;

        public ResourceService(ICultureManager cultureManager)
        {
            _cultureManager = cultureManager;
        }

        public string GetText(string resourceName, string resourceKey)
        {
            try
            {
                var activeArea = AreaManager.GetActiveArea();
                var language = _cultureManager.GetFrontCurrentCulture().ThreeLetterISOLanguageName.ToUpper();
                if (activeArea == RequestType.Admin)
                    language = AreaManager.GetAdminUserLanguageCode();
                var translationValue =
                    ResourceManager.Resources.FirstOrDefault(x => x.Key == resourceKey)
                                   .Value.FirstOrDefault(x => x.Key == resourceName)
                                   .Value.FirstOrDefault(p => p.Key == language)
                                   .Value;
                return translationValue != string.Empty ? translationValue : resourceName;
            }
            catch
            {
                return resourceName;
            }
        }

        public string GetText(string resourceKey)
        {
            try
            {
                var activeArea = AreaManager.GetActiveArea();
                var language = _cultureManager.GetFrontCurrentCulture().ThreeLetterISOLanguageName.ToUpper();
                if (activeArea == RequestType.Admin)
                    language = AreaManager.GetAdminUserLanguageCode();
                var area =
                    HttpContext.Current.Request.RequestContext.RouteData.DataTokens[Constants.ResourceAreaKey].ToString();
                var translationValue =
                    ResourceManager.Resources.FirstOrDefault(x => x.Key == area)
                                   .Value.FirstOrDefault(x => x.Key == resourceKey)
                                   .Value.FirstOrDefault(p => p.Key == language)
                                   .Value;
                return translationValue != string.Empty ? translationValue : resourceKey;
            }
            catch
            {
                return resourceKey;
            }
        }

        public Dictionary<string, Dictionary<string, string>> GetRessourcesByName(string resourceName)
        {
            try
            {
                return ResourceManager.Resources.FirstOrDefault(x => x.Key == resourceName).Value;
            }
            catch
            {
                return null;
            }
        }
    }
}