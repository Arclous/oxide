﻿using Oxide.Core.Base.OxideDependency;

namespace Oxide.Core.Services.Robot
{
    /// <summary>
    ///     Responsible service for robot txt
    /// </summary>
    public interface IRobotService : IOxideDependency
    {
        /// <summary>
        ///     Gets robot value
        /// </summary>
        string GetRobotValue();
        /// <summary>
        ///     Saves robot value
        /// </summary>
        void SaveRobotValue(string value);
    }
}