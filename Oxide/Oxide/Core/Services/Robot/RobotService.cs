﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System.Linq;
using Oxide.Core.Data.Repository;

namespace Oxide.Core.Services.Robot
{
    public class RobotService : IRobotService
    {
        private readonly IOxideRepository<Models.Robot.Robot> _robotRepository;

        public RobotService(IOxideRepository<Models.Robot.Robot> robotRepository)
        {
            _robotRepository = robotRepository;
        }

        public string GetRobotValue()
        {
            var firstOrDefault = _robotRepository.GetAll().FirstOrDefault();
            return firstOrDefault != null ? firstOrDefault.Value : string.Empty;
        }

        public void SaveRobotValue(string value)
        {
            _robotRepository.DeleteAll();
            _robotRepository.Insert(new Models.Robot.Robot {Value = value});
        }
    }
}