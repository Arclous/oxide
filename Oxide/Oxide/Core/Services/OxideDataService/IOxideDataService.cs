﻿using Oxide.Core.Base.OxideDependency;

namespace Oxide.Core.Services.OxideDataService
{
    /// <summary>
    ///     Oxide data service
    /// </summary>
    public interface IOxideDataService : IOxideDependency
    {
        /// <summary>
        ///     Checks according to key if update required or not
        /// </summary>
        /// <param name="key">Key to check</param>
        bool IsUpdateRequired(string key);
        /// <summary>
        ///     Writes updates key
        /// </summary>
        /// <param name="key">Key to check</param>
        void WriteUpdateItem(string key);
        /// <summary>
        ///     Installs defaults cultures
        /// </summary>
        void InstallCultures();
        /// <summary>
        ///     Installs defaults time zones
        /// </summary>
        void InstallTimeZones();
        /// <summary>
        ///     Installs site defaults
        /// </summary>
        void InstallSiteDefaults();
        /// <summary>
        ///     Installs default user and roles
        /// </summary>
        void InstallDefaultUserAndRoles();
        /// <summary>
        ///     Installs default modules
        /// </summary>
        void InstallDefaultModules();
        /// <summary>
        ///     Sets default theme
        /// </summary>
        void SetDefaultTheme();
        /// <summary>
        ///     Creates default pages
        /// </summary>
        void CreateDefaultPages();
        /// <summary>
        ///     Checks if mode is initilization
        /// </summary>
        bool IsInitilizationMode();
        /// <summary>
        ///     Completes initilization
        /// </summary>
        void CompleteInitilization();
    }
}