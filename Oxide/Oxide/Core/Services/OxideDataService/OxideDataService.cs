﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Globalization;
using System.Linq;
using Castle.Components.DictionaryAdapter;
using Microsoft.Ajax.Utilities;
using Oxide.Areas.Admin.Models;
using Oxide.Core.Data.Repository;
using Oxide.Core.Helpers;
using Oxide.Core.Models.AdminUser;
using Oxide.Core.Services.ModuleService;
using Oxide.Core.Services.PageService;
using Oxide.Core.Services.SiteSettings;
using Oxide.Core.Services.ThemeService;
using Oxide.Core.Syntax;
using Oxide.Core.ViewModels.PageViewModels;

namespace Oxide.Core.Services.OxideDataService
{
    public class OxideDataService : IOxideDataService
    {
        private readonly IOxideRepository<AdminUser> _adminUserRepository;
        private readonly IOxideRepository<Language> _languageRepository;
        private readonly IModuleService _moduleService;
        private readonly IPageService _pageService;
        private readonly ISiteSettingsService _siteSettings;
        private readonly IOxideRepository<Areas.Admin.Models.SiteSettings> _siteSettingsRepository;
        private readonly IThemeService _themeService;
        private readonly IOxideRepository<TimeZoneItem> _timeZoneItemRepository;
        private readonly IOxideRepository<UpdateItem> _updateRepository;

        public OxideDataService()
        {
        }

        public OxideDataService(IOxideRepository<UpdateItem> updateRepository,
            IOxideRepository<Language> languageRepository, IOxideRepository<TimeZoneItem> timeZoneItemRepository,
            IOxideRepository<Areas.Admin.Models.SiteSettings> siteSettingsRepository,
            IOxideRepository<AdminUser> adminUserRepository, IModuleService moduleService, IThemeService themeService,
            IPageService pageService, ISiteSettingsService siteSettings)
        {
            _updateRepository = updateRepository;
            _languageRepository = languageRepository;
            _timeZoneItemRepository = timeZoneItemRepository;
            _siteSettingsRepository = siteSettingsRepository;
            _adminUserRepository = adminUserRepository;
            _moduleService = moduleService;
            _themeService = themeService;
            _pageService = pageService;
            _siteSettings = siteSettings;
        }

        public bool IsUpdateRequired(string key)
        {
            var obj = _updateRepository.Get().FirstOrDefault(x => x.Key == key);
            return obj == null;
        }

        public void WriteUpdateItem(string key)
        {
            var updateItem = new UpdateItem {Name = key, Key = key, UpdateDateTime = DateTime.Today};
            _updateRepository.Insert(updateItem);
        }

        public void InstallCultures()
        {
            if (!IsUpdateRequired("Languages")) return;
            var cultureInfos = CultureInfo.GetCultures(CultureTypes.NeutralCultures).DistinctBy(x => x.Name);
            foreach (var newLaguage in
                cultureInfos.Select(
                    culture =>
                        new Language
                        {
                            Name = culture.Name,
                            Title = culture.EnglishName,
                            Code = culture.TwoLetterISOLanguageName,
                            Code3 = culture.ThreeLetterISOLanguageName,
                            Native = culture.NativeName
                        }))
            {
                _languageRepository.Insert(newLaguage);
            }
            WriteUpdateItem("Languages");
        }

        public void InstallTimeZones()
        {
            if (!IsUpdateRequired("TimeZones")) return;
            var timeZones = TimeZoneInfo.GetSystemTimeZones();
            foreach (var timeZone in from v in timeZones
                                     where v.DisplayName != string.Empty
                                     select
                                         new TimeZoneItem
                                         {
                                             Name = v.DisplayName,
                                             TimeZoneId = v.Id,
                                             Offset = v.BaseUtcOffset.TotalMilliseconds,
                                             StandardName = v.StandardName
                                         })
            {
                _timeZoneItemRepository.Insert(timeZone);
            }
            WriteUpdateItem("TimeZones");
        }

        public void InstallSiteDefaults()
        {
            if (!IsUpdateRequired("SiteDefaults")) return;
            var defaultLanguage =
                _languageRepository.Get(x => x.Code3.ToLower() == Constants.AdminDefaultLanguageCode).FirstOrDefault();
            if (defaultLanguage != null)
            {
                defaultLanguage.Active = true;
                _languageRepository.Update(defaultLanguage);
                var defaultTimeZone =
                    _timeZoneItemRepository.Table.FirstOrDefault(
                        x => x.TimeZoneId.ToLower() == Constants.DefaultTimeZoneCode);
                Areas.Admin.Models.SiteSettings defaultSettings = new Areas.Admin.Models.SiteSettings
                {
                    DefaultSiteCulture = defaultLanguage,
                    DefaultTimeZone = defaultTimeZone,
                    Name = Constants.DefaultSiteName,
                    Title = Constants.DefaultSiteName
                };
                _siteSettingsRepository.Insert(defaultSettings);
            }
            WriteUpdateItem("SiteDefaults");
        }

        public void InstallDefaultUserAndRoles()
        {
            if (!IsUpdateRequired("AdminUsers")) return;
            var defaultLanguage =
                _languageRepository.Get(x => x.Code3.ToLower() == Constants.AdminDefaultLanguageCode).FirstOrDefault();
            AdminUser adminUser = new AdminUser
            {
                UserName = "admin",
                Name = "admin",
                Title = "admin",
                CreateDateTime = DateTime.Now,
                LastLogin = DateTime.Now,
                IsActive = true,
                Roles = new EditableList<AdminUserRole>(),
                Password = OxideUtils.ToSha256("admin"),
                IsSuperAdmin = true,
                UserImageId = 0,
                UserLanguage = defaultLanguage
            };
            _adminUserRepository.Insert(adminUser);
            WriteUpdateItem("AdminUsers");
        }

        public void InstallDefaultModules()
        {
            if (!IsUpdateRequired("DefaultModules")) return;
            _moduleService.SetModuleDefault(Constants.OxideFundamentals);
            _moduleService.SetModuleDefault(Constants.OxideMedia);
            WriteUpdateItem("DefaultModules");
        }

        public void SetDefaultTheme()
        {
            if (!IsUpdateRequired("DefaultThemes")) return;
            _themeService.ChangeStatus(Constants.DefaultTheme, false);
            WriteUpdateItem("DefaultThemes");
        }

        public void CreateDefaultPages()
        {
            if (!IsUpdateRequired("DefaultPages")) return;
            var siteSettings = _siteSettings.GetSettings();
            var mainLayout =
                Oxide.LoadedLayouts.FirstOrDefault(x => x.LayoutType == Syntax.Syntax.LayoutTypes.Main && x.Id == 1);
            var innerLayout =
                Oxide.LoadedLayouts.FirstOrDefault(x => x.LayoutType == Syntax.Syntax.LayoutTypes.Inner && x.Id == 1);

            //Create Home Page
            PageViewModel homePageViewModel = new PageViewModel
            {
                AggresiveCacheEnabled = false,
                BasePageId = 0,
                CreateDate = DateTime.Now,
                Description = "Home Page",
                HomePage = true,
                Order = 0,
                PageKey = "Home Page",
                PublishImd = true,
                Published = true,
                VisibleForSite = true,
                VisibleForSeo = true,
                PageType = Syntax.Syntax.PageTypes.Standard,
                SiteMapFrequncy = Syntax.Syntax.SiteMapFrequncy.Always,
                Tag = "Home Page,Oxide Cms",
                Language = siteSettings.DefaultSiteCulture,
                Priority = 1,
                Name = "Home Page",
                Title = "Home Page"
            };
            if (mainLayout != null) homePageViewModel.MainLayoutId = mainLayout.Id;
            if (innerLayout != null) homePageViewModel.InnerLayoutId = innerLayout.Id;
            _pageService.CreatePage(homePageViewModel);
            PageViewModel aboutPageViewModel = new PageViewModel
            {
                AggresiveCacheEnabled = false,
                BasePageId = 0,
                CreateDate = DateTime.Now,
                Description = "About",
                HomePage = false,
                Order = 0,
                PageKey = "About",
                PublishImd = true,
                Published = true,
                VisibleForSite = true,
                VisibleForSeo = true,
                PageType = Syntax.Syntax.PageTypes.Standard,
                SiteMapFrequncy = Syntax.Syntax.SiteMapFrequncy.Always,
                Tag = "About,Oxide Cms",
                Language = siteSettings.DefaultSiteCulture,
                Priority = 1,
                Name = "About",
                Title = "About",
                Url = "about"
            };
            if (mainLayout != null) aboutPageViewModel.MainLayoutId = mainLayout.Id;
            if (innerLayout != null) aboutPageViewModel.InnerLayoutId = innerLayout.Id;
            _pageService.CreatePage(aboutPageViewModel);
            PageViewModel communityPageViewModel = new PageViewModel
            {
                AggresiveCacheEnabled = false,
                BasePageId = 0,
                CreateDate = DateTime.Now,
                Description = "Community",
                HomePage = false,
                Order = 0,
                PageKey = "Community",
                PublishImd = true,
                Published = true,
                VisibleForSite = true,
                VisibleForSeo = true,
                PageType = Syntax.Syntax.PageTypes.Standard,
                SiteMapFrequncy = Syntax.Syntax.SiteMapFrequncy.Always,
                Tag = "Community,Oxide Cms",
                Language = siteSettings.DefaultSiteCulture,
                Priority = 1,
                Name = "Community",
                Title = "Community",
                Url = "community"
            };
            if (mainLayout != null) communityPageViewModel.MainLayoutId = mainLayout.Id;
            if (innerLayout != null) communityPageViewModel.InnerLayoutId = innerLayout.Id;
            _pageService.CreatePage(communityPageViewModel);
            PageViewModel contactPageViewModel = new PageViewModel
            {
                AggresiveCacheEnabled = false,
                BasePageId = 0,
                CreateDate = DateTime.Now,
                Description = "Contact",
                HomePage = false,
                Order = 0,
                PageKey = "Contact",
                PublishImd = true,
                Published = true,
                VisibleForSite = true,
                VisibleForSeo = true,
                PageType = Syntax.Syntax.PageTypes.Standard,
                SiteMapFrequncy = Syntax.Syntax.SiteMapFrequncy.Always,
                Tag = "Contact,Oxide Cms",
                Language = siteSettings.DefaultSiteCulture,
                Priority = 1,
                Name = "Contact",
                Title = "Contact",
                Url = "contact"
            };
            if (mainLayout != null) contactPageViewModel.MainLayoutId = mainLayout.Id;
            if (innerLayout != null) contactPageViewModel.InnerLayoutId = innerLayout.Id;
            _pageService.CreatePage(contactPageViewModel);
            WriteUpdateItem("DefaultPages");
        }

        public bool IsInitilizationMode()
        {
            return IsUpdateRequired("Initilization");
        }

        public void CompleteInitilization()
        {
            if (IsUpdateRequired("Initilization"))
                WriteUpdateItem("Initilization");
        }
    }
}