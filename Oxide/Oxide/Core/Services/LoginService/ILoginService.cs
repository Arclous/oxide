﻿using Oxide.Areas.Admin.ViewModels;
using Oxide.Core.Base.OxideDependency;
using Oxide.Core.Models.AdminUser;

namespace Oxide.Core.Services.LoginService
{
    /// <summary>
    ///     Service for managing login process
    /// </summary>
    public interface ILoginService : IOxideDependency
    {
        /// <summary>
        ///     Logins user to admin site
        /// </summary>
        /// <param name="userName">User name</param>
        /// <param name="password">Password of the user</param>
        Syntax.Syntax.AdminLoginResult LoginUser(string userName, string password);
        /// <summary>
        ///     Sings Out user
        /// </summary>
        void SignOut();
        /// <summary>
        ///     Checks if user logined to site
        /// </summary>
        bool IsUserLogined();
        /// <summary>
        ///     Checks if the user is super admin
        /// </summary>
        bool IsSuperAdmin();
        /// <summary>
        ///     Checks permission
        /// </summary>
        /// <param name="key">Key of permission</param>
        bool CheckPermission(string key);
        /// <summary>
        ///     Updates user last access date and time
        /// </summary>
        /// <param name="userName">User name</param>
        void UpdateAccessDateTime(string userName);
        /// <summary>
        ///     Gets logined user info
        /// </summary>
        UserProfileInfoViewModel GetUserInfo();
        /// <summary>
        ///     Sets user admin language
        /// </summary>
        /// <param name="user">User </param>
        void SetUserAdminUserLanguage(AdminUser user);
        /// <summary>
        ///     Sets user admin language
        /// </summary>
        /// <param name="userId">Id of the user </param>
        void SetUserAdminUserLanguage(int userId);
        /// <summary>
        ///     Gets user admin language
        /// </summary>
        string GetUserAdminUserLanguage();
        /// <summary>
        ///     Gets user admin language
        /// </summary>
        string GetUserAdminUserLanguageCode2();
        /// <summary>
        ///     Sets user profile image url
        /// </summary>
        /// <param name="url">Url of the prifle image</param>
        void SetUserAdminUserProfileImage(string url);
        /// <summary>
        ///     Gets user admin user profile image url
        /// </summary>
        string GetUserAdminUserProfileImageUrl();
    }
}