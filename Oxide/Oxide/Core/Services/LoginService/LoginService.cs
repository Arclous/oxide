﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Linq;
using System.Security.Claims;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Oxide.Areas.Admin.ViewModels;
using Oxide.Core.Helpers;
using Oxide.Core.Models.AdminUser;
using Oxide.Core.Services.AdminRoleService;
using Oxide.Core.Services.AdminUserService;
using Oxide.Core.Services.OxideUiService;
using Constants = Oxide.Core.Syntax.Constants;

namespace Oxide.Core.Services.LoginService
{
    public class LoginService : ILoginService
    {
        private readonly IAdminUserService _adminUserService;
        private readonly IOxideUiService _oxideUiService;

        public LoginService(IAdminUserService adminUserService, IAdminRoleService adminRoleService,
            IOxideUiService oxideUiService)
        {
            _adminUserService = adminUserService;
            _oxideUiService = oxideUiService;
        }

        private static IAuthenticationManager Authentication
        {
            get { return HttpContext.Current.Request.GetOwinContext().Authentication; }
        }

        public Syntax.Syntax.AdminLoginResult LoginUser(string userName, string password)
        {
            var user = _adminUserService.GetUser(userName);
            if (user == null) return Syntax.Syntax.AdminLoginResult.UserNotFound;
            if (!IsPasswordMatch(user, password)) return Syntax.Syntax.AdminLoginResult.NotSuccess;
            var permissions =
                (from rule in user.Roles
                 from permission in rule.Role.Permissions
                 select new Claim(permission.Title, permission.Title)).ToList();
            var identity =
                new ClaimsIdentity(
                    new[]
                    {
                        new Claim(ClaimTypes.Name, user.UserName),
                        new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider",
                            user.Id.ToString()),
                        new Claim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier",
                            user.Id.ToString())
                    }, DefaultAuthenticationTypes.ApplicationCookie, ClaimTypes.Name,
                    ClaimTypes.Role);

            //Store user information
            identity.AddClaim(new Claim(Constants.UserId, user.Id.ToString()));
            identity.AddClaim(new Claim(Constants.UserName, user.UserName));
            identity.AddClaim(new Claim(Constants.User, user.Name));
            identity.AddClaim(new Claim(Constants.UserProfileImage, user.UserImageId.ToString()));
            if (user.Email != null)
                identity.AddClaim(new Claim(Constants.UserEmail, user.Email));

            //Checks if the user is super admin
            if (user.IsSuperAdmin)
                identity.AddClaim(new Claim(Constants.IsSuperAdmin, "true"));

            //Add roles
            identity.AddClaims(permissions);
            Authentication.SignIn(new AuthenticationProperties {IsPersistent = true}, identity);

            //Update last login date and time
            UpdateAccessDateTime(user.UserName);

            //Set User Admin Language
            SetUserAdminUserLanguage(user);

            //Set User Profile Image
            SetUserAdminUserProfileImage(_oxideUiService.GetImageContentUrl(user.UserImageId.ToString()));
            return Syntax.Syntax.AdminLoginResult.Success;
        }

        public void SignOut()
        {
            Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        }

        public bool IsUserLogined()
        {
            var claim = Authentication.User.Claims.FirstOrDefault(x => x.Type == Constants.UserId);
            return claim != null &&
                   (Authentication.User?.Identity != null && Authentication.User.Identity.IsAuthenticated &&
                    Convert.ToInt32(claim.Value) != 0);
        }

        public bool IsSuperAdmin()
        {
            return Authentication.User?.Identity != null &&
                   Authentication.User.HasClaim(x => x.Type == Constants.IsSuperAdmin);
        }

        public bool CheckPermission(string key)
        {
            return IsSuperAdmin() || Authentication.User.Claims.Any(x => x.Value == key);
        }

        public void UpdateAccessDateTime(string userName)
        {
            _adminUserService.UpdateLastAccessDateTime(userName);
        }

        public UserProfileInfoViewModel GetUserInfo()
        {
            if (Authentication.User?.Identity == null) return null;
            var userInfo = new UserProfileInfoViewModel();
            var firstOrDefault = Authentication.User.Claims.FirstOrDefault(x => x.Type == Constants.UserName);
            if (firstOrDefault != null)
                userInfo.UserName = firstOrDefault.Value;
            var orDefault = Authentication.User.Claims.FirstOrDefault(x => x.Type == Constants.User);
            if (orDefault != null)
                userInfo.UserRealName = orDefault.Value;
            var @default = Authentication.User.Claims.FirstOrDefault(x => x.Type == Constants.UserProfileImage);
            if (@default != null)
                userInfo.ProfileImage = @default.Value;
            var claim = Authentication.User.Claims.FirstOrDefault(x => x.Type == Constants.UserEmail);
            if (claim != null)
                userInfo.Email = claim.Value;
            var id = Authentication.User.Claims.FirstOrDefault(x => x.Type == Constants.UserId);
            if (id != null)
                userInfo.Id = Convert.ToInt32(id.Value);
            if (userInfo.Id == 0)
                Authentication.SignOut();
            userInfo.UserProfileImageUrl = GetUserAdminUserProfileImageUrl();
            return userInfo;
        }

        public void SetUserAdminUserLanguage(AdminUser user)
        {
            var code = Constants.AdminDefaultLanguageCode;
            if (user.UserLanguage != null)
                code = user.UserLanguage.Code3;
            var langCookie = new HttpCookie(Constants.AdminLanguageCookieName, code)
            {
                HttpOnly = true,
                Expires = DateTime.Now.AddYears(1)
            };
            HttpContext.Current.Response.AppendCookie(langCookie);
            if (user.UserLanguage != null)
                code = user.UserLanguage.Code;
            langCookie = new HttpCookie(Constants.AdminLanguageCookieName2, code)
            {
                HttpOnly = true,
                Expires = DateTime.Now.AddYears(1)
            };
            HttpContext.Current.Response.AppendCookie(langCookie);
        }

        public void SetUserAdminUserLanguage(int userId)
        {
            if (userId == 0) return;
            var user = _adminUserService.GetUserWithLanguage(userId);
            var langCookie = new HttpCookie(Constants.AdminLanguageCookieName, user.UserLanguage.Code3)
            {
                HttpOnly = true,
                Expires = DateTime.Now.AddYears(1)
            };
            HttpContext.Current.Response.AppendCookie(langCookie);
            langCookie = new HttpCookie(Constants.AdminLanguageCookieName2, user.UserLanguage.Code)
            {
                HttpOnly = true,
                Expires = DateTime.Now.AddYears(1)
            };
            HttpContext.Current.Response.AppendCookie(langCookie);
        }

        public string GetUserAdminUserLanguage()
        {
            var languageCookie = HttpContext.Current.Request.Cookies[Constants.AdminLanguageCookieName];
            return languageCookie?.Value.ToUpper() ?? Constants.AdminDefaultLanguageCode;
        }

        public string GetUserAdminUserLanguageCode2()
        {
            var languageCookie = HttpContext.Current.Request.Cookies[Constants.AdminLanguageCookieName2];
            return languageCookie?.Value.ToUpper() ?? Constants.AdminDefaultLanguageCode;
        }

        public string GetUserAdminUserProfileImageUrl()
        {
            var imgCookie = HttpContext.Current.Request.Cookies[Constants.AdminProfileImageCookieName];
            return imgCookie?.Value.ToLower() ?? string.Empty;
        }

        public void SetUserAdminUserProfileImage(string url)
        {
            var imgCookie = new HttpCookie(Constants.AdminProfileImageCookieName, url)
            {
                HttpOnly = true,
                Expires = DateTime.Now.AddYears(1)
            };
            HttpContext.Current.Response.AppendCookie(imgCookie);
        }

        private static bool IsPasswordMatch(AdminUser user, string password)
        {
            return user.Password == OxideUtils.ToSha256(password);
        }
    }
}