﻿using System.Collections.Generic;
using Oxide.Areas.Admin.Models;
using Oxide.Core.Base.OxideDependency;

namespace Oxide.Core.Services.TimeZoneService
{
    /// <summary>
    ///     Service for managing time zone
    /// </summary>
    public interface ITimeZoneService : IOxideDependency
    {
        /// <summary>
        ///     Creates timezone according to model
        /// </summary>
        /// <param name="model">Timezone model</param>
        void CreateTimeZone(TimeZoneItem model);
        /// <summary>
        ///     Updates timezone according to model
        /// </summary>
        /// <param name="model">Timezone model</param>
        void UpdateTimeZone(TimeZoneItem model);
        /// <summary>
        ///     Deletes timezone according to model
        /// </summary>
        /// <param name="model">Timezone model</param>
        void DeleteTimeZone(TimeZoneItem model);
        /// <summary>
        ///     Deletes timezone according to id
        /// </summary>
        /// <param name="id">Id of the timezone</param>
        void DeleteTimeZone(int id);
        /// <summary>
        ///     Gets timezone according to id
        /// </summary>
        /// <param name="id">Id of the timezone</param>
        TimeZoneItem GetTimeZone(int id);
        /// <summary>
        ///     Gets all timezones
        /// </summary>
        List<TimeZoneItem> GetAllTimeZones();
    }
}