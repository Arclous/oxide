﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System.Collections.Generic;
using System.Linq;
using Oxide.Areas.Admin.Models;
using Oxide.Core.Data.Repository;

namespace Oxide.Core.Services.TimeZoneService
{
    public class TimeZoneServices : ITimeZoneService
    {
        private readonly IOxideRepository<TimeZoneItem> _timeZoneRepository;

        public TimeZoneServices()
        {
        }

        public TimeZoneServices(IOxideRepository<TimeZoneItem> timeZoneRepository)
        {
            _timeZoneRepository = timeZoneRepository;
        }

        public void CreateTimeZone(TimeZoneItem model)
        {
            _timeZoneRepository.Insert(model);
        }

        public void UpdateTimeZone(TimeZoneItem model)
        {
            _timeZoneRepository.Update(model);
        }

        public void DeleteTimeZone(TimeZoneItem model)
        {
            _timeZoneRepository.Delete(model);
        }

        public void DeleteTimeZone(int id)
        {
            _timeZoneRepository.Delete(id);
        }

        public TimeZoneItem GetTimeZone(int id)
        {
            return _timeZoneRepository.GetById(id);
        }

        public List<TimeZoneItem> GetAllTimeZones()
        {
            return _timeZoneRepository.GetAll().ToList();
        }
    }
}