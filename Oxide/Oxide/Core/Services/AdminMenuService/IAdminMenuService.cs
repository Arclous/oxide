﻿using System.Collections.Generic;
using System.Web.Routing;
using Oxide.Core.Base.OxideDependency;
using Oxide.Core.Models.Menus;
using Oxide.Core.ViewModels.MenuViewModel;

namespace Oxide.Core.Services.AdminMenuService
{
    /// <summary>
    ///     Responsible service for admin menus
    /// </summary>
    public interface IAdminMenuService : IOxideDependency
    {
        /// <summary>
        ///     Builds admin menus
        /// </summary>
        /// <param name="requestContext">Requested context</param>
        void BuildAdminMenus(RequestContext requestContext);
        /// <summary>
        ///     Builds admin menus for module
        /// </summary>
        /// <param name="requestContext">Requested context</param>
        /// <param name="moduleName">Module name</param>
        void BuildAdminMenus(RequestContext requestContext, string moduleName);
        /// <summary>
        ///     Clears admin menus for modules
        /// </summary>
        /// <param name="moduleName">Module name</param>
        void ClearMenusForModule(string moduleName);
        /// <summary>
        ///     Adds header menu
        /// </summary>
        /// <param name="key">Key of the header menu</param>
        /// <param name="title">Title of the header menu , which can be used with translation</param>
        /// <param name="iconCss">Css name for the header menu</param>
        /// <param name="order">Order value</param>
        /// <param name="permissionKey">Permission key</param>
        HeaderMenu AddHeaderMenu(string key, string title, string iconCss, int order, string permissionKey);
        /// <summary>
        ///     Adds header menu
        /// </summary>
        /// <param name="key">Key of the header menu</param>
        /// <param name="title">Title of the header menu , which can be used with translation</param>
        /// <param name="iconCss">Css name for the header menu</param>
        /// <param name="order">Order value</param>
        /// <param name="permissionKey">Permission key</param>
        /// <param name="menuItems">Menu items </param>
        HeaderMenu AddHeaderMenu(string key, string title, string iconCss, int order, string permissionKey,
            List<MenuItem> menuItems);
        /// <summary>
        ///     Adds menu items
        /// </summary>
        /// <param name="headerMenu">Header menu for the menu item</param>
        /// <param name="key">Key of the header menu</param>
        /// <param name="title">Title of the header menu , which can be used with translation</param>
        /// <param name="controllerName">Controller name</param>
        /// <param name="permissionKey">Permission key</param>
        /// <param name="order">Order value</param>
        /// <param name="actionName">Action name</param>
        MenuItem AddMenuItem(HeaderMenu headerMenu, string key, string title, string actionName, string controllerName,
            string permissionKey, int order);
        /// <summary>
        ///     Adds menu items
        /// </summary>
        /// <param name="headerMenuKey">Key of the header menu</param>
        /// <param name="key">Key of the header menu</param>
        /// <param name="title">Title of the header menu , which can be used with translation</param>
        /// <param name="controllerName">Controller name</param>
        /// <param name="permissionKey">Permission key</param>
        /// <param name="order">Order value</param>
        /// <param name="actionName">Action name</param>
        MenuItem AddMenuItem(string headerMenuKey, string key, string title, string actionName, string controllerName,
            string permissionKey, int order);
        /// <summary>
        ///     Adds menu item
        /// </summary>
        /// <param name="headerItem">Header item</param>
        /// <param name="menuItem">Menu item</param>
        MenuItem AddMenuItem(HeaderMenu headerItem, MenuItem menuItem);
        /// <summary>
        ///     Gets header menu according to key
        /// </summary>
        /// <param name="key">Key of the header menu</param>
        HeaderMenu GetHeaderMenu(string key);
        /// <summary>
        ///     Gets menu item according to header key and menu key
        /// </summary>
        /// <param name="headerMenuKey">Key of the header</param>
        /// <param name="menuKey">Key of the menu</param>
        MenuItem GetMenuItem(string headerMenuKey, string menuKey);
        /// <summary>
        ///     Builds and returns admin menu
        /// </summary>
        LeftMenuViewModel GetAdminMenu();
        /// <summary>
        ///     Clears admin menu
        /// </summary>
        void ClearMenus();
        /// <summary>
        ///     Cheks if header menu is exist
        /// </summary>
        /// <param name="headerMenu">Header menu to check</param>
        bool IsHeaderMenuExist(HeaderMenu headerMenu);
    }
}