﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Routing;
using Oxide.Core.Helpers;
using Oxide.Core.Managers.AssemblyManager;
using Oxide.Core.Models.Menus;
using Oxide.Core.Services.Interface.Registers.AdminMenuRegister;
using Oxide.Core.Services.LoginService;
using Oxide.Core.Syntax;
using Oxide.Core.ViewModels.MenuViewModel;

namespace Oxide.Core.Services.AdminMenuService
{
    public class AdminMenuService : IAdminMenuService
    {
        private readonly ILoginService _loginService;
        public readonly List<HeaderMenu> AdminMenus = new List<HeaderMenu>();

        public AdminMenuService()
        {
        }

        public AdminMenuService(ILoginService loginService)
        {
            _loginService = loginService;
        }

        public void BuildAdminMenus(RequestContext requestContext)
        {
            foreach (var instance in AssemblyManager.GetFor<IAdminMenuRegisterer>())
                instance.BuildMenus(requestContext, this);
        }

        public void BuildAdminMenus(RequestContext requestContext, string moduleName)
        {
            foreach (var instance in AssemblyManager.GetFor<IAdminMenuRegisterer>(moduleName))
                instance.BuildMenus(requestContext, this);
        }

        public void ClearMenusForModule(string moduleName)
        {
            foreach (var v in AdminMenus)
                v.SubMenuItems.RemoveAll(x => x.ModuleName == moduleName);
            AdminMenus.RemoveAll(x => x.ModuleName == moduleName);
        }

        public HeaderMenu AddHeaderMenu(string key, string title, string iconCss, int order, string permissionKey)
        {
            var newHeaderMenu = new HeaderMenu
            {
                Key = key,
                Title = title,
                IconCss = iconCss,
                Order = order,
                PermissionKey = permissionKey,
                SubMenuItems = new List<MenuItem>(),
                ModuleName = OxideUtils.GetCallerModuleName(new StackFrame(1))
            };
            if (IsHeaderMenuExist(newHeaderMenu)) return newHeaderMenu;
            if (newHeaderMenu.PermissionKey != PermissionKeys.Empty)
            {
                if (_loginService.CheckPermission(newHeaderMenu.PermissionKey))
                    AdminMenus.Add(newHeaderMenu);
            }
            else
                AdminMenus.Add(newHeaderMenu);
            return newHeaderMenu;
        }

        public HeaderMenu AddHeaderMenu(string key, string title, string iconCss, int order, string permissionKey,
            List<MenuItem> menuItems)
        {
            var newHeaderMenu = new HeaderMenu
            {
                Key = key,
                Title = title,
                IconCss = iconCss,
                Order = order,
                PermissionKey = permissionKey,
                SubMenuItems = menuItems,
                ModuleName = OxideUtils.GetCallerModuleName(new StackFrame(1))
            };
            if (IsHeaderMenuExist(newHeaderMenu)) return newHeaderMenu;
            if (newHeaderMenu.PermissionKey != PermissionKeys.Empty)
            {
                if (_loginService.CheckPermission(newHeaderMenu.PermissionKey))
                    AdminMenus.Add(newHeaderMenu);
            }
            else
                AdminMenus.Add(newHeaderMenu);
            return newHeaderMenu;
        }

        public MenuItem AddMenuItem(HeaderMenu headerMenu, string key, string title, string actionName,
            string controllerName, string permissionKey, int order)
        {
            var newMenu = new MenuItem
            {
                Key = key,
                Title = title,
                ActionName = actionName,
                ControllerName = controllerName,
                Order = order,
                PermissionKey = permissionKey,
                SubMenuItems = new List<MenuItem>(),
                ModuleName = OxideUtils.GetCallerModuleName(new StackFrame(1))
            };
            if (permissionKey != PermissionKeys.Empty)
            {
                if (_loginService.CheckPermission(permissionKey))
                    headerMenu.SubMenuItems.Add(newMenu);
            }
            else
                headerMenu.SubMenuItems.Add(newMenu);
            headerMenu.SubMenuItems.Add(newMenu);
            return newMenu;
        }

        public MenuItem AddMenuItem(string headerMenuKey, string key, string title, string actionName,
            string controllerName, string permissionKey, int order)
        {
            var newMenu = new MenuItem
            {
                Key = key,
                Title = title,
                ActionName = actionName,
                ControllerName = controllerName,
                Order = order,
                PermissionKey = permissionKey,
                SubMenuItems = new List<MenuItem>(),
                ModuleName = OxideUtils.GetCallerModuleName(new StackFrame(1))
            };
            var headerMenu = AdminMenus.Find(x => x.Title == headerMenuKey);
            if (permissionKey != PermissionKeys.Empty)
            {
                if (_loginService.CheckPermission(permissionKey))
                    headerMenu.SubMenuItems.Add(newMenu);
            }
            else
                headerMenu.SubMenuItems.Add(newMenu);
            return newMenu;
        }

        public MenuItem AddMenuItem(HeaderMenu headerItem, MenuItem menuItem)
        {
            if (menuItem.PermissionKey != PermissionKeys.Empty)
            {
                if (_loginService.CheckPermission(menuItem.PermissionKey))
                    headerItem.SubMenuItems.Add(menuItem);
            }
            else
                headerItem.SubMenuItems.Add(menuItem);
            return menuItem;
        }

        public HeaderMenu GetHeaderMenu(string key)
        {
            return AdminMenus.Find(x => x.Title == key);
        }

        public MenuItem GetMenuItem(string headerMenuKey, string menuKey)
        {
            return AdminMenus.Find(x => x.Title == headerMenuKey).SubMenuItems.Find(x => x.Key == menuKey);
        }

        public LeftMenuViewModel GetAdminMenu()
        {
            BuildAdminMenus(HttpContext.Current.Request.RequestContext);
            var adminMen = AdminMenus.Where(v => v.SubMenuItems != null).ToList();
            foreach (var v in adminMen)
            {
                v.SubMenuItems = v.SubMenuItems.OrderBy(x => x.Order).ToList();
                foreach (var v2 in v.SubMenuItems.Where(v2 => v2.SubMenuItems != null))
                    v2.SubMenuItems = v2.SubMenuItems.OrderBy(x => x.Order).ToList();
            }
            return new LeftMenuViewModel
            {
                Menus = AdminMenus.Where(x => x.SubMenuItems.Count > 0).OrderBy(x => x.Order).ToList()
            };
        }

        public void ClearMenus()
        {
            AdminMenus.Clear();
        }

        public bool IsHeaderMenuExist(HeaderMenu headerMenu)
        {
            var checkMenu = headerMenu;
            var adminMenus = AdminMenus;
            lock (adminMenus)
            {
                return adminMenus.Any(x => x.Key == checkMenu.Key);
            }
        }
    }
}