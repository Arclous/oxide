﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Collections;
using System.Linq;
using Oxide.Core.Data.Context;
using Oxide.Core.Data.Repository;
using Oxide.Core.Services.ResourceService;
using Oxide.Core.Syntax;

namespace Oxide.Core.Services.OxideService
{
    public class OxideServices : IOxideServices
    {
        private readonly IResourceService _resourceService;

        public OxideServices(IResourceService resourceService)
        {
            _resourceService = resourceService;
        }

        public OxideServices()
        {
        }

        public object GetDynamicContent(string contentName, string value)
        {
            var contentType = Oxide.LoadedModels.FirstOrDefault(x => x.Name == contentName);
            if (contentType == null) return null;
            var type = typeof (OxideRepository<>).MakeGenericType(contentType);
            dynamic repository = Activator.CreateInstance(type);
            var dbContextProperty = type.GetProperty(OxideSystem.DbContext);
            dbContextProperty?.SetValue(repository, new OxideContext());
            object content =
                ((IEnumerable) repository.Table).Cast<dynamic>()
                                                .Where(x => x.Id == Convert.ToInt32(value))
                                                .OrderBy(x => x.Name)
                                                .FirstOrDefault();
            return content;
        }

        public string GetText(string resourceKey, string resourceName = null)
        {
            return resourceName == null
                ? _resourceService.GetText(resourceKey)
                : _resourceService.GetText(resourceKey, resourceName);
        }
    }
}