﻿using Oxide.Core.Base.OxideDependency;

namespace Oxide.Core.Services.OxideService
{
    /// <summary>
    ///     Global service
    /// </summary>
    public interface IOxideServices : IOxideDependency
    {
        /// <summary>
        ///     Gets dynamic contents according to given criterias and values as object
        /// </summary>
        /// <param name="contentName">Name of the content which will be fetched</param>
        /// <param name="value">Value which will be using duing fetching</param>
        object GetDynamicContent(string contentName, string value);
        /// <summary>
        ///     Gets localization text according to resource key and/or resource name
        /// </summary>
        /// <param name="resourceKey">Key of the element</param>
        /// <param name="resourceName">Resource name</param>
        string GetText(string resourceKey, string resourceName = null);
    }
}