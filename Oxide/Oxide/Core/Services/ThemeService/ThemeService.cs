﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Oxide.Core.Base.OxideDependency;
using Oxide.Core.Data.Repository;
using Oxide.Core.Elements;
using Oxide.Core.Extesions;
using Oxide.Core.Helpers;
using Oxide.Core.Managers.AssemblyManager;
using Oxide.Core.Models.Themes;
using Oxide.Core.Services.Interface.Registers.ResourceRegister;
using Oxide.Core.Syntax;
using Oxide.Core.ViewModels.FilterViewModels;
using Oxide.Core.ViewModels.ThemeViewModel;

namespace Oxide.Core.Services.ThemeService
{
    public class ThemeService : IThemeService, IOxideDependency
    {
        private readonly IOxideRepository<Theme> _themeRepository;

        public ThemeService()
        {
        }

        public ThemeService(IOxideRepository<Theme> themeRepository)
        {
            _themeRepository = themeRepository;
        }

        public int CreateTheme(Theme model)
        {
            _themeRepository.Insert(model);
            return model.Id;
        }

        public void UpdateTheme(Theme model)
        {
            var updateMode = _themeRepository.SearchFor(x => x.Id == model.Id).FirstOrDefault();
            if (updateMode == null) throw new ArgumentNullException(nameof(updateMode));
            updateMode = model.CopyPropertiesTo(model, new[] {"Id"});
            _themeRepository.Update(updateMode);
        }

        public void DeleteTheme(Theme model)
        {
            _themeRepository.Delete(model);
        }

        public void DeleteTheme(int id)
        {
            _themeRepository.Delete(id);
        }

        public Theme GetTheme(int id)
        {
            return _themeRepository.GetById(id);
        }

        public Theme GetTheme(string name)
        {
            return _themeRepository.SearchFor(x => x.Name == name).FirstOrDefault();
        }

        public FilterModel<ThemeViewModel> GetThemesWithFilter(int pageSize, int page, string filter)
        {
            var pageFilterViewModel = new FilterModel<ThemeViewModel>();
            if (filter != string.Empty)
                pageFilterViewModel.Records =
                    GetThemeViewModelList(
                        _themeRepository.GetAll()
                                        .OrderBy(x => x.Name)
                                        .Where(x => x.Name.ToLower().Contains(filter.ToLower()))
                                        .Skip(pageSize*page)
                                        .Take(pageSize)
                                        .ToList());
            else
                pageFilterViewModel.Records =
                    GetThemeViewModelList(
                        _themeRepository.GetAll().OrderBy(x => x.Name).Skip(pageSize*page).Take(pageSize).ToList());
            pageFilterViewModel.CurrentPage = page;
            pageFilterViewModel.DisplayingRecords = pageFilterViewModel.Records.Count();
            pageFilterViewModel.TotalRecords = _themeRepository.GetAll().Count();
            if (filter != string.Empty)
                pageFilterViewModel.TotalPages =
                    (_themeRepository.GetAll().OrderBy(x => x.Name).Count(x => x.Name.Contains(filter)) + pageSize - 1)/
                    pageSize;
            else
                pageFilterViewModel.TotalPages = (pageFilterViewModel.TotalRecords + pageSize - 1)/pageSize;
            pageFilterViewModel.NextEnable = pageFilterViewModel.CurrentPage < pageFilterViewModel.TotalPages - 1;
            pageFilterViewModel.PreviousEnable = pageFilterViewModel.CurrentPage > 0;
            return pageFilterViewModel;
        }

        public List<Theme> GetAllThemes()
        {
            return _themeRepository.GetAll().ToList();
        }

        public Theme GetActiveTheme()
        {
            return _themeRepository.SearchFor(x => x.Active).FirstOrDefault();
        }

        public bool IsThemeActive(string name)
        {
            var firstOrDefault = _themeRepository.SearchFor(x => x.Name == name).FirstOrDefault();
            return firstOrDefault != null && firstOrDefault.Active;
        }

        public FilterModel<ThemeViewModel> ThemeList(string filter)
        {
            ICollection<ThemeViewModel> moduleList;
            var moduleFilterViewModel = new FilterModel<ThemeViewModel>();
            if (filter != null)
            {
                if (filter != string.Empty)
                {
                    moduleList =
                        GetViewThemeWithFilter(
                            OxideThemeHelper.GetThemeList()
                                            .Where(
                                                x =>
                                                    x.Name.ToLower().Contains(filter.ToLower()) ||
                                                    x.Title.ToLower().Contains(filter.ToLower()) ||
                                                    x.Category.ToLower().Contains(filter.ToLower()) ||
                                                    x.Description.ToLower().Contains(filter.ToLower()))
                                            .OrderBy(x => x.Name));
                    moduleFilterViewModel.Records = moduleList;
                }
                else
                {
                    moduleList = GetViewThemeWithFilter(OxideThemeHelper.GetThemeList().OrderBy(x => x.Name));
                    moduleFilterViewModel.Records = moduleList;
                }
            }
            else
            {
                moduleList = GetViewThemeWithFilter(OxideThemeHelper.GetThemeList().OrderBy(x => x.Name));
                moduleFilterViewModel.Records = moduleList;
            }
            return moduleFilterViewModel;
        }

        public ICollection<ThemeViewModel> GetViewThemeWithFilter(IEnumerable<IOxideTheme> moduleList)
        {
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            return
                moduleList.Select(
                    module =>
                        new ThemeViewModel
                        {
                            Name = module.Name,
                            PreviewImageUrl = module.PreviewImageUrl,
                            Active = IsThemeActive(module.Name.Replace(" ", Constants.Empty)),
                            ChangeStatusUrl =
                                urlHelper.Action(MVC.Admin.Theme.ActionNames.ChangeStatus, MVC.Admin.Theme.Name,
                                    new {area = Constants.DefaultArea})
                        }).ToList();
        }

        public bool ChangeStatus(string name, bool status)
        {
            var targetTheme = _themeRepository.SearchFor(x => x.Name == name).FirstOrDefault();
            var themes = _themeRepository.GetAll().ToList();
            foreach (var theme in themes)
            {
                theme.Active = false;
                _themeRepository.Update(targetTheme);
            }
            if (targetTheme != null)
            {
                targetTheme.Active = true;
                _themeRepository.Update(targetTheme);
            }
            foreach (var thm in Oxide.LoadedThemes)
            {
                if (targetTheme != null && thm.Name == targetTheme.Name)
                    thm.Active = true;
                else
                    thm.Active = false;
            }
            foreach (var instance in from instance in AssemblyManager.GetFor<IResourceRegisterer>(Constants.ThemeArea)
                                     let themeName = ((dynamic) instance).ThemeName
                                     where targetTheme != null && themeName == targetTheme.Name
                                     select instance)
            {
                instance.RegisterResource(Oxide.BoundleCollection);
            }
            return !status;
        }

        private static ICollection<ThemeViewModel> GetThemeViewModelList(IEnumerable<Theme> recordList)
        {
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            return
                (from page in recordList
                 let pageViewModel = new ThemeViewModel()
                 select page.CopyPropertiesTo(pageViewModel)).ToList();
        }
    }
}