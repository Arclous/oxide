﻿using System.Collections.Generic;
using Oxide.Core.Elements;
using Oxide.Core.Models.Themes;
using Oxide.Core.ViewModels.FilterViewModels;
using Oxide.Core.ViewModels.ThemeViewModel;

namespace Oxide.Core.Services.ThemeService
{
    /// <summary>
    ///     Service for managing themes
    /// </summary>
    public interface IThemeService
    {
        /// <summary>
        ///     Creates theme according to model
        /// </summary>
        /// <param name="model">Theme model</param>
        int CreateTheme(Theme model);
        /// <summary>
        ///     Updates theme according to model
        /// </summary>
        /// <param name="model">Theme model</param>
        void UpdateTheme(Theme model);
        /// <summary>
        ///     Deletes theme according to model
        /// </summary>
        /// <param name="model">Theme model</param>
        void DeleteTheme(Theme model);
        /// <summary>
        ///     Deletes theme according to id
        /// </summary>
        /// <param name="id">Id of the theme</param>
        void DeleteTheme(int id);
        /// <summary>
        ///     Gets theme according to id
        /// </summary>
        /// <param name="id">Id of the theme</param>
        Theme GetTheme(int id);
        /// <summary>
        ///     Gets themes according to name
        /// </summary>
        /// <param name="name">Name of the theme</param>
        Theme GetTheme(string name);
        /// <summary>
        ///     Gets themes as filter view model
        /// </summary>
        /// <param name="pageSize">Size of the page for each page</param>
        /// <param name="page">Requested page index</param>
        /// <param name="filter">Filter criterias to filtering pages</param>
        FilterModel<ThemeViewModel> GetThemesWithFilter(int pageSize, int page, string filter);
        /// <summary>
        ///     Gets all themes
        /// </summary>
        List<Theme> GetAllThemes();
        /// <summary>
        ///     Gets active theme which is currently using on the site
        /// </summary>
        Theme GetActiveTheme();
        /// <summary>
        ///     Checks if the theme is active according to theme name
        /// </summary>
        /// <param name="name">Name of the theme</param>
        bool IsThemeActive(string name);
        /// <summary>
        ///     Gets themes according to filter as filter view model
        /// </summary>
        /// <param name="filter">Creteria for fetching themes</param>
        FilterModel<ThemeViewModel> ThemeList(string filter);
        /// <summary>
        ///     Gets views of the themes according to filer as collection
        /// </summary>
        /// <param name="moduleList">List of the module which will be used for joing theme info</param>
        ICollection<ThemeViewModel> GetViewThemeWithFilter(IEnumerable<IOxideTheme> moduleList);
        /// <summary>
        ///     Changes status of the theme according to status
        /// </summary>
        /// <param name="name">Name of the theme</param>
        /// <param name="status">New status for the theme</param>
        bool ChangeStatus(string name, bool status);
    }
}