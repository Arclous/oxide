﻿using Oxide.Core.Base.OxideDependency;
using Oxide.Core.Models.EmailTemplate;
using Oxide.Core.ViewModels.EmailTemplateViewModel;
using Oxide.Core.ViewModels.FilterViewModels;

namespace Oxide.Core.Services.EmailTemplateService
{
    /// <summary>
    ///     Service which is reponsbile for email templates
    /// </summary>
    public interface IEmailTemplateService : IOxideDependency
    {
        /// <summary>
        ///     Creates email template according to model
        /// </summary>
        /// <param name="model">Log model</param>
        int CreateEmailTemplate(EmailTemplate model);
        /// <summary>
        ///     Updates email template according to model
        /// </summary>
        /// <param name="model">Log model</param>
        void UpdateEmailTemplate(EmailTemplate model);
        /// <summary>
        ///     Deletes email template according to model
        /// </summary>
        /// <param name="model">Log model</param>
        void DeleteEmailTemplate(EmailTemplate model);
        /// <summary>
        ///     Deletes email template according to id
        /// </summary>
        /// <param name="id">Id of the log</param>
        void DeleteEmailTemplate(int id);
        /// <summary>
        ///     Gets email template model according to id
        /// </summary>
        /// <param name="id">Id of the log</param>
        EmailTemplate GetEmailTemplate(int id);
        /// <summary>
        ///     Gets email templates as filter view model
        /// </summary>
        /// <param name="pageSize">Size of the page for each page</param>
        /// <param name="page">Requested page index</param>
        /// <param name="filter">Filter criterias to filtering logs</param>
        FilterModel<EmailTemplateViewModel> GetEmailTemplateWithFilter(int pageSize, int page, string filter);
    }
}