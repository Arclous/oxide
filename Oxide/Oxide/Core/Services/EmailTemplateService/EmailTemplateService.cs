﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Oxide.Core.Data.Repository;
using Oxide.Core.Models.EmailTemplate;
using Oxide.Core.ViewModels.EmailTemplateViewModel;
using Oxide.Core.ViewModels.FilterViewModels;

namespace Oxide.Core.Services.EmailTemplateService
{
    public class EmailTemplateService : IEmailTemplateService
    {
        private readonly IOxideRepository<EmailTemplate> _emailTemplateRepository;

        public EmailTemplateService()
        {
        }

        public EmailTemplateService(IOxideRepository<EmailTemplate> emailTemplateRepository)
        {
            _emailTemplateRepository = emailTemplateRepository;
        }

        public int CreateEmailTemplate(EmailTemplate model)
        {
            _emailTemplateRepository.Insert(model);
            return model.Id;
        }

        public void UpdateEmailTemplate(EmailTemplate model)
        {
            _emailTemplateRepository.Update(model);
        }

        public void DeleteEmailTemplate(EmailTemplate model)
        {
            _emailTemplateRepository.Delete(model);
        }

        public void DeleteEmailTemplate(int id)
        {
            _emailTemplateRepository.Delete(id);
        }

        public EmailTemplate GetEmailTemplate(int id)
        {
            return _emailTemplateRepository.GetById(id);
        }

        public FilterModel<EmailTemplateViewModel> GetEmailTemplateWithFilter(int pageSize, int page, string filter)
        {
            var layoutFilterViewModel = new FilterModel<EmailTemplateViewModel>();
            var layoutList = GetLogViewModelList(_emailTemplateRepository.GetAll().OrderBy(x => x.Id));
            if (filter != string.Empty)
                layoutFilterViewModel.Records =
                    layoutList.Where(
                        (x =>
                            x.Title.ToLower().Contains(filter.ToLower()) || x.Name.ToLower().Contains(filter.ToLower())))
                              .Skip(pageSize*page)
                              .Take(pageSize)
                              .ToList();
            else
                layoutFilterViewModel.Records =
                    layoutList.Skip(pageSize*page).OrderBy(x => x.Id).Take(pageSize).ToList();
            layoutFilterViewModel.CurrentPage = page;
            layoutFilterViewModel.DisplayingRecords = layoutList.Count();
            layoutFilterViewModel.TotalRecords = layoutList.Count();
            if (filter != string.Empty)
                layoutFilterViewModel.TotalPages =
                    (layoutList.Count(x => x.Name.Contains(filter) || x.Title.ToLower().Contains(filter.ToLower())) +
                     pageSize - 1)/pageSize;
            else
                layoutFilterViewModel.TotalPages = (layoutFilterViewModel.TotalRecords + pageSize - 1)/pageSize;
            layoutFilterViewModel.NextEnable = layoutFilterViewModel.CurrentPage < layoutFilterViewModel.TotalPages - 1;
            layoutFilterViewModel.PreviousEnable = layoutFilterViewModel.CurrentPage > 0;
            return layoutFilterViewModel;
        }

        public ICollection<EmailTemplateViewModel> GetLogViewModelList(IEnumerable<EmailTemplate> emailTemplatesList)
        {
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            return
                emailTemplatesList.Select(
                    emailTemplate =>
                        new EmailTemplateViewModel
                        {
                            Id = emailTemplate.Id,
                            Title = emailTemplate.Title,
                            Name = emailTemplate.Name,
                            Template = emailTemplate.Template,
                            EditUrl =
                                urlHelper.Action(MVC.Admin.EmailTemplate.ActionNames.EditEmailTemplate,
                                    MVC.Admin.EmailTemplate.Name, new {templateId = emailTemplate.Id})
                        }).ToList();
        }
    }
}