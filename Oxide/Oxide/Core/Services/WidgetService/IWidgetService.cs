﻿using System.Collections.Generic;
using Oxide.Areas.Admin.ViewModels;
using Oxide.Core.Base.OxideDependency;
using Oxide.Core.Elements;
using Oxide.Core.Models.Layouts;
using Oxide.Core.Models.Pages;
using Oxide.Core.ResultModels;
using Oxide.Core.ViewModels.FilterViewModels;
using Oxide.Core.ViewModels.WidgeViewModels;

namespace Oxide.Core.Services.WidgetService
{
    public interface IWidgetService : IOxideDependency
    {
        /// <summary>
        ///     Gets view model collection of the items
        /// </summary>
        /// <param name="widgetList">Items to be convertted to view model collection</param>
        /// <returns>Widget view model collection</returns>
        ICollection<WidgetViewModel> GetWidgetViewModelList(IEnumerable<IOxideWidget> widgetList);
        /// <summary>
        ///     Gets widget list
        /// </summary>
        /// <param name="filter">Filter for getting widget list</param>
        /// <returns>Filter model for widget view model</returns>
        FilterModel<WidgetViewModel> WidgetList(string filter);
        /// <summary>
        ///     Changes status of the widget
        /// </summary>
        /// <param name="widgetKey">Key of the widget</param>
        /// <param name="status">Status of the widget</param>
        /// <returns>Boolean as new status</returns>
        bool ChangeStatus(string widgetKey, bool status);
        /// <summary>
        ///     Adds widget to page
        /// </summary>
        /// <param name="pageId">Id of the page which widget will be added</param>
        /// <param name="layoutId">Layout id of the layout inside the page</param>
        /// <param name="elementId">Element id of the element inside the layout</param>
        /// <param name="widgetKey">Key of the widget</param>
        PageWidgetViewModel AddWidgetToPage(int pageId, int layoutId, int elementId, string widgetKey);
        /// <summary>
        ///     Updates widget on page
        /// </summary>
        /// <param name="id">Id of the widget on the container page</param>
        /// <param name="pageId">Id of the page which widget will be updated</param>
        /// <param name="layoutId">Layout id of the layout inside the page</param>
        /// <param name="elementId">Element id of the element inside the layout</param>
        /// <param name="widgetKey">Key of the widget</param>
        /// <param name="orderList">Order of the widget</param>
        void UpdateWidgetOnPage(int id, int pageId, int layoutId, int elementId, string widgetKey, string orderList);
        /// <summary>
        ///     Deletes widgets from page
        /// </summary>
        /// <param name="id">Id of the widget</param>
        void DeleteWidgetFromPage(int id);
        /// <summary>
        ///     Adds to widget to main layout
        /// </summary>
        /// <param name="layoutId">Id of the layout in the main layout</param>
        /// <param name="elementId">Id of the element in the layout</param>
        /// <param name="widgetKey">Key of the widget</param>
        /// <param name="culture">Culture of the layout</param>
        PageWidgetViewModel AddWidgetToMainLayout(int layoutId, int elementId, string widgetKey, int culture);
        /// <summary>
        ///     Updates widget on layout
        /// </summary>
        /// <param name="id">Id of the widget on the container page</param>
        /// <param name="layoutId">Layout id of the layout inside the page</param>
        /// <param name="elementId">Element id of the element inside the layout</param>
        /// <param name="widgetKey">Key of the widget</param>
        /// <param name="orderList">Order of the widget</param>
        void UpdateWidgetOnMainLayout(int id, int layoutId, int elementId, string widgetKey, string orderList);
        /// <summary>
        ///     Deletes widget from the main layout
        /// </summary>
        /// <param name="id">Id of the widget</param>
        void DeleteWidgetFromMainLayout(int id);
        /// <summary>
        ///     Gets the page widgets as page widget view model accoring to element in the page
        /// </summary>
        /// <param name="pageId">Id of the page</param>
        /// <param name="elementId">Id of the element</param>
        /// <returns>Page widget view model as list</returns>
        List<PageWidgetViewModel> GetPageWidgets(int pageId, int elementId);
        /// <summary>
        ///     Gets the main layout widget according to main layout and element which is inside the layout
        /// </summary>
        /// <param name="mainLayoutId">Id of the main layout</param>
        /// <param name="elementId">Id of the element</param>
        /// <param name="culture">Culture of the layout</param>
        /// <returns>Page wdget view model as list</returns>
        List<PageWidgetViewModel> GetMainLayoutWidgets(int mainLayoutId, int elementId, int culture);
        /// <summary>
        ///     Gets widgets of the page
        /// </summary>
        /// <param name="pageId">Id of the page</param>
        /// <param name="elementId">Id of the element</param>
        /// <returns>Page widgets as list</returns>
        List<PageWidget> GetPageWidgetsForRender(int pageId, int elementId);
        /// <summary>
        ///     Gets widgets of the main layout
        /// </summary>
        /// <param name="layoutId">Id of the layout</param>
        /// <param name="elementId">Id of the element</param>
        /// <param name="culture">Culture of the layout</param>
        /// <param name="pageId">Id of the page</param>
        /// <returns>Main layout widgets as list</returns>
        List<MainLayoutWidget> GetMainLayoutWidgetsForRender(int layoutId, int elementId, int culture, int pageId);
        /// <summary>
        ///     Gets widget of the page
        /// </summary>
        /// <param name="id">Id of the widget</param>
        /// <returns>Widget as page widget</returns>
        PageWidget GetPageWidget(int id);
        /// <summary>
        ///     Gets main layout widget
        /// </summary>
        /// <param name="id">Id of the widget</param>
        /// <returns>Widget as main layout widget</returns>
        MainLayoutWidget GetMainLayoutWidget(int id);
        /// <summary>
        ///     Updates title of the widget
        /// </summary>
        /// <param name="widgetId">Id of the widget</param>
        /// <param name="title">Title of the widget</param>
        void UpdateWidgetTitle(int widgetId, string title);
        /// <summary>
        ///     Updates title of the widget
        /// </summary>
        /// <param name="widgetId">Id of the widget</param>
        /// <param name="title">Title of the widget</param>
        void UpdateMainLayotWidgetTitle(int widgetId, string title);
        /// <summary>
        ///     Inserts main layout widget
        /// </summary>
        /// <param name="layoutId">Id of the layout in the main layout</param>
        /// <param name="elementId">Id of the element in the layout</param>
        /// <param name="widgetKey">Key of the widget</param>
        /// <param name="culture">Culture of the layout</param>
        /// <param name="order">Order of the widget</param>
        /// <param name="title">Title of the widget</param>
        WidgetInfoPackage InsertMainLayoutWidget(int layoutId, int elementId, string widgetKey, int culture, int order,
            string title);
        /// <summary>
        ///     Inserts page widget
        /// </summary>
        /// <param name="pageId">Id of the page which widget will be added</param>
        /// <param name="layoutId">Layout id of the layout inside the page</param>
        /// <param name="elementId">Element id of the element inside the layout</param>
        /// <param name="widgetKey">Key of the widget</param>
        /// <param name="order">Order of the widget</param>
        /// <param name="title">Title of the widget</param>
        WidgetInfoPackage InsertPageWidget(int pageId, int layoutId, int elementId, string widgetKey, int order,
            string title);
    }
}