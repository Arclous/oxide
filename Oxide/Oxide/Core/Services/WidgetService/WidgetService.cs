﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Oxide.Areas.Admin.ViewModels;
using Oxide.Core.Data.Repository;
using Oxide.Core.Elements;
using Oxide.Core.Extesions;
using Oxide.Core.Helpers;
using Oxide.Core.Managers.WidgetManager;
using Oxide.Core.Models.Layouts;
using Oxide.Core.Models.Pages;
using Oxide.Core.Models.Widgets;
using Oxide.Core.ResultModels;
using Oxide.Core.Syntax;
using Oxide.Core.ViewModels.FilterViewModels;
using Oxide.Core.ViewModels.WidgeViewModels;

namespace Oxide.Core.Services.WidgetService
{
    public class WidgetService : IWidgetService
    {
        private readonly IOxideRepository<MainLayoutWidget> _mainLayoutWidgetRepository;
        private readonly IOxideRepository<PageWidget> _pageWidgetRepository;
        private readonly IWidgetManager _widgetManager;
        private readonly IOxideRepository<Widget> _widgetRepository;

        public WidgetService()
        {
        }

        public WidgetService(IOxideRepository<PageWidget> pageWidgetRepository,
            IOxideRepository<Widget> widgetRepository, IOxideRepository<MainLayoutWidget> mainLayoutWidgetRepository,
            IWidgetManager widgetManager)
        {
            _pageWidgetRepository = pageWidgetRepository;
            _widgetRepository = widgetRepository;
            _mainLayoutWidgetRepository = mainLayoutWidgetRepository;
            _widgetManager = widgetManager;
        }

        public FilterModel<WidgetViewModel> WidgetList(string filter)
        {
            ICollection<WidgetViewModel> widgetList;
            var widgetFilterViewModel = new FilterModel<WidgetViewModel>();
            if (filter != null)
            {
                if (filter != string.Empty)
                {
                    widgetList =
                        GetWidgetViewModelList(
                            OxideWidgetHelper.GetWidgetList()
                                             .Where(
                                                 x =>
                                                     x.Name.ToLower().Contains(filter.ToLower()) ||
                                                     x.Title.ToLower().Contains(filter.ToLower()) ||
                                                     x.Category.ToLower().Contains(filter.ToLower()) ||
                                                     x.Description.ToLower().Contains(filter.ToLower()))
                                             .OrderBy(x => x.Name));
                    widgetFilterViewModel.Records = widgetList;
                }
                else
                {
                    widgetList = GetWidgetViewModelList(OxideWidgetHelper.GetWidgetList().OrderBy(x => x.Name));
                    widgetFilterViewModel.Records = widgetList;
                }
            }
            else
            {
                widgetList = GetWidgetViewModelList(OxideWidgetHelper.GetWidgetList().OrderBy(x => x.Name));
                widgetFilterViewModel.Records = widgetList;
            }
            return widgetFilterViewModel;
        }

        public ICollection<WidgetViewModel> GetWidgetViewModelList(IEnumerable<IOxideWidget> widgetList)
        {
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            return
                widgetList.Select(
                    widget =>
                        new WidgetViewModel
                        {
                            Description = widget.Description,
                            Name = widget.Name,
                            Title = widget.Title,
                            Category = widget.Category,
                            PreviewImageUrl = widget.PreviewImageUrl,
                            Active = IsWidgetActive(widget.WidgetKey),
                            InfoUrl = "",
                            Dependencies = widget.Dependencies?.Split(Constants.Splitter) ?? new string[] {},
                            WidgetKey = widget.WidgetKey,
                            ChangeStatusUrl =
                                urlHelper.Action(MVC.Admin.Widget.ActionNames.ChangeStatus, MVC.Admin.Widget.Name,
                                    new {area = Constants.DefaultArea})
                        }).ToList();
        }

        public List<PageWidgetViewModel> GetPageWidgets(int pageId, int elementId)
        {
            return
                GetPageWidgetViewModelList(
                    _pageWidgetRepository.SearchFor(x => x.PageId == pageId && x.LayoutKeyId == elementId)
                                         .OrderBy(x => x.Order), pageId);
        }

        public List<PageWidgetViewModel> GetMainLayoutWidgets(int mainLayoutId, int elementId, int culture)
        {
            return
                GetMainLayoutWidgetViewModelList(
                    _mainLayoutWidgetRepository.SearchFor(
                        x => x.LayoutId == mainLayoutId && x.LayoutKeyId == elementId && x.Culture == culture)
                                               .OrderBy(x => x.Order), mainLayoutId, culture);
        }

        public List<PageWidget> GetPageWidgetsForRender(int pageId, int elementId)
        {
            return
                _pageWidgetRepository.GetAll()
                                     .Where(x => x.PageId == pageId && x.LayoutKeyId == elementId)
                                     .OrderBy(x => x.Order)
                                     .ToList();
        }

        public List<MainLayoutWidget> GetMainLayoutWidgetsForRender(int layoutId, int elementId, int culture, int pageId)
        {
            var widgets =
                _mainLayoutWidgetRepository.GetAll()
                                           .Where(
                                               x =>
                                                   x.LayoutId == layoutId && x.LayoutKeyId == elementId &&
                                                   x.Culture == culture)
                                           .OrderBy(x => x.Order)
                                           .ToList();

            //Assing caller page id
            widgets.ForEach(widget => widget.MainPageId = pageId);
            return widgets;
        }

        public PageWidget GetPageWidget(int id)
        {
            return _pageWidgetRepository.GetById(id);
        }

        public MainLayoutWidget GetMainLayoutWidget(int id)
        {
            return _mainLayoutWidgetRepository.GetById(id);
        }

        public void UpdateWidgetTitle(int widgetId, string title)
        {
            var widget = _pageWidgetRepository.GetById(widgetId);
            widget.Title = title;
            _pageWidgetRepository.Update(widget);
        }

        public void UpdateMainLayotWidgetTitle(int widgetId, string title)
        {
            var widget = _mainLayoutWidgetRepository.GetById(widgetId);
            widget.Title = title;
            _mainLayoutWidgetRepository.Update(widget);
        }

        public WidgetInfoPackage InsertMainLayoutWidget(int layoutId, int elementId, string widgetKey, int culture,
            int order, string title)
        {
            var containerPageId = -1;
            var widget = OxideWidgetHelper.GetWidget(x => x.WidgetKey == widgetKey);
            var mainLayoutWidget = new MainLayoutWidget
            {
                LayoutId = layoutId,
                LayoutKeyId = elementId,
                WidgetKey = widgetKey,
                Culture = culture
            };
            if (widget != null)
            {
                mainLayoutWidget.Name = widget.Name;
                mainLayoutWidget.Title = title;
                mainLayoutWidget.Order = order;
                _mainLayoutWidgetRepository.Insert(mainLayoutWidget);
            }
            if (widget == null) return null;
            var widgetInfoPackage = new WidgetInfoPackage
            {
                WidgetId = mainLayoutWidget.Id,
                CurrentLanguage = culture,
                PageId = containerPageId,
                WidgetKey = widget.WidgetKey,
                LayoutId = layoutId,
                LayoutKeyId = elementId,
                Title = title,
                Regular = containerPageId != -1
            };
            widgetInfoPackage.UniqeHash = widgetInfoPackage.GetUniqueHash();
            return widgetInfoPackage;
        }

        public WidgetInfoPackage InsertPageWidget(int pageId, int layoutId, int elementId, string widgetKey, int order,
            string title)
        {
            var widgetCulture = -1;
            var widget = OxideWidgetHelper.GetWidget(x => x.WidgetKey == widgetKey);
            var pageWidget = new PageWidget
            {
                LayoutId = layoutId,
                LayoutKeyId = elementId,
                PageId = pageId,
                WidgetKey = widgetKey
            };
            if (widget != null)
            {
                pageWidget.Name = widget.Name;
                pageWidget.Title = title;
                pageWidget.Order = order;
                _pageWidgetRepository.Insert(pageWidget);
            }
            if (widget == null) return null;
            var widgetInfoPackage = new WidgetInfoPackage
            {
                WidgetId = pageWidget.Id,
                CurrentLanguage = widgetCulture,
                PageId = pageId,
                MainPageId = pageId,
                WidgetKey = widget.WidgetKey,
                LayoutId = layoutId,
                LayoutKeyId = elementId,
                Title = title,
                Regular = pageId != -1
            };
            widgetInfoPackage.UniqeHash = widgetInfoPackage.GetUniqueHash();
            return widgetInfoPackage;
        }

        public bool ChangeStatus(string widgetKey, bool status)
        {
            if (!status)
            {
                var widget = new Widget {WidgetKey = widgetKey};
                var widgetInfo = Oxide.LoadedWidgets.FirstOrDefault(x => x.WidgetKey == widgetKey);
                if (widgetInfo != null)
                {
                    widget.Name = widgetInfo.Name;
                    widget.Title = widgetInfo.Title;
                }
                _widgetRepository.Insert(widget);

                //Activate Dependencies
                if (widgetInfo?.Dependencies != null) ActivateDependencies(widgetInfo.Dependencies);
                return true;
            }
            else
            {
                var widget = _widgetRepository.SearchFor(x => x.WidgetKey == widgetKey).FirstOrDefault();
                _widgetRepository.Delete(widget);
                return false;
            }
        }

        public PageWidgetViewModel AddWidgetToPage(int pageId, int layoutId, int elementId, string widgetKey)
        {
            var widget = OxideWidgetHelper.GetWidget(x => x.WidgetKey == widgetKey);
            var pageWidget = new PageWidget
            {
                LayoutId = layoutId,
                LayoutKeyId = elementId,
                PageId = pageId,
                WidgetKey = widgetKey
            };
            if (widget != null)
            {
                pageWidget.Name = widget.Name;
                pageWidget.Title = widget.Title;
                pageWidget.Order =
                    _pageWidgetRepository.TotalRecords(x => x.LayoutKeyId == elementId && x.LayoutId == layoutId) + 1;
                _pageWidgetRepository.Insert(pageWidget);
            }
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            var pageWidgetViewModel = new PageWidgetViewModel
            {
                ManageWidgetUrl =
                    urlHelper.Action(MVC.Admin.Widget.ActionNames.RenderWidgetAdmin, MVC.Admin.Widget.Name,
                        new
                        {
                            area = Constants.DefaultArea,
                            widgetKey = pageWidget.WidgetKey,
                            widgetId = pageWidget.Id,
                            containerPageId = pageId,
                            layoutId = pageWidget.LayoutId,
                            layoutKeyId = pageWidget.LayoutKeyId,
                            dynamicTitle = pageWidget.Title
                        }),
                EditWidgetUrl =
                    urlHelper.Action(MVC.Admin.Widget.ActionNames.EditRegularWidget, MVC.Admin.Widget.Name,
                        new {area = Constants.DefaultArea, widgetId = pageWidget.Id, pageId = pageWidget.PageId})
            };
            if (widget != null) pageWidgetViewModel.PreviewImageUrl = urlHelper.Content(widget.PreviewImageUrl);
            pageWidget.CopyPropertiesTo(pageWidgetViewModel);
            return pageWidgetViewModel;
        }

        public void UpdateWidgetOnPage(int id, int pageId, int layoutId, int elementId, string widgetKey,
            string orderList)
        {
            var pageWidget = _pageWidgetRepository.Get(x => x.Id == id).FirstOrDefault();
            if (pageWidget != null)
            {
                pageWidget.LayoutKeyId = elementId;
                _pageWidgetRepository.LazyUpdate(pageWidget);
            }
            var ids = Array.ConvertAll(orderList.Split(Constants.Splitter), int.Parse);
            for (var dIndex = 0; dIndex <= ids.Length - 1; dIndex++)
            {
                var index = dIndex;
                var pageWidgetId = ids[index];
                var pageWidgetsToOrder = _pageWidgetRepository.Get(x => x.Id == pageWidgetId).FirstOrDefault();
                if (pageWidgetsToOrder != null)
                {
                    pageWidgetsToOrder.Order = dIndex;
                    _pageWidgetRepository.LazyUpdate(pageWidgetsToOrder);
                }
            }
            _pageWidgetRepository.SaveChanges();
        }

        public void DeleteWidgetFromPage(int id)
        {
            var widget = _pageWidgetRepository.Get(x => x.Id == id).FirstOrDefault();
            if (widget == null) return;
            _widgetManager.FireOnWidgetDeleted(widget);
            _pageWidgetRepository.Delete(widget.Id);
        }

        public PageWidgetViewModel AddWidgetToMainLayout(int layoutId, int elementId, string widgetKey, int culture)
        {
            var widget = OxideWidgetHelper.GetWidget(x => x.WidgetKey == widgetKey);
            var mainLayoutWidget = new MainLayoutWidget
            {
                LayoutId = layoutId,
                LayoutKeyId = elementId,
                WidgetKey = widgetKey,
                Culture = culture
            };
            if (widget != null)
            {
                mainLayoutWidget.Name = widget.Name;
                mainLayoutWidget.Title = widget.Title;
                mainLayoutWidget.Order =
                    _mainLayoutWidgetRepository.TotalRecords(
                        x => x.LayoutKeyId == elementId && x.LayoutId == layoutId && x.Culture == culture) + 1;
                _mainLayoutWidgetRepository.Insert(mainLayoutWidget);
            }
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            var layoutName = Oxide.LoadedLayouts.FirstOrDefault(x => x.Id == layoutId).Name;
            var pageWidgetViewModel = new PageWidgetViewModel
            {
                ManageWidgetUrl =
                    urlHelper.Action(MVC.Admin.Widget.ActionNames.RenderMainWidgetAdmin, MVC.Admin.Widget.Name,
                        new
                        {
                            area = Constants.DefaultArea,
                            widgetKey = mainLayoutWidget.WidgetKey,
                            widgetId = mainLayoutWidget.Id,
                            layoutId = mainLayoutWidget.LayoutId,
                            layoutKeyId = mainLayoutWidget.LayoutKeyId,
                            widgetCulture = culture
                        }),
                EditWidgetUrl =
                    urlHelper.Action(MVC.Admin.Widget.ActionNames.EditMainLayoutWidget, MVC.Admin.Widget.Name,
                        new
                        {
                            area = Constants.DefaultArea,
                            widgetId = mainLayoutWidget.Id,
                            pageId = mainLayoutWidget.LayoutId,
                            layoutName
                        })
            };
            if (widget != null) pageWidgetViewModel.PreviewImageUrl = urlHelper.Content(widget.PreviewImageUrl);
            mainLayoutWidget.CopyPropertiesTo(pageWidgetViewModel);
            return pageWidgetViewModel;
        }

        public void UpdateWidgetOnMainLayout(int id, int layoutId, int elementId, string widgetKey, string orderList)
        {
            var pageWidget = _mainLayoutWidgetRepository.Get(x => x.Id == id).FirstOrDefault();
            if (pageWidget != null)
            {
                pageWidget.LayoutKeyId = elementId;
                _mainLayoutWidgetRepository.LazyUpdate(pageWidget);
            }
            var ids = orderList.Split(Constants.Splitter);
            for (var dIndex = 0; dIndex <= ids.Length - 1; dIndex++)
            {
                var index = dIndex;
                var pageWidgetId = Convert.ToInt32(ids[index]);
                var pageWidgetsToOrder = _mainLayoutWidgetRepository.Get(x => x.Id == pageWidgetId).FirstOrDefault();
                if (pageWidgetsToOrder != null)
                {
                    pageWidgetsToOrder.Order = dIndex;
                    _mainLayoutWidgetRepository.LazyUpdate(pageWidgetsToOrder);
                }
            }
            _pageWidgetRepository.SaveChanges();
        }

        public void DeleteWidgetFromMainLayout(int id)
        {
            var widget = _mainLayoutWidgetRepository.Get(x => x.Id == id).FirstOrDefault();
            if (widget == null) return;
            _widgetManager.FireOnWidgetDeleted(widget);
            _mainLayoutWidgetRepository.Delete(widget.Id);
        }

        private void ActivateDependencies(string dependencies)
        {
            var widgetsToActivate = dependencies.Split(Constants.Splitter);
            var widgets =
                Oxide.LoadedWidgets.Where(x => widgetsToActivate.Contains(x.Name) && x.Active == false).ToList();
            foreach (var widget in widgets)
                ChangeStatus(widget.WidgetKey, false);
        }

        private List<PageWidgetViewModel> GetPageWidgetViewModelList(IEnumerable<PageWidget> widgetList, int pageId)
        {
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            return widgetList.Select(widget =>
            {
                var firstOrDefault = OxideWidgetHelper.GetWidgetList().FirstOrDefault(x => x.Name == widget.Name);
                return firstOrDefault != null
                    ? new PageWidgetViewModel
                    {
                        Id = widget.Id,
                        Name = widget.Name,
                        Title = widget.Title,
                        Active = IsWidgetActive(widget.WidgetKey),
                        WidgetKey = widget.WidgetKey,
                        Order = widget.Order,
                        Description = firstOrDefault.Description,
                        InfoUrl = "",
                        PreviewImageUrl = urlHelper.Content(firstOrDefault.PreviewImageUrl),
                        ManageWidgetUrl =
                            urlHelper.Action(MVC.Admin.Widget.ActionNames.RenderWidgetAdmin, MVC.Admin.Widget.Name,
                                new
                                {
                                    area = Constants.DefaultArea,
                                    widgetKey = widget.WidgetKey,
                                    widgetId = widget.Id,
                                    containerPageId = pageId,
                                    layoutId = widget.LayoutId,
                                    layoutKeyId = widget.LayoutKeyId,
                                    dynamicTitle = widget.Title
                                }),
                        EditWidgetUrl =
                            urlHelper.Action(MVC.Admin.Widget.ActionNames.EditRegularWidget, MVC.Admin.Widget.Name,
                                new {area = Constants.DefaultArea, widgetId = widget.Id, pageId = widget.PageId})
                    }
                    : null;
            }).OrderBy(x => x.Order).ToList();
        }

        private List<PageWidgetViewModel> GetMainLayoutWidgetViewModelList(IEnumerable<MainLayoutWidget> widgetList,
            int mainLayoutId, int culture)
        {
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            var layoutName = Oxide.LoadedLayouts.FirstOrDefault(x => x.Id == mainLayoutId).Name;
            return widgetList.Select(widget =>
            {
                var firstOrDefault = OxideWidgetHelper.GetWidget(x => x.Name == widget.Name);
                return firstOrDefault != null
                    ? new PageWidgetViewModel
                    {
                        Id = widget.Id,
                        Name = widget.Name,
                        Title = widget.Title,
                        Active = IsWidgetActive(widget.WidgetKey),
                        WidgetKey = widget.WidgetKey,
                        Order = widget.Order,
                        Description = firstOrDefault.Description,
                        InfoUrl = "",
                        PreviewImageUrl = urlHelper.Content(firstOrDefault.PreviewImageUrl),
                        ManageWidgetUrl =
                            urlHelper.Action(MVC.Admin.Widget.ActionNames.RenderMainWidgetAdmin, MVC.Admin.Widget.Name,
                                new
                                {
                                    area = Constants.DefaultArea,
                                    widgetKey = widget.WidgetKey,
                                    widgetId = widget.Id,
                                    layoutId = widget.LayoutId,
                                    layoutKeyId = widget.LayoutKeyId,
                                    widgetCulture = culture
                                }),
                        EditWidgetUrl =
                            urlHelper.Action(MVC.Admin.Widget.ActionNames.EditMainLayoutWidget, MVC.Admin.Widget.Name,
                                new
                                {
                                    area = Constants.DefaultArea,
                                    widgetId = widget.Id,
                                    pageId = widget.LayoutId,
                                    layoutName
                                })
                    }
                    : null;
            }).OrderBy(x => x.Order).ToList();
        }

        protected bool IsWidgetActive(string widgetKey)
        {
            return _widgetRepository.GetAll().Any(x => x.WidgetKey == widgetKey);
        }
    }
}