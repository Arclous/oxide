﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System.Collections.Generic;
using System.Linq;
using Castle.Components.DictionaryAdapter;
using Oxide.Core.Data.Repository;
using Oxide.Core.Extesions;
using Oxide.Core.Models.AdminPermission;
using Oxide.Core.Models.AdminRole;
using Oxide.Core.Services.ResourceService;
using Oxide.Core.Syntax;
using Oxide.Core.ViewModels.AdminPermisionViewModels;

namespace Oxide.Core.Services.AdminPermissionService
{
    public class AdminPermissionServices : IAdminPermissionService
    {
        private readonly IOxideRepository<AdminRolePermission> _adminRolePermissionRepository;
        private readonly IOxideRepository<AdminPermission> _permissionRepository;
        private readonly IResourceService _resourceService;

        public AdminPermissionServices()
        {
        }

        public AdminPermissionServices(IOxideRepository<AdminPermission> permissionRepository,
            IOxideRepository<AdminRolePermission> adminRolePermissionRepository)
        {
            _permissionRepository = permissionRepository;
            _adminRolePermissionRepository = adminRolePermissionRepository;
        }

        public AdminPermissionServices(IOxideRepository<AdminPermission> permissionRepository,
            IOxideRepository<AdminRolePermission> adminRolePermissionRepository, IResourceService resourceService)
        {
            _permissionRepository = permissionRepository;
            _adminRolePermissionRepository = adminRolePermissionRepository;
            _resourceService = resourceService;
        }

        public void CreateAdminPermission(AdminPermission model)
        {
            _permissionRepository.Insert(model);
        }

        public void UpdateAdminPermission(AdminPermission model)
        {
            var updateMode = _permissionRepository.SearchFor(x => x.Id == model.Id).FirstOrDefault();
            model.CopyPropertiesTo(updateMode, new[] {"Id"});
            _permissionRepository.Update(updateMode);
        }

        public void DeleteAdminPermission(AdminPermission model)
        {
            _permissionRepository.Delete(model);
        }

        public void DeleteAdminPermission(int id)
        {
            _permissionRepository.Delete(id);
        }

        public void DeleteAdminPermission(string key)
        {
            var permission = _permissionRepository.Get(x => x.Key == key).FirstOrDefault();
            _permissionRepository.Delete(permission);
        }

        public void DeleteAllPermissions(string moduleName)
        {
            _permissionRepository.BulkDeleteAll(x => x.Owner == moduleName);
        }

        public AdminPermission GetAdminPermission(int id)
        {
            return _permissionRepository.GetById(id);
        }

        public IEnumerable<AdminPermission> GetAllAdminPermissions()
        {
            return _permissionRepository.GetAll().ToList();
        }

        public IEnumerable<AdminPermissionOwnerViewModel> GetAllAdminPermissionsOrganized(int? roleId)
        {
            var resultList = new List<AdminPermissionOwnerViewModel>();
            var ownerList = _permissionRepository.GetAll().Select(x => x.Owner).Distinct().ToList();
            List<int> activePermissionList = new EditableList<int>();
            if (roleId != null)
                activePermissionList =
                    _adminRolePermissionRepository.GetAll()
                                                  .Where(x => x.Role.Id == roleId)
                                                  .Select(x => x.Permission.Id)
                                                  .ToList();
            foreach (var owner in ownerList)
            {
                var ownerModel = new AdminPermissionOwnerViewModel
                {
                    Name = owner,
                    Groups = new EditableList<AdminPermissionGroupViewModel>()
                };
                foreach (var group in
                    _permissionRepository.GetAll().Where(o => o.Owner == owner).Select(x => x.Group).Distinct())
                {
                    var ownerGroup = new AdminPermissionGroupViewModel
                    {
                        Name =
                            ownerModel.Name != Constants.DefaultNameSpace
                                ? _resourceService.GetText(@group, ownerModel.Name)
                                : _resourceService.GetText(@group),
                        Permissions = new EditableList<AdminPermissionViewModel>()
                    };
                    foreach (var permission in
                        _permissionRepository.GetAll().Where(o => (o.Owner == owner) && (o.Group == group)))
                    {
                        var permissionItem = new AdminPermissionViewModel
                        {
                            Group =
                                ownerModel.Name != Constants.DefaultNameSpace
                                    ? _resourceService.GetText(@group, ownerModel.Name)
                                    : _resourceService.GetText(@group),
                            Owner = owner,
                            Key = permission.Key,
                            Id = permission.Id,
                            Name = permission.Name,
                            Title =
                                ownerModel.Name != Constants.DefaultNameSpace
                                    ? _resourceService.GetText(permission.Title, ownerModel.Name)
                                    : _resourceService.GetText(permission.Title),
                            Status = activePermissionList.Contains(permission.Id)
                        };
                        ownerGroup.Permissions.Add(permissionItem);
                    }
                    ownerModel.Groups.Add(ownerGroup);
                }
                resultList.Add(ownerModel);
            }
            return resultList;
        }

        public IEnumerable<AdminPermission> GetAllAdminPermissions(string moduleName)
        {
            return _permissionRepository.SearchFor(x => x.Owner == moduleName).ToList();
        }

        public bool IsAdminPermissionExist(int id)
        {
            return _permissionRepository.GetAll().Any(x => x.Id == id);
        }

        public bool IsAdminPermissionExist(string key)
        {
            return _permissionRepository.GetAll().Any(x => x.Key == key);
        }

        public bool IsAdminPermissionExist(string key, string moduleName)
        {
            return _permissionRepository.GetAll().Any(x => (x.Key == key) && (x.Owner == moduleName));
        }
    }
}