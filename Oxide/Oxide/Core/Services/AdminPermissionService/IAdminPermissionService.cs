﻿using System.Collections.Generic;
using Oxide.Core.Base.OxideDependency;
using Oxide.Core.Models.AdminPermission;
using Oxide.Core.ViewModels.AdminPermisionViewModels;

namespace Oxide.Core.Services.AdminPermissionService
{
    /// <summary>
    ///     Responsible service for permissions
    /// </summary>
    public interface IAdminPermissionService : IOxideDependency
    {
        /// <summary>
        ///     Creates adminpermission according to  model
        /// </summary>
        /// <param name="model">Admin permission model for page</param>
        void CreateAdminPermission(AdminPermission model);
        /// <summary>
        ///     Updates admin permission
        /// </summary>
        /// <param name="model">Admin permission model for permission</param>
        void UpdateAdminPermission(AdminPermission model);
        /// <summary>
        ///     Deletes admin permission according to model
        /// </summary>
        /// <param name="model">Model for admin permission</param>
        void DeleteAdminPermission(AdminPermission model);
        /// <summary>
        ///     Deletes admin permission according to id
        /// </summary>
        /// <param name="id">Id of the admin permission</param>
        void DeleteAdminPermission(int id);
        /// <summary>
        ///     Deletes admin permission according to key
        /// </summary>
        /// <param name="key">Key of the admin permission</param>
        void DeleteAdminPermission(string key);
        /// <summary>
        ///     Deletes all admin permissions which are belongs to given module name
        /// </summary>
        /// <param name="moduleName">Name of the owner module</param>
        void DeleteAllPermissions(string moduleName);
        /// <summary>
        ///     Gets admin permission according to id
        /// </summary>
        /// <param name="id">Id of the admin permission</param>
        AdminPermission GetAdminPermission(int id);
        /// <summary>
        ///     Gets all permissions
        /// </summary>
        IEnumerable<AdminPermission> GetAllAdminPermissions();
        /// <summary>
        ///     Gets all permissions as organized
        /// </summary>
        /// <param name="roleId">Id of the admin role which is currently viewing</param>
        IEnumerable<AdminPermissionOwnerViewModel> GetAllAdminPermissionsOrganized(int? roleId);
        /// <summary>
        ///     Gets all admin permissions which are belongs to given module name
        /// </summary>
        /// <param name="moduleName">Name of the owner module</param>
        IEnumerable<AdminPermission> GetAllAdminPermissions(string moduleName);
        /// <summary>
        ///     Checks admin permission according to id if it is exist
        /// </summary>
        /// <param name="id">Id of the admin permission</param>
        bool IsAdminPermissionExist(int id);
        /// <summary>
        ///     Checks admin permission according to key if it is exist
        /// </summary>
        /// <param name="key">Key of the admin permission</param>
        bool IsAdminPermissionExist(string key);
        /// <summary>
        ///     Checks admin permission according to key if it is exist
        /// </summary>
        /// <param name="key">Key of the admin permission</param>
        /// <param name="moduleName">Name of the owner module</param>
        bool IsAdminPermissionExist(string key, string moduleName);
    }
}