﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Diagnostics;
using System.Web.Mvc;
using Oxide.Core.Helpers;
using Oxide.Core.Models.Log;
using Oxide.Core.Services.LogService;

namespace Oxide.Core.Base.OxideActionFilters
{
    public class LoggingExceptionFilter : IExceptionFilter
    {
        private readonly IExceptionFilter _filter;
        private readonly ILogService _logWriter;
        public LoggingExceptionFilter(IExceptionFilter filter, ILogService logWriter)
        {
            _filter = filter;
            _logWriter = logWriter;
        }
        public void OnException(ExceptionContext filterContext)
        {
            var log = new Log
            {
                DateTimeInfo = DateTime.Now,
                LogType = Syntax.Syntax.LogTypes.Error,
                Title = filterContext.Exception.Message,
                ModuleName = OxideUtils.GetCallerModuleName(new StackFrame(1)),
                Description = filterContext.Exception.StackTrace
            };
            if (filterContext.Exception.InnerException != null)
                log.InnerException = filterContext.Exception.InnerException.Message;
            _logWriter.CreateLog(log);
            _filter.OnException(filterContext);
        }
    }
}