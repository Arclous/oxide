﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.Managers.SettingsManager;
using Oxide.Core.Services.LoginService;
using Oxide.Core.Syntax;

namespace Oxide.Core.Base.OxideActionFilters
{
    public class OxideAuthorizeAttribute : AuthorizeAttribute
    {
        public string AccessLevel { get; set; }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var providerManager = new ProviderManager();
            var loginService = providerManager.Provide<ILoginService>();
            return loginService.IsUserLogined() && SettingsManager.IsAdmin();
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (filterContext.IsChildAction)
                base.HandleUnauthorizedRequest(filterContext);
            else
            {
                filterContext.Result =
                    new RedirectToRouteResult(
                        new RouteValueDictionary(
                            new
                            {
                                controller = MVC.Admin.Login.Name,
                                action = MVC.Admin.Login.ActionNames.LoginPage,
                                area = Constants.DefaultArea,
                                returnUrl = filterContext.RequestContext.HttpContext.Request.UrlReferrer
                            }));
            }
        }
    }
}