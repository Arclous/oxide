﻿using System.Net;
using System.Web.Mvc;

namespace Oxide.Core.Base.OxideHttpResult
{
    public class Http403 : ActionResult
    {
        public override void ExecuteResult(ControllerContext context)
        {
            context.HttpContext.Response.StatusCode = (int) HttpStatusCode.Forbidden;
            context.HttpContext.Response.Write(
                "You dont have enough permission to complette this action, please contact with your administrator to extend your permission to complete this action.");
            context.HttpContext.Response.End();
            context.HttpContext.Response.SuppressFormsAuthenticationRedirect = true;
        }
    }
}