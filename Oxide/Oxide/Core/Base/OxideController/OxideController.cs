﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System.Text;
using System.Web.Mvc;
using Oxide.Core.Services.OxideService;

namespace Oxide.Core.Base.OxideController
{
    public abstract class OxideController : Controller
    {
        protected readonly IOxideServices OxideServices;
        protected OxideController()
        {
        }
        protected OxideController(IOxideServices oxideServices)
        {
            OxideServices = oxideServices;
        }
        /// <summary>
        ///     Override the JSON Result with Max integer JSON lenght
        /// </summary>
        /// <param name="data">Data</param>
        /// <param name="contentType">Content Type</param>
        /// <param name="contentEncoding">Content Encoding</param>
        /// <param name="behavior">Behavior</param>
        /// <returns>As JsonResult</returns>
        protected override JsonResult Json(object data, string contentType, Encoding contentEncoding,
            JsonRequestBehavior behavior)
        {
            return new JsonResult
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = int.MaxValue
            };
        }
    }
}