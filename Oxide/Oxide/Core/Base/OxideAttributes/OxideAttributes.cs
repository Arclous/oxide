﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Oxide.Core.Base.OxideHttpResult;
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.Services.LoginService;
using Oxide.Core.Syntax;

namespace Oxide.Core.Base.OxideAttributes
{
    public class OxideAttributes
    {
    }
    [AttributeUsage(AttributeTargets.Class)]
    public class OxideDatabase : Attribute
    {
        private string _contextName;
        public OxideDatabase(string contextName)
        {
            _contextName = contextName;
        }
    }
    public class OxideAdmin : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

        }
    }
    public class Front : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
    
        }
    }
    public class BlankFrame : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
      
        }
    }
    public class AdminLogin : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
  
        }
    }
    public class OxideAdminPermission : AuthorizeAttribute
    {
        public string Group { get; set; }
        public string Permission { get; set; }
        public string Key { get; set; }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            IProviderManager providerManager = new ProviderManager();
            var loginService = providerManager.Provide<ILoginService>();
            return (loginService.IsUserLogined()) && (loginService.IsSuperAdmin() || loginService.CheckPermission(Key));
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            IProviderManager providerManager = new ProviderManager();
            var loginService = providerManager.Provide<ILoginService>();
            if (!loginService.IsUserLogined()) return;
            if (filterContext.RequestContext.HttpContext.Request.IsAjaxRequest())
                filterContext.Result = new Http403();
            else
            {
                filterContext.Result =
                    new RedirectToRouteResult(
                        new RouteValueDictionary(
                            new
                            {
                                controller = MVC.Admin.Login.Name,
                                action = MVC.Admin.Login.ActionNames.AccessDenied,
                                area = Constants.DefaultArea,
                                returnUrl = filterContext.RequestContext.HttpContext.Request.UrlReferrer
                            }));
            }
        }
    }

}