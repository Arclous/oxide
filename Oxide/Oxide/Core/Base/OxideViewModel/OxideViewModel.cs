﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Oxide.Core.Base.OxideViewModel
{
    public class OxideViewModel : IOxideViewModel
    {
        [NotMapped]
        public int Id { get; set; }

        [NotMapped]
        public string Name { get; set; }

        [NotMapped]
        public string Title { get; set; }
    }
}