﻿namespace Oxide.Core.Base.OxideViewModel
{
    public class OxideWidgetViewModel : IOxideWidgetViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int PageId { get; set; }
        public int BasePageId { get; set; }
        public int CurrentLanguage { get; set; }
        public int UniqeHash { get; set; }
        public string WidgetKey { get; set; }
    }
}