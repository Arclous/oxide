﻿using System.Data.Entity;
using Oxide.Core.Data.Context;

namespace Oxide.Core.Base.OxideModelManager
{
    public interface IOxideModelManager
    {
        void OnModelCreating(DbModelBuilder modelBuilder);
        void Seed(OxideContext context);
    }
}