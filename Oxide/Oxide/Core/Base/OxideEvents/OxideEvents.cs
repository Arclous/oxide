﻿using System;
using System.Collections.Generic;
using Castle.MicroKernel.Registration;
using Oxide.Core.Base.OxideEvents.Events;
using Oxide.Core.Base.OxideEvents.Handlers;
using Oxide.Core.Managers.SettingsManager;

namespace Oxide.Core.Base.OxideEvents
{
    public static class OxideEvents
    {
        private static List<Delegate> _actions;
        public static IEventDispatcher Dispatcher { get; set; }
        public static void RegisterAction<T>(Action<T> callback) where T : IDomainEvent
        {
            _actions = _actions ?? new List<Delegate>();
            _actions.Add(callback);
        }
        public static void RegisterEvent<T>(Syntax.Syntax.EventTypes eventType)
        {
            if (!SettingsManager.IsAdmin()) return;
            var key = string.Format(@"{0}_{1}", typeof (T).Name, eventType);
            if (Oxide.Events.ContainsKey(key)) return;
            Oxide.Events.Add(key, typeof (T));
            switch (eventType)
            {
                case Syntax.Syntax.EventTypes.OnCreated:
                {
                    Oxide.Container.Register(
                        Component.For(typeof (IDomainHandler<OnCreated<T>>))
                            .ImplementedBy(typeof (OnCreatedHandler<T>))
                            .LifeStyle.PerWebRequest);
                    break;
                }
                case Syntax.Syntax.EventTypes.OnUpdated:
                {
                    Oxide.Container.Register(
                        Component.For(typeof (IDomainHandler<OnUpdated<T>>))
                            .ImplementedBy(typeof (OnUpdateHandler<T>))
                            .LifeStyle.PerWebRequest);
                    break;
                }
                case Syntax.Syntax.EventTypes.OnDeleted:
                {
                    Oxide.Container.Register(
                        Component.For(typeof (IDomainHandler<OnDeleted<T>>))
                            .ImplementedBy(typeof (OnDeleteHandler<T>))
                            .LifeStyle.PerWebRequest);
                    break;
                }
            }
        }
        public static void RegisterEvent(Type T, Syntax.Syntax.EventTypes eventType)
        {
            if (!SettingsManager.IsAdmin()) return;
            var key = string.Format(@"{0}_{1}", T.Name, eventType);
            if (Oxide.Events.ContainsKey(key)) return;
            Oxide.Events.Add(key, T);
            switch (eventType)
            {
                case Syntax.Syntax.EventTypes.OnCreated:
                {
                    var baseType = typeof (OnCreated<>);
                    var genericBaseType = baseType.MakeGenericType(T);
                    var instanceBaseType = Activator.CreateInstance(genericBaseType);
                    var domainHandlerType = typeof (IDomainHandler<>);
                    var genericDomainHandler = domainHandlerType.MakeGenericType(instanceBaseType.GetType());
                    var handlerType = typeof (OnCreatedHandler<>);
                    var genericHandlerType = handlerType.MakeGenericType(T);
                    Oxide.Container.Register(
                        Component.For(genericDomainHandler).ImplementedBy(genericHandlerType).LifeStyle.PerWebRequest);
                    break;
                }
                case Syntax.Syntax.EventTypes.OnUpdated:
                {
                    var baseType = typeof (OnUpdated<>);
                    var genericBaseType = baseType.MakeGenericType(T);
                    var instanceBaseType = Activator.CreateInstance(genericBaseType);
                    var domainHandlerType = typeof (IDomainHandler<>);
                    var genericDomainHandler = domainHandlerType.MakeGenericType(instanceBaseType.GetType());
                    var handlerType = typeof (OnUpdateHandler<>);
                    var genericHandlerType = handlerType.MakeGenericType(T);
                    Oxide.Container.Register(
                        Component.For(genericDomainHandler).ImplementedBy(genericHandlerType).LifeStyle.PerWebRequest);
                    break;
                }
                case Syntax.Syntax.EventTypes.OnDeleted:
                {
                    var baseType = typeof (OnDeleted<>);
                    var genericBaseType = baseType.MakeGenericType(T);
                    var instanceBaseType = Activator.CreateInstance(genericBaseType);
                    var domainHandlerType = typeof (IDomainHandler<>);
                    var genericDomainHandler = domainHandlerType.MakeGenericType(instanceBaseType.GetType());
                    var handlerType = typeof (OnDeleteHandler<>);
                    var genericHandlerType = handlerType.MakeGenericType(T);
                    Oxide.Container.Register(
                        Component.For(genericDomainHandler).ImplementedBy(genericHandlerType).LifeStyle.PerWebRequest);
                    break;
                }
            }
        }
        public static void ClearCallbacks()
        {
            _actions = null;
        }
        public static void Raise<T>(T @event) where T : IDomainEvent
        {
            Dispatcher?.Dispatch(@event);
            if (_actions == null) return;
            foreach (var action in _actions)
                if (action is Action<T>)
                {
                    ((Action<T>) action)(@event);
                }
        }
    }
}