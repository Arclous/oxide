﻿namespace Oxide.Core.Base.OxideEvents
{
    public interface IDomainHandler<T> where T : IDomainEvent
    {
        void Handle(T @event);
    }
}