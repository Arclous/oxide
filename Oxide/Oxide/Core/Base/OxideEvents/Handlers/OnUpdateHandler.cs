﻿using Castle.Core.Internal;
using Oxide.Core.Base.OxideEvents.Events;

namespace Oxide.Core.Base.OxideEvents.Handlers
{
    public class OnUpdateHandler<T> : IDomainHandler<OnUpdated<T>>
    {
        public void Handle(OnUpdated<T> @event)
        {
            Oxide.LoadedEvents.ForEach(instance => instance.OnModelUpdated(@event.Entity));
        }
    }
}