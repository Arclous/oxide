﻿using Castle.Core.Internal;
using Oxide.Core.Base.OxideEvents.Events;

namespace Oxide.Core.Base.OxideEvents.Handlers
{
    public class OnDeleteHandler<T> : IDomainHandler<OnDeleted<T>>
    {
        public void Handle(OnDeleted<T> @event)
        {
            Oxide.LoadedEvents.ForEach(instance => instance.OnModelDeleted(@event.Entity));
        }
    }
}