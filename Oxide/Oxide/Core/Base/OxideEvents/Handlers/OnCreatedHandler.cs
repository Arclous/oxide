﻿using Castle.Core.Internal;
using Oxide.Core.Base.OxideEvents.Events;

namespace Oxide.Core.Base.OxideEvents.Handlers
{
    public class OnCreatedHandler<T> : IDomainHandler<OnCreated<T>>
    {
        public void Handle(OnCreated<T> @event)
        {
            Oxide.LoadedEvents.ForEach(instance => instance.OnModelCreated(@event.Entity));
        }
    }
}