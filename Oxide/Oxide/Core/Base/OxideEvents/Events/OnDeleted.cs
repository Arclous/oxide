﻿namespace Oxide.Core.Base.OxideEvents.Events
{
    public class OnDeleted<T> : IDomainEvent
    {
        public object[] Entity { get; set; }
    }
}