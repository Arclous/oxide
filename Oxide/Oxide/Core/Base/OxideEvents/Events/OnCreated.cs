﻿namespace Oxide.Core.Base.OxideEvents.Events
{
    public class OnCreated<T> : IDomainEvent
    {
        public object[] Entity { get; set; }
    }
}