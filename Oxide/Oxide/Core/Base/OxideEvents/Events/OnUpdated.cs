﻿namespace Oxide.Core.Base.OxideEvents.Events
{
    public class OnUpdated<T> : IDomainEvent
    {
        public object[] Entity { get; set; }
    }
}