﻿using System.Linq;

namespace Oxide.Core.Base.OxideEvents
{
    public class OxideEventContainer : IEventDispatcher
    {
        public void Dispatch<TEvent>(TEvent eventToDispatch) where TEvent : IDomainEvent
        {
            var subscribers = Oxide.Container.ResolveAll<IDomainHandler<TEvent>>().ToList();
            try
            {
                subscribers.ForEach(s => s.Handle(eventToDispatch));
            }
            finally
            {
                subscribers.ForEach(s => Oxide.Container.Release(s));
            }
        }
    }
}