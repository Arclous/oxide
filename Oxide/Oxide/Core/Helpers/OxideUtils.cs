﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Oxide.Core.Base.OxideDependency;
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Syntax;

namespace Oxide.Core.Helpers
{
    public class OxideUtils : IOxideDependency
    {
        private static readonly IProviderManager ProviderManager = new ProviderManager();
        public static SelectList GetInnerLayoutSelectList()
        {
            return
                new SelectList(
                    Oxide.LoadedLayouts.Where(x => x.LayoutType == Syntax.Syntax.LayoutTypes.Inner)
                        .Select(x => new SelectListItem {Text = x.Name, Value = x.Id.ToString()}), "Value", "Text");
        }
        public static SelectList GetMainLayoutSelectList()
        {
            return
                new SelectList(
                    Oxide.LoadedLayouts.Where(x => x.LayoutType == Syntax.Syntax.LayoutTypes.Main)
                        .Select(x => new SelectListItem {Text = x.Name, Value = x.Id.ToString()}), "Value", "Text");
        }
        public static IList<SelectListItem> GetFrequncy()
        {
            return EnumHelper.GetSelectList(typeof (Syntax.Syntax.SiteMapFrequncy));
        }
        public static IList<SelectListItem> GetFrequncyWithTranslations()
        {
            var provider = new ProviderManager();
            var oxideService = provider.Provide<IOxideServices>();
            return
                EnumHelper.GetSelectList(typeof (Syntax.Syntax.SiteMapFrequncy))
                    .Select(v => new SelectListItem {Value = v.Value, Text = oxideService.GetText(v.Text)})
                    .ToList();
        }
        public static IList<SelectListItem> GetScheduleIntervls()
        {
            return EnumHelper.GetSelectList(typeof (Syntax.Syntax.ScheduleIntervals));
        }
        public static IList<SelectListItem> GetScheduleIntervalTypes()
        {
            return EnumHelper.GetSelectList(typeof (Syntax.Syntax.ScheduleIntervalTypes));
        }
        public static IList<SelectListItem> GetEventTypesWithTranslations(string value)
        {
            var provider = new ProviderManager();
            var oxideService = provider.Provide<IOxideServices>();
            return
                EnumHelper.GetSelectList(typeof (Syntax.Syntax.EventTypes))
                    .Select(
                        v =>
                            new SelectListItem
                            {
                                Value = v.Value,
                                Text = oxideService.GetText(v.Text),
                                Selected = (v.Value == value)
                            })
                    .ToList();
        }
        public static IList<SelectListItem> GetDomainRoles()
        {
            return EnumHelper.GetSelectList(typeof (Syntax.Syntax.ScheduledJobRunPlatforms));
        }
        public static IList<SelectListItem> GetRedirectionStatusList()
        {
            return EnumHelper.GetSelectList(typeof(Syntax.Syntax.RedirectionStatusCode));
        }

        #region Url
        public static string GetFullUrl(string url)
        {
            return url == string.Empty
                ? ""
                : $"{HttpContext.Current.Request.Url.Scheme}://{HttpContext.Current.Request.Url.Authority}{VirtualPathUtility.ToAbsolute(url)}";
        }
        public static string GetPureUrl(string url)
        {
            var urlBase = url;
            var pureUrl = urlBase;
            if (urlBase.IndexOf("?", StringComparison.Ordinal) > 0)
                pureUrl = urlBase.Substring(0, urlBase.IndexOf("?", StringComparison.Ordinal));
            return pureUrl;
        }
        #endregion

        #region Helpers
        public static IEnumerable<T> GetEnumValues<T>()
        {
            return Enum.GetValues(typeof (T)).Cast<T>();
        }
        public static string GetCallerModuleName(StackFrame stackFrame)
        {
            return stackFrame.GetMethod().Module.Name.Replace(Constants.Dll, Constants.Empty);
        }
        public static IList<SelectListItem> GetEnumListWithTranslaions(Type targetEnum)
        {
            //var oxideService =
            //    new OxideServices(new ResourceService(new CultureManager(),
            //        new SiteSettingsService(new OxideRepository<SiteSettings>(new OxideContext()))));
            var oxideService = ProviderManager.Provide<IOxideServices>();
            return
                EnumHelper.GetSelectList(targetEnum)
                    .Select(v => new SelectListItem {Value = v.Value, Text = oxideService.GetText(v.Text)})
                    .ToList();
        }
        public static double ConvertBytesToMegabytes(long bytes)
        {
            return (bytes/1024f)/1024f;
        }
        public static double ConvertKilobytesToMegabytes(long kilobytes)
        {
            return kilobytes/1024f;
        }
        public static string FillParameters(string template, Dictionary<string, string> keyWords)
        {
            return keyWords.Aggregate(template, (current, keyword) => current.Replace(keyword.Key, keyword.Value));
        }
        #endregion

        #region Security
        public static string ToSha256(string password)
        {
            var crypt = new SHA256Managed();
            var hash = new StringBuilder();
            var crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(password), 0, Encoding.UTF8.GetByteCount(password));
            foreach (var theByte in crypto)
            {
                hash.Append(theByte.ToString("x2"));
            }
            return hash.ToString();
        }
        private const string PasswordHash = "&$%^@P@@_)@Sw0rd";
        private const string SaltKey = "W$S%^C)&*@LT&KEY";
        private const string ViKey = "@1B2c3D4e5F6g7H8";
        public static string Encrypt(string text)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(text);
            var keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256/8);
            var symmetricKey = new RijndaelManaged {Mode = CipherMode.CBC, Padding = PaddingMode.Zeros};
            var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(ViKey));
            byte[] cipherTextBytes;
            using (var memoryStream = new MemoryStream())
            {
                using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                {
                    cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                    cryptoStream.FlushFinalBlock();
                    cipherTextBytes = memoryStream.ToArray();
                    cryptoStream.Close();
                }
                memoryStream.Close();
            }
            return Convert.ToBase64String(cipherTextBytes);
        }
        public static string Decrypt(string encryptedText)
        {
            try
            {
                var cipherTextBytes = Convert.FromBase64String(encryptedText);
                var keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256/8);
                var symmetricKey = new RijndaelManaged {Mode = CipherMode.CBC, Padding = PaddingMode.None};
                var decryptor = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(ViKey));
                var memoryStream = new MemoryStream(cipherTextBytes);
                var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
                var plainTextBytes = new byte[cipherTextBytes.Length];
                var decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                memoryStream.Close();
                cryptoStream.Close();
                return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount).TrimEnd("\0".ToCharArray());
            }
            catch (Exception)
            {
                return null;
            }
        }
        private const string Key = "ABC123DEF456GH78";
        private static byte[] GetByte(string data)
        {
            return Encoding.UTF8.GetBytes(data);
        }
        public static byte[] EncryptString(string data)
        {
            var byteData = GetByte(data);
            var algo = SymmetricAlgorithm.Create();
            algo.Key = GetByte(Key);
            algo.GenerateIV();
            var mStream = new MemoryStream();
            mStream.Write(algo.IV, 0, algo.IV.Length);
            var myCrypto = new CryptoStream(mStream, algo.CreateEncryptor(), CryptoStreamMode.Write);
            myCrypto.Write(byteData, 0, byteData.Length);
            myCrypto.FlushFinalBlock();
            return mStream.ToArray();
        }
        public static string DecryptString(byte[] data)
        {
            var algo = SymmetricAlgorithm.Create();
            algo.Key = GetByte(Key);
            var mStream = new MemoryStream();
            var byteData = new byte[algo.IV.Length];
            Array.Copy(data, byteData, byteData.Length);
            algo.IV = byteData;
            var readFrom = 0;
            readFrom += algo.IV.Length;
            var myCrypto = new CryptoStream(mStream, algo.CreateDecryptor(), CryptoStreamMode.Write);
            myCrypto.Write(data, readFrom, data.Length - readFrom);
            myCrypto.FlushFinalBlock();
            return Encoding.UTF8.GetString(mStream.ToArray());
        }
        public static string GetEncryptedQueryString(string data)
        {
            return Convert.ToBase64String(EncryptString(data));
        }
        public static string GetDecryptedQueryString(string data)
        {
            var byteData = Convert.FromBase64String(data.Replace(" ", "+"));
            return DecryptString(byteData);
        }
        public static string CreateKey(int length)
        {
            const string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789!#=+*@$?_-";
            var chars = new char[length];
            var rd = new Random();
            for (var i = 0; i < length; i++)
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            return new string(chars);
        }
        #endregion

        #region GlobalProperties
        public static void AddProperty(string key, string value)
        {
            lock (Oxide.GlobalProperties)
            {
                if (Oxide.GlobalProperties.ContainsKey(key))
                    Oxide.GlobalProperties.Remove(key);
                Oxide.GlobalProperties.Add(key, value);
            }
        }
        public static string GetProperty(string key)
        {
            return Oxide.GlobalProperties[key];
        }
        #endregion

        #region General
        public static int CalculatePercent(int total, int current)
        {
            return (int) Math.Round((double) (100*current)/total);
        }
        public static int CalculateValueFromPercent(int total, int percent)
        {
            return (percent/100)*total;
        }
        #endregion
    }
}