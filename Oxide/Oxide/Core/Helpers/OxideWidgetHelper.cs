﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Oxide.Core.Elements;
using Oxide.Core.Syntax;

namespace Oxide.Core.Helpers
{
    public static class OxideWidgetHelper
    {
        public static List<IOxideWidget> GetWidgetList()
        {
            lock (Oxide.LoadedWidgets)
            {
                return Oxide.LoadedWidgets.OrderBy(x => x.Name).ToList();
            }
        }
        public static List<IOxideWidget> GetWidgetList(Func<IOxideWidget, bool> predicate)
        {
            lock (Oxide.LoadedWidgets)
            {
                return Oxide.LoadedWidgets.Where(predicate).OrderBy(x => x.Name).ToList();
            }
        }
        public static IOxideWidget GetWidget(Func<IOxideWidget, bool> predicate)
        {
            lock (Oxide.LoadedWidgets)
            {
                return Oxide.LoadedWidgets.FirstOrDefault(predicate);
            }
        }
        public static List<IOxideDashboardWidget> GetDashboardWidgetList()
        {
            lock (Oxide.LoadedDashboardWidgets)
            {
                return Oxide.LoadedDashboardWidgets.OrderBy(x => x.Name).ToList();
            }
        }
        public static IHtmlString RenderViewToHtml(ControllerContext controllerContext, string viewName, object model)
        {
            controllerContext.Controller.ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                //#TODO make it generic
                if (controllerContext.Controller.ControllerContext == null)
                    controllerContext.Controller.ControllerContext = controllerContext;
                var viewResult = ViewEngines.Engines.FindPartialView(controllerContext, viewName);
                var viewContext = GetViewContext(controllerContext, viewResult, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(controllerContext, viewResult.View);
                return new HtmlString(sw.GetStringBuilder().ToString());
            }
        }
        public static void FixRoute(string moduleName, ControllerContext context)
        {
            if (Oxide.LoadedModules.All(x => x.Name != moduleName))
                throw new ArgumentException("Cannot find module name, please check widget module name");
            context.RequestContext.RouteData.DataTokens[Constants.AreaKeyword] = moduleName;
            context.RequestContext.RouteData.DataTokens[Constants.ControllerKeyword] = moduleName +
                                                                                       Constants.Controllers;
        }
        private static ViewContext GetViewContext(ControllerContext controllerContext, ViewEngineResult viewEngineResult,
            StringWriter stringWriter)
        {
            if (stringWriter == null) throw new ArgumentNullException(nameof(stringWriter));
            if (viewEngineResult.View == null)
                throw new ArgumentException("Cannot find module view, please check defined view value");
            return new ViewContext(controllerContext, viewEngineResult.View, controllerContext.Controller.ViewData,
                controllerContext.Controller.TempData, stringWriter);
        }
        public static string GetWidgetUniqueKey(IOxideWidget instance)
        {
            instance.WidgetKey = instance.ModuleName.Replace(Constants.Space, Constants.SpaceReplacer) +
                                 Constants.SpaceReplacer +
                                 instance.Name.Replace(Constants.Space, Constants.SpaceReplacer);
            return instance.WidgetKey;
        }
        public static string GetWidgetUniqueKey(IOxideDashboardWidget instance)
        {
            instance.WidgetKey = instance.ModuleName.Replace(Constants.Space, Constants.SpaceReplacer) +
                                 Constants.SpaceReplacer +
                                 instance.Name.Replace(Constants.Space, Constants.SpaceReplacer);
            return instance.WidgetKey;
        }
    }
}