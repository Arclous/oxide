﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oxide.Core.ViewModels.GeneralViewModels;

namespace Oxide.Core.Helpers
{
    public static class OnlineUsers
    {
        public static void OnlineUserStart()
        {
            HttpContext.Current.Session["ip"] = new Random().Next(100);
            lock (Oxide.OnlineUsers)
            {
                if (HttpContext.Current == null) return;
                var ip = HttpContext.Current.Session["ip"].ToString();
                var user = new OnlineUser {Ip = ip, Online = true, SeasonStart = DateTime.Now};
                if (ip == null) return;
                if (!Oxide.OnlineUsers.ContainsKey(ip))
                    Oxide.OnlineUsers.Add(ip, user);
            }
        }
        public static void OnlineUserFinish()
        {
            lock (Oxide.OnlineUsers)
            {
                if (HttpContext.Current == null) return;
                var ip = HttpContext.Current.Session["ip"].ToString();
                if (ip == null) return;
                var user = Oxide.OnlineUsers[ip];
                if (user == null) return;
                Oxide.OnlineUsers[ip].SeasonFinish = DateTime.Now;
                Oxide.OnlineUsers[ip].Online = false;
            }
        }
        public static int TotalOnlineUser()
        {
            return Oxide.OnlineUsers.Count(x => x.Value.Online);
        }
        public static int TotalOfflineUser()
        {
            return Oxide.OnlineUsers.Count(x => !x.Value.Online);
        }
        public static double GetAverageTime()
        {
            var offlineUsers = Oxide.OnlineUsers.Where(x => !x.Value.Online);
            var totalTime = 0;
            var keyValuePairs = offlineUsers as KeyValuePair<string, OnlineUser>[] ?? offlineUsers.ToArray();
            foreach (var user in keyValuePairs)
                totalTime = (user.Value.SeasonStart - user.Value.SeasonFinish).Minutes;
            if (totalTime != 0)
                return totalTime/keyValuePairs.Count();
            return 0;
        }
    }
}