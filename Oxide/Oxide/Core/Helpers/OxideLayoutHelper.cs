﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System.Collections.Generic;
using System.Linq;
using Oxide.Areas.Admin.ViewModels;
using Oxide.Core.Elements;
using Oxide.Core.Models.Layouts;
using Oxide.Core.Models.Pages;
using Oxide.Core.ViewModels.LayoutViewModels;
using Oxide.Core.ViewModels.PageViewModels;

namespace Oxide.Core.Helpers
{
    public static class OxideLayoutHelper
    {
        public static List<IOxideLayout> GetMainLayoutList()
        {
            return
                Oxide.LoadedLayouts.Where(x => x.LayoutType == Syntax.Syntax.LayoutTypes.Main)
                    .OrderBy(x => x.Id)
                    .ToList();
        }
        public static List<IOxideLayout> GetInnerLayoutList()
        {
            return
                Oxide.LoadedLayouts.Where(x => x.LayoutType == Syntax.Syntax.LayoutTypes.Inner)
                    .OrderBy(x => x.Id)
                    .ToList();
        }
        public static List<LayoutElementViewModel> GetElements(int layoutId, int layoutType)
        {
            ICollection<LayoutElementViewModel> elementList = new List<LayoutElementViewModel>();
            var selectedLayout = Oxide.LoadedLayouts.FirstOrDefault(x => x.Id == layoutId && x.LayoutType == layoutType);
            if (selectedLayout != null)
            {
                foreach (var element in
                    selectedLayout.GetLayoutsKeys()
                        .Select(
                            v =>
                                new LayoutElementViewModel
                                {
                                    Id = v.Key,
                                    Title = v.Value,
                                    Widgets = new List<PageWidgetViewModel>()
                                }))
                {
                    elementList.Add(element);
                }
            }
            return elementList.ToList();
        }
        public static List<ElementModel> GetElementsForRender(int layoutId, int layoutType)
        {
            ICollection<ElementModel> elementList = new List<ElementModel>();
            var selectedLayout = Oxide.LoadedLayouts.FirstOrDefault(x => x.Id == layoutId && x.LayoutType == layoutType);
            if (selectedLayout != null)
            {
                foreach (var element in
                    selectedLayout.GetLayoutsKeys()
                        .Select(v => new ElementModel {Id = v.Key, Title = v.Value, Widgets = new List<PageWidget>()}))
                {
                    elementList.Add(element);
                }
            }
            return elementList.ToList();
        }
        public static List<ElementModelMain> GetMainElementsForRender(int layoutId, int layoutType)
        {
            ICollection<ElementModelMain> elementList = new List<ElementModelMain>();
            var selectedLayout = Oxide.LoadedLayouts.FirstOrDefault(x => x.Id == layoutId && x.LayoutType == layoutType);
            if (selectedLayout != null)
            {
                foreach (var element in
                    selectedLayout.GetLayoutsKeys()
                        .Select(
                            v =>
                                new ElementModelMain
                                {
                                    Id = v.Key,
                                    Title = v.Value,
                                    Widgets = new List<MainLayoutWidget>()
                                }))
                {
                    elementList.Add(element);
                }
            }
            return elementList.ToList();
        }
        public static List<LayoutElementViewModel> GetLayoutElements(int layoutId, int layoutType)
        {
            ICollection<LayoutElementViewModel> elementList = new List<LayoutElementViewModel>();
            var selectedLayout = Oxide.LoadedLayouts.FirstOrDefault(x => x.Id == layoutId && x.LayoutType == layoutType);
            if (selectedLayout != null)
            {
                foreach (var element in
                    selectedLayout.GetLayoutsKeys()
                        .Select(
                            v =>
                                new LayoutElementViewModel
                                {
                                    Id = v.Key,
                                    Title = v.Value,
                                    Widgets = new List<PageWidgetViewModel>()
                                }))
                {
                    elementList.Add(element);
                }
            }
            return elementList.ToList();
        }
    }
}