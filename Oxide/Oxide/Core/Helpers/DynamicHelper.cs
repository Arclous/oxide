﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System.Collections.Generic;
using System.Dynamic;
using System.Reflection;

namespace Oxide.Core.Helpers
{
    public static class DynamicHelper
    {
        public static ExpandoObject ConvertToExpando(object obj)
        {
            const BindingFlags flags = BindingFlags.Public | BindingFlags.Instance;
            var properties = obj.GetType().GetProperties(flags);
            var expando = new ExpandoObject();
            foreach (var property in properties)
            {
                AddProperty(expando, property.Name, property.GetValue(obj));
            }
            return expando;
        }
        public static void AddProperty(ExpandoObject expando, string propertyName, object propertyValue)
        {
            var expandoDict = expando as IDictionary<string, object>;
            ;
            if (expandoDict.ContainsKey(propertyName))
                expandoDict[propertyName] = propertyValue;
            else
                expandoDict.Add(propertyName, propertyValue);
        }
    }
}