﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Castle.Components.DictionaryAdapter;
using Oxide.Core.Models.Pages;

namespace Oxide.Core.Helpers
{
    public static class Expressions
    {
        /// <summary>
        ///     return include properities for the page
        /// </summary>
        public static List<Expression<Func<Page, object>>> PageIncludeProperities()
        {
            List<Expression<Func<Page, object>>> properities = new EditableList<Expression<Func<Page, object>>>();
            properities.Add(x => x.Language);

            return properities;
        }

        /// <summary>
        ///     return expression for the pages according to filter by applying to page titles
        /// </summary>
        public static Expression<Func<Page, bool>> GetPageFilterCriteria(string filter)
        {
            Expression<Func<Page, bool>> expression =
                page =>
                    page.Name.ToLower().Contains(filter.ToLower()) || page.Title.ToLower().Contains(filter.ToLower());

            return expression;
        }

        /// <summary>
        ///     return expression for the pages according page type
        /// </summary>
        public static Expression<Func<Page, bool>> GetPageTypeCriteria(Syntax.Syntax.PageTypes pageType)
        {
            Expression<Func<Page, bool>> expression = p => p.PageType == pageType;
            return expression;
        }

        /// <summary>
        ///     return expression for the pages accoding to published status and page type
        /// </summary>
        public static Expression<Func<Page, bool>> GetPagePublishStatusCriteria(bool published,
            Syntax.Syntax.PageTypes pageType)
        {
            Expression<Func<Page, bool>> expression = p => p.Published == published && p.PageType == pageType;
            return expression;
        }

        /// <summary>
        ///     return expression for the pages accoding to page type and DateTime.Now
        /// </summary>
        public static Expression<Func<Page, bool>> GetPagePublishStatusWithDateCriteria(Syntax.Syntax.PageTypes pageType)
        {
            Expression<Func<Page, bool>> expression =
                p => p.PublishLater && p.PublishDate > DateTime.Now && p.PageType == pageType;
            return expression;
        }
    }
}