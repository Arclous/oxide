﻿using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Core.ViewModels.ModuleViewModels
{
    public class MessageAdminViewModel : OxideViewModel
    {
        public string Reciver { get; set; }
        public string Message { get; set; }
    }
}