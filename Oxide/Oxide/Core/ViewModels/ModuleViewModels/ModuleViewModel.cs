﻿using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Core.ViewModels.ModuleViewModels
{
    public class ModuleViewModel : OxideViewModel
    {
        public string ChangeStatusUrl { get; set; }
        public string InfoUrl { get; set; }
        public int Order { get; set; }
        public string Description { get; set; }
        public string Author { get; set; }
        public string Category { get; set; }
        public string PreviewImageUrl { get; set; }
        public bool Active { get; set; }
        public string Namespace { get; set; }
        public string[] Dependencies { get; set; }
    }
}