﻿namespace Oxide.Core.ViewModels.LoginViewModels
{
    public class LoginViewModel
    {
        public string UserName { get; set; }
        public string Message { get; set; }
        public string ReturnUrl { get; set; }
    }
}