﻿using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Core.ViewModels.LayoutViewModels
{
    public class LayoutViewModel : OxideViewModel
    {
        public string Layouts { get; set; }
        public string ManageWidgetsUrl { get; set; }
        public string PreviewImage { get; set; }
    }
}