﻿using System.Collections.Generic;
using Oxide.Areas.Admin.ViewModels;

namespace Oxide.Core.ViewModels.LayoutViewModels
{
    public class LayoutElementViewModel
    {
        public string Title { get; set; }
        public int Id { get; set; }
        public List<PageWidgetViewModel> Widgets { get; set; }
    }
    public class LayoutElementViewHolder
    {
        public ICollection<LayoutElementViewModel> Elements { get; set; }
        public string AddWidgetUrl { get; set; }
        public string EditWidgetUrl { get; set; }
        public string DeleteWidgetUrl { get; set; }
        public string UpdateWidgetUrl { get; set; }
        public int Id { get; set; }
    }
}