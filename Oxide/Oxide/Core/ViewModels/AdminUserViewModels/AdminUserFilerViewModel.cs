﻿using Oxide.Core.ViewModels.FilterViewModels;

namespace Oxide.Core.ViewModels.AdminUserViewModels
{
    public class AdminUserFilerViewModel : FilterModel<AdminUserViewModel>
    {
        public string DeleteUrl { get; set; }
    }
}