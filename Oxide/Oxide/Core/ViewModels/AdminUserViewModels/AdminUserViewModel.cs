﻿using System;
using Oxide.Areas.Admin.Models;
using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Core.ViewModels.AdminUserViewModels
{
    public class AdminUserViewModel : OxideViewModel
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public DateTime LastLogin { get; set; }
        public Syntax.Syntax.AdminUserStatus Status { get; set; }
        public string StatusText { get; set; }
        public string ConfirmPassword { get; set; }
        public int UserImageId { get; set; }
        public string UserImageIdValue { get; set; }
        public string EditUrl { get; set; }
        public string ManageRolesUrl { get; set; }
        public string UserLanguageId { get; set; }
        public Language UserLanguage { get; set; }
    }
}