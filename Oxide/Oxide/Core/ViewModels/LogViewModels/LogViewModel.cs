﻿using System;
using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Core.ViewModels.LogViewModels
{
    public class LogViewModel : OxideViewModel
    {
        public virtual Syntax.Syntax.LogTypes LogType { get; set; }
        public string ModuleName { get; set; }
        public string Description { get; set; }
        public string InnerException { get; set; }
        public DateTime DateTimeInfo { get; set; }
    }
}