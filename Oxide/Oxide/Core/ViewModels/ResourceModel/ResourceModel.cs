﻿using System.Collections.Generic;

namespace Oxide.Core.ViewModels.ResourceModel
{
    public class ResourceModel
    {
        public Dictionary<string, ResourceValueItem> Values = new Dictionary<string, ResourceValueItem>();
        public string Module { get; set; }
        public string Text { get; set; }
    }
    public class ResourceValueItem
    {
        public string Language { get; set; }
        public string Value { get; set; }
    }
}