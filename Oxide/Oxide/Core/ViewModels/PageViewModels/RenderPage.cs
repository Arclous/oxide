﻿using System.Collections.Generic;
using Oxide.Core.Models.Layouts;
using Oxide.Core.Models.Pages;

namespace Oxide.Core.ViewModels.PageViewModels
{
    public class RenderPage
    {
        public Page Page { get; set; }
        public Dictionary<string, ElementModel> InnerElements { get; set; }
        public Dictionary<string, ElementModelMain> MainElements { get; set; }
        public Dictionary<string, string> Meta { get; set; }
        public Dictionary<string, object> Data { get; set; }
        public string InnerPageLayout { get; set; }
        public string LanguageCode { get; set; }
    }
    public class ElementModel
    {
        public string Title { get; set; }
        public int Id { get; set; }
        public List<PageWidget> Widgets { get; set; }
    }
    public class ElementModelMain
    {
        public string Title { get; set; }
        public int Id { get; set; }
        public List<MainLayoutWidget> Widgets { get; set; }
    }
}