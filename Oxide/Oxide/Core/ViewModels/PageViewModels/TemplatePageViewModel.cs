﻿namespace Oxide.Core.ViewModels.PageViewModels
{
    public class TemplatePageViewModel : PageViewModel
    {
        public string ActivePageUrl { get; set; }
    }
}