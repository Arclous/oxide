﻿using System;
using System.Collections;
using System.Collections.Generic;
using Oxide.Areas.Admin.Models;
using Oxide.Core.Base.OxideViewModel;
using Oxide.Core.Models.Layouts;
using Oxide.Core.Models.Pages;

namespace Oxide.Core.ViewModels.PageViewModels
{
    public class PageViewModel : OxideViewModel
    {
        public string ManageWidgetsUrl { get; set; }
        public string EditPageUrl { get; set; }
        public int SiteMapFrequncySelected { get; set; }
        public int LanguageSelected { get; set; }
        public string IsVisibleForSite { get; set; }
        public string IsVisibleForSeo { get; set; }
        public virtual Layout InnerLayout { get; set; }
        public virtual Layout MainLayout { get; set; }
        public string SelectedDate { get; set; }
        public string IsPublishImd { get; set; }
        public string IsPublishLater { get; set; }
        public IEnumerable LanguageSelectList { get; set; }
        public string PageKey { get; set; }
        public string Url { get; set; }
        public int Order { get; set; }
        public string Tag { get; set; }
        public string Description { get; set; }
        public bool VisibleForSite { get; set; }
        public bool VisibleForSeo { get; set; }
        public List<int> WidgetsId { get; set; }
        public virtual ICollection<PageWidget> Widgets { get; set; }
        public int LanguageId { get; set; }
        public virtual Language Language { get; set; }
        public virtual Syntax.Syntax.PageTypes PageType { get; set; }
        public virtual Syntax.Syntax.SiteMapFrequncy SiteMapFrequncy { get; set; }
        public int BasePageId { get; set; }
        public virtual int InnerLayoutId { get; set; }
        public virtual int MainLayoutId { get; set; }
        public bool PublishImd { get; set; }
        public bool PublishLater { get; set; }
        public DateTime PublishDate { get; set; }
        public DateTime LastUpdate { get; set; }
        public DateTime CreateDate { get; set; }
        public bool Published { get; set; }
        public bool HomePage { get; set; }
        public string IsHomePage { get; set; }
        public bool AggresiveCacheEnabled { get; set; }
        public string IsAggresiveCacheEnabled { get; set; }
        public virtual double Priority { get; set; }
        public string ReturUrl { get; set; }
    }
}