﻿using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Core.ViewModels.DashboardWidgetViewModels
{
    public class DashboardWidgetPackageViewModel : OxideViewModel
    {
        public int Order { get; set; }
        public string WidgetKey { get; set; }
        public int UserId { get; set; }
        public string DashboardWidgetRunUrl { get; set; }
        public string ManageDashboardWidgetUrl { get; set; }
        public bool Manageable { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public string PreviewImageUrl { get; set; }
    }
}