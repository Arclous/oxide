﻿namespace Oxide.Core.ViewModels.ViewEngineModels
{
    public class ViewEngineResultData
    {
        public string ViewPath { get; set; }
        public string MasterPath { get; set; }
    }
}