﻿using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Core.ViewModels.AdminRoleViewModels
{
    public class AdminRoleViewModel : OxideViewModel
    {
        public string EditUrl { get; set; }
        public string ManagePermissionUrl { get; set; }
    }
}