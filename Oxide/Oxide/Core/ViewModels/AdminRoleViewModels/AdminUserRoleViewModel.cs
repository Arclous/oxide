﻿using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Core.ViewModels.AdminRoleViewModels
{
    public class AdminUserRoleViewModel : OxideViewModel
    {
        public bool Status { get; set; }
    }
}