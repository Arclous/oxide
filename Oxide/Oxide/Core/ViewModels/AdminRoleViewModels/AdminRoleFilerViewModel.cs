﻿using Oxide.Core.ViewModels.FilterViewModels;

namespace Oxide.Core.ViewModels.AdminRoleViewModels
{
    public class AdminRoleFilerViewModel : FilterModel<AdminRoleViewModel>
    {
        public string DeleteUrl { get; set; }
    }
}