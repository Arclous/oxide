﻿using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Core.ViewModels.ThemeViewModel
{
    public class ThemeViewModel : OxideViewModel
    {
        public string ChangeStatusUrl { get; set; }
        public string PreviewImageUrl { get; set; }
        public bool Active { get; set; }
    }
}