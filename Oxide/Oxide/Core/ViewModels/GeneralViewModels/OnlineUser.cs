﻿using System;

namespace Oxide.Core.ViewModels.GeneralViewModels
{
    public class OnlineUser
    {
        public string Ip { get; set; }
        public DateTime SeasonStart { get; set; }
        public DateTime SeasonFinish { get; set; }
        public bool Online { get; set; }
    }
}