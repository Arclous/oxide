﻿using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Core.ViewModels.AdminPermisionViewModels
{
    public class AdminPermissionViewModel : OxideViewModel
    {
        public string Key { get; set; }
        public string Owner { get; set; }
        public string Group { get; set; }
        public bool Status { get; set; }
    }
}