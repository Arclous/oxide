﻿using System.Collections.Generic;

namespace Oxide.Core.ViewModels.AdminPermisionViewModels
{
    public class AdminPermissionGroupViewModel
    {
        public string Name { get; set; }
        public List<AdminPermissionViewModel> Permissions { get; set; }
    }
}