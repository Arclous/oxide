﻿using System.Collections.Generic;

namespace Oxide.Core.ViewModels.AdminPermisionViewModels
{
    public class AdminPermissionOwnerViewModel
    {
        public string Name { get; set; }
        public List<AdminPermissionGroupViewModel> Groups { get; set; }
    }
}