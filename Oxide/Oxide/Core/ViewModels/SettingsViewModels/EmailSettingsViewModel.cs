﻿using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Core.ViewModels.SettingsViewModels
{
    public class EmailSettingsViewModel : OxideViewModel
    {
        public string Sender { get; set; }
        public string HostName { get; set; }
        public int PortNumber { get; set; }
        public bool EnableSsl { get; set; }
        public bool RequireCredentials { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string IsEnableSsl { get; set; }
        public string IsRequireCredentials { get; set; }
    }
}