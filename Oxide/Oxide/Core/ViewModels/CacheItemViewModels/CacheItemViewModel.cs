﻿using System;
using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Core.ViewModels.CacheItemViewModels
{
    public class CacheItemViewModel : OxideViewModel
    {
        public DateTime LastUpdate { get; set; }
        public int Value { get; set; }
        public bool IsCached { get; set; }
    }
}