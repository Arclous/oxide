﻿using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Core.ViewModels.WidgeViewModels
{
    public class WidgetViewModel : OxideViewModel
    {
        public string Description { get; set; }
        public string Category { get; set; }
        public string PreviewImageUrl { get; set; }
        public string ChangeStatusUrl { get; set; }
        public string InfoUrl { get; set; }
        public bool Active { get; set; }
        public int Order { get; set; }
        public string WidgetKey { get; set; }
        public string[] Dependencies { get; set; }
    }
}