﻿using System.Collections.Generic;

namespace Oxide.Core.ViewModels.FilterViewModels
{
    public class FilterModel<T> where T : class
    {
        public ICollection<T> Records { get; set; }
        public int TotalRecords { get; set; }
        public int DisplayingRecords { get; set; }
        public int TotalPages { get; set; }
        public int CurrentPage { get; set; }
        public bool NextEnable { get; set; }
        public bool PreviousEnable { get; set; }
    }
}