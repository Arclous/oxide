﻿using System.Collections.Generic;
using Oxide.Core.Models.Menus;

namespace Oxide.Core.ViewModels.MenuViewModel
{
    public class LeftMenuViewModel
    {
        public List<HeaderMenu> Menus { get; set; }
    }
}