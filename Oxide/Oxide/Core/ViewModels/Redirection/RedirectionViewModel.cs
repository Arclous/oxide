﻿using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Core.ViewModels.Redirection
{
    public class RedirectionViewModel : OxideViewModel
    {
        public string Url { get; set; }

        public string NewUrl { get; set; }

        public virtual Syntax.Syntax.RedirectionStatusCode StatusCode { get; set; }
        public int StatusCodeSelected { get; set; }
        public string ReturUrl { get; set; }
        public bool Active { get; set; }
        public string IsActive { get; set; }
    }
}