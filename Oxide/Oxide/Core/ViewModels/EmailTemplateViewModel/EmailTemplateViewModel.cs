﻿using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Core.ViewModels.EmailTemplateViewModel
{
    public class EmailTemplateViewModel : OxideViewModel
    {
        public string Template { get; set; }
        public string EditUrl { get; set; }
    }
}