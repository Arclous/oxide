﻿using System;
using Oxide.Core.Base.OxideViewModel;

namespace Oxide.Core.ViewModels.MessageViewModel
{
    public class MessageViewModel : OxideViewModel
    {
        public string MessageBody { get; set; }
        public DateTime MessageDateTime { get; set; }
        public bool IsMessageRead { get; set; }
        public string SenderUserName { get; set; }
        public string SenderUserImage { get; set; }
    }
}