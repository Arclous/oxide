﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.ResultModels;

namespace Oxide.Core.Elements
{
    public interface IOxideDashboardWidget
    {
        string Name { get; }
        string Title { get; }
        string Description { get; }
        string Category { get; }
        string PreviewImageUrl { get; }
        bool Active { get; set; }
        string ModuleName { get; }
        string FrontView { get; set; }
        string EditorView { get; set; }
        string WidgetKey { get; set; }
        bool Manageable { get; set; }
        WidgetResult RenderDashboard(DashboardWidgetInfoPackage widgetInfoPacket, IProviderManager providerManager);
        WidgetResult RenderDashboardManagement(DashboardWidgetInfoPackage widgetInfoPacket,
            IProviderManager providerManager);
    }
}