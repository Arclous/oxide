﻿namespace Oxide.Core.ResultModels
{
    public class WidgetUpdatePackage
    {
        public bool Success { get; set; }
        public string SuccessMessage { get; set; }
        public string ErrorMessage { get; set; }
    }
}