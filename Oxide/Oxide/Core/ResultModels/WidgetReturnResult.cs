﻿using System.Web;
using Oxide.Core.Models.Base;

namespace Oxide.Core.ResultModels
{
    public class WidgetReturnResult : OxideModel
    {
        public IHtmlString Data { get; set; }
    }
}