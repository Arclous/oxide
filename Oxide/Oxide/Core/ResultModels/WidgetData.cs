﻿namespace Oxide.Core.ResultModels
{
    public class WidgetData
    {
        public string WidgetKey { get; set; }
        public int WidgetId { get; set; }
        public int LayoutId { get; set; }
        public int LayoutKeyId { get; set; }
        public string DynamicTitle { get; set; }
        public int ContainerPageId { get; set; }
        public bool Regular { get; set; }
        public int WidgetCulture { get; set; }
    }
}