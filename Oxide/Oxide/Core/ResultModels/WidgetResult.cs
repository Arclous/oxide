﻿namespace Oxide.Core.ResultModels
{
    public class WidgetResult
    {
        public string ViewName { get; set; }
        public object Model { get; set; }
    }
}