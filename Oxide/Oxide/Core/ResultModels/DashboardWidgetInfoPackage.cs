﻿using Oxide.Core.Extesions;

namespace Oxide.Core.ResultModels
{
    public class DashboardWidgetInfoPackage
    {
        public int WidgetId { get; set; }
        public int UniqeHash { get; set; }
        public string WidgetKey { get; set; }
        public string Title { get; set; }
        public int UserId { get; set; }
        public int GetUniqueHash()
        {
            return (WidgetKey + WidgetId + UniqeHash + UserId).GetStableHashCode();
        }
    }
}