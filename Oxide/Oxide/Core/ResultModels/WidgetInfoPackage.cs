﻿using Oxide.Core.Extesions;

namespace Oxide.Core.ResultModels
{
    public class WidgetInfoPackage
    {
        public int WidgetId { get; set; }
        public int PageId { get; set; }
        public int CurrentLanguage { get; set; }
        public int UniqeHash { get; set; }
        public string WidgetKey { get; set; }
        public int LayoutId { get; set; }
        public int LayoutKeyId { get; set; }
        public string Title { get; set; }
        public bool Regular { get; set; }
        public int MainPageId { get; set; }
        public int GetUniqueHash()
        {
            return Regular
                ? (WidgetKey + WidgetId + LayoutId + PageId).GetStableHashCode()
                : (WidgetKey + WidgetId + LayoutId + PageId + CurrentLanguage).GetStableHashCode();
        }
        //Use for cache key for master page widgets
        public int GetUniqueHashWithPageId()
        {
            return (WidgetKey + WidgetId + LayoutId + MainPageId).GetStableHashCode();
        }
    }
}