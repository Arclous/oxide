﻿namespace Oxide.Core.Syntax
{
    public enum LinkTypes
    {
        Internal = 0,
        External = 1
    }
}