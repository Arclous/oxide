﻿namespace Oxide.Core.Syntax
{
    public enum ErrorTypes
    {
        Error403 = 0,
        Error404 = 1,
        Error500 = 2
    }
}