﻿namespace Oxide.Core.Syntax
{
    public static class OxideSystem
    {
        public const string DbContext = "DbContext";
        public const string OxideDatabase = "OxideDatabase";
    }
}