﻿namespace Oxide.Core.Syntax
{
    public static class Syntax
    {
        public enum AdminLoginResult
        {
            Success = 0,
            NotSuccess = 1,
            UserNotFound = 2
        }
        public enum AdminUserStatus
        {
            Online = 0,
            Offline = 1
        }
        public enum AlternateTypes
        {
            FunctionFront = 0,
            FunctionAdmin = 1,
            ViewAdmin = 2,
            ViewFront = 3,
            ViewAdminFront = 4,
            Completely = 5,
            CompletelyFront = 6,
            CompletelyAdmin = 7
        }
        public enum BackgroundTaskActionType
        {
            Daily = 0,
            Calendar = 1,
            Scheduled = 2
        }
        public enum EventTypes
        {
            OnCreated = 0,
            OnUpdated = 1,
            OnDeleted = 2
        }
        public enum LogsClean
        {
            EveryDay = 0,
            EveryHour = 1
        }
        public enum LogTypes
        {
            Error = 0,
            Warning = 1,
            Info = 2
        }
        public enum PageTypes
        {
            Standard = 0,
            Dynamic = 1,
            Template=2
        }
        public enum ScheduledJobRunPlatforms
        {
            Cms = 0,
            Front = 1
        }
        public enum ScheduleIntervals
        {
            EverySeconds = 0,
            EveryMinutes = 1,
            EveryHours = 2
        }
        public enum ScheduleIntervalTypes
        {
            Everyday = 0,
            Weekdays = 1,
            Weekend = 2
        }
        public enum SiteMapFrequncy
        {
            Always = 0,
            Hourly = 1,
            Daily = 2,
            Weekly = 3,
            Monthly = 4,
            Yearly = 5,
            Never = 6
        }
        public enum UrlType
        {
            Absolute = 0,
            Relative = 1
        }
        public enum RedirectionStatusCode
        {
            MovedPermanently = 301,
            Found = 302
        }

        public static class CssClass
        {
            public const string FaHome = "fa fa-home";
            public const string FaEdit = "fa fa-edit";
            public const string FaDesktop = "fa fa-desktop";
            public const string FaTable = "fa fa-table";
            public const string FaChart = "fa fa-bar-chart-o";
            public const string FaLayout = "fa fa-clone";
            public const string FaBug = "fa fa-bug";
            public const string FaWindows = "fa fa-windows";
            public const string FaSiteMap = "fa fa-sitemap";
            public const string FaUser = "fa fa-user";
            public const string FaUsers = "fa fa-users";
            public const string FaFolder = "fa folder-open";
            public const string FaKey = "fa fa-key";
            public const string FaAsterisk = "fa fa-asterisk";
            public const string FaCheck = "fa fa-check";
            public const string FaClose = "fa fa-times";
            public const string FaSettings = "fa fa-cog";
            public const string FaAdd = "fa fa-plus";
            public const string FaInfo = "fa fa-info-circle";
            public const string FaCertificate = "fa fa-certificate";
            public const string FaPaint = "fa fa-paint-brush";
            public const string FaPuzzle = "fa fa-puzzle-piece";
            public const string FaCache = "fa fa-tasks";
            public const string Menu = "fa fa-ellipsis-h";
            public const string Media = "fa fa-file-picture-o";
            public const string FileBrowser = "fa fa-folder-open-o";
            public const string Schedule = "fa fa-calendar";
            public const string Code = "fa fa-code";
            public const string CodeSettings = " fa fa-cog";
            public const string Templates = " fa fa-newspaper-o";
            public const string Seo = "fa fa-google";
        }
        public static class Requests
        {
            public const int Static = 0;
            public const int Dynamic = 1;
            public const int Panel = 2;
        }
        public class LayoutTypes
        {
            public const int Inner = 0;
            public const int Main = 1;
        }
        public static class ProviderNames
        {
            public const string SystemDataSqlClient = "System.Data.SqlClient";
        }

        public static class Keywords
        {
            public const string RedirectionItem = @"RedirectionItem";
        }
    }
}