﻿namespace Oxide.Core.Syntax
{
    public enum LinkOpenTypes
    {
        Parent = 0,
        New = 1
    }
}