﻿namespace Oxide.Core.Syntax
{
    public static class PermissionKeys
    {
        public const string Empty = "";
        //Page Management
        public const string CanCreatePage = "Can_Create_Pages";
        public const string CanDisplayPage = "Can_Display_Pages";
        public const string CanEditPages = "Can_Edit_Pages";
        public const string CanCreatePages = "Can_Create_Pages";
        public const string CanManagePageWidgets = "Can_Manage_Page_Widgets";
        public const string CanUpdatePages = "Can_Update_Pages";
        public const string CanDisplayPages = "Can_Display_Pages";
        public const string CanChangePublishStatus = "Can_Change_Publish_Status";
        public const string CanDeletePage = "Can_Delete_Page";
        public const string CanClonePage = "Can_Clone_Pages";
        //Layout Management
        public const string CanDisplayLayouts = "Can_Display_Layouts";
        public const string CanDisplayLayoutWidgets = "Can_Display_Layout_Widgets";
        //Log Management
        public const string CanDisplayLogs = "Can_Display_Logs";
        public const string CanClearLogs = "Can_Clear_Logs";
        public const string CanManageLogSettings = "Can_Manage_Log_Settings";
        //Email Template Management
        public const string CanDisplayEmailTemplate = "Can_Display_Email_Templates";
        public const string CanDeleteEmailTemplate = "Can_Delete_Email_Templates";
        public const string CanEditEmailTemplate = "Can_Edit_Email_Templates";
        public const string CanCreateEmailTemplate = "Can_Create_Email_Templates";
        //Widget Management
        public const string CanDisplayWidgets = "Can_Display_Widgets";
        public const string CanEditWidgets = "Can_Edit_Widgets";
        public const string CanManageWidgets = "Can_Manage_Widgets";
        public const string CanAddWidgetsToPage = "Can_Add_Widgets_To_Page";
        public const string CanUpdateWidgetsOnPage = "Can_Update_Widgets_On_Page";
        public const string CanDeleteWidgetsFromPage = "Can_Delete_Widgets_From_Page";
        public const string CanAddWidgetsToLayout = "Can_Add_Widgets_To_Layout";
        public const string CanUpdateWidgetsOnLayout = "Can_Update_Widgets_On_Layout";
        public const string CanDeleteWidgetsFromLayout = "Can_Delete_Widgets_From_Layout";
        //Dashboard Widget Management
        public const string CanManageDashboardWidgets = "Can_Manage_Dashboard_Widgets";
        public const string CanDisplayDashboardWidgets = "Can_Display_Dashboard_Widgets";
        //Theme Management
        public const string CanDisplayThemes = "Can_Display_Themes";
        public const string CanManageThemes = "Can_Manage_Themes";
        //Settings Management
        public const string CanDisplaySettings = "Can_Display_Settings";
        public const string CanManageSettings = "Can_Manage_Settings";
        //Email Settings Management
        public const string CanDisplayEmailSettings = "Can_Display_Email_Settings";
        public const string CanManageEmailSettings = "Can_Manage_Email_Settings";
        //Seo Settings Management
        public const string CanDisplaySeoSettings = "Can_Display_Seo_Settings";
        public const string CanManageSeoSettings = "Can_Manage_Seo_Settings";
        public const string CanDisplayRobotSettings = "Can_Display_Robot_Settings";
        public const string CanManageRobotSettings = "Can_Manage_Robot_Settings";
        public const string CanDisplayRedirectionSettings = "Can_Display_Redirection_Settings";
        public const string CanManageRedirectionSettings = "Can_Manage_Redirection_Settings";
        //Scheduled Job Management
        public const string CanReScheduleJobs = "Can_ReSchedule_Jobs";
        public const string CanDisplayScheduleJobs = "Can_Display_Scheduled_Jobs";
        public const string CanStartJobs = "Can_Start_Jobs";
        public const string CanStopJob = "Can_Stop_Jobs";
        public const string CanForceStartJob = "Can_Force_Start_Jobs";
        //Module Management
        public const string CanDisplayModules = "Can_Display_Modules";
        public const string CanManageModules = "Can_Manage_Modules";
        //Message Management
        public const string CanDisplayMessages = "Can_Display_Messages";
        public const string CanSendMessages = "Can_Send_Messages";
        //User Management
        public const string CanDisplayUsers = "Can_Display_Users";
        public const string CanCreateUsers = "Can_Create_Users";
        public const string CanUpdateUsers = "Can_Update_Users";
        public const string CanManageRoles = "Can_Manage_Roles";
        public const string CanDisplayRoles = "Can_Display_Roles";
        public const string CanCreateRoles = "Can_Create_Roles";
        public const string CanUpdateRoles = "Can_Update_Roles";
        public const string CanManagePermissions = "Can_Manage_Permissions";
        public const string CanDipslayUsers = "Can_Display_Users";
        public const string CanDeleteUsers = "Can_Delete_Users";
        public const string CanDeleteRoles = "Can_Delete_Roles";
        public const string CanSetRoles = "Can_Set_Roles";
        public const string CanDisplayPermissions = "Can_Display_Permissions";
        public const string CanSetPermissions = "Can_Set_Permissions";
        //Languages Management
        public const string CanDisplayLanguaeSettings = "Can_Display_Language_Settings";
        public const string CanUpdateLanguaeSettings = "Can_Update_Language_Settings";
        //Cache Management
        public const string CanDisplayCacheItems = "Can_Display_Cache_Items";
        public const string CanDisplayCacheSettings = "Can_Display_Cache_Settings";
        public const string CanClearCacheItems = "Can_Clear_Cache_Items";
        public const string CanUpdateCacheItemSettings = "Can_Update_Cache_Items_Settings";
        //Permission Groups
        public const string PageManagement = "Page Management";
        public const string LayoutManagement = "Layout Management";
        public const string WidgetManagement = "Widget Management";
        public const string ThemeManagement = "Theme Management";
        public const string SettingsManagement = "Settings Management";
        public const string ScheduledJobManagement = "Scheduled Job Management";
        public const string ModuleManagement = "Module Management";
        public const string UserManagement = "User Management";
        public const string LanguageManagement = "Languages Management";
        public const string CacheManagement = "Cache Management";
        public const string LogManagement = "Log Management";
        public const string EmailTemplateManagement = "Email Template Management";
        public const string DashaboardWidgetManagement = "Dashboard Widget Management";
        public const string MessageManagement = "Message Management";
        public const string RobotManagement = "Robot Management";
        public const string RedirectionManagement = "Redirection Management";
    }
}