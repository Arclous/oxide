﻿namespace Oxide.Core.Syntax
{
    public enum RequestType
    {
        Front = 0,
        Admin = 1,
        Login = 2,
        BlankFrame
    }
}