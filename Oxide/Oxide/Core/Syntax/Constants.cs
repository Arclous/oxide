﻿namespace Oxide.Core.Syntax
{
    public class Constants
    {
        public const string AttributeKey = "action";
        public const string OxideController = "Oxide.Areas.Admin.Controllers";
        public const string AreasKeyword = "~/Areas";
        public const string AreasName = "Areas";
        public const string Controllers = ".Controllers";
        public const string AdminLayout = "~/Areas/Admin/Views/Shared/_Layout.cshtml";
        public const string FrontLayout = "~/Areas/Admin/Views/Shared/_LayoutFront.cshtml";
        public const string LoginLayout = "~/Areas/Admin/Views/Shared/_LayoutLogin.cshtml";
        public const string BlankLayout = "~/Areas/Admin/Views/Shared/_LayoutBlank.cshtml";
        public const string DialogLayout = "~/Areas/Admin/Views/Shared/_LayoutDialog.cshtml";
        public const string DefaultNameSpace = "Oxide";
        public const string DefaultNameSchema = "dbo";
        public const string ThemeNameSpace = "Theme";
        public const string DefaultArea = "Admin";
        public const string ThemeArea = "Theme";
        public const string DefaultErrorPage = "~/Areas/Admin/Views/Error/Error.cshtml";
        public const string DefaultMasterPage = "~/Areas/Admin/Views/Front/Index.cshtml";
        public const string DefaultDataBaseName = "Oxide";
        public const string TableNameSpereator = "_";
        public const string SpaceReplacer = "_";
        public const char CharSpaceReplacer = '_';
        public const string HomePage = "/";
        public const string BackSlash = @"\";
        public const string Empty = "";
        public const string Space = " ";
        public const char CharSpace = ' ';
        public const string Bin = "bin";
        public const string Interface = "I";
        public const string Dot = ".";
        public const string DllPatern = "*.dll";
        public const string Dll = ".dll";
        public const int DefaultTopCount = 50;
        public const string RenderController = "controller";
        public const string RenderValue = "someValue";
        public const string TableCreaterMethodName = "Entity";
        public const char Splitter = ',';
        public const string AreaKeyword = "area";
        public const string ControllerKeyword = "controller";
        public const string Checked = "checked";
        public const string Diez = "#";
        public const string Default = "_default";
        public const string Controller = "{controller}";
        public const string Action = "{action}";
        public const string Id = "{id}";
        public const string Admin = "Admin";
        public const string Index = "Index";
        public const string AdminController = "Oxide.Areas.Admin.Controllers";
        public const string DefaultKeyword = "default";
        public const string UrlPattern = "{*url}";
        public const string Front = "Front";
        public const string VirtualStart = "\\";
        public const string ArchiveExtension = ".zip";
        public const string FileContent = "File";
        public const string PageContent = "Page";
        public const string AdminArea = "Admin";
        public const string IsSuperAdmin = "IsSuperAdmin";
        public const string UserName = "UserName";
        public const string User = "User";
        public const string UserProfileImage = "UserProfileImage";
        public const string UserEmail = "UserEmail";
        public const string UserId = "Id";
        public const string ExpandoImageField = "ContentImagePath";
        public const string TitleFieldTitle = "Name";
        public const string NameFieldTitle = "Title";
        public const string TitleField = "Name";
        public const string NameField = "Title";
        public const string DefaultIdentity = "_Id";
        public const string DefaultIdentityField = "Id";
        //Temp Data Keys
        public const string NewAdminUser = "NewAdminUser";
        public const string AdminRole = "AdminRole";
        public const string RescheduledJob = "RescheduledJob";
        //Settings Keys
        public const string SiteName = "Site_Name";
        public const string DefaultSiteName = "Oxide";
        //Default Modules
        public const string OxideFundamentals = "Oxide.Fundamentals";
        public const string OxideMedia = "Oxide.Media";
        //Cache Settings Keys
        public const string PageWidgetsOn = "Page Widgets On";
        //Resources Constants
        public const string LanguageCookieName = "lang";
        public const string FrontLanguage = "lang";
        public const string LanguageCulture = "culture";
        public const string ResourceAreaKey = "area";
        public const string ResourceKey = "key";
        public const string ResourceName = "name";
        public const string Resource = "Area";
        public const string Translation = "Translation";
        public const string ResourceLanguage = "Language";
        public const string ResourceModule = "module";
        public const string LanguageFolder = "Languages";
        public const string LanguageFile = "Languages.xml";
        public const string FrontLanguageId = "FrontLanguageId";
        //Admin Language Constants
        public const string AdminDefaultLanguageCode = "ENG";
        public const string AdminLanguageCookieName = "adminlang";
        public const string AdminLanguageCookieName2 = "adminlang2";
        public const string AdminLanguage = "AdminLanguage";
        public const string AdminLanguageId = "AdminLanguageId";
        public const string AdminProfileImageCookieName = "profileImage";
        //Site Language Constants
        public const string DefaultLanguageCode = "ENG";
        public const string DefaultTimeZoneCode = "utc";
        //Default Theme Constants
        public const string DefaultTheme = "Default";
        //RequireScript Constants
        public const string ScriptList = "client-script-list";
        public const string Script = "script";
        public const string ScriptTypeKey = "type";
        public const string ScriptTypeValue = "text/javascript";
        public const string ScriptSourceKey = "src";
        //Seo Keywords
        public const string Description = "description";
        public const string Keywords = "keywords";
        //Cache Keys
        public const string Core = "Core";
        public const string AdminSettingsCache = "AdminSettingsCache";
        public const string LanguageId = "LanguageId";
        public const string Redirections = "Redirections";
        public const string Culture = "Culture";
        //Request defaults
        public const string Www = "www";
        public const string WwwWithDot = "www.";
        public const string Location = "Location";
        public const string Slash = "/";
        //Web Request Type
        public const string Get = "GET";
        public const string Post = "POST";
    }
    public class CacheLength
    {
        public const int Hour1 = 60;
        public const int Hour2 = 120;
        public const int Hour3 = 180;
        public const int Hour4 = 240;
        public const int Hour5 = 300;
        public const int Hour6 = 360;
        public const int Hour7 = 400;
        public const int Hour8 = 460;
        public const int Hour9 = 520;
        public const int Hour10 = 600;
        public const int Hour11 = 660;
        public const int Hour12 = 720;
        public const int Day = 1444;
    }
}