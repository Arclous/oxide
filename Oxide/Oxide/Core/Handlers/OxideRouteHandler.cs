﻿using System.Web;
using System.Web.Routing;

namespace Oxide.Core.Handlers
{
    public class OxideRouteHandler : IRouteHandler
    {
        public IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            return new OxideHttpHandler();
        }
    }
}