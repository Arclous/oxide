﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Syntax;

namespace Oxide.Core.Extesions
{
    public static class Extensions
    {
        public static SelectList ToSelectList<TEnum>(this TEnum obj)
            where TEnum : struct, IComparable, IFormattable, IConvertible
        {
            return
                new SelectList(
                    Enum.GetValues(typeof (TEnum))
                        .OfType<Enum>()
                        .Select(
                            x =>
                                new SelectListItem
                                {
                                    Text = Enum.GetName(typeof (TEnum), x),
                                    Value = (Convert.ToInt32(x)).ToString()
                                }), "Value", "Text");
        }

        //Transfer information from base to inherited model, in cases database model to view model
        public static TU CopyPropertiesTo<T, TU>(this T source, TU dest)
        {
            if (source == null)
                throw new Exception("Source is null");
            var sourceProps = typeof (T).GetProperties().Where(x => x.CanRead).ToList();
            var destProps = typeof (TU).GetProperties().Where(x => x.CanWrite).ToList();
            foreach (var sourceProp in sourceProps)
            {
                if (destProps.Any(x => x.Name == sourceProp.Name))
                {
                    var p = destProps.First(x => x.Name == sourceProp.Name);
                    p.SetValue(dest, sourceProp.GetValue(source, null), null);
                }
            }
            return dest;
        }

        public static TU CopyPropertiesTo<T, TU>(this T source, TU dest, string[] exceptionProperities)
        {
            var sourceProps =
                typeof (T).GetProperties().Where(x => x.CanRead && !exceptionProperities.Contains(x.Name)).ToList();
            var destProps = typeof (TU).GetProperties().Where(x => x.CanWrite).ToList();
            foreach (var sourceProp in sourceProps)
            {
                if (destProps.Any(x => x.Name == sourceProp.Name))
                {
                    var p = destProps.First(x => x.Name == sourceProp.Name);
                    p.SetValue(dest, sourceProp.GetValue(source, null), null);
                }
            }
            return dest;
        }

        public static string ConvertStringArrayToString(this string[] array)
        {
            return string.Join(".", array);
        }

        public static string CreateUrl(this string value, string parent = null)
        {
            return string.Format(@"/{1}{0}", value.ToLower().Replace(' ', '_'), parent != null ?
                string.Format(@"{0}/", parent) : string.Empty);
        }

        public static bool IsUrlFinishWithSlash(this string value)
        {
            string[] operators = { Constants.Slash };
            return operators.Any(value.EndsWith);
        }

        public static bool IsBaseUrl(this string value)
        {
            return value == Constants.Slash;
        }

        public static string ClearUrlFromSlash(this string value)
        {
            return value.Length > 0 ? value.Substring(0, value.Length - 1) : value;
        }

        public static string GetSafeName(this string value)
        {
            return value.Replace(Constants.CharSpace, Constants.CharSpaceReplacer);
        }

        public static string GetSafeUrl(this string value)
        {
            return value.Replace(Constants.BackSlash, Constants.Slash);
        }

        public static ControllerContext GetControllerContext(this RequestContext requestContext, Type controllerType)
        {
            return controllerType != null
                ? new ControllerContext(requestContext, Activator.CreateInstance(controllerType) as ControllerBase)
                : null;
        }

        public static RequestType GetTypeOfRequest(this ControllerContext controllerContext)
        {
            var nameSpace = controllerContext.Controller.GetType().Namespace;
            var controllerType = controllerContext.Controller.GetType();
            var actionDescriptor = new ReflectedControllerDescriptor(controllerType).FindAction(controllerContext,
                controllerContext.RouteData.GetRequiredString(Constants.AttributeKey));
            var attributesAdmin = actionDescriptor.GetCustomAttributes(typeof(OxideAdmin), false);
            var attributesFront = actionDescriptor.GetCustomAttributes(typeof(Front), false);
            var attributesLogin = actionDescriptor.GetCustomAttributes(typeof(AdminLogin), false);
            var attributesBlankFrame = actionDescriptor.GetCustomAttributes(typeof(BlankFrame), false);
            if (attributesAdmin.Length > 0)
                return RequestType.Admin;
            if (attributesFront.Length > 0)
                return RequestType.Front;
            if (attributesLogin.Length > 0)
                return RequestType.Login;
            if (attributesBlankFrame.Length > 0)
                return RequestType.BlankFrame;
            return nameSpace == Constants.OxideController ? RequestType.Admin : RequestType.Front;
        }

        public static MvcHtmlString RequireScript(this HtmlHelper helper, string virtualPath)
        {
            var path = Scripts.Url(virtualPath).ToHtmlString();
            var scripts = helper.ViewContext.HttpContext.Items[Constants.ScriptList] as Dictionary<string, string> ??
                          new Dictionary<string, string>();
            if (scripts.ContainsKey(path)) return MvcHtmlString.Empty;
            var stringBuilder = new StringBuilder();
            var tag = new TagBuilder(Constants.Script);
            tag.Attributes.Add(Constants.ScriptTypeKey, Constants.ScriptTypeValue);
            tag.Attributes.Add(Constants.ScriptSourceKey, path);
            stringBuilder.AppendLine(tag.ToString());
            scripts.Add(path, path);
            helper.ViewContext.HttpContext.Items[Constants.ScriptList] = scripts;
            return new MvcHtmlString(stringBuilder.ToString());
        }

        public static string FirstLetterToUpper(this string input)
        {
            return input = input.Substring(0, 1).ToUpper() + input.Substring(1, input.Length - 1);
        }

        public static bool HasProperty(this ExpandoObject obj, string propertyName)
        {
            return ((IDictionary<string, object>) obj).ContainsKey(propertyName);
        }

        public static List<string> GetStringList(this string value)
        {
            return value.Split(Constants.Splitter).ToList();
        }

        public static List<int> GetIntList(this string value)
        {
            return value.Split(Constants.Splitter).Select(x => Convert.ToInt32(x)).ToList();
        }

        public static Dictionary<string, PropertyInfo> GetPropertyMap(this object source)
        {
            var propertyMap =
                source.GetType()
                    .GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)
                    .ToDictionary(p => p.Name.ToLower(), p => p);
            return propertyMap;
        }

        public static string GetTableName(this Type type)
        {
            var tableName = Constants.DefaultNameSchema + Constants.Dot +
                            type.Module.Name.Replace(Constants.Dll, Constants.Empty)
                                .Replace(Constants.Dot, Constants.TableNameSpereator) + Constants.TableNameSpereator +
                            type.Name;
            return tableName;
        }
    }

    public static class Identifier
    {
        public static string GetOxideMd5HashCode(this string input)
        {
            var hash = new StringBuilder();
            var md5Provider = new MD5CryptoServiceProvider();
            var bytes = md5Provider.ComputeHash(new UTF8Encoding().GetBytes(input));
            foreach (var t in bytes)
            {
                hash.Append(t.ToString("x2"));
            }
            return hash.ToString();
        }
        public static int GetStableHashCode(this string str)
        {
            unchecked
            {
                var hash1 = (5381 << 16) + 5381;
                var hash2 = hash1;
                for (var i = 0; i < str.Length; i += 2)
                {
                    hash1 = ((hash1 << 5) + hash1) ^ str[i];
                    if (i == str.Length - 1)
                        break;
                    hash2 = ((hash2 << 5) + hash2) ^ str[i + 1];
                }
                return hash1 + (hash2*1566083941);
            }
        }
    }

    public static class EnumerableExtensions
    {
        public static DataTable AsDataTable<T>(this IEnumerable<T> data)
        {
            var properties = TypeDescriptor.GetProperties(typeof (T));
            var table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (var item in data)
            {
                var row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }
    }

    public static class Mapper<T> where T : class
    {
        private static readonly Dictionary<string, PropertyInfo> PropertyMap;
        static Mapper()
        {
            PropertyMap =
                typeof (T).GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)
                    .ToDictionary(p => p.Name.ToLower(), p => p);
        }
        public static void Map(ExpandoObject source, T destination)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (destination == null)
                throw new ArgumentNullException(nameof(destination));
            foreach (var kv in source)
            {
                PropertyInfo p;
                if (PropertyMap.TryGetValue(kv.Key.ToLower(), out p))
                {
                    MergeProperty(p, source, destination);
                }
            }
        }
        public static void MergeProperty(PropertyInfo pi, ExpandoObject source, object target)
        {
            var propType = pi.PropertyType;
            if (propType.IsValueType || propType == typeof (string))
            {
                var sourceVal = source.First(kvp => kvp.Key == pi.Name).Value;
                if (sourceVal == null) return;
                var newValue = Convert.ChangeType(sourceVal, propType);
                pi.SetValue(target, newValue, null);
            }
            else
            {
                var props = propType.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                foreach (var p in props)
                {
                    var sourcePropValue = source.First(kvp => kvp.Key == pi.Name).Value;
                    var targetPropValue = pi.GetValue(target, null);
                    if (sourcePropValue == null) continue;
                    if (targetPropValue == null)
                        pi.SetValue(target, source.First(kvp => kvp.Key == pi.Name).Value, null);
                    else
                        MergeProperty(p, sourcePropValue, targetPropValue);
                }
            }
        }
        public static void MergeProperty(PropertyInfo pi, object source, object target)
        {
            var propType = pi.PropertyType;
            var sourcePi = source.GetType().GetProperty(pi.Name);
            if (propType.IsValueType || propType == typeof (string))
            {
                var sourceVal = sourcePi.GetValue(source, null);
                if (sourceVal != null)
                    pi.SetValue(target, sourceVal, null);
            }
            else
            {
                var props = propType.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                foreach (var p in props)
                {
                    var sourcePropValue = sourcePi.GetValue(source, null);
                    var targetPropValue = pi.GetValue(target, null);
                    if (sourcePropValue == null) continue;
                    if (targetPropValue == null)
                        pi.SetValue(target, sourcePi.GetValue(source, null), null);
                    else
                        MergeProperty(p, sourcePropValue, targetPropValue);
                }
            }
        }
    }

    public static class Utility
    {
        public static Expression<T> Compose<T>(this Expression<T> first, Expression<T> second,
            Func<Expression, Expression, Expression> merge)
        {
            var map = first.Parameters.Select((f, i) => new {f, s = second.Parameters[i]})
                .ToDictionary(p => p.s, p => p.f);
            var secondBody = ParameterRebinder.ReplaceParameters(map, second.Body);
            return Expression.Lambda<T>(merge(first.Body, secondBody), first.Parameters);
        }

        public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> first,
            Expression<Func<T, bool>> second)
        {
            return first.Compose(second, Expression.And);
        }

        public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> first,
            Expression<Func<T, bool>> second)
        {
            return first.Compose(second, Expression.Or);
        }

    }

    public class ParameterRebinder : ExpressionVisitor
    {
        private readonly Dictionary<ParameterExpression, ParameterExpression> map;
        public ParameterRebinder(Dictionary<ParameterExpression, ParameterExpression> map)
        {
            this.map = map ?? new Dictionary<ParameterExpression, ParameterExpression>();
        }

        public static Expression ReplaceParameters(Dictionary<ParameterExpression, ParameterExpression> map,
            Expression exp)
        {
            return new ParameterRebinder(map).Visit(exp);
        }

        protected override Expression VisitParameter(ParameterExpression p)
        {
            ParameterExpression replacement;
            if (map.TryGetValue(p, out replacement))
            {
                p = replacement;
            }
            return base.VisitParameter(p);
        }

    }
}