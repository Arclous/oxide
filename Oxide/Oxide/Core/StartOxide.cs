﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Oxide.Core.Base.OxideDataBaseModel;
using Oxide.Core.Data.Context;
using Oxide.Core.Engines;
using Oxide.Core.Managers.AssemblyManager;
using Oxide.Core.Managers.AttiributeManager;
using Oxide.Core.Managers.CultureManager;
using Oxide.Core.Managers.EventsManager;
using Oxide.Core.Managers.LayoutManager;
using Oxide.Core.Managers.ModuleManager;
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.Managers.ResourceManager;
using Oxide.Core.Managers.RouteManager;
using Oxide.Core.Managers.ScheduledJobManager;
using Oxide.Core.Managers.SettingsManager;
using Oxide.Core.Managers.ThemeManager;
using Oxide.Core.Managers.ViewEngineManager;
using Oxide.Core.Managers.WidgetManager;
using Oxide.Core.Services.CacheItemService;
using Oxide.Core.Services.Interface.Registers.FilterRegister;
using Oxide.Core.Services.Interface.Registers.OxideStarter;
using Oxide.Core.Services.Interface.Registers.ResourceRegister;
using Oxide.Core.Services.ModuleService;
using Oxide.Core.Services.OxideDataService;
using Oxide.Core.Services.ResourceService;
using Oxide.Core.Services.SiteSettings;
using Oxide.Core.Syntax;
using Oxide.Migrations;

namespace Oxide.Core
{
    public static class StartOxide
    {
        private static IModuleService _moduleService;
        private static IOxideDataService _oxideDataService;

        public static void Start(BundleCollection boundleCollection, GlobalFilterCollection filters,
            RouteCollection routes)
        {
            IProviderManager providerManager = new ProviderManager();
            _oxideDataService = providerManager.Provide<IOxideDataService>();

            //Load application settings
            SettingsManager.LoadApplicationSettings();

            //Before loading modules, update core database in case if it is changed 
            if (SettingsManager.IsAdmin())
                UpdateCoreDatabase();

            //Prepeare module service
            _moduleService = providerManager.Provide<IModuleService>();

            //Load Modules
            var moduleManager = new ModuleManager(_moduleService);
            AssemblyManager.ModuleActivationList = _moduleService.GetAllModules();
            moduleManager.LoadModules();

            //Update database for each active modules
            // UpdateDatabase();

            //Register areas and routes
            RouteManager.Register(RouteTable.Routes);

            //Register Engines for each active module
            RunFilterEngine(GlobalFilters.Filters);
            RunResourceEngines(BundleTable.Bundles);

            //Load Widgets
            var widgetManager = new WidgetManager();
            widgetManager.LoadWidgets();

            //Load admin widgets
            widgetManager.LoadAdminWidgets();

            //Load Themes
            var themeManager = providerManager.Provide<IThemeManager>();
            themeManager.LoadThemes(Constants.ThemeNameSpace);
            _oxideDataService.SetDefaultTheme();

            //Load Layouts
            var layoutManager = providerManager.Provide<ILayoutManager>();
            layoutManager.LoadLayouts(Constants.ThemeNameSpace);

            //Register view engine
            RegisterViewEngine();

            //Get all cache items
            var cacheItemService = providerManager.Provide<ICacheItemService>();
            Oxide.LoadedCacheItems = cacheItemService.GetAllCacheItems();

            //Load site defaults
            SettingsManager.LoadDefaultSettings();

            //Set Default Modules
            if (SettingsManager.IsAdmin())
            {
                var oxideDataService = providerManager.Provide<IOxideDataService>();
                oxideDataService.InstallDefaultModules();
            }

            //Register models
            RegisterModels();

            //Run all scheduled background job registerers
            RunScheduledBackgroundJobRegisterers();

            //Register events
            RegisterEvents();

            //Load Language Resources
            var resourceService = providerManager.Provide<IResourceService>();
            var cultureManager = providerManager.Provide<ICultureManager>();
            var siteSettingsService = providerManager.Provide<ISiteSettingsService>();
            ResourceManager.RegisterProvider(resourceService, cultureManager, siteSettingsService);
            ResourceManager.LoadTranslations();

            //Get All Attiributes
            AttiributeManager.OrganizeAdminRoles();

            //Create Defaults
            _oxideDataService.CreateDefaultPages();

            //Run all starters
            RunStarters();
            _oxideDataService.CompleteInitilization();
        }

        //Here we define all functions which will run releated functions
        //from the releated interface

        #region EngineRunners

        /// <summary>
        ///     Runs all class which is inherited from IFilterRegisterEngine interface
        /// </summary>
        private static void RunFilterEngine(GlobalFilterCollection filters)
        {
            Oxide.Filters = filters;
            foreach (var instance in AssemblyManager.GetFor<IFilterRegisterer>())
            {
                instance.RegisterFilter(filters);
            }
        }

        /// <summary>
        ///     Runs all class which is inherited from IResourceRegisterEngine interface
        /// </summary>
        private static void RunResourceEngines(BundleCollection boundleCollection)
        {
            BundleTable.EnableOptimizations = true;
            Oxide.BoundleCollection = boundleCollection;
            foreach (var instance in AssemblyManager.GetFor<IResourceRegisterer>())
            {
                instance.RegisterResource(boundleCollection);
            }
        }

        /// <summary>
        ///     Register all models which is inherited from IOxideDataBaseModel
        /// </summary>
        private static void RegisterModels()
        {
            foreach (var instance in
                AssemblyManager.GetFor<IOxideDataBaseModel>()
                               .Where(instance => !Oxide.LoadedModels.Contains(instance.GetType())))
            {
                Oxide.LoadedModels.Add(instance.GetType());
            }
        }

        /// <summary>
        ///     Run all class which is inherited from IOxideStarter
        /// </summary>
        private static void RunStarters()
        {
            var providerManager = new ProviderManager();
            foreach (var instance in AssemblyManager.GetFor<IOxideStarterRegisterer>())
            {
                instance.Start(providerManager);
            }
        }

        /// <summary>
        ///     Runs all class which is inherited from OxideScheduledBackgroundJob
        /// </summary>
        private static void RunScheduledBackgroundJobRegisterers()
        {
            IProviderManager providerManager = new ProviderManager();
            var scheduledJobManager = providerManager.Provide<IScheduledJobManager>();
            scheduledJobManager.RegisterAllBackgroundJobs();
        }

        /// <summary>
        ///     Registers to events listener which are inherited from IOxideEventManager
        /// </summary>
        private static void RegisterEvents()
        {
            //Call initalizer
            Oxide.LoadedEvents = AssemblyManager.GetFor<IOxideEventManager>();
            foreach (var instance in Oxide.LoadedEvents)
                instance.Initalize();
        }

        /// <summary>
        ///     Update core database, it should run before runing any other service or manager
        /// </summary>
        private static void UpdateCoreDatabase()
        {
            using (var oxideContext = new OxideContext())
            {
                oxideContext.Database.CreateIfNotExists();
                var configuration = new Configuration
                {
                    TargetDatabase =
                        new DbConnectionInfo(oxideContext.Database.Connection.ConnectionString,
                            Syntax.Syntax.ProviderNames.SystemDataSqlClient)
                };
                var migrator = new DbMigrator(configuration);
                migrator.Update();
            }

            //Add defaults
            _oxideDataService.InstallCultures();
            _oxideDataService.InstallTimeZones();
            _oxideDataService.InstallDefaultUserAndRoles();
            _oxideDataService.InstallSiteDefaults();
        }

        /// <summary>
        ///     Registers Oxide View Engine
        /// </summary>
        private static void RegisterViewEngine()
        {
            Oxide.ViewLocationFormats = ViewEngineManager.GetViewLocations();
            Oxide.PartialViewLocationFormats = ViewEngineManager.GetPartialViewLocations();
            Oxide.MasterLocationFormats = ViewEngineManager.GetMasterLocations();
            Oxide.AreaMasterLocationFormats = ViewEngineManager.GetAreaMasterLocations();
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new OxideViewEngine());
        }

        #endregion
    }
}