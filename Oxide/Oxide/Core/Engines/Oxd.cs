﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Newtonsoft.Json;
using Oxide.Areas.Admin.Controllers;
using Oxide.Areas.Admin.ViewModels;
using Oxide.Core.Data.Context;
using Oxide.Core.Data.Repository;
using Oxide.Core.Helpers;
using Oxide.Core.Managers.AreaManager;
using Oxide.Core.Managers.CacheManager;
using Oxide.Core.Managers.ProviderManager;
using Oxide.Core.Managers.WidgetManager;
using Oxide.Core.Models.Base;
using Oxide.Core.Models.Cache;
using Oxide.Core.Models.Layouts;
using Oxide.Core.Models.Pages;
using Oxide.Core.Services.CacheItemService;
using Oxide.Core.Services.OxideService;
using Oxide.Core.Services.OxideUiService;
using Oxide.Core.Syntax;
using Oxide.Core.ViewModels.PageViewModels;

namespace Oxide.Core.Engines
{
    public static class Oxd
    {
        //TODO Clean Code
        private static readonly IProviderManager ProviderManager = new ProviderManager();
        private static readonly IWidgetManager WidgetManager = ProviderManager.Provide<IWidgetManager>();

        private static readonly IOxideCacheManager CacheManager =
            new OxideCacheManager(new CacheItemService(new OxideRepository<CacheItem>(new OxideContext())));

        /// <summary>
        ///     Renders page widgets
        /// </summary>
        /// <param name="pageWidget">Page to render</param>
        public static IHtmlString RenderWidget(PageWidget pageWidget)
        {
            //Get and return widget result
            return WidgetManager.RenderFront(pageWidget);
        }
        /// <summary>
        ///     Renders page widgets
        /// </summary>
        /// <param name="mainLayoutWidget">Main layout widgets to render</param>
        public static IHtmlString RenderWidget(MainLayoutWidget mainLayoutWidget)
        {
            //Get and return widget result
            return WidgetManager.RenderFront(mainLayoutWidget);
        }
        private static IHtmlString RenderInnerSectorPreparer(string sector, RenderPage renderPage)
        {
            var sb = new StringBuilder();
            for (var dIndex = 0; dIndex <= renderPage.InnerElements[sector].Widgets.Count - 1; dIndex++)
                sb.Append(RenderWidget(renderPage.InnerElements[sector].Widgets[dIndex]).ToHtmlString());
            return MvcHtmlString.Create(sb.ToString());
        }
        private static IHtmlString RenderSectorPreparer(string sector, RenderPage renderPage)
        {
            var sb = new StringBuilder();
            for (var dIndex = 0; dIndex <= renderPage.MainElements[sector].Widgets.Count - 1; dIndex++)
                sb.Append(RenderWidget(renderPage.MainElements[sector].Widgets[dIndex]).ToHtmlString());
            return MvcHtmlString.Create(sb.ToString());
        }
        /// <summary>
        ///     Renders widgets inside the inner layouts of to page
        /// </summary>
        /// <param name="sector">Layout sector to render</param>
        /// <param name="renderPage">Page to render</param>
        public static IHtmlString RenderInnerSector(string sector, RenderPage renderPage)
        {
            if (!renderPage.Page.AggresiveCacheEnabled) return RenderInnerSectorPreparer(sector, renderPage);
            var cacheCode = $@"{sector}-{renderPage.Page.Id}";
            var cacheName = $@"{sector}-{renderPage.Page.Name}-Aggresive!";
            var html = CacheManager.GetOrSet(cacheCode, cacheName, renderPage.Page.Name,
                () => RenderInnerSectorPreparer(sector, renderPage));
            return html;
        }
        /// <summary>
        ///     Renders widgets inside the maain layouts of to page
        /// </summary>
        /// <param name="sector">Layout sector to render</param>
        /// <param name="renderPage">Page to render</param>
        public static IHtmlString RenderSector(string sector, RenderPage renderPage)
        {
            if (!renderPage.Page.AggresiveCacheEnabled) return RenderSectorPreparer(sector, renderPage);
            var cacheCode = $@"{sector}-{renderPage.Page.Id}";
            var cacheName = $@"{sector}-{renderPage.Page.Name}-Aggresive!";
            var html = CacheManager.GetOrSet(cacheCode, cacheName, renderPage.Page.Name,
                () => RenderSectorPreparer(sector, renderPage));
            return html;
        }
        /// <summary>
        ///     Renders content selecter
        /// </summary>
        /// <param name="htmlElementId">Html id of the container</param>
        /// <param name="contentName">Name of the content</param>
        /// <param name="view">View file, optional, in case of the customize</param>
        /// <param name="multiple">Multiple selection option</param>
        /// <param name="selected">Id is of the selected contents</param>
        public static IHtmlString RenderContentSelecter(string htmlElementId, string contentName, string view = null,
            bool? multiple = false, string selected = null)
        {
            var selectedContents = new List<object>();
            if (selected != null)
            {
                var oxideUiService = ProviderManager.Provide<IOxideUiService>();
                var items = selected.Split(Constants.Splitter);
                if (items.Count() == 1)
                {
                    dynamic content = oxideUiService.GetContent(contentName, selected);
                    if (content != null)
                    {
                        selectedContents.Add(content);
                    }
                }
                else if (items.Count() > 1)
                {
                    var contents = oxideUiService.GetContents(contentName, selected);
                    selectedContents.AddRange(contents.Cast<dynamic>().Cast<object>());
                }
            }
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            var context = new HttpContextWrapper(HttpContext.Current);
            var routeData = new RouteData();
            var contentListUrl = urlHelper.Action(MVC.Admin.OxideUi.ActionNames.GetContents, MVC.Admin.OxideUi.Name,
                new {area = T4MVCHelpers.DefaultArea()});
            routeData.Values.Add(Constants.RenderController, MVC.Admin.OxideUi.Name);
            var controllerContext = new ControllerContext(new RequestContext(context, routeData),
                new OxideUiController());
            var contentHolder = new ContentViewModelHolder
            {
                ElementId = htmlElementId,
                ContentName = contentName,
                LoadContentUrl = contentListUrl,
                Multiple = multiple,
                SelectedRecords = JsonConvert.SerializeObject(selectedContents)
            };
            return OxideWidgetHelper.RenderViewToHtml(controllerContext, view ?? MVC.Admin.OxideUi.Views.ContentSelecter,
                contentHolder);
        }
        /// <summary>
        ///     Renders file content selecter
        /// </summary>
        /// <param name="htmlElementId">Html id of the container</param>
        /// <param name="mimeType">Mime type of the file</param>
        /// <param name="view">View file, optional, in case of the customize</param>
        /// <param name="multiple">Multiple selection option</param>
        /// <param name="selected">Id is of the selected contents</param>
        public static IHtmlString RenderFileContentSelecter(string htmlElementId, string mimeType, string view = null,
            bool? multiple = false, string selected = null)
        {
            var selectedContents = new List<object>();
            if (selected != null)
            {
                var oxideUiService = ProviderManager.Provide<IOxideUiService>();
                var items = selected.Split(Constants.Splitter);
                if (items.Count() == 1)
                {
                    dynamic content = oxideUiService.GetContent(Constants.FileContent, selected);
                    if (content != null)
                    {
                        selectedContents.Add(content);
                    }
                }
                else if (items.Count() > 1)
                {
                    var contents = oxideUiService.GetContents(Constants.FileContent, selected);
                    selectedContents.AddRange(contents.Cast<dynamic>().Cast<object>());
                }
            }
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            var context = new HttpContextWrapper(HttpContext.Current);
            var routeData = new RouteData();
            var contentListUrl = urlHelper.Action(MVC.Admin.OxideUi.ActionNames.GetFileContents, MVC.Admin.OxideUi.Name,
                new {area = T4MVCHelpers.DefaultArea()});
            routeData.Values.Add(Constants.RenderController, MVC.Admin.OxideUi.Name);
            var controllerContext = new ControllerContext(new RequestContext(context, routeData),
                new OxideUiController());
            var contentHolder = new ContentViewModelHolder
            {
                ElementId = htmlElementId,
                ContentName = mimeType,
                LoadContentUrl = contentListUrl,
                Multiple = multiple,
                SelectedRecords = JsonConvert.SerializeObject(selectedContents)
            };
            return OxideWidgetHelper.RenderViewToHtml(controllerContext, view ?? MVC.Admin.OxideUi.Views.ContentSelecter,
                contentHolder);
        }
        /// <summary>
        ///     Renders media content selecter
        /// </summary>
        /// <param name="htmlElementId">Html id of the container</param>
        /// <param name="multiple">Multiple selection option</param>
        /// <param name="selected">Id is of the selected contents</param>
        public static IHtmlString RenderMediaSelecter(string htmlElementId, bool? multiple = false,
            string selected = null)
        {
            var selectedContents = new List<object>();
            var items = new List<int>();
            if (selected != null)
            {
                var oxideUiService = ProviderManager.Provide<IOxideUiService>();
                items = selected.Split(Constants.Splitter).Select(x => Convert.ToInt32(x)).ToList();
                if (items.Count() == 1)
                {
                    dynamic content = oxideUiService.GetContent(Constants.FileContent, selected);
                    if (content != null)
                    {
                        selectedContents.Add(content);
                    }
                }
                else if (items.Count() > 1)
                {
                    var contents = oxideUiService.GetContents(Constants.FileContent, selected);
                    selectedContents.AddRange(contents.Cast<dynamic>().Cast<object>());
                }
            }
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            var context = new HttpContextWrapper(HttpContext.Current);
            var routeData = new RouteData();
            var contentListUrl = urlHelper.Action(MVC.Admin.OxideUi.ActionNames.GetMediaContents, MVC.Admin.OxideUi.Name,
                new {area = T4MVCHelpers.DefaultArea()});
            routeData.Values.Add(Constants.RenderController, MVC.Admin.OxideUi.Name);
            var controllerContext = new ControllerContext(new RequestContext(context, routeData),
                new OxideUiController());
            var contentHolder = new FilterContentViewModelHolder
            {
                ElementId = htmlElementId,
                LoadContentUrl = contentListUrl,
                SelectedRecords =
                    JsonConvert.SerializeObject(selectedContents.OrderBy(d => items.IndexOf(((dynamic) d).Id))),
                Multiple = multiple,
                LoadDirectoriesUrl = urlHelper.Action("DirectoryList", "FileManager", new {area = "Oxide.Media"}),
                LoadSubDirectoriesUrl = urlHelper.Action("DirectorySubList", "FileManager", new {area = "Oxide.Media"})
            };
            return OxideWidgetHelper.RenderViewToHtml(controllerContext, MVC.Admin.OxideUi.Views.MediaContentSelecter,
                contentHolder);
        }

        /// <summary>
        ///     Renders data list
        /// </summary>
        /// <param name="contentName">Name of the content</param>
        /// <param name="editUrl">Edit url for editing data</param>
        /// <param name="deleteUrl">Delete url for delting data</param>
        /// <param name="loadUrl">Load url for the list</param>
        /// <param name="fields">Fields to display, as default Name and Title</param>
        /// <param name="fieldsTitles">Custom titles</param>
        /// <param name="withPicture">Data with picture or without picture</param>
        /// <param name="pictureField">Name of the picture field</param>
        public static IHtmlString RenderDataList(string contentName, string editUrl = null, string deleteUrl = null,
            string loadUrl = null, string fields = null, string fieldsTitles = null, bool withPicture = false,
            string pictureField = null)
        {
            var oxideService = ProviderManager.Provide<IOxideServices>();
            var fieldList = new List<string>();
            var fieldTitleList = new List<string>();
            if (fields != null)
            {
                if (fieldsTitles == null)
                {
                    foreach (var field in fields.Split(Constants.Splitter))
                    {
                        fieldList.Add(field);
                        fieldTitleList.Add(oxideService.GetText(field));
                    }
                }
                else
                {
                    var fieldListTemp = fields.Split(Constants.Splitter);
                    var fieldTitleListtemp = fieldsTitles.Split(Constants.Splitter);
                    for (var index = 0; index <= fieldListTemp.Count() - 1; index++)
                    {
                        fieldList.Add(fieldListTemp[index]);
                        fieldTitleList.Add(oxideService.GetText(fieldTitleListtemp[index]));
                    }
                }
            }
            else
            {
                fieldList.Add(Constants.NameField);
                fieldTitleList.Add(oxideService.GetText(Constants.NameFieldTitle, Constants.Admin));
                fieldList.Add(Constants.TitleField);
                fieldTitleList.Add(oxideService.GetText(Constants.TitleFieldTitle, Constants.Admin));
            }
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            var context = new HttpContextWrapper(HttpContext.Current);
            var routeData = new RouteData();
            var contentListUrl = urlHelper.Action(MVC.Admin.OxideUi.ActionNames.GetDataListContents,
                MVC.Admin.OxideUi.Name, new {area = T4MVCHelpers.DefaultArea()});
            routeData.Values.Add(Constants.RenderController, MVC.Admin.OxideUi.Name);
            var controllerContext = new ControllerContext(new RequestContext(context, routeData),
                new OxideUiController());
            var contentHolder = new ContentViewModelHolder
            {
                ContentName = contentName,
                LoadContentUrl = loadUrl ?? contentListUrl,
                EditUrl = editUrl,
                DeleteUrl = deleteUrl,
                Fields = fieldList,
                FieldTitles = fieldTitleList,
                WithPictures = withPicture,
                PictureField = pictureField
            };
            return OxideWidgetHelper.RenderViewToHtml(controllerContext, MVC.Admin.OxideUi.Views.DataList, contentHolder);
        }
        /// <summary>
        ///     Renders media content selecter
        /// </summary>
        /// <param name="htmlElementId">Html id of the container</param>
        /// <param name="contentName">Name of the content</param>
        /// <param name="imageField">Name of the image field</param>
        /// <param name="fieldName1">Name of the first field, as default name</param>
        /// <param name="view">View file, optional, in case of the customize</param>
        /// <param name="multiple">Multiple selection option</param>
        /// <param name="selected">Id is of the selected contents</param>
        /// <param name="fieldName2">Name of the second field, as default Title</param>
        /// <param name="pictureEnabled">Display with picture or without picture</param>
        public static IHtmlString RenderMediaContentSelecter(string htmlElementId, string contentName, string imageField,
            string fieldName1, string view = null, bool? multiple = false, string selected = null,
            string fieldName2 = null, bool pictureEnabled = true)
        {
            var selectedContents = new List<object>();
            var newContentList = new List<object>();
            if (selected != null)
            {
                var contentType = Oxide.LoadedModels.FirstOrDefault(x => x.Name == contentName);
                var oxideUiService = ProviderManager.Provide<IOxideUiService>();
                var items = selected.Split(Constants.Splitter);
                if (items.Count() == 1)
                {
                    dynamic content = oxideUiService.GetContent(contentName, selected);
                    if (content != null)
                    {
                        selectedContents.Add(content);
                    }
                }
                else if (items.Count() > 1)
                {
                    var contents = oxideUiService.GetContents(contentName, selected);
                    selectedContents.AddRange(contents.Cast<dynamic>().Cast<object>());
                }
                if (contentType != null)
                    foreach (
                        var field in contentType.GetProperties().Where(x => x.CanRead && x.Name == imageField).ToList())
                    {
                        foreach (var v in selectedContents)
                        {
                            var content = oxideUiService.GetContent(Constants.FileContent, field.GetValue(v).ToString());
                            var newObject = DynamicHelper.ConvertToExpando(v);
                            if (pictureEnabled)
                                DynamicHelper.AddProperty(newObject, Constants.ExpandoImageField,
                                    ((dynamic) content).Path);
                            newContentList.Add(newObject);
                        }
                    }
            }
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            var context = new HttpContextWrapper(HttpContext.Current);
            var routeData = new RouteData();
            var contentListUrl = urlHelper.Action(MVC.Admin.OxideUi.ActionNames.GetMediaContentsWithImage,
                MVC.Admin.OxideUi.Name, new {area = T4MVCHelpers.DefaultArea()});
            routeData.Values.Add(Constants.RenderController, MVC.Admin.OxideUi.Name);
            var controllerContext = new ControllerContext(new RequestContext(context, routeData),
                new OxideUiController());
            var contentHolder = new ContentViewModelHolder
            {
                ElementId = htmlElementId,
                ContentName = contentName,
                LoadContentUrl = contentListUrl,
                Multiple = multiple,
                SelectedRecords = newContentList,
                Field1 = fieldName1,
                Field2 = fieldName2,
                PictureField = imageField,
                WithPictures = pictureEnabled
            };
            return OxideWidgetHelper.RenderViewToHtml(controllerContext,
                view ?? MVC.Admin.OxideUi.Views.ImageContentSelecter, contentHolder);
        }
        /// <summary>
        ///     Renders editor view
        /// </summary>
        /// <param name="htmlElementId">Html id of the container</param>
        /// <param name="content">Content</param>
        /// <param name="heigtht">Height of the editor as pixel</param>
        /// <param name="width">Width of the editor as pixel</param>
        public static IHtmlString RenderEditor(string htmlElementId, string content = null, string heigtht = null,
            string width = null)
        {
            var model = new CkEditorViewModel
            {
                ElementId = htmlElementId,
                Content = content,
                Height = heigtht,
                Width = width,
                Language = AreaManager.GetAdminUserLanguageCode2()
            };
            var context = new HttpContextWrapper(HttpContext.Current);
            var routeData = new RouteData();
            routeData.Values.Add(Constants.RenderController, MVC.Admin.OxideUi.Name);
            var controllerContext = new ControllerContext(new RequestContext(context, routeData),
                new OxideUiController());
            return OxideWidgetHelper.RenderViewToHtml(controllerContext, MVC.Admin.OxideUi.Views.ViewNames.CkEditor,
                model);
        }
        /// <summary>
        ///     Renders editor view
        /// </summary>
        /// <param name="htmlElementId">Html id of the container</param>
        /// <param name="value">Color value</param>
        public static IHtmlString RenderColorPicker(string htmlElementId, string value = null)
        {
            var model = new ColorPickerViewModel {ElementId = htmlElementId, Value = value};
            var context = new HttpContextWrapper(HttpContext.Current);
            var routeData = new RouteData();
            routeData.Values.Add(Constants.RenderController, MVC.Admin.OxideUi.Name);
            var controllerContext = new ControllerContext(new RequestContext(context, routeData),
                new OxideUiController());
            return OxideWidgetHelper.RenderViewToHtml(controllerContext, MVC.Admin.OxideUi.Views.ViewNames.ColorPicker,
                model);
        }
        /// <summary>
        ///     Renders confirm helper
        /// </summary>
        public static IHtmlString RenderConfirmHelper()
        {
            var context = new HttpContextWrapper(HttpContext.Current);
            var routeData = new RouteData();
            routeData.Values.Add(Constants.RenderController, MVC.Admin.OxideUi.Name);
            var controllerContext = new ControllerContext(new RequestContext(context, routeData),
                new OxideUiController());
            return OxideWidgetHelper.RenderViewToHtml(controllerContext, MVC.Admin.Templates.Views.ViewNames.Confirm,
                null);
        }
        /// <summary>
        ///     Renders error view
        /// </summary>
        /// <param name="title">Titke</param>
        /// <param name="message">Message</param>
        public static IHtmlString RenderError(string title, string message)
        {
            var model = new OxideModel {Header = title, ErrorMessage = message};
            var context = new HttpContextWrapper(HttpContext.Current);
            var routeData = new RouteData();
            routeData.Values.Add(Constants.RenderController, MVC.Admin.OxideUi.Name);
            var controllerContext = new ControllerContext(new RequestContext(context, routeData),
                new OxideUiController());
            return OxideWidgetHelper.RenderViewToHtml(controllerContext, MVC.Admin.Templates.Views.ViewNames.Error,
                model);
        }
    }
}