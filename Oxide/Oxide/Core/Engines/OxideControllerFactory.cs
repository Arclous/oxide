﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Castle.MicroKernel;
using Castle.MicroKernel.Lifestyle;
using Oxide.Core.Extesions;
using Oxide.Core.Managers.AreaManager;

namespace Oxide.Core.Engines
{
    public class OxideControllerFactory : DefaultControllerFactory
    {
        private readonly IKernel _kernel;
        public OxideControllerFactory(IKernel kernel)
        {
            _kernel = kernel;
        }
        public override void ReleaseController(IController controller)
        {
            _kernel.ReleaseComponent(controller);
        }
        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            if (controllerType == null)
            {
                throw new HttpException(404,
                    $"The controller for path '{requestContext.HttpContext.Request.Path}' could not be found.");
            }
            AreaManager.SetActiveArea(requestContext.GetControllerContext(controllerType).GetTypeOfRequest());
            _kernel.BeginScope();
            return (IController) _kernel.Resolve(controllerType);
        }
    }
}