﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Web.Mvc;
using Oxide.Core.Managers.ViewEngineManager;

namespace Oxide.Core.Engines
{
    public class OxideViewEngine : RazorViewEngine, IViewEngine
    {
        public OxideViewEngine()
        {
            ViewLocationFormats = Oxide.ViewLocationFormats;
            PartialViewLocationFormats = Oxide.PartialViewLocationFormats;
            MasterLocationFormats = Oxide.MasterLocationFormats;
            AreaMasterLocationFormats = Oxide.AreaMasterLocationFormats;
        }
        private static string GetFolderName(string path)
        {
            return
                (path.Substring(path.LastIndexOf("\\", StringComparison.Ordinal),
                    path.Length - path.LastIndexOf("\\", StringComparison.Ordinal))).Replace("\\", "/");
        }
        protected override IView CreatePartialView(ControllerContext controllerContext, string partialPath)
        {
            var nameSpace = controllerContext.Controller.GetType().Namespace;
            return base.CreatePartialView(controllerContext, partialPath.Replace("%1", nameSpace));
        }
        protected override IView CreateView(ControllerContext controllerContext, string viewPath, string masterPath)
        {
            var viewResult = ViewEngineManager.GetAlternateView(viewPath, controllerContext);
            var viewEngineResult = ViewEngineManager.GetViewData(controllerContext, viewResult, masterPath);
            return base.CreateView(controllerContext, viewEngineResult.ViewPath, viewEngineResult.MasterPath);
        }
    }
}