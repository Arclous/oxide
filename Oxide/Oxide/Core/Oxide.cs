﻿/* ========================================================================
 * Oxide 1.0
 * http://www.oxidecms.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Castle.Components.DictionaryAdapter;
using Castle.Windsor;
using Oxide.Core.Base.OxideBaseContext;
using Oxide.Core.Elements;
using Oxide.Core.Managers.EventsManager;
using Oxide.Core.Models.Base;
using Oxide.Core.Models.Cache;
using Oxide.Core.ViewModels.GeneralViewModels;

namespace Oxide.Core
{
    public static class Oxide
    {
        public static Assembly[] LoadedAssemblies;
        public static List<IOxideModule> LoadedModules;
        public static List<IOxideWidget> LoadedWidgets;
        public static List<IOxideDashboardWidget> LoadedDashboardWidgets;
        public static List<IOxideLayout> LoadedLayouts = new List<IOxideLayout>();
        public static List<IOxideTheme> LoadedThemes = new List<IOxideTheme>();
        public static List<IOxideBaseContext> LoadedContexts = new EditableList<IOxideBaseContext>();
        public static List<Type> LoadedModels = new EditableList<Type>();
        public static List<CacheItem> LoadedCacheItems = new EditableList<CacheItem>();
        public static IOxideEventManager[] LoadedEvents;
        public static GlobalFilterCollection Filters;
        public static RouteCollection Routes;
        public static BundleCollection BoundleCollection;
        public static string[] ViewLocationFormats;
        public static string[] PartialViewLocationFormats;
        public static string[] MasterLocationFormats;
        public static string[] AreaMasterLocationFormats;
        public static IWindsorContainer Container;
        public static Dictionary<string, string> GlobalProperties = new Dictionary<string, string>();
        public static Dictionary<string, Type> Events = new Dictionary<string, Type>();
        public static Dictionary<string, AlternateWidget> WidgetAlternates = new Dictionary<string, AlternateWidget>();
        public static Dictionary<string, string> ViewAlternates = new Dictionary<string, string>();
        public static Dictionary<string, OnlineUser> OnlineUsers = new Dictionary<string, OnlineUser>();
    }
}