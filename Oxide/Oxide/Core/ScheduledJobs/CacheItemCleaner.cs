﻿using System;
using Oxide.Core.Data.Context;
using Oxide.Core.Data.Repository;
using Oxide.Core.Models.Cache;
using Oxide.Core.Services.CacheItemService;
using Oxide.Core.Services.Implemantation;
using Quartz;

namespace Oxide.Core.ScheduledJobs
{
    public class CacheItemCleaner : OxideScheduledBackgroundJob
    {
        public override string Name { get; } = "Cache_Item_Cleaner";
        public override string Title { get; } = "Cache Item Cleaner";
        public override bool CanForceToRun { get; } = true;
        public override bool CanManage { get; } = false;

        public override Syntax.Syntax.BackgroundTaskActionType ActionType { get; } =
            Syntax.Syntax.BackgroundTaskActionType.Daily;

        public override Action<DailyTimeIntervalScheduleBuilder> DailyAction { get; } =
            s => s.WithIntervalInHours(6).OnEveryDay();

        public override Action<CalendarIntervalScheduleBuilder> Action { get; }
        public override void Execute(IJobExecutionContext context)
        {
            ICacheItemService cacheManager = new CacheItemService(new OxideRepository<CacheItem>(new OxideContext()));
            cacheManager.OrganizeCacheItems(1);
        }
    }
}