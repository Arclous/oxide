﻿using System;
using System.Linq;
using Oxide.Areas.Admin.Models;
using Oxide.Core.Data.Context;
using Oxide.Core.Data.Repository;
using Oxide.Core.Models.Pages;
using Oxide.Core.Services.Implemantation;
using Oxide.Core.Services.PageService;
using Quartz;

namespace Oxide.Core.ScheduledJobs
{
    public class PagePublisher : OxideScheduledBackgroundJob
    {
        public override string Name { get; } = "Page_Publisher";
        public override string Title { get; } = "Page Publisher";
        public override bool CanForceToRun { get; } = true;
        public override bool CanManage { get; } = false;
        public override Syntax.Syntax.ScheduledJobRunPlatforms CanRunOn { get; }

        public override Syntax.Syntax.BackgroundTaskActionType ActionType { get; } =
            Syntax.Syntax.BackgroundTaskActionType.Daily;

        public override Action<DailyTimeIntervalScheduleBuilder> DailyAction { get; } =
            s => s.StartingDailyAt(new TimeOfDay(00, 00, 00)).OnEveryDay();

        public override Action<CalendarIntervalScheduleBuilder> Action { get; }
        public override void Execute(IJobExecutionContext context)
        {
            IPageService pageService = new PageService(new OxideRepository<Page>(new OxideContext()),
                new OxideRepository<Language>(new OxideContext()));
            foreach (var page in
                pageService.GetAllPages(Syntax.Syntax.PageTypes.Standard)
                    .Where(x => x.Published == false && x.PublishLater && x.PublishDate >= DateTime.Today))
                pageService.Publish(page.Id);
        }
    }
}