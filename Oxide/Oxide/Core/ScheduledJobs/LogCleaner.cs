﻿using System;
using Oxide.Core.Data.Context;
using Oxide.Core.Data.Repository;
using Oxide.Core.Models.Log;
using Oxide.Core.Services.Implemantation;
using Oxide.Core.Services.LogService;
using Quartz;

namespace Oxide.Core.ScheduledJobs
{
    public class LogCleaner : OxideScheduledBackgroundJob
    {
        public override string Name { get; } = "Log_Cleaner";
        public override string Title { get; } = "Log Cleaner";
        public override bool CanForceToRun { get; } = true;
        public override bool CanManage { get; } = true;

        public override Syntax.Syntax.BackgroundTaskActionType ActionType { get; } =
            Syntax.Syntax.BackgroundTaskActionType.Daily;

        public override Action<DailyTimeIntervalScheduleBuilder> DailyAction { get; } =
            s => s.WithIntervalInHours(6).OnEveryDay();

        public override Action<CalendarIntervalScheduleBuilder> Action { get; }
        public override void Execute(IJobExecutionContext context)
        {
            ILogService logService = new LogService(new OxideRepository<Log>(new OxideContext()));
            logService.ClearLogs(7);
        }
    }
}