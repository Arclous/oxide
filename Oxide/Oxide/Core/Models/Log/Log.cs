﻿using System;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Core.Models.Log
{
    [OxideDatabase("OxideContext")]
    public class Log : OxideDatabaseModel, IOxideDataBaseModel
    {
        public virtual Syntax.Syntax.LogTypes LogType { get; set; }
        public string ModuleName { get; set; }
        public string Description { get; set; }
        public string InnerException { get; set; }
        public DateTime DateTimeInfo { get; set; }
    }
}