﻿using System;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Core.Models.Cache
{
    public class CacheItem : OxideDatabaseModel, IOxideDataBaseModel
    {
        public DateTime LastUpdate { get; set; }
        public int Value { get; set; }
        public string Group { get; set; }
        public string Instance { get; set; }
    }
}