﻿using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Core.Models.Themes
{
    [OxideDatabase("OxideContext")]
    public class Theme : OxideDatabaseModel, IOxideDataBaseModel
    {
        public bool Active { get; set; }
    }
}