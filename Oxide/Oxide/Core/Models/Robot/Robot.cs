﻿using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Core.Models.Robot
{
    [OxideDatabase("OxideContext")]
    public class Robot : OxideDatabaseModel, IOxideDataBaseModel
    {
        public string Value { get; set; }
    }
}