﻿using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Core.Models.Layouts
{
    [OxideDatabase("OxideContext")]
    public class MainLayoutWidget : OxideDatabaseModel, IOxideDataBaseModel
    {
        public int Order { get; set; }
        public int LayoutId { get; set; }
        public int LayoutKeyId { get; set; }
        public string WidgetKey { get; set; }
        public int Culture { get; set; }
        public int MainPageId { get; set; }
    }
}