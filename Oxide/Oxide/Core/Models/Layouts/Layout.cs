﻿using System.ComponentModel.DataAnnotations.Schema;
using Oxide.Core.Elements;

namespace Oxide.Core.Models.Layouts
{
    public class Layout
    {
        [NotMapped]
        public IOxideLayout LayoutItem { get; set; }
    }
}