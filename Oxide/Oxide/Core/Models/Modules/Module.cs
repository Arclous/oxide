﻿using System.ComponentModel.DataAnnotations;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Core.Models.Modules
{
    [OxideDatabase("OxideContext")]
    public class Module : OxideDatabaseModel, IOxideDataBaseModel
    {
        [StringLength(1000, MinimumLength = 1,
            ErrorMessage = "Description of the module needs be between 1-1000 characters.")]
        [DataType(DataType.MultilineText, ErrorMessage = "Description of the module needs be text format")]
        public string Description { get; set; }

        [Required]
        [StringLength(255, MinimumLength = 1,
            ErrorMessage = "Author Name of the module needs be between 1-255 characters.")]
        [DataType(DataType.Text, ErrorMessage = "Author Name of the module needs be text format")]
        public string Author { get; set; }

        [Required]
        [StringLength(255, MinimumLength = 1, ErrorMessage = "Category of the module needs be between 1-255 characters."
            )]
        [DataType(DataType.Text, ErrorMessage = "Category of the module needs be text format")]
        public string Category { get; set; }

        [Required]
        [StringLength(500, MinimumLength = 1,
            ErrorMessage = "Preview Image of the module needs be between 1-500 characters.")]
        [DataType(DataType.Text, ErrorMessage = "Preview Image of the module needs be text format")]
        public string PreviewImageUrl { get; set; }

        [Required]
        public bool Active { get; set; }

        [Required]
        [StringLength(1000, MinimumLength = 1,
            ErrorMessage = "Namespace of the module needs be between 1-1000 characters.")]
        [DataType(DataType.Text, ErrorMessage = "Namespace of the module needs be text format")]
        public string Namespace { get; set; }
    }
}