﻿namespace Oxide.Core.Models.Base
{
    public class OxideBaseViewModel
    {
        public object Model { get; set; }
        public OxideModel Holder { get; set; }
    }
}