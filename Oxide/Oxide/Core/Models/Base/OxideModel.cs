﻿namespace Oxide.Core.Models.Base
{
    public class OxideModel
    {
        public int RequestType { get; set; }
        public int Id { get; set; }
        public string Header { get; set; }
        public string Title { get; set; }
        public object Parameters { get; set; }
        public string SuccessMessage { get; set; }
        public string ErrorMessage { get; set; }
    }
}