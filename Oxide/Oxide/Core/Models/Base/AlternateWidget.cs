﻿using Oxide.Core.Elements;

namespace Oxide.Core.Models.Base
{
    public class AlternateWidget
    {
        public IOxideWidget Widget { get; set; }
        public Syntax.Syntax.AlternateTypes AlternateTypes { get; set; }
        public bool ModifyContext { get; set; }
    }
}