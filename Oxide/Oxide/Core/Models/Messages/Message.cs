﻿using System;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Core.Models.Messages
{
    [OxideDatabase("OxideContext")]
    public class Message : OxideDatabaseModel, IOxideDataBaseModel
    {
        public string MessageBody { get; set; }
        public DateTime MessageDateTime { get; set; }
        public bool IsMessageRead { get; set; }
        public AdminUser.AdminUser Sender { get; set; }
        public AdminUser.AdminUser Reciever { get; set; }
    }
}