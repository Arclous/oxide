﻿using System.Collections.Generic;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Core.Models.AdminRole
{
    public class AdminRole : OxideDatabaseModel, IOxideDataBaseModel
    {
        public virtual List<AdminRolePermission> Permissions { get; set; }
    }
}