﻿using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Core.Models.AdminRole
{
    public class AdminRolePermission : OxideDatabaseModel, IOxideDataBaseModel
    {
        public virtual AdminPermission.AdminPermission Permission { get; set; }
        public virtual AdminRole Role { get; set; }
    }
}