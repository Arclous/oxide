﻿using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Core.Models.Settings
{
    [OxideDatabase("OxideContext")]
    public class EmailSettings : OxideDatabaseModel, IOxideDataBaseModel
    {
        public string Sender { get; set; }
        public string HostName { get; set; }
        public int PortNumber { get; set; }
        public bool EnableSsl { get; set; }
        public bool RequireCredentials { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}