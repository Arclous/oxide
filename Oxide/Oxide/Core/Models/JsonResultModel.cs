﻿namespace Oxide.Core.Models
{
    public class JsonResultModel
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }
    public class JsonWidgetActionResultModel : JsonResultModel
    {
        public bool Active { get; set; }
    }
    public class JsonPageActionResultModel : JsonResultModel
    {
        public bool Published { get; set; }
    }
}