﻿using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Core.Models.Redirection
{
    [OxideDatabase("OxideContext")]
    public class Redirection : OxideDatabaseModel, IOxideDataBaseModel
    {
        public string Url { get; set; }

        public string NewUrl { get; set; }

        public virtual Syntax.Syntax.RedirectionStatusCode StatusCode { get; set; }

        public int StatusCodeSelected { get; set; }

        public bool Active { get; set; }
    }
}