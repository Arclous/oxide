﻿using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Core.Models.AdminUser
{
    public class AdminUserRole : OxideDatabaseModel, IOxideDataBaseModel
    {
        public virtual AdminUser AdminUser { get; set; }
        public virtual AdminRole.AdminRole Role { get; set; }
    }
}