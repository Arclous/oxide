﻿using System;
using System.Collections.Generic;
using Oxide.Areas.Admin.Models;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Core.Models.AdminUser
{
    public class AdminUser : OxideDatabaseModel, IOxideDataBaseModel
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public virtual ICollection<AdminUserRole> Roles { get; set; }
        public int UserImageId { get; set; }
        public DateTime LastLogin { get; set; }
        public DateTime CreateDateTime { get; set; }
        public bool IsActive { get; set; }
        public bool IsSuperAdmin { get; set; }
        public virtual Language UserLanguage { get; set; }
    }
}