﻿using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Core.Models.EmailTemplate
{
    [OxideDatabase("OxideContext")]
    public class EmailTemplate : OxideDatabaseModel, IOxideDataBaseModel
    {
        public string Template { get; set; }
    }
}