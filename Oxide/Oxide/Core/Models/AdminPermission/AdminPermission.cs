﻿using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Core.Models.AdminPermission
{
    public class AdminPermission : OxideDatabaseModel, IOxideDataBaseModel
    {
        public string Group { get; set; }
        public string Owner { get; set; }
        public string Key { get; set; }
    }
}