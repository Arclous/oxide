﻿using System;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Core.Models.ScheduledJob
{
    public class ScheduledJob : OxideDatabaseModel, IOxideDataBaseModel
    {
        public string Owner { get; set; }
        public bool Active { get; set; }
        public bool CanManage { get; set; }
        public DateTime LastStart { get; set; }
        public DateTime LastFinish { get; set; }
        public DateTime NextStart { get; set; }
        public string Key { get; set; }
        public string Group { get; set; }
        public int Type { get; set; }
        public int IntervalType { get; set; }
        public int Interval { get; set; }
        public Syntax.Syntax.ScheduledJobRunPlatforms CanRunOn { get; set; }
    }
}