﻿using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Core.Models.Dashboard
{
    [OxideDatabase("OxideContext")]
    public class Dashboard : OxideDatabaseModel, IOxideDataBaseModel
    {
        public int Order { get; set; }
        public int UserId { get; set; }
        public string WidgetKey { get; set; }
    }
}