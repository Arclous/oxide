﻿using System.ComponentModel.DataAnnotations;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Core.Models.Widgets
{
    [OxideDatabase("OxideContext")]
    public class Widget : OxideDatabaseModel, IOxideDataBaseModel
    {
        [Required]
        [StringLength(300, MinimumLength = 1,
            ErrorMessage = "Unique key of the widget needs be between 1-255 characters.")]
        [DataType(DataType.Text, ErrorMessage = "Unique key of the widget needs be text format")]
        public string WidgetKey { get; set; }
    }
}