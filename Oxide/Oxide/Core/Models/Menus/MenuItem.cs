﻿using System.Collections.Generic;

namespace Oxide.Core.Models.Menus
{
    public class MenuItem
    {
        public string Key { get; set; }
        public string Title { get; set; }
        public string ActionName { get; set; }
        public string ControllerName { get; set; }
        public string ModuleName { get; set; }
        public List<MenuItem> SubMenuItems { get; set; }
        public string UrlAddress { get; set; }
        public int Order { get; set; }
        public bool HasSubMenu => (SubMenuItems != null) && (SubMenuItems.Count > 0);
        public object Params { get; set; }
        public string PermissionKey { get; set; }
    }
}