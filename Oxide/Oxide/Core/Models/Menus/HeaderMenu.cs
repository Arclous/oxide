﻿using System.Collections.Generic;

namespace Oxide.Core.Models.Menus
{
    public class HeaderMenu
    {
        public string Key { get; set; }
        public string Title { get; set; }
        public string IconCss { get; set; }
        public string ModuleName { get; set; }
        public List<MenuItem> SubMenuItems { get; set; }
        public int Order { get; set; }
        public string PermissionKey { get; set; }
    }
}