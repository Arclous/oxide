﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Oxide.Areas.Admin.Models;
using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Core.Models.Pages
{
    [OxideDatabase("OxideContext")]
    public class Page : OxideDatabaseModel, IOxideDataBaseModel
    {
        [Required]
        [StringLength(255, MinimumLength = 1, ErrorMessage = "Page Key of the page needs be between 1-255 characters.")]
        [DataType(DataType.Text, ErrorMessage = "Page Key of the page needs be text format")]
        public string PageKey { get; set; }

        [Required]
        [StringLength(750, MinimumLength = 1, ErrorMessage = "Url of the Page needs be between 1-750 characters.")]
        [DataType(DataType.Url, ErrorMessage = "Url of the Page needs be text format")]
        public string Url { get; set; }

        public int Order { get; set; }

        [StringLength(1000, MinimumLength = 1, ErrorMessage = "Tags of the Page needs be between 1-1000 characters.")]
        [DataType(DataType.MultilineText, ErrorMessage = "Tags of the Page needs be text format")]
        public string Tag { get; set; }

        [StringLength(1000, MinimumLength = 1,
            ErrorMessage = "Description of the Page needs be between 1-1000 characters.")]
        [DataType(DataType.MultilineText, ErrorMessage = "Description of the Page needs be text format")]
        public string Description { get; set; }

        public bool VisibleForSite { get; set; }
        public bool VisibleForSeo { get; set; }
        public List<int> WidgetsId { get; set; }
        public virtual ICollection<PageWidget> Widgets { get; set; }
        public int LanguageId { get; set; }
        public virtual Language Language { get; set; }
        public virtual Syntax.Syntax.PageTypes PageType { get; set; }
        public virtual Syntax.Syntax.SiteMapFrequncy SiteMapFrequncy { get; set; }
        public int BasePageId { get; set; }
        public virtual int InnerLayoutId { get; set; }
        public virtual int MainLayoutId { get; set; }
        public bool PublishImd { get; set; }
        public bool PublishLater { get; set; }
        public DateTime PublishDate { get; set; }
        public DateTime LastUpdate { get; set; }
        public DateTime CreateDate { get; set; }
        public bool Published { get; set; }
        public bool HomePage { get; set; }
        public bool AggresiveCacheEnabled { get; set; }
        public int TotalVisited { get; set; }
        public virtual double Priority { get; set; }
    }
}