﻿using Oxide.Core.Base.OxideAttributes;
using Oxide.Core.Base.OxideDataBaseModel;

namespace Oxide.Core.Models.Pages
{
    [OxideDatabase("OxideContext")]
    public class PageWidget : OxideDatabaseModel, IOxideDataBaseModel
    {
        public int Order { get; set; }
        public int LayoutId { get; set; }
        public int LayoutKeyId { get; set; }
        public int PageId { get; set; }
        public string WidgetKey { get; set; }
    }
}